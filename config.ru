# This file is used by Rack-based servers to start the application.

if ENV['RAILS_ENV'] == 'production'
  require 'unicorn/worker_killer'
  oom_min = (Integer(ENV['UNICORN_MEMORY_LIMIT_LOW'] || 400)) * (1024**2)
  oom_max = (Integer(ENV['UNICORN_MEMORY_LIMIT_HIGH'] || 550)) * (1024**2)
  cycle = Integer(ENV['UNICORN_MEMORY_LIMIT_CYCLE'] || 5_000)
  use Unicorn::WorkerKiller::Oom, oom_min, oom_max, cycle, true
  begin # Only activates if both ENV configs are set
    max_req_min, max_req_max = Integer(ENV['UNICORN_MAX_REQ_MIN']), Integer(ENV['UNICORN_MAX_REQ_MAX'])
    use Unicorn::WorkerKiller::MaxRequests, max_req_min, max_req_max, true
  rescue
  end
end

require ::File.expand_path('../config/environment',  __FILE__)

run Rails.application
