require 'rest_client'

class ClientApplicationsController < ApplicationController
  load_and_authorize_resource
  before_action :set_client_application, only: [:copy, :create_copy, :show, :edit, :preview, :update_preview, :update, :enable, :disable, :destroy, :compile_assets, :notify_clients, :purge_routes, :invalidate_routes, :extra_attributes]
  before_action :filter,       only: [:index]
  before_action :clear_filter, only: [:index]

  respond_to :html

  def index
    @client_applications = @filter.all.page(params[:page]).per(per_page)
  end

  def show
  end

  def preview
    render layout: 'preview'
  end

  def update_preview
    representation = BaseRepresentation.find(params[:representation_id]) if params[:representation_id].present?
    client_application = @client_application.for_preview(representation)
    route = client_application.routes.find(params[:route])

    user = Yavu::API::Resource::User.new(username: 'test', full_name: 'Test User', uid: 'test') if params[:user_authenticated]
    session['HTTP_X_YAVU_USER'] = user.to_json

    server = client_application.preview_server request
    renderer = client_application.preview_renderer route, server: server, locale: 'es', logger: Rails.logger
    parameters = (params[:parameters] || {}).reject { |_, v| v.blank? }

    context = route.build_context(parameters, { 'USER' => session['HTTP_X_YAVU_USER'] })
    # Simulate what a frontend instance will receive
    context = Yavu::API::Resource::Base.from_json context.to_json
    render text: renderer.output(context._locals, params[:format]), layout: false
  end

  def new
    @client_application = ClientApplication.new
  end

  def edit
  end

  def create
    @client_application = ClientApplication.new(client_application_params)

    if @client_application.save
      redirect_to @client_application, notice: I18n.t('client_applications.new.messages.success')
    else
      render action: 'new'
    end
  end

  def copy
    @uncopyable_representations = @client_application.representations
      .where(id: RepresentationDraft.where(representation_id: @client_application.representations.pluck(:id)).pluck(:representation_id))
  end

  def create_copy
    CopyClientApplicationWorker.perform_async(@client_application.id,
                                              client_application_params,
                                              current_user.username,
                                              view_context.announcements_url)
    redirect_to client_applications_path, notice: I18n.t('client_applications.copy.messages.started')
  end

  def update
    if @client_application.update(client_application_params)
      redirect_to @client_application, notice: I18n.t('client_applications.edit.messages.success')
    else
      render action: 'edit'
    end
  end

  def search
    ids = (params[:ids] || []).reject(&:blank?)

    matches = if ids.empty?
      ClientApplication.search(query: params[:q]).limit(search_limit)
    else
      ClientApplication.find(params[:ids])
    end

    key_method = params[:key_method].to_sym rescue :id
    render json: matches.map { |client_application| { id: client_application.send(key_method), text: client_application.name } }
  end

  def compile_assets
    CompileAssetsWorker.perform_async @client_application.id, current_user.username
    redirect_to @client_application, notice: I18n.t('client_applications.assets_compile.message')
  end

  def notify_clients
    @client_application.notify_clients
    redirect_to client_applications_path, notice: I18n.t('client_applications.notify.message')
  end

  def enable
    @client_application.update(enabled: true)
    redirect_to @client_application, notice: I18n.t('client_applications.enable.messages.success')
  end

  def disable
    @client_application.update(enabled: false)
    redirect_to @client_application, notice: I18n.t('client_applications.disable.messages.success')
  end

  def destroy
    redirect_to client_applications_url(page: current_page), notice: I18n.t('client_applications.destroy.messages.success')
    @client_application.destroy
  end

  def invalidate_routes
  end

  def purge_routes
    status = Purger.purge_urls @client_application, Array(params[:raw_route].presence)
    case status
    when :ok
      redirect_to client_applications_url, notice: I18n.t('client_application_routes.varnish.success')
    else
      redirect_to invalidate_routes_client_application_url, alert: I18n.t("client_application_routes.varnish.#{status}")
    end
  end

  def extra_attributes
  end

  def manage_extra_attributes
    restricted_params = params.require(:client_application).permit(extra_attribute_model_attributes: ExtraAttribute.for(ClientApplication).map(&:name)+[:id])
    if @client_application.update(restricted_params)
      redirect_to client_applications_path, notice: I18n.t('client_applications.edit.messages.success')
    else
      flash[:error] = I18n.t('client_applications.extra_attributes.error')
      render action: 'extra_attributes'
    end
  end

  private
    def set_client_application
      @client_application = ClientApplication.includes(:routes).find(params[:id])
    end

    def set_original_client_application
      @original_client_application = @client_application
      @client_application = nil
    end

    def client_application_params
      params.require(:client_application)
      .permit(:name, :url, :identifier, :news_source_id, :enabled, :assets_url, :media_url, :route_cache_control, :service_cache_control, :internal_varnish_urls, :external_varnish_urls, extra_attribute_model_attributes: ExtraAttribute.pluck(:name)+[:id])
    end

    def filter_params
      if params.has_key?(:filter)
        params.require(:filter).permit(:name, :url, :enabled, :query).to_options
      end
    end

    # Create the @filter object with the currently-set values for the filters
    def filter
      @filter = create_filter ClientApplicationsFilter, values: filter_params
    end

    # Clear the currently-set filters, restoring them to the defaults
    def clear_filter
      if params[:commit] == t('activerecord.actions.shared.clear_filter')
        @filter.clear!
        redirect_to request.path
      end
    end
end
