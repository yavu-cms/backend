class BaseRepresentationsViewsController < ApplicationController
  load_and_authorize_resource :client_application
  before_action :set_representation_draft
  before_action :set_view, only: [:disassociate, :show, :edit, :update, :destroy]

  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found

  def index
    @views = View.representation.alternative
  end

  def new
    @view = @representation.views.build
    @view.format = ""
    flash.now[:info] = I18n.t('representations_views.new.info_message')
  end

  def edit
    flash.now[:info] = I18n.t('representations_views.edit.info_message')
  end

  def create
    @view = @representation.views.build(view_params)
    @view.must_be_alternative = true
    if @view.save
      redirect_to client_application_representation_view_path(@client_application, @representation.representation, @view),
                  notice: I18n.t('representations_views.create.messages.success')
    else
      render 'new'
    end
  end

  def update
    @view.must_be_alternative = true
    if @view.update(view_params)
      redirect_to client_application_representation_view_path(@client_application, @representation.representation, @view),
                  notice: I18n.t('representations_views.update.messages.success')
    else
      render 'edit'
    end
  end

  def show
  end

  def associate
    view = View.representation.alternative.find(params[:id])
    if @representation.can_have_view? view
      @representation.views << view
      flash[:notice] = I18n.t('representations_views.associate.messages.success')
    else
      flash[:alert] = I18n.t('representations_views.associate.messages.format_in_use')
    end
    redirect_to client_application_representation_views_path(@client_application, @representation.representation)
  end

  def disassociate  
    if @representation.views.include?(@view)
      @representation.views.delete(@view)
      flash[:notice] = I18n.t('representations_views.disassociate.messages.success')
    else
      flash[:alert] = I18n.t('representations.errors.view_unavailable')
    end
    redirect_to client_application_representation_views_path(@client_application, @representation.representation)
  end

  def destroy
    if @view.destroy
      flash[:notice] = I18n.t('representations_views.destroy.messages.success')
    else
      flash[:alert] = I18n.t('representations_views.destroy.messages.error')
    end
    redirect_to client_application_representation_views_path(@client_application, @representation.representation)
  end

  private

  def record_not_found
    redirect_to client_applications_path, alert: I18n.t('representations.errors.invalid_client_application_or_representation')
  end

  def view_params
    params.require(:view).permit(:name, :value, :format)
  end

  def fetch_representation
    Representation.find_by!(client_application: @client_application, id: params[:representation_id])
  end

  def set_representation_draft
    @representation = fetch_representation.draft || raise(ActiveRecord::RecordNotFound)
  rescue ActiveRecord::RecordNotFound
    redirect_to client_application_representations_path(@client_application), alert: I18n.t('representations.errors.draft_unavailable')
  end

  def set_view
    @view = View.alternative.where(component_id: nil).find params[:id]
  end
end
