class ComponentTranslationsController < ApplicationController
  load_resource :component, class: BaseComponent
  load_and_authorize_resource through: :component, through_association: :translations

  def index
    @component_translations = @component.translations
  end

  def new
  end

  def create
    if @component_translation.save
      redirect_to component_translation_path(@component, @component_translation), notice: I18n.t('component/translations.new.messages.success')
    else
      render action: 'new'
    end
  end

  def edit
  end

  def update
    if @component_translation.update(component_translation_params)
      redirect_to component_translation_path(@component, @component_translation), notice: I18n.t('component/translations.update.messages.success')
    else
      render action: 'edit'
    end
  end

  def show
  end

  def destroy
    if @component_translation.destroy
      flash[:notice] = I18n.t('component/translations.destroy.messages.success')
    else
      flash[:alert] = I18n.t('component/translations.destroy.messages.alert')
    end
    redirect_to component_translations_url(@component)
  end

  private

  def component_translation_params
    params.require(:component_translation).permit(:locale, :values)
  end
end
