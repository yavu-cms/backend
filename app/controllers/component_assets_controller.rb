class ComponentAssetsController < ApplicationController
  load_resource :component, class: BaseComponent
  before_action :set_empty_asset, only: [:create_empty]
  load_and_authorize_resource through: :component, through_association: :assets, new: [:new_empty, :create_empty]
  before_action :check_editable?, only: [:edit_content, :update_content]

  def index
    @component_assets = @component.assets.page(params[:page]).per(per_page)
  end

  def new
  end

  def create
    if @component_asset.save
      redirect_to component_asset_path(@component, @component_asset), notice: I18n.t('component/assets.new.messages.success')
    else
      render action: 'new'
    end
  end

  def edit
  end

  def edit_content
  end

  def show
  end

  def rename
  end

  def new_empty
  end

  def create_empty
    # Creates a new empty asset for this component allowing to write it in the future.
    @component_asset.file = File.open("#{Dir.tmpdir}/#{params[:component_asset][:file]}.#{params[:component_asset][:type]}", 'w')
    if @component_asset.save
      redirect_to component_asset_path(@component, @component_asset), notice: I18n.t('component/assets.new_empty.success')
    else
      render action: 'new_empty'
    end
  end

  def update
    if @component_asset.update(component_asset_params)
      redirect_to component_asset_path(@component, @component_asset), notice: I18n.t('component/assets.update.messages.success')
    else
      render action: 'edit'
    end
  end

  def update_content
    # This action wont fail
    @component_asset.update_content! component_assets_content_params[:content]
    redirect_to component_asset_path(@component, @component_asset),
                  notice: I18n.t('component/assets.update.messages.success')
  end

  def update_name
    if @component_asset.rename(params[:component_asset][:file])
      redirect_to component_asset_path(@component, @component_asset)
    else
      render action: 'rename'
    end
  end

  def destroy
    @component_asset.destroy
    redirect_to component_assets_url(@component),
                  notice: I18n.t('component/assets.destroy.messages.success')
  end

  private
  def set_empty_asset
    @component_asset = @component.assets.build
  end

  def component_asset_params
    params[:component_asset].permit(:file) if params[:component_asset]
  end

  def component_assets_content_params
    params[:component_asset].permit(:content)
  end

  def check_editable?
    unless @component_asset.editable?
      redirect_to component_asset_path(@component, @component_asset), alert: I18n.t('component/assets.edit_content.messages.error')
    end
  end
end
