class SettingsController < ApplicationController
  load_and_authorize_resource :setting
  before_action :set_settings, except: :update_all

  def index
  end

  def edit_all
  end

  def update_all
    Setting.update_all(settings_params)
    redirect_to :settings, notice: I18n.t('settings.update_all.messages.success')
  rescue ActiveRecord::RecordInvalid
    redirect_to edit_all_settings_path, alert: I18n.t('settings.update_all.messages.error')
  end

  private
    def permit_params
      Setting.unscoped.map { |s| s.var }
    end

    def settings_params
      params.require(:settings).permit(permit_params)
    end

    def set_settings
      @settings = Setting.unscoped
    end
end
