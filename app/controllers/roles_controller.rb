class RolesController < ApplicationController
  load_and_authorize_resource

  before_action :order, only: [:index]
  before_action :filter, only: [:index]
  before_action :clear_filter, only: [:index]

  def index
    @roles = @order.sort_list @filter.all.page(params[:page]).per(per_page)
  end

  def show
  end

  def new
  end

  def edit
  end

  def create
    if @role.save
      redirect_to @role, notice: t('roles.new.messages.success')
    else
      render action: 'new'
    end
  end

  def update
    if @role.update(role_params)
      redirect_to @role, notice: t('roles.edit.messages.success')
    else
      render action: 'edit'
    end
  end

  def destroy
    if @role.destroy
      flash[:notice] = I18n.t('roles.destroy.messages.success')
    else
      flash[:alert] = I18n.t('roles.destroy.messages.error')
    end
    redirect_to roles_path(page: current_page)
  end

  private

  def role_params
    params.require(:role).permit(:name, :enabled, permissions: [])
  end

  def filter_params
    if params.has_key?(:filter)
      params.require(:filter).permit(:name).to_options
    end
  end

  def filter
    @filter = create_filter RolesFilter, values: filter_params
  end

  def order
    @order = create_order RolesOrder, sort: sort_params
  end

  def clear_filter
    if params[:commit] == t('activerecord.actions.shared.clear_filter')
      @filter.clear!

      redirect_to request.path
    end
  end
end
