class RoutesController < ApplicationController
  load_and_authorize_resource :client_application
  load_and_authorize_resource :route, through: :client_application, except: :create
  before_action :set_new_route, only: :create

  before_action :order, only: [:index]
  before_action :set_client_application_routes, only: [:index, :new, :create, :edit, :update]

  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found
  respond_to :html

  def index
    @routes = @order.sort_list @routes.reorder(nil).page(params[:page]).per(per_page)
  end

  def show; end

  def new; end

  def create
    if @route.save
      redirect_to client_application_routes_path, notice: I18n.t('client_application_routes.new.messages.success')
    else
      render action: 'new'
    end
  end

  def edit; end

  def update
    if @route.update(route_params)
      redirect_to client_application_routes_path, notice: I18n.t('client_application_routes.edit.messages.success')
    else
      render action: 'edit'
    end
  end

  def destroy
    if @route.destroy
      flash[:notice] = I18n.t('client_application_routes.destroy.messages.success')
    else
      flash[:alert] = I18n.t('client_application_routes.destroy.messages.error')
    end
    redirect_to client_application_routes_path
  end

  private

  def set_new_route
    @route = Route.new client_application: @client_application, type: route_params[:type]
    @route.attributes = route_params
  end

  def route_params
    params.require(:route).permit(:id, :name, :pattern, :type, :default_route, :representation_id, :redirect_url,
                                  :constrained_article_id, :constrained_section, :cache_control,
                                  :constrained, :priority)
  end

  def set_client_application_routes
    @routes = @client_application.routes
  end

  def order
    @order = create_order RoutesOrder, { sort: sort_params }
  end
end
