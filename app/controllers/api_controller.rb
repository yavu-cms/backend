class ApiController < ActionController::Base
  before_action :ensure_client_application, except: [:service_preview, :alive]
  before_action :ensure_client_application_for_preview, only: [:service_preview]
  before_action :ensure_service_preview_context, only: [:service_preview]
  before_action :ensure_route_context, only: [:context]
  before_action :ensure_service_context, only: [:service]

  skip_before_action :authenticate_user!
  skip_before_action :verify_authenticity_token # Frontend will validate CSRF

  rescue_from ActiveRecord::RecordNotFound, with: :error_404

  EXPIRATION_TIME = YavuSettings.cache.api.expiration_time

  def show
    render json: @client_application if stale?(@client_application, public: true)
  end

  def register
    render json: @client_application.as_api_resource.to_json if stale?(@client_application, public: true)
  end

  def context
    invoke do
      context = @route.build_context(MultiJson.load(params['params'] || '{}'), context_headers)
      #FIXME: ETag and Last-modified works wrong!
      context.etag = nil
      context.last_modified = nil
      expires_in EXPIRATION_TIME.seconds, public: true
      return render json: context.to_json
    end
  end

  def service
    invoke do
      context = @service.build_context(params, context_headers)
      #FIXME: ETag and Last-modified works wrong!
      context.etag = nil
      context.last_modified = nil
      expires_in EXPIRATION_TIME.seconds, public: @service.cache_public?
      return render json: context.to_json
    end
  end

  def service_preview
    invoke do
      renderer = @client_application.preview_renderer(@route, server: @client_application.preview_server(request), locale: 'es', logger: Rails.logger)
      context = @service.build_context(params, context_headers(session.to_hash))
      context = ::Yavu::API::Resource::Base.from_json(context.to_json)
      return render text: renderer.render(partial: @service.template, locals: context._locals)
    end
  end

  def accounting
    status = process_accounting_information
    render json: { accounting: { status: status } }
  end

  def alive
    frontend_identifier, client_identifier = params[:frontend_identifier].presence, params[:client_identifier].presence
    FrontendServer.mark_alive frontend_identifier, client_identifier
    head 200
  end

  private

  def process_accounting_information
    routes_data = MultiJson.load(params['bag'])
    routes_data.map do |raw_article_info, visits|
      route_pattern, route_accounting_info = MultiJson.load raw_article_info
      @client_application.routes.find_by(pattern: route_pattern).try(:accounting, route_accounting_info, visits.to_i)
    end
  end

  #Implements halt functionality for context processing
  def invoke
    ret = catch(:halt_processing_context) { yield }
    return head ret   if ret.is_a?(Fixnum)
    return head(*ret) if ret.is_a?(Array)
    ret
  end

  def context_headers(from = nil)
    h = from || request.headers
    # Forces User encoding to UTF8 because it can have strange characters
    # and session marshalling dumping/loading dont resolve it
    h['HTTP_X_YAVU_USER'] = h['HTTP_X_YAVU_USER'].try :force_encoding, 'UTF-8'
    selected_headers = Hash[h.select {|k,_| k.match(/^HTTP_X_YAVU/) }.map {|k,v| [k.gsub(/^HTTP_X_YAVU_/,'').gsub('_','-'),v] }]
    logger.debug('ApiController') { "API request headers filtered to headers=#{selected_headers}" }
    Rack::Utils::HeaderHash.new(selected_headers)
  end

  def ensure_client_application
    @client_application = ClientApplication.enabled.find_by_identifier params[:id]
    head :forbidden and return unless @client_application
  end

  def ensure_client_application_for_preview
    @client_application = ClientApplication.enabled.find_by_identifier(params[:id]).try(:for_preview)
    head :forbidden and return unless @client_application
  end

  def ensure_service_context
    @component_configuration = ComponentConfiguration.find(params[:component_configuration_id])
    @service = @component_configuration.configured_services[params[:service_name]] if @component_configuration
    @route = @client_application.routes.detect { |x| x.services.include? @service } if @service
    head :forbidden and return unless @service && @route
  end

  def ensure_service_preview_context
    @component_configuration = ComponentConfiguration.find(params[:component_configuration_id])
    @service = @component_configuration.configured_services[params[:service_name]] if @component_configuration
    @route = @client_application.routes.find(params[:route])
    @representation = BaseRepresentation.find_by(id: params[:representation]) || @route.representation  # Use specified representation(for draft rendering) or use route representation
    if @representation
      #if rendering a draft then we must reload objects with preview values
      @client_application = @client_application.for_preview(@representation)
      @route = @client_application.routes.find(params[:route])
    end
    head :forbidden and return unless @service && @route && @representation
  end

  def ensure_route_context
    @route = @client_application.routes.find_by_pattern params[:route]
    head :forbidden and return unless @route
  end

  # Return a 404 status instead of showing an ActiveRecord exception,
  # even in :development environment -- frontend application needs to
  # know it was an actual 404 instead of receiving an erroneous 500.
  def error_404
    head 404
  end
end
