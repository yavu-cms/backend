class DashboardController < ApplicationController

  def index
    @article_groups = Array.new.tap do |article_groups|
      %w[ most_commented most_visited last_year migrated_today migrated_yesterday news_today news_yesterday last_week ten_minutes ].each do |type|
        article_groups << article_group_data(type)
      end
    end
  end

  def jobs
    render layout: 'preview'
  end

  def most_box
    articles_data = article_group_data(params[:articles_range])
    render partial: 'dashboard/info', locals: { resources: articles_data[:articles], accounting_method: params[:accounting_method].to_sym, accounting_params: params[:accounting_params] }
  end

  private

  def limit
    15
  end

  def article_group_data(articles_range)
    Rails.cache.fetch(article_group_cache_key(articles_range), expires_in: 2.minutes) do
      {
        update_params: {
          articles_range: articles_range,
          accounting_method: groups_data[articles_range]['accounting_method'] || :visits,
          accounting_params: groups_data[articles_range]['accounting_params'] || []
        }.to_json,
        title: t("accounting.#{articles_range}"),
        range: groups_data[articles_range]['range'],
        articles: "Accounting::#{groups_data[articles_range]['accounting_class'] || 'MostVisitedArticles'}".constantize.send(:last_period, groups_data[articles_range]['range'], limit, migrated: groups_data[articles_range]['migrated']),
        accounting_method: groups_data[articles_range]['accounting_method'] || :visits,
        accounting_params: groups_data[articles_range]['accounting_params'] || []
      }
    end
  end

  def groups_data
    current = DateTime.current
    @groups_data ||= {
      'last_year'          => {'range' => 1.year.ago..current, 'migrated' => nil},
      'last_week'          => {'range' => 1.week.ago..current, 'migrated' => nil},
      'ten_minutes'        => {'range' => 10.minutes.ago..current, 'migrated' => nil},
      'migrated_today'     => {'range' => DateTime.current.beginning_of_day..DateTime.current.end_of_day, 'migrated' => true},
      'migrated_yesterday' => {'range' => 2.days.ago..1.day.ago, 'migrated' => true},
      'most_commented'     => {'range' => DateTime.current.beginning_of_day..DateTime.current.end_of_day, 'migrated' => nil,
                               'accounting_class' => 'MostCommentedArticles', 'accounting_method' => :comments_quantity,
                               'accounting_params' => [{ timerange: DateTime.current.beginning_of_day..DateTime.current.end_of_day }]},
      'most_visited'       => {'range' => DateTime.current.beginning_of_day..DateTime.current.end_of_day, 'migrated' => nil},
      'news_today'         => {'range' => DateTime.current.beginning_of_day..DateTime.current.end_of_day, 'migrated' => false},
      'news_yesterday'     => {'range' => 2.days.ago..1.day.ago, 'migrated' => false},
    }
  end

  def article_group_cache_key(articles_range)
    "#{articles_range}_cache"
  end

end
