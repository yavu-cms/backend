class BackupsController < ApplicationController
  load_and_authorize_resource :client_application
  before_action :set_representation
  before_action :set_backup,                only: [:show, :restore, :destroy]

  def index
    @backups = @representation.backups.page(params[:page]).per(per_page)
  end

  def show
  end

  def restore
    if @backup.restore!
      flash[:notice] = I18n.t('representations/backups.restore.messages.success')
      redirect_to client_application_representation_path(@client_application, @representation)
    else
      flash[:alert] = I18n.t('representations/backups.restore.messages.fail')
      redirect_to client_application_representation_backups_path(@client_application, @representation)
    end
  end

  def destroy
    if @backup.destroy
      flash[:notice] = I18n.t('representations/backups.actions.destroy.messages.success')
    else
      flash[:alert] = I18n.t('representations/backups.actions.destroy.messages.fail')
    end

    redirect_to client_application_representation_backups_url(@client_application, @representation)
  end

  private 

  def set_representation
    @representation = @client_application.representations.find(params[:representation_id])
  end

  def set_backup
    @backup = @representation.backups.find(params[:id])
  end
end
