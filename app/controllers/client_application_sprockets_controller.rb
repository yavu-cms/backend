class ClientApplicationSprocketsController
  def self.call(env)
    env['PATH_INFO'] = params(env)[:path]
    configuration = assets_configuration client_application(env)
    environment = Yavu::Frontend::Util::AssetConfigurationFactory.sprockets_environment(configuration)
    Yavu::Frontend::Util::AssetConfigurationFactory.configure_sprockets_helpers configuration, environment
    environment.call(env)
  end

  protected

  def self.client_application(env)
    ClientApplication.find_by_identifier! params(env)[:identifier]
  end

  def self.assets_configuration(client)
    client.assets_configuration
  end

  def self.params(env)
    env['action_dispatch.request.path_parameters']
  end
end
