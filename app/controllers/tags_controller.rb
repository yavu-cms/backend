class TagsController < ApplicationController
  load_and_authorize_resource

  before_action :order, only: [:index]
  before_action :filter, only: [:index]
  before_action :clear_filter, only: [:index]

  def index
    @in_scope = params['hidden'].present? ? :hidden : :visible
    @tags = @order.sort_list @filter.all(@in_scope).page(params[:page]).per(per_page)
  end

  def show
  end

  def new
  end

  def edit
  end

  def combine
  end

  def combine_update
    tags = Tag.where(name: serialized_combined_param)
    @tag.combines! tags
    redirect_to tag_path(@tag), notice: t('tags.combine.messages.success')
  end

  def separate
    @tag = Tag.find_by!(id: params[:id])
    @tag.separate!
    render nothing: true
  end

  def create
    if @tag.save
      redirect_to @tag, notice: t('tags.new.messages.success')
    else
      render action: 'new'
    end
  end

  def search
    ids = (params[:ids] || []).reject(&:blank?)

    if ids.empty?
      matches = Tag.search({ query: params[:q], except: params[:except]}).limit(search_limit)
    else
      matches = Tag.where(name: ids)
    end

    key_method = params[:key_method].presence || :name
    result = matches.map do |tag|
      {id: tag.send(key_method), text: tag.to_s}
    end

    render json: result
  end

  def update
    if @tag.update(tag_params)
      redirect_to @tag, notice: t('tags.edit.messages.success')
    else
      render action: 'edit'
    end
  end

  def destroy
    if @tag.destroy
      flash[:notice] = I18n.t('tags.destroy.messages.success')
    else
      flash[:alert] = I18n.t('tags.destroy.messages.error')
    end
    redirect_to tags_path(page: current_page)
  end

  def unblacklist
    status = (@tag.update(hidden: false) ? 'notice' : 'error')
    flash[status.to_sym] = I18n.t("tags.unblacklist.message.#{status}", tag: @tag)
    redirect_to "#{tags_path(hidden: true, page: params[:page])}#tag_#{params[:nt]}"
  end

  def blacklist
    status = (@tag.update(hidden: true) ? 'notice' : 'error')
    flash[status.to_sym] = I18n.t("tags.blacklist.message.#{status}", tag: @tag)
    redirect_to "#{tags_path(page: params[:page])}#tag_#{params[:nt]}"
  end

  private

  def tag_params
    params.require(:tag).permit(:name, :slug, :combined, :serialized_combined, :hidden)
  end

  def filter_params
    if params.has_key?(:filter)
      params.require(:filter).permit(:name).to_options
    end
  end

  def filter
    @filter = create_filter TagsFilter, values: filter_params
  end

  def order
    @order = create_order TagsOrder, {sort: sort_params}
  end

  def clear_filter
    if params[:commit] == t('activerecord.actions.shared.clear_filter')
      @filter.clear!

      redirect_to request.path
    end
  end

  def serialized_combined_param
    tag_params[:serialized_combined].split(',').reject(&:blank?)
  end
end
