class ArticlesController < ApplicationController
  before_action :order,        only: [:index]
  before_action :filter,       only: [:index]
  before_action :clear_filter, only: [:index]
  before_action :set_articles, only: [:index]
  load_and_authorize_resource new: :create_copy

  before_action :set_defaults, only: [:new]

  after_action :invalidate_routes, only: [:update]

  respond_to :html

  def index
  end

  def new
  end

  def edit
    @embedded_medium = EmbeddedMedium.new
    render layout: !request.xhr?
  end

  def show
  end

  def copy
    @article = @article.copy
    flash.now[:info] = I18n.t('articles.copy.notice')
  end

  def create_copy
    @article = @article.copy article_params

    if @article.save
      redirect_to @article, notice: I18n.t('articles.new.messages.success')
    else
      render action: 'copy'
    end
  end

  def create
    if @article.save
      redirect_to @article, notice: I18n.t('articles.new.messages.success')
    else
      render action: 'new', layout: !request.xhr?
    end
  end

  def update
    if @article.update_with_extra_attributes(article_params)
      UniqueAccessWorker.perform_async(view_context.channel_path(@article, namespace: '/uniqueaccess'), {user: current_user.username, saved: true}, view_context.announcements_url)
      redirect_to @article, notice: I18n.t('articles.edit.messages.success')
    else
      @embedded_medium = EmbeddedMedium.new
      render action: 'edit', layout: !request.xhr?
    end
  end

  def destroy
    if @article.destroy
      UniqueAccessWorker.perform_async(view_context.channel_path(@article, namespace: '/uniqueaccess'), {user: current_user.username, destroyed: true}, view_context.announcements_url)
      flash[:notice] = I18n.t('articles.destroy.messages.success')
    else
      flash[:alert]  = I18n.t('articles.destroy.messages.has_covers_associated')
    end
    redirect_to articles_path(page: current_page)
  end

  def media_by_article
    redirect_to media_path({ filter: { article_id: @article.id } })
  end

  # Search for articles using params hash, which may contain:
  #   * query [String] string to be searched
  #   * id [Integer] element to exclude from the result set obtenined with query search
  #   * ids [Array] array that contains ids of articles to search.
  #       If this parameter is set are ignored both "id" and "query".
  def search
    ids = (params[:ids] || []).reject &:blank?
    key_method = params[:key_method].to_sym rescue :id
    matches = if ids.empty?
                Article.ordered.search(query: params[:q], except: params[:id].presence).includes(:section).limit(search_limit)
              else
                meth = params[:order].present? ? :find_ordered : :find
                Article.eager_load(:section).public_send meth, ids
              end
    render json: matches.map { |article| Hash[id: article.send(key_method), text: render_to_string(article)] }
  end

  def search_by_section_and_edition
    @articles = Article.all_them.visible.where(section_id: current_user.accessible_sections)
    @articles = @articles.with_section(params[:section_id].presence).for_cover if params[:section_id].present?
    @articles = @articles.with_edition_date(params[:edition_date])             if params[:edition_date].present?
    @articles.limit!(params[:limit].presence || search_limit)

    # Use an alternate view if the request comes with the parameter f=r
    # @see the cover editor and its JS plugin
    @partial = params[:f] == 'r' ? 'representations/cover_editor/article' : 'articles/article_item'
    render partial: 'available_articles_results'
  end

  private
    def article_params
      params.require(:article)
        .permit(:slug, :heading, :title, :lead, :body, :signature,
                :time, :section_id, :initial_likes, :initial_dislikes,
                :initial_visits, :serialized_tags, :serialized_related,
                :edition_id, :is_visible, :hide_from_cover,
                extra_attribute_model_attributes: ExtraAttribute.for(Article).map(&:name)+[:id],
                article_media_attributes: [:id, :caption, :order, :medium_id, :main, :only_cover, :for_gallery, :_destroy, :alt, :title])
    end

    def set_defaults
      supplement = NewsSource.default_supplement
      if supplement
        @article.section = Section.with_supplement.find_by(supplement: supplement)
        @article.edition = supplement.active_edition
      end
    end

    def filter_params
      if params.has_key?(:filter)
        params.require(:filter)
          .permit(:body, :containing, :edition_date_from, :edition_date_to,
                  :created_time_from, :created_time_to,
                  :last_updated_from, :last_updated_to,
                  :section_id, :edition_id, :tag).to_options
      end
    end

    # Create the @filter object with the currently-set values for the filters
    def filter
      @filter = create_filter ArticlesFilter, {values: filter_params}
    end

    def order
      @order = create_order ArticlesOrder, {sort: sort_params}
    end

    # Clear the currently-set filters, restoring them to the defaults
    def clear_filter
      if params[:commit] == t('activerecord.actions.shared.clear_filter')
        @filter.clear!
        redirect_to request.path
      end
    end

    def set_articles
      @articles = @order.sort_list @filter.all.page(params[:page]).per(per_page)
    end

    def invalidate_routes
      Purger.purge_article @article
    end
end
