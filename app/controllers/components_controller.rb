class ComponentsController < ApplicationController
  load_and_authorize_resource class: BaseComponent
  skip_authorize_resource only: :configuration_form
  before_action :filter,       only: [:index]
  before_action :clear_filter, only: [:index]
  before_action :order,        only: [:index]
  before_action :copy_original_component, only: [:copy, :create_copy]

  def index
    @components = @order.sort_list @filter.all.page(params[:page]).per(per_page)
  end

  def new
  end

  def create
    build_component # sets @component
    if @component.save
      redirect_to component_path(@component), notice: t('components.new.messages.success')
    else
      render action: 'new'
    end
  end

  def show
  end

  def edit
    @form_url = component_path(@component)
  end

  def update
    if @component.update(component_params)
      redirect_to edit_component_path(@component), notice: I18n.t('components.edit.messages.success')
    else
      render action: 'edit'
    end
  end

  def enable
    @component.update(enabled: true)
    redirect_to components_path, notice: I18n.t('components.enable.messages.success', component_name: @component.name)
  end

  def disable
    @component.update(enabled: false)
    redirect_to components_path, notice: I18n.t('components.disable.messages.success', component_name: @component.name)
  end

  def destroy
    if @component.destroy
      flash[:notice] = I18n.t('components.destroy.messages.success')
    else
      flash[:alert] = I18n.t('components.destroy.messages.fail')
    end
    redirect_to components_path
  end

  def copy
    flash.now[:info] = I18n.t('components.copy.info')
  end

  def create_copy
    if @component.update(name: component_params[:name])
      redirect_to component_path(@component), notice: I18n.t('components.copy.messages.success')
    else
      render action: 'copy'
    end
  end

  def configuration_form
    @component_configuration = @component.new_configuration
    render partial: 'components/configuration/configuration_form',
           layout: false,
           locals: { values: @component_configuration.available_attributes,
                     component_configuration: @component_configuration }
  end

  # Used by client application assets form to fetch prefix_path of component slugs
  def search
    render json: BaseComponent.search(params[:q]).limit(search_limit)
  end

  private

  def copy_original_component
    @original_component = @component
    @component = @original_component.copy
  end

  def component_params
    params.require(:component)
      .permit(:name, :description, :readme, :context, :extras, :type, :enabled,
              fields_attributes: [:id, :name, :description, :type, :model_class, :value, :_destroy])
  end

  def filter_params
    if params.has_key?(:filter)
      params.require(:filter).permit(:name).to_options
    end
  end

  def order
    @order = create_order ComponentsOrder, sort: sort_params
  end

  # Create the @filter object with the currently-set values for the filters
  def filter
    @filter = create_filter ComponentsFilter, {values: filter_params}
  end

  # Clear the currently-set filters, restoring them to the defaults
  def clear_filter
    if params[:commit] == t('activerecord.actions.shared.clear_filter')
      @filter.clear!
      redirect_to request.path
    end
  end

  # Depending on component type we build an instance of a different BaseComponent subclass
  def build_component
    klass = component_params.delete(:type).try(:safe_constantize) || Component
    @component = klass.new(component_params)
    @component.views << View.new(name: I18n.t('components.default_view_name', default: 'default')) unless @component.cover?
  end
end
