class GalleriesController < ApplicationController
  load_and_authorize_resource new: [:new_media]
  before_action :order, only: [:index]
  before_action :filter, only: [:index]
  before_action :clear_filter, only: [:index]

  def index
    @galleries = @order.sort_list @filter.all.page(params[:page]).per(per_page)
  end

  def show
  end

  def new
    redirect_to galleries_path, alert: I18n.t('galleries.new.messages.missing_type')
  end

  def new_media
    @gallery = MediaGallery.new
    render action: 'new'
  end

  def edit
  end

  def create
    if @gallery.save
      redirect_to gallery_path(@gallery), notice: I18n.t('galleries.new.messages.success')
    else
      render action: 'new'
    end
  end

  def update
    if @gallery.update(gallery_params)
      redirect_to gallery_path(@gallery), notice: I18n.t('galleries.edit.messages.success')
    else
      render action: 'edit'
    end
  end

  def destroy
    if @gallery.destroy
       flash[:notice] = I18n.t('galleries.destroy.messages.success')
    else
       flash[:alert] = I18n.t('galleries.destroy.messages.error')
    end
    redirect_to galleries_path(page: current_page)
  end

  def search
    matches = if params[:id]
      [Gallery.find(params[:id])]
    else
      Gallery.search(query: params[:q]).limit(search_limit)
    end
    render json: matches.map { |gallery| Hash[id: gallery.id, text: gallery.title] }
  end

  private
    def gallery_params
      params.require(:gallery).permit(:title, :type)
    end

    def order
      @order = create_order GalleriesOrder, {sort: sort_params}
    end

    def clear_filter
      if params[:commit] == t('activerecord.actions.shared.clear_filter')
        @filter.clear!

        redirect_to request.path
      end
    end

    def filter_params
      if params.has_key?(:filter)
        params.require(:filter).permit(:title, :type).to_options
      end
    end

    def filter
      @filter = create_filter GalleriesFilter, values: filter_params
    end
end
