class ComponentServicesController < ApplicationController
  load_resource :component, class: BaseComponent
  load_and_authorize_resource through: :component, through_association: :services, instance_name: :service

  def index
    @services = @component.services
  end

  def new
  end

  def create
    if @service.save
      redirect_to component_service_path(@component, @service), notice: I18n.t('component/services.new.messages.success')
    else
      render action: 'new'
    end
  end

  def edit
  end

  def update
    if @service.update(component_service_params)
      redirect_to component_service_path(@component, @service), notice: I18n.t('component/services.update.messages.success')
    else
      render action: 'edit'
    end
  end

  def show
  end

  def destroy
    if @service.destroy
      flash[:notice] = I18n.t('component/services.destroy.messages.success')
    else
      flash[:alert] = I18n.t('component/services.destroy.messages.fail')
    end
    redirect_to component_services_url(@component)
  end

  private

  def component_service_params
    params.require(:component_service).permit(:http_method, :name, :cache_control, :body, :available_parameters_string, :view_id )
  end
end
