class NewslettersController < ApplicationController
  load_and_authorize_resource :client_application
  before_action :set_newsletter, only: [:show, :edit, :update, :enable, :disable, :destroy, :sendmail]

  def index
    @newsletters = @client_application.newsletters.page(params[:page]).per(per_page)
  end

  def show
  end

  def new
    @newsletter = Newsletter.new
  end

  def edit
  end

  def create
    @newsletter = @client_application.newsletters.create(newsletter_params)
    if @newsletter.valid?
      redirect_to [ @client_application, @newsletter ], notice: I18n.t('newsletters.new.messages.success')
    else
      render action: 'new', layout: !request.xhr?
    end
  end

  def update
    if @newsletter.update(newsletter_params)
      redirect_to [ @client_application, @newsletter ], notice: I18n.t('newsletters.edit.messages.success')
    else
      render action: 'edit'
    end
  end

  def destroy
    @newsletter.destroy
    redirect_to client_application_newsletters_path(@client_application), notice: I18n.t('newsletters.destroy.messages.success')
  end

  def enable
    @newsletter.update(enabled: true)
    redirect_to client_application_newsletters_path(@client_application), notice: I18n.t('newsletters.enable.messages.success')
  end

  def disable
    @newsletter.update(enabled: false)
    redirect_to client_application_newsletters_path(@client_application), notice: I18n.t('newsletters.disable.messages.success')
  end

  def sendmail
    NewsletterSenderWorker.perform_async(@newsletter.name)
    redirect_to client_application_newsletters_path(@client_application), notice: I18n.t('newsletters.sendmail.messages.success')
  end

  private

  def newsletter_params
    params.require(:newsletter)
      .permit(:name, :subject, :frequency, :expiration, :generator, :context, :enabled)
  end

  def set_newsletter
    @newsletter = @client_application.newsletters.find(params[:id])
  end
end
