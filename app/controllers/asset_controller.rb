class AssetController < ApplicationController
  def serve_component_content
    serve ComponentAsset.find(params[:id])
  end

  def serve_client_application_asset_content
    serve ClientApplicationAsset.find(params[:id])
  end

  private
    def serve(asset)
      content = IO.read(asset.file.current_path) rescue ''
      send_data content, filename: asset.file_identifier
    end

end
