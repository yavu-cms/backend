class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :authenticate_user!
  before_action :save_current_page, only: :index

  rescue_from CanCan::AccessDenied, with: :deny_access unless Rails.env.test?

  protected
    # Return the limit of records to be returned in search actions
    def search_limit
      Setting['search.limit'] || 20
    end

    # redirect to menu if a user has not permission to access to certain place
    def deny_access(exception)
      redirect_to main_app.root_url, alert: I18n.t('session.unauthorized')
    end

  private
    # Create a :filter_class instance with some non-mandatory options (which
    # will be merged with some defaults).
    def create_filter(filter_class, opts = {})
      options = { user: @current_user, store: session, persist: true }.merge opts
      filter_class.new options
    end

    # Create a :order_class instance with some non-mandatory options (which
    # will be merged with some defaults).
    def create_order(order_class, opts = {})
      options = { store: session, persist: true }.merge opts
      order_class.new options
    end

    # Check if the parameters to sort the list are available.
    def sort_params
      if params.has_key?(:sort)
        params.require(:sort).permit(:field, :order)
      else
        { field: nil, order: nil }
      end
    end

    # helper method to get the page size configuration value
    def per_page
      params_per_page = params[:per_page].to_i if params[:per_page].to_i > 0
      @per_page = session[:per_page] = params_per_page || session[:per_page] || search_per_page
    end

    def search_per_page
      Setting['pagination.per_page'] || 10
    end

    def save_current_page
      session[self.class.to_s] ||= {}
      session[self.class.to_s][:current_page] = params[:page] || 1
    end

    def current_page
      page = 1
      if session[self.class.to_s] && session[self.class.to_s][:current_page]
        page = session[self.class.to_s][:current_page]
      end
      page
    end

    def not_found
      raise ActionController::RoutingError.new('Not Found')
    end

    def record_not_found
      # TODO: Improve this to use a custom view - https://proyectos.cespi.unlp.edu.ar/issues/8471
      render text: 'Not found', status: 404
    end

    def json_request?
      request.format.json?
    end
end
