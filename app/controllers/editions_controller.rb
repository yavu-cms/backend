class EditionsController < ApplicationController
  load_and_authorize_resource
  before_action :order, only: [:index]
  before_action :filter, only: [:index]
  before_action :clear_filter, only: [:index]

  respond_to :html

  def index
    @editions = @order.sort_list @filter.all.page(params[:page]).per(per_page)
  end

  def show
  end

  def new
  end

  def edit
  end

  def create
    if @edition.save
      redirect_to @edition, notice: t('editions.new.messages.success')
    else
      render action: 'new'
    end
  end

  def update
    if @edition.update(edition_params)
      redirect_to @edition, notice: t('editions.edit.messages.success')
    else
      render action: 'edit'
    end
  end

  def become_active
    @edition.become_active!
    redirect_to editions_path, notice: t('editions.become_active.messages.success')
  end

  def destroy
    if @edition.destroy
      flash[:notice] = t('editions.destroy.messages.success')
    else
      flash[:alert] = t('editions.destroy.messages.error')
    end
    redirect_to editions_path(page: current_page)
  end

  def search
    ids = (params[:ids] || []).reject &:blank?
    key_method = params[:key_method].to_sym rescue :id
    matches = if ids.empty?
                Edition.search(query: params[:q]).limit(search_limit)
              else
                Edition.find(params[:ids])
              end
    render json: matches.map { |edition| Hash[id: edition.send(key_method), text: edition.to_s] }
  end

  def articles
    redirect_to articles_path(filter: { edition_id: @edition.id })
  end

  private
    def edition_params
      params.require(:edition).permit(:name, :date_string, :is_visible, :supplement_id,
                                      extra_attribute_model_attributes: ExtraAttribute.for(Edition).map(&:name) + [:id])
    end

    def filter_params
      if params.has_key?(:filter)
        params.require(:filter).permit(:name, :date, :supplement_id).to_options
      end
    end

    def filter
      @filter = create_filter EditionsFilter, values: filter_params
    end

    def order
      @order = create_order EditionsOrder, {sort: sort_params}
    end

    def clear_filter
      if params[:commit] == t('activerecord.actions.shared.clear_filter')
        @filter.clear!
        redirect_to request.path
      end
    end
end
