class ExtraAttributesController < ApplicationController
  load_and_authorize_resource :extra_attribute

  # GET /extra_attributes
  def index
    @extra_attributes = ExtraAttribute.all.page(params[:page]).per(per_page)
  end

  # GET /extra_attributes/new
  def new
    @extra_attribute = ExtraAttribute.new
  end

  # GET /extra_attributes/1/edit
  def edit
  end

  # POST /extra_attributes
  def create
    @extra_attribute = ExtraAttribute.new(extra_attribute_params)

    if @extra_attribute.save
      redirect_to extra_attributes_url, notice: I18n.t('extra_attributes.new.messages.success')
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /extra_attributes/1
  def update
    if @extra_attribute.update(extra_attribute_params)
      redirect_to extra_attributes_url, notice: I18n.t('extra_attributes.edit.messages.success')
    else
      render action: 'edit'
    end
  end

  # DELETE /extra_attributes/1
  def destroy
    @extra_attribute.destroy
    redirect_to extra_attributes_url, notice: I18n.t('extra_attributes.destroy.messages.success')
  end

  private
    # Only allow a trusted parameter "white list" through.
    def extra_attribute_params
      params.require(:extra_attribute).permit(:name,:description,:attribute_type,model_classes: ExtraAttribute.available_model_classes)
    end
end
