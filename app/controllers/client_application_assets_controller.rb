class ClientApplicationAssetsController < ApplicationController
  load_and_authorize_resource :client_application
  load_resource :asset, :parent => false, :through => :client_application, new: [:new_empty, :create_empty]

  # The following two filters are needed to implement go_back path
  before_action :set_paths_to_client_application_assets_controller, only: [:new_empty, :show, :edit, :new, :rename, :edit_content]
  before_action :set_paths_from_session
  before_action :check_editable?, only: [:edit_content, :update_content]

  def index
    @application_assets = @client_application.assets.page(params[:page]).per(per_page)
  end

  def new
  end

  def new_empty
  end

  def create
    # After creation we MUST redirect to list because of go back shared implementation
    if @asset.save
      redirect_to @list_path, notice: t('client_application_assets.new.messages.success')
    else
      render action: "new"
    end
  end

  def create_empty
    # Creates a new empty asset for this client application allowing to write it
    # in the future.
    @asset.prefix_path = params[:client_application_asset][:prefix_path] if params[:client_application_asset][:prefix_path]
    @asset.file = File.open("#{Dir.tmpdir}/#{params[:client_application_asset][:file]}.#{params[:client_application_asset][:type]}", "w")
    @asset.client_application = @client_application
    if @asset.save
      redirect_to @list_path, notice: t('client_application_assets.new.messages.success')
    else
      render action: 'new_empty'
    end
  end

  def edit; end
  def edit_content; end
  def rename; end

  def show
    @container = @client_application
  end

  def update
    if @asset.update(asset_params)
      redirect_to @show_path, notice: I18n.t('client_application_assets.edit.messages.success')
    else
      render action: 'edit'
    end
  end

  def update_content
    #This action wont fail
    @asset.update_content! asset_content_params[:content]
    redirect_to @show_path, notice: I18n.t('client_application_assets.edit_content.messages.success')
  end

  def update_name
    if @asset.rename(params[:client_application_asset][:file])
      redirect_to @show_path
    else
      render action: 'rename'
    end
  end

  def destroy
    if @asset.destroy
      redirect_to client_application_assets_path(@client_application),
        notice: t('client_application_assets.destroy.messages.success')
    else
      redirect_to client_application_assets_path(@client_application),
        alert:  t('client_application_assets.destroy.messages.has_representations')
    end
  end

  def dissasociate_representations
    @asset.base_representations = []
    @asset.save!
    redirect_to client_application_assets_path(@client_application),
      notice: t('client_application_assets.dissasociate_representations.messages.success')
  end

  def toggle_as
    # Toggles this asset as logo or favicon for current client application
    toggable = params[:toggable]
    return unless ["favicon", "logo"].include? toggable
    msg = if @asset.send("toggle_as_#{toggable}", @client_application)
      status = @asset.send("#{toggable}_for?", @client_application)? 'set' : 'unset'
      {notice: t("client_application_assets.#{status}_as_#{toggable}.messages.success")}
    else
      {alert: t("client_application_assets.set_as_#{toggable}.messages.error")}
    end
    redirect_to client_application_assets_path(@client_application), msg
  end

  private

  def set_client_application
    @client_application = ClientApplication.find(params[:client_application_id])
  end

  def asset_params
    params.require(:client_application_asset).permit(:file, :prefix_path, :toggable)
  end

  def asset_content_params
    params.require(:client_application_asset).permit(:content)
  end

  def set_paths_to_client_application_assets_controller
    if @client_application
      session['client_application_asset.list_path'] = client_application_assets_path(@client_application)
      if @asset.id
        session['client_application_asset.edit_path'] = edit_client_application_asset_path(@client_application, @asset)
        session['client_application_asset.edit_content_path'] = edit_content_client_application_asset_path(@client_application, @asset)
        session['client_application_asset.rename_path'] = rename_client_application_asset_path(@client_application, @asset)
        session['client_application_asset.show_path'] = client_application_asset_path(@client_application, @asset)
      end
    end
  end

  def set_paths_from_session
    @list_path         = session['client_application_asset.list_path'] || client_application_assets_path(@client_application)
    @edit_path         = session['client_application_asset.edit_path'] ||
      @asset && @asset.persisted? && edit_client_application_asset_path(@client_application, @asset) ||
      client_application_assets_path(@client_application)
    @edit_content_path = session['client_application_asset.edit_content_path'] ||
      @asset && @asset.persisted? && edit_content_client_application_asset_path(@client_application, @asset) ||
      client_application_assets_path(@client_application)
    @show_path         = session['client_application_asset.show_path'] ||
      @asset && @asset.persisted? && client_application_asset_path(@client_application, @asset) ||
      client_application_assets_path(@client_application)
    @rename_path       = session['client_application_asset.rename_path'] ||
      @asset && @asset.persisted? && rename_client_application_asset_path(@client_application, @asset) ||
      client_application_assets_path(@client_application)
  end

    def check_editable?
      unless @asset.editable?
        redirect_to @show_path,
                    alert: I18n.t('client_application_assets.edit_content.messages.error')
      end
    end
end
