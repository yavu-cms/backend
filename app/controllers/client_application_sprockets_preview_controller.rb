class ClientApplicationSprocketsPreviewController < ClientApplicationSprocketsController
  protected

  def self.client_application(env)
    super.for_preview
  end
end
