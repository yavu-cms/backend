class SectionsController < ApplicationController
  load_and_authorize_resource
  before_action :filter, only: [:index]
  before_action :order,  only: [:index]
  before_action :clear_filter, only: [:index]

  def index
    @sections = @order.sort_list @filter.all.page(params[:page]).per(per_page)
  end

  def show
    render layout: !request.xhr?
  end

  def new
  end

  def edit
  end

  def create
    @section = Section.new(section_params)
    if @section.save
      redirect_to @section, notice: I18n.t('sections.new.messages.success')
    else
      render action: 'new'
    end
  end

  def update
    if @section.update(section_params)
      redirect_to @section, notice: I18n.t('sections.edit.messages.success')
    else
      render action: 'edit'
    end
  end

  def destroy
    if @section.destroy
      flash[:notice] = I18n.t('activerecord.actions.section.destroyed')
    else
      flash[:alert] = I18n.t('activerecord.actions.section.has_articles_associated')
    end
    redirect_to sections_url(page: current_page)
  end

  def tree
    @sections = Section.roots
    render layout: !request.xhr?
  end

  def search
    ids = (params[:ids] || []).reject &:blank?
    key_method = params[:key_method].to_sym rescue :id
    matches = if ids.empty?
                Section.search(query: params[:q]).limit(search_limit)
              else
                meth = params[:order].present?? :find_ordered : :find
                Section.with_supplement.public_send meth, ids
              end
    render json: matches.map { |section| Hash[id: section.send(key_method), text: section.to_s] }
  end

  def articles
    redirect_to articles_path(filter: { section_id: @section.id })
  end

  private
    def section_params
      params.require(:section).permit(:name, :slug, :is_visible, :main_article_id, :supplement_id, :parent_id, extra_attribute_model_attributes: ExtraAttribute.for(Section).map(&:name)+[:id])
    end

    def filter_params
      if params.has_key?(:filter)
        params.require(:filter).permit(:name, :supplement_id).to_options
      end
    end

    def filter
      @filter = create_filter SectionsFilter, { values: filter_params }
    end

    def order
      @order = create_order SectionsOrder, { sort: sort_params }
    end

    def clear_filter
      if params[:commit] == t('activerecord.actions.shared.clear_filter')
        @filter.clear!
        redirect_to request.path
      end
    end
end
