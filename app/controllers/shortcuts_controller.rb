class ShortcutsController < ApplicationController
  load_and_authorize_resource

  def manage
    @shortcuts = current_user.shortcuts.order(:title)
    render layout: !request.xhr?
  end

  def add_to_menu
    @shortcut.show_in_menu!
    redirect_to action: :manage
  end

  def remove_from_menu
    @shortcut.remove_from_menu!
    redirect_to action: :manage
  end

  def new
    @shortcut = current_user.shortcuts.build(url: params[:url], title: params[:title])
    render layout: !request.xhr?
  end

  def edit
    render layout: !request.xhr?
  end

  def create
    if @shortcut.save
      @notice = I18n.t('shortcuts.new.messages.success')
      if params.has_key? :postbuster
        render action: 'create', layout: !request.xhr?
      else
        redirect_to shortcuts_path, notice: @notice
      end
    else
      render action: 'new', layout: !request.xhr?
    end
  end

  def update
    if @shortcut.update(shortcut_params)
      redirect_to shortcuts_url, notice: I18n.t('shortcuts.edit.messages.success')
    else
      render action: 'edit', layout: !request.xhr?
    end
  end

  def destroy
    @shortcut.destroy
    redirect_to request.referer || shortcuts_path, notice: I18n.t('shortcuts.destroy.messages.success')
  end

  private
    def set_shortcut
      @shortcut = current_user.shortcuts.find(params[:id])
    end

    def shortcut_params
      params.require(:shortcut).permit(:title, :url, :show_in_menu)
    end
end
