class VersionsController < ApplicationController
  load_and_authorize_resource

  before_action :filter,       only: [:index]
  before_action :clear_filter, only: [:index]
  before_action :order,        only: [:index]
  before_action :set_versions, only: [:index]

  def index
  end

  # TODO: add proper docs
  # busco el previous en la version q estoy viendo y
  # busco el resource hacia adelante con version.next
  def show
    klass = @version.item_type.constantize
    @previous = @version.try(:reify, reify_params) || klass.new
    @resource = (@version.event == 'destroy' ? klass.new : (@version.next.try(:reify, reify_params) || klass.find_by(id: @version.item_id)))
  end

  # def diff_current
  #   @previous = @version.try(:reify, has_many: true, has_one: true) || @version.item_type.constantize.new
  #   @resource = @version.item_type.constantize.find(@version.item_id) rescue @version.item_type.constantize.new
  #   render :show
  # end

  # TODO: receive more params for reification?
  # TODO: what happens when you can't restore
  def restore
    reified = @version.reify(reify_params).restore
    instance_variable_set "@#{@version.item_type.underscore}", reified
    redirect_to (view_context.generic_show_path(reified) || versions_path), notice: t('versions.restored')
  end

  private

  def reify_params
    { has_many: false, has_one: false }
  end

  def filter
    @filter = create_filter VersionsFilter, {values: filter_params}
  end

  def filter_params
    params.require(:filter).permit([:event, :item_type, :item_id, :whodunnit]).to_options if params.has_key?(:filter)
  end

  def order
    @order = create_order VersionsOrder, sort: sort_params
  end

  def clear_filter
    if params[:commit] == t('activerecord.actions.shared.clear_filter')
      @filter.clear!
      redirect_to request.path
    end
  end

  def set_versions
    @versions = @order.sort_list @filter.all.page(params[:page]).per(per_page)
  end
end
