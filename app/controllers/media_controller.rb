class MediaController < ApplicationController
  load_and_authorize_resource class: 'Medium', new: [:new_audio, :new_embedded, :new_file, :new_image]

  before_action :guard_type, only: [:create]
  before_action :filter, only: [:index]
  before_action :clear_filter, only: [:index]

  respond_to :html, :json

  def index
    @media = @filter.all.page(params[:page]).per(per_page)
  end

  def show
    # Allow creating and showing embedded content in Chrome
    response.headers['X-XSS-Protection'] = '0'
    respond_with @medium
  end

  def new
    redirect_to media_path, alert: I18n.t('media.new.messages.missing_type')
  end

  def new_audio
    @medium = AudioMedium.new
    render action: 'new'
  end

  def new_embedded
    @medium = EmbeddedMedium.new
    render action: 'new'
  end

  def new_file
    @medium = FileMedium.new
    render action: 'new'
  end

  def new_image
    @medium = ImageMedium.new
    render action: 'new'
  end

  def create
    @medium = Medium.new(medium_params)
    if @medium.save
      respond_to do |format|
        format.html { redirect_to medium_path(@medium) }
        format.json {
          if params[:media_modal]
            find
          else
            render json: { id: @medium.id , src: @medium.thumbnail, type: @medium.type, filename: @medium.filename }
          end
        }
      end
    else
      respond_to do |format|
        format.html { render action: 'new'}
        format.json { render json: { errors: @medium.errors.full_messages }, status: 422 }
      end
    end
  end

  def edit
  end

  def update
    if @medium.update(medium_params)
      redirect_to medium_path(@medium), notice: t('media.edit.messages.success')
    else
      render action: 'edit'
    end
  end

  def destroy
    begin
      if @medium.destroy
        flash[:notice] = I18n.t('media.destroy.messages.success')
      else
        flash[:alert] = I18n.t('media.destroy.messages.use_on_component')
      end
    rescue ActiveRecord::DeleteRestrictionError
      flash[:alert] = I18n.t('media.destroy.messages.already_in_use')
    end
    redirect_to media_path(page: current_page)
  end

  def search
    @media = Medium.search(search_params).page(params[:page]).per(search_per_page)
    render layout: !request.xhr?
  end

  def find
    @article = Article.find_by(id: params[:article_id]) if params[:article_id]
    result = {
      preview: if params[:fronterized].present? && params[:media_size].present?
                 view_context.medium_tag(@medium, size: params[:media_size])
               else
                 view_context.preview_medium(@medium, prefix: 'modal', controller: 'articles')
               end,
      caption: @article.try(:title),
      medium_id: @medium.id
    }
    render json: result
  end

  def autocomplete
    ids = (autocomplete_params[:ids] || []).reject &:blank?
    matches = if ids.empty?
                Medium.search(query: autocomplete_params[:q]).limit(search_limit)
              else
                meth = autocomplete_params[:order].present?? :find_ordered : :find
                Medium.public_send(meth, autocomplete_params[:ids])
              end
    render json: matches.map { |medium| Hash[id: medium.id, text: render_to_string(partial: "media/#{medium.type.underscore}/select2_options", locals: { medium: medium } )] }
  end

  private
    # Before-action callback to guard the validity of the media type provided
    # by the user. Any invalid type will trigger a redirection to the index
    # page with an exaplanatory flash message
    def guard_type
      valid_types = Medium.types.collect &:to_s
      type = params[:medium][:type]
      unless valid_types.include? type
        respond_to do |format|
          format.html { redirect_to media_path, alert: I18n.t('media.new.messages.missing_type') }
          format.json { render json: { errors: I18n.t('media.new.messages.missing_type') }, status: 422 }
        end
      end
    end

    # Façade method to provide a unique entry-point for any of the *_params
    # methods that are type-specific.
    def medium_params
      type = params[:medium][:type]
      fields = send :"#{type.underscore}_fields" rescue {}
      params.require(:medium).permit(*fields).to_options
    end

    def filter_params
      if params.has_key?(:filter)
        params.require(:filter)
          .permit(:query, :type, :date, :date_from, :date_to, :article_id).to_options
      end
    end

    def search_params
      params.require(:search)
    end

    def autocomplete_params
      params.permit(:q, :order, ids: [])
    end

    def filter
      @filter = create_filter MediaFilter, values: filter_params
    end

    def clear_filter
      if params[:commit] == I18n.t('activerecord.actions.shared.clear_filter')
        @filter.clear!
        redirect_to request.path
      end
    end

    # Parameter filter for AudioMedium-specific parameters
    def audio_medium_fields
      %i(type name date_string serialized_tags file)
    end

    # Parameter filter for EmbeddedMedium-specific parameters
    def embedded_medium_fields
      %i(type name date_string serialized_tags content video embedded)
    end

    # Parameter filter for FileMedium-specific parameters
    def file_medium_fields
      %i(type name date_string serialized_tags file)
    end

    # Parameter filter for ImageMedium-specific parameters
    def image_medium_fields
      %i(type name date_string serialized_tags file xsmall_version small_version medium_version big_version xbig_version)
    end
end
