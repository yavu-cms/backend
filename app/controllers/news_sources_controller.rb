class NewsSourcesController < ApplicationController
  load_and_authorize_resource

  respond_to :html

  def index
    @news_sources = NewsSource.all.page(params[:page]).per(per_page)
  end

  def new
  end

  def create
    @news_source = NewsSource.create news_source_params
    redirect_to @news_source, notice: t('news_sources.new.messages.success')
  end

  def edit
  end

  def update
    @news_source.update news_source_params
    redirect_to @news_source, notice: t('news_sources.edit.messages.success')
  end

  def show
  end

  protected

  def news_source_params
    params.require(:news_source).permit(:name, :is_default, supplement_ids: [], client_application_ids: [])
  end
end
