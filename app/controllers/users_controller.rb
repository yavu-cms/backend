class UsersController < ApplicationController
  before_action :set_current_user, only: [:profile, :update_profile]
  load_and_authorize_resource except: [:profile, :update_profile]

  before_action :filter,       only: [:index]
  before_action :clear_filter, only: [:index]

  def index
    @users = @filter.all.page(params[:page]).per(per_page)
  end

  def show
  end

  def new
  end

  def edit
  end

  def create
    if @user.save
      redirect_to @user, notice: t('users.new.messages.success')
    else
      render action: 'new'
    end
  end

  def update
    if @user.update(user_params)
      redirect_to @user, notice: t('users.edit.messages.success')
    else
      render action: 'edit'
    end
  end

  def destroy
    if @user.destroy
      flash[:notice] = t('users.destroy.messages.success')
    else
      flash[:alert] = t('users.destroy.messages.alert')
    end
    redirect_to users_path
  end

  def profile
  end

  def update_profile
    @user.requires_current_password = true # Force validation of the current password
    if @user.update(profile_params)
      redirect_to profile_path, notice: t('users.profile.messages.success')
    else
      render action: 'profile'
    end
  end

  private
    def set_current_user
      @user = current_user
      authorize! :profile, @user
    end

    def user_params
      params.require(:user).permit(:username, :email, :current_password, :new_password, :new_password_confirmation,
                                   :all_client_applications, :all_sections, :all_representations,
                                   role_ids: [], section_ids: [], client_application_ids: [], representation_ids: [])
    end

    def profile_params
      params.require(:user).permit(:email, :current_password, :new_password, :new_password_confirmation)
    end

    def filter_params
      if params.has_key?(:filter)
        params.require(:filter).permit(:user).to_options
      end
    end

    def filter
      @filter = create_filter UsersFilter, values: filter_params
    end

    def clear_filter
      if params[:commit] == t('activerecord.actions.shared.clear_filter')
        @filter.clear!
        redirect_to request.path
      end
    end
end
