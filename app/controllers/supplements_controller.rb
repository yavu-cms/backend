class SupplementsController < ApplicationController
  load_and_authorize_resource
  before_action :filter,         only: [:index]
  before_action :clear_filter,   only: [:index]

  def index
    @index_path = supplements_path
    @supplements = @filter.all.page(params[:page]).per(per_page)
  end

  def show
  end

  def new
  end

  def edit
  end

  def active_edition
    if @supplement.active_edition
      redirect_to edition_path(@supplement.active_edition)
    else
      redirect_to supplements_path, alert: t('supplements.active_edition.messages.failed')
    end
  end

  def editions
    redirect_to editions_path(filter: { supplement_id: @supplement.id, name: nil })
  end

  def sections
    redirect_to sections_path(filter: { supplement_id: @supplement.id, name: nil })
  end

  def become_default
    @supplement.become_default!
    redirect_to supplements_path, notice: t('supplements.become_default.messages.success')
  rescue Exception
    redirect_to supplements_path, alert: t('supplements.become_default.messages.no_active_edition')
  end

  def create
    @supplement = Supplement.new(supplement_params)

    if @supplement.save
      redirect_to @supplement, notice: (I18n.t 'supplements.new.messages.success')
    else
      render action: "new"
    end
  end

  def update
    if @supplement.update(supplement_params)
      redirect_to @supplement, notice: (I18n.t 'supplements.edit.messages.success')
    else
      render action: "edit"
    end
  end

  def destroy
    if @supplement.destroy
      flash[:notice] = t('activerecord.actions.supplement.destroyed')
    else
      # Assumes asociation restriction error
      flash[:alert] = t('supplements.destroy.messages.has_editions_or_sections_associated')
    end

    redirect_to supplements_url(page: current_page)
  end

  private

  def supplement_params
    params.require(:supplement).permit(:name, :logo_id, :news_source_id, :is_default, extra_attribute_model_attributes: ExtraAttribute.for(Supplement).map(&:name)+[:id])
  end

  def filter_params
    if params.has_key?(:filter)
      params.require(:filter).permit(:name).to_options
    end
  end

  def filter
    @filter = create_filter SupplementsFilter, values: filter_params
  end

  def clear_filter
    if params[:commit] == t('activerecord.actions.shared.clear_filter')
      @filter.clear!
      redirect_to request.path
    end
  end
end
