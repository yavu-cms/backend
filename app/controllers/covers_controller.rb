class CoversController < ApplicationController
  load_and_authorize_resource :client_application
  before_action :set_original_representation, only: [:edit, :update]
  before_action :set_representation_draft,    only: [:edit, :update]
  before_action :ensure_cover,                only: [:edit, :update]
  before_action :ensure_cover_articles_components_are_available, only: [:edit, :update]
  before_action :ensure_draft_exists,         only: [:edit, :update]
  before_action :authorize_cover_update,      only: [:edit, :update]

  before_action :filter, only: [:index]
  before_action :clear_filter, only: [:index]
  before_action :order, only: [:index]

  respond_to :html

  def index
    authorize! :read, :cover
    @covers = @order.sort_list @filter.all.page(params[:page]).per(per_page)
  end

  def edit
    @module_types = CoverArticlesComponent.enabled
    @sections = current_user.accessible_sections
    @configuration_template = CoverArticlesComponentConfiguration.new template_attributes
  end

  def update
    begin
      if @draft.cover_from_json(cover_params[:configuration])
        UniqueAccessWorker.perform_async(view_context.channel_path(@draft, namespace: '/uniqueaccess'), {user: current_user.username, saved: true}, view_context.announcements_url)
        flash[:notice] = I18n.t('representations.cover.messages.success')
        return redirect_to preview_path_from(:covers) if cover_params[:operation] == 'apply'
      else
        flash[:alert] = I18n.t('representations.cover.messages.wrong_structure')
      end
    rescue ActiveRecord::StaleObjectError
      flash[:alert] = I18n.t('activerecord.errors.stale_object')
    end

    redirect_to params[:gbt] || covers_path
  end

  private

  def filter_params
    if params.has_key?(:filter)
      params.require(:filter).permit(:name, :client_application_id).to_options
    end
  end

  def filter
    @filter = create_filter CoversFilter, values: filter_params
  end

  def clear_filter
    if params[:commit] == t('activerecord.actions.shared.clear_filter')
      @filter.clear!

      redirect_to request.path
    end
  end

  def order
    @order = create_order CoversOrder, {sort: sort_params}
  end

  # Set the requested client_application in @client_application.
  # Redirect to +client_applications_path+ if unable to find it.
  def set_client_application
    @client_application = ClientApplication.find(params[:client_application_id])
  rescue ActiveRecord::RecordNotFound
    redirect_to client_applications_path,
                alert: I18n.t('representations.errors.missing_client_application')
  end

  # Return the requested representation from those belonging to @client_application.
  # Redirect to +client_application_representations_path+ if unable to find the representation.
  def fetch_representation
    @client_application.representations.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    redirect_to client_application_representations_path(@client_application),
                alert: I18n.t('representations.errors.representation_does_not_belong_to_client_application')
  end

  # Set the requested representation in @representation.
  # @see RepresentationsController#fetch_representation
  def set_representation
    @representation = fetch_representation
  end

  # Set the requested representation in @original_representation.
  # @see RepresentationsController#fetch_representation
  def set_original_representation
    @original_representation = fetch_representation
  end

  # Set the requested draft in @draft and @representation.
  # @see RepresentationsController#fetch_representation
  def set_representation_draft
    @representation = @draft = RepresentationDraft.includes(rows: {columns: { component_configurations: :component }}).find_by(representation: @original_representation).tap do |draft|
    # @representation = @draft = @original_representation.draft.tap do |draft|
      if draft
        draft.enable_locking!
        draft.lock_version = lock_version_params[:representation][:lock_version] if lock_version_params[:representation] && lock_version_params[:representation][:lock_version]
      end
    end
  end

  # Ensure that the requested representation is a cover one, or redirect to its show action with an alert message
  def ensure_cover
    redirect_to covers_path, alert: I18n.t('representations.cover.messages.not_cover') unless @original_representation.cover?
  end

  def ensure_draft_exists
    unless @draft.present?
      @original_representation.trigger_draft_creation
      redirect_to covers_path, alert: I18n.t('representations.errors.draft_unavailable')
    end
  end

  # Ensure that at least one CoverArticlesComponent is available, otherwise the cover editor is useless.
  # Redirect to the representation's show action with an alert message if none is available.
  # There's no need to check for CoverComponents here, as we have a previous check to see if the requested
  # representation is a cover.
  def ensure_cover_articles_components_are_available
    unless CoverArticlesComponent.any?
      redirect_to covers_path, alert: I18n.t('representations.cover.messages.no_cover_components_available')
    end
  end

  def representation_params
    params.require(:representation).permit(:name, :configuration)
  end

  def cover_params
    params.require(:cover).permit(:configuration, :operation)
  end

  def lock_version_params
    params.permit(representation: [:lock_version])
  end

  def preview_path_from(action)
    path = send(:"#{action}_path")
    preview_client_application_representation_path(@client_application, @original_representation, gbt: path)
  end

  def authorize_cover_update
    authorize! :update, :cover
    authorize! :update, @original_representation
  end

  def template_attributes
    template_cover_article = CoverArticlesComponent.first
    { description: I18n.t('representations.cover.editor.settings.default_description'),
      component: template_cover_article,
      view: template_cover_article.views.first }
  end
end
