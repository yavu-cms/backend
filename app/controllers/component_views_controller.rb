class ComponentViewsController < ApplicationController
  load_resource :component, class: BaseComponent
  load_and_authorize_resource through: :component, through_association: :views, class: View

  def index
    @component_views = @component.views
  end

  def new
  end

  def create
    if @component_view.save
      redirect_to component_view_path(@component, @component_view), notice: I18n.t('component/views.new.messages.success')
    else
      render action: 'new'
    end
  end

  def edit
  end

  def update
    if @component_view.update(component_view_params)
      redirect_to component_view_path(@component, @component_view), notice: I18n.t('component/views.update.messages.success')
    else
      render action: 'edit'
    end
  end

  def make_default
    if @component_view.make_default!
      flash[:notice] = I18n.t('component/views.make_default.messages.success')
    else
      flash[:alert] = I18n.t('component/views.make_default.messages.fail')
    end
    redirect_to component_views_path(@component)
  end

  def show
  end

  def destroy
    if @component_view.destroy
      flash[:notice] = I18n.t('component/views.destroy.messages.success')
    else
      flash[:alert] = I18n.t('component/views.destroy.messages.fail')
    end
    redirect_to component_views_path(@component)
  end

  private

  def component_view_params
    params.require(:view).permit(:name, :value, :is_layout, :format, :default)
  end
end
