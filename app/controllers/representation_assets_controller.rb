class RepresentationAssetsController < ApplicationController
  DELETE_FROM_REPRESENTATION_BATCH = 1
  ADD_TO_REPRESENTATION_BATCH = 2

  load_and_authorize_resource :client_application
  before_action :set_representation_draft
  before_action :set_client_application_asset, only: [:show, :edit, :edit_content, :rename, :toggle_as_favicon, :toggle_into_representation]

  # The following two filters are needed to implement go_back path
  before_action :set_paths_to_representation_assets_controller, only: [:new, :new_empty, :show, :edit, :edit_content, :rename, :toggle_as_favicon, :toggle_into_representation]
  before_action :set_paths_from_session
  before_action :check_editable?, only: [:edit_content]

  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found

  def index
    @application_assets = @client_application.assets.page(params[:page]).per(per_page)
  end

  def new
    @asset = @client_application.assets.new
    render 'client_application_assets/new'
  end

  def new_empty
    @asset = @client_application.assets.new
    render 'client_application_assets/new_empty'
  end

  def edit
    render 'client_application_assets/edit'
  end

  def edit_content
    render 'client_application_assets/edit_content'
  end

  def rename
    render 'client_application_assets/rename'
  end

  def show
    @container = @representation
    render 'client_application_assets/show'
  end

  def toggle_as_favicon
    msg = if @asset.toggle_as_favicon(@representation)
      status = @asset.favicon_for?(@representation)? 'set' : 'unset'
      {notice: t("client_application_assets.#{status}_as_favicon.messages.success")}
    else
      {alert: t('client_application_assets.set_as_favicon.messages.error')}
    end
    redirect_to @list_path, msg
  end

  def toggle_into_representation
    if @representation.assets.include? @asset
      @representation.assets.destroy(@asset)
      msg = I18n.t('representation_assets.toggle_into_representation.delete')
    else
      @representation.assets << @asset
      msg = I18n.t('representation_assets.toggle_into_representation.add')
    end
    redirect_to @list_path, notice: msg
  end

  def batch_action
    case Integer(params[:batch_action])
    when DELETE_FROM_REPRESENTATION_BATCH then
      @representation.assets.delete(ClientApplicationAsset.find(params[:assets]))
      msg = I18n.t('representation_assets.batch_action.delete.message')
    when ADD_TO_REPRESENTATION_BATCH then
      @representation.assets.concat(ClientApplicationAsset.where(id: params[:assets]).where.not(id: @representation.assets.pluck(:id)))
      msg = I18n.t('representation_assets.batch_action.add.message')
    end if params[:assets]
    redirect_to @list_path, notice: msg
  end

  private

  def record_not_found
    redirect_to client_applications_path, alert: I18n.t('representations.errors.missing_client_application')
  end

  def set_representation_draft
    @representation = RepresentationDraft.find_by! client_application: @client_application, id: params[:representation_id]
  rescue ActiveRecord::RecordNotFound
    redirect_to client_application_representations_path(@client_application), alert: I18n.t('representations.errors.draft_unavailable')
  end

  def set_client_application_asset
    @asset = @client_application.assets.find(params[:id])
  end

  def set_paths_to_representation_assets_controller
    if @client_application && @representation
      session['client_application_asset.list_path'] = client_application_representation_assets_path(@client_application, @representation)
      if @asset
        session['client_application_asset.edit_path'] = edit_client_application_representation_asset_path(@client_application, @representation, @asset)
        session['client_application_asset.edit_content_path'] = edit_content_client_application_representation_asset_path(@client_application, @representation, @asset)
        session['client_application_asset.rename_path'] = rename_client_application_representation_asset_path(@client_application, @representation, @asset)
        session['client_application_asset.show_path'] = client_application_representation_asset_path(@client_application, @representation, @asset)
      end
    end
  end

  def set_paths_from_session
    @list_path         = session['client_application_asset.list_path'] ||
      client_application_representation_assets_path(@client_application, @representation)
    @edit_path         = session['client_application_asset.edit_path'] ||
      @asset && @asset.persisted? && edit_client_application_representation_asset_path(@client_application, @representation,  @asset) ||
      client_application_representation_assets_path(@client_application, @representation)
    @edit_content_path = session['client_application_asset.edit_content_path'] ||
      @asset && @asset.persisted? &&
      edit_content_client_application_representation_asset_path(@client_application, @representation, @asset) ||
      client_application_representation_assets_path(@client_application, @representation)
    @rename_path = session['client_application_asset.rename_path'] ||
      @asset && @asset.persisted? &&
      rename_client_application_representation_asset_path(@client_application_asset, @representation, @asset) ||
      client_application_representation_assets_path(@client_application, @representation)
    @show_path         = session['client_application_asset.show_path'] ||
      @asset && @asset.persisted? && client_application_representation_asset_path(@client_application, @representation, @asset) ||
      client_application_representation_assets_path(@client_application, @representation)
  end

    def check_editable?
      unless @asset.editable?
        redirect_to @show_path, alert: I18n.t('client_application_assets.edit_content.messages.error')
      end
    end
end
