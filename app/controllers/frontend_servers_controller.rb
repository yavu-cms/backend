class FrontendServersController < ApplicationController
  before_action :ensure_frontend_server, only: [:configure, :restart]
  before_action :ensure_client_application, only: :configure
  def index
    @servers = FrontendServer.all
  end

  def configure
    @server.configure @client_application
    redirect_to frontend_servers_path, notice: I18n.t('frontend_servers.configure.message')
  end

  def restart

  end

  private
  def ensure_client_application
    @client_application = ClientApplication.enabled.find params[:client_application_id]
  end

  def ensure_frontend_server
    @server = FrontendServer.find params[:id]
    head :forbidden and return unless @server
  end
end
