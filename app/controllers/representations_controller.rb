class RepresentationsController < ApplicationController
  load_and_authorize_resource :client_application
  load_and_authorize_resource :representation, through: :client_application
  before_action :order,                       only: [:index]
  before_action :filter,                      only: [:index]
  before_action :clear_filter,                only: [:index]
  before_action :set_representation,          only: [:show, :rename, :update_name, :discard_draft, :destroy]
  before_action :set_original_representation, only: [:edit, :update, :preview, :apply, :manage_assets, :copy, :create_copy]
  before_action :set_representation_draft,    only: [:edit, :update, :preview, :apply, :manage_assets]
  before_action :set_editor_templates,        only: [:edit, :update]
  before_action :ensure_draft_exists,         only: [:edit, :update, :preview, :apply, :manage_assets]

  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found
  respond_to :html

  def index
    @representations = @order.sort_list @filter.all.page(params[:page]).per(per_page)
  end

  def show
  end

  def new
    @representation = Representation.new client_application: @client_application
  end

  def rename
  end

  def edit
    @available_components = BaseComponent.available_for_editor
  end

  def copy
    @representation = @original_representation.copy
  end

  def create_copy
    @representation = @original_representation.copy representation_params[:name]
    if @representation.save
      redirect_to client_application_representation_path(@client_application, @representation),
                  notice: I18n.t('representations.copy.messages.success')
    else
      render action: 'copy'
    end
  end

  def create
    @representation = Representation.new(representation_params)
    @representation.client_application = @client_application
    if @representation.save
      redirect_to edit_client_application_representation_path(@client_application, @representation),
                  notice: I18n.t('representations.new.messages.success')
    else
      render action: 'new'
    end
  end

  def update_name
    new_name = params[:representation][:name] rescue nil
    if @representation.update(name: new_name)
      redirect_to client_application_representation_path(@client_application, @representation),
                  notice: I18n.t('representations.update_name.messages.success')
    else
      render action: 'rename'
    end
  end

  def update
    begin
      if @draft.from_json(editor_params[:configuration])
        UniqueAccessWorker.perform_async(view_context.channel_path(@draft, namespace: '/uniqueaccess'), {user: current_user.username, saved: true}, view_context.announcements_url)
        @result = {notice: I18n.t('representations.save_draft.messages.success'), lock_version: BaseRepresentation.find(@draft.id).lock_version}
        @result[:redirect] = preview_path_from(:edit) if editor_params[:operation] == 'apply'
      else
        # Set an error HTTP status code
        self.status = 422
        @result = { alert: I18n.t('representations.apply_draft.messages.fail'), failures: @draft.errors.full_messages }
      end
    rescue ActiveRecord::StaleObjectError
      self.status = 422
      @result = { alert: I18n.t('representations.apply_draft.messages.fail'), failures: [I18n.t('activerecord.errors.stale_object')], lock_version: BaseRepresentation.find(@draft.id).lock_version }
    end

    respond_to do |format|
      format.json do
        render json: @result
      end
      format.html do
        redirect_to edit_client_application_representation_path(@client_application, @draft.representation), @result
      end
    end
  end

  def discard_draft
    old_draft = @representation.draft
    if @representation.discard_draft
      if old_draft
        UniqueAccessWorker.perform_async(view_context.channel_path(old_draft, namespace: '/uniqueaccess'), {user: current_user.username, destroyed: true}, view_context.announcements_url)
      end
      flash[:notice] = I18n.t('representations.discard_draft.messages.success')
    else
      flash[:alert] = I18n.t('representations.discard_draft.messages.failed')
    end
    redirect_to params[:gbt] || client_application_representations_path(@client_application)
  end

  def destroy
    if @representation.destroy
      if @representation.has_draft?
        UniqueAccessWorker.perform_async(view_context.channel_path(@representation.draft, namespace: '/uniqueaccess'), {user: current_user.username, destroyed: true}, view_context.announcements_url)
      end
      flash[:notice] = I18n.t('representations.destroy.messages.success')
    else
      flash[:alert] = I18n.t('representations.destroy.messages.error')
    end
    redirect_to client_application_representations_path(@client_application)
  end

  def preview
    @go_back = params[:gbt] || client_application_representations_path(@client_application)
    @default_route = @client_application.routes.find_by representation: @original_representation
    render 'client_applications/preview', layout: 'preview'
  end

  def apply
    @draft.apply!
    # At this point, THE representation is the previous draft
    @representation = @draft
    # Send to purge the representation's routes
    Purger.purge_representation @representation
    UniqueAccessWorker.perform_async(view_context.channel_path(@draft, namespace: '/uniqueaccess'), {user: current_user.username, destroyed: true}, view_context.announcements_url)
    redirect_to params[:gbt] || client_application_representations_path(@client_application),
                notice: I18n.t('representations.apply_draft.messages.success')
  end

  def manage_assets
    redirect_to client_application_representation_assets_path(@client_application, @draft)
  end

  private

  def record_not_found
    redirect_to client_applications_path, alert: I18n.t('representations.errors.invalid_client_application_or_representation')
  end

  # Return the requested representation from those belonging to @client_application.
  # Redirect to +client_application_representations_path+ if unable to find the representation.
  def fetch_representation
    @client_application.representations.find(params[:id])
  end

  # Set the requested representation in @representation.
  # @see RepresentationsController#fetch_representation
  def set_representation
    @representation = fetch_representation
  end

  # Set the requested representation in @original_representation.
  # @see RepresentationsController#fetch_representation
  def set_original_representation
    @original_representation = fetch_representation
  end

  # Set the requested draft in @draft and @representation.
  # @see RepresentationsController#fetch_representation
  def set_representation_draft
    @representation = @draft = @original_representation.draft.tap do |draft|
      if draft
        draft.enable_locking!
        draft.lock_version = lock_version_params[:representation][:lock_version] if lock_version_params[:representation] && lock_version_params[:representation][:lock_version]
      end
    end
  end

  def set_editor_templates
    @row_template = Row.new
    @column_template = Column.new
    @component_configuration_template = ComponentConfiguration.new description: I18n.t('representations.edit.component.default_description'), component: Component.new
  end

  def representation_params
    params.require(:representation).permit(:name, :configuration)
  end

  def editor_params
    params.require(:representation).permit(:configuration, :operation)
  end

  def cover_params
    params.require(:cover).permit(:configuration, :operation)
  end

  def lock_version_params
    params.permit(representation: [:lock_version])
  end

  def ensure_draft_exists
    unless @draft.present?
      @original_representation.trigger_draft_creation
      redirect_to client_application_representations_path(@client_application),
                  alert: I18n.t('representations.errors.draft_unavailable')
    end
  end

  def order
    @order = create_order RepresentationsOrder, sort: sort_params
  end

  def filter_params
    if params.has_key?(:filter)
      params.require(:filter).permit(:name).to_options
    end
  end

  # Create the @filter object with the currently-set values for the filters
  def filter
    @filter = create_filter RepresentationsFilter, {values: filter_params}
    @filter.client_application = @client_application
    @filter
  end

  # Clear the currently-set filters, restoring them to the defaults
  def clear_filter
    if params[:commit] == t('activerecord.actions.shared.clear_filter')
      @filter.clear!
      redirect_to request.path
    end
  end

  def preview_path_from(action)
    path = send(:"#{action}_client_application_representation_path", @client_application, @original_representation)
    preview_client_application_representation_path(@client_application, @original_representation, gbt: path)
  end
end
