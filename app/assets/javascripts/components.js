$(document).foundation('section', { callback: function() {
  $('form').find('.js-plugin-codemirror').each(function(i, elem) {
    $(elem).data('cm-instance').refresh();
  });
}});

$('textarea.codemirror').live('click', function(){
  $(this).backendPlugin('codemirror', $(this).data('plugins')['codemirror']);
  $(this).data('cm-instance').refresh();
});

$('.color-picker .reset-color').on('click', function(){
	var $this = $(this);
	$this.closest('.color-picker').find('input[type=color]').prop('disabled', true);;
	$this.closest('.color-picker').find('input[type=hidden]').val('');
	return false;
})

$('.color-picker input[type=color]').on('change', function(){
	var $this = $(this);
	$this.closest('.color-picker').find('input[type=hidden]').val($this.val());
})