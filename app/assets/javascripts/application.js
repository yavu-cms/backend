//
// Application-wide scripts.
//
//= require announcements
//= require engines
//= require components

jQuery(function($) {
  // FOUC avoidance - Change the not-loaded class from the <html> element to loaded
  document.documentElement.className = document.documentElement.className.replace(/\bnot-loaded\b/, 'loaded');

  // Automatically call the Initializer backend plugin
  // on .auto-initialize elements
  $('.auto-initialize').backendPlugin('initializer');

  // Initialize Foundation's JS plugins
  $(document).foundation();
});
