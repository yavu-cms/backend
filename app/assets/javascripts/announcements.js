jQuery(function($) {
  var $announcements = $('#ws_announcements'),
    defaults = {timeout: 45, channels: {}, messages: {error: 'Connection error. Is backend running?'}},
    options = $.extend(defaults, $announcements.data('options')),
    client,
    ANNOUNCEMENT_AUTO_FADE_TIME = 10000, // 10 secs
    ANIMATION_DURATION = 250;

  function removeAnnouncement(announcement) {
    announcement.fadeOut(ANIMATION_DURATION, function() { announcement.remove(); });
  }

  function buildAnnouncement(info) {
    return $('<div/>', {class: [info.type, 'announcement'].join(' ')})
             .append($('<span/>', {class: 'message', text: info.message}))
             .append($('<time/>', {datetime: info.created_at}))
             .append($('<span/>', {class: 'detail', text: info.detail}))
             .append($('<a/>', {class: 'close', html: '&times;', href: '#'}).prepend($('<span/>', {class: 'countdown', text: ANNOUNCEMENT_AUTO_FADE_TIME / 1000})));
  }

  function addAnnouncement(data) {
    var announcement = buildAnnouncement(data),
      countdown = ANNOUNCEMENT_AUTO_FADE_TIME / 1000,
      $countdown = announcement.find('.countdown'),
      t;
    $announcements.find('.logs').append(announcement.hide().fadeIn(ANIMATION_DURATION));
    if (data.type === 'alert') {
      $countdown.remove();
    } else {
      t = setInterval(function() {
        $countdown.text(--countdown);
        if (countdown === 0) {
          removeAnnouncement(announcement);
          clearInterval(t);
        }
      }, 1000);
      announcement.on('mouseenter', function() { if (t) { clearInterval(t); $countdown.remove(); } });
    }
    announcement.on('click', '.close', function() { removeAnnouncement(announcement); return false; })
  }

  if (!options.url) {
    console.error('[Announcements] Missing URL option');
  } else {
    client = new Faye.Client(options.url, {timeout: options.timeout});

    client.on('transport:up', function() {
      $announcements.find('.status').empty();
    });

    for (var type in options.channels) {
      if (options.channels.hasOwnProperty(type)) {
        client.subscribe(options.channels[type], addAnnouncement);
      }
    }
  }
});
