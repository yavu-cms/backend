;(function($, undefined) {
  'use strict';

  var pluginName = 'covereditor',
    pluginDef = {},
    defaults = {
      searchArticlesUrl: '#missing',
      articleResultsLimit: 50
    },
    // Constants
    NAMESPACE                     = '.' + pluginName,
    DATATTR_SORTABLE_INITIALIZED  = pluginName + '-sortable-initialized',
    DATATTR_SUBMISSION_TEXT       = 'submission-text',
    ANIMATION_SPEED               = 'fast',
    // Element selectors
    SELECTOR_MODULE_CONTAINER     = '.module-container',
    SELECTOR_COVER                = '.cover-component-configuration',
    SELECTOR_MODULES_CONTAINER    = '.cover-articles-component-configurations-container',
    SELECTOR_SETUP_MODULE_MODAL   = '.settings-modal',
    SELECTOR_ARTICLES_RESULTS     = '.search-results ul',
    SELECTOR_ARTICLES_LIVEFILTER  = '.livefilter',
    SELECTOR_ARTICLES_SEARCHING_BOX = '.searching-box',
    SELECTOR_USED_ARTICLES        = '.associated-articles',
    SELECTOR_USED_ARTICLE         = SELECTOR_USED_ARTICLES + ' .article',
    SELECTOR_SORTABLE             = '.sortable',
    // Module interactions
    SELECTOR_SETUP_MODULE_BUTTON  = '.settings',
    SELECTOR_ADD_MODULE_BUTTON    = '.add-module',
    SELECTOR_MOVE_MODULE_BUTTON   = '.move',
    SELECTOR_REMOVE_MODULE_BUTTON = '.remove',
    SELECTOR_ARTICLES_SEARCH_FORM = '.available-articles-filters',
    SELECTOR_BUTTONS_SUBMIT_FORM  = '.form-actions input',
    // Const/Display/Value selectors
    SELECTOR_USABLE_INPUT         = ':input:not(:disabled)',
    CONST_COVER_DESCRIPTION       = '.const-cover-description',
    DISPLAY_COVER_DESCRIPTION     = '.display-cover-description',
    DISPLAY_ARTICLES_COUNT        = '.display-articles-count',
    DISPLAY_MAIN_ARTICLE          = '.display-main-article',
    DISPLAY_MODULE_DESCRIPTION    = '.display-module-description',
    DISPLAY_MODULE_TYPE           = '.display-module-type',
    DISPLAY_MODULE_VIEW           = '.display-module-view',
    SELECTOR_TYPE_DEPENDENT       = '.type-dependent',
    VALUE_MODULE_DESCRIPTION      = '.value-module-description',
    VALUE_MODULE_TYPE             = 'select.value-module-type',
    VALUE_MODULE_VIEW             = '.value-module-view',
    VALUE_ARTICLE_VIEW            = '.value-article-view',
    VALUE_MODULE_VIEW_USABLE      = VALUE_MODULE_VIEW + SELECTOR_USABLE_INPUT,
    VALUE_ARTICLE_VIEW_USABLE     = VALUE_ARTICLE_VIEW + SELECTOR_USABLE_INPUT,
    VALUE_WHOLE_CONFIGURATION     = '#value-configuration',
    VALUE_FORM_OPERATION          = '#value-form-operation',
    // General interactions
    SELECTOR_MODAL                = '.reveal-modal',
    SELECTOR_DISMISS_MODAL        = '.dismiss-modal',
    // Template selectors
    SELECTOR_TEMPLATE_MODULE      = '#template-module',
    // Loading indicators
    CLASS_SHOW_ON_LOAD            = 'show-on-load',
    SELECTOR_SHOW_ON_LOAD         = '.' + CLASS_SHOW_ON_LOAD,

    // Actions
    DELETE_ARTICLE_ACTION         = '.delete-action';

  // Create an element using the template identified by `templateId` within `container`
  function createElement(container, templateId) {
    return $(container.find(templateId).html());
  }

  function markAsDirty(pivot) {
    pivot.trigger('ua:dirty');
  }

  // Add a child element to a container
  function addChildElement(container, parent, templateId, type, action, afterCreationCb) {
    var child = createElement(container, templateId);
    // If an after-creation callback was provided, invoke it passing the new child element to it
    if (typeof afterCreationCb == 'function') {
      afterCreationCb(child);
    }
    // Add the the child element...
    $.fn[action].call(parent, child);
    // ...initialize any backend plugin it may have
    child.backendPlugin('initializer');
    markAsDirty(container);
    return container;
  }

  // Move an element before/after a sibling
  function moveElement(pivot, direction, siblingSelector, parent, type, previousDirectionValue) {
    var usePrevious = direction == (previousDirectionValue || 'up'),
      traversalMethod = usePrevious ? 'prev' : 'next',
      sibling = $.fn[traversalMethod].call(pivot, siblingSelector);

    if (sibling.length > 0) {
      pivot.swap(sibling);
      markAsDirty(pivot);
    }
  }

  function removeElement(element, parent, type) {
    element.fadeOut(ANIMATION_SPEED, function() {
      markAsDirty(element);
      element.remove();
    });
  }

  // Plugin initializer
  pluginDef.init = function(options) {
    var $this = this,
      opts = $.extend({}, defaults, options);

    // Utility function to declare general-purpose user event handlers
    function registerEventHandler(eventType, selector, handler) {
      $this.on(eventType + NAMESPACE, selector, function(e) {
        var $target = $(this);

        e.preventDefault();
        // Ask for confirmation before proceeding if a [data-ask] attribute is present
        if ($target.data('ask') && !confirm($target.data('ask'))) {
          return false;
        }
        handler.call($target);
      });
    }

    // Event for adding modules
    registerEventHandler('click', SELECTOR_ADD_MODULE_BUTTON, function() {
      var $source = $(this),
        cover = $source.closest(SELECTOR_COVER);

      // Add the selected component to the active column
      $this.backendPlugin(pluginName, 'addModule', cover, ($source.is(':first-child') ? 'prepend' : 'append') );
    });

    // Setup module
    registerEventHandler('click', SELECTOR_SETUP_MODULE_BUTTON, function() {
      $this.backendPlugin(pluginName, 'setupModule', this.closest(SELECTOR_MODULE_CONTAINER));
    });

    // Remove module
    registerEventHandler('click', SELECTOR_REMOVE_MODULE_BUTTON, function() {
      $this.backendPlugin(pluginName, 'removeModule', this.closest(SELECTOR_MODULE_CONTAINER));
    });

    // Event for moving modules up/down
    registerEventHandler('click', SELECTOR_MOVE_MODULE_BUTTON, function() {
      $this.backendPlugin(pluginName, 'moveModule', this.closest(SELECTOR_MODULE_CONTAINER), this.data('direction'));
    });

    // Event to search for available articles
    registerEventHandler('change', SELECTOR_ARTICLES_SEARCH_FORM, function() {
      $this.backendPlugin(pluginName, 'searchArticles', this.closest(SELECTOR_MODULE_CONTAINER), this, opts.searchArticlesUrl, opts.articleResultsLimit);
    });

    // Event to search for available articles on model opening
    registerEventHandler('open', SELECTOR_SETUP_MODULE_MODAL, function() {
      $this.backendPlugin(pluginName, 'searchArticles', this.closest(SELECTOR_MODULE_CONTAINER), this, opts.searchArticlesUrl, opts.articleResultsLimit);
    });

    // Event to update available views when the module type is changed
    registerEventHandler('change', VALUE_MODULE_TYPE, function() {
      $this.backendPlugin(pluginName, 'toggleModuleViews', this.closest(SELECTOR_SETUP_MODULE_MODAL), this.val());
    });

    // Event to update available views when the module type is changed
    registerEventHandler('change', VALUE_MODULE_VIEW, function() {
      $this.backendPlugin(pluginName, 'updateModuleDescription', this, this.closest(SELECTOR_SETUP_MODULE_MODAL), this.val());
    });

    // General interaction events
    registerEventHandler('click', SELECTOR_DISMISS_MODAL, function() {
      this.closest(SELECTOR_MODAL).foundation('reveal', 'close');
    });

    // General interaction events
    registerEventHandler('closed', SELECTOR_SETUP_MODULE_MODAL, function() {
      $this.backendPlugin(pluginName, 'setupClosed', this.closest(SELECTOR_MODULE_CONTAINER));
      this.closest(SELECTOR_MODAL).foundation('reveal', 'close');
    });

    // General interaction events
    registerEventHandler('click', SELECTOR_BUTTONS_SUBMIT_FORM, function() {
      var $btn = $(this);
      $this.find(SELECTOR_BUTTONS_SUBMIT_FORM).attr('disabled', true);
      $btn.val($btn.data(DATATTR_SUBMISSION_TEXT));
      $this.find(VALUE_WHOLE_CONFIGURATION).val(
        $this.backendPlugin(pluginName, 'serializeConfiguration')
      );
      $this.find(VALUE_FORM_OPERATION).val($btn.attr('name'));
      $this.find('form').submit();
    });

    // Trigger change behavior on every module type field
    $this.find(VALUE_MODULE_TYPE).trigger('change' + NAMESPACE);
    // When initialization is done, hide the loader and show the editor
    $this.find(SELECTOR_SHOW_ON_LOAD).removeClass(CLASS_SHOW_ON_LOAD);
  };

  // Add a module to a cover
  pluginDef.addModule = function(cover, addPlacement) {
    var afterCb = function(child) {
        cachedChild = child;
        // Copy the cover description to the settings modal for the new module
        child.find(DISPLAY_COVER_DESCRIPTION).html(cover.find(CONST_COVER_DESCRIPTION).html());
      },
      cachedChild, result;
    result = addChildElement(this, cover.find(SELECTOR_MODULES_CONTAINER), SELECTOR_TEMPLATE_MODULE, 'module', addPlacement, afterCb);
    // Trigger a change event on its fields to update dependent elements
    cachedChild.find(VALUE_ARTICLE_VIEW_USABLE).trigger('change');
    cachedChild.find(VALUE_MODULE_TYPE).trigger('change');
    return result;
  };
  // Pop the setup modal for a module
  pluginDef.setupModule = function(mod) {
    var $this = this,
      modal = mod.find(SELECTOR_SETUP_MODULE_MODAL);

    if (!modal.data(DATATTR_SORTABLE_INITIALIZED)) {
      modal.find(SELECTOR_SORTABLE)
        .disableSelection()
        .sortable({
          tolerance: 'pointer',
          items: '.article',
          connectWith: '.sortable',
          placeholder: 'placeholder',
          forcePlaceholderSize: true
        });
      // Initialize any JS plugin in the dropped element
      modal.find(SELECTOR_USED_ARTICLES).on('sortreceive', function(e, ui) {
        ui.item.backendPlugin('initializer');
        $this.backendPlugin(pluginName, 'toggleModuleViews', modal, modal.find(VALUE_MODULE_TYPE).val());
      });
      modal.find(SELECTOR_USED_ARTICLES).on('sortupdate', function(e, ui) {
        $this.backendPlugin(pluginName, 'updateMainArticle', mod, this);
      });
      modal.find(SELECTOR_USED_ARTICLES).on('click', DELETE_ARTICLE_ACTION, function(e, ui) {
        $this.backendPlugin(pluginName, 'removeArticleFromCoverList', mod, this);
      });
      modal.data(DATATTR_SORTABLE_INITIALIZED, true);
    }
    modal.foundation('reveal', 'open');
    return $this;
  };
  // Move a module in the given `direction` (either 'up' or 'down').
  pluginDef.moveModule = function(mod, direction) {
    moveElement(mod, direction, SELECTOR_MODULE_CONTAINER, mod.closest(SELECTOR_COVER), 'module');
    return this;
  };
  // Remove a module from its containing cover
  pluginDef.removeModule = function(mod) {
    removeElement(mod, mod.closest(SELECTOR_COVER), 'module');
    return this;
  };

  // Update display values on the module overview when the setup modal is closed
  pluginDef.setupClosed = function(mod) {
    var moduleType = mod.find(VALUE_MODULE_TYPE).find('option:selected').text();
    mod.find(DISPLAY_ARTICLES_COUNT).html(mod.find(SELECTOR_USED_ARTICLE).length);
    mod.find(DISPLAY_MODULE_DESCRIPTION).html(mod.find(VALUE_MODULE_DESCRIPTION).val());
    if (moduleType != '') {
      mod.find(DISPLAY_MODULE_TYPE).html(moduleType);
    }
    mod.find(DISPLAY_MODULE_VIEW).html(mod.find(VALUE_MODULE_VIEW_USABLE).find('option:selected').text());
    return this;
  };

  // Change module description on layout change
  pluginDef.updateModuleDescription = function(input, modal) {
    modal.find(VALUE_MODULE_DESCRIPTION).val(input.find('option:selected').text());
    return this;
  };

  // Change main article title in module when there are changes in article sort list
  pluginDef.updateMainArticle = function(mod, list) {
    var mainArticleTitle = $(list).children().first().find('.article-title').html()
    mod.find(DISPLAY_MAIN_ARTICLE).html(mainArticleTitle);
  };

  pluginDef.removeArticleFromCoverList = function($mod, a) {
    $(a).closest(SELECTOR_USED_ARTICLE).remove();
  }

  function toggleArticleSearchingVisualFeedback($module, on) {
    var $searching = $module.find(SELECTOR_ARTICLES_SEARCHING_BOX);
    var $results = $module.find(SELECTOR_ARTICLES_RESULTS);
    var $livefilter = $module.find(SELECTOR_ARTICLES_LIVEFILTER);

    if (on) {
      $searching.css('display', 'block');
      $livefilter.css('display', 'none');
      $results.css('filter', 'blur(5px)');
      $results.css('opacity', '0.5');
    } else {
      $searching.css('display', 'none');
      $livefilter.css('display', 'block');
      $results.css('filter', 'none');
      $results.css('opacity', '1');
    }
  }

  // Articles search for module configuration modal
  pluginDef.searchArticles = function(mod, form, searchUrl, limit) {
    var $this = this,
      results = mod.find(SELECTOR_ARTICLES_RESULTS),
      filters = {
        edition_date: form.find('input.edition-date').val(),
        section_id: form.find('select.section-id').val(),
        limit: limit
      };

    toggleArticleSearchingVisualFeedback(mod, true);

    $.get(searchUrl, filters, function(response) {
      results.html(response);
// RESTRICTION DISABLED -> $this.backendPlugin(pluginName, 'removeArticlesInUse', results);
      mod.find(SELECTOR_ARTICLES_LIVEFILTER).backendPlugin('livefilter', 'reload');
    })
      .done(function() { toggleArticleSearchingVisualFeedback(mod, false) });

    return this;
  };

  // Removes articles that are in use in the module from the results list
  pluginDef.removeArticlesInUse = function(results) {
    var usedArticlesSelector = $.map(this.backendPlugin(pluginName, 'getArticlesInUse'), function(e) {
                                 return '[data-id="' + $(e).data('id') + '"]';
                               }).join(',');
    results.find(usedArticlesSelector).remove();
    return this;
  };

  pluginDef.getArticlesInUse = function() {
    return this.find(SELECTOR_USED_ARTICLE);
  };

  pluginDef.serializeConfiguration = function() {
    var $this = this,
      configuration = {};

    $this.find(SELECTOR_COVER).each(function(i, e) {
      var $cover = $(this),
        coverConfiguration = { modules: {}, new: {} };

      $cover.find(SELECTOR_MODULE_CONTAINER).each(function(j, e) {
        var $mod = $(this),
          rawId = $mod.data('module-id'),
          id, parent;

        if (typeof rawId == 'number') {
          id = rawId;
          parent = coverConfiguration.modules;
        } else {
          id = j;
          parent = coverConfiguration.new;
        }

        parent[id] = {
          order: j,
          type_id: $mod.find(VALUE_MODULE_TYPE).val(),
          view_id: $mod.find(VALUE_MODULE_VIEW_USABLE).val(),
          description: $mod.find(VALUE_MODULE_DESCRIPTION).val(),
          articles: $.map($mod.find(SELECTOR_USED_ARTICLE), function(a) {
            return {
              id: $(a).data('id'),
              view_id: $(a).find(VALUE_ARTICLE_VIEW_USABLE).val()
            };
          })
        };
      });

      configuration[$cover.data('cover-id')] = coverConfiguration;
    });

    return JSON.stringify(configuration);
  };

  pluginDef.toggleModuleViews = function(settingsModal, moduleTypeId) {
    var selector = '.show-for-type-' + moduleTypeId;

    settingsModal
      .find(SELECTOR_TYPE_DEPENDENT)
        .hide()
        .attr('disabled', true)
      .end()
      .find(selector)
        .removeAttr('disabled')
        .show()
        .trigger('change');
    return this;
  };

  // Destroy this plugin
  pluginDef.destroy = function() {
    this.off(NAMESPACE);
  };

  $.backendPlugin(pluginName, pluginDef);
})(jQuery);
