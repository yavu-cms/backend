;(function($) {
  'use strict';

  /**
   * Client applications details page ad-hoc logic.
   */

  var pluginName = 'clientapplicationsdetails',
    clientApplicationsDetails = {},
    // Plugin-specific constants
    NAMESPACE        = '.' + pluginName,
    DURATION         = 250,
    SELECTOR_DETAILS = '.details',
    SELECTOR_TOGGLE  = '.details-toggle',
    SELECTOR_ITEM    = 'li';

  // Plugin initializer
  clientApplicationsDetails.init = function() {
    var $this = $(this),
      details = $this.find(SELECTOR_DETAILS),
      visible = $this.find(SELECTOR_DETAILS + ':first-of-type').is(':visible');

    $this.on('click' + NAMESPACE, SELECTOR_TOGGLE, function(e) {
      e.preventDefault();

      if (visible) {
        details.slideUp(DURATION);
      } else {
        details.slideDown(DURATION);
      }

      visible = !visible;

      return false;
    });

    $this.on('click' + NAMESPACE, SELECTOR_ITEM, function(e) {
      e.preventDefault();

      $(this).find(SELECTOR_DETAILS).slideToggle(DURATION);

      return false;
    });
  };

  // Plugin destroyer
  clientApplicationsDetails.destroy = function() {
    var $this = $(this);

    $this.off(NAMESPACE);
  };

  // Plugin definition
  $.backendPlugin(pluginName, clientApplicationsDetails);
})(jQuery);
