/**
 *= require jquery.swap.js
 *= require_self
 */

;(function($, undefined) {
  var pluginName = 'representationeditor',
    pluginDef = {},
    defaults = {
      componentFormUrl: '#missing'
    },
    // Constants
    NAMESPACE                              = '.' + pluginName,
    DATATTR_ACTIVE_COLUMN                  = pluginName + '-current-column',
    DATATTR_SUBMISSION_TEXT                = 'submission-text',
    DATATTR_SUBMITTED_SUCCESS_TEXT         = 'success-text',
    DATATTR_SUBMITTED_ERROR_TEXT           = 'error-text',
    DATATTR_REGULAR_TEXT                   = 'regular-text',
    ANIMATION_SPEED                        = 'fast',
    // Element selectors
    SELECTOR_CANVAS                        = '.canvas',
    SELECTOR_PALETTE                       = '#palette',
    SELECTOR_ROW                           = '.r-row',
    SELECTOR_COLUMNS_CONTAINER             = '.cols-container',
    SELECTOR_COLUMN                        = '.r-col',
    SELECTOR_COMPONENTS_CONTAINER          = '.components-container',
    SELECTOR_COMPONENT                     = '.r-component',
    SELECTOR_COMPONENT_FORM                = '.form-container',
    SELECTOR_COMPONENT_TYPE                = '.component-type',
    SELECTOR_COMPONENT_DESCRIPTION         = 'span.component-description',
    SELECTOR_REPRESENTATION_FORM           = '#representation_form',
    // Row interactions
    SELECTOR_ADD_ROW_BUTTON                = '.add-row',
    SELECTOR_MOVE_ROW_BUTTON               = '.move-row',
    SELECTOR_SETUP_ROW_BUTTON              = '.setup-row',
    SELECTOR_SETUP_ROW_MODAL               = '.setup-row-modal',
    SELECTOR_REMOVE_ROW_BUTTON             = '.remove-row',
    // Column interactions
    SELECTOR_ADD_COLUMN_BUTTON             = '.add-col',
    SELECTOR_MOVE_COLUMN_BUTTON            = '.move-col',
    SELECTOR_SETUP_COLUMN_BUTTON           = '.setup-col',
    SELECTOR_SETUP_COLUMN_MODAL            = '.setup-col-modal',
    SELECTOR_REMOVE_COLUMN_BUTTON          = '.remove-col',
    // Component interactions
    SELECTOR_POP_PALETTE_BUTTON            = '.pop-palette',
    SELECTOR_ADD_COMPONENT_BUTTON          = '.add-component',
    SELECTOR_SETUP_COMPONENT_BUTTON        = '.setup-component',
    SELECTOR_SETUP_COMPONENT_MODAL         = '.setup-component-modal',
    SELECTOR_MOVE_COMPONENT_BUTTON         = '.move-component',
    SELECTOR_REMOVE_COMPONENT_BUTTON       = '.remove-component',
    // General interactions
    SELECTOR_MODAL                         = '.reveal-modal',
    SELECTOR_DISMISS_MODAL                 = '.dismiss-modal',
    SELECTOR_FORM                          = 'form.edit_representation',
    // Template selectors
    SELECTOR_TEMPLATE_ROW                  = '#template-row',
    SELECTOR_TEMPLATE_COLUMN               = '#template-column',
    SELECTOR_TEMPLATE_COMPONENT            = '#template-component',
    // Configurations selectors
    SELECTOR_CONFIGURATION_ROW             = '.r-row-configuration',
    SELECTOR_CONFIGURATION_COLUMN          = '.r-col-configuration',
    SELECTOR_CONFIGURATION_COMPONENT       = '.r-component-configuration',
    SELECTOR_CONFIGURATION_REPRESENTATION  = '.r-representation-configuration',
    // Inputs selectors
    SELECTOR_INPUT_LISTEN_CHANGE           = '.listen-change',
    SELECTOR_DEFAULT_VIEWS                 = '.r-views-configuration',
    SELECTOR_VIEW_COLUMN                   = '.r-col-view',
    SELECTOR_VIEW_ROW                      = '.r-row-view',
    SELECTOR_COMPONENT_FIELD               = ':input[name]',
    // Value selectors
    VALUE_REPRESENTATION_CONFIGURATION     = '#representation_configuration',
    VALUE_REPRESENTATION_OPERATION         = '#representation_operation',
    VALUE_COMPONENT_DESCRIPTION            = 'input.component-description',
    // Buttons selectors
    SELECTOR_BUTTONS_SUBMIT_FORM           = '#save-draft, #apply-draft',
    CLASS_SHOW_ON_LOAD                     = 'show-on-load',
    CLASS_SUBMISSION_ERROR                 = 'alert',
    SELECTOR_SHOW_ON_LOAD                  = '.' + CLASS_SHOW_ON_LOAD,
    SELECTOR_HIDEABLE_EDITOR               = '.hideable-editor',
    SELECTOR_LOADER                        = '.loader';

  // Define and serialize method to return a hash with { key: value } format
  $.fn.serializeObject = function() {
    var o = {_id: this.data('id')};
    // Iterate over all inputs from component configuration setup
    this.find(SELECTOR_COMPONENT_FIELD).each(function() {
      var $input = $(this),
        name = $input.attr('name'),
        value = $input.val() || '',
        // Use leafParent to hold the parent node to insert the new values,
        // allowing make structure like { hash: { key1: val1, key2: val2 } }
        leafParent = o,
        leafName = name;

      // In cases that the input is a checkbox this set value like
      // the property of checked
      if ($input.is(':checkbox')) {
        value = $input.is(':checked');
      }

      // In cases that the input has the format val[key]
      // this part iterate over name and make the structure of hash { val: { key: value } }
      if (name.indexOf('[') > 0)
      {
        // parts match with the names of each key in the name an return an array
        // eg. 'a[b][c]' -> ['a', '[b]', '[c]']
        var parts = name.match(/\[?\w+\]?/g);
        $.each(parts, function(index, e) {
          // Remove brackets from index, eg. '[a]' -> 'a'
          leafName = this.replace(/\[/, '').replace(/\]/, '');
          if (index < parts.length - 1 && typeof leafParent[leafName] == 'undefined') {
            leafParent[leafName] = {};
            leafParent = leafParent[leafName];
          } else if (leafParent[leafName]) {
            leafParent = leafParent[leafName];
          }
        });
      }

      leafParent[leafName] = value;
    });

    return o;
  };

  // Create an element using the template identified by `templateId` within `container`
  function createElement(container, templateId) {
    return $(container.find(templateId).html());
  }

  function markAsDirty(pivot) {
    pivot.trigger('ua:dirty');
  }

  // Add a child element to a container
  function addChildElement(container, parent, templateId, type, action, afterCreationCb) {
    var child = createElement(container, templateId);
    // If an after-creation callback was provided, invoke it passing the new child element to it
    if (typeof afterCreationCb == 'function') {
      afterCreationCb(child);
    }
    // Add the the child element
    $.fn[action].call(parent, child);
    child.backendPlugin('initializer');
    markAsDirty(container);
    return container;
  }

  // Move an element before/after a sibling
  function moveElement(pivot, direction, siblingSelector, parent, type, previousDirectionValue) {
    var usePrevious = direction == (previousDirectionValue || 'up'),
      traversalMethod = usePrevious ? 'prev' : 'next',
      sibling = $.fn[traversalMethod].call(pivot, siblingSelector);

    if (sibling.length > 0) {
      pivot.swap(sibling);
      markAsDirty(pivot);
    }
  }

  function removeElement(element, parent, type) {
    element.fadeOut(ANIMATION_SPEED, function() {
      markAsDirty(element);
      element.remove();
    });
  }

  // Serialization functions

  // Serialize a row
  function serializeRow(row) {
    var $row = $(row),
      conf = {_id: $row.data('id')};

    conf['configuration'] = $row.find(SELECTOR_CONFIGURATION_ROW).serializeObject() || {};
    conf['view'] = $row.find(SELECTOR_VIEW_ROW).serializeObject() || {};
    conf['columns'] = [];

    $row.find(SELECTOR_COLUMN).each(function(i, col) {
      conf['columns'].push(serializeColumn(col));
    });

    return conf
  }

  // Serialize a column
  function serializeColumn(column) {
    var $column = $(column),
      conf = {_id: $column.data('id')};

    conf['configuration'] = $column.find(SELECTOR_CONFIGURATION_COLUMN).serializeObject() || {};
    conf['view'] = $column.find(SELECTOR_VIEW_COLUMN).serializeObject() || {};
    conf['component_configurations'] = [];

    $column.find(SELECTOR_COMPONENT).each(function(i, component) {
      conf['component_configurations'].push(serializeComponent(component));
    });

    return conf
  }

  // Serialize a component
  function serializeComponent(component) {
    return $(component).find(SELECTOR_CONFIGURATION_COMPONENT).serializeObject() || {};
  }

  // Changes the text of a button (input or anchor) to the text in the data attribute `dataAttr`
  function changeButtonText(button, dataAttr) {
    var meth = button.is(':input') ? 'val' : 'text';
    $.fn[meth].apply(button, [button.data(dataAttr)]);
  }

  function buildFlashAlert(message, type, parent, duration) {
    var alert = $('<div class="alert-box"><span class="message"></span><a href="#" class="close">&times;</a></div>');
    message = typeof message == 'string' ? message : message.join('<br/>');
    parent.find('.alert-box').remove();
    alert
      .addClass(type)
      .find('.message')
        .html(message)
      .end()
      .prependTo(parent);
    if (duration) {
      setTimeout(function() {
        alert.fadeOut(ANIMATION_SPEED, function() { alert.remove(); })
      }, duration);
    }
  }

  // Plugin initializer
  pluginDef.init = function(options) {
    var $this = this,
      opts = $.extend({}, defaults, options);

    // Utility function to declare general-purpose user event handlers
    function registerEventHandler(eventType, selector, handler) {
      $this.on(eventType + NAMESPACE, selector, function(e) {
        var $target = $(this);

        if ($(':animated').length > 0) { return false; }

        e.preventDefault();

        // Ask for confirmation before proceeding if a [data-ask] attribute is present
        if ($target.data('ask') && !confirm($target.data('ask'))) {
            return false;
        }
        handler.call($target);
      });
    }

    // Generate the component form url for the given `componentId`
    function componentFormUrlFor(componentId) {
      return opts.componentFormUrl.replace(':id', componentId);
    }

    // Row-related events
    // Add a row (prepend/append)
    registerEventHandler('click', SELECTOR_ADD_ROW_BUTTON, function() {
      $this.backendPlugin(pluginName, 'addRow', this.data('action'));
    });
    // Move row up/down
    registerEventHandler('click', SELECTOR_MOVE_ROW_BUTTON, function() {
      $this.backendPlugin(pluginName, 'moveRow', this.closest(SELECTOR_ROW), this.data('direction'));
    });
    // Setup row
    registerEventHandler('click', SELECTOR_SETUP_ROW_BUTTON, function() {
      $this.backendPlugin(pluginName, 'setupRow', this.closest(SELECTOR_ROW));
    });
    // Remove row
    registerEventHandler('click', SELECTOR_REMOVE_ROW_BUTTON, function() {
      $this.backendPlugin(pluginName, 'removeRow', this.closest(SELECTOR_ROW));
    });

    // Column-related events
    // Add column (prepend/append)
    registerEventHandler('click', SELECTOR_ADD_COLUMN_BUTTON, function() {
      $this.backendPlugin(pluginName, 'addColumn', this.closest(SELECTOR_ROW), this.data('action'));
    });
    // Move row up/down
    registerEventHandler('click', SELECTOR_MOVE_COLUMN_BUTTON, function() {
      $this.backendPlugin(pluginName, 'moveColumn', this.closest(SELECTOR_COLUMN), this.data('direction'));
    });
    // Setup column
    registerEventHandler('click', SELECTOR_SETUP_COLUMN_BUTTON, function() {
      $this.backendPlugin(pluginName, 'setupColumn', this.closest(SELECTOR_COLUMN));
    });
    // Remove column
    registerEventHandler('click', SELECTOR_REMOVE_COLUMN_BUTTON, function() {
      $this.backendPlugin(pluginName, 'removeColumn', this.closest(SELECTOR_COLUMN));
    });

    // Event for popping the palette modal
    registerEventHandler('click', SELECTOR_POP_PALETTE_BUTTON, function() {
      $this.backendPlugin(pluginName, 'activeColumn', this.closest(SELECTOR_COLUMN));
    });

    // Event for updating component configuration description
    registerEventHandler('change', SELECTOR_INPUT_LISTEN_CHANGE, function() {
      var $input = $(this),
          parent = $input.closest(SELECTOR_COMPONENT);
      parent.find(SELECTOR_COMPONENT_DESCRIPTION)
        .text($input.find(':input').val());
    });

    // Event for adding components
    registerEventHandler('click', SELECTOR_ADD_COMPONENT_BUTTON, function() {
      var $source = $(this),
        componentData = $source.data('component'),
        activeColumn = $this.backendPlugin(pluginName, 'activeColumn');

      // if component button is disabled, don't try to process it
      if($source.hasClass('disabled')) { return; }

      function callback(component) {
        component
          .data('component-id', componentData.id)
          .find(SELECTOR_COMPONENT_TYPE)
            .text(componentData.name);

        // Initialize backend plugins after retrieve the new form
        $.get(componentFormUrlFor(componentData.id), function(html) {
          component
            .find(SELECTOR_COMPONENT_FORM)
              .replaceWith(html)
            .end()
            .find(VALUE_COMPONENT_DESCRIPTION)
              .val(component.find(SELECTOR_COMPONENT_DESCRIPTION).text().trim())
            .end()
            .backendPlugin('initializer');
        });
      }

      // Add the selected component to the active column
      $this.backendPlugin(pluginName, 'addComponent', activeColumn, callback);

      // Close the modal after click
      $source.closest(SELECTOR_PALETTE).foundation('reveal', 'close');
    });

    // Setup component
    registerEventHandler('click', SELECTOR_SETUP_COMPONENT_BUTTON, function() {
      $this.backendPlugin(pluginName, 'setupComponent', this.closest(SELECTOR_COMPONENT));
    });

    // Event for moving components up/down
    registerEventHandler('click', SELECTOR_MOVE_COMPONENT_BUTTON, function() {
      $this.backendPlugin(pluginName, 'moveComponent', this.closest(SELECTOR_COMPONENT), this.data('direction'));
    });

    // Event for removing components
    registerEventHandler('click', SELECTOR_REMOVE_COMPONENT_BUTTON, function() {
      $this.backendPlugin(pluginName, 'removeComponent', this.closest(SELECTOR_COMPONENT));
    });

    // General interaction events
    registerEventHandler('click', SELECTOR_DISMISS_MODAL, function() {
      this.closest(SELECTOR_MODAL).foundation('reveal', 'close');
    });

    // General interaction events
    registerEventHandler('click', SELECTOR_BUTTONS_SUBMIT_FORM, function() {
      $this.backendPlugin(pluginName, 'submit', $(this));
    });

    // Set event handler for representation configuration modal
    // to refresh code mirror instances. we love you code mirror!
    $(SELECTOR_REPRESENTATION_FORM).on('opened', function(){
      $(SELECTOR_DEFAULT_VIEWS).find('.js-plugin-codemirror').each(function(i, elem) {
        $(elem).data('cm-instance').refresh();
      });
    });

    $this.find(SELECTOR_LOADER).fadeOut(ANIMATION_SPEED / 2);
    $this.find(SELECTOR_SHOW_ON_LOAD).removeClass(CLASS_SHOW_ON_LOAD);
  };

  // Add a row to the canvas. The `action` should either be 'prepend' or 'append'.
  pluginDef.addRow = function(action) {
    return addChildElement(this, this.find(SELECTOR_CANVAS), SELECTOR_TEMPLATE_ROW, 'row', action);
  };
  // Pop the setup modal for a row
  pluginDef.setupRow = function(row) {
    var $modal = row.find(SELECTOR_SETUP_ROW_MODAL);
    $modal.on('opened', function() {
      var codeMirrorInstance = $modal.find('.js-plugin-codemirror').data('cm-instance');
      if (typeof codeMirrorInstance !== 'undefined') {
        codeMirrorInstance.refresh();
      }
    });
    $modal.foundation('reveal', 'open');
    return this;
  };
  // Move a row 'up' or 'down'
  pluginDef.moveRow = function(row, direction) {
    moveElement(row, direction, SELECTOR_ROW, this.find(SELECTOR_CANVAS), 'row');
    return this;
  };
  // Remove a row from the canvas
  pluginDef.removeRow = function(row) {
    removeElement(row, this.find(SELECTOR_CANVAS), 'row');
    return this;
  };

  // Add a column to a row. The `action` should either be 'prepend' or 'append'.
  pluginDef.addColumn = function(row, action) {
    return addChildElement(this, row.find(SELECTOR_COLUMNS_CONTAINER), SELECTOR_TEMPLATE_COLUMN, 'column', action);
  };
  // Move a column 'left' or 'right'
  pluginDef.moveColumn = function(column, direction) {
    moveElement(column, direction, SELECTOR_COLUMN, column.closest(SELECTOR_ROW), 'column', 'left');
    return this;
  };
  // Pop the setup modal for a column
  pluginDef.setupColumn = function(column) {
    $modal = column.find(SELECTOR_SETUP_COLUMN_MODAL)
    $modal.on('opened', function(){
      $modal.find('.js-plugin-codemirror').data('cm-instance').refresh();
    });
    $modal.foundation('reveal', 'open');

    return this;
  };
  // Remove a column from its row
  pluginDef.removeColumn = function(column) {
    removeElement(column, column.closest(SELECTOR_ROW), 'column');
    return this;
  };

  // Add a component to a column
  pluginDef.addComponent = function(column, afterCreationCb) {
    return addChildElement(this, column.find(SELECTOR_COMPONENTS_CONTAINER), SELECTOR_TEMPLATE_COMPONENT, 'component', 'append', afterCreationCb);
  };
  // Pop the setup modal for a component
  pluginDef.setupComponent = function(component) {
    component.find(SELECTOR_SETUP_COMPONENT_MODAL).foundation('reveal', 'open');
    return this;
  };
  // Move a component in the given `direction` (either 'up' or 'down').
  pluginDef.moveComponent = function(component, direction) {
    moveElement(component, direction, SELECTOR_COMPONENT, component.closest(SELECTOR_COLUMN), 'component');
    return this;
  };

  // Remove a component from its containing column
  pluginDef.removeComponent = function(component) {
    removeElement(component, component.closest(SELECTOR_COLUMN), 'component');
    return this;
  };

  // Manage a reference to the currently active column.
  // This method can be used in 3 different ways:
  //   - As a getter: with no arguments         -> $el.backendPlugin(pluginName, 'activeColumn')
  //   - As a setter: with a non-false argument -> $el.backendPlugin(pluginName, 'activeColumn', false)
  //   - As a remover: with a false argument    -> $el.backendPlugin(pluginName, 'activeColumn', $column)
  pluginDef.activeColumn = function(column) {
    if (arguments.length == 0 || column == undefined) {
      // Getter - $el.backendPlugin(pluginName, 'activeColumn')
      return this.data(DATATTR_ACTIVE_COLUMN);
    } else if (column == false) {
      // Remover - $el.backendPlugin(pluginName, 'activeColumn', false)
      return this.removeData(DATATTR_ACTIVE_COLUMN);
    } else {
      // Setter - $el.backendPlugin(pluginName, 'activeColumn', $column)
      return this.data(DATATTR_ACTIVE_COLUMN, column)
    }
  };

  pluginDef.submit = function(pressedButton) {
    var $this = this,
      $form = $this.find(SELECTOR_FORM),
      $buttons = $this.find(SELECTOR_BUTTONS_SUBMIT_FORM).attr('disabled', true);

    $this.find(SELECTOR_HIDEABLE_EDITOR).addClass(CLASS_SHOW_ON_LOAD);
    $this.find(SELECTOR_LOADER).fadeIn(ANIMATION_SPEED / 2);
    // Process and submit the configuration
    changeButtonText(pressedButton, DATATTR_SUBMISSION_TEXT);
    $this.find(VALUE_REPRESENTATION_OPERATION).val(pressedButton.attr('name'));
    $this.find(VALUE_REPRESENTATION_CONFIGURATION).val(
      $this.backendPlugin(pluginName, 'serializeConfiguration')
    );

    $.post($form.attr('action'), $form.serializeArray(), 'json')
      .always(function() {
        setTimeout(function() {
          pressedButton.removeClass(CLASS_SUBMISSION_ERROR);
          changeButtonText(pressedButton, DATATTR_REGULAR_TEXT);
        }, 3000);
        $buttons.attr('disabled', false);
        $this.find(SELECTOR_LOADER).fadeOut(ANIMATION_SPEED / 2);
        $this.find(SELECTOR_HIDEABLE_EDITOR).removeClass(CLASS_SHOW_ON_LOAD);
      })
      .done(function(data) {
        if (data.redirect && data.redirect !== '') {
          window.location.href = data.redirect;
        } else {
          $form.find('#representation_lock_version').val(data.lock_version);
          changeButtonText(pressedButton, DATATTR_SUBMITTED_SUCCESS_TEXT);
          buildFlashAlert(data.notice, 'success', $form, 1500);
        }
      })
      .fail(function(data) {
        var msg;
        try {
          msg = [data.responseJSON.alert, data.responseJSON.failures];
        } catch (e) {
          msg = ['Internal server error'];
          console.error('Unknown error when trying to submit form', data);
        }
        pressedButton.addClass(CLASS_SUBMISSION_ERROR);
        changeButtonText(pressedButton, DATATTR_SUBMITTED_ERROR_TEXT);
        buildFlashAlert(msg, 'alert', $form);
        if (data.responseJSON.lock_version) {
          $this.find('#representation_lock_version').val(data.responseJSON.lock_version);
        }
      });
  };

  // Serialize configuration
  pluginDef.serializeConfiguration = function() {
    var conf = {};
    conf['representation'] = {};
    conf['representation']['configuration'] = this.find(SELECTOR_CONFIGURATION_REPRESENTATION).serializeObject() || {};
    conf['representation']['views'] = this.find(SELECTOR_DEFAULT_VIEWS).serializeObject() || {};
    conf['representation']['rows'] = [];
    this.find(SELECTOR_ROW).each(function(i, row) {
      conf['representation']['rows'].push(serializeRow(row));
    });
    return JSON.stringify(conf);
  };

  // Destroy this plugin
  pluginDef.destroy = function() {
    this.off(NAMESPACE);
  };

  $.backendPlugin(pluginName, pluginDef);
})(jQuery);
