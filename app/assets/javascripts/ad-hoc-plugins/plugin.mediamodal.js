;(function($) {
  'use strict';

  /**
   * Ad-hoc logic for Media management modal for article elements.
   */

  var pluginName = 'mediamodal',
    mediaModal   = {},
    defaults     = {
      checkEmptynessOn: '.selected-media-pane'
    },
    // Constants
    NAMESPACE                    = '.' + pluginName,
    OPTIONS_ATTR                 = pluginName + '-opts',
    SELECTOR_MEDIUM              = '.medium',
    CLASS_MAIN_MEDIUM            = 'main-medium',
    SELECTOR_SAVE_MODAL          = '#save-data-modal',
    SELECTOR_CLOSE_MODAL         = '#close-modal',
    SELECTOR_MAIN_MEDIUM         = '.' + CLASS_MAIN_MEDIUM,
    SELECTOR_SET_MAIN_MEDIUM     = '.set-main-medium',
    SELECTOR_EMPTYNESS_INDICATOR = '.emptyness-item',
    SELECTOR_ADD_MEDIUM          = '.add-medium',
    SELECTOR_REMOVE_MEDIUM       = '.remove_nested_fields',
    SELECTOR_ADD_EMPTY_MEDIUM    = '#add_nested_fields',
    SELECTOR_DROPZONE_FORM       = '#form-dropzone-modal',
    SELECTOR_RELATED             = '.selected-media-pane',
    SELECTOR_SEARCH_RESULTS      = '#search-results-container',
    CLASS_MEDIUM_IN_USE          = 'in-use',
    SELECTOR_MEDIUM_IN_USE       = '.' + CLASS_MEDIUM_IN_USE,
    SELECTOR_SEARCH_FORM         = '#search-media-form',
    SELECTOR_SEARCH_BUTTON       = '#search_media_button',
    SELECTOR_CREATE_EMBEDDED     = '#create_embedded_button',
    SELECTOR_NEW_MEDIUM_FORM     = '#new_medium',
    SELECTOR_FLASH_SUCCESS       = '#success_alert',
    SELECTOR_FLASH_ERROR_UPLOAD  = '#error_alert_upload',
    SELECTOR_FLASH_ERROR_EMBEDDED= '#error_alert_embedded',
    SELECTOR_FLASH_ERROR_SEARCH  = '#error_alert_search',
    SELECTOR_MAIN_IMAGE_NOTICE   = '#no-media-notice',
    CLASS_SUCCESS_ALERT          = 'alert-box success',
    CLASS_ERROR_ALERT            = 'alert-box alert',
    SELECTOR_PAGINATION_LINKS    = 'ul.pagination a[href!=#]',
    CAPTION_FIELD                = '#article_article_media__caption',
    SELECTOR_RELATED_ORDER       = '.fields:visible .order-field',
    SELECTOR_MEDIUM_TYPE         = '#medium_type';

  // Plugin initializer
  mediaModal.init = function(opts) {
    var $this               = $(this),
      options               = $.extend({}, defaults, opts),
      $container            = $(options.container),
      $related              = $container.find(SELECTOR_RELATED),
      $currentMainMedium    = $container.find(SELECTOR_MAIN_MEDIUM),
      $emptynessCheck       = $container.find(options.checkEmptynessOn),
      $emptynessIndicator   = $container.find(SELECTOR_EMPTYNESS_INDICATOR),
      $dropzoneForm         = $container.find(SELECTOR_DROPZONE_FORM),
      $main_image_alert     = $container.find(SELECTOR_MAIN_IMAGE_NOTICE),
      dropzoneRegistered    = false,
      $searchForm           = $container.find(SELECTOR_SEARCH_FORM),
      $searchFormFields     = $searchForm.find(':input'),
      $searchBtn            = $container.find(SELECTOR_SEARCH_BUTTON),
      $searchResults        = $container.find(SELECTOR_SEARCH_RESULTS),
      $addMediumBtn         = $container.find(SELECTOR_ADD_MEDIUM),
      $newMediumForm        = $container.find(SELECTOR_NEW_MEDIUM_FORM),
      $newMediumFormFields  = $newMediumForm.find(':input'),
      $addEmptyMediumBtn    = $container.find(SELECTOR_ADD_EMPTY_MEDIUM),
      $createEmbedded       = $container.find(SELECTOR_CREATE_EMBEDDED),
      $captionField         = $container.find(CAPTION_FIELD),
      $mediumType           = $dropzoneForm.find(SELECTOR_MEDIUM_TYPE),
      $embeddedFlashError   = $container.find(SELECTOR_FLASH_ERROR_EMBEDDED),
      $embeddedFlashSuccess = $container.find(SELECTOR_FLASH_SUCCESS),
      $uploadFlashError     = $container.find(SELECTOR_FLASH_ERROR_UPLOAD);

    function setMediumField(data) {
      var $newMedium = $related.find('.medium:last');
      // Add field with new embedded medium
      $newMedium.attr('data-medium-id', data.medium_id)
      $newMedium.find('.medium-preview-row').prepend(data.preview);
      $newMedium.find('.caption-field').attr('placeholder', data.caption);
      $newMedium.find('.medium_id-field').val(data.medium_id);
      $newMedium.find('.order-field').val(0);
      sortFields(SELECTOR_RELATED_ORDER);
    }

    function notifyChanges() {
      $(SELECTOR_SAVE_MODAL).removeAttr('disabled');
    }

    // Check if the $emptynessCheck element is empty and show/hide
    // the $emptynessIndicator accordingly.
    function checkEmptyness() {
      if ($emptynessCheck.find('> *').length === 0) {
        $emptynessIndicator.show();
      } else {
        $emptynessIndicator.hide();
      }
    }

    // Set the selection to be the main medium
    function defaultMainMedium() {
      $currentMainMedium
          .addClass(CLASS_MAIN_MEDIUM)
          .find(SELECTOR_SET_MAIN_MEDIUM)
          .attr('checked', true);
    }

    // Handle the addition of a new medium from the search results.
    function handleAddMedium(e) {
      var $btn = $(this);

      $.get($btn.data('medium-url'), {article_id: options.articleId}, function(response) {
        setMediumField(response);
        setUsedMediaFromResult($searchResults);
      });
    }

    function handleRemoveMedium(e) {
      var $btn = $(this),
        medium = $btn.closest(SELECTOR_MEDIUM);

      unsetUsedMediaFormResult($searchResults, medium.data('medium-id'));
    }

    function setUsedMediaFromResult(result) {
      var selector = $related.find(SELECTOR_MEDIUM + ':visible').map(function() {
          return '.medium-' + $(this).data('medium-id');
        }).toArray().join(', ');
      result.find(selector).map (function() { 
        $(this).addClass(CLASS_MEDIUM_IN_USE);
      })
    }

    function unsetUsedMediaFormResult(result, mediumId) {
      result.find('.medium-' + mediumId).removeClass(CLASS_MEDIUM_IN_USE);
    }

    function handleSearchMedia(e) {
      e.preventDefault();

      // Empty the search results section
      $searchResults.empty();
      $searchBtn.attr('disabled', true);

      // Perform the search and update the results section
      $.get($searchBtn.attr('href'), $searchFormFields.serializeArray(), function(response) {
        $searchResults.html(response);
        setUsedMediaFromResult($searchResults);
        $searchBtn.removeAttr('disabled');
      });

      return false;
    }

    function handlePagination(e) {
      e.preventDefault();
      $searchResults.load($(this).attr('href'), function() { setUsedMediaFromResult($searchResults); });
      return false;
    }

    function handleEmbeddedFormSubmit(e) {

      var $url        = $createEmbedded.data('medium-url'),
        $mediumParams = $newMediumFormFields.serialize() + "&medium[type]=EmbeddedMedium&media_modal=true&article_id=" + options.articleId;

      $.ajax({
        type: "POST",
        dataType: 'json',
        url: $url,
        data: $mediumParams,
          success: function(data) {
            setMediumField(data);
            // Resets fields forms
            $('#medium_name').val("");
            $('#medium_content').val("");
            // Show succes flashes
            $embeddedFlashSuccess.addClass(CLASS_SUCCESS_ALERT).fadeIn(1000, function () {
              $(this).fadeOut(5000);
            });
          },
          error: function(xhr){
            var $newMedium = $related.find('.medium:last'),
              errors = $.parseJSON(xhr.responseText).errors;
            // Remove nested field
            $newMedium.remove();
            // Show errors
            $embeddedFlashError.addClass(CLASS_ERROR_ALERT).append("Se produjeron los siguientes problemas: " + errors);
            

          }
      });
    }

    function sortFields(selector) {
      $related.find(selector).each(function(i, e) {
        $(this).val(i + 1);
      });
    }

    // Store the options for later retrieval
    $this.attr(OPTIONS_ATTR, options);

    // Register event listeners:
    // * Sort fields
    var orderChangedHandler = function() { sortFields(SELECTOR_RELATED_ORDER); };
    $container.on('nested:fieldAdded nested:fieldRemoved', SELECTOR_RELATED, orderChangedHandler);
    
    // * Search results addition
    $container.on('click' + NAMESPACE, SELECTOR_ADD_MEDIUM, handleAddMedium);

    // * Search results subtraction
    $container.on('click' + NAMESPACE, SELECTOR_REMOVE_MEDIUM, handleRemoveMedium);
    // * Media Search
    $searchBtn.on('click' + NAMESPACE, handleSearchMedia)

    // * Embedded form submit
    $createEmbedded.on('click' + NAMESPACE, handleEmbeddedFormSubmit);

    // * Pagination links
    $container.on('click' + NAMESPACE, SELECTOR_PAGINATION_LINKS, handlePagination);
    // Refresh the plugin on rise to display foundation
    $container.on('opened', function () {
      if (!dropzoneRegistered) {
        dropzoneRegistered = true;

        if ($dropzoneForm.length) {
          $dropzoneForm.backendPlugin('dropzone', 'on', 'sending', function(file, xhr, formData) {
            formData.append("media_modal", true)
            formData.append("medium[type]", $mediumType.val());
          });
          $dropzoneForm.backendPlugin('dropzone', 'on', 'success', function(file, data) {
            $addEmptyMediumBtn.click();
            setMediumField(data);
          });
          $dropzoneForm.backendPlugin('dropzone', 'on', 'error', function(file, errors, xhr) {
            errors = $.parseJSON(xhr.responseText).errors;
            $uploadFlashError.addClass(CLASS_ERROR_ALERT).append("Se produjeron los siguientes problemas: " + errors);
            $uploadFlashError.fadeIn(1000, function () {
              $(this).fadeOut(5000, function () {
              $(this).empty();
              });
            });
          });
        }
      }

      $(this).foundation('section', 'reflow');
    });

    $container.on('click' + NAMESPACE, SELECTOR_CLOSE_MODAL, function() {
      $(this).foundation('reveal', 'close');
    });

    $captionField.on('input' + NAMESPACE, notifyChanges);

    checkEmptyness();
    defaultMainMedium();

    $related.sortable({
      axis: 'y',
      update: orderChangedHandler
    });

  };

  // Plugin destructor
  mediaModal.destroy = function() {
    var $this = $(this),
      options = $this.data(OPTIONS_ATTR);

    // Unregister event listeners
    $(options.container).off(NAMESPACE);
    $(SELECTOR_SEARCH_FORM).off(NAMESPACE);

    $this.removeData(OPTIONS_ATTR);
  };

  // Plugin definition
  $.backendPlugin(pluginName, mediaModal);

})(jQuery);
