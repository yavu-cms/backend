;(function($) {
  'use strict';

  var pluginName = 'uniqueaccess',
    uniqueAccess = {},
    // Constants
    CLASS_PARTICIPANTS      = 'ua-participants',
    CLASS_PARTICIPANT       = 'ua-participant',
    CLASS_EMPTY             = 'ua-empty',
    CLASS_LEGEND            = 'ua-legend',
    CLASS_CHANGED           = 'ua-made-changes',
    CLASS_SAVED             = 'ua-saved',
    CLASS_DESTROYED         = 'ua-destroyed',
    CLASS_NOTICE            = 'ua-notice',
    CLASS_CHANGED_NOTICE    = 'ua-notice-made-changes',
    CLASS_SAVED_NOTICE      = 'ua-notice-saved',
    CLASS_DESTROYED_NOTICE  = 'ua-notice-destroyed',
    DATATTR_DIRTY           = pluginName + '-dirty',
    DATATTR_PARTICIPANTS    = pluginName + '-participants',
    DATATTR_CHANGED_TEXT    = pluginName + '-changed-text',
    DATATTR_SAVED_TEXT      = pluginName + '-saved-text',
    DATATTR_DESTROYED_TEXT  = pluginName + '-destroyed-text',
    SELECTOR_PARTICIPANTS   = '.' + CLASS_PARTICIPANTS,
    SELECTOR_NOTICE         = '.' + CLASS_NOTICE;

  function slugify(string) {
    return string.toLowerCase().replace(/[^\w ]+/g,'').replace(/ +/g,'-');
  }

  uniqueAccess.init = function(options) {
    var $this = this,
      client = new Faye.Client(options.url);

    $this.data(DATATTR_PARTICIPANTS, []);
    $this.data(DATATTR_CHANGED_TEXT, options.changedText);
    $this.data(DATATTR_SAVED_TEXT, options.savedText);
    $this.data(DATATTR_DESTROYED_TEXT, options.destroyedText);

    // TODO: FIXME - This ain't working :(
    $(window).on('beforeunload', function() {
      $this.backendPlugin(pluginName, 'publish', client, options.channel, {user: options.user, left: true});
    });

    function dirtyHandler() {
      $this.data(DATATTR_DIRTY, true);
      $this.backendPlugin(pluginName, 'publish', client, options.channel, {user: options.user});
    }

    this
      .backendPlugin(pluginName, 'createParticipantsContainer', options.legend || 'Other users')
      .backendPlugin(pluginName, 'subscribe', client, options.channel, options.user)
      .backendPlugin(pluginName, 'publish', client, options.channel, {user: options.user})
      .on('change', ':input', dirtyHandler)
      .on('ua:dirty', dirtyHandler);
  };

  uniqueAccess.createParticipantsContainer = function(legend) {
    var container = $('<div/>', {class: [CLASS_EMPTY, CLASS_PARTICIPANTS].join(' ')})
                      .append($('<span/>', {class: CLASS_LEGEND, text: legend}));
    return this.prepend(container);
  };

  uniqueAccess.publish = function(client, channel, data) {
    var msg = $.extend({changed: this.data(DATATTR_DIRTY)}, data);
    client.publish(channel, msg)
      .errback(function(error) {
        console.error("Couldn't publish at channel " + channel, error);
      });
    return this;
  };

  uniqueAccess.markUser = function(user, markClass, markNoticeClass, text) {
    var allMarkClasses = [CLASS_SAVED, CLASS_CHANGED, CLASS_DESTROYED].join(' ');
    if (!user.hasClass(markClass)) {
      user
        .removeClass(allMarkClasses)
        .addClass(markClass)
        .find(SELECTOR_NOTICE).remove().end()
        .append($('<span/>', {class: [CLASS_NOTICE, markNoticeClass].join(' '), text: text}));
    }
  };

  uniqueAccess.subscribe = function(client, channel, user) {
    var $this = this,
      $participants = this.find(SELECTOR_PARTICIPANTS),
      participants = this.data(DATATTR_PARTICIPANTS);

    client.subscribe(channel, function(data) {
      var index = $.inArray(data.user, participants),
        userClass = 'ua-' + slugify(data.user);
      if (data.user !== user) {
        if (data.left) {
          if (index >= 0) {
            $participants.find('.' + userClass).remove();
            participants.splice(data.user, 1);
            if (participants.length === 0) {
              $participants.addClass(CLASS_EMPTY);
            }
          }
        } else {
          if (index === -1) {
            $participants
              .append($('<span/>', {class: [CLASS_PARTICIPANT, userClass].join(' '), text: data.user}))
              .removeClass(CLASS_EMPTY);
            participants.push(data.user);
          }

          if (data.changed) {
            $this.backendPlugin(pluginName, 'markUser', $participants.find('.' + userClass), CLASS_CHANGED, CLASS_CHANGED_NOTICE, $this.data(DATATTR_CHANGED_TEXT));
          } else if (data.saved) {
            $this.backendPlugin(pluginName, 'markUser', $participants.find('.' + userClass), CLASS_SAVED, CLASS_SAVED_NOTICE, $this.data(DATATTR_SAVED_TEXT));
          } else if (data.destroyed) {
            $this.backendPlugin(pluginName, 'markUser', $participants.find('.' + userClass), CLASS_DESTROYED, CLASS_DESTROYED_NOTICE, $this.data(DATATTR_DESTROYED_TEXT));
            console.log($this.find(':submit'));
            $this.find(':submit').attr('disabled', true).on('click', function(e) { e.preventDefault(); return false; });
          }

          if (!data.reply && !data.saved) {
            $this.backendPlugin(pluginName, 'publish', client, channel, {user: user, reply: true});
          }
        }
        $this.data(DATATTR_PARTICIPANTS, participants);
      }
    });
    return this;
  };

  $.backendPlugin(pluginName, uniqueAccess);
})(jQuery);

