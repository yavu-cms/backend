;(function($) {
  'use strict';

  /**
   * Ad-hoc plugin for the components form.
   */

  var pluginName = 'componentsform',
    pluginDef = {},
    // Plugin-specific constants
    NAMESPACE                    = '.' + pluginName,
    KEY_SPACE                    = 32,
    SELECTOR_NAME_FIELD          = '.name-field',
    SELECTOR_MODEL_FIELD         = '.model-field',
    SELECTOR_TYPE_FIELD          = '.type-field',
    SELECTOR_TYPE_DEPENDENT      = '.type-dependent',
    SELECTOR_MODEL_DEPENDENT     = '.model-dependent',
    SELECTOR_ROW                 = '.row',
    SELECTOR_FIELD_GROUP         = '.panel',
    SELECTOR_DISPLAY_NAME        = '.display-name',
    SELECTOR_INPUT               = ':input[name*="[value]"]',
    MODEL_FIELD_ACTIVATING_TYPES = /^ComponentField(\w*)Model$/i;

  // Plugin initializer
  pluginDef.init = function() {
    var $this = this;

    $this.on('keyup' + NAMESPACE, SELECTOR_NAME_FIELD, function(e) {
      var $field = $(this);
      $field.closest(SELECTOR_ROW)
        .find(SELECTOR_DISPLAY_NAME).text($field.val());
    });

    $this.on('keypress' + NAMESPACE, SELECTOR_NAME_FIELD, function(e) {
      var key = e.which ? e.which : e.keyCode,
        range;

      // If <space> key was pressed, insert an underscore instead
      if (key == KEY_SPACE) {
        e.preventDefault();
        if (document.selection) {
          range = document.selection.createRange();
          range.text = '_';
        } else if (this.selectionStart || this.selectionStart == '0') {
          range = {start: this.selectionStart, end: this.selectionEnd};
          this.value = this.value.substring(0, range.start) + '_' + this.value.substring(range.end, this.value.length);
          this.selectionStart = this.selectionEnd = range.start + 1;
        } else {
          this.value += '_';
        }
        return false;
      }
    });

    $this.on('nested:fieldAdded' + NAMESPACE, function(e) {
      e.field.backendPlugin('initializer');
      triggerChanges(e.field); });

    $this.on('change' + NAMESPACE, SELECTOR_TYPE_FIELD, function() {
      var $field = $(this),
        dependentSelector = '.show-when-type-' + $field.val(),
        $container = $field.closest(SELECTOR_FIELD_GROUP);

      $container
        .find(SELECTOR_TYPE_DEPENDENT)
          .hide()
          .find(SELECTOR_INPUT)
            .prop('disabled', true)
          .end()
        .end()
        .find(dependentSelector)
          .find(SELECTOR_INPUT)
            .prop('disabled', false)
          .end()
          .show();

      if ($field.val().match(MODEL_FIELD_ACTIVATING_TYPES)) {
        $container.find(SELECTOR_MODEL_FIELD).trigger('change' + NAMESPACE);
      }
    });

    $this.on('change' + NAMESPACE, SELECTOR_MODEL_FIELD, function() {
      var $field = $(this),
        $container = $field.closest(SELECTOR_FIELD_GROUP),
        $typeField = $container.find(SELECTOR_TYPE_FIELD),
        dependentSelector = '.show-when-type-' + $typeField.val() + ' .show-when-model-' + $field.val();

      $container
        .find(SELECTOR_MODEL_DEPENDENT)
          .hide()
          .find(SELECTOR_INPUT)
            .prop('disabled', true)
          .end()
        .end()
        .find(dependentSelector)
          .find(SELECTOR_INPUT)
            .prop('disabled', false)
          .end()
          .show();
    });

    // Trigger initial events
    function triggerChanges(container) {
      container.find(SELECTOR_TYPE_FIELD).trigger('change' + NAMESPACE);
    }

    triggerChanges($this);
  };

  // Plugin destroyer
  pluginDef.destroy = function() {
    this.off(NAMESPACE);
  };

  // Plugin definition
  $.backendPlugin(pluginName, pluginDef);
})(jQuery);
