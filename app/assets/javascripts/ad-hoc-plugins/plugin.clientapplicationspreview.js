;(function($) {
  'use strict';

  /**
   * Client applications preview page ad-hoc logic.
   */

  var pluginName = 'clientapplicationspreview',
    clientApplicationsPreview = {},
    defaults = {message: 'Loading...', previewBaseUrl: '#'},
    // Plugin-specific constants
    NAMESPACE                  = '.' + pluginName,
    ROUTES_SELECTOR_ID         = '#routes',
    PARAMETERS_SELECTORS_CLASS = '.parameters',
    PREVIEW_FORM_ID            = '#preview-form',
    PREVIEW_FRAME_ID           = '#preview-frame',
    SPEED = 250;

  function createOverlay($target, msg) {
    var ov = $('<span class="overlay initially-hidden"><span class="message">' + msg + '</span></span>');
    $target.parent().prepend(ov);
    return ov;
  }

  // Plugin initializer
  clientApplicationsPreview.init = function(opts) {
    var options     = $.extend({}, defaults, opts),
        $this       = this,
        $routes     = $this.find(ROUTES_SELECTOR_ID),
        $parameters = $this.find(PARAMETERS_SELECTORS_CLASS),
        $preview    = $this.find(PREVIEW_FORM_ID),
        $iframe     = $this.find(PREVIEW_FRAME_ID),
        $overlay    = createOverlay($iframe, options.message);

    var toggleFormInputsFor = function($routeParams, action) {
      $routeParams.find(':input').attr('disabled', action == 'disable');
    };

    var hideAllParametersSelectors = function() {
      $parameters.each(function(i, elem) {
        toggleFormInputsFor($(elem), 'disable');
        $(elem).hide();
      });
    };

    var showParametersForSelectedRoute = function() {
      hideAllParametersSelectors();
      $parameters.each(function(i, elem) {
        if($(elem).data('routes').indexOf(parseInt($routes.val())) != -1) {
          toggleFormInputsFor($(elem), 'enable');
          $(elem).show();
        }
      });
    };

    showParametersForSelectedRoute();

    $routes.on('change' + NAMESPACE, function() {
      showParametersForSelectedRoute();
    });

    $preview.on('submit' + NAMESPACE, function() {
      var previewParams = $preview.serialize(),
        previewUrl = options.previewBaseUrl + (options.previewBaseUrl.indexOf('?') == -1 ? '?' : '&') + previewParams;
      $overlay.fadeIn(SPEED);
      $iframe.attr('src', previewUrl);
      return false;
    });

    $iframe.on('load' + NAMESPACE, function() {
      $overlay.fadeOut(SPEED);
    });
  };

  // Plugin destroyer
  clientApplicationsPreview.destroy = function() {
    var $this = $(this);

    $this.off(NAMESPACE);
  };

  // Plugin definition
  $.backendPlugin(pluginName, clientApplicationsPreview);
})(jQuery);
