;(function($) {
  'use strict';

  /**
   * Routes ad-hoc logic.
   */

  var pluginName = 'routesform',
    routesForm = {},
    // Plugin-specific constants
    NAMESPACE = '.' + pluginName,
    SELECTOR_PATTERN_PARAMS_SECTION= '.params-for-',
    SELECTOR_PATTERN_ATTRS_SECTION= '.attrs-for-',
    SELECTOR_PATTERN_CONSTRAINTS_SECTION = '.constraint-for-',
    SELECTOR_PARAMS_CONTAINERS = '.params-container',
    SELECTOR_ATTRS_CONTAINERS = '.attrs-container',
    SELECTOR_TYPE_ID = 'select.type',
    SELECTOR_CONSTRAINTS_TOGGLE = '.constraints-toggle',
    SELECTOR_CONSTRAINTS_CONTAINER = '.constraints-container',
    SELECTOR_CONSTRAINT_CONTAINER = '.constraint-container',
    SELECTOR_ROUTE_API_TIMEOUT_OVERRIDE_TOGGLE = '.route-override-client-application-cache-control-toggle',
    SELECTOR_ROUTE_API_TIMEOUT_OVERRIDE_CONTAINER = '.route-override-client-application-cache-control-container',
    SELECTOR_ROUTE_API_TIMEOUT_FIELD = '.route-override-client-application-cache-control',
    SELECTOR_FIELDS_GROUP = '.form-container',
    SELECTOR_ROUTES_INFO_ID = '#all-routes-priorities',
    SELECTOR_ROUTE_INFO = '.route-priority-info',
    SELECTOR_ROUTE_NEW_POS = '.new-route-position',
    SELECTOR_ROUTE_FLASH = '#route-priority-flash';

  // Plugin initializer
  routesForm.init = function() {
    var $this = $(this);

    $this.on('change' + NAMESPACE, SELECTOR_TYPE_ID, function() {
      var $pivot = $(this),
        $container = $pivot.closest(SELECTOR_FIELDS_GROUP),
        $allParams = $container.find(SELECTOR_PARAMS_CONTAINERS),
        $allAttrs  = $container.find(SELECTOR_ATTRS_CONTAINERS),
        $allConstraints = $container.find(SELECTOR_CONSTRAINT_CONTAINER),
        paramsSelector  = SELECTOR_PATTERN_PARAMS_SECTION + $pivot.val(),
        attrsSelector   = SELECTOR_PATTERN_ATTRS_SECTION + $pivot.val(),
        constraintsSelector = SELECTOR_PATTERN_CONSTRAINTS_SECTION + $pivot.val();

      $allParams.hide();
      $container.find(paramsSelector).show();
      $allAttrs.hide();
      $container.find(attrsSelector).show();
      $allConstraints.hide();
      $container.find(constraintsSelector).show();
    });

    $this.on('change' + NAMESPACE, SELECTOR_CONSTRAINTS_TOGGLE, function() {
      var $pivot = $(this),
        $container = $pivot.closest(SELECTOR_FIELDS_GROUP),
        $toggle = $container.find(SELECTOR_CONSTRAINTS_TOGGLE),
        $constraints = $container.find(SELECTOR_CONSTRAINTS_CONTAINER);

      if ($toggle.is(':checked')) {
        $constraints.show();
        $constraints.find('input').each(function() { $(this).prop('disabled', false); });
      } else {
        $constraints.hide();
        $constraints.find('input').each(function() { $(this).prop('disabled', true); });
      }
    });

    $this.on('change' + NAMESPACE, SELECTOR_ROUTE_API_TIMEOUT_OVERRIDE_TOGGLE, function() {
      var $pivot = $(this),
        $container = $pivot.closest(SELECTOR_FIELDS_GROUP),
        $routeApiOverridesFields = $container.find(SELECTOR_ROUTE_API_TIMEOUT_OVERRIDE_CONTAINER),
        $overrideApiTimeoutField = $routeApiOverridesFields.find(SELECTOR_ROUTE_API_TIMEOUT_FIELD);

        if ($pivot.is(':checked')) {
          $routeApiOverridesFields.show();
          $overrideApiTimeoutField.focus();
        } else {
          $overrideApiTimeoutField.val('');
          $routeApiOverridesFields.hide();
        }
    });

    $(SELECTOR_ROUTES_INFO_ID).on('click' + NAMESPACE, SELECTOR_ROUTE_NEW_POS, function() {
      var $selector = $(this);
      var newPriority, $route;
      if ($selector.hasClass('up')) {
        $route = $selector.next();
        newPriority = 1;
      } else if ($selector.hasClass('down')) {
        $route = $selector.prev();
        newPriority = -1;
      }
      var selectedPriority = parseInt($route.find('.route-priority').html());
      if (!isNaN(selectedPriority)) {
        selectedPriority += newPriority;
        var flash = 'La ruta actual fue configurada con prioridad ' + selectedPriority;
        var $routeFlash = $('#route-priority-flash');
        $routeFlash.html(flash);
        $routeFlash.removeClass('hide-for-medium-up');
        $('#route_priority').val(selectedPriority);
      }
    })

    $this.find(SELECTOR_TYPE_ID).trigger('change');
    $this.find(SELECTOR_CONSTRAINTS_TOGGLE).trigger('change');
    $this.find(SELECTOR_ROUTE_API_TIMEOUT_OVERRIDE_TOGGLE).trigger('change');
  };

  // Plugin destroyer
  routesForm.destroy = function() {
    var $this = $(this);

    $this.off(NAMESPACE);
  };

  // Plugin definition
  $.backendPlugin(pluginName, routesForm);
})(jQuery);
