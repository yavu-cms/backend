;(function($, window) {

  /**
   * Backend Dashboard ad-hoc logic.
   */

  var pluginName = 'dashboard',
    dashboard = {},
    // Constants
    BOX_CONTAINER_SELECTOR  = '.info_table_wrapper',
    DATA_CONTAINER_SELECTOR = '.data_container';


  function searchAndReplaceBoxes(){
    $(BOX_CONTAINER_SELECTOR).each(function(index){
      var data = $(this).data();
      if (data['params']) {
        dataContainer = $(this).find(DATA_CONTAINER_SELECTOR);
        dataContainer.load(data['url'], data['params']);
      }
    });

  };

  // Plugin initializer
  dashboard.init = function(opts) {
    setTimeout(function() {
      setInterval(function(){
        searchAndReplaceBoxes();
      }, 120000);
    }, 120000);
  };

  // Plugin definition
  $.backendPlugin(pluginName, dashboard);

})(jQuery, this);
