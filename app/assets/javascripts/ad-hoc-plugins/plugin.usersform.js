;(function($) {
  'use strict';

  /**
   * Ad-hoc logic for Users administration form.
   */

  var pluginName = 'usersform',
    usersForm = {},
    // Constants
    NAMESPACE = '.' + pluginName;

  // Plugin initializer
  usersForm.init = function(opts) {
    var $this = this;

    this
      .on('change' + NAMESPACE, '#user_all_client_applications', function() {
        $this.find('#user_client_application_ids').prop('disabled', $(this).prop('checked'));
      })
      .on('change' + NAMESPACE, '#user_all_sections', function() {
        $this.find('#user_section_ids').prop('disabled', $(this).prop('checked'));
      })
      .on('change' + NAMESPACE, '#user_all_representations', function() {
        $this.find('#user_representation_ids').prop('disabled', $(this).prop('checked'));
      });

    this.find('#user_all_client_applications, #user_all_sections, #user_all_representations').trigger('change' + NAMESPACE);
  };

  // Plugin destructor
  usersForm.destroy = function() {
    this.off(NAMESPACE);
  };

  // Plugin definition
  $.backendPlugin(pluginName, usersForm);
})(jQuery);