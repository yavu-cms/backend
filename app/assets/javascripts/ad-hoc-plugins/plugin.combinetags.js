;(function($) {
  'use strict';

  /**
   * Ad-hoc logic for the combine tags page.
   *
   * Usage example:
   *
   *     $formContainer.backendPlugin('combinetags');
   */

  var pluginName = 'combinetags',
    combineTags  = {},
    defaults     = {
      separateEndpoint: '#separate',
      separateConfirmation: 'Are you sure?'
    },
    // Constants
    NAMESPACE      = '.' + pluginName,
    CLASS_DISABLED = 'disabled',
    // Selectors
    SELECTOR_SELECT2_SELECT   = '#tag_combined',
    SELECTOR_SEPARATE_BUTTONS = '#already-combined .separate-button',
    SELECTOR_COMBINED_ITEM    = '.already-combined-item',
    SELECTOR_COMBINE_BUTTONS  = '#similar .combine-button',
    SELECTOR_SIMILAR_ITEM     = '.similar-item';

  // Plugin initializer
  combineTags.init = function(opts) {
    var $this = $(this),
      options = $.extend({}, defaults, opts),
      $select = $(SELECTOR_SELECT2_SELECT);

    // Register event listeners
    // 1. Separate buttons click
    $this.on('click' + NAMESPACE, SELECTOR_SEPARATE_BUTTONS, function(event) {
      var $self = $(event.target);

      if (confirm(options.separateConfirmation)) {
        // Perform the ajax request to separate the tag
        $.post(options.separateEndpoint, { id: $self.data('id') }, function() {
          var $option = $('<option/>')
            .attr('value', $self.data('id'))
            .text($self.data('name'));

          // Remove the item
          $self.attr('disabled', true);
          $self.closest(SELECTOR_COMBINED_ITEM).addClass(CLASS_DISABLED);

          // Add the option back to the select
          $select.append($option).trigger('liszt:updated');
        });
      }

      return false;
    });

    // 2. Combine buttons click
    $this.on('click' + NAMESPACE, SELECTOR_COMBINE_BUTTONS, function(event) {
      var $self = $(event.target),
        current = $select.select2('val') || [];

      // Add the id to the list of selected values
      current.push($self.data('id'));
      $select.select2('val', current).trigger('change');

      return false;
    });

    // 3. Select change
    $select.on('change' + NAMESPACE, function() {
      // Check if any of the similar values is on the list and disable them
      var selected = $select.select2('val');

      $(SELECTOR_COMBINE_BUTTONS).each(function() {
        var $me = $(this);

        if ($.inArray($me.data('id').toString(), selected) !== -1) {
          $me.attr('disabled', true);
          $me.closest(SELECTOR_SIMILAR_ITEM).addClass(CLASS_DISABLED);
        } else {
          $me.removeAttr('disabled');
          $me.closest(SELECTOR_SIMILAR_ITEM).removeClass(CLASS_DISABLED);
        }
      });

    });
  };

  // Plugin destroyer
  combineTags.destroy = function() {
    var $this = $(this),
      $select = $(SELECTOR_SELECT2_SELECT);

    // Unregister event listeners
    $this.off(NAMESPACE);
    $select.off(NAMESPACE);
  };

  // Define the plugin
  $.backendPlugin(pluginName, combineTags);

})(jQuery);