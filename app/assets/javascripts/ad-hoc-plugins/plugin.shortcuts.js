;(function($, window) {
  'use strict';

  /**
   * Shortcuts ad-hoc logic.
   */

  var pluginName = 'shortcuts',
    shortcuts    = {},
    defaults     = {
      // Context in which the modal will be created
      context:     'body',
      // Suffix to remove from the document's title when suggesting the title for the shortcut
      titleSuffix: ''
    },
    // Constants
    NAMESPACE          = '.' + pluginName,
    MODAL_MARKUP       = '<div id="shortcuts-modal" class="reveal-modal expand" data-animation="fadeAndPop"><div class="modal-body"></div><a class="close-reveal-modal">&#215;</a></div>',
    SELECTOR_CONTENT   = '.modal-body',
    SELECTOR_CATCHABLE = '.catchable';

  // Create a modal and append it to $context.
  // This is a lazy initializer for the modal.
  function createModal($context) {
    var modal = $(MODAL_MARKUP);

    modal.backendPlugin('postbuster', { update: modal.find(SELECTOR_CONTENT) });

    return modal.appendTo($context);
  }

  // Plugin initializer
  shortcuts.init = function(opts) {
    var $this  = $(this),
      options  = $.extend({}, defaults, opts),
      $context = $(options.context),
      $modal;

    // Register event listeners
    // Any click event on a `.catchable` element will be intercepted
    $this.on('click' + NAMESPACE, SELECTOR_CATCHABLE, function(e) {
      var $self = $(this),
        url     = $self.attr('href'),
        title   = document.title.trim().replace(options.titleSuffix, '');

      e.preventDefault();

      // Lazy initialization of the modal
      $modal = $modal || createModal($context);
      $modal.find(SELECTOR_CONTENT).empty();

      // Add the initial url and title values to the original target
      url += (url.match(/\?/) ? '&' : '?')
          + $.param({ url: window.location.href, title: title });

      // Turn the original request into an ajax one and show the response in a modal
      $.get(url, function(response) {
        var body = $modal.find(SELECTOR_CONTENT);

        // Initialize any backend plugin in the response
        body.html(response).backendPlugin('initializer');

        $modal.foundation('reveal', 'open');
      });

      return false;
    });
  };

  // Plugin destroyer
  shortcuts.destroy = function() {
    var $this = $(this);

    // Remove event listeners
    $this.off(NAMESPACE);
  };

  // Plugin definition
  $.backendPlugin(pluginName, shortcuts);

})(jQuery, this);