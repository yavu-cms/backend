//
// JS libraries.
//
//= require i18n
//= require jquery_ujs
//= require mediaelement_rails
//= require typeahead
//= require codemirror
//= require foundation
//= require select2
//= require dropzone
//= require jquery_nested_form
//= require jquery-ui-timepicker-addon
//= require faye-browser
