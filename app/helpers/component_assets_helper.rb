module ComponentAssetsHelper
  def form_component_asset_path(component, component_asset)
    if component_asset.new_record?
      component_assets_path(component)
    else
      component_asset_path(component, component_asset)
    end
  end
end
