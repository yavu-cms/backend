module VersionsHelper
  def version_info(version)
    t('versions.versioned_at',
      action: t("versions.events.#{version.event}"),
      date: l(version.created_at, format: :long_with_time),
      perpetrator: perpetrator(version.whodunnit))
  end

  def generic_show_path(resource)
    polymorphic_path(resource) rescue false
  end

  def version_link(version)
    link_to version_info(version), version_path(version)
  end

  def perpetrator(user_id)
    User.find_by(id: user_id) || User.new({username: t('auditing.user_undefined')})
  end

  # TODO: ask cuesta about this => I18n.backend.send(:translations)[:es][:auditing][:events]
  def events_with_translation
    Hash[%i[update create restore destroy].map { |event| [t("versions.events.#{event}"), event] }]
  end


  # TODO: set versionable models someplace
  def versionable_item_types_with_translation
    Hash[%i[ article client_application component view ].map { |model| [t("activerecord.models.#{model}"), model] }]
  end
end
