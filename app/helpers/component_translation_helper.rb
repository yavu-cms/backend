module ComponentTranslationHelper
  def form_component_translation_path(component, component_translation)
    if component_translation.new_record?
      component_translations_path(component)
    else
      component_translation_path(component, component_translation)
    end
  end
end