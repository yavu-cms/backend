module AssetHelper
  def render_asset(asset, compiled: false)
    send "render_#{asset.type}", asset, compiled
  end

  def render_css(asset, compiled)
    raw CodeRay.scan("#{asset.content(compiled)}", :css).div(line_numbers: :table)
  end
  alias :render_stylesheet :render_css

  def render_font(asset, _)
    link_to asset.to_s, client_application_asset_serve_path(asset)
  end

  def render_image(asset, _)
    image_tag asset_preview_path(asset)
  end

  def render_javascript(asset, compiled)
    raw CodeRay.scan("#{asset.content(compiled)}", :js).div(line_numbers: :table)
  end

  def asset_preview_path(asset)
    try(:"#{asset.class.name.underscore}_serve_path", asset) || "#"
  end
end
