require 'sanitize'

module BackendPluginHelper
  # Escape the provided configuration so that it is suitable for its use
  # in an HTML snippet.
  # Usage:
  #   <section id="container" data-plugins="<%= escape_configuration initializer: {} %>">Content</section>
  #   <!--
  #     Will render to:
  #       <section id="container" data-plugins="{&quot;initializer&quot;:{}}">Content</section>
  #     -->
  def escape_configuration(configuration)
    h MultiJson.encode(configuration)
  end

  # Un-escape the provided (escaped) configuration so that it is a valid hash object again. This is the
  # opposite operation of #escape_configuration.
  # Usage:
  #   original_hash = unescape_configuration '{&quot;initializer&quot;:{}}'
  #     # => { initializer: {} }
  def unescape_configuration(configuration)
    configuration = Sanitize.clean(configuration)

    MultiJson.load(configuration).to_options
  end

  # Add the data-plugins attribute to an HTML element.
  # Usage:
  #   <section id="container" <%= plugins initializer: {} %>>Content</section>
  #   <!--
  #     Will render to:
  #       <section id="container" data-plugins="{&quot;initializer&quot;:{}}">Content</section>
  #     -->
  def plugins(configuration)
    raw "data-plugins=\"#{escape_configuration(configuration)}\""
  end
end