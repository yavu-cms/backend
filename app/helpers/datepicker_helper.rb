module DatepickerHelper
  def datepicker_field(object, method, options = {}, html_options = {})
    options[:dateFormat] = datepicker_format(options[:dateFormat])
    options.reverse_merge! datepicker_defaults

    html_options[:data] ||= {}
    html_options[:data][:plugins] = escape_configuration datepicker: options
    html_options[:autocomplete] = 'off'

    text_field object, method, html_options
  end

  def timepicker_field(object, method, options = {}, html_options = {})
    options.reverse_merge! timepicker_defaults

    html_options[:data] ||= {}
    html_options[:data][:plugins] = escape_configuration timepicker: options
    html_options[:autocomplete] = 'off'

    text_field object, method, html_options
  end

  private
    def datepicker_defaults
      { culture: I18n.locale }
    end

    def timepicker_defaults
      { culture: I18n.locale }
    end

    def datepicker_format(format = '')
      format ||= I18n.t('date.formats.default')
      case format
      when I18n.t('date.formats.default')
        "dd/mm/yy"
      when I18n.t('date.formats.short')
        "dd 'de' M"
      when I18n.t('date.formats.long')
        "dd 'de' MM 'de' yy"
      else
        format
      end
    end
end
