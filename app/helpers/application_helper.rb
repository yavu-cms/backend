module ApplicationHelper
  def render_flashes
    render partial: 'shared/flashes' if flash.any?
  end

  def render_form_errors(source)
    render(partial: "shared/form_errors", locals: { errors: source.errors, model_name: t("activerecord.models.#{source.class.name.to_s.downcase}")}) unless source.errors.blank?
  end

  def render_boolean(value)
    content_tag :i, '', class: value ? 'fi-check text-success' : 'fi-x'
  end

  def render_color(value)
    if value.present?
      content_tag(:div, '', style: "background-color: #{value}; height: 1em; width: 4em;") +
        content_tag(:span, "(#{value})", style: " font-size: 0.8em; font-weight: bold; text-transform: uppercase;")
    else
      content_tag(:span, 'N/A')
    end
  end

  def render_collection(values, i18n: false, glue: ', ')
    values = values.map { |v| t(v, scope: i18n) } if i18n.present?
    values.join(glue)
  end

  # Check if the given value should display a default "N/A" style
  # value (checker returns a truthy value) or if the value itself
  # can be safely rendered (checker returns false).
  def render_value(value, default = nil, checker = :blank?)
    value.try(checker) ? (default || t('helpers.render_value.default').html_safe) : value
  end

  def render_medium(medium)
    if medium.type == 'image'
      image_tag(medium.file_url(:thumb).to_s)
    else
      link_to medium.name, medium.file_url.to_s
    end
  end

  def render_parameterized_url(url)
    raw url.gsub /(:[^\/]+)/, '<span class="url-parameter">\\1</span>'
  end

  def title(content = nil, &block)
    content = capture(&block) if block_given?
    content_for :title, content.html_safe
    content
  end

  def kaminari_info(collection)
    page_entries_info collection, locale: YavuSettings.i18n.default_locale
  end

  def date_with_format(datetime, date_format = :default)
    I18n.l datetime.to_date, format: date_format unless datetime.nil?
  end

  def time_with_format(datetime, time_format = :default)
    I18n.l datetime.to_time, format: time_format unless datetime.nil?
  end

  # Add a help icon with a tooltip. Useful with form elements.
  def help(content, options = {})
    options.reverse_merge!({ position: 'bottom', inline: true, tag: :i, title: content })

    options[:class] ||= ''
    options[:class] << "help has-tip #{'inline' if options[:inline]} #{options[:position]}"
    options[:data] ||= {}
    options[:data][:tooltip] = true

    options.delete :inline
    options.delete :position
    tag = options.delete :tag

    content_tag tag, t('helpers.help.content'), options
  end

  def date_range_with_format(dates, date_range_format = 'from_to')
    I18n.t "date_range.formats.#{date_range_format}",
      begin_date: date_with_format(dates.begin),
      end_date: date_with_format(dates.end)
  end
end
