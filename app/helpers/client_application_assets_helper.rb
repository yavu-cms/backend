module ClientApplicationAssetsHelper
  def representations_which_is_present(asset)
    asset.representations.any? ? asset.representations.map(&:to_s).join(', ') : '-'
  end
end