module MediaHelper
  # Returns a select tag for the upload file types.
  #
  # Available options:
  #   :exclude (Array) A set of media types to exclude from the select. For instance, ['EmbeddedMedium']
  def upload_type_select_tag(object, method, options = {})
    types = Medium.types.map(&:name)
    types -= options.delete(:exclude) if options[:exclude]
    opts = options.reverse_merge({ i18n_key: 'media.types', include_blank: true })
    i18n_select object, method, types, opts
  end

  # Render the preview partial for the given +medium+, optionally specifying
  # a prefix for the partial path (for instance, 'index' will generate 'index_<type>_preview')
  # and an alternate controller, instead of the one this helper is being invoked.
  #
  # === Examples
  #
  #    preview_medium(image_medium)
  #      # => renders partial '_image_medium_preview' on the current controller
  #    preview_medium(image_medium, 'index')
  #      # => renders partial '_index_image_medium_preview' on the current controller
  #    preview_medium(image_medium, nil, 'articles')
  #      # => renders partial '_image_medium_preview' on the articles controller
  #    preview_medium(image_medium, 'index', 'articles')
  #      # => renders partial '_index_image_medium_preview' on the articles controller
  def preview_medium(medium, prefix: nil, controller: nil)
    controller ||= params[:controller]
    partial_path = "#{controller + '/' unless controller.blank?}#{medium.type.underscore + '/'}#{prefix + '_' unless prefix.blank?}preview"
    render partial: partial_path, locals: { medium: medium }, formats: [:html]
  end

  # Helper method to solve a BC-breaking change introduced in Rails 4.0.1.
  # This method deals with the decision of which route should be used to upload
  # a new +Medium+ object (or any of its subclasses) in a form where one needs
  # to specify the URL (because of using a subclass instead of the root +Medium+
  # class)
  def form_upload_medium_path(medium)
    if medium.persisted?
      medium_path(medium)
    else
      media_path
    end
  end

  def medium_tag(medium, host: nil, size: :medium)
    case medium
    when ImageMedium
      image_tag medium_url(medium, size: size), alt: medium.name, class: 'defer-media-url'
    when EmbeddedMedium
      medium.content
    else
      nil
    end
  end

  def medium_url(medium, host: nil, size: :medium)
    medium.public_send("#{size}_url") || medium.url
  rescue
    Rails.logger.error "#{size} is not a valid medium size for #{medium.class}"
    medium.url
  end
end
