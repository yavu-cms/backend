module FilterHelper
  def render_simple_filter(action, filter_key, options = {})
    render(partial: 'shared/filters/simple', locals: { action: action, filter_key: filter_key, options: options })    
  end

  def render_filter_labels(scope)
    render(partial: 'shared/filters/labels', locals: { scope: scope })
  end
end