module AnnouncementsHelper
  def announcements_url(given_url = nil)
    build_announcements_url(given_url || YavuSettings.announcements.hostspec || root_url)
  end

  def announcements_timeout
    YavuSettings.announcements.timeout
  end

  def announcements_options(given_url = nil)
    {
      url: announcements_url(given_url),
      timeout: announcements_timeout,
      channels: {
        global: channel_path(:global),
        user: channel_path(current_user)
      },
      messages: {
        error: ''
      }
    }
  end

  def channel_path(resource, namespace: nil)
    uri = case resource
      when :global
        '/announcement/all'
      when User
        "/announcement/#{resource.username}"
      when Article
        "/article/#{resource.to_param}"
      when RepresentationDraft
        "/representation/#{resource.to_param}"
      else
        "/announcement/#{resource}"
    end
    uri = namespace + uri unless namespace.blank?
    uri
  end

  protected

  def build_announcements_url(hostspec)
    URI(hostspec).tap do |uri|
      uri.path = YavuSettings.announcements.path
    end.to_s
  end
end
