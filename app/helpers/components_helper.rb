module ComponentsHelper
  mattr_accessor :autocomplete_fields

  self.autocomplete_fields = {
    'article' => ->(object, *args) { object.send :search_articles_path, *args    },
    'gallery' => ->(object, *args) { object.send :search_galleries_path, *args   },
    'medium'  => ->(object, *args) { object.send :autocomplete_media_path, *args },
    'section' => ->(object, *args) { object.send :search_sections_path, *args    }
  }

  # Helper method to solve a BC-breaking change introduced in Rails 4.0.1.
  # This method deals with the decision of which route should be used to upload
  # a new +Component+ object (or any of its subclasses) in a form where one needs
  # to specify the URL (because of using a subclass instead of the root +BaseComponent+
  # class)
  def form_component_path(component)
    if component.persisted?
      component_path(component)
    else
      components_path
    end
  end

  def autocomplete_field_options_path(model, *args)
    autocomplete_fields[model.to_s.parameterize].call(self, args)
  rescue
    raise "Don't know which path should be used for field options of model #{model}"
  end
end
