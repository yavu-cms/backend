module ActionsHelper
  # Render lists of actions buttons on both sides of context-actions container.
  # => scope is use to get translations.
  # => left_actions contain a hash with actions to display on left side.
  # => right_actions contain a hash with actions to display on right side.
  # => options contains rendering options, html tags, html options, etc.
  #   # => Example:
  #           <%= render_context_actions :article,
  #             left: { back: {url: articles_path} },
  #             right: {
  #               clone: {url: copy_article_path(@article), class: 'warning'},
  #               edit:  {url: edit_article_path(@article)},
  #               destroy: {url: article_path(@article)}
  #             },
  #             target: @article %>
  def render_context_actions(scope, left: {}, right: {}, options: {}, target: nil)
    options[:buttons] = true
    render partial: 'shared/context_actions/show', locals: {
      left_actions: filter_actions(create_links(scope, left, options), target),
      right_actions: filter_actions(create_links(scope, right, options), target)
    }
  end

  # Render lists of actions buttons on both sides of context-actions container.
  # => target is the object on which the actions apply.
  # => scope is use to get translations.
  # => actions contain a hash with actions to display.
  # => options contains rendering options, html tags, html options, etc.
  #   # => Example:
  #        <%= render_dropdown_actions article, :article, show:    {url: article_path(article)},
  #                                                       edit:    {url: edit_article_path(article)},
  #                                                       clone:   {url: copy_article_path(article)},
  #                                                       destroy: {url: article_path(article)} %>
  def render_dropdown_actions(target, scope, actions = {}, options = {})
    render partial: 'shared/dropdown_actions/index', locals: {
      target: target,
      actions: filter_actions(create_links(scope, actions, options), target)
    }
  end

  private

  def filter_actions(actions, target = nil)
    actions.select do |action|
      action.delete(:condition) && (target ? can?(action.delete(:action), target) : true)
    end
  end

  # Return de specific action translation for scope if exist, else return shared translation.
  def t_action(action, scope)
    t action, scope: [:activerecord, :actions, scope], default: t(action, scope: [:activerecord, :actions, :shared])
  end

  # Set to each elements for actions hash the correct options to render.
  def create_links(scope, actions, options)
    actions.map do |action, action_options|
      build_options(scope, action, action_options, options)
    end
  end

  def build_options(scope, action, action_options, options)
    # Action defaults
    case action
      when :new, :edit
        action_options.reverse_merge! class: 'success'
      when :associate, :enable
        action_options.reverse_merge! class: 'success', divider: true
      when :disassociate, :disable
        action_options.reverse_merge! class: 'alert', divider: true
      when :back
        action_options.reverse_merge! class: 'secondary'
      when :destroy
        action_options.reverse_merge! divider: true, class: 'alert', method: :delete, data: {}
        action_options[:data][:confirm] ||= t(:destroy_confirmation, scope: [:activerecord, :actions, scope], default: t('activerecord.actions.shared.confirmation.destroy'))
    end
    # Group action title
    action_options[:actions_title_class] = 'actions-group-title' if action_options[:title]
    # Permission-related action name
    action_options[:action] ||= action
    # Text button
    action_options[:text] ||= t_action(action, scope)
    # Group action title
    action_options[:actions_title_class] = 'actions-group-title' if action_options[:title]
    # Divider
    action_options[:divider_class] = 'divider' if action_options[:divider]
    # Condition
    action_options[:condition] = true unless action_options.has_key? :condition
    # Add common css classes
    action_options[:class] = "action button radius #{action_options[:class]}" if options[:buttons]
    action_options
  end
end
