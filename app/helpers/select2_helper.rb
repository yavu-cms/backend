module Select2Helper

  # Collection select that adds the necessary backend plugin configuration to use
  # the select2 JS plugin with the select.
  def collection_select2_select(object, method, collection, value_method, text_method, options = {}, html_options = {}, select2_options = {})
    # Merge some defaults into select2_options
    select2_options.reverse_merge! select2_defaults

    # Placeholder
    if options[:prompt]
      select2_options[:placeholder] = t('helpers.select.prompt')
      options.delete :prompt
    end

    if select2_options[:placeholder]
      options[:include_blank] = true
    end

    # Single deselect allowance
    if options[:include_blank] && !select2_options.has_key?(:allowClear)
      select2_options[:allowClear] = true
    end

    # Finally add the Initializer backend plugin data-plugins attribute
    configuration = { select2: select2_options }
    # Merge the backend plugin configuration with the existing ones in the html_options hash
    configuration.merge!(unescape_configuration html_options[:'data-plugins']) if html_options.has_key? :'data-plugins'

    html_options[:'data-plugins'] = escape_configuration configuration

    collection_select(object, method, collection, value_method, text_method, options, html_options)
  end

  def ajax_select2_field(object, method, url, options = {}, select2_options = {})
    select2_options.reverse_merge! select2_defaults
    select2_options[:ajax] ||= {}
    select2_options[:ajax][:url] = url
    select2_options[:ajax][:dataType] ||= 'json'

    # Initializer backend plugin data-plugins attribute
    configuration = { select2: select2_options }

    # Merge the backend plugin configuration with the existing ones in the options hash
    configuration.merge!(unescape_configuration options[:'data-plugins']) if options.has_key? :'data-plugins'

    options[:'data-plugins'] = escape_configuration configuration

    text_field object, method, options
  end

  def ajax_select2_field_tag(name, value, url, options = {}, select2_options = {})
    select2_options.reverse_merge! select2_defaults
    select2_options[:ajax] ||= {}
    select2_options[:ajax][:url] = url
    select2_options[:ajax][:dataType] ||= 'json'

    # Initializer backend plugin data-plugins attribute
    configuration = { select2: select2_options }
    # Merge the backend plugin configuration with the existing ones in the options hash
    configuration.merge!(unescape_configuration options[:'data-plugins']) if options.has_key? :'data-plugins'

    options[:'data-plugins'] = escape_configuration configuration

    text_field_tag name, value, options
  end

  def i18n_select(object, method, collection, options = {})
    key = options.delete :i18n_key

    if key
      key << '.'
      collection = collection.collect { |item| [item, I18n.t("#{key}#{item}")] }
    end

    select_2_options = options.delete :select_2
    html_options = options.delete :html

    collection_select2_select(object, method, collection, :first, :last, options, html_options || {}, select_2_options || {})
  end

  def tagging_select(object, method, url, scope = :visible, options = {}, select2_options = {})
    # Merge some defaults into select2_options
    select2_options.reverse_merge! select2_defaults


    select2_options[:tags] = [] if can? :create, Tag
    select2_options[:tokenSeparators] = ['\n', ',']

    select2_options[:ajax] ||= {}
    select2_options[:ajax][:url] = url
    select2_options[:ajax][:dataType] ||= 'json'

    configuration = { select2: select2_options }
    configuration.merge!(unescape_configuration options[:'data-plugins']) if options.has_key? :'data-plugins'

    options[:'data-plugins'] = escape_configuration configuration

    text_field(object, method, options)
  end

  def multi_select(object, method, options = {}, select2_options = {})
    # Merge some defaults into select2_options
    select2_options.reverse_merge! select2_defaults

    select2_options[:tags] = []
    select2_options[:tokenSeparators] = [',', ' ']

    configuration = { select2: select2_options }
    configuration.merge!(unescape_configuration options[:'data-plugins']) if options.has_key? :'data-plugins'

    options[:'data-plugins'] = escape_configuration configuration

    text_field(object, method, options)
  end

  private
    def select2_defaults
      { no_results_text: t('helpers.select2.no_results') }
    end
end
