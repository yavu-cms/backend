module MenuHelper
  def menu_divider
    content_tag :li, '', class: 'divider'
  end

  def menu_group(attrs = {}, &block)
    content_tag :ul, attrs, &block
  end

  def menu_dropdown(key, url = '#', action: :read, target: nil, skip_divider: false, &block)
    return unless menu_can?(action, target)
    content_tag(:li, class: 'has-dropdown') do
      link_to(t(key, scope: [:menu], default: key), url) + content_tag(:ul, class: 'dropdown', role: 'menu', &block)
    end + (skip_divider ? '' : menu_divider)
  end

  def menu_item(label, url, action: :read, target: nil, **options)
    return unless menu_can?(action, target)
    content_tag :li do
      link_to label, url, options
    end
  end

  def menu_can?(action, target)
    target.blank? || begin
      target = [target] unless target.respond_to? :each
      target.any? { |each_target| can? action, each_target }
    end
  end
end