module ComponentViewHelper
  def form_component_view_path(component, component_view)
    if component_view.new_record?
      component_views_path(component)
    else
      component_view_path(component, component_view)
    end
  end
end