module ComponentServicesHelper
  def form_component_service_path(component, service)
    if service.new_record?
      component_services_path(component)
    else
      component_service_path(component, service)
    end
  end
end
