module ClientApplicationsHelper
  def client_application_image_tag(asset)
    image_tag client_asset_download_path(asset)
  end

  def client_asset_download_path(asset)
    serve_client_application_application_asset_path(asset.client_application, asset)
  end
end
