class HealthCheck
  HEALTHY_RESPONSE = [200, { 'Content-Type' => 'text/plain' }, ['App is responsive'.freeze]]

  def initialize(app)
    @app = app
  end

  def call(env)
    if env['PATH_INFO'.freeze] == '/_health_check'.freeze
      HEALTHY_RESPONSE
    else
      @app.call(env)
    end
  end
end
