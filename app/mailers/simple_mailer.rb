class SimpleMailer < ActionMailer::Base
  default content_type: 'text/plain'

  def sendmail(from:, to:, subject:, body:)
    mail(from: YavuSettings.mailer.smtp_settings.user_name || from, reply_to: from, to: to, body: body, subject: subject).deliver
  end
end
