class NewsletterMailer < ActionMailer::Base
  default from: YavuSettings.mailer.smtp_settings.user_name,
          content_type: 'text/html'

  NEWSLETTER_BATCH_SIZE = 20
  SENDMAIL_INTERVAL = 30

  # This email addresses are the newsletters testing receivers
  def test_receivers
    if (emails = ENV['NEWSLETTER_TESTING'].presence)
      emails.split(';')
    else
      []
    end
  end

  def users_mails
    tr = test_receivers; return tr unless tr.empty?
    users_api = YavuSettings.users_api
    Rails.logger.debug('NewsletterMailer') { "Fetching emails url=#{users_api.newsletters_emails_endpoint}" }
    emails = RestClient.get users_api.newsletters_emails_endpoint, params: { token: users_api.token }
    MultiJson.load emails
  rescue
    []
  end

  def broadcast(static_data_id)
    newsletterGD = NewsletterGeneratedData.find_by(slug: static_data_id)
    body = newsletterGD.try(:content)
    subject = newsletterGD.newsletter.try(:subject)
    users_mails.each_slice(NEWSLETTER_BATCH_SIZE) { |addresses| sendmail addresses, body, subject } if body
  end

  private

  def sendmail(addresses, body, subject)
    mail(bcc: addresses, body: body, subject: subject).deliver
    sleep SENDMAIL_INTERVAL
  rescue Exception => e
    ExceptionNotifier.notify_exception e, data: { message: 'Newsletter mailer found a failure' }
  end
end
