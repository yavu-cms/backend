# Documentation for Shortcut class
#
# == Summary
#
# The class must be created with:
# * user_id
# * title
# * url
# * show_in_menu
#
# == Example
#
# Shortcut.create(user_id: 1, title: 'sarasa', url: 'http://localhost:3000/surveys')
#
# === Class Shortcut
#
#
class Shortcut < ActiveRecord::Base
  # Associations
  belongs_to :user

  # Validations
  validates :user_id,   presence: true
  validates :title,     presence: true, length: {maximum: 255}
  validates :url,       presence: true, uniqueness: { scope: :user_id, case_sensitive: false }, length: {maximum: 255}


  # Scope for sorting the shortcuts
  def self.sorted
    order('title ASC')
  end

  # Scope for limiting the result set only to the elements
  # that should be shown in the shortcuts menu.
  def self.for_menu
    where show_in_menu: true
  end

  # Return the shorcuts for a given user
  def self.for(user)
    where user_id: user
  end

  # Set a shortcut like must be shown
  def show_in_menu!
    self.show_in_menu = true
    save
  end

  # Set a shortcut like must not be shown
  def remove_from_menu!
    self.show_in_menu = false
    save
  end
end
