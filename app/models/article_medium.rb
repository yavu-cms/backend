# Documentacion para la clase ArticleMedium
#
# == Resumen
#
# Crea un medio para el articulo
#
# La clase ArticleMedium crea un medio para el articulo con:
# * article_id
# * medium_id
#
# == Ejemplo
#
# ArticleMedium = ArticleMedium.create(article_id: 6, medium_id: 9)
#
# === Clase ArticleMedium
#

class ArticleMedium < ActiveRecord::Base
  belongs_to :article, touch: true, inverse_of: :article_media
  belongs_to :medium

  has_paper_trail

  validates :medium, :article, presence: true
  validates_length_of %i[alt title], maximum: 255

  delegate :type, :file, :tags, to: :medium

  default_scope { order :order }
  scope :image,     -> { eager_load(:medium).where(media: {type: ImageMedium}) }
  scope :embedded,  -> { joins(:medium).where(media: {type: EmbeddedMedium}) }
  scope :file,      -> { joins(:medium).where(media: {type: FileMedium}) }
  scope :audio,     -> { joins(:medium).where(media: {type: AudioMedium}) }

  set_api_resource id: :id,
    type: :type_for_frontend,
    caption: :calculated_caption,
    order: :order,
    medium: :medium,
    alt: :alt,
    title: :title,
    only_cover: :only_cover,
    for_gallery: :for_gallery,
    is_embedded: ->(_) { medium.is_a?(EmbeddedMedium) },
    is_image: ->(_) { medium.is_a?(ImageMedium) }

  # Automatically transfer the caption to associated medium's name
  # if the latter has not a previously set name
  def medium
    if (s = super)
      # (0..250) range is because medium name size in db is 256
      s.tap { |m| m.name = caption.try(:slice, 0..250) unless m.name.present? }
    end
  end

  def calculated_caption
    caption.presence || article.try(:title)
  end

  private

  def type_for_frontend
    medium.class.to_s.demodulize.chomp('Medium').underscore.to_sym
  end
end
