class ClientApplication < ActiveRecord::Base
  include ExtraAttributeModelable, Versionable
  has_many :routes, dependent: :destroy, inverse_of: :client_application
  has_many :representations, dependent: :destroy
  has_many :assets, class_name: 'ClientApplicationAsset', dependent: :destroy
  has_many :newsletters, dependent: :destroy
  belongs_to :favicon, class_name: 'ClientApplicationAsset'
  belongs_to :logo, class_name: 'ClientApplicationAsset'
  belongs_to :news_source

  accepts_nested_attributes_for :extra_attribute_model

  validates :identifier, uniqueness: true, length: {maximum: 50}
  validates :name, uniqueness: true, presence: true, length: {maximum: 50}
  validates :url,  uniqueness: true, presence: true
  validates :news_source, presence: true
  validates_length_of %i[url assets_url media_url route_cache_control service_cache_control], maximum: 255

  # For testing purposes only. By setting this attribute to +true+, the creation
  # of default routes will be skipped, making our tests faster.
  attr_accessor :skip_initialize_routes

  has_paper_trail

  before_create :generate_identifier
  after_create :create_default_routes!, if: ->{ !skip_initialize_routes && routes.empty? }
  after_save :update_related_entities

  scope :enabled, -> { where(enabled: true) }


  set_api_resource id: :identifier,
    routes: :routes,
    enabled: :enabled,
    base_url: :url,
    secure_base_url: :secure_url,
    accounting_url: :accounting_url,
    service_prefix: :service_prefix,
    internal_media_url: ->(x) { BaseUploader.base_path },
    external_media_url: :media_url,
    logo: :logo,
    assets_configuration: :assets_configuration,
    runtime_options: :runtime_options

  # Search for client applications using a query hash, which may contain:
  #   * query  [String] string to be searched on the name (using %<query>%)
  def self.search(params = {})
    if params[:query].blank?
      all
    else
      where('name like ?', "%#{params[:query]}%")
    end
  end

  def secure_url
    secure = URI(url)
    secure.scheme = 'https'
    secure.to_s
  rescue
  end


  def accounting_url
    '/track.css'
  end

  def service_prefix
    '/_serve'
  end

  def self.route_cache_control_default
    "public, max-age=120, s-maxage=600"
  end

  def calculated_route_cache_control
    route_cache_control.blank? ? ClientApplication.route_cache_control_default : route_cache_control
  end

  def self.service_cache_control_default
    "private, must-revalidate, max-age=0"
  end

  def calculated_service_cache_control
    service_cache_control.blank? ? ClientApplication.service_cache_control_default : service_cache_control
  end

  def to_s
    name
  end

  # Create default routes for a newly-created application
  def create_default_routes!
    self.routes = default_routes
  end

  def can_have_logo?
    true
  end

  # Return the default homepage route for this application
  def homepage_route
    @homepage_route ||= routes.find_by(type: HomepageRoute)
  end

  # Return the articles set in the homepage
  def home_articles(section = nil, limit = nil)
    return [] unless homepage_route
    home_representation = homepage_route.representation
    articles = home_representation.cover_articles do |ar|
      ar.with_section(section.try(:id)).for_cover
    end
    limit ? articles.first(limit) : articles
  end

  # Return every representation that is a cover - i.e., it responds with true
  # to :cover?
  # @see Representation#cover?
  def cover_representations
    representations.select(&:cover?)
  end

  def assets_prefix
    YavuSettings.assets.prefix
  end

  def assets_host
    unless assets_url.blank?
      URI(assets_url).host rescue nil
    end
  end

  def self.assets_configuration(client_application = nil, prefix: '', for_preview: false)
    Yavu::API::Resource::Base.new.tap do |config|
      config.base_url        = client_application.try(:url).to_s
      config.service_prefix  = client_application.try(:service_prefix).to_s
      config.root            = Rails.root.to_s
      config.public_path     = Rails.public_path.to_s
      prefix, path           = client_application.present?? [client_application.assets_prefix, client_application.identifier] : [YavuSettings.assets.prefix, prefix]
      config.prefix          = File.join(prefix, path)
      config.js_compressor   = YavuSettings.assets.js_compressor
      config.css_compressor  = YavuSettings.assets.css_compressor
      config.digest          = YavuSettings.assets.digest
      config.debug           = client_application.try :debug
      config.asset_host      = client_application.try :assets_host

      config.manifest_file   = File.join(config[:public_path], config[:prefix], 'manifest.json')
      config.append_paths    = BaseComponent.assets_append_paths + (client_application.try(:assets_append_paths, for_preview: for_preview) || [])
    end
  end

  def assets_configuration
    self.class.assets_configuration(self)
  end

  def compile_assets
    configuration = assets_configuration
    environment = Yavu::Frontend::Util::AssetConfigurationFactory.sprockets_environment configuration
    manifest = Yavu::Frontend::Util::AssetConfigurationFactory.configure_sprockets_helpers(configuration, environment)
    manifest.try(:compile)
    manifest.try(:clean, 10)
    notify_clients
  end

  def notify_clients
    FrontendServer.notify self
  end

  def servers
    FrontendServer.all.select { |x| x.client_application_identifier == identifier }
  end

  def for_preview(representation = nil)
    becomes(PreviewClientApplication).tap do |pca|
      pca.preview_representation = representation
      pca.updated_at = Time.now # Avoid caching
    end
  end

  def runtime_options
    extra_attribute_model.to_hash
  end

  def internal_varnish_urls=(urls)
    urls = urls.split(',') if urls.is_a? String
    write_attribute(:internal_varnish_urls, urls.uniq.join(','))
  end

  def internal_varnish_urls
    read_attribute(:internal_varnish_urls).try(:split, ',') || []
  end

  def external_varnish_urls=(urls)
    urls = urls.split(',') if urls.is_a? String
    write_attribute(:external_varnish_urls, urls.uniq.join(','))
  end

  def external_varnish_urls
    read_attribute(:external_varnish_urls).try(:split, ',') || []
  end

  def copy(params = {})
    copy = dup except: [:name, :url, :identifier], include: [:representations, :routes, :extra_attribute_model]
    copy.assign_attributes url: params[:url], name: params[:name] || I18n.t('client_applications.copy.prefix', name: self.name)
    copy.routes.each {|route| route.client_application = copy }
    copy.representations = representations.map(&:copy)
    copy.assets = self.assets.map  { |asset| asset.copy(copy) }
    copy
  end

  def assets_append_paths(for_preview: false)
    AssetUploader.types.inject([]) do |result, asset_type|
      if for_preview
        result << ClientApplicationAssetUploader.absolute_prefix_for_representation_type(self, asset_type, 'drafts')
      else
        result << ClientApplicationAssetUploader.absolute_prefix_for_representation_type(self, asset_type, 'representations')
      end
      result << ClientApplicationAssetUploader.absolute_prefix_for_type(self, asset_type)
    end.map(&:to_s)
  end

  private

  # Generate the identifier for this client application
  def generate_identifier
    self.identifier = Digest::SHA1.hexdigest name
  end

  def default_routes
    [
      HomepageRoute.new(name: 'Home', pattern: '/', priority: 5),
      SectionRoute.new(name: 'Section', pattern: '/:section', priority: 3 ),
      ArticleRoute.new(name: 'Article', pattern: '/:section/:article', priority: 2 ),
      SearchRoute.new(name: 'Search', pattern: '/search/query', priority: 0 ),
      NewsletterRoute.new(name: 'Newsletter', pattern: '/newsletter/:newsletter', priority: 4 ),
    ].each do |route|
      if route.requires_representation?
        route.representation = Representation.new(name: route.name, client_application: self)
      end
    end
  end

  def update_related_entities
    routes.update_all(updated_at: Time.now)
    representations.map{|r| r.services.update_all(updated_at: Time.now) }
  end
end
