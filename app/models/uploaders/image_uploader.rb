class ImageUploader < BaseUploader
  include CarrierWave::MiniMagick
  include CarrierWave::ImageOptimizer

  # process store_dimensions: :original

  def self.quality
    Setting['media_processing.quality'].presence || 72
  end

  version :xsmall, if: :generate_versions?  do
    process resize_and_resample: :xsmall_version
    process optimize: [{quality: ImageUploader.quality, quiet: true}], if: Proc.new { Rails.env.production? }
    process store_dimensions: :xsmall
  end

  version :small, if: :generate_versions?  do
    process resize_and_resample: :small_version
    process optimize: [{quality: ImageUploader.quality, quiet: true}], if: Proc.new { Rails.env.production? }
    process store_dimensions: :small
  end

  version :medium, if: :generate_versions?  do
    process resize_and_resample: :medium_version
    process optimize: [{quality: ImageUploader.quality, quiet: true}], if: Proc.new { Rails.env.production? }
    process store_dimensions: :medium
  end

  version :big, if: :generate_versions?  do
    process resize_and_resample: :big_version
    process optimize: [{quality: ImageUploader.quality, quiet: true}], if: Proc.new { Rails.env.production? }
    process store_dimensions: :big
  end

  version :xbig, if: :generate_versions?  do
    process resize_and_resample: :xbig_version
    process optimize: [{quality: ImageUploader.quality, quiet: true}], if: Proc.new { Rails.env.production? }
    process store_dimensions: :xbig
  end

  def extension_white_list
    YavuSettings.media.allowed_extensions.image
  end

  def generate_versions?(file)
    model.valid? && !model.vectorial?
  end

  private

  def resize_and_resample(version)
    dimensions = model.send(version)
    resolution = Setting['media_processing.resolution'].presence || 72
    manipulate! do |img|
      img.combine_options do |c|
        c.fuzz       '3%'
        c.density    "#{resolution}"
        c.resize     "#{dimensions}>"
        c.interlace  'none'
        c.colorspace 'sRGB'
      end
    end
  end

  def store_dimensions(version)
    if file && model
      width, height = ::MiniMagick::Image.open(file.file)[:dimensions]
      model.dimensions[version] = {width: width, height: height}
    end
  end
end
