class ClientApplicationAssetUploader < AssetUploader
  # Helper for absoulete_prefix_path and store_dir_prefix
  def self.prefix_for_type(client_application, type)
    File.join %W(#{client_application.identifier} assets #{type.pluralize}).map { |x| x.parameterize '_' }
  end

  # Helper for ClientApplication#assets_configuration
  def self.absolute_prefix_for_type(client_application, type)
    File.join root, prefix_for_type(client_application, type)
  end

  def self.prefix_for_representation_type(client_application, type, kind = nil)
    kind ||= 'representations'
    File.join [client_application.identifier, kind, type.pluralize].map { |x| x.parameterize '_' }
  end

  # Helper for ClientApplication#assets_configuration
  def self.absolute_prefix_for_representation_type(client_application, type, kind = nil)
    File.join root, prefix_for_representation_type(client_application, type, kind)
  end

  def store_dir_prefix
    self.class.prefix_for_type(model.client_application, model.type)
  end

  def store_dir_suffix
    model.prefix_path
  end

  def self.javascript_representation_store_filename(representation, was = false)
    raise 'AssetUploader types must include javascript' unless types.index('javascript')
    representation_store_filename(representation, 'javascript','js', was)
  end

  def self.stylesheet_representation_store_filename(representation, was = false)
    raise 'AssetUploader types must include stylesheet' unless types.index('stylesheet')
    representation_store_filename(representation, 'stylesheet','css', was)
  end

  private

  # Returns full representation path name for specified asset_type
  def self.representation_store_filename(representation, asset_type, extension, was)
    dirname = absolute_prefix_for_representation_type(representation.client_application, asset_type, representation.kind_for_assets)
    FileUtils.mkdir_p dirname
    File.join dirname, "#{representation.slug(was)}.#{extension}"
  end
end
