class AudioUploader < BaseUploader
  def extension_white_list
    YavuSettings.media.allowed_extensions.audio
  end
end