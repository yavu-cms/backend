class BaseUploader < CarrierWave::Uploader::Base
  storage :file

  #ditributing files in to avoid file system issues. if this parameter is changed every allready stored file wont be found by carrierwave. be warned.
  def store_dir
    "#{ model.id % 100 }/#{ model.id }"
  end

  #adding file upload digest to filename
  def filename
    extension = ::File.extname(current_path)
    @name ||= "#{digest}#{extension}" if original_filename.present? and super.present?
  end

  def digest
    var = :"@#{mounted_as}_hexdigest"
    model.instance_variable_get(var) or model.instance_variable_set(var, ::Digest::MD5.file(current_path).hexdigest)
  end
end
