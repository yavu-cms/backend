class ComponentAssetUploader < AssetUploader

  # Helper for absoulete_prefix_for_type and store_dir_prefix
  def self.prefix_for_type(type)
    type.pluralize.parameterize '_'
  end

  # Helper for ClientApplication#asset_configuration
  def self.absolute_prefix_for_type(type)
    File.join root, prefix_for_type(type)
  end

  def store_dir_prefix
    self.class.prefix_for_type(model.type)
  end

  def store_dir_suffix
    model.component.slug
  end
end
