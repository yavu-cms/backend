class AssetUploader < CarrierWave::Uploader::Base
  # Supported assets are configured through YavuSettings·
  # global configurations: settings.yml
  #   assets:
  #     types:
  #       stylesheets:
  #         directory: dir_for_stylesheets (relative to configuration folder)
  #         allowed_extensions:
  #           - css
  #           - sass
  #           - scss
  #

  include CarrierWave::MimeTypes
  include CarrierWave::ImageOptimizer

  storage :file

  process :update_model
  process :set_content_type
  process :optimize, if: Proc.new { Rails.env.production? }

  def self.types
    _types.keys
  end

  def self.directory_for(type)
    raise ArgumentError.new "Directory for type '#{type}' can't be retrieved" unless type && _types.has_key?(type)
    _types[type][:directory] || type.pluralize
  end

  def self.extension_white_list
    types.map {|x| _types[x][:allowed_extensions] }.flatten
  end

  def self.editables_asset
    %w(stylesheet javascript)
  end

  def self.empty_asset_extensions
     editables_asset.inject([]) { |acc, asset_type| acc + _complete_types[asset_type][:allowed_extensions] }
  end

  def update_model
    model.type = self.class.type_by_extension(file_extension)
    model.content_type = file.content_type
  end

  def extension_white_list
    self.class.extension_white_list
  end

  def store_dir
    File.join %W(#{store_dir_prefix} #{store_dir_suffix})
  end

  def store_dir_prefix
    raise NotImplementedError.new('subclasses must implement this method')
  end

  def store_dir_suffix
    raise NotImplementedError.new('subclasses must implement this method')
  end

  protected
    def self.file_extension(filename, strip = true)
      File.extname(filename).gsub(/^\./, '')
    end

    def file_extension
      self.class.file_extension(file.filename)
    end


  private
    def self.flatten_types(h)
      h.each do |k, v|
        v[:allowed_extensions].map! do |x|
          extension = file_extension x
          extension.blank? ? x : extension
        end
      end
    end

    def self._types
      @@types ||= flatten_types YavuSettings.assets.types.to_hash.with_indifferent_access
    end

    def self._complete_types
      @@complete_types ||= YavuSettings.assets.types.to_hash.with_indifferent_access
    end

    def self.type_by_extension(extension)
      types.detect { |type| _types[type][:allowed_extensions].map(&:downcase).include? extension.downcase }
    end
end
