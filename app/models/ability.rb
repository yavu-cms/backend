class Ability
  include CanCan::Ability

  # See the wiki for details: https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  def initialize(user = nil)
    return unless user.present?

    alias_action :search, :search_by_section_and_edition, :back, :active_edition, :editions,
                 :sections, :tree, :autocomplete, :find, :media_by_article, :preview, :notify_clients, :update_preview,
                 :edit_all,
                 to: :read
    alias_action :combine, :combine_update, :separate, :become_default, :make_default, :enable, :disable,
                 :edit_content, :update_content, :rename, :update_name, :compile_assets, :dissasociate_representations,
                 :toggle_as, :update_all, :extra_attributes_management, :blacklist, :unblacklist, :copy,
                 to: :update
    alias_action :copy, :create_copy, :media,
                 to: :create

    # Establish the basic permissions for any logged in user
    establish_basic_permissions(user)

    # Establish permissions for the user based on his/her role(s)
    self.class.permissions_proxy.establish_for(user, self)
  end

  def self.permissions_proxy
    Permission
  end

  protected

  # Establishes the basic permissions for any logged in user.
  # These basic permissions are:
  #   * read/update its own profile
  #   * search for tags
  def establish_basic_permissions(user)
    can :profile, user
    can :search, [Tag, Medium, Edition]
    can :autocomplete, Medium
    can :manage, Shortcut, user_id: user.id
  end
end
