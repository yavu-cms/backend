class ElasticSearchQuery
  attr_accessor :text, :sort, :criterias, :filters

  def initialize text = "", sort = "_score", criterias = nil, filters = nil
    @text= text
    @sort= sort
    @criterias= criterias || [:tag_field, :match_phrase, :match_words]
    @filters= filters || {}
  end

  def build
    { query: { filtered: { query: { bool: { should: queries } }, filter: { bool: { must: filters_methods } } } }, highlight: { fields: { title: {}, body: {}, lead: {}, heading: {} } }, sort: sort_by }
  end

  def queries
    @criterias.map do |criteria|
      send "#{criteria}_query"
    end
  end

  def filters_methods
    @filters.map do |key, value|
      send "#{key}_filter"
    end
  end

  private
    def sort_by
      case @sort
        when 'date_asc'
          { 'edition.date' => { order: 'asc' } }
        when 'date_desc'
          { 'edition.date' => { order: 'desc' } }
        when 'rank'
          '_score'
        else
          '_score'
      end
    end

    def date_filter
      date_object = filters[:date].to_date rescue nil
      Hash[term: { 'edition.date' => date_object }] if date_object.present?
    end

    def section_filter
      section_name = filters[:section]
      Hash[term: { 'section.name' => section_name }] if section_name.present?
    end

    def news_source_filter
      news_source_name = filters[:news_source]
      Hash[term: { 'news_source' => news_source_name }] if news_source_name.present?
    end

    def tag_field_query
      Hash[
        match: {
          'tags.name' => {
            query: @text,
            type: 'phrase',
            boost: 10
          }
        }
      ]
    end

    def match_phrase_query
      Hash[
        multi_match: {
          query: @text,
          type: 'phrase',
          fields: [ 'title^3', 'lead^2', 'body', 'heading' ],
          boost: 10
        }
      ]
    end

    def match_words_query
      Hash[
        multi_match: {
          query: @text,
          operator: 'and',
          type: 'best_fields',
          fields: [ 'title^3', 'lead^2', 'body', 'heading' ],
        }
      ]
    end
end
