class Column < ActiveRecord::Base
  include RenderableWithChildren

  serialize :configuration, Hash

  belongs_to :row

  has_many :component_configurations,
    dependent: :destroy,
    class_name: 'BaseComponentConfiguration',
    after_add: :update_representation_assets,
    after_remove: :update_representation_assets

  delegate :representation, to: :row

  validates :row, presence: true
  validates :order, presence: true
  belongs_to :view, dependent: :delete

  default_scope { order('columns.order ASC') }

  def default_view
    representation.default_column_view
  end

  def children
    component_configurations
  end

  def children=(component_configurations)
    self.component_configurations = component_configurations
  end

  def parent
    row
  end

  def parent=(row)
    self.row = row
  end

  # Return a copy of this column, preserving same order
  # and copying component_configurations. Row it must be set to nil
  # See test case to understand what I'm exactly doing
  def copy
    copy = dup except: :row_id, include: :view
    copy.component_configurations = component_configurations.map(&:copy).each { |x| x.column = copy }
    copy
  end

  private

  def update_representation_assets(component_configuration)
    representation.update_assets! if row && representation
  end
end
