class Medium < ActiveRecord::Base
  include Taggable, FindableInOrder

  # Versions
  has_paper_trail

  has_many :medium_media_galleries, dependent: :restrict_with_exception
  has_many :media_galleries, through: :medium_media_galleries
  has_many :article_media, dependent: :restrict_with_exception
  has_many :articles, through: :article_media
  has_many :component_values, as: :referable, dependent: :restrict_with_error
  has_many :supplements, foreign_key: 'logo_id', dependent: :restrict_with_error

  # Callbacks
  after_save :touch_related
  after_touch :touch_related
  after_destroy :remove_upload_dir
  before_create :set_default_date,  if: ->{ date.nil? }
  # Validations
  validate  :correct_format_for_date
  validate  :type_is_not_changed, on: :update
  validates_length_of %i[type small_version medium_version big_version xbig_version xsmall_version name credits ], maximum: 255

  set_api_resource id: :id,
    name: :name,
    date: :date,
    credits: :credits,
    url: ->(x) {},
    small_url: ->(x) {},
    medium_url: ->(x) {},
    big_url: ->(x) {}


  # Return all the available media types
  def self.types
    [ImageMedium, FileMedium, EmbeddedMedium, AudioMedium]
  end

  # Search for articles using a query hash, which may contain:
  #   * type [String] specific type of media to search for
  #   * date [String] a date to filter the media by
  #   * article_id [integer] the ID of an article to scope the search only to its related media
  #   * query [String] string to be searched amongst the related models (tags and articles)
  def self.search(params = {})
    all.tap do |query|
      query.where!(type: params[:type]) unless params[:type].blank?
      query.where!(date: params[:date]) unless params[:date].blank?
      unless params[:article_id].blank?
        query.includes!(:articles).where!(article_media: { article_id: params[:article_id] })
      end
      unless params[:query].blank?
        query.includes!(:tags).where!('tags.name LIKE :name OR media.name LIKE :name', name: "%#{params[:query]}%")
      end
    end
  end

  # Alternate setter to enable setting the value for the date attribute from a string.
  def date_string=(string)
    self.date = string.to_date unless string.blank?
  rescue ArgumentError
    @invalid_date = true
  end

  # Return the filename for this medium, if any
  def filename
    file.filename unless file.nil?
  end

  # Used for distinguishing which media should be visible in the frontend application
  # A hint: all of it!
  # @see ComponentFieldModel#value
  # @see ComponentFieldMultiModel#value
  def visible?
    true
  end

  # Checks if a medium is part of an active edition
  def published?
    articles.joins(:edition).where('editions.is_active' => true).any?
  end

  def unpublished?
    !published?
  end

  def in_use?
    medium_media_galleries.any? || article_media.any? || component_values.any? || supplements.any?
  end

  private
    # Validate that the value entered for +date+ has the correct format (%F, "2014-01-01" for instance)
    def correct_format_for_date
      errors.add(:date, I18n.t('activerecord.errors.dates.format')) if @invalid_date
    end

    # Before-create callback to set a default date for the medium
    def set_default_date
      self.date = Date.today
    end

    # Validate that the media type is not changed on update and reset it if needed
    def type_is_not_changed
      if type_changed? && !type_was.nil?
        self.type = type_was
        errors.add(:type, I18n.t('activerecord.errors.models.media.attributes.type.wrong_type'))
      end
    end

    # After-destroy callback to remove the uploads directory for this medium
    def remove_upload_dir
      store_dir = File.dirname(file.path)
      FileUtils.remove_dir(store_dir, force: true)
    end

    def touch_related
      article_media.each &:touch
    end
end
