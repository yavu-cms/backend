class AudioMedium < Medium
  # Validations
  validates :file, presence: true

  mount_uploader :file, AudioUploader

  set_api_resource id: :id,
    url: :file_url

  def url
    file_url
  end

  def thumbnail
    file_url
  end

end
