class ImageMedium < Medium
  serialize :dimensions, Hash

  # Callbacks
  after_initialize :set_default_versions
  before_create :create_versions, if: :valid?
  after_update :recreate_versions
  # Validations
  validates :xsmall_version, :small_version, :medium_version, :big_version, :xbig_version, format: {with: /\A\d+x\d+\Z/}
  validates :xsmall_version, :small_version, :medium_version, :big_version, :xbig_version, presence: true
  validates :file, presence: true

  # CarrierWave uploader
  mount_uploader :file, ImageUploader

  set_api_resource id: :id,
    name: :name,
    date: :date,
    credits: :credits,
    url: :file_url,
    xsmall_url: :xsmall_url,
    xsmall_dimensions: :xsmall_dimensions,
    small_url: :small_url,
    small_dimensions: :small_dimensions,
    medium_url: :medium_url,
    medium_dimensions: :medium_dimensions,
    big_url: :big_url,
    big_dimensions: :big_dimensions,
    xbig_url: :xbig_url,
    xbig_dimensions: :xbig_dimensions

  %i(xsmall small medium big xbig).each do |version|
    send :define_method, :"#{version}_url" do
      file_url version
    end

    send :define_method, :"#{version}_dimensions" do
      dimensions[version] || {}
    end
  end

  alias_method :thumbnail, :small_url

  def url
    file_url
  end

  def vectorial?
    !!file.file.file.match(/svg$/i)
  end

  def default_url_for_preview
    if vectorial?
      file_url.to_s
    else
      file_url(:small).to_s
    end
  end

  private

  # Force the creation of the versions for this ImageMedium
  def create_versions
    self.file = file.file
  end

  # Forces the creation and overwrites of the versions that have
  # changed since the last time this ImageMedium was updated
  def recreate_versions
    changed = changed_versions
    file.recreate_versions! *changed if changed.any?
  end

  # Return which versions have changed since last save
  def changed_versions
    %i(xsmall small medium big xbig).inject([]) do |changed, size|
      changed << size if send(:"#{size}_version_changed?")
      changed
    end
  end

  # After-initialize callback to set any missing value for the different
  # +*_version+ attributes
  def set_default_versions
    self.xsmall_version = default_versions['media_versions.xsmall'] if xsmall_version.blank?
    self.small_version = default_versions['media_versions.small'] if small_version.blank?
    self.medium_version = default_versions['media_versions.medium'] if medium_version.blank?
    self.big_version = default_versions['media_versions.big'] if big_version.blank?
    self.xbig_version = default_versions['media_versions.xbig'] if xbig_version.blank?
  end

  # Return the default versions for any ImageMedium, which are taken
  # from the settings. This method performs a simple transformation to the
  # settings so that the versions are returned as a simple hash like:
  #
  #   { small: '<SMALL VALUE>', medium: '<MEDIUM VALUE>', big: '<BIG VALUE>' }
  #
  # This method implements a very simple caching strategy so that subsequent
  # calls to it won't hit the Setting class again to fetch de default values
  def default_versions
    @default_versions ||= fetch_default_versions.presence || self.class.basic_default_versions
  end

  # This method should be called just once to fetch from the Setting class
  # the default versions for ImageMedium objects
  def fetch_default_versions
    Setting.get_all("media_versions.")
  end

  def self.basic_default_versions
    {
      'media_versions.xbig'   => '932x932',
      'media_versions.big'    => '576x576',
      'media_versions.medium' => '392x392',
      'media_versions.small'  => '272x272',
      'media_versions.xsmall' => '212x212'
    }
  end
end
