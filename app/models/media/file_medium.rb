class FileMedium < Medium
  # Validations
  validates :file, presence: true

  mount_uploader :file, BaseUploader

  set_api_resource id: :id,
    url: :url,
    extension: :extension

  def url
    file_url
  end

  def thumbnail
    file_url
  end

  def extension
    file.file.extension rescue ''
  end

end
