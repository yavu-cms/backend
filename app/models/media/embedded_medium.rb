class EmbeddedMedium < Medium
  # Callbacks
  skip_callback :destroy, :after, :remove_upload_dir
  # Validations
  validates :content, presence: true

  set_api_resource id: :id,
    content: :content,
    name: :name,
    is_video: :video?,
    is_youtube: :youtube?,
    is_vimeo: :vimeo?,
    is_jw: :jw?

  def thumbnail
    content
  end

  def filename
    name
  end

  def video?
    youtube? || vimeo? || jw?
  end

  def youtube?
    is_from? /youtube\.com/
  end

  def vimeo?
    is_from? /vimeo\.com/
  end

  def jw?
    !!(content =~ /^<(script|iframe) src="\/\/content\.jwplatform\.com.*".*><\/(script|iframe)>$/)
  end

  # An embedded does not have a URL by default
  # except if it is a youtube or vimeo video.
  # In that case, the URL is in the 'content' field
  def url
    content if youtube? || vimeo?
  end

  private

  def is_from?(site)
    !!(URI.parse(content).host =~ site)
  rescue URI::InvalidURIError
    false
  end
end
