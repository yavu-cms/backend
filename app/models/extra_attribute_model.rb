class ExtraAttributeModel < ActiveRecord::Base
  belongs_to :model, polymorphic: true, touch: true
  serialize :values, Hash
  validate :model, presence: true

  # Add getter and setter foreach extra attribute defined
  def method_missing(meth, *args, &block)
    # First of all, we must give delegate the call to AR because
    # a great part of AR working relies on this method. After completed
    # this step, now we can hook up into it and do whatever we want.
    super
  rescue NoMethodError => e
    method = meth.to_s.gsub(/=$/,'')
    attribute = ExtraAttribute.find_by name: method
    if attribute.try(:is_for?, my_model_class)
      case args.count
      when 0 then
        return get_value(method) unless meth.match(/=$/)
      when 1 then
        return set_value(method, attribute, *args) if meth.match(/=$/)
      end
    end
    raise e
  end

  # Only shows the extra attribute that are allowed
  # for the current model type
  def allowed_extra_attributes
    ExtraAttribute.for my_model_class
  end

  def to_hash
    Hash[allowed_extra_attributes.map { |ea| [ea.name, get_value(ea.name)] }]
  end

  def names
    values.keys.map(&:to_s)
  end

  def allowed_names
    allowed_extra_attributes.map &:name
  end

  def merge(other)
    clone.tap do |c|
      c.values = Hash[ExtraAttribute.where(name: names + other.names, id: allowed_extra_attributes.map(&:id)).map do |e|
        value = self.send(e.name) || other.send(e.name) rescue nil
        [ e.name, value ]
      end] if other
    end
  end

  private

  def my_model_class
    model_type.try(:constantize)
  end

  def get_value(name)
    self.values[name.to_s]
  end

  def set_value(name, attribute, v)
    if attribute.attribute_type.boolean?
      if v.nil?
        self.values.delete name.to_s
      else
        self.values[name.to_s] = ActiveRecord::ConnectionAdapters::Column::TRUE_VALUES.include?(v)
      end
    elsif v.blank?
        self.values.delete name.to_s
    else
        self.values[name.to_s] = v
    end
    attribute_will_change! 'values'
  end
end
