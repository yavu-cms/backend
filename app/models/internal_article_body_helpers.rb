module InternalArticleBodyHelpers
  class << self
    include Rails.application.routes.url_helpers

    # Warning: This method changes model state (the body attribute and maybe creates new articles_media and article_relations)
    def create_new_resources!(model)
      create_articles_media! model
      create_articles_relations! model
    end

    def create_articles_relations!(model)
      model.body.gsub!(INTERNAL_NEW_ARTICLE_TAG_PLACEHOLDER) do |_|
        article_id, caption = $1, $2
        if (article = Article.find(article_id))
          model.articles << article
          "{{article:#{article_id}|caption:#{caption}}}"
        end
      end
    end

    def create_articles_media!(model)
      model.body.gsub!(INTERNAL_NEW_MEDIA_TAG_PLACEHOLDER) do |_|
        medium_id, options = $1, $2
        options = parse_options options
        caption = options[:caption]
        rest_options = unparse_options options.except(:caption)
        article_media = model.article_media.find_or_create_by(medium_id: medium_id) do |media|
          media.for_gallery = false
          media.order = model.article_media.count + 1
          media.caption = caption
        end
        "{{article_media:#{article_media.id}|#{rest_options}}}"
      end
    end

    def backerize_internal_tags(model)
      model.body.gsub(INTERNAL_RESOURCE_TAG_PLACEHOLDER) do |_|
        type, id, options = $1, $2, $3
        case type
        when 'article_media'
          intermedia = article_media(from_model: model, type: type, id: id)
          "<a class=\"article-link-to-medium\" href=\"#{medium_path(intermedia.medium)}\">#{intermedia.caption} (#{intermedia.medium.type})</a>"
        when 'article'
          if (related = model.articles.find_by(id: id)) && related.visible?
            caption = parse_options(options)[:caption].presence || related.title
            "<a class=\"article-link-to-article\" href=\"#{article_path(related)}\">#{caption} (Link)</a>"
          end
        else
          '[Unknown medium type]'
        end
      end
    end

    def fronterize_internal_tags(model)
      model.body.gsub(INTERNAL_RESOURCE_TAG_PLACEHOLDER) do |_|
        type, id, options = $1, $2, $3
        case type
        when 'article_media'
          intermedia = article_media(from_model: model, type: type, id: id)
          medium_tag intermedia, options if intermedia
        when 'article'
          if (related = model.articles.find_by(id: id)) && related.visible?
            caption = parse_options(options)[:caption].presence || related.title
            "<%= link_to \"#{caption}\", related.find { |r| r.article_id == #{id} } %>"
          end
        else
          ''
        end
      end
    end

  private

    INTERNAL_RESOURCE_TAG_PLACEHOLDER    = /{{(\w*):(\d*)\|?(.*)}}/
    INTERNAL_NEW_MEDIA_TAG_PLACEHOLDER   = /{{new_medium\?:(\d*)\|(.*)}}/
    INTERNAL_NEW_ARTICLE_TAG_PLACEHOLDER = /{{new_article\?:(\d*)\|(.*)}}/

    def article_media(from_model: nil, type: nil, id: nil)
      from_model.article_media.find_by(id: id)
    end

    def medium_tag(intermedia, options)
      parsed_options = (options.present? ? parse_options(options) : {}).slice(:size)
      "<%= medium_tag medias.find { |r| r.id == #{intermedia.id} }, #{parsed_options} %>"
    end

    def parse_options(opts_str)
      opts_ary = opts_str.split(',')
      opts_ary.inject({}) do |ret, option|
        ret.merge opt_to_hash(option)
      end
    end

    def unparse_options(opts_h)
      opts_h.to_a.map { |i| i.join ':' }.join ','
    end

    def opt_to_hash(opt_str)
      case opt_str
      when /\Asize:(\w*)\Z/
        { size: $1 }
      when /\Acaption:(.*)\Z/
        { caption: $1 }
      else
        {}
      end
    end
  end
end
