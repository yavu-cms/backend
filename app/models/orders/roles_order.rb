class RolesOrder < BaseModelOrder
  # Class to be ordered.
  def model
    Role
  end

  # Default field to order by.
  def default_field
    'name'
  end
end
