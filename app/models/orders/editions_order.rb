class EditionsOrder < BaseModelOrder
  # Class to be ordered.
  def model
    Edition
  end

  # Available sorting fields
  def sort_fields
    # 'supplement' is an "artificial" parameter used to provided later
    # a more complex mechanism of sorting for editions's supplement
    super + ['supplement', 'edition']
  end

  # Default field to order by.
  def default_field
    'created_at'
  end

  # Default order.
  def default_order
    :desc
  end

  # Sorts +matches+ using +sort+ configuration.
  def sort_list(matches)
    # if field matches to special parameter 'edition', perform the order for it.
    if sort[:field] == 'edition'
      matches.order "editions.date #{sort[:order]}"
    elsif sort[:field] == 'supplement'
      matches.joins(:supplement).order "supplements.name #{sort[:order]}"
    # if not, do the default sort_list because
    # the parameter is "natural" (belongs to the model)
    else
      super matches
    end
  end
end
