class ComponentsOrder < BaseModelOrder
  # Class to be ordered.
  def model
    BaseComponent
  end

  # Default field to order by.
  def default_field
    'name'
  end
end
