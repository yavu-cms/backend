class VersionsOrder < BaseModelOrder
  # Class to be ordered.
  def model
    Version
  end

  # Default field to order by.
  def default_field
    'created_at'
  end
end
