class TagsOrder < BaseModelOrder
  # Class to be ordered.
  def model
    Tag
  end

  # Default field to order by.
  def default_field
    'name'
  end
end
