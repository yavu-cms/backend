class SectionsOrder < BaseModelOrder
  # Class to be ordered.
  def model
    Section
  end

  # Default field to order by.
  def default_field
    'name'
  end
end
