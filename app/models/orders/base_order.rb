#Implements all the order logic that will be sumistrated to the controllers

class BaseOrder
  attr_accessor :sort

  # Check if not receiving the sorting params via URL
  def no_sort_params?
    @options[:sort][:field].nil? && @options[:sort][:order].nil?
  end

  # Returns a key to map to a specific slot within the store hash. This
  # saves the order configuration for each CRUD list.
  def keyid
    self.class.name
  end

  def default_order
    :asc
  end

  # Returns a params list which is extracted from (in this priority order):
  # 1- Inmediate URL params
  # 2- Session store
  # 3- By default
  def sorting_params
    sorting = { field: default_field, order: default_order }
    store  = @options[:store]
    sorting = store[:sorted_by][keyid] if store && store[:sorted_by] && store[:sorted_by][keyid]
    sorting = @options[:sort] unless no_sort_params?

    sorting
  end

  # Initializes the order with given options
  # e.g. field => 'title'
  #      order => :asc
  def initialize(options)
    @options = { sort: nil }.merge options
    self.sort = sanitize sorting_params

    self
  end

  # Returns the order direction which is NOT.
  def opposite_order
    if     ascendent? then :desc
    elsif descendent? then :asc
    else  default_order
    end
  end

  # Check if the field is sorted with a criteria
  def sorted_by?(field)
    sort[:field] == field
  end

  # Check if the field is ordered by a criteria
  def order?(order)
    sort[:order].to_sym == order
  end

  # Check if the order is ascendent
  def ascendent?
    order? :asc
  end

  # Check if the order is descendent
  def descendent?
    order? :desc
  end

  # Get all the matching records to the current filter criteria ordered by
  # the specification of the params.
  # e.g. matches.order articles.title desc
  # CAN BE OVERRIDED to perform more precising
  # ordenings (for example, when needed ordening in columns which are foreign keys)
  def sort_list(matches)
    matches.order "#{column_prefix}.#{sort[:field]} #{sort[:order]}"
  end

  # Validate and clean up the params of 'sort', returning its sanitized version.
  def sanitize(sort)
    sort[:field] = default_field unless self.sort_fields.include? sort[:field]
    sort[:order] = default_order unless sort[:order] && ([:asc, :desc].include? sort[:order].downcase.to_sym)

    sort
  end

  # Sets the values for this filter to :new_sort.
  # Will persist if the :persist option was set to true when creating
  # this object and a valid store has been provided.
  def sort=(new_sort)
    @sort = new_sort
    if @options[:store] && @options[:persist]
      @options[:store][:sorted_by] ||= {}
      @options[:store][:sorted_by][keyid] = @sort
    end
  end
end
