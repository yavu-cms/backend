class CoversOrder < BaseModelOrder
  # Class to be ordered.
  def model
    Representation
  end

  # Default field to order by.
  def default_field
    'name'
  end
end
