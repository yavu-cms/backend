# Common logic for all order classes. Subclasses may override
# these methods with different order criteria

class BaseModelOrder < BaseOrder

  # Subclasses must implement this
  def model
    raise NotImplementedError
  end

  # Default sort fields.
  def sort_fields
    model.column_names
  end

  # Default column prefix.
  def column_prefix
    model.table_name
  end

  # Default field to order by.
  def default_field
    sort_fields.first
  end
end
