class GalleriesOrder < BaseModelOrder
  # Class to be ordered.
  def model
    Gallery
  end

  # Default field to order by.
  def default_field
    'title'
  end
end
