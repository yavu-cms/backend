class ArticlesOrder < BaseModelOrder
  # Class to be ordered.
  def model
    Article
  end

  def sort_fields
    # 'section' is an "artificial" parameter used to provided later
    # a more complex mechanism of sorting for article's sections
    super + ['section']
  end

  # Default field to order by.
  def default_field
    'time'
  end

  def default_order
    :desc
  end

  # Sorts +matches+ using +sort+ configuration.
  def sort_list(matches)
    # if field matches to special parameter 'section', perform the order for it.
    case sort[:field]
    when 'section'
      matches.joins(:section).order "sections.name #{sort[:order]}"
    # if not, do the default sort_list because
    # the parameter is "natural" (belongs to the model)
    else
      super matches
    end
  end
end
