class RoutesOrder < BaseModelOrder
  def model
    Route
  end

  def sort_fields
    super + %w( representation )
  end

  def default_order
    :desc
  end

  def default_field
    'priority'
  end

  def sort_list(matches)
    case sort[:field]
    when 'representation'
      matches.joins(:representation).order "base_representations.name #{sort[:order]}"
    else
      super matches
    end
  end
end
