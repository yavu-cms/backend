
# = View
#
# A View defines the way some object will be displayed.
#
# This is achieved by defining a +format+ which will be used as a template
# filled with data by the object being represented. See View#render for more
# information.
#
# One View should only be referenced by one and only one object.
#
# Views can belong to a Representation in which case +component_id+ will be nil



class View < ActiveRecord::Base
  include Versionable

  attr_accessor :must_be_alternative
  belongs_to :component,
    foreign_key: 'component_id',
    touch: true,
    class_name: 'BaseComponent'

  has_many :component_configurations,
    dependent: :restrict_with_error

  has_and_belongs_to_many :base_representations

  validates :name, presence: true, length: {maximum: 255}
  validates :format, length: {maximum: 255}

  # Views not only are used for components so uniqueness is valid when a component is set
  validates_uniqueness_of :name, scope: :component_id, unless: 'component_id.nil?'
  validates_uniqueness_of :format, scope: :component_id, unless: 'component_id.nil? || !alternative?'
  validate  :only_one_default, unless: 'component_id.nil? || !default?'
  validate  :presence_of_content_placeholder, if: 'component.nil? && alternative?'
  validate  :is_not_html, if: 'must_be_alternative'

  before_validation :set_blank_value
  before_validation :clean_default_flag, if: 'alternative?'
  before_destroy :check_before_destroy
  # Touch representations only if this view is a representation view
  after_save :touch_representations, if: 'component_id.nil?'

  default_scope { order :name }
  # Scopes to filter CoverArticleComponent views
  scope :for_layout,  -> { where(is_layout: true) }
  scope :for_article, -> { where(is_layout: false) }
  # Scops to filter Alternative views
  scope :component, -> { joins(:component) }
  scope :representation, -> { where(component_id: nil).eager_load(:base_representations) }
  # Alternative views: those not being HTML
  scope :html, -> { where format: 'text/html' }
  scope :alternative, -> { where.not format: 'text/html' }
  # Partial / Non partial scopes
  scope :partials, -> { where 'name like ?', '\_%' }
  scope :templates, -> { where 'name not like ?', '\_%' }

  def cover_view?
    format == 'text/html' && !name.starts_with?('\_')
  end

  def article_view?
    format == 'text/html' && !is_layout?
  end

  PLACEHOLDER_PATTERN            = '[[%s]]'.freeze
  FAVICON_PLACEHOLDER            = (PLACEHOLDER_PATTERN % :favicon).freeze             # [[favicon]]
  STYLESHEETS_PLACEHOLDER        = (PLACEHOLDER_PATTERN % :stylesheets).freeze         # [[stylesheets]]
  JAVASCRIPTS_PLACEHOLDER        = (PLACEHOLDER_PATTERN % :javascripts).freeze         # [[javascripts]]
  EXTRA_JAVASCRIPTS_PLACEHOLDER  = (PLACEHOLDER_PATTERN % :extra_javascripts).freeze   # [[extra_javascripts]]
  SEO_PLACEHOLDER                = (PLACEHOLDER_PATTERN % :seo).freeze                 # [[seo]]
  IMAGE_LINK_PLACEHOLDER         = (PLACEHOLDER_PATTERN % :image_link).freeze          # [[image_link]]
  CONTENT_PLACEHOLDER            = (PLACEHOLDER_PATTERN % :content).freeze             # [[content]]
  ADDITIONAL_HEADERS_PLACEHOLDER = (PLACEHOLDER_PATTERN % :additional_headers).freeze  # [[additional_headers]]

  # Create a new View object to be used as the default for the given kind of elements
  # (either :representation, :row or :column).
  # @see View#default_attributes_for
  # @throws RuntimeError if +kind+ is invalid
  def self.create_default_for(kind)
    create default_attributes_for(kind)
  end

  def to_s
    name.to_s
  end

  def has_placeholder?(placeholder)
    value.include? placeholder
  end

  def has_content_placeholder?
    has_placeholder? CONTENT_PLACEHOLDER
  end

  # Render this view object using the provided arguments to replace any placeholder this
  # view may have. +arguments+ is expected to be a hash with the placeholder names as the
  # keys and their respective values as the values - if any.
  #
  # === Examples
  #
  #     view.value = '<p>[[content]]</p><p>No value specified? The placeholder stays</p>'
  #     view.render
  #       # => '<p>[[content]]</p><p>No value specified? The placeholder stays</p>'
  #
  #     view.value = '<p>[[content]]</p><footer>[[footer]]</footer>'
  #     view.render(content: 'My content', footer: '&copy; 2014')
  #       # => '<p>My content</p><footer>&copy; 2014</footer>'
  def render(arguments = {})
    arguments.inject(value) do |result, (placeholder_key, placeholder_value)|
      placeholder = PLACEHOLDER_PATTERN % placeholder_key
      result.gsub placeholder, placeholder_value
    end
  end

  # Returns wether view's value is blank
  def blank_value?
    value.blank?
  end

  # Template name is tokenized as: snake case, prefixed by component name if any
  # and spaces replaced by underscores
  def template_name
    "#{component && "#{component.slug}/"}#{name.parameterize('_')}"
  end

  def partial?
    name.starts_with? '_'
  end

  def can_be_default?
    !(default? || partial? || alternative?)
  end

  def alternative?
    !GenericRenderer.html?(format)
  end

  def make_default!
    transaction do
      component.default_view.try(:update, default: false)
      self.update(default: true)
    end if can_be_default?
  end

  private
    # Return the attributes hash of the default View for the given kind of element.
    # @throws RuntimeError if +kind+ is invalid
    def self.default_attributes_for(kind)
      {
        representation: {
          name: 'representation_layout',
          value: <<-HTML
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    #{SEO_PLACEHOLDER}
    #{FAVICON_PLACEHOLDER}
    #{ADDITIONAL_HEADERS_PLACEHOLDER}
    #{STYLESHEETS_PLACEHOLDER}
  </head>
  <body>
    #{CONTENT_PLACEHOLDER}
    #{JAVASCRIPTS_PLACEHOLDER}
    #{EXTRA_JAVASCRIPTS_PLACEHOLDER}
  </body>
</html>
          HTML
        },
        row: {
          name: 'row_layout',
          value: "<div>#{CONTENT_PLACEHOLDER}</div>",
        },
        column: {
          name: 'column_layout',
          value: "<div>#{CONTENT_PLACEHOLDER}</div>",
        },
        error: {
          name: 'error_view',
          value: '<div class="error"><h1><%= t "renderer.error.title", default: "Error" %></h1><%= t "renderer.error.message", default: "Some error was produced while processing component" %><div class="message"><%= error %></div></div>'
        }
      }[kind] || raise("Don't know how to create default view for #{kind.inspect}")
    end

    def touch_representations
      base_representations.each &:touch_routes
    end

    def only_one_default
      count = component.views.where(default: true).where.not(id: id).count
      if count > 0
        # Trying to specify a new default when there was one already
        errors.add(:default, I18n.t('activerecord.errors.models.view.default.conflict'))
        false
      end
    end

    # View can be destroyed if it doesn't belong to a component
    # or if it isn't the last view of the component it belongs to
    # And it has no ComponentConfigurations using it
    def check_before_destroy
      (
        component.nil? ||
        component.destroy_view?(self)
      ) && component_configurations.empty? && base_representations.empty?
    end

    def set_blank_value
      self.value = '' if value.nil?
    end

    def clean_default_flag
      self.default = false
      true
    end

    def presence_of_content_placeholder
      errors.add(:value, I18n.t('activerecord.errors.models.representation_view.value.not_present_placeholder')) unless has_content_placeholder?
    end

    def is_not_html
      errors.add(:format, I18n.t('activerecord.errors.models.representation_view.format.not_html')) unless alternative?
    end
end
