require 'nokogiri'

class Weather
  include Mongoid::Document
  include Mongoid::Timestamps

  include Yavu::API::Resource::Resourceable

  store_in collection: 'weathers'

  field :temperature, type: String
  field :wind, type: String
  field :humidity, type: String
  field :description, type: String

  default_scope ->{ asc(:created_at) }

  set_api_resource temperature: :temperature,
                   wind: :wind,
                   humidity: :humidity,
                   description: :description

  # Remove old weather records
  after_create do |_|
    if Weather.count > Setting['weather.records_keep_limit'].to_i
      Weather.first.destroy
    end
  end

  def self.actual_record
    last
  end

  def self.retrieve_and_store_record
    page = Nokogiri::HTML(open(Setting['weather.url_to_scrap']))
    # Span tag with description and temp
    span = page.css('#divPronostico div span')
    description = span.first.text
    temp = span.last.text
    wind = page.css('#divPronostico div div').last.text
    wind = wind.slice(/ .* /).strip + ' Km/h'
    hu = page.css('div.font1')[2].text
    # Processing humidity
    hu.slice!(/Vis: \d* km Hr:/)
    hu.strip!
    create(temperature: temp, wind: wind, humidity: hu, description: description)
  end
end
