module ClientApplicationPermissions
  extend ActiveSupport::Concern

  included do
    # Should also cover any related elements (like frontend_servers, assets, background jobs)
    #
    # Notes:
    #   - Must be able to access ComponentsController#search to fetch the prefix_path for client application assets slugs
    #       i.e.: can(:search, BaseComponent)
    #
    permissions_for :client_application do
      {
        read: can(:read, ClientApplication, :only_associated_client_application),
        manage: [
          can([:update, :remote_management, :notify_clients, :invalidate_routes, :disable, :enable], ClientApplication, :only_associated_client_application),
          can(:create, ClientApplication)
        ],
        design_manage: [
          can([:design, :manage_assets, :manage_representations, :compile_assets, :manage_newsletters, :preview, :update_preview, :sendmail], ClientApplication, :only_associated_client_application),
          can(:manage, [ClientApplicationAsset, BaseRepresentation, Newsletter, View])
        ],
        extra_attributes_management: can([:read, :extra_attributes, :manage_extra_attributes, :extra_attributes_management ], ClientApplication, :only_associated_client_application),
        destroy: can(:destroy, ClientApplication, :only_associated_client_application),
        invalidate_routes: can([:read, :invalidate_routes], ClientApplication, :only_associated_client_application)
      }
    end

    condition :only_associated_client_application do |client_application|
      included_in_workspace? client_application
    end
  end
end
