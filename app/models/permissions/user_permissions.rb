module UserPermissions
  extend ActiveSupport::Concern

  included do
    permissions_for :user do
      {
        read: can(:read, User),
        create: can(:create, User),
        update: can(:update, User),
        destroy: can(:destroy, User, :anyone_but_the_logged_in_user)
      }
    end

    condition :anyone_but_the_logged_in_user do |other_user|
      user.id != other_user.id
    end
  end
end