module GalleryPermissions
  extend ActiveSupport::Concern

  included do
    # @see MediumPermissions which rely on these permissions
    permissions_for :gallery do
      {
        read: can(:read, Gallery),
        create: can([:create, :new_media], Gallery),
        update: {
          unused: can(:update, Gallery, :only_unused_galleries),
          all: can(:update, Gallery, :only_when_gallery_associations_can_be_updated)
        },
        destroy: {
          unused: can(:destroy, Gallery, :only_unused_galleries),
          all: can(:destroy, Gallery, :only_when_gallery_associations_can_be_updated)
        }
      }
    end

    condition :only_unused_galleries do |gallery|
      gallery.unused?
    end

    condition :only_when_gallery_associations_can_be_updated do |gallery|
      gallery.unused? || !gallery.component_values.any? { |cv| cannot? :update, cv.component }
    end
  end
end