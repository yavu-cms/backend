module VersionPermissions
  extend ActiveSupport::Concern

  included do
    permissions_for :version do
      {
        read: can(:read, Version),
        restore: can(:restore, Version)
      }
    end
  end
end
