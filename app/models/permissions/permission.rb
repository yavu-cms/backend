class Permission
  cattr_accessor :user, :ability
  attr_accessor :predicates

  class << self
    # Class-level delegation of ability methods for using them in conditions
    delegate :can?, :cannot?, to: :ability
  end

  SEPARATOR = '/'

  include MediumPermissions,
          ExtraAttributePermissions,
          ClientApplicationPermissions,
          ComponentPermissions,
          CoverPermissions,
          GalleryPermissions,
          # RepresentationPermissions,
          RolePermissions,
          UserPermissions,
          EditionPermissions,
          ArticlePermissions,
          SectionPermissions,
          SettingPermissions,
          SupplementPermissions,
          TagPermissions,
          VersionPermissions,
          SuperAdminPermissions,
          PermissionsDSL

  class << self
    def all
      trace, list = [], []
      mark = lambda do |k, v|
        trace.push k.to_s
        unless v.is_a?(Hash)
          list << trace.dup
        end
      end
      go_back = lambda do |k, v|
        trace.pop
      end
      table.walk pre: mark, post: go_back
      list.map { |permission| permission.join SEPARATOR }
    end

    def establish_for(user, ability)
      self.user = user
      self.ability = ability
      user.permissions.each { |slug| build(slug).apply }
    end

    def build(permission_id)
      begin
        predicates = permission_id.split(SEPARATOR).map(&:to_sym).reduce(table) do |stream, current|
          stream[current] || handle_invalid_permission(permission_id)
        end
      rescue TypeError
        predicates = handle_invalid_permission(permission_id)
      end
      new predicates
    end

    def included_in_workspace?(resource_or_resources)
      resource_or_resources = [resource_or_resources] unless resource_or_resources.respond_to?(:each)
      !resource_or_resources.any? { |resource| !user.included_in_workspace? resource }
    end
  end

  def initialize(predicates)
    self.predicates = predicates.respond_to?(:each) ? predicates : [predicates]
  end

  # Apply the currently configured permission to the provided ability.
  # This will go through any predicates assigned to this permission
  # applying them to the ability.
  # @see PermissionPredicate#apply_to
  def apply
    predicates.each { |predicate| predicate.apply_to(ability, self) }
  end

  protected

  # Handles an invalid permission id received. This will log it to the Rails logger
  # with an ERROR level, and then return an empty set of arguments that will in turn
  # be ignored by Permission#apply_to.
  def self.handle_invalid_permission(permission_id)
    Rails.logger.error('Permission') { "Invalid permission specified. permission_id=#{permission_id}" }
    []
  end
end
