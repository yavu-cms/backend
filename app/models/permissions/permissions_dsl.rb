module PermissionsDSL
  extend ActiveSupport::Concern

  included do
    cattr_accessor(:table) { Hash.new }
  end

  module ClassMethods
    # Allow for using a DSL-like mechanism to define permissions for resources.
    #
    # For instance, one could define permissions for the :admin namespace like this:
    #     permissions_for :admin do
    #       { articles: can(:manage, Article),
    #         media: can(:manage, Medium) }
    #     end
    def permissions_for(key, &block)
      raise "Trying to overwrite permissions for key '#{key.inspect}'" if table.has_key? key
      table[key] = yield
    end

    # Allow for using a DSL-like mechanism to define condition blocks for permissions.
    #
    # For example, if a permission states the following:
    #    permissions_for :article do
    #      { new: can(:create, Article, :only_on_wednesdays) }
    #    end
    #
    # Then one would use +condition+ to define the +:only_on_wednesdays+ condition:
    #    condition :only_on_wednesdays do |article|
    #      Date.today.wednesday?
    #    end
    def condition(key, &block)
      raise "Trying to create permission condition using existing key '#{key.inspect}'" if method_defined? key
      define_method(key) { block }
    end

    # Define an affirmative PermissionPredicate with the given definition.
    #
    # For example, an 'admin' permission may be defined as follows:
    #    permissions_for :admin do
    #      can(:manage, :all)
    #    end
    #
    # @see PermissionPredicate
    def can(*definition)
      build_predicate(definition)
    end

    # Define a negative PermissionPredicate with the given definition.
    # For example, a 'locked' permission may be defined as follows:
    #    permissions_for :locked do
    #      cannot(:manage, :all)
    #    end
    #
    # @see PermissionPredicate
    def cannot(*definition)
      build_predicate(definition).tap do |predicate|
        predicate.negative = true
      end
    end

    private

    # Build a PermissionPredicate with the given definition.
    def build_predicate(definition)
      PermissionPredicate.new(definition)
    end
  end
end