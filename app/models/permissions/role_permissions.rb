module RolePermissions
  extend ActiveSupport::Concern

  included do
    permissions_for :role do
      {
        read: can(:read, Role),
        create: can(:create, Role),
        update: can(:update, Role),
        destroy: can(:destroy, Role)
      }
    end
  end
end