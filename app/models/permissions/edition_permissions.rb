module EditionPermissions
  extend ActiveSupport::Concern

  included do
    permissions_for :edition do
      {
        read: can([:read, :articles], Edition),
        create: can(:create, Edition),
        update: {
          non_active: can(:update, Edition, :only_non_active_editions),
          all: can(:update, Edition),
        },
        change_active: can(:become_active, Edition),
        destroy: {
          non_active: can(:destroy, Edition, :only_non_active_editions),
          all: can(:destroy, Edition),
        }
      }
    end

    condition :only_non_active_editions do |edition|
      !edition.active?
    end
  end
end