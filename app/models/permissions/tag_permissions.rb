module TagPermissions
  extend ActiveSupport::Concern

  included do
    permissions_for :tag do
      {
        read: can(:read, Tag),
        create: can(:create, Tag),
        update: {
          all: can([:update, :blacklist, :unblacklist], Tag, :only_when_tag_associations_can_be_updated),
          unused: can([:update, :blacklist, :unblacklist], Tag, :only_unused_tags)
        },
        destroy: {
          all: can(:destroy, Tag, :only_when_tag_associations_can_be_updated),
          unused: can(:destroy, Tag, :only_unused_tags)
        }
      }
    end

    condition :only_unused_tags do |tag|
      tag.unused?
    end

    condition :only_when_tag_associations_can_be_updated do |tag|
      tag.unused? || !(
        tag.articles.any? { |article| cannot? :update, article } ||
        tag.media.any? { |medium| cannot? :update, medium }
      )
    end
  end
end