module SectionPermissions
  extend ActiveSupport::Concern

  included do
    permissions_for :section do
      {
        read: can(:read, Section),
        create: can(:create, Section),
        update: {
          associated: can(:update, Section, :only_associated_sections),
          all: can(:update, Section)
        },
        destroy: {
          associated: can(:destroy, Section, :only_associated_sections),
          all: can(:destroy, Section)
        }
      }
    end

    condition :only_associated_sections do |section|
      included_in_workspace? section
    end
  end
end