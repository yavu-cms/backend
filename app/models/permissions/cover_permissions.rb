module CoverPermissions
  extend ActiveSupport::Concern

  included do
    permissions_for :cover do
      {
        read: [
          can(:read, :cover),
          can(:read, ClientApplication, :only_associated_client_application_from_workspace),
          can([:read, :draft_preview_and_publish, :preview, :apply], Representation, :only_cover_representations_from_workspace)
        ],
        update: {
          unused: [
            can(:update, :cover),
            can(:read, ClientApplication, :only_associated_client_application_from_workspace),
            can(:update, Representation, :only_unused_cover_representations_from_workspace)
          ],
          all: [
            can(:update, :cover),
            can(:read, ClientApplication, :only_associated_client_application_from_workspace),
            can(:update, Representation, :only_cover_representations_from_workspace),
            can(:update, ComponentValue, :only_from_components_from_workspace)
          ]
        }
      }
    end

    condition :only_from_components_from_workspace do |component_value|
      cc = component_value.try(:component_field).try(:component_configuration)
      representation = cc && cc.representation
      !!representation &&
      included_in_workspace?(representation.client_application) &&
      included_in_workspace?(representation)
    end

    condition :only_cover_representations_from_workspace do |representation|
      representation.cover? &&
      included_in_workspace?(representation.client_application) &&
      included_in_workspace?(representation)
    end

    condition :only_unused_cover_representations_from_workspace do |representation|
      representation.cover? &&
      representation.unused? &&
      included_in_workspace?(representation.client_application) &&
      included_in_workspace?(representation)
    end

    condition :only_associated_client_application_from_workspace do |client_application|
      included_in_workspace? client_application
    end
  end
end
