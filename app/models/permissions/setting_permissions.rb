module SettingPermissions
  extend ActiveSupport::Concern

  included do
    permissions_for :setting do
      {
        manage: can(:manage, Setting)
      }
    end
  end
end