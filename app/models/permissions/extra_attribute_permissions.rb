module ExtraAttributePermissions
  extend ActiveSupport::Concern

  included do
    permissions_for :extra_attribute do
      {
        manage: can(:manage, ExtraAttribute)
      }
    end
  end
end