module ArticlePermissions
  extend ActiveSupport::Concern

  included do
    permissions_for :article do
      {
        read: can(:read, Article, :only_articles_from_workspace),
        create: {
          invisible: [
            can(:create, Article, :only_invisible_articles_from_workspace),
            can(:find, Medium)
          ],
          all: [
            can(:create, Article, :only_articles_from_workspace),
            can(:find, Medium)
          ],
        },
        update: {
          invisible: [
            can(:update, Article, :only_invisible_articles_from_workspace),
            can(:find, Medium)
          ],
          all: [
            can(:update, Article, :only_articles_from_workspace),
            can(:find, Medium)
          ]
        },
        destroy: {
          invisible: can(:destroy, Article, :only_invisible_articles_from_workspace),
          all: can(:destroy, Article, :only_articles_from_workspace)
        }
      }
    end

    condition :only_articles_from_workspace do |article|
      article.section.blank? || included_in_workspace?(article.section)
    end

    condition :only_invisible_articles_from_workspace do |article|
      !article.visible? && (article.section.blank? || included_in_workspace?(article.section))
    end
  end
end
