module ComponentPermissions
  extend ActiveSupport::Concern

  included do
    permissions_for(:component) do
      {
        read: [
          can([:read, :views, :assets, :translations, :services], [BaseComponent, ComponentAsset, ComponentTranslation, ComponentService]),
          can(:read, View, :only_component_views)
        ],
        manage: [
          can([:create, :update, :views, :assets, :translations, :services], BaseComponent),
          can(:manage, [ComponentAsset, ComponentTranslation, ComponentService]),
          can(:manage, View, :only_component_views),
        ],
        destroy: can(:destroy, BaseComponent)
      }
    end

    condition :only_component_views do |view|
      view.component.present?
    end
  end
end
