module MediumPermissions
  extend ActiveSupport::Concern

  included do
    permissions_for :medium do
      {
        read: can(:read, Medium),
        create: can([:create, :new_audio, :new_embedded, :new_file, :new_image], Medium),
        update: {
          all: can(:update, Medium, :only_when_medium_associations_can_be_updated),
          unused: can(:update, Medium, :only_unused_media),
        },
        destroy: {
          all: can(:destroy, Medium, :only_when_medium_associations_can_be_updated),
          unused: can(:destroy, Medium, :only_unused_media)
        }
      }
    end

    condition :only_unused_media do |medium|
      !medium.in_use?
    end

    condition :only_when_medium_associations_can_be_updated do |medium|
      !medium.in_use? || !(
        medium.media_galleries.any? { |mg| cannot? :update, mg } ||
        medium.component_values.any? { |cv| cannot? :update, cv } ||
        medium.articles.any? { |a| cannot? :update, a }
      )
    end
  end
end