module SupplementPermissions
  extend ActiveSupport::Concern

  included do
    permissions_for :supplement do
      {
        read: can(:read, Supplement),
        create: can(:create, Supplement),
        update: can(:update, Supplement),
        destroy: can(:destroy, Supplement)
      }
    end
  end
end