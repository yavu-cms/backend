class PermissionPredicate
  attr_accessor :negative, :definition
  attr_reader :block

  def initialize(definition)
    self.definition = definition
  end

  # Apply this predicate to the provided +ability+, using the +permission+
  # as a support object for conditions evaluation.
  def apply_to(ability, permission)
    apply_definition ability, fetch_block_from_definition(permission) if applicable?
  end

  # Answer whether this predicate is applicable (i.e., it has a non-empty definition)
  def applicable?
    definition.any?
  end

  protected

  # Actually apply this definition to the +ability+, with an optional
  # condition block.
  #
  # @see PermissionPredicate#apply_to
  # @see PermissionPredicate#fetch_block_from_definition
  def apply_definition(ability, block = nil)
    if block.present?
      ability.send can_or_cannot, *definition, &block
    else
      ability.send can_or_cannot, *definition
    end
  end

  # Return the method that needs to be used for applying this predicate
  # to the ability instance.
  # If this predicate is negative, :cannot should be used, whereas :can
  # should be used if this predicate is affirmative.
  def can_or_cannot
    negative ? :cannot : :can
  end

  # Fetch (and store) the block from this predicate's definition.
  # A block may come as the last argument, or a condition symbol
  # may be provided, in which case it should be a valid method name
  # defined in +permission+.
  def fetch_block_from_definition(permission)
    @block ||= if block_provided?
      definition.pop
    elsif condition_provided?(permission)
      permission.send definition.pop
    end
  end

  # Answer whether a block is provided as the last element in the definition
  def block_provided?
    definition.last.respond_to? :call
  end

  # Answer whether a condition symbol is provided as the last element in the definition
  def condition_provided?(permission)
    definition.count > 2 && definition.last.is_a?(Symbol) && permission.respond_to?(definition.last, true)
  end
end