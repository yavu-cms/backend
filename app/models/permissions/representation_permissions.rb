module RepresentationPermissions
  extend ActiveSupport::Concern

  included do
    # Should also cover its draft/backups/(cover_)component_configurations/fields/values/etc
    #
    # Notes:
    #   - Must be able to access ComponentsController#configuration_form in order to fetch configuration forms
    #     for ComponentConfiguration instances in the representation editor.
    #       i.e.: can(:configuration_form, BaseComponent)
    #
    raise 'TODO: Implement these permissions'
    # is necessary to implement this permission?
  end
end