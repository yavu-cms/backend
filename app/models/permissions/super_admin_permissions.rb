module SuperAdminPermissions
  extend ActiveSupport::Concern

  included do
    # 'super_admin' gives complete access to every resource on the application
    permissions_for(:super_admin) { can(:manage, :all) }
  end
end