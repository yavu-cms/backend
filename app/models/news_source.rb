class NewsSource < ActiveRecord::Base
  include ExtraAttributeModelable

  has_many :supplements
  has_many :client_applications

  has_many :articles, through: :supplements
  has_many :sections, through: :supplements

  validates :name, presence: true

  def self.default
    find_by is_default: true
  end

  def self.default_supplement
    default.default_supplement
  end

  def default_supplement
    supplements.find_by is_default: true
  end

  def to_s
    name
  end
end
