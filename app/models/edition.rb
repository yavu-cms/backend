class Edition < ActiveRecord::Base
  include ExtraAttributeModelable, Versionable

  belongs_to :supplement
  has_one    :news_source, through: :supplement

  has_many   :articles, dependent: :restrict_with_error
  # Callbacks
  after_update  :touch_articles, if: ->{ is_visible_changed? }
  before_destroy :become_inactive!
  # Validations
  validates :supplement, presence: true
  validate  :presence_of_name_or_date
  validate  :correct_format_for_date
  validates :name, length: {maximum: 255}

  # Scope to work with visible editions only
  scope :visible, -> { where(is_visible: true) }
  # Find any editions matching the given query either on ther name or on their date attributes
  scope :by_name_or_date, ->(query) { visible.where('name = :query OR date = :query', query: query) }
  # Try to find the edition with the given ID, and if it fails, return the active edition for the supplement received as argument.
  scope :find_or_active, ->(id, supplement) { find_by(id: id) || supplement.active_edition }

  accepts_nested_attributes_for :extra_attribute_model

  set_api_resource id: :name,
    name: :name,
    date: :date

  # Search for any edition(s) matching the specified criteria
  def self.search(params = {})
    return none if params[:query].blank?
    date = params[:query].to_date rescue nil
    where('date = :date OR name like :query', date: date, query: "%#{params[:query]}%").order(date: :desc)
  end

  def to_s
    "#{supplement} - #{name_or_date} (#{news_source})"
  end

  # Used for distinguishing which editions should be visible in the frontend application
  # @see ComponentFieldModel#value
  # @see ComponentFieldMultiModel#value
  def visible?
    is_visible?
  end

  # Alternate setter to enable setting the value for the date attribute from a string.
  # Note that this string is expected to have +%F+ format ("YYYY-mm-dd").
  def date_string=(string)
    self.date = string.to_date unless string.blank?
  rescue ArgumentError
    @invalid_date = true
  end

  # Return the name of this edition if set, or its date otherwise
  def name_or_date
    name.blank? ? date_with_format : name
  end

  # Return the date of this edition formatted using the provided +format+.
  def date_with_format(format = :long)
    I18n.l(date.to_date, format: format) rescue date
  end

  # Make this edition the active one for its supplement
  def become_active!
    transaction do
      supplement.replace_active_edition_with! self
      self.is_active = true
      save
    end
  end

  def active?
    is_active
  end

  # Make this edition not active for its supplement
  # is the default
  def become_inactive!
    if is_active
      transaction do
        supplement.clear_active_edition!
        self.is_active = false
        save
      end
    end
    true
  end

  private
    # Validate that either the name or the date are present
    def presence_of_name_or_date
      errors.add(:name, :name_or_date_blank) if name.blank? && date.blank?
    end

    # Validate that the value entered for +date+ has the correct format (%F, "2014-01-01" for instance)
    def correct_format_for_date
      errors.add(:date, I18n.t('activerecord.errors.dates.format')) if @invalid_date
    end

    def touch_articles
      articles.find_each &:touch
    end
end
