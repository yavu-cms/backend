# Documentation for Tag class
#
# == Summary
#
# An instance of this class is created with:
# * name
# * slug
#
# == Example
#
# tag = Tag.create(:tag, name:'aName')
#
# === Class Tag
#
#
class Tag < ActiveRecord::Base
  MAX_SLUG_LENGTH = 240
  MINIMUM_NAME_LENGTH = 3

  include Versionable

  # Associations
  has_and_belongs_to_many :articles
  has_and_belongs_to_many :media
  belongs_to              :combiner, class_name: 'Tag'
  has_many                :combined, class_name: 'Tag',
                                     foreign_key: 'combiner_id',
                                     dependent: :destroy
  has_many                :sections, through: :articles
  # Callbacks
  before_create :set_slug
  after_create :set_slug_postfix
  before_validation :squish_name
  # Validations
  validates :name, presence: true, length: { minimum: MINIMUM_NAME_LENGTH, maximum: 255 }, uniqueness: true
  validates :slug, uniqueness: true
  # Scopes
  # 'relation' can be :articles or :media
  scope :not_used_in, -> (relation) { includes(relation).where(relation => { id: nil }) }
  scope :used_in,     -> (relation) { eager_load(relation).where.not(relation => { id: nil }) }
  scope :not_used,    -> { where(id: not_used_in(:articles) & not_used_in(:media)) }
  scope :used,        -> { where(id: used_in(:articles) + used_in(:media)) }
  scope :root,        -> { where(combiner_id: nil) }
  scope :visible,     -> { root.where(hidden: false) }
  scope :hidden,      -> { where(hidden: true) }
  default_scope { order(:name) }

  set_api_resource id: :slug,
    name: :name

  def used?
    articles.any? || media.any?
  end

  def unused?
    !used?
  end

  # Returns all tags least sent as parameter.
  def self.but(*tags)
    where.not(id: tags)
  end

  # Returns a collection of tags that meet the parameter comparing name
  # with like if no parameter is passed all tags are returned.
  def self.search(params = {})
    scope = all
    scope = scope.where('name like :query', query: "%#{params[:query]}%") unless params[:query].blank?
    scope = scope.but(params[:except]) unless params[:except].blank?
    scope
  end

  # Return a comma-delimited string with the NAMEs of the combined tags
  # associated to this tag
  def serialized_combined
    combined.collect(&:name).join(',')
  end

  # Add tags to set tags combined with this, and tells each of
  # the new combined update their references via #combined_with!
  def combines!(tags)
    combined << tags

    tags = [tags] unless tags.respond_to? :each
    tags.each { |tag| tag.combine_with! self }
  end

  # Returns the name after apply the squish method if name is not nil,
  # name.squish =>
  #   First removing all whitespace on both ends of the string,
  #   and then changing remaining consecutive whitespace groups into one space each.
  def squish_name
    self.name = name.squish if name
  end

  # Update all references to this tag so that they now point at :other_tag.
  def combine_with!(other_tag)
    other_tag.combines! combined unless combined.empty?

    other_tag.articles << articles.where.not(id: other_tag.articles)
    articles.clear
  end

  # Isolated this tag and persists
  def separate!
    self.combiner = nil
    save
  end

  def to_s
    name.to_s
  end

  # Detects tags whose name is similar to the name of this label
  # The delta indicates how many characters from the end of this
  # Name of the label can be removed to search for other labels
  # Similar names.
  #
  # Example::
  #    tag = Tag.find(9)
  #    tag.name
  #      # => 'Educational'
  #    tag.similar 5
  #      # => ['Eductionality, Education']
  def similar(delta = 3)
    search = name[0..-delta]
    Tag.visible.where('name like ?', "%#{search}%").but(self)
  end

  protected

  # Before-create callback that generates a slug for this tag if
  # none has been defined yet. The slug will be truncated to a maximum of
  # Tag::MAX_SLUG_LENGTH characters
  def set_slug
    self.slug = name.parameterize if slug.blank?
    self.slug = slug.truncate(MAX_SLUG_LENGTH, omission: '', separator: '-') if slug.length > MAX_SLUG_LENGTH
  end

  # After the tag is created and you can get the id of the same,
  # it concatenates the slug
  def set_slug_postfix
    slug << "-#{id}"
  end
end
