class Role < ActiveRecord::Base
  has_and_belongs_to_many :users

  serialize :permissions

  validates :name, presence: true, uniqueness: true, length: {maximum: 255}

  scope :enabled, ->{ where enabled: true }

  def has_permission?(id)
    permissions.try :include?, id
  end

  def to_s
    name
  end
end
