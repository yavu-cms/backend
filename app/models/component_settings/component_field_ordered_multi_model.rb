class ComponentFieldOrderedMultiModel < ComponentFieldMultiModel
  has_many :component_values, -> { ordered },
           foreign_key: 'component_field_id',
           dependent: :destroy,
           autosave: true

  validates :model_class, inclusion: { in: ->(x) { x.class.model_classes.map &:to_s } }, presence: true

  def field_type
    :ordered_multi_model
  end

  def value=(value)
    value = value.split(',').map(&:to_i) if value.is_a?(String)
    self.component_values = value.map.with_index do |(each_value, order), serial_order|
      component_values.build referable_type: model_class, referable_id: each_value, order: order || serial_order, component_field: self
    end
  end

  # Set the value from the given information.
  # Requires +information+ to have :model_class and :default keys set, being the
  # latter an array of hashes with :default and :order set.
  def value_from(information)
    self.model_class = information[:model_class]
    self.value = (information[:default] || []).map { |i| [i[:default], i[:order]] }
  end
end