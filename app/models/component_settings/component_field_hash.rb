class ComponentFieldHash < ComponentField
  serialize :default, Hash

  before_validation :set_default

  def field_type
    :hash
  end

  private

  def sanitize(raw_value)
    if raw_value.is_a? String
      raw_value = eval(raw_value) rescue Hash.new
    end
    raw_value.with_indifferent_access
  end

  def set_default
    self.default = {} if default.nil?
  end
end