class ComponentField < ActiveRecord::Base
  belongs_to :component,
             foreign_key: 'base_component_id',
             touch: true,
             class_name: 'BaseComponent'
  belongs_to :component_configuration,
             touch: true,
             foreign_key: 'base_component_configuration_id',
             class_name: 'BaseComponentConfiguration'

  validates :name, presence: true, uniqueness: { scope: [:base_component_id, :base_component_configuration_id] }
  validates :description, :name, :type, :model_class, length: {maximum: 255}

  cattr_reader :model_dict

  def self.types
    [ComponentFieldScalar, ComponentFieldInteger, ComponentFieldHash, ComponentFieldModel, ComponentFieldMultiModel, ComponentFieldOrderedMultiModel]
  end

  def self.build_from(information, name, extras = {})
    class_for(information[:type]).new(extras.merge(name: name)) do |instance|
      instance.value_from(information)
    end
  end

  # Factory method to return the class for the given type symbol
  # Will throw a RuntimeError if +type+ is not valid.
  def self.class_for(type)
    case type
    when :scalar; ComponentFieldScalar
    when :integer; ComponentFieldInteger
    when :hash; ComponentFieldHash
    when :model; ComponentFieldModel
    when :multi_model; ComponentFieldMultiModel
    when :ordered_multi_model; ComponentFieldOrderedMultiModel
    else raise "Unknown ComponentField type: #{type.inspect}"
    end
  end

  def to_s
    description.presence || name
  end

  def context_name
    "@#{view_name}"
  end

  def view_name
    "settings['#{name}']"
  end

  # Return true if this field represents a default (i.e. it belongs to a component)
  def default?
    component.present?
  end

  # Return the value for this field
  def value
    sanitize(default)
  end

  # Set the value for this field to +value+.
  # This method is a default stub and may be overridden in subclasses.
  def value=(value)
    self.default = sanitize(value)
  end

  # Set the value from the given information.
  # This method is a default stub and may be overridden in subclasses.
  def value_from(information)
    self.default = information[:default]
  end

  # Copy this field overwriting its attributes with the provided +new_attributes+
  def copy(new_attributes)
    dup.tap do |copy|
      self.copy_onto copy
      copy.assign_attributes new_attributes
    end
  end

  def model_class=(view_value)
    write_attribute(:model_class, self.model_dict[view_value.downcase].name)
  end

  def model_class
    self.model_dict[super].try(:name) || super
  end

  protected

  def copy_onto(new_field)
  end

  # Sanitize the raw value to be returned by #value.
  # This method is a default stub and may be overridden in subclasses.
  def sanitize(raw_value)
    raw_value
  end

  @@model_dict = {
    'article' => Article,
    'section' => Section,
    'medium'  => Medium,
    'gallery' => Gallery
  }

  def self.model_classes
    self.model_dict.values
  end
end
