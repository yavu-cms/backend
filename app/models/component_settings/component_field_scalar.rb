class ComponentFieldScalar < ComponentField
  def field_type
    :scalar
  end
end