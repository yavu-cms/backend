class ComponentFieldInteger < ComponentFieldScalar
  DEFAULT = 0

  def field_type
    :integer
  end

  protected

  def sanitize(raw_value)
    raw_value.to_i unless raw_value.blank?
  rescue NoMethodError => e
    logger.error('ComponentFieldInteger') { "Unable to sanitize value into integer. value=\"#{raw_value.inspect}\" message=#{e.message} assumption=#{DEFAULT}" }
    DEFAULT
  end
end