class ComponentFieldModel < ComponentField
  has_one :component_value,
          foreign_key: 'component_field_id',
          dependent: :destroy

  validates :model_class, inclusion: { in: ->(x) { x.class.model_classes.map &:to_s } }, presence: true

  def field_type
    :model
  end

  def value
    component_value.referable if component_value && component_value.try(:referable).try(:visible?)
  end

  # Set the value for this field to +value+.
  # You should note that this method assumes that :model_class is already set.
  def value=(value)
    self.component_value = build_component_value referable_type: model_class, referable_id: value rescue  nil
  end

  # Set the value from the given information.
  # Requires +information+ to have :model_class and :default keys set.
  def value_from(information)
    self.model_class = information[:model_class]
    self.value = information[:default]
  end

  protected

  def copy_onto(new_field)
    new_field.component_value = component_value.dup.tap do |cvc|
      cvc.component_field = new_field
    end if component_value
  end
end
