class ComponentFieldMultiModel < ComponentField
  has_many :component_values,
           foreign_key: 'component_field_id',
           dependent: :destroy,
           autosave: true

  validates :model_class, inclusion: { in: ->(x) { x.class.model_classes.map &:to_s } }, presence: true

  def field_type
    :multi_model
  end

  def value
    component_values.map { |cv| cv.referable if cv.referable.try(:visible?) }.reject &:nil?
  end

  def value=(value)
    value = value.split(',') if value.is_a?(String)
    self.component_values = value.map do |each_value|
      component_values.build referable_type: model_class, referable_id: each_value, component_field: self
    end
  end

  # Set the value from the given information.
  # Requires +information+ to have :model_class and :default keys,
  # being the latter an array of IDs.
  def value_from(information)
    self.model_class = information[:model_class]
    self.value = information[:default] || []
  end

  protected

  def copy_onto(new_field)
    new_field.component_values = component_values.map do |cv|
      cv.dup.tap do |cvc|
        cvc.component_field = new_field
      end
    end
  end
end