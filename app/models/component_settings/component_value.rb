class ComponentValue < ActiveRecord::Base
  belongs_to :component_field, foreign_key: 'component_field_id'
  belongs_to :referable, polymorphic: true
  has_one :component, through: :component_field

  validates :component_field, presence: true
  validates :referable, presence: true

  scope :ordered, -> { order order: :asc }
end