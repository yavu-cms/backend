class MediumMediaGallery < ActiveRecord::Base
  belongs_to :media_gallery
  belongs_to :medium

  default_scope { order :order }
end
