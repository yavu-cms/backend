class ServiceContextProcessor < ContextProcessor

  def build(request_parameters = {}, headers = {})
    # Global context is composed by a root context with children.
    # Serices context is only a child context, so we return this particular
    # context
    evaluated_context = super
    evaluated_context.components[root.component_configuration_id].tap do |ec|
      ec.etag = evaluated_context.etag
      ec.last_modified = evaluated_context.last_modified
      ec.shared_refs = evaluated_context.shared_refs
    end
  end

  protected

   def process_context_by_children(evaluated_context)
    # Finally add component_configurations parameters
    # Only a component configuration knows how extract its own parameters
    ecc = create_evaluated_component_service_context(evaluated_context)
    evaluated_context.add_processed_component(root.component_configuration.id,ecc)
   end

  def create_evaluated_component_service_context(evaluated_context)
    create_evaluated_context.tap do |ec|
      process_component_service_context(ec,evaluated_context)
    end
  end

  def process_component_service_context(component_context, parent_context)
    klass = process_component_klass component_context, parent_context, root.component_configuration
    klass.class_exec do
      def evaluate(context, service)
        # If Component#context raises an exception it will print a pretty message with line error in the context of Component#context code
        instance_eval(service.body.to_s, "ComponentService#context[#{service.name}]", 1)
        __calculated_values(context)
      end
    end
    object = klass.new(parent_context, root.component_configuration,user)
    object.evaluate(component_context, root)
  rescue Exception => e
    component_context.errors << e.message
    logger.error('ServiceContextProcessor') { "Error populating service. method=#{__method__} name=#{root.name} id=#{root.id} errors=#{component_context.errors.join(";")} trace=#{e.backtrace.join(";")}" }
  end

end
