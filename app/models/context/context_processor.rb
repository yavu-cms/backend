class ContextProcessor
  attr_reader :root, :user

  @@logger = nil

  def self.logger
    unless @@logger
      logfile = if YavuSettings.context_processor.use_logfile
        # create log file
        File.open("#{Rails.root}/log/context_processor.log", 'a').tap do |f|
          f.sync = true
        end
      else
        STDOUT
      end
      @@logger = Logger.new logfile
      @@logger.level = Rails.logger.level
    end
    @@logger
  end

  def self.sanitize_instance_variable_name(str)
    str.to_s.gsub(/^[^a-zA-Z_]/, '')
  end

  def logger
    self.class.logger
  end

  # Creates a new ContextProcessor a Route instance will be necessary
  def initialize(root)
    @root = root
  end

  # Generates a Context::Preprocessed from related resources obtained from route:
  #  - route constraints: predefined constrains defined for route
  #  - request parameters: web request received parameters
  #  - component configurations used for
  def build(request_parameters = {}, headers = {})
    params = process_parameters(request_parameters, headers)
    @user = Yavu::API::Resource::User.from_json(headers['USER']) rescue nil
    evaluated_context = create_evaluated_context

    logger.debug('ContextProcessor') { "New context method=#{__method__} root=#{root} parameters=#{request_parameters.keys.join(',')}" }
    root.populate_evaluated_context(evaluated_context, HashWithIndifferentAccess.new(params))
    logger.debug('ContextProcessor') { "Added parameters and constraints context=#{evaluated_context}" }

    process_context_by_children(evaluated_context)
    evaluated_context.stop_processing(self)
  end

  def build_single_component_context(component_configuration)
    evaluated_context = create_evaluated_context
    component_context = create_evaluated_component_context evaluated_context, component_configuration
    process_component_context component_context, evaluated_context, component_configuration
    component_context.stop_processing(self)
  end

  protected

  def process_context_by_children(evaluated_context)
    # Finally add component_configurations parameters
    # Only a component configuration knows how to extract its own parameters
    component_configurations.includes(:fields, component: [:fields, :services]).references(:fields).each do |component_configuration|
      ecc = create_evaluated_component_context(evaluated_context, component_configuration)
      evaluated_context.add_processed_component(component_configuration.id, ecc)
    end
  end

  def component_configurations
    root.component_configurations
  end

  def create_evaluated_context
    Yavu::API::Resource::PreprocessedContext.new
  end

  def create_evaluated_component_context(evaluated_context, component_configuration)
    create_evaluated_context.tap do |ec|
      process_component_context(ec, evaluated_context, component_configuration)
      if component_configuration.cover?
        component_configuration.cover_articles_component_configurations.includes(:fields, :component, articles_cover_articles_component_configurations: { article: [:section, :edition] }).each do |ca|
          new_ecc = create_evaluated_component_context(ec, ca)
          ec.add_processed_component(ca.id, new_ecc)
        end
      end
      logger.debug('ContextProcessor') { "Created evaluated context for component=#{component_configuration.component.name} id=#{component_configuration.id} context=#{ec}" }
    end
  end

  def process_component_klass(component_context, parent_context, component_configuration)
    Class.new do
      class_eval(component_configuration.component.extras.to_s, "Component#extras[#{component_configuration.component.name}]", 1)

      def initialize(evaluated_context, component_configuration, user)
        # Presets current evaluated context values
        evaluated_context._values.each do |k,v|
          instance_variable_set "@#{ContextProcessor.sanitize_instance_variable_name k}", v
        end

        # Be careful: @settings must be set AFTER preset current evaluated context values!!!
        @settings = component_configuration.available_attributes.with_indifferent_access
        @services = component_configuration.configured_services(true)
        @current_user = user
      end

      def halt(code)
        throw :halt_processing_context, code
      end

      def __calculated_values(context)
        instance_variables.each do |name|
          context[name.to_s.gsub('@', '')] = instance_variable_get(name)
        end
      end
    end
  end

  # Process a component configured context. In order to do that:
  #  * Create a new unnamed class initialized with
  #     - component_configuration available_attributes as instance_variables
  #     - This new object respond_to #evaluate that it is implemented by component#context field
  #     - When an error occurs, it will be trapped, mark component_context as erroneous and log error
  # * Call new_object#evaluate(component_context, component_configuration)
  # * Expect to set component_context values properly or mark it as erroneous
  def process_component_context(component_context, parent_context, component_configuration)
    klass = process_component_klass component_context, parent_context, component_configuration
    klass.class_exec do
      def evaluate(context, component_configuration)
        # If Component#context raises an exception it will print a pretty message with line error in the context of Component#context code
        instance_eval(component_configuration.component.context.to_s, "Component#context[#{component_configuration.component.name}]", 1)
        __calculated_values(context)
      end
    end
    object = klass.new(parent_context, component_configuration,user)
    object.evaluate(component_context, component_configuration)
  rescue Exception => e
    component_context.errors << e.message
    logger.error('ContextProcessor') { "Error populating component. method=#{__method__}, component_name=#{component_configuration.component.name} id=#{component_configuration.id} errors=#{component_context.errors.join(";")} trace=#{e.backtrace.join(";")}" }
  end

  def process_parameters(request_parameters = {}, headers = {})
    request_parameters.select! {|k,_| root.available_parameters.include? k.to_s }
    params = Hash[request_parameters.each {|k,v| [ ContextProcessor.sanitize_instance_variable_name(k), v ]}]
    params.merge(headers: headers).with_indifferent_access
  end
end
