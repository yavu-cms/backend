class ExtraAttribute < ActiveRecord::Base
  extend Enumerize
  enumerize :attribute_type, in: [:boolean, :string, :color, :date, :time], predicates: true
  serialize :model_classes, Hash
  validates :name, presence: true, uniqueness: true, format: { with: /\A\w[\w_]*\z/ }
  validates :attribute_type, presence: true
  validates_length_of %i[name description attribute_type], maximum: 255

  default_scope  { order(:name) }

  def self.classes
    [Article, Section, Edition, Supplement, ClientApplication]
  end

  def self.model_class_name(klass)
    klass.name.underscore
  end

  def model_classes
    Hash[self.class.initial_model_classes.map do |model, value|
      [model, super.has_key?(model) ? super[model] : value]
    end]
  end

  def serialized_model_classes
    model_classes.select { |_, value| value == '1' }.keys.map do |name|
      I18n.t("activerecord.models.#{name}")
    end.join(', ')
  end

  def is_for?(klass)
    model_classes[self.class.model_class_name(klass)] == '1'
  end

  def self.for(klass)
    all.select { |ea| ea.is_for? klass }
  end

  private

  def self.available_model_classes
    classes.map { |mc| model_class_name mc }
  end

  def self.initial_model_classes
    available_model_classes.inject({}) do |acc, mc_name|
      acc.merge({ mc_name => '0' })
    end
  end
end
