# Represents the main menu of the application.
# This model allows engines to add new items int the "extras" dropdown
# through initializers (or even at Runtime):
#
#     # my_engine/lib/my_engine/engine.rb
#     module MyEngine
#       class Engine < ::Rails::Engine
#         isolate_namespace MyEngine
#
#         initializer :menu_extras do |app|
#           Menu.extras << MyMenu.new
#         end
#       end
#     end
#
# where +MyMenu+ is a subclass of +Menu+ like:
#
#     # my_engine/app/models/my_engine/my_menu.rb
#     class MyEngine::MyMenu < Menu
#       def value
#         'My menu item'
#       end
#
#       def path
#         MyEngine::Engine.routes.url_helpers.golden_path
#       end
#
#       def options
#         { class: 'golden' }
#       end
#     end
class Menu
  cattr_accessor :extras do
    []
  end

  def self.extras?
    extras.any?
  end

  # Should return the value of the menu item link (required)
  def value; end

  # Should return the path for the menu item link (required)
  def path; end

  # Should return any options needed for the menu item link (optional)
  def options; end
end