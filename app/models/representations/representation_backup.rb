class RepresentationBackup < BaseRepresentation
  belongs_to :representation, foreign_key: 'representation_id', class_name: 'Representation'
  # Validations
  validates :representation, presence: true

  default_scope { order created_at: :desc }

  # Restore this backup, overwriting the associated representation.
  # @see Representation#restore_backup!
  def restore!
    representation.restore_backup! self
  end

  def kind_for_assets
    'backups'
  end
end
