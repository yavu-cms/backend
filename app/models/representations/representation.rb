class Representation < BaseRepresentation
  has_many :routes, foreign_key: 'representation_id'
  # Draft and Backups
  has_one  :draft,   class_name: 'RepresentationDraft', foreign_key: 'representation_id', dependent: :destroy
  has_many :backups, class_name: 'RepresentationBackup', foreign_key: 'representation_id', dependent: :destroy

  after_save :trigger_draft_creation
  after_touch :trigger_draft_creation
  before_destroy :unused?

  include Representations::DraftManagement,
          Representations::BackupManagement

  # Return a persisted copy of this representation with its +type+ (and thus its class)
  # changed to +target_class+.
  # @see BaseRepresentation#copy
  def copy_and_save_as(target_class, name)
    alter_ego = copy name
    alter_ego.being_copied = target_class.to_s != 'Representation'
    alter_ego.type = target_class
    alter_ego.representation_id = self.id
    alter_ego.save!
    # Reload from database so that the object is properly instantiated with the +target_class+
    BaseRepresentation.find(alter_ego.id)
  end

  # Update every associated route
  # Touching routes is enough because client_application is touched by superclass
  def touch_routes
    routes.update_all(updated_at: Time.now)
    client_application.touch
  end

  def trim_backups!(keep = 5)
    backups.where.not(id: backups.limit(keep).pluck(:id)).destroy_all
  end

  def unused?
    routes.empty?
  end

  def kind_for_assets
    'representations'
  end

  protected

  # Replace this object with that +other_thing+ (expected to be a BaseRepresentation)
  # updating any reference to this object so that it references +other_thing+, and
  # changing its type to +Representation+ and its name to this objects name.
  # Optionally, this method either turns this object into an instance of +become+
  # (if such parameter is other than nil) or it destroys this object (if +become+
  # is nil and +destroy+ is true).
  # @see BaseRepresentation#change_references_to
  def replace_with(other_thing, become: nil, destroy: true)
    transaction do
      rep_name = name
      self.update! name: "#{name} #{created_at}"
      other_thing.update! type: 'Representation', name: rep_name
      # Reload the other_thing from db so that it becomes a Representation
      another_representation = Representation.find(other_thing.id)
      change_references_to another_representation
      another_representation.touch_routes
      another_representation.update_assets!
      # Decide whether we are becoming other class, destroying this instance or doing nothing at all
      if become
        self.update! type: become.to_s
        if become.to_s == 'RepresentationBackup'
          backup_representation = RepresentationBackup.find(id) 
          another_representation.backups << backup_representation
          another_representation.trim_backups!
          backup_representation.update_assets!
        end
      elsif destroy
        destroy!
      end
      another_representation.touch
    end
  end

  # Update incoming references to this object so that they point at +another_representation+
  def change_references_to(another_representation)
    routes.update_all representation_id: another_representation
    backups.update_all representation_id: another_representation
  end
end
