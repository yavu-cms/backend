class RepresentationDraft < BaseRepresentation
  belongs_to :representation, foreign_key: 'representation_id'
  # Validations
  validates :representation, presence: true

  # Apply this draft onto its representation
  # @see Representation#apply_draft!
  def apply!
    representation.apply_draft!
  end

  def kind_for_assets
    'drafts'
  end
end
