class BaseRepresentation < ActiveRecord::Base
  include Versionable
  serialize :configuration, Hash

  belongs_to :client_application
  # Structure
  has_many :rows, foreign_key: 'representation_id', dependent: :destroy
  has_many :columns, through: :rows
  has_many :component_configurations, through: :columns
  has_many :components, through: :component_configurations
  has_many :cover_component_configurations, class_name: 'CoverComponentConfiguration', through: :columns, source: :component_configurations
  has_many :cover_components, through: :cover_component_configurations, source: :component
  has_many :services, through: :components

  # Visual aspects
  belongs_to :view, dependent: :destroy, autosave: true
  belongs_to :default_row_view, class_name: 'View', foreign_key: 'row_view_id', dependent: :destroy, autosave: true
  belongs_to :default_column_view, class_name: 'View', foreign_key: 'column_view_id', dependent: :destroy, autosave: true
  belongs_to :error_view, class_name: 'View', foreign_key: 'error_view_id', dependent: :destroy, autosave: true
  belongs_to :favicon, class_name: 'ClientApplicationAsset'

  has_many :client_application_assets_base_representations
  has_many :assets, class_name: 'ClientApplicationAsset', through: :client_application_assets_base_representations, source: :client_application_asset
  has_and_belongs_to_many :views

  before_validation :set_default_views
  before_save :update_assets_when_name_changes
  after_destroy :remove_asset_files

  validates :client_application, presence: true
  validates :name, presence: true, uniqueness: { scope: :client_application_id }
  validates :view, presence: true
  validate  :view_has_content_placeholder
  validate  :views_dont_repeat_formats
  validates_length_of %i[name type], maximum: 255

  scope :all_covers, -> { joins(:cover_component_configurations).uniq }

  alias_method :own_favicon, :favicon
  # Add an "I'm being copied" flag so that we don't try to create drafts for Representations that won't stay as such
  attr_accessor :being_copied
  attr_reader :locking

  include Representations::Rendering,
          Representations::FromJson,
          Representations::CoverFromJson

  def to_s
    name
  end

  # Return a copy of this representation, updating its name and including any
  # related rows.
  # See test case to understand what I'm exactly doing
  def copy(name = nil)
    copy = dup except: :representation_id, include: [ :view, :default_row_view, :default_column_view, :error_view, :views ]
    copy.assign_attributes being_copied: true, name: (name || I18n.t('activerecord.attributes.representation.copy.prefix', name: self.name))
    copy.rows = copy_rows(copy)
    copy.client_application_assets_base_representations = copy_client_application_assets_base_representations(copy)
    copy
  end

  def locking_enabled?
    locking && super
  end

  def enable_locking!
    @locking = true
  end

  # Return true if this representation is a cover - i.e., if it has at least one
  # +cover_component+.
  def cover?
    cover_components.any?
  end

  def slug(was=false)
    (was ? name_was : name).parameterize('_')
  end

  # Will dump main stylesheet and javascript for current representation
  # using sprockets pre-processor directives to include used component assets
  # and associated client_application assets
  # This method shall be called when component_configurations or client_application_assets changes
  def update_assets!(*args)
    File.open(main_javascript_file, 'w') do |file|
      file.write <<-HEAD
//
//  Representation [#{slug}] wide scripts
//  File automatically generated at #{Time.now}
//
      HEAD
      file.write build_javascripts_assets
      file.write "\n"
    end
    File.open(main_stylesheet_file, 'w') do |file|
      file.write <<-HEAD
/*
 *  Representation [#{slug}] wide styles
 *  File automatically generated at #{Time.now}
      HEAD
      file.write build_stylesheets_assets
      file.write "\n */\n"
    end
  end

  # Return all cover articles by cover placement order
  # Can filter by cover component configuration description
  # Admits a block to perform a middle-processing in each articles's relationship
  # with representation's cover articles components
  def cover_articles(*cover_components_config_names)
    Article.find_ordered(cover_articles_component_configurations(*cover_components_config_names).map do |cacc|
      (block_given? ? yield(cacc.articles) : cacc.articles).pluck(:id)
    end.flatten.uniq)
  end

  def cover_articles_component_configurations(*cover_components_config_names)
    selected_cover_components_configurations = if cover_components_config_names.any?
                                                 cover_component_configurations.find_ordered_where(description: cover_components_config_names)
                                               else
                                                 cover_component_configurations
                                               end
    selected_cover_components_configurations.map {|x| x.cover_articles_component_configurations }.flatten
  end

  def cover_articles_components
    Set.new(cover_articles_component_configurations.map { |x| x.component}).to_a
  end

  # Return an array joining components & cover_articles_components
  def all_components
    components + cover_articles_components
  end

  # Returns this representation's asset, if strict is set to true
  # it will not return client application's favicon in case this representation
  # doesn't have any.
  def favicon(strict = false)
    own_favicon || (client_application.favicon unless strict)
  end

  def can_have_logo?
    false
  end

  def can_have_view?(view)
    !views.pluck(:format).include? view.format
  end

  def has_view?(view)
    views.include? view
  end

  # Update every associated route
  # Touching routes is enough because client_application is touched by superclass
  # This method must be implemented in the subclasses
  def touch_routes
  end

  def component_assets
    all_components.map(&:assets).flatten.uniq
  end

  # Specify the correct kind for assets main files generation.
  # Should be overridden in subclasses.
  # @see ClientApplicationAssetUploader#representation_store_filename
  def kind_for_assets; end

  # Enumerates all kind of representations for use in assets
  # TODO: Make it more flexible making use of #kind_for_assets method
  def self.kinds_for_assets
    %w( representations drafts backups )
  end

  protected
    # Create and set default views for layout, columns, rows and error
    def set_default_views
      self.view                ||= View.create_default_for(:representation)
      self.default_row_view    ||= View.create_default_for(:row)
      self.default_column_view ||= View.create_default_for(:column)
      self.error_view          ||= View.create_default_for(:error)
    end

    def view_has_content_placeholder
      errors.add(:view, I18n.t('errors.messages.has_not_content')) unless view.has_content_placeholder?
      false
    end

    def views_dont_repeat_formats
      formats = views.map(&:format)
      if formats.count > formats.uniq.count
        errors.add(:views, I18n.t('errors.messages.format_repeated'))
        false
      end
    end

    def build_javascripts_assets
      build_assets(:javascript?, assets + component_assets,'//= require')
    end

    def build_stylesheets_assets
      build_assets(:stylesheet?, assets + component_assets,' *= require')
    end

    # build for each received asset a list of files without extension prefixing each line with
    # received prefix
    def build_assets(type, assets, prefix)
      assets.select(&type).map do |asset|
        "#{prefix} #{asset.relative_url_without_extension}"
      end.uniq.join "\n"
    end

    def copy_rows(representation)
      rows.map(&:copy).each {|row| row.representation = representation }
    end

    def copy_client_application_assets_base_representations(representation)
      client_application_assets_base_representations.map(&:copy)
    end

    def main_javascript_file(was=false)
      ClientApplicationAssetUploader.javascript_representation_store_filename(self, was)
    end

    def main_stylesheet_file(was=false)
      ClientApplicationAssetUploader.stylesheet_representation_store_filename(self, was)
    end

    def remove_asset_files
      old_js  = main_javascript_file
      old_css = main_stylesheet_file
      File.unlink old_js  if File.exists? old_js
      File.unlink old_css if File.exists? old_css
    end

    def update_assets_when_name_changes
      if type_changed? && type == 'RepresentationBackup'
        old_css = main_stylesheet_file
        old_js = main_javascript_file
        File.unlink old_js  if File.exists? old_js
        File.unlink old_css if File.exists? old_css
      end
      if name_changed? && name_was && !type_changed?
        # If name changed from a valid name to a new name...
        old_js  = main_javascript_file(true)
        old_css = main_stylesheet_file(true)
        File.unlink old_js  if File.exists? old_js
        File.unlink old_css if File.exists? old_css
        update_assets!
      end
      true
    end
end
