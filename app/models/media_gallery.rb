class MediaGallery < Gallery
  has_many :medium_media_galleries, dependent: :destroy
  has_many :media, through: :medium_media_galleries
end
