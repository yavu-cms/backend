class FrontendServer
  def self.notify(client_application)
    FrontendNotificatorWorker.perform_async(client_application.identifier)
  end

  def self.all
    $redis.hgetall(YavuSettings.redis.client_frontend_heartbeat_namespace).map {|id, data| self.new id, JSON.load(data) }
  end

  def self.clean(before)
    all.select {|x| x.updated_at.nil? || x.updated_at <= before }.each { |x| x.delete }
  end

  def self.find(id)
    data = $redis.hget YavuSettings.redis.client_frontend_heartbeat_namespace, id
    self.new id, JSON.load(data) if data
  end

  def self.save(frontend)
    data = { ts: Time.now.to_i, identifier: frontend.client_application_identifier, status: frontend.status }.to_json
    $redis.hset YavuSettings.redis.client_frontend_heartbeat_namespace, frontend.name, data
    true
  end

  def self.mark_alive(frontend_identifier, client_identifier)
    frontend = find(frontend_identifier) || new(frontend_identifier, {})
    frontend.client_application_identifier = client_identifier.presence
    if client_identifier
      frontend.client_application_identifier = client_identifier
      frontend.mark_up
    else
      frontend.mark_without_client_id
    end
    frontend.save
  end

  attr_accessor :id, :name, :client_application_identifier, :updated_at, :status

  def initialize(id, data)
    self.id = id.parameterize
    self.name = id
    self.client_application_identifier = data['identifier']
    self.updated_at = Time.at(data['ts']).to_datetime if data['ts']
    self.status = data['status']
  end

  def to_s
    name
  end


  def reload
    self.class.find name
  end

  def save
    self.class.save self
  end

  def restart!
    mark_sending_restart
    self.class.save self

    total_info = {
      ts: Time.now.to_i,
      identifier: client_application_identifier,
      hostname: name,
      status: status
    }.to_json
    $redis.publish YavuSettings.redis.client_frontend_channel, total_info
  end

  def configure(client_application)
    data = {
      id: name,
      ts: Time.now.to_i,
      configure: client_application.identifier
    }.to_json

    $redis.publish YavuSettings.redis.client_frontend_channel, data
  end

  def delete
    $redis.hdel YavuSettings.redis.client_frontend_heartbeat_namespace, name
  end

  def client_application
    unless @client_application
      @client_application = ClientApplication.find_by_identifier client_application_identifier
    end
    @client_application
  end

  def healthy?
    status['code'] == 'ok' rescue false
  end

  def status_message
    status['message'] || '' rescue ''
  end

  # Frontend states

  def mark_up
    self.status = { 'code' => 'ok' }
  end

  def mark_unknown(msg)
    self.status = { 'code' => 'unknown', message: msg }
  end

  def mark_down
    self.status = { code: 'down_or_restarting', message: 'Frontend server down (Perhaps restarting?)' }
  end

  def mark_without_client_id
    self.status = { 'code' => 'unassigned_client_application', 'message' => 'No client application assigned' }
  end

  def mark_sending_restart
    self.status = { code: 'sending-frontend-restart', message: 'Sending frontend restart signal...' }
  end
end
