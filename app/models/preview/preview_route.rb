module PreviewRoute
  attr_accessor :preview_representation

  def self.included(base)
    base.set_api_resource_class regular_route_class(base)
  end

  def representation_id
    representation.id
  end

  def representation
    preview_representation
  end

  def component_configurations
    representation.component_configurations
  end

  private

  def self.regular_route_class(preview_class)
    preview_class.name.gsub(/\APreview/, '').constantize
  end
end