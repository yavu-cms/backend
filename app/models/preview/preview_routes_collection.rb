class PreviewRoutesCollection
  include Enumerable

  attr_accessor :routes

  def initialize(routes, representation = nil)
    self.routes = Hash[routes.map { |route| [route.id.to_s, route.for_preview(representation)] }]
  end

  def find(id)
    routes[id.to_s]
  end

  def each(&block)
    routes.values.each &block
  end
end