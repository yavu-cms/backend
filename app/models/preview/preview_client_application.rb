class PreviewClientApplication < ClientApplication
  attr_accessor :preview_representation

  alias_method :actual_routes, :routes

  def debug
    true
  end

  def assets_host
    nil
  end

  def routes
    PreviewRoutesCollection.new super, preview_representation
  end

  def service_prefix
    "/api/service/preview/#{identifier}/#{preview_representation.try(:id) || 0}"
  end

  def assets_prefix
    YavuSettings.assets.prefix_preview
  end

  def assets_configuration
    self.class.assets_configuration(self, for_preview: true)
  end

  def preview_renderer(route, server: nil, locale: 'en', logger: nil)
    client_resource = Yavu::API::Resource::Base.from_json as_api_resource.to_json

    Yavu::Frontend::Renderer::Preview.configure do |config|
      config.base_url = client_resource.base_url
      config.service_prefix = "#{client_resource.service_prefix}/#{route.id}"
      config.routes = client_resource.routes
      config.logo = client_resource.logo
      config.assets_configuration = client_resource.assets_configuration
      config.runtime_options = client_resource.runtime_options
      config.media_url = client_resource.external_media_url.try(:chomp, '/')
    end
    route = route.as_api_resource if route.is_a?(ActiveRecord::Base)
    Yavu::Frontend::Renderer::Preview.new route, server: server, locale: locale, logger: logger
  end

  def preview_server(request, authenticated_user = nil)
    Yavu::Frontend::Server::Preview.new request, authenticated_user
  end
end
