class ClientApplicationAsset < ActiveRecord::Base
  include Asset
  self.inheritance_column = nil # no STI

  belongs_to :client_application

  has_many :client_application_assets_base_representations
  has_many :base_representations, through: :client_application_assets_base_representations, source: :base_representation
  alias_method :representations, :client_application_assets_base_representations

  mount_uploader :file, ClientApplicationAssetUploader

  validates_presence_of [ :client_application, :type, :file ]
  validates_length_of %i[type prefix_path content_type], maximum: 255

  after_save :update_representation_assets
  after_destroy :update_representation_assets

  before_destroy :check_before_destroy

  # We must check for uniqueness of file with scope [:client_application_id, :type, :prefix_path]
  # manually because carrierwave sets file after saving record, and before commiting changes
  # There is a database unique index, so check if database restriction is applied
  validate :file_uniqueness

  default_scope { order(:file) }

  set_api_resource id: :id,
      type: ->(x) { x.type.to_s},
      file: ->(x) { x.relative_url }

  def self.toggables
    %w(favicon logo)
  end

  # Methods to manage toggables on client application
  # - Check if it can be toggled.
  # - Check if it is actually set as that
  # - Toggle it
  toggables.each do |toggable|
    define_method("can_be_#{toggable}?") { type == 'image' }
    define_method("#{toggable}_for?") do |container, strict: false|
      container.send("#{toggable}", strict: strict) == self
    end
    define_method("toggle_as_#{toggable}") do |container|
      return false unless send("can_be_#{toggable}?")
      container.send("#{toggable}=", send("#{toggable}_for?", container) ? nil : self)
      container.save!
    end
  end

  def prefix_path=(value)
    old_path = file.current_path
    self[:prefix_path] = value
    if persisted? && prefix_path_changed?
      self.file = File.open(old_path)
    end
  end

  # Complete path to this asset's dir.
  # e.g. data/clients/client_app_identifier/assets/asset_type/
  # Used by rename method on asset's concern.
  def absolute_url
    "#{ClientApplicationAssetUploader.absolute_prefix_for_type(client_application, type)}"
  end

  def copy(client_app = nil)
    client_app.assets.build file: File.open(file.current_path, 'r'), client_application: client_app
  end

  private

    def file_uniqueness
      if ClientApplicationAsset.where('client_application_id = ? and type = ? and file = ? and prefix_path= ?',
                                      client_application_id, type, file.filename, prefix_path).any?
        errors.add(:file,I18n.t('errors.messages.taken'))
      end
    end

    # ClientApplicationAsset can be destroyed if there are no
    # representations associated
    def check_before_destroy
      base_representations.empty?
    end

    def assets_configuration
      client_application.assets_configuration if client_application
    end

    def update_representation_assets
      client_application.representations.each {|r| r.update_assets! }
    end

end
