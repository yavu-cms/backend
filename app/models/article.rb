class Article < ActiveRecord::Base
  include Searchable, Taggable, FindableInOrder, ExtraAttributeModelable, Versionable

  MAX_SLUG_LENGTH = 240

  belongs_to :section
  belongs_to :edition

  has_and_belongs_to_many :articles, join_table: 'article_relations', foreign_key: 'article_a_id', association_foreign_key: 'article_b_id'
  has_many :article_media, dependent: :destroy, inverse_of: :article
  has_many :articles_cover_articles_component_configurations, dependent: :restrict_with_error
  has_many :cover_articles_component_configurations, through: :articles_cover_articles_component_configurations
  has_many :component_values, as: :referable, dependent: :restrict_with_error

  # Overwrite default versioning options set by Versionable Concern
  has_paper_trail ignore: [:slug]

  # Callbacks
  before_validation :set_defaults, on: :create
  before_create :autotag, if: ->{ Setting['articles.autotag'].present? }
  before_create :set_slug
  after_create  :append_id_to_slug
  after_save :touch_related, :update_accounting, :update_main_medium
  after_touch :touch_related, :update_accounting
  before_save :update_time, :look_for_new_resources_inside_body

  after_save    :set_initial_votes, :set_initial_visits

  scope :ordered, (-> do
    article_time = Setting['use_article_time'] ? 'cast(articles.time AS TIME)' : 'cast(articles.updated_at AS TIME)'
    order('articles.updated_at DESC')
    if Setting['use_edition_date']
      eager_load(:edition).order("editions.date DESC, #{article_time} DESC")
    else
      order("cast(articles.time AS DATE) DESC, #{article_time} DESC")
    end
  end)

  scope :all_them,          -> { eager_load(:edition, :section).ordered }
  scope :visible,           -> { all_them.merge(unscoped_visible) }
  scope :on_active_edition, -> { joins(:edition).where("editions.is_active = true") }
  scope :unscoped_visible,  -> { unscoped.merge(Section.unscoped.visible).merge(Edition.unscoped.visible).where(is_visible: true) }
  scope :for_cover,         -> { visible.where(hide_from_cover: false) }
  scope :with_main_image,   -> { all_them.joins(article_media: :medium).where('article_media.main' => true, 'media.type' => 'ImageMedium') }
  scope :with_edition_date, ->(date_string) { where(editions: { date: Date.strptime(date_string, '%d/%m/%Y') }) }
  scope :with_section,      ->(section_id) { where(section_id ? { section_id: section_id } : nil) }

  # Validations
  validates :title, presence: true
  validates :section, presence: true
  validate :same_supplement

  accepts_nested_attributes_for :article_media, allow_destroy: true
  accepts_nested_attributes_for :extra_attribute_model

  # Attributes to be exported for API
  set_api_resource id: :slug,
    slug: :slug,
    article_id: :id,
    title: :title,
    lead: :lead,
    body: :frontend_body,
    date: :date,
    full_date: :full_date,
    time: :time,
    section_id: ->(x) { section.slug },
    section_name: ->(x) { section.name },
    section_color: ->(x) { section.color },
    heading: :heading,
    signature: :signature,
    main_image: :main_image,
    main_embedded: :main_embedded,
    main_medium: :main_medium,
    edition: ->(x) { edition.try(:name_or_date) },
    image_media: ->(x) { article_media.image.limit(api_resource_settings[:media_limit]).order(:order) if api_resource_settings[:media_limit] },
    embedded_media: ->(x) { article_media.embedded.limit(api_resource_settings[:media_limit]).order(:order) if api_resource_settings[:media_limit] },
    file_media: ->(x) { article_media.file.limit(api_resource_settings[:media_limit]).order(:order) if api_resource_settings[:media_limit] },
    audio_media: ->(x) { article_media.audio.limit(api_resource_settings[:media_limit]).order(:order) if api_resource_settings[:media_limit] },
    images_count: ->(_) { article_media.image.count },
    embeddeds_count: ->(_) { article_media.embedded.count },
    files_count: ->(_) { article_media.file.count },
    audios_count: ->(_) { article_media.audio.count },
    tags: ->(x) { related_tags(api_resource_settings[:tags_limit]).root.visible if api_resource_settings[:tags_limit] },
    # related.visible cause Active::Record error
    related: ->(x) { related.limit(api_resource_settings[:related_limit]).select(&:visible?) if api_resource_settings[:related_limit] },
    comes_from_migration: :migrated?,
    open_in_new_tab: ->(_){ extra_attribute_model.open_in_new_tab rescue nil },
    redirect_to_external_link: ->(_) do
      begin
        extra_attribute_model.redirect_to_external_link if extra_attribute_model.redirect_to_external_link.present?
      rescue
        nil
      end
    end

  # Elasticsearch mappings
  mappings do
    indexes :heading,  type: :string, analyzer: :html, similarity: 'BM25'
    indexes :title, type: :string, analyzer: :text, similarity: 'BM25'
    indexes :lead,  type: :string, analyzer: :html, similarity: 'BM25'
    indexes :body, type: :string, analyzer: :text, similarity: 'BM25'
    indexes :time, type: :date
    indexes :tags do
      indexes :name, type: :string, analyzer: :text
    end
    indexes :edition do
      indexes :name_or_date, type: :string, analyzer: :text
      indexes :date, type: :date
    end
    indexes :section do
      indexes :slug, type: :string, index: :not_analyzed
      indexes :name, type: :string, index: :not_analyzed
      indexes :color, type: :string, index: :not_analyzed
    end
    indexes :news_source, type: :string, index: :not_analyzed
    indexes :fronterized_main_image, type: :string, index: :not_analyzed
  end

  # Return all articles *except* the one passed in as argument
  def self.but(article)
    article ? where.not(id: article) : all_them
  end

  # Search for articles using a query hash, which may contain:
  #   * query  [String] string to be searched amongst the title, heading, lead and body (using OR and %<query>%)
  #   * except [Article | integer] element to exclude from the result set
  def self.search(params = {})
    scope = all
    scope = scope.where('title like :query OR heading like :query OR lead like :query', query: "%#{params[:query]}%") unless params[:query].blank?
    scope = scope.but(params[:except]) unless params[:except].blank?
    scope
  end

  def to_s
    title
  end

  # Mediums: an article can have images, embedded, files, audios, etc
  # but only ONE main medium, that is, one main image OR one main embedded (Not both)

  # Return the main Image for this article, if any
  def main_image
    article_media.joins(:medium).where(main: true, media: { type: 'ImageMedium' }).first
  end

  # Return the main Embedded for this article, if any
  def main_embedded
    article_media.joins(:medium).where(main: true, media: { type: 'EmbeddedMedium' }).first
  end

  # Return the main Medium for this article, if any
  def main_medium
    article_media.joins(:medium).where(main: true).first
  end

  # Get its own news source through section's supplement
  def news_source
    section.try(:supplement).try(:news_source)
  end

  # Override Taggable#serialized_tags to use related_tags
  def serialized_tags
    related_tags.pluck(:name).join(',')
  end

  # Patch for slow-query in tags
  def related_tags(limit = 10)
    article_tags_ids = ArticlesTag.where(article: self).pluck :tag_id
    Tag.where(id: article_tags_ids).limit limit
  end

  # Return other articles related to this one.
  # Note that this method is different from Article#articles as it does a
  # both-ways check on the association of the articles, i.e. any articles
  # related to this one either as the originator or the target of the relation
  # will be included by this method.
  def related
    related_ids = ArticleRelations.where('article_a_id = :id OR article_b_id = :id', id: id).
                                   pluck(:article_a_id, :article_b_id).flatten.uniq
    self.class.but(id).where(id: related_ids)
  end

  # Return a comma-delimited string with the IDs of the related articles
  # associated to this article
  def serialized_related
    articles.collect(&:id).join(',')
  end

  # Update the set of articles related to this one to match the ones
  # specified in +related+.
  def serialized_related=(related)
    related_ids = (related.is_a?(Array) ? related.first : related).split ','
    self.articles = Article.but(self).where(id: related_ids)
  end

  def copy(attributes = {})
    # Copy attributes
    dup(except: [:slug, :time], include: [:tags, :articles, :article_media]).tap do |copy|
      copy.time = Time.current
      copy.update_attributes(attributes) unless attributes.empty?
    end
  end

  # Extract the edition date or the created_at article field
  # If is a new record, returns the current date
  def date
    if Setting['use_edition_date']
      edition.try(:date) || created_at.try(:to_date) || Date.today
    else
      time.to_date || Date.today
    end
  end

  # Ensures time is recorded in UTC format
  def time=(new_time)
    new_time = Time.parse(new_time) if new_time.is_a?(String)
    super new_time.utc
  end

  # Ensures time is returned in localtime format
  def time
    super.try(:localtime)
  end

  # Return an object representing the date & time information that should be
  # published for this article
  def full_date
    DateTime.new date.year, date.month, date.day, time.hour, time.min, time.sec
  end

  def migrated?
    # By default, all articles are created in the CMS,
    # therefore, migrated? will response 'false'
    # Extend this if articles can be imported from an external data source.
    false
  end

  # Answer whether this article is visible
  def visible?
    is_visible? && section.try(:is_visible?) && edition.try(:is_visible?)
  end

  def calculated_extra_attribute_model
    extra_attribute_model.merge(section.try(:extra_attribute_model)).merge(edition.try(:extra_attribute_model))
  end

  def calculated_extra_attribute(attribute)
    calculated_extra_attribute_model.send attribute rescue nil
  end


  # Assigns a liking vote or a disliking vote in the article from a user
  # Returns false when something was wrong (user has already voted, mispelled feeling action)
  def vote(feeling, user)
    Accounting::MostVotedArticles.make(self, user, feeling)
  end

  # Retrieve votes for an article. if *only_real_votes* is set
  # it returns the real users votes without fake base votes
  def votes(only_real_votes: false)
    Accounting::MostVotedArticles.votes self, only_real_votes: only_real_votes
  end

  # Increment the number of visits for this article
  def visit!(count = 1)
    Accounting::MostVisitedArticles.visit! self, count
  rescue => error
    logger.fatal('Article') { "Article visits not modified. article_id=#{id} method=#visit! error=#{error}" }; false
  end

  # Return the number of visits this article has received. If *only_real_visits* is set
  # it returns the real visits without fake base visits
  def visits(only_real_visits: false)
    Accounting::MostVisitedArticles.visits self, only_real_visits: only_real_visits
  rescue => error
    logger.fatal('Article') { "Account visits could not be fetched.  article_id=${id} method=#visits error=#{error}" }
    0
  end

  # Return the number of comments of this article in a timerange
  def comments_quantity(timerange: nil)
    Accounting::MostCommentedArticles.comments_quantity_from self, timerange: timerange
  end

  # Lazy getters/setters to optimize excedent queries to MongoDB
  def initial_visits
    @initial_visits ||= Accounting::MostVisitedArticles.initial_visits_for(self)
  end

  def initial_likes
    @initial_likes ||= Accounting::MostVotedArticles.initial_votes_for(self).first
  end

  def initial_dislikes
    @initial_dislikes ||= Accounting::MostVotedArticles.initial_votes_for(self).last
  end

  attr_accessor :initial_visits_changed
  def initial_visits=(new_visits)
    new_visits = new_visits.to_i
    unless initial_visits == new_visits
      @initial_visits = new_visits
      self.initial_visits_changed = true
    end
  end

  attr_accessor :initial_likes_changed
  def initial_likes=(new_likes)
    new_likes = new_likes.to_i
    unless initial_likes == new_likes
      @initial_likes = new_likes
      self.initial_likes_changed = true
    end
  end

  attr_accessor :initial_dislikes_changed
  def initial_dislikes=(new_dislikes)
    new_dislikes = new_dislikes.to_i
    unless initial_dislikes == new_dislikes
      @initial_dislikes = new_dislikes
      self.initial_dislikes_changed = true
    end
  end

  # Elasticsearch support methods
  #

  # Abstraction method that provides a simple interface for performing a
  # full search based on the values of +query_string+, +date+, +section_name+, +news_source_name+ & +sort_by+.
  def self.perform_search(query_string = nil, date = nil, section_name = nil, news_source_name = nil, sort_by = 'rank')
    filters= {}
    filters[:date] = date if date.present?
    filters[:section] = section_name if section_name.present?
    filters[:news_source] = news_source_name if news_source_name.present?
    query = ElasticSearchQuery.new(query_string, sort_by, nil, filters).build
    es_search(query)
  end

  def indexable?
    visible?
  end

  def as_indexed_json(options = {})
    data = as_json only: [:slug, :title, :heading, :lead, :time],
            include: {
              tags: { only: [:name] },
              edition: { methods: [:name_or_date], only: [:name, :date] },
              section: { only: [:slug, :name] }
            }
    data.tap do |d|
      d['body'] = stripped_body
      d['news_source'] = news_source.try :name
      d['fronterized_main_image'] = main_image.try(:as_api_resource).try(:to_json)
      d['section']['color'] = section.color
    end
  end

  # Used for indexing: strips all html tags and normalizes control characters
  def stripped_body
    HTML::FullSanitizer.new.sanitize(body).gsub(/(\r\n)+/, "\n")
  end

  def frontend_body
    InternalArticleBodyHelpers.fronterize_internal_tags self
  end

  def update_with_extra_attributes(article_params)
    transaction do
      extra_attribute_model.update(article_params[:extra_attribute_model_attributes]) || raise(ActiveRecord::Rollback)
      update(article_params.except('extra_attribute_model_attributes')) || raise(ActiveRecord::Rollback)
    end
  end

  protected
    # Automatically tags an article based on its content using an autotagger
    # object (available via @autotagger)
    def autotag
      if self.body
        autotags = @autotagger.tags_from self.body
        self.transaction do
          autotags = autotags
            .map    { |t| Tag.find_or_create_by(name: t) }
            .reject { |t| t.invalid? }
          self.tags |= autotags
        end
      end
    end

    # Before-create callback that generates a slug for this article if
    # none has been defined yet. The slug will be truncated to a maximum of
    # Article::MAX_SLUG_LENGTH characters
    def set_slug
      self.slug = title.parameterize if slug.blank?
      self.slug = slug.truncate(MAX_SLUG_LENGTH, omission: '', separator: '-') if slug.length > MAX_SLUG_LENGTH
    end

    # After-create callback that appends the ID to the autogenerated slug
    def append_id_to_slug
      reload
      update_attribute :slug, "#{slug}-#{id}" unless slug == "#{slug}-#{id}"
    end

    # Set the default values for this article without overwriting any
    # value that might have been specified for any of the target fields
    def set_defaults
      self.time = Time.current if time.nil?
      # Initialize the default autotagger - @see Article#autotag
      @autotagger = SimpleTagging.new if @autotagger.nil?
    end

    # Base votes/visits store callbacks to external DB
    def set_initial_votes
      if initial_likes_changed || initial_dislikes_changed
        Accounting::MostVotedArticles.set_initial_votes_for self, { likes: initial_likes, dislikes: initial_dislikes }
        initial_likes_changed, initial_dislikes_changed = false
      end
    end

    def set_initial_visits
      if initial_visits_changed
        Accounting::MostVisitedArticles.set_initial_visits_for self, initial_visits.to_i
        initial_visits_changed = false
      end
    end

    private

    def update_main_medium
      run_scrapping if article_media.any? and !article_media.first.main
      transaction do
        article_media.update_all(main: false)
        article_media.first.try(:update, main: true)
      end
    end

    def same_supplement
      if (self.section && self.edition)
        errors.add(
          :edition,
          I18n.t('activerecord.errors.models.article.attributes.edition.different_supplement')
        ) if self.section.supplement != self.edition.supplement
      end
    end

    def touch_related
      articles_cover_articles_component_configurations.each &:touch
    end

    def update_accounting
      Accounting::Most.update_status_for self
    end

    def run_scrapping
      FacebookScrapperWorker.perform_async self.id
    end

    def update_time
      if changed?
        newtime = DateTime.now.utc
        if time_changed?
          # TODO: Take custom date and time from modified time field and save it
          # self.time = DateTime.new(newtime.year, newtime.month, newtime.day, time.hour, time.minute)
          self.time = newtime
        else
          self.time = newtime
        end
      end
    end

    def look_for_new_resources_inside_body
      if body_changed?
        InternalArticleBodyHelpers.create_new_resources! self
      end
    end
end
