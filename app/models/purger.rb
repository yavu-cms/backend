# Monkey patch Ruby Net library in order
# to provide PURGE HTTP method
module Net
  class HTTP::Purge < HTTPRequest
    METHOD='PURGE'
    REQUEST_HAS_BODY = false
    RESPONSE_HAS_BODY = true
  end
end

class Purger
  # Number of seconds to wait in order to let the
  # frontends get up before run the purging method
  # and, so as, prevents errors
  INVALIDATION_TIME_WAITING = ENV.fetch('INVALIDATION_TIME_WAITING') { 90 }.to_i

  def self.purge_route(route, object: nil)
    cliapp = route.client_application
    purge_urls cliapp, [ route.absolute_url(object: object) ]
  end

  def self.purge_representation(representation)
    Thread.new do
      sleep INVALIDATION_TIME_WAITING
      ActiveRecord::Base.connection_pool.with_connection do
        Route.where(representation: representation).each do |route|
          if route.has_params?
            purge_absolute_urls_from(route)
          else
            purge_route route
          end
        end
        Rails.logger.info "Finished route invalidation of #{representation.name}"
      end
    end
  end

  def self.purge_article(article)
    Thread.new do
      ActiveRecord::Base.connection_pool.with_connection do
        article_routes_involved_with(article).each do |route|
          purge_route route, object: article
        end
        Rails.logger.info "Finished route invalidation of #{article.title}"
      end
    end
  end

  # This procedure must "guess" all the urls from this route
  # in function of all parameters. There can be several strategies:
  # - Calculate all possible params (ie: purge all articles from an articule route)
  # - Calculate the params using an heuristic (ie: purge the top-10 most visited articles)
  def self.purge_absolute_urls_from(route)
    #TODO: Implement the functionality
  end

  def self.purge_urls(client_application, urls)
    uris_to_purge = urls.map { |url| URI(url).path }
    purge_data = varnishes_from(client_application).product(uris_to_purge)
    purge_data.each do |varnish_data, path|
      perform_purge_req! varnish_data, URI(client_application.url).host, path
    end
    Rails.logger.info "Finished route invalidation: #{purge_data}"
  end

  protected

  def self.perform_purge_req!(varnish_data, host, path)
    http_req(Net::HTTP::Purge, varnish_data, host, path) &&
    http_req(Net::HTTP::Get, varnish_data, host, path) &&
    :ok
  rescue Net::OpenTimeout, Errno::ECONNREFUSED => e
    Rails.logger.error "Cannot connect to Varnish: #{e.message}"
    :connection_error
  rescue EOFError => e
    Rails.logger.error "Problems with Varnish connection to its backend: #{e.message}"
    :varnish_error
  end

  def self.http_req(method_class, conn_data, host, path, options: { timeout: 5 })
    Net::HTTP.start(conn_data[:host], conn_data[:port], open_timeout: options[:timeout]) do |http|
      req = method_class.new(path).tap do |req|
        req.add_field 'Host', host
      end
      response = http.request req
      if (200...400).include?(response.code.to_i)
        true
      else
        Rails.logger.error "A problem occurred. #{method_class} was not performed. Code error was: #{response.code}"
        false
      end
    end
  end

  def self.varnishes_from(client_application)
    client_application.external_varnish_urls.map do |varnish_url|
      varnish_uri = URI(varnish_url)
      { host: varnish_uri.host, port: varnish_uri.port }
    end
  end

  def self.article_routes_involved_with(article)
    ArticleRoute
      .where(client_application_id: article.section.supplement.news_source.client_applications)
      .reject do |article_route|
        article_route.constrained? && article_route.constrained_article != article.slug
      end
  end
end
