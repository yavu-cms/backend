# == Summary
# A Supplement is a logical grouping of Editions and information that allows
# having a group of related objects linked to a specific Edition, invalidate
# another group of related objects without affecting other groups of objects.
#
# === Examples
# The supplement "Everyday news" has editions that invalidate each of
# them on a daily basis, while the supplement "Saturdays" has editions
# that appear on a weekly basis, every Saturday.
# By having the supplements group their editions, the ones associated to
# the "Everyday news" supplement won't invalidate the ones associated to
# the other supplement.
class Supplement < ActiveRecord::Base
  include ExtraAttributeModelable

  belongs_to :active_edition, class_name: 'Edition'
  belongs_to :logo, class_name: 'Medium'
  belongs_to :news_source
  has_many :editions, dependent: :restrict_with_error
  has_many :articles, through: :editions
  has_many :sections, dependent: :restrict_with_error
  # Validations
  validates :news_source, presence: true
  validates :name, presence: true, length: {maximum: 255}
  validate  :active_edition_belongs_to_me

  accepts_nested_attributes_for :extra_attribute_model

  # FIXME: DEPRECATED
  # Return the default supplement
  def self.default
    Rails.logger.error <<-EOT
    --- Using Supplement.default is deprecated, called in #{caller[0]}.
    Please, fix it soon. Perhaps, you could use NewsSource.default_supplement... ---
      EOT
    NewsSource.default_supplement
  end

  def to_s
    name
  end

  def siblings_and_i
    news_source.supplements
  end

  # Make this supplement the default one
  def become_default!
    transaction do
      siblings_and_i.update_all(is_default: false)
      self.update!(is_default: true)
    end
    self
  end

  # Replace the active edition (if had some) with _edition_.
  def replace_active_edition_with!(edition)
    active_edition.become_inactive! if active_edition
    self.update!(active_edition: edition)
  end

  # Clear the currently active edition on this supplement
  def clear_active_edition!
    self.update!(active_edition: nil)
  end

  private
    # Validate that the +active_edition+ set to this supplement actually belongs to itself
    def active_edition_belongs_to_me
      if active_edition && active_edition.supplement != self
        errors.add(:active_edition, I18n.t('activerecord.errors.models.supplement.active_edition'))
      end
    end
end
