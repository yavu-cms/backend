class ClientApplicationAssetsBaseRepresentation < ActiveRecord::Base
  belongs_to :base_representation
  belongs_to :client_application_asset

  after_destroy :update_representation_assets
  after_create :update_representation_assets

  def to_s
    base_representation.to_s if base_representation
  end

  def copy
    dup except: :base_representation_id
  end

  private
  def update_representation_assets
    base_representation.update_assets! if base_representation
  end
end
