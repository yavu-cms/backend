class SectionRoute < Route
  def self.available_parameters
    %w(section)
  end

  # Get the value of the :section constraint
  def constrained_section
     constraints[:section]
  end

  # Set value for :section constraint
  def constrained_section=(value)
    set_constrained_item :section, value
  end

  def absolute_url(object: nil)
    base_url = super
    if object
      base_url
        .gsub(':section', object.slug)
    else
      base_url
    end
  end

  protected
  def _populate_evaluated_context(evaluated_context, params)
    section_slug = params.delete 'section'
    section = news_source.sections.visible.find_by_slug! section_slug
    evaluated_context[:section] = section
    _set_seo evaluated_context, section
    super
  end

  def _set_seo(evaluated_context, section)
    # Build static part
    static_seo = {
      title: section.extra_attribute_model.seo_title,
      description: section.extra_attribute_model.seo_description,
      keywords: section.extra_attribute_model.seo_keywords
    } rescue {}
    # Build dynamic part
    best = client_application.home_articles section, 2
    evaluated_context[:seo] = {
      title: "#{best.map(&:title).join(', ')} #{static_seo[:title]}",
      description: "#{section.name} #{Date.today} #{static_seo[:description]}",
      keywords: "#{best.first.try(:tags).try(:join, ', ')} #{static_seo[:keywords]}"
    }
  end
end
