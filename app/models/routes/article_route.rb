class ArticleRoute < Route
  # Return the parameters that this type of route accepts
  def self.available_parameters
    %w(section article)
  end

  def constrained_article_id
    news_source.articles.visible.find_by(slug: constrained_article).try(:id) if constrained_article.present?
  end

  def constrained_article_id=(article_id)
    self.constrained_article = news_source.articles.visible.find_by(id: article_id).try(:slug)
  end

  # Get the value of the :article constraint
  def constrained_article
     constraints[:article]
  end

  # Set value for :article constraint
  def constrained_article=(value)
    set_constrained_item :article, value
  end

  # Get the value of the :section constraint
  def constrained_section
     constraints[:section]
  end

  # Set value for :section constraint
  def constrained_section=(value)
    set_constrained_item :section, value
  end

  # Tries to visit an article
  def accounting(params, count = 1)
    article(params).visit! count
  rescue
    false
  end

  def absolute_url(object: nil)
    base_url = super
    if object
      base_url
        .gsub(':section', object.section.slug)
        .gsub(':article', object.slug)
    else
      base_url
    end
  end

  protected

  def _populate_evaluated_context(evaluated_context, params)
    present_article = article(params)
    evaluated_context[:article] = present_article
    _set_seo evaluated_context, present_article
    super
  end

  def _set_seo(evaluated_context, article)
    # Build static part
    static_seo = {
      title: article.section.extra_attribute_model.seo_title,
      description: article.section.extra_attribute_model.seo_description,
      keywords: article.section.extra_attribute_model.seo_keywords
    } rescue {}
    # Build dynamic part
    evaluated_context[:seo] = {
      title: "#{article.title} #{static_seo[:title]}",
      description: "#{article.section.name} #{article.full_date} #{article.lead} #{static_seo[:description]}",
      keywords: "#{article.related_tags.root.visible.reorder('name').join(', ')} #{static_seo[:keywords]}"
    }
  end

  def article(params)
    section_slug = params.delete 'section'
    article_slug = params.delete 'article'
    if section_slug
      section = news_source.sections.visible.find_by_slug! section_slug
      section.articles.visible.find_by_slug! article_slug
    else
      news_source.articles.visible.find_by_slug! article_slug
    end
  end
end
