class Route < ActiveRecord::Base
  belongs_to :client_application, touch: true, inverse_of: :routes
  delegate :news_source, to: :client_application

  belongs_to :representation, foreign_key: 'representation_id'
  has_many :component_configurations, through: :representation
  has_many :services, through: :representation

  after_initialize :set_default_constraints

  serialize :constraints, Hash

  validates :client_application,   presence: true
  validates :pattern,              presence: true, uniqueness: { scope: :client_application_id }, format: { with: /\A\// }
  validates :representation,       presence: true, if: ->{ requires_representation? }
  validates :name,                 presence: true, uniqueness: { scope: :client_application_id }
  validate  :pattern_parameters_availability
  validates_length_of %i[name pattern redirect_url type cache_control], maximum: 255

  # Needed for route controller that save routes
  # These accessors are defined in specific subclasses
  attr_accessor :constrained_article, :constrained_article_id, :constrained_section

  set_api_resource id: :pattern,
    name: :name,
    priority: :priority,
    default_route: :default_route,
    layouts: :layouts,
    partials: ->(x) { representation && representation.templates[:partials] },
    error: ->(x) { representation && representation.templates[:error] },
    translations: ->(x) { representation && representation.translations },
    cache_control: :calculated_cache_control,
    services: :component_configuration_services,
    formats: ->(x) { representation && representation.formats }


  def component_configuration_services
    component_configurations.map { |c| c.configured_services.values }.flatten
  end

  def calculated_cache_control
    cache_control.blank? ? cache_control_default : cache_control
  end

  def cache_control_default
    client_application.try(:calculated_route_cache_control)
  end

  default_scope { order('priority DESC') }

  # Return an array of available Routes
  def self.types
    [ArticleRoute, HomepageRoute, RedirectRoute, SectionRoute, SearchRoute, CustomRoute, NewsletterRoute]
  end

  # Every subclass shall overwrite this method
  def self.available_parameters
    []
  end

  def available_parameters
    self.class.available_parameters
  end

  # Only redirec routes returns true
  def self.requires_redirect_url?
    false
  end

  def requires_redirect_url?
    self.class.requires_redirect_url?
  end

  # Almost every route will need a representation
  def self.requires_representation?
    true
  end

  # Instance version of class method
  def requires_representation?
    self.class.requires_representation?
  end

  def to_s
    name
  end

  def slug
    name.parameterize('_')
  end

  # Can be overriden in subclasses
  def absolute_url(object: nil)
    "#{client_application.url.gsub(/\/$/,'')}#{pattern}"
  end

  def has_params?
    !!pattern.match(/:\w+/)
  end

  # Make some accounting tracking over the object related.
  # Returns true if tracking was sucessfull. Otherwise, returns false.
  # Due the latter is route type dependant, this method
  # MUST BE implemented in the subclasses
  def accounting(params, count); end

  # This method is the starting point of Context Generation Chain:
  # It uses a ContextProcessor to return an EvaluatedContext object with all
  # objects needed to render route view
  def build_context(params={}, headers = {})
    ContextProcessor.new(self).build(params.stringify_keys, headers)
  end

  # Add received parameters constrained to received evaluated_context
  def populate_evaluated_context(evaluated_context, params)
    evaluated_context[:route] = self
    evaluated_context[:client_application] = client_application
    _populate_evaluated_context(evaluated_context, params.merge(constraints))
  end

  # Answer whether this route has any constraint applied
  def constrained?
    !constraints.empty?
  end
  alias_method :constrained, :constrained?

  # Needed for client_application controller that save us
  def constrained=(val)
    val = val == '1' if val.is_a? String
    self.constraints = {} unless val
  end


  def for_preview(preview_representation = nil)
    becomes(preview_route_class).tap do |pr|
      pr.preview_representation = preview_representation || representation
      pr.updated_at = Time.now # Avoid caching
    end
  end

  def override_client_application_cache_control
    !cache_control.blank?
  end

  def layouts
    representation.try(:views_by_format) || {}
  end

  def full_url(prepend_url)
    "#{prepend_url}#{pattern}"
  end

  protected

  def preview_route_class
    "Preview#{self.class.to_s}".constantize
  end

  # Specialized action implemented by subclasses
  def _populate_evaluated_context(evaluated_context, params)
    params.each { |attr, value| evaluated_context[attr] = value }
    # Make sure that route subclass has set seo context info:
    # If not, we do it here with the route seo data or the default seo data
    _set_seo(evaluated_context, nil) if evaluated_context[:seo].nil?
  end

  # OVERRIDE this method in each route to customize the seo information
  def _set_seo(evaluated_context, model)
    evaluated_context[:seo] = {
      title: client_application.extra_attribute_model.seo_title,
      description: client_application.extra_attribute_model.seo_description,
      keywords: client_application.extra_attribute_model.seo_keywords
    } rescue {}
  end

  def extract_parameters_from(pattern)
    matches = []
    (pattern || "").scan(/:([^\/]+)/) { |match| matches = Regexp.last_match.captures }
    matches
  end

  # Check if used parameters considers every available parameters
  def pattern_parameters_availability
    used_parameters = extract_parameters_from pattern
    unless (used_parameters - available_parameters).empty?
      errors.add(:pattern)
      false
    end
  end

  # Set an item value/object over a item constrain (which is a symbol).
  # If item is nil, the corresponding constrain will be deleted.
  # Item object can be an article, a section, ...
  def set_constrained_item(item_constrain, item_object)
    if item_object.blank?
      self.constraints.delete item_constrain
    else
      self.constraints[item_constrain] = item_object
    end
    attribute_will_change! 'constraints'
  end

  # Set the default value for the :constraints column if it has no value set
  def set_default_constraints
    self.constraints ||= {}
  end
end
