class SearchRoute < Route
  def self.available_parameters
    %w(query date section_name order_by page)
  end

  def cache_control_default
    'public, must-revalidate, max-age=120, s-maxage=600'
  end

  protected

  def _populate_evaluated_context(evaluated_context, params)
    evaluated_context[:q] = params.delete('query').try(:strip)
    evaluated_context[:date] = params.delete('date')
    evaluated_context[:section_name] = params.delete('section_name')
    evaluated_context[:order_by] = params.delete('order_by')
    evaluated_context[:page] = Integer(params.delete('page')) rescue 1
    super
  end
end
