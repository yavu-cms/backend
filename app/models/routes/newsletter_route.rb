class NewsletterRoute < Route
  # Return the parameters that this type of route accepts
  def self.available_parameters
    %w(newsletter)
  end

  def self.requires_representation?
    false
  end

  def representation
    Representation.new.tap do |r|
      r.define_singleton_method(:formats) { ['text/html'] }
      r.define_singleton_method(:templates) { {partials: {}, error: ''} }
      r.define_singleton_method(:translations) { {} }
      r.define_singleton_method(:views_by_format) { nil }
    end
  end

  protected

  def _populate_evaluated_context(evaluated_context, params)
    newsletter_slug = params.delete 'newsletter'
    newsletter = NewsletterGeneratedData.find_by!(slug: newsletter_slug)
    evaluated_context[:newsletter] = newsletter.content
    super
  end
end
