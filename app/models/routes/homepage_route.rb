class HomepageRoute < Route

  protected

  def _set_seo(evaluated_context, _)
    # Build static part
    static_seo = {
      title: client_application.extra_attribute_model.seo_title,
      description: client_application.extra_attribute_model.seo_description,
      keywords: client_application.extra_attribute_model.seo_keywords
    } rescue {}
    # Build dynamic part
    best = representation.cover_articles.first(3)
    evaluated_context[:seo] = {
      title: "#{best.map(&:title).join(', ')} #{static_seo[:title]}",
      description: "#{best.first.try(:tags).try(:join, ', ')} #{static_seo[:description]}",
      keywords: "#{best.first.try(:tags).try(:join, ', ')} #{static_seo[:keywords]}"
    }
  end
end
