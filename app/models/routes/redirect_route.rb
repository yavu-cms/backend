class RedirectRoute < Route
  validates :redirect_url, presence: true

  set_api_resource id: :pattern,
    priority: :priority,
    redirect_url: :redirect_url

  def self.requires_redirect_url?
    true
  end

  def self.requires_representation?
    false
  end
end
