# Wraps a Faye Client to set announcements for a specific user
class Announcement
  class << self
    include AnnouncementsHelper

    # Type may be :alert or :info
    def publish(message, user: nil, detail: '', type: :notice, url: nil)
      user = User.find_by(username: user) if user && !user.is_a?(User)
      data = {
        channel: channel_path(user),
        data: {
          type: type,
          message: message,
          detail: detail,
          created_at: Time.current
        }
      }
      Net::HTTP.post_form build_uri(url), message: data.to_json
    end

    def notify_updates_to(channel, info: {}, url: nil)
      Net::HTTP.post_form build_uri(url), message: { channel: channel, data: info }.to_json
    end

    protected

    def build_uri(url)
      raise 'Missing announcements URL' if url.blank?
      URI(url)
    end
  end
end