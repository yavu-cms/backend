class Row < ActiveRecord::Base
  include RenderableWithChildren

  serialize :configuration, Hash

  belongs_to :representation, foreign_key: 'representation_id', class_name: 'BaseRepresentation'
  has_many   :columns, dependent: :destroy
  belongs_to :view, dependent: :delete

  validates :representation, presence: true
  validates :order, presence: true

  default_scope { order('rows.order ASC') }

  def default_view
    representation.default_row_view
  end

  def children
    columns
  end

  def children=(columns)
    self.columns = columns
  end

  def parent
    representation
  end

  def parent=(representation)
    self.representation = representation
  end

  # Return a copy of this row, preserving same order 
  # and copying columns. Representation must be set to nil
  # See test case to understand what I'm exactly doing
  def copy
    copy = dup except: :representation_id, include: :view
    copy.columns = columns.map(&:copy).each { |x| x.row = copy }
    copy
  end
end
