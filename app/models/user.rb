class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :registerable, :validatable, and :omniauthable
  devise :database_authenticatable, :timeoutable, :recoverable, :rememberable, :trackable

  has_and_belongs_to_many :roles
  has_and_belongs_to_many :sections
  has_and_belongs_to_many :client_applications
  has_and_belongs_to_many :representations, association_foreign_key: 'base_representation_id'
  has_many :shortcuts

  # Virtual field to run password validations over unencrypted password
  attr_accessor :new_password, :requires_current_password

  validates :email, uniqueness: true, presence: true
  validates :username, uniqueness: true, presence: true
  validates :new_password, confirmation: true, if: '@new_password.present?'
  validates :new_password, presence: true, on: :create
  validate  :check_current_password, if: 'requires_current_password && @new_password.present?', on: :update
  validates_length_of %i[username email encrypted_password current_sign_in_ip last_sign_in_ip], maximum: 255

  before_validation :clean_workspace
  before_save :update_password

  def self.change_representation_reference!(new_id, old_id)
    BaseRepresentationUser
      .where(base_representation_id: old_id)
      .update_all(base_representation_id: new_id)
  end

  def to_s
    username
  end

  def new_password=(value)
    @new_password = value.presence
  end

  def current_password=(value)
    @current_password = value
  end

  def check_current_password
    unless valid_password? @current_password
      errors.add(:current_password,  I18n.t('activerecord.errors.models.user.password'))
      false
    end
  end

  def permissions
    @permissions ||= roles.enabled.map(&:permissions).flatten.uniq
  end

  def has_permission?(permission)
    permissions.include? permission
  end

  def accessible_sections
    @accessible_sections ||= if all_sections?
                               Section.all
                             else
                               sections.map(&:offspring).flatten.uniq
                             end
  end

  def accessible_client_applications
    @accessible_client_applications ||= all_client_applications?? ClientApplication.all : client_applications
  end

  def accessible_representations
    @accessible_representations ||= all_representations?? BaseRepresentation.all : representations
  end

  def workspace
    accessible_sections + accessible_client_applications + accessible_representations
  end

  def included_in_workspace?(resource)
    workspace.include? resource
  end

  def add_to_workspace(resource_or_resources)
    resource_or_resources = [resource_or_resources] unless resource_or_resources.respond_to?(:each)
    resource_or_resources.each do |resource|
      case resource
        when ClientApplication
          self.client_applications << resource
        when Section
          self.sections << resource
        when BaseRepresentation
          self.representations << resource
        else
          raise "You are trying to add a resource of class #{resource.class} to the user workspace, but there is no association for that kind of resources"
      end
    end
  end

  private

  # Clean the workspace if needed
  def clean_workspace
    self.client_applications.clear if all_client_applications
    self.sections.clear if all_sections
    self.representations.clear if all_representations
  end

  # Update the current password only if a new one has been provided
  def update_password
    self.password = new_password unless new_password.blank?
  end
end
