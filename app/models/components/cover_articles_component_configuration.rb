class CoverArticlesComponentConfiguration < BaseComponentConfiguration
  has_many :articles_cover_articles_component_configurations, -> { includes(article: [:section,:edition]).merge(Article.unscoped_visible) }, dependent: :destroy
  has_many :articles, -> {includes(articles_cover_articles_component_configurations: { article: [:section,:edition] }).merge(Article.unscoped_visible)}, through: :articles_cover_articles_component_configurations
  belongs_to :cover_component_configuration, touch: true

  delegate :representation, to: :cover_component_configuration, allow_nil: true

  validates_presence_of :cover_component_configuration

  def to_s
    description || ''
  end

  def ordered_articles=(articles_with_views)
    # Build an ordered list of the articles with their corresponding views.
    # +articles_with_views+ is expected to be an array of hashes like +{id: <article_id>, view_id: <view_id>}+
    self.articles_cover_articles_component_configurations.clear
    articles_with_views.each_with_index do |data, idx|
      data.stringify_keys!
      article = Article.find_by id: data['id']
      view = component.views.find_by(id: data['view_id'])
      next unless article && view
      self.articles_cover_articles_component_configurations << articles_cover_articles_component_configurations.build(article: article, order: idx, view: view, cover_articles_component_configuration: self)
    end
  end

  # Return a copy of this component_configuration, preserving same values
  def copy
    super.tap do |copy|
      copy.articles_cover_articles_component_configurations = articles_cover_articles_component_configurations.map do |each|
        each.copy.tap do |each_copy|
          each_copy.cover_articles_component_configuration = copy
        end
      end
    end
  end
end
