# This component group articles in specified order
# allowing the user to show them in different ways
# For example: assume there are 30 articles associated
# We can select one of the different ways to draw this component
#   1.- One big article (the first one will be shown)
#   2.- 5 articles sliding as a gallery
#   3.- Two columns of 15 small articles
class CoverArticlesComponent < BaseComponent
  # This is a CoverArticlesComponent
  def cover_articles?
    true
  end

  def takes_layout_views?
    true
  end
end