# Collection of CoverArticlesComponentConfigurations
class CoverComponentConfiguration < BaseComponentConfiguration
  include FindableInOrder

  has_many :cover_articles_component_configurations, dependent: :destroy

  # Return the merge of every hash returned from cover_articles_component_configurations
  # This will merge repeated views so it reduces data
  def templates
    cover_articles_component_configurations.map(&:templates).inject({}) { |ret, hash_element| ret.merge hash_element }
  end

  # Return the merge of every hash returned from cover_articles_component_configurations
  # This will merge repeated views so it reduces data
  def translations
    cover_articles_component_configurations.map(&:translations).inject({}) { |ret, hash_element| ret.merge hash_element }
  end

  # Return a copy of this component_configuration, preserving same values
  # and copying cover_articles_component_configurations
  def copy
    super.tap do |copy|
      copy.cover_articles_component_configurations = cover_articles_component_configurations.map do |each|
        each.copy.tap do |each_copy|
          each_copy.cover_component_configuration = copy
        end
      end
    end
  end
end
