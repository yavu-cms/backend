class ComponentTranslation < ActiveRecord::Base
  belongs_to :component,
    foreign_key: 'component_id',
    touch: true,
    class_name: 'BaseComponent'

  validate :validate_yaml
  validates_presence_of [:component, :locale]
  validates_uniqueness_of :locale, scope: :component
  validates :locale, length: {maximum: 255}

  def self.filetype
    'text/x-yaml'
  end

  def filetype
    self.class.filetype
  end

  def to_s
    locale
  end

  def to_hash
    parse_values! || {}
  end

  private
    # Must fail when YAML is malformed
    # This method must be used by: validate_yaml & parse_values 
    def parse_values!
      YAML.load(values || "")
    end

    def validate_yaml
      parse_values!
    rescue Psych::SyntaxError
      errors.add(:values, I18n.t('errors.messages.yml_format_error'))
    end
end
