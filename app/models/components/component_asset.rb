class ComponentAsset < ActiveRecord::Base
  include Asset
  self.inheritance_column = nil # no STI

  belongs_to :component,
    foreign_key: 'component_id',
    touch: true,
    class_name: 'BaseComponent'

  mount_uploader :file, ComponentAssetUploader

  after_save :update_component_assets
  after_destroy :update_component_assets

  attr_accessor :skip_update_component_assets
  skip_callback :save,    :after, :update_component_assets, if: -> { self.skip_update_component_assets }
  skip_callback :destroy, :after, :update_component_assets, if: -> { self.skip_update_component_assets }

  validates_presence_of [ :component, :type, :file ]
  validates_length_of   [ :type, :content_type ], {maximum: 255}

  # We must check for uniqueness of file with scope [:component_id, :type]
  # manually because carrierwave sets file after saving record, and before commiting changes
  # There is a database unique index, so check if database restriction is applied
  validate :file_uniqueness

  default_scope { order(:file) }

  # Complete path to this asset's dir.
  # e.g. data/components/asset_type/component_slug
  def absolute_url
    "#{ComponentAssetUploader.absolute_prefix_for_type(type)}/#{component.slug}"
  end

  private
    def file_uniqueness
      if ComponentAsset.where('component_id = ? and type = ? and file = ?', component_id, type, file.filename).any?
        errors.add(:file, I18n.t('errors.messages.taken'))
      end
    end

    def update_component_assets
      component.update_representation_assets
    end
end
