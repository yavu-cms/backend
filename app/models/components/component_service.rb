class ComponentService < ActiveRecord::Base
  extend ::Enumerize
  enumerize :http_method, in: [:GET, :POST, :PUT, :DELETE]

  serialize :available_parameters, Array

  belongs_to :view
  belongs_to :component,
    foreign_key: 'component_id',
    touch: true,
    class_name: 'BaseComponent'

  validates :view, presence: true
  validates :component, presence: true
  validates :name, presence: true, uniqueness: { scope: [:http_method, :component_id] }
  validates :http_method, presence: true
  validates :cache_control, :name, :http_method, length: {maximum: 255}

  # Useful to emulate component_service_configurations. They are not an entity
  # They are just component_services with an component_configuration associated
  attr_accessor :component_configuration_id, :component_configuration

  set_api_resource id: :slug,
    component_configuration_id: :component_configuration_id,
    cache_control: :calculated_cache_control,
    http_method: :http_method,
    template: :template,
    body: :body

  def cache_key
    "#{super}/#{component_configuration_id}"
  end

  def to_s
    "#{http_method} #{slug}"
  end

  def calculated_cache_control
    cache_control.blank? ? component_configuration.try(:representation).try(:client_application).try(:calculated_service_cache_control) : cache_control
  end

  def cache_public?
    calculated_cache_control =~ /public/i
  end

  def slug
    name.parameterize
  end

  def available_parameters_string
    available_parameters.join ','
  end

  def available_parameters_string=(v)
    self.available_parameters = Set.new(v.split(',')).to_a
  end


  def template
    view.template_name
  end

  def component_configuration
    @component_configuration ||= BaseComponentConfiguration.find(component_configuration_id) if component_configuration_id
  end

  # This method is the starting point of Context Generation Chain:
  # It uses a SericeContextProcessor to return an EvaluatedContext object with all
  # objects needed to render service view
  def build_context(params={}, headers = {})
    ServiceContextProcessor.new(self).build(params.stringify_keys, headers)
  end

  # Specialized action implemented by subclasses
  def populate_evaluated_context(evaluated_context, params)
    params.each do |attr,value|
      evaluated_context[attr] =  value
    end
    evaluated_context[:client_application] = component_configuration.try(:representation).try(:client_application)
  end
end
