class BaseComponentConfiguration < ActiveRecord::Base
  belongs_to :component,
    foreign_key: 'component_id',
    class_name: 'BaseComponent'

  belongs_to :column

  belongs_to :view

  has_many :fields,
           class_name: 'ComponentField',
           dependent: :destroy,
           foreign_key: 'base_component_configuration_id'

  delegate :representation, to: :column, allow_nil: true

  delegate :services, to: :component

  default_scope { order('base_component_configurations.order ASC') }

  validates :component, presence: true
  validates :description, presence: true, length: {maximum: 255}
  validates :view, presence: true, unless: :cover?
  validates :column, presence: true, unless: :cover_articles?
  validates :order, presence: true
  validates :type, length: {maximum: 255}

  validate :view_belongs_to_component, unless: :cover?
  after_save :update_representation_assets!

  def to_s
    description || ''
  end

  def configured_services(duplicate = false)
    ss = duplicate ? ComponentService.find(services.pluck(:id)) : services
    HashWithIndifferentAccess[ss.each {|x| x.component_configuration_id = id}.map {|c| [c.slug, c]}]
  end

  # Return the settings specified for this component
  def values
    HashWithIndifferentAccess[fields.map { |f| [f.name, f.value] }]
  end

  def values=(values)
    self.fields = component.fields_hash.map do |name, field|
      next unless (value = values[name]).present?
      field.copy value: value, component: nil, component_configuration: self
    end.reject(&:nil?)
  end

  # Return values merged with component defaults
  def available_attributes
    HashWithIndifferentAccess[all_fields_hash.map { |name, field| [name, field.value] }]
  end

  # Is this a CoverComponentConfiguration?
  def cover?
    component && component.cover?
  end

  # Is this a CoverArticlesComponentConfiguration?
  def cover_articles?
    component && component.cover_articles?
  end

  # Return a view, either the one specified on this configuration or the first one from the
  # component (as a failsafe mechanism)
  def renderable_view(format)
    case format
      when 'text/html'
        view || component.views.first
      else
        component.views.find_by(format: format)
    end
  end

  # Return a hash with each component's associated view. One of those
  # views must be component main view
  # The hash format is:
  # { view.template_name => view.value }
  def templates
    # We solve this problem unifying partials with main view
    # and then map this array into an array of hashes, each hash with
    # views name as key and views value as value. Finally we need to merge
    # all hashes into a unique hash. By @car
    component.views.map do |v|
      { v.template_name => v.value }
    end.inject({}) { |ret, hash_element| ret.merge hash_element }
  end

  # Return a hash with each component's translations
  # The hash format is:
  # { locale => translation.to_hash }
  def translations
    component.translations.map do |translation|
      { translation.locale => translation.to_hash }
    end.inject({}) { |ret, hash_element| ret.merge hash_element }
  end

  # Return a copy of this component_configuration, preserving same values
  def copy
    copy = dup except: [ :column_id, :cover_component_configuration_id ]
    copy.component = component
    copy.fields = fields.map { |x| x.copy component_configuration: copy }
    copy
  end

  def fields_hash
    HashWithIndifferentAccess[fields.map { |f| [f.name, f] }]
  end

  def all_fields_hash
    component_fields = component.fields_hash
    fields.each do |f|
      component_fields[f.name] = f if component_fields.has_key?(f.name)
    end
    component_fields
  end

  private

  def view_belongs_to_component
    errors.add(:view, I18n.t('errors.messages.must_be_a_component_view')) unless view && view.component == component
  end

  def update_representation_assets!
    representation.update_assets! if representation && representation.persisted?
  end
end
