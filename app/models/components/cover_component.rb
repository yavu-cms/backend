# This component is a collection of CoverArticlesComponent
# So it's view is a renderization of each
class CoverComponent < BaseComponent

  # This is a CoverComponent
  def cover?
    true
  end

  # As this component does not have assocaited views
  # Any view can always be destroyed
  def destroy_view?(view)
    true
  end

end
