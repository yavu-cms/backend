class BaseComponent < ActiveRecord::Base
  include Versionable

  MAX_SLUG_LENGTH = 240

  has_many :views,
    dependent: :delete_all,
    foreign_key: 'component_id'

  has_many :services,
    class_name: 'ComponentService',
    foreign_key: 'component_id',
    dependent: :destroy

  has_many :translations,
    dependent: :destroy,
    class_name: 'ComponentTranslation',
    foreign_key: 'component_id'

  has_many :assets,
    class_name: 'ComponentAsset',
    dependent: :destroy,
    foreign_key: 'component_id'

  has_many :configurations,
    class_name: 'BaseComponentConfiguration',
    dependent: :restrict_with_error,
    foreign_key: 'component_id'

  has_many :fields,
    class_name: 'ComponentField',
    dependent: :destroy,
    foreign_key: 'base_component_id'

  # Validations
  validates :name, presence: true, uniqueness: true, length: {maximum: 255}
  validates :views, presence: true, unless: :cover?
  validate  :views_dont_repeat_alternative_formats, unless: :cover?
  validates :type, length: {maximum: 255}

  # Default scope is ordering by name
  default_scope { order(name: :asc) }

  # Scopes
  scope :enabled, ->{ where(enabled: true) }

  # Callbacks
  before_save :set_slug
  after_save :touch_configurations
  after_touch :touch_configurations

  accepts_nested_attributes_for :fields, allow_destroy: true, reject_if: ->(values) { values['name'].blank? || values['type'].blank? }

  # Return an array of available types
  def self.types
    [Component, CoverComponent, CoverArticlesComponent]
  end

  # Search components by name
  def self.search(name)
    if name.present?
      BaseComponent.where("name like :name", name: "%#{name}%")
    else
      []
    end
  end

  # Return those representations where a component instance
  # (component configuration) is present
  def associated_representations
    configurations.map { |conf| conf.representation }.uniq.compact
  end

  def self.available_for_editor
    where.not(type: CoverArticlesComponent)
  end

  def to_s
    name.to_s
  end

  def fields_hash
    HashWithIndifferentAccess[fields.map { |f| [f.name, f] }]
  end

  def defaults
    HashWithIndifferentAccess[fields.map { |f| [f.name, f.value] }]
  end

  def defaults=(defaults)
    self.fields = defaults.map do |name, info|
      ComponentField.build_from(info, name, component: self) rescue next
    end.reject(&:nil?)
  end

  # Is this a CoverComponent?
  def cover?
    false
  end

  # Is this a CoverArticlesComponent?
  def cover_articles?
    false
  end

  # Is this a CoverArticlesComponent?
  def takes_layout_views?
    false
  end

  # Instantiates a new ComponentConfiguration for me
  def new_configuration(attributes = {})
    attributes.merge! component: self
    configuration = "#{self.class}Configuration".constantize.new(attributes)
    configurations << configuration
    configuration
  end

  # Return a copy of this component, updating its name and including any
  # related views, translations and assets.
  def copy(name = nil)
    copy = dup include: [:views, :translations, :fields] # , :services] # Seems to be giving trouble
    self.assets.each do |asset|
      copy.assets.build file: File.open(asset.file.current_path,'r'), component: copy
    end
    copy.name = name || I18n.t('components.copy.prefix', default: 'Copy of %{name}', name: self.name)
    copy
  end

  # View before_destroy delegation for specified view
  # Any view can be destroyed if there are at least more than one view
  # associated to a component
  def destroy_view?(view)
    views.include?(view) && views.count > 1
  end

  def update_representation_assets
    configurations.map(&:representation).reject(&:nil?).uniq.each do |rep|
      rep.update_assets!
    end
  end

  def default_view
    views.find_by(default: true)
  end

  def self.assets_append_paths
    AssetUploader.types.inject([]) do |result, asset_type|
      result << ComponentAssetUploader.absolute_prefix_for_type(asset_type)
    end.map(&:to_s)
  end

  private
  # Before-save callback that generates a slug for this component
  # The slug will be truncated to a maximum of Component::MAX_SLUG_LENGTH characters
  def set_slug
    self.slug = name.parameterize('_')
    self.slug = slug.truncate(MAX_SLUG_LENGTH, omission: '', separator: '-') if slug.length > MAX_SLUG_LENGTH
  end

  def touch_configurations
    configurations.update_all(updated_at: Time.now)
    configurations.each do |conf|
      if conf.representation
        conf.representation.touch
        conf.representation.touch_routes
      end
    end
  end

  def views_dont_repeat_alternative_formats
    formats = views.map(&:format).reject { |f| GenericRenderer.html? f }
    if formats.count > formats.uniq.count
      errors.add(:views, I18n.t('errors.messages.format_repeated'))
      false
    end
  end
end
