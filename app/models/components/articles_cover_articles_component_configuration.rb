class ArticlesCoverArticlesComponentConfiguration < ActiveRecord::Base
  belongs_to :article
  belongs_to :cover_articles_component_configuration, touch: true
  belongs_to :view

  validates_presence_of [:cover_articles_component_configuration, :article]
  validates_uniqueness_of :article, scope: [:cover_articles_component_configuration_id]
  validates :view, presence: true
  validate  :view_belongs_to_component

  default_scope { includes(:article).references(:article).order('articles_cover_articles_component_configurations.order ASC') }

  # Attributes to be exported for API
  set_api_resource id: :id,
                   article: :article,
                   partial: ->(x) { view.template_name }

  def copy
    dup except: [ :cover_articles_component_configuration_id ]
  end

  def component
    @component ||= cover_articles_component_configuration.component if cover_articles_component_configuration
  end

  private

  def view_belongs_to_component
    errors.add(:view, :invalid) unless component && component.views.include?(view)
  end
end
