class Section < ActiveRecord::Base
  include FindableInOrder, ExtraAttributeModelable, Versionable

  belongs_to :supplement
  has_one    :news_source, through: :supplement
  belongs_to :parent, class_name: 'Section'
  belongs_to :main_article, class_name: 'Article'

  has_many :children, class_name: 'Section', foreign_key: 'parent_id'
  has_many :articles, dependent: :restrict_with_error
  has_many :tags, through: :articles
  has_many :component_values, as: :referable, dependent: :restrict_with_error
  # Validations
  validates :name, presence: true, length: {maximum: 255}
  validates :supplement, presence: true
  validates :slug, presence: true, length: {maximum: 255}
  validate  :not_parent_of_itself
  validate  :check_uniqueness_in_newssource
  # Callbacks
  after_initialize :set_slug
  after_update     :touch_articles, :touch_children

  # It's a fake touch for the sake of simplicity in our callbacks nesting graph
  def touch_children
    children.find_each { |child| child.save(updated_at: Time.now) }
  end

  default_scope { order(:name) }

  scope :with_supplement, ->{ eager_load(:supplement) }
  scope :visible, -> { where(is_visible: true) }
  scope :workspace_of, ->(user) { where(id: user.accessible_sections) }

  accepts_nested_attributes_for :extra_attribute_model

  # Attributes to be exported for API
  set_api_resource id: :slug,
    slug: :slug,
    name: :name,
    color: :color,
    priority: :priority,
    children: ->(_) do
      unless api_resource_settings[:no_include_section_children]
        limit = api_resource_settings[:section_children_limit]
        sorted_children = children.sort { |x, y| x.priority <=> y.priority }
        limit ? sorted_children.first(limit) : sorted_children
      end
    end

  def priority
    extra_attribute_model.priority.to_i
  rescue
    0
  end

  def color
    my_color = extra_attribute_model.color
    my_color.presence || parent.try(:color)
  rescue
    nil
  end

  # Return all sections *except* the one passed in as argument
  def self.but(section)
    section ? where.not(id: section) : where(nil)
  end

  # Used for distinguishing which sections should be visible in the frontend application
  # @see ComponentFieldModel#value
  # @see ComponentFieldMultiModel#value
  def visible?
    is_visible?
  end

  # Scope to work with root sections
  def self.roots
    where parent_id: nil
  end

  # Search for sections using a query hash, which may contain:
  #   * query  [String] string to be searched amongst the slug and name (using OR and %<query>%)
  def self.search(params = {})
    scope = where(nil)
    scope = scope.where('slug like :query OR name like :query', query: "%#{params[:query]}%") unless params[:query].blank?
    scope
  end

  def to_s
    "#{name} (#{slug}) [#{news_source}]"
  end

  # Return whether this section is a root (i.e., it has no parent)
  def root?
    parent_id.nil?
  end

  def offspring
    [self] + children.map { |child| child.offspring }
  end

  private
    def check_uniqueness_in_newssource
      if news_source && (s = news_source.sections.find_by(slug: slug)) && (self.id != s.id)
        errors.add(:slug, I18n.t('activerecord.errors.models.section.attributes.slug.same_slug_same_newssource'))
      end
    end

    def set_slug
      self.slug = (!slug.present? ? name : slug).try(:parameterize)
    end

    # Validation callback to make sure a section is not parent of itself
    # and thus avoid circular loops on the sections tree
    def not_parent_of_itself
      if parent == self
        errors.add(:parent, I18n.t('activerecord.errors.models.section.attributes.parent.loop'))
      end
    end

    def touch_articles
      articles.find_each &:touch
    end
end
