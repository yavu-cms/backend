class Version < PaperTrail::Version

  NON_VERSIONABLE_EVENTS = %w[ create restore ]
  NON_PRINTABLE_MODELS   = %w[ ArticleMedium ]

  scope :printable, ->{ where.not(item_type: NON_PRINTABLE_MODELS) }

  def restorable?
    ! NON_VERSIONABLE_EVENTS.include? event
  end
end
