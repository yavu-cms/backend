module Accounting
  class WithExtraAttribute
    include Mongoid::Document

    field :a, as: :article,            type: String
    field :i, as: :without_main_image, type: Boolean
    field :l, as: :invisible,          type: Boolean

    index a:  1
    index i: -1
    index l: -1

    validates_presence_of :a

    class << self
      def forbidden
        self.or(invisible: true).or(without_main_image: true).map(&:article)
      end

      def invisible
        where(l: true).map(&:article)
      end

      def without_main_image
        where(i: true).map(&:article)
      end
    end
  end
end
