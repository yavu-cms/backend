module Accounting
  class Most
    include Mongoid::Document

    field :a, as: :article,        type: String
    field :d, as: :date,           type: DateTime
    field :s, as: :section,        type: Integer
    field :m, as: :migrated,       type: Boolean

    validates_presence_of :a
    validates_presence_of :d

    index a:  1
    index s:  1
    index d: -1
    index m: -1

    class << self
      def last_day(limit = 10, section: nil, with_image: nil, migrated: nil)
        last_period(DateTime.current.beginning_of_day..DateTime.current.end_of_day, limit, section: section, with_image: with_image, migrated: migrated)
      end

      def last_week(limit = 10, section: nil, with_image: nil, migrated: nil)
        last_period(DateTime.current.beginning_of_week..DateTime.current.end_of_week, limit, section: section, with_image: with_image, migrated: migrated)
      end

      def last_month(limit = 10, section: nil, with_image: nil, migrated: nil)
        last_period(DateTime.current.beginning_of_month..DateTime.current.end_of_month, limit, section: section, with_image: with_image, migrated: migrated)
      end

      def last_period(timerange, limit = 10, section: nil, with_image: nil, migrated: nil, skope: nil)
        skope ||= Article.where(nil)
        skope.find_ordered_where slug: most_something(timerange, section, with_image, migrated, limit)
      end

      def most_something(timerange, section = nil, with_image = nil, migrated = nil, limit = 10)
        raise NotImplementedError, 'This is an abstract accounting method. A subclass must to define what to do.'
      end

      def update_status_for(article)
        article_key = key_field_for(article)
        articles = Accounting::WithExtraAttribute.where(article: article_key)
        if articles.exists?
          articles.update_all(default_extra_attributes_for(article))
        elsif needs_extra_attributes?(article)
          Accounting::WithExtraAttribute.create(default_extra_attributes_for(article).merge(article: article_key))
        end
      end

      def null_record
        raise NotImplementedError, 'This is an abstract accounting method. A subclass must to define what to do.'
      end

      protected

      def prefilter(timerange, section, has_main_image, migrated = nil)
        closure = where(d: timerange)
        closure = closure.and(_type: self)
        closure = closure.not_in(a: forbidden(has_main_image.present?))
        closure = closure.and(s: key_field_for(section)) if section.present?
        closure = closure.and(m: migrated) unless migrated.nil?
        closure = closure.ne(null_record)
        closure.selector
      end

      def forbidden(include_main_image = false)
        if include_main_image
          Accounting::WithExtraAttribute.forbidden
        else
          Accounting::WithExtraAttribute.invisible
        end
      end

      def key_field_for(object)
        if object.is_a? Article
          object.slug
        elsif object.is_a? Section
          object.id
        else # Fallback to object, because probably it's the key
          object
        end
      end

      def needs_extra_attributes?(article)
        default_extra_attributes_for(article).values.any?
      end

      def default_extra_attributes_for(article)
        {
          without_main_image: article.main_image.nil?,
          invisible: !article.visible?
        }
      end

      def default_attributes_for(article)
        {
          section: key_field_for(article.section),
          date: article.date,
          migrated: article.migrated?
        }
      end
    end
  end
end
