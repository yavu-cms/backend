module Accounting
  class MostVotedArticles < Most

    field :u, as: :user,  type: String
    field :v, as: :value, type: Integer

    index u:  1
    index v: -1

    SYS_BASE_LIKE    = '__system_base_like'
    SYS_BASE_DISLIKE = '__system_base_dislike'

    def positive?
      value > 0
    end

    def negative?
      value < 0
    end

    class << self

      def null_record
        { v: 0 }
      end

      def retrieve_user_vote(user, article)
        vote = where(user: user, article: key_field_for(article)).first
        if vote
          # We're going to return a simple hash with correctly named fields
          { article: vote.article, section: vote.section, user: vote.user, value: vote.value, date: vote.date }
        end
      end

      def already_voted?(user, article)
        !retrieve_user_vote(user, article).nil?
      end

      def initial_votes_for(article)
        likes    = retrieve_user_vote(SYS_BASE_LIKE,    article) || { value: 0 }
        dislikes = retrieve_user_vote(SYS_BASE_DISLIKE, article) || { value: 0 }
        [likes[:value], dislikes[:value]]
      end

      def set_initial_votes_for(article, initial_votes)
        perform_vote(article, SYS_BASE_LIKE, initial_votes[:likes])
        perform_vote(article, SYS_BASE_DISLIKE, initial_votes[:dislikes])
      end

      # Voting methods:
      # Returns true if the voting was succesful or false if the user has already voted
      def make(article, user, feeling)
        case feeling
          when "like"    then up(article, user); true
          when "dislike" then down(article, user); true
          else false
        end
      end

      def up(article, user)
        perform_vote(article, user, 1)
      end

      def down(article, user)
        perform_vote(article, user, -1)
      end

      # Uses mongodb aggregation framework. See http://docs.mongodb.org/manual/applications/aggregation/
      def most_something(timerange, section = nil, with_image = nil, migrated = nil, limit = 10)
        collection.aggregate([
          { "$match" => prefilter(timerange, section, with_image, migrated) },
          { "$group" => { "_id" => "$a", "votes" => { "$sum" => "$v" } } },
          { "$sort" => { "votes" => -1 } },
          { "$project" => { "_id" => 0, "a" => "$_id" } },
          { "$limit" => limit }]).map { |article_entry| article_entry['a'] }
      end
      alias :most_voted :most_something

      # Retrieves all votes for an article with the following structure:
      #
      # { up: <positive_votes>, down: <negative_votes> }
      #
      # Uses mongodb aggregation framework. See http://docs.mongodb.org/manual/applications/aggregation/
      def votes(article, only_real_votes: false)
        # Filter by article
        filtering_by_article = { "$match" => { "a" => key_field_for(article) } }
        # For each vote generate a new temp document with this structure
        count_real_votes = only_real_votes ? 0 : "$v"
        counting_votes_by_type = {
          "$project" => {
            "up"   => { "$cond" => [{ "$eq" => ["$u", SYS_BASE_LIKE]},    count_real_votes, { "$cond" => [{ "$eq" => ["$v",  1]}, 1, 0]  }]},
            "down" => { "$cond" => [{ "$eq" => ["$u", SYS_BASE_DISLIKE]}, count_real_votes, { "$cond" => [{ "$eq" => ["$v", -1]}, 1, 0]  }]}
          }
        }

        # Group by article_id and sum each column
        # (in fact, there will always be only one group because of the previous match operation )
        grouping_vote_types = {
          "$group" => {
            "_id"  => "$a",
            "up"   => { "$sum" => "$up" },
            "down" => { "$sum" => "$down" }
          }
        }
        # Remove the _id field of the result document
        removing_id_field = { "$project" => { "_id" => 0, "up" => 1, "down" => 1 } }

        # Finally, feed the mongodb aggregate pipeling with each step built previosly:
        res = collection.aggregate([
          filtering_by_article,
          counting_votes_by_type,
          grouping_vote_types,
          removing_id_field]).first # because #aggregate always returns an array

        # And return the resulting hash
        res ? res.symbolize_keys : { up: 0, down: 0 }
      end

      def perform_vote(article, user, new_value)
        find_or_create_by(user: user, article: key_field_for(article)).tap do |v|
          v.update_attributes(default_attributes_for(article).merge(value: new_value, user: user))
        end
      end
    end
  end
end
