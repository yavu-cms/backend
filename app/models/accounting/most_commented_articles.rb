require 'json'

module Accounting
  class MostCommentedArticles < Most
    include Mongoid::Document

    field :i, as: :interval, type: String
    field :p, as: :posts, type: Integer

    validates_presence_of :i
    validates_presence_of :p

    class << self
      def most_something(timerange, section = nil, with_image = nil, migrated = nil, limit = 10)
        list_popular(timerange, section, with_image, migrated, limit)
      end

      def refreshDisqusDb(limit = 10)
        %w(1d 7d 30d).each do |interval|
          result = DisqusAPI.list_popular(interval, limit)
          persist(result, interval)
        end
      end

      # we delete the entry from the array if the Article is not visible
      # FIXME: Reimplement this method using .agreggate and .prefilter from Most module
      def list_popular(timerange, section = nil, with_image = nil, migrated = nil, limit = 10)
        articles = all.in(i: parse_interval(timerange)).map { |most_commented| Article.find_by slug: most_commented.a }
        articles.delete_if do |article|
          section_pred  = section        ? ->(a) { a.section != section }    : ->(_) { false }
          migrated_pred = !migrated.nil? ? ->(a) { a.migrated? == migrated } : ->(_) { false }
          !article || (!article.visible? || section_pred.(article) || migrated_pred.(article) )
        end.uniq.first(limit).map &:slug
      end

      # we only persist most commented last_day, last_week and last_month
      def persist(hash, interval)
        # we first delete all the previos persisted objects
        where(i: interval).delete

        hash.each do |elem|
          create a: elem['identifiers'].first, i: interval, d: DateTime.now, p: elem['postsInInterval']
        end
      end

      def comments_quantity_from(article, timerange: nil)
        query = where(a: key_field_for(article))
        query = query.in(i: parse_interval(timerange)) if timerange
        query.pluck(:posts).last || 0
      end

      private

      def parse_interval(interval, strict = false)
        n_days = interval.count
        interval = case n_days
        when (1..2)
          %w( 1d )
        when (7..8)
          %w( 1d 7d )
        when (28..31)
          %w( 1d 7d 30d )
        end
        strict ? [interval.last] : interval
      end
    end
  end
end
