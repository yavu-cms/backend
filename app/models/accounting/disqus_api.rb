class DisqusAPI
  cattr_accessor :config
  self.config = YavuSettings.disqus

  class << self
    # Interval types and examples:
    # ----------------------------
    # '1d'  -> Every day (daily)
    # '7d'  -> Every week (weekly)
    # '30d' -> Every month (monthly)
    def list_popular(interval = '7d', limit = 10, timeout = 60)
      url = "#{Setting['disqus.base_url']}/#{Setting['disqus.api_version']}/threads/listPopular.json?api_key=#{Setting['disqus.public_key']}&forum=#{Setting['disqus.forum_shortname']}&interval=#{interval}&limit=#{limit}"
      Rails.logger.debug('DisqusAPI') { "Fetching popular comments url=#{url}" }
      response = RestClient.get url, accept: :json, timeout: timeout
      MultiJson.load(response)['response']
    rescue => e
      []
    end

    def list_comments_from(article_identifier, limit = 100, order = 'desc', timeout = 60)
      url = "#{Setting['disqus.base_url']}/#{Setting['disqus.api_version']}/threads/listPosts.json?api_key=#{Setting['disqus.public_key']}&forum=#{Setting['disqus.forum_shortname']}&thread:ident=#{article_identifier}&limit=#{limit}&order=#{order}"
      Rails.logger.debug('DisqusAPI') { "Fetching comments url=#{url}" }
      response = RestClient.get url, accept: :json, timeout: timeout
      MultiJson.load(response)['response'].reverse
    rescue => e
      []
    end
  end

end
