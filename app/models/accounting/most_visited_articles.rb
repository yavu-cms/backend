module Accounting
  class MostVisitedArticles < Most

    field :c, as: :count,       type: Integer
    field :b, as: :is_base,     type: Boolean

    index b:  1
    index c: -1

    class << self
      def initial_visits_for(article)
        visits(article, is_base: true)
      end

      def set_initial_visits_for(article, initial_visits)
        perform_visit(article, initial_visits, is_base: true)
      end

      def visit!(article, count = 1)
        perform_visit(article, count)
      end

      def visits(article, is_base: false, only_real_visits: false)
        real_and_base_condition = only_real_visits ? { 'b' => false } : (is_base ? { 'b' => true } : {})
        collection.aggregate([
          { '$match' => { 'a' => key_field_for(article) }.merge(real_and_base_condition) },
          { '$group' => { '_id' => '$a', 'visits' => { '$sum' => '$c' } } }])
        .collect { |p| p['visits'] }.first || 0
      end

      def null_record
        { c: 0 }
      end

      private

      # Filters visits by a date range, groups visits by article and sum.
      # The result is returned as an array of articles ids.
      def most_something(timerange, section = nil, with_image = nil, migrated = nil, limit = 10)
        articles_ids = collection.aggregate([
          { '$match'   => prefilter(timerange, section, with_image, migrated) },
          { '$group'   => { '_id' => '$a', 'visits' => { '$sum' => '$c' } } },
          { '$sort'    => { 'visits' => -1 } },
          { '$project' => { '_id' => 0, 'a' => '$_id' } },
          { '$limit'   => limit }]).map { |article_entry| article_entry['a'] }
      end
      alias :most_visited :most_something

      def perform_visit(article, visits, is_base: false)
        find_or_create_by(article: key_field_for(article), is_base: is_base).tap do |art|
          attrs = default_attributes_for(article).merge(is_base ? {count: visits} : {})
          art.update_attributes(attrs)
          art.inc(count: visits) unless is_base
        end
        true
      end
    end
  end
end
