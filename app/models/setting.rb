class Setting < RailsSettings::CachedSettings
  validates :var, presence: true, length: {maximum: 255}
  validates :thing_type, length: {maximum: 30}

  def self.update_all(params)
    Setting.transaction do
      Setting.unscoped.each do |e|
        e.value = params[e.var.to_s]
        e.save!
      end
    end
  end
end
