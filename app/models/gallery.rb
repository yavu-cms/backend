class Gallery < ActiveRecord::Base
  validates :title, presence: true, uniqueness: true
  has_many :component_values, as: :referable, dependent: :restrict_with_error
  validates_length_of %i[title type], maximum: 255

  set_api_resource id: :id,
    title: :title,
    media: :media

  # Search for sections using a query hash, which may contain:
  #   * query  [String] string to be searched amongst the slug and name (using OR and %<query>%)
  def self.search(params = {})
    scope = where(nil)
    scope = scope.where('title like :query', query: "%#{params[:query]}%") unless params[:query].blank?
    scope
  end

  # Return all the available gallery types
  def self.types
    [MediaGallery]
  end

  def visible?
    true
  end

  def unused?
    component_values.empty?
  end
end
