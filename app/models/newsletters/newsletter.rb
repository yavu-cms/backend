class Newsletter < ActiveRecord::Base
  NEWSLETTER_VIEW = 'newsletter_view'
  TEMPLATE_ERROR  = 'error'

  belongs_to :client_application
  has_many   :static_data, ->{ order(created_at: :desc) }, class_name: 'NewsletterGeneratedData', dependent: :destroy

  after_save :program_worker

  validates :name, presence: true, uniqueness: true
  validates :frequency, presence: true
  validates :expiration, presence: true, numericality: { only_integer: true, greater_than: 0 }
  validates :client_application, presence: true
  validate  :syntax
  validates_length_of %i[name frequency subject], maximum: 255

  set_api_resource id: :name,
    name: :name,
    active_static_newsletter: ->(_){ active_static_newsletter }

  def self.static_data_id_for newsletter_name
    newsletter = find_by name: newsletter_name
    static_slug = newsletter.name.parameterize + '-' + Time.now.to_i.to_s
    # Warning: the next 2 lines are correct because a dependency problem in generation context
    new_data = newsletter.static_data.create(content: '', slug: static_slug)
    new_data.update(content: newsletter.generate_content)
    (new_data.content.present?) ? new_data.slug : nil
  end

  def active_static_newsletter
    static_data.first
  end

  def active_static_newsletter_id
    active_static_newsletter.slug
  end

  # TODO: Make this method safer
  def generate_content
    do_generation!
  end

  def copy(name = nil)
    copy = dup except: [:name]
    copy.name = name || I18n.t('newsletters.copy.prefix', default: 'Copy of %{name}', name: self.name)
    copy
  end

  private

  def program_worker
    transaction do
      existent_job = Sidekiq::Cron::Job.find cron_worker_name
      if existent_job
        existent_job.cron = calculate_cron_time
        existent_job.save
        if enabled?
          existent_job.enable!
        else
          existent_job.disable!
        end
      else
        job = Sidekiq::Cron::Job.create name: cron_worker_name,
          cron: calculate_cron_time,
          klass: 'NewsletterSenderWorker',
          args: name
        job.disable! unless enabled?
      end
    end
  end

  def syntax
    do_generation!
  rescue SyntaxError, StandardError => e
    errors.add :generator, "presenta errores: #{e.to_s}"
  end

  def cron_worker_name
    'Newsletter Cron Worker for ' + name
  end

  #TODO: Implement this using frequency attribute
  def calculate_cron_time
    frequency
  end

  #TODO: Put i18n messages instead of hardcoded spanish labels
  def do_generation!
    newsletter_context = id ? "Newsletter.find(#{id})" : "Newsletter.new"
    self_in_context = <<-END
      @my = #{newsletter_context};
      @client_application = ClientApplication.find(#{client_application.id});
    END
    super_context = "#{self_in_context}#{context}"
    c   = Component.new context: super_context, views: [View.new(name: NEWSLETTER_VIEW, value: generator)]
    cc  = ComponentConfiguration.new component: c
    col = Column.new component_configurations: [cc]
    row = Row.new columns: [col]
    r   = Representation.new client_application: client_application,
      view: View.new(value: '[[content]]'),
      error_view: View.new(value: "#{TEMPLATE_ERROR}"),
      default_row_view: View.new(value: '[[content]]'),
      default_column_view: View.new(value: '[[content]]'),
      rows: [row]
    row.representation = r
    col.row = row
    cc.column = col
    r.define_singleton_method(:component_configurations) { [cc] }

    fake_route = Route.new(representation: r)
    cp = ContextProcessor.new fake_route
    newsletter_context = cp.build_single_component_context cc
    raise(StandardError, "Hay problemas con el contexto: #{newsletter_context.errors}") unless newsletter_context.errors.empty?
    client_resource = client_application.as_api_resource
    Yavu::Frontend::Renderer::Base.configure do |config|
      config.base_url = client_resource.base_url
      config.service_prefix = "#{client_resource.service_prefix}/NOT_USED"
      config.routes = client_resource.routes
      config.assets_configuration = client_resource.assets_configuration
    end
    yr = Yavu::Frontend::Renderer::Base.new fake_route.as_api_resource, logger: Rails.logger
    refreshed_nl_context = Yavu::API::Resource::Base.from_json(newsletter_context.to_json)
    generated_newsletter_content = yr.render_component NEWSLETTER_VIEW, refreshed_nl_context
    had_errors = generated_newsletter_content == TEMPLATE_ERROR
    raise(SyntaxError, "Hay problemas con la vista") if had_errors
    # If __no_send context variable is set on or yielded view had errors, newsletter data wont be generated
    if newsletter_context.__no_send || had_errors
      ''
    else
      generated_newsletter_content
    end
  end
end
