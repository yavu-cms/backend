class NewsletterGeneratedData < ActiveRecord::Base
  belongs_to :newsletter, autosave: true, touch: true

  validates :slug, presence: true, length: {maximum: 255}
  validates :newsletter, presence: true

  set_api_resource id: :slug,
    slug: :slug,
    content: :content
end
