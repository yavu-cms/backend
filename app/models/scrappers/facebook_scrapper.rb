module Scrappers
  class FacebookScrapper
    def self.clear_cache(url)
      params = {  
        client_id: YavuSettings.facebook_account.id,
        client_secret: YavuSettings.facebook_account.secret,
        grant_type: "client_credentials"
      }
      uri = URI("https://graph.facebook.com/oauth/access_token?#{params.to_query}")
      response = Net::HTTP.get(uri)
      access_token = Rack::Utils.parse_nested_query(response)["access_token"]
      if access_token
        uri = URI('https://graph.facebook.com')
        res = Net::HTTP.post_form(uri, 'id' => "#{url}", 'scrape' => 'true',
	    'access_token' => "#{access_token}", 'max' => '500')
      else
        puts "No tenemos access token"      
      end
    end
  end
end
