# Asset
#
# Provides the includer class the ability to respond as an assets builded upon a carrierwave uploader
#
# The behavior is implemented by defining methods for interacting with carrierwave
# uploader, check uniqueness and DRY code needed by every class that manages assets like:
#   - Component
#   - ClientApplication
#
# Classes that includes this module must mount carrierwave uploader into #file
#
# This module adds the following features:
#
#   #editable? whether asset can be edited with a text editor
#   #content returns asset content
#   #update_content! sets content to asset
#
module Asset
  # On module inclusion, define AR after-callbacks for create, update and destroy operations
  def self.included(base)
    base.class_eval do
      extend ::Enumerize
      enumerize :type, in: AssetUploader.types, predicates: true
    end
  end

  def editable?
    stylesheet? || javascript?
  end

  def content(compiled=false)
    if compiled
      begin
      conf = assets_configuration
      env = sprockets_environment(conf)
      configure_sprockets_helpers(conf, env)
      env.find_asset(relative_url, bundle: false)
      rescue Exception => e
        e.message
      end
    else
      IO.read(file.current_path) rescue ''
    end
  end

  def update_content!(content)
    raise "Can't update content when asset is not editable" unless editable?
    File.open(file.current_path, 'w') {|f| f.write(content) }
    self.update({})
  end

  def to_s
    relative_url
  end


# Returns asset relative path from uploader store_dir
  def relative_url(include_namespace = true)
    url = Pathname(file_url).relative_path_from(Pathname(File.join('',file.store_dir_prefix,''))).to_s
    url = remove_namespace(url) unless include_namespace
    url
  end

  def remove_namespace(url)
    url.split('/').last
  end

  # Returns relative asset path inside asset_type directory.
  # For example, the returned value for a component asset named javascripts/image_galery/main.js
  # will be image_gallery/main.js
  # If a client applications asset named javascripts/somejs.js
  # will be somejs.js
  # This method is used by sprockets
  def relative_url_without_extension(include_namespace = true)
    path = relative_url(include_namespace).split '/'
    file = File.basename path.pop, '.*'
    if path.count > 0
      File.join(path,file)
    else
      file
    end
  end

  def rename(new_name)
    new_file = "#{new_name}.#{self.file.file.extension}"
    File.rename(self.file.file.file, "#{absolute_url}/#{new_file}")
    self.file = File.open("#{absolute_url}/#{new_file}")
    save
  end

  def sprockets_environment(assets_configuration)
    Yavu::Frontend::Util::AssetConfigurationFactory.sprockets_environment assets_configuration
  end

  def configure_sprockets_helpers(assets_configuration, environment)
    Yavu::Frontend::Util::AssetConfigurationFactory.configure_sprockets_helpers(assets_configuration, environment)
  end

  def assets_configuration
    ClientApplication.assets_configuration prefix: ':client_identifier'
  end
end
