module FindableInOrder
  extend ActiveSupport::Concern

  module ClassMethods
    def find_ordered(ids)
      find_ordered_where(id: ids)
    end

    # condition must be a Hash
    def find_ordered_where(condition)
      objects = Hash[where(condition).map { |o| [o.send(condition.keys[0]).to_s, o] }]
      condition.values[0].map { |v| objects.delete(v.to_s) }.reject &:nil?
    end
  end
end