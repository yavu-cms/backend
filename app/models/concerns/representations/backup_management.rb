module Representations::BackupManagement
  extend ActiveSupport::Concern

  # Return whether this representation has backups
  def has_backups?
    backups.any?
  end

  # Destructively restore the given backup onto this representation.
  # Please note that this representation will be destroyed after this operation.
  # @see Representation#replace_with
  def restore_backup!(backup)
    transaction do
      self.replace_with backup, destroy: true
      client_application.notify_clients
    end
  end
end