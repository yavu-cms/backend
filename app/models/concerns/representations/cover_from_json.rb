module Representations::CoverFromJson
  extend ActiveSupport::Concern

  def cover_from_json(json)
    configuration = parse_and_validate_cover(json)
    return false if errors.present?
    transaction do
      process_cover_config(configuration)
      save!
    end
    true
  rescue ActiveRecord::RecordInvalid
    false
  end

  private

  def parse_and_validate_cover(json_string)
    JSON.parse(json_string)
  rescue JSON::ParserError, RuntimeError
    errors.add(:base, :invalid)
  end

  def process_cover_config(whole_config)
    cover_component_configurations.each do |ccc|
      config = (whole_config.delete(ccc.id.to_s) || {}).reverse_merge 'modules' => {}, 'new' => {}
      process_existing_cover_articles ccc, config.delete('modules')
      process_new_cover_articles ccc, config.delete('new')
    end
  end

  def process_existing_cover_articles(cover_component_configuration, cover_articles)
    cover_component_configuration.cover_articles_component_configurations.each do |cacc|
      config = cover_articles.delete(cacc.id.to_s)
      if config.nil?
        cacc.destroy! # The module is not present -- it's been removed
      else
        cover_attrs = cover_articles_component_configuration_attrs(config)
        cacc = fix_cover_articles_component_configuration_if_component_type_changed(cacc, cover_attrs)
        cacc.update! cover_attrs
      end
    end
  end

  def fix_cover_articles_component_configuration_if_component_type_changed(cover_articles_component_configuration, attributes)
    if attributes[:component] != cover_articles_component_configuration.component
      cover_articles_component_configuration.update(component: attributes[:component], description: attributes[:description])
    end
    cover_articles_component_configuration
  end

  def process_new_cover_articles(cover_component_configuration, new_cover_articles)
    new_cover_articles.each do |i, config|
      cacc = cover_component_configuration.cover_articles_component_configurations.create!(
        new_cover_articles_component_configuration_attrs(config)
      )
      cacc.ordered_articles = config['articles'] || []
    end
  end

  def cover_articles_component_configuration_attrs(configuration)
    new_cover_articles_component_configuration_attrs(configuration).
      merge({ ordered_articles: configuration['articles'] || [] })
  end

  def new_cover_articles_component_configuration_attrs(configuration)
    component = CoverArticlesComponent.find(configuration['type_id'])
    { order: configuration['order'].to_i,
      description: configuration['description'].chomp,
      component: component,
      view: component.views.find(configuration['view_id']) }
  end
end
