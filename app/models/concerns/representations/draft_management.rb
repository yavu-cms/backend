module Representations::DraftManagement
  extend ActiveSupport::Concern

  # Return whether this representation has a draft
  def has_draft?
    draft.present?
  end

  # Create a draft for this representation (copying its related elements)
  # @see BaseRepresentation#copy
  def generate_draft
    return draft if has_draft?
    draft_name = I18n.t 'activerecord.attributes.draft.draft_name', representation: name
    transaction do
      copy_and_save_as(RepresentationDraft, draft_name)
    end
  end

  # Destroy the draft associated to this representation - if any.
  def discard_draft
    if has_draft?
      draft.destroy.tap do
        trigger_draft_creation
      end
    end
  end

  # Destructively apply this representation's draft, replacing this representation
  # with the draft. If no draft is available, this method won't do a thing.
  # This representation object will be turned into a RepresentationBackup so that
  # any changes can be undone.
  # @see Representation#replace_with
  def apply_draft!
    transaction do
      self.replace_with draft, become: 'RepresentationBackup'
      User.change_representation_reference! draft.id, self.id
      client_application.notify_clients
    end if has_draft?
  end

  def trigger_draft_creation
    DraftsCreationWorker.perform_async(id) unless being_copied || has_draft?
  end
end
