module Representations::FromJson
  extend ActiveSupport::Concern

  def from_json(json)
    configuration = parse_and_validate(json)
    return false if errors.present?
    transaction do
      process_config configuration
      save!
    end
    true
  rescue ActiveRecord::RecordInvalid
    false
  end

  private

  def parse_and_validate(json_string)
    structure = JSON.parse(json_string).delete('representation')
    raise unless structure.present?
    structure
  rescue JSON::ParserError, RuntimeError
    errors.add(:base, :invalid)
  end

  def process_config(config)
    views_from_config config.delete('views')
    configuration_from_config config.delete('configuration')
    self.rows = rows_from_config(config.delete('rows') || [])
  end

  def views_from_config(hash)
    view.value                = hash['layout'] unless hash['layout'].blank?
    default_row_view.value    = hash['default_row_view'] unless hash['default_row_view'].blank?
    default_column_view.value = hash['default_column_view'] unless hash['default_column_view'].blank?
  end

  def configuration_from_config(hash)
    self.configuration = hash.symbolize_keys
  end

  def rows_from_config(hash)
    hash.map.with_index do |row_hash, order|
      row_from_config(row_hash, order).tap do |row|
        row.columns = columns_from_config(row_hash.delete('columns') || [], row)
      end
    end
  end

  def row_from_config(hash, order)
    row = rows.find(hash.delete('_id')) rescue rows.build
    row.representation = self
    row.order = order
    row.configuration = hash.delete('configuration')
    view = hash.delete('view').delete('row') rescue nil
    row.build_view(name: 'Row view') if view.present? && !row.view.present?
    row.update_view view
    row.save!
    row
  end

  def columns_from_config(hash, parent_row)
    hash.map.with_index do |column_hash, order|
      column_from_config(column_hash, parent_row, order).tap do |column|
        column.component_configurations =
          component_configurations_from_config(column_hash.delete('component_configurations') || [], column)
      end
    end
  end

  def column_from_config(hash, parent_row, order)
    column = parent_row.columns.find(hash.delete('_id')) rescue parent_row.columns.build
    column.row = parent_row
    column.order = order
    view = hash.delete('view').delete('column') rescue nil
    column.build_view(name: 'Column view') if view.present? && !column.view.present?
    column.update_view view
    column.save!
    column
  end

  def component_configurations_from_config(hash, parent_column)
    hash.map.with_index do |component_hash, order|
      component_configuration_from_config(component_hash, parent_column, order)
    end
  end

  def component_configuration_from_config(hash, parent_column, order)
    configuration = parent_column.component_configurations.find(hash.delete('_id')) rescue nil
    if configuration
      component = configuration.component
    else
      component = BaseComponent.find hash.delete('component_id')
      configuration = component.new_configuration
    end
    configuration.column = parent_column
    configuration.order = order
    configuration.description = hash.delete('description')
    configuration.view = component.views.find_by name: hash.delete('component_configuration').delete('view')
    configuration.values = hash
    configuration.save!
    configuration
  end
end