module Representations::Rendering
  extend ActiveSupport::Concern

  included do
    attr_writer :evaluator
    alias_attribute :main_template_key, :name
  end

  def formats(force_html = true)
    formats = views.pluck :format
    formats << 'text/html' if force_html
    formats
  end

  # Return the translations that are available for this representation
  def translations
    component_configurations.inject({}) do |result, component_configuration|
      component_slug = component_configuration.component.slug
      result.deep_merge!({component_slug => component_configuration.translations})
    end
  end

  def views_by_format(force_html = true)
    Hash[formats(force_html).map { |f| [f, renderer(f).render(self)] }]
  end

  # Return the templates for this representation, which are:
  #   * This representation's layout
  #   * This representation's error view
  #   * The views of all the component configurations associated to this representation
  def templates
    Hash[
      main: layout,
      error: error_view.value,
      partials: component_configurations.map { |x| x.templates }.inject({}) { |ret, hash_element| ret.merge hash_element }
    ]
  end

  protected

  # Return the template for this representation's layout, which includes the rows
  # associated to this representation, which in turn includes its associated
  # columns, and so on.
  # This method will use this representation's +view+ to render the template.
  def layout
    renderer(:html).render(self)
  end

  # Return a Renderer instance built using the GenericRenderer.build factory.
  # @see GenericRenderer#build
  def renderer(format)
    GenericRenderer.build(format)
  end
end
