module Versionable
  extend ActiveSupport::Concern
  DEFAULT_VERSIONING_OPTIONS = {skip: [], only: [], ignore: [:updated_at], meta: {}}

  included do
    has_paper_trail DEFAULT_VERSIONING_OPTIONS unless paper_trail_enabled_for_model?
  end

  def restore
    self.update_attributes(created_at: DateTime.now, paper_trail_event: 'restore')
    self
  end
end
