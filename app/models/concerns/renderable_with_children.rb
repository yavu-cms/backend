module RenderableWithChildren
  extend ActiveSupport::Concern

  # Define
  # - children
  # - children=
  # - parent
  # - parent=
  # - default_view

  included do
    belongs_to :view, autosave: true

    validate :view_has_content_placeholder
  end

  def copy(parent)
    transaction do
      self.class.create(order: self.order, configuration: configuration).tap do |my_copy|
        my_copy.children = self.children.map { |child| child.copy(my_copy) }
        my_copy.parent = parent
        my_copy.save!
      end
    end
  end

  def renderable_view
    if (selected_view = get_view).blank_value?
      default_view
    else
      selected_view
    end
  end

  def get_view
    view || View.new
  end

  def update_view(value)
    if value.blank?
      get_view.destroy; nil
    else
      get_view.tap {|v| v.value = value}
    end
  end

  private

  def view_has_content_placeholder
    if view
      unless view.has_content_placeholder?
        errors.add(:view, I18n.t('errors.messages.has_not_content'))
        false
      end
    end
  end
end
