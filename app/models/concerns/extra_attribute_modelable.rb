module ExtraAttributeModelable
  def custom_build_extra_attribute_model
    build_extra_attribute_model.tap do |attr|
      attr.values = Hash[ExtraAttribute.pluck(:name).map {|x| [x,nil]}]
    end
  end
  def extra_attribute_model_with_lazy_evaluation
    extra_attribute_model_without_lazy_evaluation || custom_build_extra_attribute_model
  end

  
  def self.included(base)
    base.class_eval do
      has_one :extra_attribute_model, as: :model, dependent: :destroy
      alias_method_chain :extra_attribute_model, :lazy_evaluation
    end
  end
end
