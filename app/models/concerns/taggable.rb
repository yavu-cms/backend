module Taggable
  extend ActiveSupport::Concern

  included do
    has_and_belongs_to_many :tags
  end

  # Return a comma-delimited string with the names of the tags associated to
  # this model.
  def serialized_tags
    tags.pluck(:name).join(',')
  end

  # Set the associated tags for this model to be the ones in the provided
  # comma-delimited string, creating any one of them which doesn't exist.
  # If any of the tags is a combined one (isn't a root tag), its root will be
  # used instead.
  def serialized_tags=(tags)
    self.tags = tags.split(',').collect do |name|
      tag = Tag.find_or_create_by(name: name)
      # If the tag has a combiner, use that instead
      tag = tag.combiner unless tag.combiner.nil?
      tag
    end.uniq
  end
end