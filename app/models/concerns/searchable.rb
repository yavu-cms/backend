module Searchable
  extend ActiveSupport::Concern

  included do
    include Elasticsearch::Model

    index_name YavuSettings.elasticsearch.index_name
    # Set default settings if provided by YavuSettings
    settings YavuSettings.elasticsearch.default_settings.to_hash if YavuSettings.elasticsearch.default_settings.present?

    after_commit :index_document_callback, on: :create
    after_commit :update_document_callback, on: :update
    after_commit :delete_document_callback, on: :destroy
    after_touch  :update_document_callback

    # Exposes Elasticsearch's .search method without clashing into existing .search methods in the including model
    def self.es_search(query)
      __elasticsearch__.search query
    end
  end

  # This method should be overridden in subclasses.
  # All instances are indexable by default.
  def indexable?
    true
  end

  protected

  def index_document_callback
    ElasticsearchIndexer.perform_async(:index, self.class.name, self.id) if indexable?
  end

  def update_document_callback
    operation = indexable?? :update : :delete
    ElasticsearchIndexer.perform_async(operation, self.class.name, self.id)
  end

  def delete_document_callback
    ElasticsearchIndexer.perform_async(:delete, self.class.name, self.id)
  end
end