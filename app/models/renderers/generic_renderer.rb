class GenericRenderer
  attr_reader :format

  GLUE = "\n"

  def self.build(format)
    class_for_format(format).new(format)
  end

  def self.class_for_format(format)
    html?(format) ? HtmlRenderer : self
  end

  def self.html?(format)
    format == :html || format == 'text/html'
  end

  def initialize(format)
    @format = format
  end

  def render(representation)
    view = representation.views.find_by(format: format)
    if view.present?
      render_view view, main_view_locals(representation)
    else
      # Default to the HTML renderer
      self.class.build(:html).render(representation)
    end
  end

  protected

  def render_view(view, locals = {})
    view.render locals
  end

  def render_collection(collection)
    collection.map { |item| render_item(item) }.reject(&:blank?).join(GLUE).presence || default_empty_string
  end

  def render_item(item)
    if item.respond_to? :children
      render_container_item(item)
    else
      render_leaf item
    end
  end

  def render_container_item(item)
    render_collection item.children
  end

  def render_leaf(item)
    if item.respond_to? :articles_cover_articles_component_configurations
      render_cover_articles_component(item)
    elsif item.respond_to? :cover_articles_component_configurations
      render_cover_component(item)
    else
      render_component(item)
    end
  end

  def render_cover_component(item)
    render_collection item.cover_articles_component_configurations
  end

  def render_cover_articles_component(item)
    if (view = item.renderable_view(format)).present?
      <<-EOR % {id: item.id, template: view.template_name, cover_component_configuration_id: item.cover_component_configuration_id}
<%%= render_component '%{template}', _context.components[%{cover_component_configuration_id}].components[%{id}] %%>
      EOR
    end
  end

  def render_component(item)
    if (view = item.renderable_view(format)).present?
      <<-EOR % {id: item.id, template: view.template_name}
<%%= render_component '%{template}', _context.components[%{id}] %%>
      EOR
    end
  end

  def default_empty_string
    ''
  end

  def main_view_locals(representation)
    { content: render_collection(representation.rows) }
  end
end
