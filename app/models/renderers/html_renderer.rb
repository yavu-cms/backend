class HtmlRenderer < GenericRenderer
  def format
    'text/html'
  end

  def render(representation)
    render_view representation.view, main_view_locals(representation)
  end

  protected

  def render_container_item(item)
    if item.children.any?
      item.renderable_view.render content: render_collection(item.children)
    end
  end

  def render_cover_articles_component(item)
    if (view = item.renderable_view(format)).present?
      <<-EOR % {id: item.id, template: view.template_name, cover_component_configuration_id: item.cover_component_configuration_id}
<%% if _context.components[%{cover_component_configuration_id}].components[%{id}].try(:cover_articles).try :any? %%>
  <%%= render_component '%{template}', _context.components[%{cover_component_configuration_id}].components[%{id}] %%>
<%% end %%>
      EOR
    end
  end

  def main_view_locals(representation)
    super.merge favicon: render_favicon(representation).to_s,
                javascripts: render_assets(representation, :javascript).to_s,
                stylesheets: render_assets(representation, :stylesheet).to_s,
                seo: render_seo.to_s,
                image_link: render_image_link.to_s,
                additional_headers: render_additional_headers(representation).to_s,
                extra_javascripts: render_extra_javascripts(representation)
  end

  # wraps the seo rendering. By now, due the dynamic
  # nature of this, we show a frontend rendering method
  def render_seo
    %Q(<%= render_seo %>)
  end

  def render_image_link
    %Q(<%= render_image_link %>)
  end

  def render_additional_headers(representation)
    representation.client_application.extra_attribute_model.additional_headers.html_safe rescue ''
  end

  def default_empty_string
    '&nbsp;'
  end

  # Render ERB tags for every available asset of the given +type+.
  # Supported types are +:javascript+ and +:stylesheet+.
  # If an invalid type is provided, nil will be returned.
  def render_assets(representation, type)
    '<%%= %{type}_tag "%{asset}" %%>' % {type: type, asset: send(type.to_s.pluralize, representation) }
  rescue NoMethodError => e
    ''
  end

  # Return the main javascript file for the representation
  def javascripts(representation)
    File.basename representation.send(:main_javascript_file)
  end

  # Return the main stylesheet file for the representation
  def stylesheets(representation)
    File.basename representation.send(:main_stylesheet_file)
  end

  # Render the HTML for this representation's favicon, if set in +configuration[:favicon]+
  def render_favicon(representation)
    if (favicon = representation.favicon).present?
      type = favicon.relative_url =~ /\.ico$/i ? 'image/x-icon' : favicon.content_type
      %Q(<link rel="icon" type="%{type}" href="<%%= image_path "%{file}" %%>"/>) % {type: type, file: favicon.relative_url}
    end
  end

  def render_extra_javascripts(representation)
    <<-HTML
      <script></script>
    HTML
  end
end
