class ArticleRelations < ActiveRecord::Base
  belongs_to :article_a, class_name: 'Article'
  belongs_to :article_b, class_name: 'Article'
end
