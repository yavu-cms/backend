# TODO: Implement Tagging superclass and do a pull-up refactor
# if several tagging strategies are designed in future.

class SimpleTagging

  attr_accessor :word_threshold, :ocurrences_limit, :stopwords, :tags_limit

  def initialize(word_threshold: 3, ocurrences_limit: 3, tags_limit: 10, names_limit: 1)
    @word_threshold   = word_threshold
    @ocurrences_limit = ocurrences_limit
    @stopwords = merge_stopwords
    @tags_limit = tags_limit
  end

  # Se crean expresiones y filtros para:
  #  - remover las etiquetas HTML
  #  - remover los signos de puntuacion
  #  - remover palabras mayusculas al principio de oraciones
  #  - identificar tags compuestos
  #  - identificar nombres propios
  #  - filtrar palabras mayores que un valor, que no esten en una lista negra y que no sean numeros

  def tags_from(text)

    name_regex = "[A-Z][a-záéíóúüñ]+"
    punctuations_regex = '\p{Punct}'
    number = lambda { |str| str.to_i.to_s == str }
    words_filter = lambda { |w| w.size <= word_threshold || stopwords.include?(w.downcase) || number.(w) }

    # Descartando todo el ruido
    plain_text = strip_html(text)

    # Se sacan todas las palabras que empiezan con mayuscula al principio de una frase
    first_words = /^\s*#{name_regex}|[\.!\?]\s*#{name_regex}/
    plain_text.gsub!(first_words, '')

    # se guardan nombres propios. usamos parameterize para no considerar caracteres especiales
    name_tags = plain_text.scan(/#{name_regex}/).reject { |name| stopwords.include? name.parameterize }

    # Se almacenan en una primera instancia los tags candidatos
    simple_tags_candidates = plain_text.split(/\s+|#{punctuations_regex}/).reject(&words_filter)

    # Para los tags compuestos: la expresion regular captura todos los grupos de palabras que inician con letra mayúscula.
    compounded_tags_candidates = compounded_tags_from(plain_text)

    # Luego, se rechazan las palabras simples y aquellas que no cumplan con las condiciones predefinidas anteriormente.
    compounded_tags_candidates = compounded_tags_candidates.reject { |word| word.split.size == 1}.reject(&words_filter)

    name_tags.reject! { |tag| compounded_tags_candidates.join(' ').include? tag }

    # Se construye un mapa de palabras -> #ocurrencias ...
    words_map = map_ocurrences(simple_tags_candidates)
    tags = words_map.select { |_, ocurrences| ocurrences >= ocurrences_limit }

    # Se retorna todos los tags dandole preferencia a los compuestos, despues los nombres propios y despues las palabras repetidas
    (compounded_tags_candidates +
     name_tags +
     tags.sort_by { |_, value| value }.map { |pair| pair.first }).uniq.first(tags_limit)
  end

  private

  def merge_stopwords
    (YavuSettings.tagging.stopwords + Tag.hidden.map(&:name)).uniq
  end

  def strip_html(text)
    HTMLEntities.new.decode(text).gsub(/(<[^>]*>|\n|\r)/, '.')
  end

  def compounded_tags_from(text)
    text
      .gsub(/(\b-|-\b)/) { |s| s.delete('-') } # Excluye '-' que esten en los extremos de una palabra
      .scan(/((([A-Z][^\s\p{Punct}\d]+)[\s]*)+)/).map { |group| group.first.rstrip }
  end

  def map_ocurrences(tags)
    ocurrences_map = lambda { |array| array.reduce({}) { |h, key| h.merge({key => tags.count(key)}) } }
    # ... y se recuperan aquellas que esten dentro de un umbral
    ocurrences_map.call(tags)
  end
end
