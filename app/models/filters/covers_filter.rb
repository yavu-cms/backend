class CoversFilter < BaseModelFilter
  def model
    Representation
  end

  def scope
    :all_covers
  end

  def self.defaults
    { name: nil, client_application_id: nil }
  end

  def prematches
    if user
      Representation.all_covers.where(id: user.accessible_representations)
    else
      Representation.none
    end
  end

  def simple_filter_key
    :name
  end

  def filter_by_client_application(partial, value)
    partial.joins(:client_application).where(client_application: { id: value })
  end
end