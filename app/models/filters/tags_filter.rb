class TagsFilter < BaseModelFilter
  def model
    Tag
  end

  def scope
    :root
  end

  def self.defaults
    { name: nil }
  end

  def simple_filter_key
    :name
  end
end
