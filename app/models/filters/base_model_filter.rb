# Base class for ActiveRecord-bound filter classes.
# Subclasses must implement the #model method and optionally add
# custom filtering methods, as explained below in the #apply_to method.
class BaseModelFilter < BaseFilter
  # Get the model class to which the filters will be applied.
  # This method must be overridden in subclasses.
  def model
    raise NotImplementedError
  end

  # This is a hook for providing some
  # initial collection of items determinated
  # by a custom criteria
  def prematches
    nil
  end

  # Get all the matching records to the current filter criteria.
  # Allow a custom scope as symbol if we need a more specific filtering
  def all(custom_scope = nil)
    matches = prematches || model.send(custom_scope || scope)
    matches = apply_to matches
  end

  # Get the scope that will start the filter criteria as a symbol.
  # The default scope is :all.
  def scope
    :all
  end

  # Applies the filter criteria specified for the different fields to :matches,
  # an ActiveRecordRelation which should start by matching all the "filterable"
  # records.
  #
  # Each field will be processed in the following way:
  # 1. Check if self responds to a custom filter method of the form: "filter_by_<FIELD_NAME>". If it does,
  #    delegate the filtering of records by that field to the method. Such a method must return the updated
  #    ActiveRecordRelation.
  #    For instance, if the field name is "slug", its custom filter method will be named "filter_by_slug":
  #
  #     def filter_by_slug(partial, value)
  #       partial.where(slug: value)
  #     end
  #
  # 2. If no custom filter method is available for the field, check for a default filter method by the class
  #    of the value. Supported classes out-of-the-box are:
  #
  #    - String:    Will search with a 'like "%value%"'.
  #
  # 3. Otherwise, leave the logic up to ActiveRecord. Will add a where constraint like follows:
  #
  #     partial.where(field.to_sym => value)
  #
  def apply_to(matches)
    values.inject(matches) do |partial, pair|
      field, value = pair
      unless value.blank?
        type = value.class.name.downcase
        custom_filter_method = "filter_by_#{field}"
        default_filter_method = "filter_#{type}_field"
        if self.respond_to? custom_filter_method
          # Custom filtering
          partial = self.send custom_filter_method, partial, value
        elsif self.respond_to? default_filter_method
          # Default method filtering
          partial = self.send default_filter_method, partial, field, value
        else
          # Leave it up to AR and its magic
          partial = partial.where(field.to_sym => value)
        end
      end

      partial
    end
  end

  # Filters String fields using LIKE.
  def filter_string_field(partial, field, value)
    partial.where("#{field} like ?", "%#{value}%")
  end
end
