class ArticlesFilter < BaseModelFilter

  # Default values for this filter.
  def self.defaults
    { body: nil, containing: nil, edition_date_from: nil, edition_date_to: nil, created_time_from: nil, created_time_to: nil, last_updated_from: nil, last_updated_to: nil, section_id: nil, edition_id: nil, tag: nil }
  end

  def simple_filter_key
    :containing
  end

  # Model class to be filtered.
  def model
    Article
  end

  def prematches
    if user
      Article.where(section_id: user.accessible_sections)
    else
      Article.none
    end
  end

  def encode(value = '')
    HTMLEntities.new.encode(value, :named)
  end

  # Custom filter method for :body field.
  def filter_by_body(partial, value)
    value = "#{value}"
    encoded = "#{encode value}"

    partial.where('body like ? OR body like ?', "%#{value}%", "%#{encoded}%")
  end

  # Custom filter method for article content.
  # Not include the body field for performance
  def filter_by_containing(partial, value)
    value = "#{value}"
    partial.where('heading like :heading OR title like :title OR lead like :lead OR signature like :signature', heading: "%#{value}%", title: "%#{value}%", lead: "%#{value}%", signature: "%#{value}%")
  end

  # Custom filter method for this article's tags :name fields.
  def filter_by_tag(partial, value)
    partial.joins(:tags).where(tags: { name: value })
  end

  # Custom filter method to filter sections by id.
  def filter_by_section_id(partial, value)
    partial.joins(:section).where(sections: { id: value, is_visible: true })
  end

  # Custom filter method to filter editions by id.
  def filter_by_edition_id(partial, value)
    partial.joins(:edition).where(editions: { id: value, is_visible: true })
  end

  # Custom filter method for this article's edition :date range.
  def filter_by_edition_date_from(partial, value)
    partial.joins(:edition).where('editions.date >= ?', value.to_date)
  end

  # Custom filter method for this article's edition :date range.
  def filter_by_edition_date_to(partial, value)
    partial.joins(:edition).where('editions.date <= ?', value.to_date)
  end

  # Custom filter method for created :time range.
  def filter_by_created_time_from(partial, value)
    partial.where('articles.time >= ?', value.to_time)
  end

  # Custom filter method for created :time range.
  def filter_by_created_time_to(partial, value)
    partial.where('articles.time <= ?', value.to_time)
  end

  # Custom filter method for last :updated_at range.
  def filter_by_last_updated_from(partial, value)
    partial.where('articles.updated_at >= ?', value.to_date)
  end

  # Custom filter method for last :updated_at range.
  def filter_by_last_updated_to(partial, value)
    partial.where('articles.updated_at <= ?', value.to_date)
  end

  def sort_fields
    model.column_names
  end
end
