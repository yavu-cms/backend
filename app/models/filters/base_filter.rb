# Base class for any object used for keeping filtering logic + criteria together.
#
# This class provides the following functionality out-of-the-box:
#
#   - Automatic values sanitizing: The values passed to the #initialize method
#     will be fed into #sanitize and the result of that process will be used for
#     setting the filter values.
#   - Hash-like accessors: Any value for the filters can be get/set via []/[]=,
#     just like Hashes.
#
# For examples, please:
# @see ArticlesFilter
# @see SupplementFilter
class BaseFilter

  # Default values. This method should be overridden
  # when in need to specify particular defaults for a
  # subclass.
  def self.defaults
    {}
  end

  # Returns the user passed (if any)
  def user
    @options[:user]
  end

  # Global key that will contain the values for any filter class
  # inside the store or the request values source.
  # In extension, the store options should be structured
  # like this:
  #     {
  #       :"#{BaseFilter.global_key}" => {
  #         :"#{@filter.key}" => {
  #           :value => 'for a filter field'
  #         }
  #       }
  #     }
  def self.global_key
    :filter
  end

  # Particular key that will contain the values for this specific
  # filter class in the store/request values source - under the
  # BaseFilter#global_key main key.
  # In extension, the store options should be structured
  # like this:
  #     {
  #       :"#{BaseFilter.global_key}" => {
  #         :"#{@filter.key}" => {
  #           :value => 'for a filter field'
  #         }
  #       }
  #     }
  def self.key
    self.name.to_sym
  end

  # Return symbol of the chosen simple filter key.
  # This method must be implemented in subclasses.
  def simple_filter_key
    raise NotImplementedError
  end

  # Return true if the filter use simple filter key.
  def simple_filter?
    !self.values[simple_filter_key].nil? unless simple_filter_key.nil?
  end

  # Initialize a new filter with the given options.
  def initialize(options = {})
    @options = { user: nil, values: nil, store: nil, persist: false }.merge options

    # This is the order of precedence:
    # values => store => defaults
    actual = options[:values]
    store  = options[:store]

    source = self.class.defaults
    source = store[self.class.global_key][key] if store && store[self.class.global_key] && store[self.class.global_key][key]
    source = actual if actual

    self.values = sanitize source

    self
  end

  # Conditionally clean up some values after the initialization is finished
  # Note:
  #   These method have sense when the subclasses uses it,
  #   BaseFilter class will not allows to pass any value
  #   due that the defaults is an empty hash so the valid keys is an empty array
  def sanitize(values)
    valid_keys = self.class.defaults.keys
    values.inject(self.class.defaults) do |carry, (k, v)|
      carry[k] = v if valid_keys.include?(k) && !v.blank?
      carry
    end
  end

  # Instance method used for convenience. Just a wrapper for the
  # class method #key.
  def key
    self.class.key
  end

  # Filter field value accessor. Provides a Hash-like API for
  # getting the values set to the different fields of this filter.
  # Will return the value set for :key or nil.
  def [] key
    values[key] if values.has_key? key
  end

  # Filter field value writer. Provides a Hash-like API for
  # setting the values of the filter fields.
  def []=(key, value)
    values[key] = value
  end

  # Answer whether any filter values have been applied. Will return
  # true if the current values differ from the default ones.
  def any?
    self.class.defaults != self.values
  end

  # Gets the values for this filter.
  def values
    @values
  end

  # Sets the values for this filter to :new_values.
  # Will persist if the :persist option was set to true when creating
  # this object and a valid store has been provided.
  def values=(new_values)
    @values = new_values

    # Define getters for each of the new_values
    new_values.each do |key, value|
      self.class.send(:define_method, key, proc { self[key] }) unless self.respond_to? key
    end

    self.persist! @options[:store] if @options[:store] && @options[:persist]
  end

  # Persist the current values to the given :store.
  def persist!(store)
    store[self.class.global_key] ||= {}
    store[self.class.global_key][self.class.key] = self.values
  end

  # Clear the values of the filters - set them to the defaults.
  def clear!
    self.values = self.class.defaults
  end

  # Get not nil values for this filter with correct format to render
  def values_to_print(scope)
    # Select not nil values
    values = @values.reject {|key, value| value.blank? }
    # Change values of hash with the correct format
    Hash[values.map do |key, value|
      [key, render_value(key, value, scope)]
    end]
  end

  # Get all the matching records to the current filter criteria.
  # This method must be implemented in subclasses.
  def all
    raise NotImplementedError
  end

  protected

  def render_value(key, value, scope)
    custom_value_method = "value_for_#{key}"
    value = case
      when self.respond_to?(custom_value_method)
        send(custom_value_method, value)
      when is_foreign_key?(key.to_s)
        key = key.to_s.gsub(/_id\z/, '')
        value_of_foreign_key(key, value)
      else
        value_of_default(value)
      end
    value = I18n.t(key, scope: [ scope, :filter]) + ": " + value.to_s
  end

  def is_foreign_key?(key)
    key =~ /_id\z/ && key.gsub(/_id\z/, '').classify.safe_constantize.present?
  end

  def value_of_foreign_key(key, value)
    model = key.to_s.gsub(/_id\z/, '').classify.safe_constantize.find_by(id: value)
    model || '-'
  end

  def value_of_default(value)
    value.to_s
  end

  def value_of_boolean(value)
     I18n.t('filter.labels.boolean.' + value)
  end
end
