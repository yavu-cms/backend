class ComponentsFilter < BaseModelFilter

  def model
    BaseComponent
  end

  def self.defaults
    { name: nil }
  end

  def simple_filter_key
    :name
  end

  def all
    matches = model.send(scope)
    matches = apply_to matches
  end
end
