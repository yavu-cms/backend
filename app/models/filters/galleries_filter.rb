class GalleriesFilter < BaseModelFilter
  def model
    Gallery
  end

  def self.defaults
    { title: nil, type: nil }
  end

  def simple_filter_key
    :title
  end

  def value_for_type(value)
    I18n.t 'galleries.types.' + value
  end
end