class SectionsFilter < BaseModelFilter
  def model
    Section
  end

  def self.defaults
    { name: nil, supplement_id: nil }
  end

  def simple_filter_key
    :name
  end
end
