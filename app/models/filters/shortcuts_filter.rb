class ShortcutsFilter < BaseModelFilter
  def model
    Shortcut
  end

  def scope
    :sorted
  end

  def self.defaults
    { title: nil }
  end
end