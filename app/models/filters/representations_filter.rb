class RepresentationsFilter < BaseModelFilter
  attr_accessor :client_application

  def model
    Representation
  end

  def self.defaults
    { name: nil }
  end

  def simple_filter_key
    :name
  end

  def all
    matches = model.send(scope)
    matches = matches.where(client_application: client_application) unless client_application.nil?
    matches = apply_to matches
  end
end
