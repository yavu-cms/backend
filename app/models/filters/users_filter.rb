class UsersFilter < BaseModelFilter

  def self.defaults
    { user: nil }
  end

  def simple_filter_key
    :user
  end

  def model
    User
  end

  # Custom filter method for user.
  def filter_by_user(partial, value)
    partial.where("username like ? OR email like ?", "%#{value}%", "%#{value}%")
  end
end