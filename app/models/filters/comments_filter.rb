class CommentsFilter < BaseModelFilter
  def model
    Comment
  end

  def self.defaults
    { article_id: nil, time_from: nil, time_to: nil, reader: nil, body: nil }
  end

  def time_from
    Time.parse(values[:time_from]) unless values[:time_from].blank?
  end

  def time_to
    Time.parse(values[:time_to]) unless values[:time_to].blank?
  end

  # Custom filter method for :time field range.
  def filter_by_time_from(partial, value)
    partial.where('time >= ?', Time.parse(value))
  end

  # Custom filter method for :time field range.
  def filter_by_time_to(partial, value)
    partial.where('time <= ?', Time.parse(value))
  end

  # Custom filter method for :article_id field.
  def filter_by_article_id(partial, value)
    partial.where(article_id: value.split(','))
  end
end
