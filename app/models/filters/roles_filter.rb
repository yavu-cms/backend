class RolesFilter < BaseModelFilter
  def model
    Role
  end

  def self.defaults
    { name: nil }
  end

  def simple_filter_key
    :name
  end
end
