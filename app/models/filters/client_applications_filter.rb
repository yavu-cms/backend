class ClientApplicationsFilter < BaseModelFilter
  def model
    ClientApplication
  end

  def self.defaults
    { name: nil, url: nil, enabled: nil, query: nil }
  end

  def simple_filter_key
    :query
  end
  
  # Custom filter method for :name and :url
  def filter_by_query(partial, value)
    partial.where('name like :value OR url like :value', value: "%#{value}%")
  end

  # Custom filter method for :enabled field.
  def filter_by_enabled(partial, value)
    case value
      when 'enabled'  then partial.where enabled: true
      when 'disabled' then partial.where enabled: false
      else partial
    end
  end

  def value_for_enabled(value)
    value_of_boolean(value)
  end
end
