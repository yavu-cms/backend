class SupplementsFilter < BaseModelFilter
  def model
    Supplement
  end

  def self.defaults
    { name: nil }
  end

  def simple_filter_key
    :name
  end
end