class VersionsFilter < BaseModelFilter
  def model
    Version
  end

  def scope
    :printable
  end

  def self.defaults
    { item_id: nil, item_type: nil, event: nil, whodunnit: nil}
  end

  def filter_by_whodunnit(partial, value)
    partial.where(whodunnit: User.find_by(username: value).try(:id))
  end
end

