class MediaFilter < BaseModelFilter

  def model
    Medium
  end

  def self.defaults
    { query: nil, article_id: nil, type: nil, date: nil, date_from: nil, date_to: nil }
  end

  def simple_filter_key
    :query
  end

  # Custom filter method for :date field range.
  def filter_by_date_from(partial, value)
    partial.where('media.date >= ?', value.to_date)
  end

  # Custom filter method for :date field range.
  def filter_by_date_to(partial, value)
    partial.where('media.date <= ?' , value.to_date)
  end

  # Custom filter method for :article_id field.
  def filter_by_article_id(partial, value)
    partial.joins(:articles).where(article_media: { article_id: value })
  end

  # Custom filter method for searching by value on articles or medias.
  def filter_by_query(partial, value)
    value = "%#{value}%"
    # May brings performance issues
    articles_ids =  Article.search(query: value).ids
    media_ids = ArticleMedium.where(article_id: articles_ids).uniq.pluck(:medium_id)
    partial.where('name like :value OR media.id IN(:ids)', value: value, ids: media_ids)
  end

  def value_for_type(value)
    I18n.t 'media.types.' + value
  end
end
