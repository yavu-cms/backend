class EditionsFilter < BaseModelFilter
  def model
    Edition
  end

  def self.defaults
    { name: nil, date: nil, supplement_id: nil }
  end

  def simple_filter_key
    :name
  end

  # Custom filter method for edition's name.
  def filter_by_name(partial, value)
    date = value.to_date rescue nil
    partial.where 'date = :date OR name like :value', date: date, value: "%#{value}%"
  end

  def filter_by_date(partial, value)
    partial.where 'date = ?', value.to_date
  end
end
