class RedisFrontendServerCleaner
  include Sidekiq::Worker

  sidekiq_options queue: 'critical'

  def perform(*args)
    Sidekiq.logger.info('RedisFrontendServerCleaner') { "Cleaning entries upto=#{1.day.ago}" }
    FrontendServer.clean 1.day.ago
  end
end

Sidekiq::Cron::Job.create( name: 'Clean FrontendServers daily', cron: '1 3 * * *', klass: 'RedisFrontendServerCleaner')