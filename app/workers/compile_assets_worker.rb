class CompileAssetsWorker
  include Sidekiq::Worker

  sidekiq_options queue: 'critical', retry: false
  sidekiq_options unique: true, unique_args: :unique_args

  def perform(client_application_id, user, url = nil)
    # Shell out to a separate process to run the rake task that will actually compile the assets.
    # Compiling assets is a task that will greatly increase the size of the VM heap size (RSS)
    # thus we shell out here to make all the new object allocations involved in such process in a
    # different, discardable OS process.
    %x{RAILS_ENV=#{Rails.env} rake background:compile_assets[#{client_application_id},#{user},'#{url}']}
  end

  def unique_args(client_application_id, user)
    client_application_id
  end
end
