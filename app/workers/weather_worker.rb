class WeatherWorker
  include Sidekiq::Worker

  sidekiq_options queue: 'default', retry: false

  def perform
    logger.info('Weather') { 'Start scraping weather' }
    Weather.retrieve_and_store_record
    logger.info('Weather') { 'Weather scraping done' }
  rescue Exception => e
    logger.error('Weather') { "Weather scraping failed message=#{e.message} trace=#{e.backtrace.join(';')}" }
  end
end

Sidekiq::Cron::Job.create(name: 'Weather worker', cron: Setting['weather.frequency'], klass: 'WeatherWorker')