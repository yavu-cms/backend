class ElasticsearchIndexer
  include Sidekiq::Worker

  sidekiq_options queue: 'default', retry: 1

  def perform(operation, model_name, id)
    logger.info('ElasticsearchIndexer') { "Start operation=#{operation}_document model=#{model_name} id=#{id}" }
    begin
      model_name.constantize.find(id).__elasticsearch__.send :"#{operation}_document"
    rescue ActiveRecord::RecordNotFound
      logger.warn('ElasticsearchIndexer') { "No model found model=#{model_name} id=#{id} operation=#{operation}" }
    end
    logger.info('ElasticsearchIndexer') { "Finished operation=#{operation}_document model=#{model_name} id=#{id}" }
  rescue Exception => e
    logger.error('ElasticsearchIndexer') do
      "Error on operation=#{operation}_document model=#{model_name} id=#{id} message=#{e.message} trace=#{e.backtrace.join(';')}"
    end
  end
end
