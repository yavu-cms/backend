class NewsletterSenderWorker
  include Sidekiq::Worker

  sidekiq_options queue: 'critical', retry: false

  def perform(newsletter)
    Sidekiq.logger.info('NewsletterSender') { "Broadcasting newsletter=#{newsletter}" }
    static_data_id = Newsletter.static_data_id_for newsletter
    if static_data_id
      NewsletterMailer.broadcast(static_data_id)
      Sidekiq.logger.info('NewsletterSender') { "Broadcast done newsletter=#{newsletter}" }
    else
      Sidekiq.logger.info('NewsletterSender') { "Refusing to send empty newsletter=#{newsletter}" }
    end
  end
end
