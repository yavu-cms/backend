class CopyClientApplicationWorker
  include Sidekiq::Worker

  sidekiq_options queue: 'critical'

  def perform(client_app_id, client_app_params, username, url)
    Sidekiq.logger.debug('CopyClientApplicationWorker') { I18n.t('client_applications.copy.messages.started') }
    client_app_copy = ClientApplication.find(client_app_id).copy HashWithIndifferentAccess.new(client_app_params)

    if client_app_copy.save
      Sidekiq.logger.debug('CopyClientApplicationWorker') { I18n.t('client_applications.copy.messages.success') }
      Announcement.publish I18n.t('client_applications.copy.messages.success'), user: username, url: url
    else
      Sidekiq.logger.error(client_app_copy.errors.full_messages)
      Sidekiq.logger.error('CopyClientApplicationWorker') { I18n.t('client_applications.copy.messages.error') }
      Announcement.publish client_app_copy.errors.to_s, user: username, url: url, type: :error
    end
  end
end


