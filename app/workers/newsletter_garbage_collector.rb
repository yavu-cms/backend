class NewsletterGarbageCollectorWorker
  include Sidekiq::Worker

  sidekiq_options queue: 'default'

  def perform
    Sidekiq.logger.debug('NewsletterGarbageCollector') { 'Deleting old newsletters' }
    Newsletter.all.each do |newsletter|
      newsletter.static_data.where('created_at <= ?', newsletter.expiration.days.ago).delete_all
    end
    Sidekiq.logger.debug('NewsletterGarbageCollector') { 'Done deleting old newsletters' }
  end
end

Sidekiq::Cron::Job.create( name: 'Newsletter garbage collector worker', cron: YavuSettings.newsletter.garbage_time , klass: 'NewsletterGarbageCollectorWorker')