class FacebookScrapperWorker
  include Sidekiq::Worker

  sidekiq_options queue: 'default', retry: false

  def perform(article_id)
    article = Article.find article_id
    routes = ArticleRoute.joins(:client_application).where(id: article.news_source.client_applications.joins(:routes).select('routes.id'))
    routes = routes.reject &:constrained?
    routes.each do |ar|
      url_to_scrap = ar.absolute_url object: article
      logger.debug "Intentando scrappear esta url: #{url_to_scrap} para facebook"
      Scrappers::FacebookScrapper.clear_cache url_to_scrap
    end
  end
end
