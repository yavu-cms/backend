class UniqueAccessWorker
  include Sidekiq::Worker

  sidekiq_options queue: 'critical', retry: false

  def perform(channel, data, url)
    Announcement.notify_updates_to(channel, info: data, url: url)
  end
end
