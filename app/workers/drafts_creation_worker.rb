class DraftsCreationWorker
  include Sidekiq::Worker

  sidekiq_options queue: 'critical', retry: 1
  sidekiq_options unique: true, unique_args: :unique_args

  def perform(representation_id)
    logger.info('DraftsCreation') { "Started draft creation for representation_id=#{representation_id}" }
    # Shell out to a separate process to run the rake task that will actually crete the draft.
    # Creating a draft can potentially comprehend a significant amount of new object allocations which
    # in turn will greatly increase the size of the VM heap size (RSS) so we shell out here to make all
    # the new object allocations involved in such process in a different, discardable OS process.
    %x{cd #{Rails.root} && RAILS_ENV=#{Rails.env} rake background:create_draft[#{representation_id}]}
    logger.info('DraftsCreation') { "Finished draft creation for representation_id=#{representation_id}" }
  end

  def unique_args(representation_id)
    representation_id
  end
end
