class FrontendNotificatorWorker
  include Sidekiq::Worker

  sidekiq_options queue: 'critical', retry: false
  sidekiq_options unique: true, unique_args: :unique_args

  SLEEP_TIME  = 10
  MAX_RETRIES = 12

  def perform(client_application_identifier)
    # Randomly create a queue of frontends to restart and wait for their healthy
    frontends_to_restart = FrontendServer.all.shuffle
    count = frontends_to_restart.size
    frontends_to_restart.each.with_index(1) do |frontend, i|
      Sidekiq.logger.info('FrontendNotificatorWorker') do
        "Starting frontend notification randomly on frontend -> #{frontend.name}, step #{i}/#{count}"
      end
      frontend.restart!
      retries = 0
      until frontend.reload.healthy? || (retries > MAX_RETRIES)
        Sidekiq.logger.info('FrontendNotificatorWorker') do
          "Waiting for frontend -> #{frontend.name} to be OK, step #{i}/#{count}, sleeping #{SLEEP_TIME} seconds..."
        end
        sleep SLEEP_TIME
        retries += 1
      end
      if retries > MAX_RETRIES
        Sidekiq.logger.error('FrontendNotificatorWorker') do
          "Frontend notification timeout -> #{frontend.name}, aborting notification in step #{i}/#{count}."
        end
        return
      end
    end
    Sidekiq.logger.info('FrontendNotificatorWorker') do
      "Finished the notification of the #{count} connected frontends for client id -> #{client_application_identifier}"
    end
  end

  def unique_args(client_application_identifier)
    client_application_identifier
  end
end
