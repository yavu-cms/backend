class FrontendStatusRetriever
  include Sidekiq::Worker

  sidekiq_options queue: 'default'

  def perform(*args)
    FrontendServer.all.each do |server|
      begin
        body = RestClient::Request.execute method: :get, url: "#{server.name}/_yavu_frontend_status", timeout: 2
        if body.blank?
          server.mark_up
        else
          server.mark_unknown body
        end
      rescue Errno::EHOSTUNREACH, Errno::ECONNREFUSED, RestClient::RequestTimeout, RestClient::Exception => e
        if server.client_application_identifier
          server.mark_down
        else
          server.mark_without_client_id # It's still down
        end
      ensure
        server.save
      end
    end
  end

end

Sidekiq::Cron::Job.create( name: 'Frontend status retriever every one minute', cron: '*/1 * * * *', klass: 'FrontendStatusRetriever')
