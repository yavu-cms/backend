class DisqusCommentsWorker
  include Sidekiq::Worker

  sidekiq_options queue: 'default'

  def perform(*args)
    Sidekiq.logger.debug('DisqusComments') { 'Trying to retrieve disqus comments information from Disqus API' }
    Accounting::MostCommentedArticles.refreshDisqusDb
    Sidekiq.logger.debug('DisqusComments') { 'Disqus API requests done' }
  end
end

Sidekiq::Cron::Job.create( name: 'Disqus API requests every 5 minutes', cron: '*/5 * * * *', klass: 'DisqusCommentsWorker')