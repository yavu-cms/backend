class AddDefaultRouteToRoutes < ActiveRecord::Migration
  def change
    add_column :routes, :default_route, :boolean, default: false, null: false
  end
end
