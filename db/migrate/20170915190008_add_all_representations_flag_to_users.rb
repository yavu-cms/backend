class AddAllRepresentationsFlagToUsers < ActiveRecord::Migration
  def change
    add_column :users, :all_representations, :boolean, default: false, null: false
  end
end
