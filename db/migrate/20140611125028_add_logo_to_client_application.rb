class AddLogoToClientApplication < ActiveRecord::Migration
  def change
    add_reference :client_applications, :logo, index: true
  end
end
