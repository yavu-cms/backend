class CreateClientRepresentations < ActiveRecord::Migration
  def change
    create_table :client_representations do |t|
      t.references :client_application, index: true
      t.string :name
      t.boolean :published, default: false
      t.text :configuration

      t.index [:client_application_id, :name], unique: true

      t.timestamps
    end
  end
end
