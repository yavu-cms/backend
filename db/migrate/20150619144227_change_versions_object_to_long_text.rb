class ChangeVersionsObjectToLongText < ActiveRecord::Migration
  def up
    change_column :versions, :object, :text, limit: 4.gigabytes - 1
  end

  def down
    puts 'Skipping change_versions_object_to_long_text.'
  end
end
