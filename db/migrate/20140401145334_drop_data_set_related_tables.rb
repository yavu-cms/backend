class DropDataSetRelatedTables < ActiveRecord::Migration
  def change
    drop_table :data_sets
    drop_table :articles_data_sets
  end
end
