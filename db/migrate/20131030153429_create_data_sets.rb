class CreateDataSets < ActiveRecord::Migration
  def change
    create_table :data_sets do |t|
      t.timestamps
    end
  end
end
