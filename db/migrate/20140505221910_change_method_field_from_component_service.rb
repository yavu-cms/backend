class ChangeMethodFieldFromComponentService < ActiveRecord::Migration
  def change
    remove_index :component_services, name: 'index_component_services_on_method_and_name_and_component_id'
    rename_column :component_services, :method, :http_method
    add_index :component_services, [:http_method, :name, :component_id], unique: true, name: 'index_component_services_on_method_and_name_and_component_id'
  end
end
