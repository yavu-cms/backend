class AddDefaultValueToDateTimeArticles < ActiveRecord::Migration
  def change
    change_column :articles, :show_date, :boolean, :default => true
    change_column :articles, :show_time, :boolean, :default => true
  end
end
