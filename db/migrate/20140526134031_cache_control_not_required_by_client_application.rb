class CacheControlNotRequiredByClientApplication < ActiveRecord::Migration
  def change
    change_column :client_applications, :route_cache_control, :string,  null: true
    change_column :client_applications, :service_cache_control, :string, null: true
  end
end
