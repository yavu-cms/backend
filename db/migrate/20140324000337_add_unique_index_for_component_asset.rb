class AddUniqueIndexForComponentAsset < ActiveRecord::Migration
  def change
    add_index :component_assets, [:component_id, :file, :type ],unique: true
  end
end
