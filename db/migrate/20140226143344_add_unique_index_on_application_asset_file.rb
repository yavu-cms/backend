class AddUniqueIndexOnApplicationAssetFile < ActiveRecord::Migration

  def change
    add_index :client_application_assets, :file, unique: true
  end
end
