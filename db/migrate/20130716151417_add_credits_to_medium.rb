class AddCreditsToMedium < ActiveRecord::Migration
  def change
    add_column :media, :credits, :string
  end
end
