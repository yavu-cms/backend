class DropApiCacheTimeoutFields < ActiveRecord::Migration
  def change
    remove_column :client_applications, :api_current_version, :integer
    remove_column :client_applications, :api_cache_timeout, :integer
    remove_column :routes, :api_cache_timeout, :integer
  end
end
