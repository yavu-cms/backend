class AddConstraintsAndConditionsToRoute < ActiveRecord::Migration
  def change
    add_column :routes, :constraints, :text
    add_column :routes, :conditions,  :text
  end
end
