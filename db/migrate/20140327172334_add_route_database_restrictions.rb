class AddRouteDatabaseRestrictions < ActiveRecord::Migration
  def change
    change_column :routes, :name,                  :string, null: false
    change_column :routes, :pattern,               :string, null: false
    change_column :routes, :client_application_id, :int,    null: false

    add_index :routes, [:client_application_id, :name],    unique: true, name: 'cli_app_route_name'
    add_index :routes, [:client_application_id, :pattern], unique: true, name: 'cli_app_route_pattern'
  end
end
