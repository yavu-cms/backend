class CreateClientAssets < ActiveRecord::Migration
  def change
    create_table :client_assets do |t|
      t.string :type
      t.string :file
      t.references :client_component, index: true

      t.timestamps
    end
  end
end
