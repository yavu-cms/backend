class AddDefaultToViews < ActiveRecord::Migration
  def change
    add_column :views, :default, :boolean, default: false
  end
end
