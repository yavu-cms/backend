class RenameClientViews < ActiveRecord::Migration
  def change
    rename_table :client_views, :views
    rename_column :views, :client_component_id, :component_id
  end
end
