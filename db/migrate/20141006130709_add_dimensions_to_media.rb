class AddDimensionsToMedia < ActiveRecord::Migration
  def change
    add_column :media, :dimensions, :text
  end
end
