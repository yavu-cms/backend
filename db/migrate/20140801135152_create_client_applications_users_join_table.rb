class CreateClientApplicationsUsersJoinTable < ActiveRecord::Migration
  def change
    create_join_table :client_applications, :users do |t|
      t.index [:client_application_id, :user_id], unique: true, name: 'capp_id_user_id'
    end
  end
end
