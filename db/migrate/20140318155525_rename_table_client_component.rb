class RenameTableClientComponent < ActiveRecord::Migration
  def change
    rename_table :client_components, :components
  end
end
