class AddNewsSourceFkToSupplement < ActiveRecord::Migration
  def change
    add_reference :supplements, :news_source, index: true
  end
end
