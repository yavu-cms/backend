class AddNameDateToMedia < ActiveRecord::Migration
  def change
    add_column :media, :name, :string
    add_column :media, :date, :date

    add_index :media, :name
    add_index :media, :date
  end
end
