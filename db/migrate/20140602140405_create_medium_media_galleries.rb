class CreateMediumMediaGalleries < ActiveRecord::Migration
  def change
    create_table :medium_media_galleries do |t|
      t.references :media_gallery, index: true
      t.references :medium, index: true

      t.timestamps
    end
  end
end
