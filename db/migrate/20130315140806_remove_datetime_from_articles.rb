class RemoveDatetimeFromArticles < ActiveRecord::Migration
  def change
    remove_column :articles, :datetime, :datetime
  end
end
