class AddNewsSourceFkToClientApplication < ActiveRecord::Migration
  def change
    add_reference :client_applications, :news_source, index: true
  end
end
