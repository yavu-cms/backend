class DropRepresentationTypeToRow < ActiveRecord::Migration
  def change
    remove_column :rows, :representation_type
  end
end
