class AddRedirectUrlToRoute < ActiveRecord::Migration
  def change
    add_column :routes, :redirect_url, :string
  end
end
