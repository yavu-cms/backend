class DropRepresentationIdFromRoutes < ActiveRecord::Migration
  def change
  	remove_column :routes, :representation_id
  end
end
