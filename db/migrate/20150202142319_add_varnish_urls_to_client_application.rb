class AddVarnishUrlsToClientApplication < ActiveRecord::Migration
  def change
    add_column :client_applications, :varnish_urls, :text
  end
end
