class CreateRolesUsersJoinTable < ActiveRecord::Migration
  def change
    create_join_table :roles, :users do |t|
      t.index [:role_id, :user_id], unique: true, name: 'role_id_user_id'
    end
  end
end
