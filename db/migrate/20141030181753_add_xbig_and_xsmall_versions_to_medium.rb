class AddXbigAndXsmallVersionsToMedium < ActiveRecord::Migration
  def change
    add_column :media, :xbig_version, :string
    add_column :media, :xsmall_version, :string
  end
end
