class MakeNewsletterNameIndexUnique < ActiveRecord::Migration
  def up
    remove_index :newsletters, :name
    add_index :newsletters, :name, unique: true
  end
  def down
    remove_index :newsletters, :name
    add_index :newsletters, :name
  end
end
