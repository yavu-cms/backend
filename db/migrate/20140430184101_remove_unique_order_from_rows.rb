class RemoveUniqueOrderFromRows < ActiveRecord::Migration
  def change
    remove_index :rows, name: 'representation_id_order'
  end
end
