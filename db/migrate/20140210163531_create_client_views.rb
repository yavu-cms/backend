class CreateClientViews < ActiveRecord::Migration
  def change
    create_table :client_views do |t|
      t.text :name
      t.text :value
      t.references :client_component, index: true

      t.timestamps
    end
  end
end
