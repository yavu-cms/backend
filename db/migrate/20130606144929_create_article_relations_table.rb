class CreateArticleRelationsTable < ActiveRecord::Migration
  def change
    create_table :article_relations, id: false do |t|
      t.integer :article_a_id, null: false
      t.integer :article_b_id, null: false

      t.index [:article_a_id, :article_b_id], unique: true
    end
  end
end
