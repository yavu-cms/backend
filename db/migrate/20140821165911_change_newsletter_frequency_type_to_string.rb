class ChangeNewsletterFrequencyTypeToString < ActiveRecord::Migration
  def change
    remove_column :newsletters, :frequency, :integer
    add_column :newsletters, :frequency, :string
  end
end
