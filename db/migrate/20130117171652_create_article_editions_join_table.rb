class CreateArticleEditionsJoinTable < ActiveRecord::Migration
  def change
    create_join_table :articles, :editions do |t|
      t.index [:edition_id, :article_id], unique: true
    end
  end
end
