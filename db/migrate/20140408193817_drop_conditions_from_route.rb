class DropConditionsFromRoute < ActiveRecord::Migration
  def change
    remove_column :routes, :conditions, :text
  end
end
