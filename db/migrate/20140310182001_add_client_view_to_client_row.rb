class AddClientViewToClientRow < ActiveRecord::Migration
  def change
    add_reference :client_rows, :client_view, index: true
  end
end
