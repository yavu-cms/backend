class AddMainArticleToSection < ActiveRecord::Migration
  def change
  	add_reference :sections, :main_article, index: true
  end
end