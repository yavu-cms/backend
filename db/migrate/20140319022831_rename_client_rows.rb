class RenameClientRows < ActiveRecord::Migration
  def change
    rename_table :client_rows, :rows
    rename_column :rows, :client_representation_id, :representation_id
    rename_column :rows, :client_view_id, :view_id
  end
end
