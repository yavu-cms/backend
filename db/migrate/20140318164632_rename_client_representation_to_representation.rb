class RenameClientRepresentationToRepresentation < ActiveRecord::Migration
  def change
    rename_table :client_representations, :representations
    rename_column :representations, :client_representation_id, :representation_id
  end
end
