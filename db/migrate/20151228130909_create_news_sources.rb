class CreateNewsSources < ActiveRecord::Migration
  def change
    create_table :news_sources do |t|
      t.string :name, null: false
      t.boolean :is_default, null: false, default: false
    end
  end
end
