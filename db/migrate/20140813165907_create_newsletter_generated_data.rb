class CreateNewsletterGeneratedData < ActiveRecord::Migration
  def change
    create_table :newsletter_generated_data do |t|
      t.string :slug, null: false, unique: true
      t.text :content, null: false

      t.references :newsletter, null: false
      t.index :slug
      t.timestamps
    end
  end
end
