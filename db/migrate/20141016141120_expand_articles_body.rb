class ExpandArticlesBody < ActiveRecord::Migration
  def change
    change_column :articles, :body, :text, limit: 4.gigabytes - 1 # Creates a longtext column
  end
end
