class DropSerializedServersFromClientApplication < ActiveRecord::Migration
  def change
    remove_column :client_applications, :serialized_servers
  end
end
