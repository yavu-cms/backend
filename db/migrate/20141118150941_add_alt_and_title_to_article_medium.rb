class AddAltAndTitleToArticleMedium < ActiveRecord::Migration
  def change
    add_column :article_media, :alt, :string
    add_column :article_media, :title, :string
  end
end
