class RemoveSectionSlugIndex < ActiveRecord::Migration
  def change
    remove_index :sections, :slug
  end
end
