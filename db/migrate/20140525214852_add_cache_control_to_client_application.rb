class AddCacheControlToClientApplication < ActiveRecord::Migration
  def change
    add_column :client_applications, :route_cache_control, :string, null: false
    add_column :client_applications, :service_cache_control, :string, null: false
  end
end
