class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string      :slug,      null: false, index: true
      t.text        :heading
      t.text        :title,     null: false
      t.text        :lead
      t.text        :body
      t.text        :signature
      t.datetime    :datetime
      t.boolean     :show_date, null: false, default: false
      t.boolean     :show_time, null: false, default: false
      t.integer     :read,      null: false, default: 0

      t.belongs_to  :section,   index: true
      t.belongs_to  :edition,   index: true

      t.timestamps
    end
  end
end
