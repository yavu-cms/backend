class CreateArticleMedia < ActiveRecord::Migration
  def change
    create_table :article_media do |t|
      t.references :article, index: true
      t.references :medium, index: true
      t.boolean :main
      t.string :caption
      t.integer :order

      t.timestamps
    end
  end
end
