class AddClientApplicationAssetDbRestrictions < ActiveRecord::Migration
  def change
    remove_column :client_application_assets, :full_name
    add_index :client_application_assets, [:client_application_id, :type, :prefix_path, :file ], unique: true, name: :client_application_file_unique
    add_column :client_application_assets, :content_type, :string
  end
end
