class CreateJoinTableClientApplicationAssetRepresentation < ActiveRecord::Migration
  def change
    create_join_table :client_application_assets, :representations do |t|
       t.index [:representation_id, :client_application_asset_id], unique: true, name: "index_client_application_assets_representations"
    end
  end
end
