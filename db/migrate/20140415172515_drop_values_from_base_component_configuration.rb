class DropValuesFromBaseComponentConfiguration < ActiveRecord::Migration
  def change
    remove_column :base_component_configurations, :values, :text
  end
end
