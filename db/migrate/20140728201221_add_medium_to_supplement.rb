class AddMediumToSupplement < ActiveRecord::Migration
  def change
    add_reference :supplements, :logo, index: true
  end
end
