class CreateComponentServices < ActiveRecord::Migration
  def change
    create_table :component_services do |t|
      t.string :method, null: false
      t.string :name, null:false
      t.text :body
      t.references :component, null: false
      t.timestamps
    end
    add_index :component_services, [:method, :name], unique: true
  end
end
