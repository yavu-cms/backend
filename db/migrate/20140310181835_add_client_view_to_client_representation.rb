class AddClientViewToClientRepresentation < ActiveRecord::Migration
  def change
    add_reference :client_representations, :client_view, index: true
  end
end
