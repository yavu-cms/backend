class AddCommentsEnabledToSections < ActiveRecord::Migration
  def change
    add_column :sections, :comments_enabled, :boolean
  end
end
