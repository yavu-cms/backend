class RemoveDateAndShowsFromArticle < ActiveRecord::Migration
  def change
    remove_column :articles, :date, :datetime
    remove_column :articles, :show_date, :boolean
    remove_column :articles, :show_time, :boolean
  end
end
