class RenameRepresentationsToBaseRepresentations < ActiveRecord::Migration
  def change
    rename_table :representations, :base_representations
  end
end
