class CreateClientRows < ActiveRecord::Migration
  def change
    create_table :client_rows do |t|
      t.references :client_representation
      t.integer :order
      t.text :configuration

      t.timestamps
    end
  end
end
