class AddDatabaseRestrictionsToComponentConfiguration < ActiveRecord::Migration
  def change
    change_column :component_configurations, :component_id, :integer, null: false
    change_column :component_configurations, :description, :string, null: false
    change_column :component_configurations, :view_id, :integer, null: false
    change_column :component_configurations, :column_id, :integer, null: false
  end
end
