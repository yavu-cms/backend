class RemoveAssetsDebugFromClientApplications < ActiveRecord::Migration
  def change
    remove_column :client_applications, :assets_debug, :boolean
  end
end
