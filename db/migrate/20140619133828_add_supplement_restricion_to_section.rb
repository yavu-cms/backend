class AddSupplementRestricionToSection < ActiveRecord::Migration
  def change
    change_column :sections, :supplement_id, :integer, null: false
  end
end
