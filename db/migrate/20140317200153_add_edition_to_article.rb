class AddEditionToArticle < ActiveRecord::Migration
  def change
    add_reference :articles, :edition, index: true
  end
end
