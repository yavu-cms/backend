class CreateClientApplicationAssets < ActiveRecord::Migration
  def change
    create_table :client_application_assets do |t|
      t.string :type
      t.string :file
      t.references :client_application, index: true

      t.timestamps
    end
  end
end
