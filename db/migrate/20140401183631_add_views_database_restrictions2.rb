class AddViewsDatabaseRestrictions2 < ActiveRecord::Migration
  def change
    change_column :views, :value, :text, null: false
  end
end
