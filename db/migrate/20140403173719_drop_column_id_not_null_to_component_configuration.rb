class DropColumnIdNotNullToComponentConfiguration < ActiveRecord::Migration
  def change
    change_column :component_configurations, :column_id, :integer, null: true, index: true
  end
end
