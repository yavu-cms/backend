class RenameClientColumn < ActiveRecord::Migration
  def change
    rename_table :client_columns, :columns
    rename_column :columns, :client_row_id, :row_id
    rename_column :columns, :client_view_id, :view_id
  end
end
