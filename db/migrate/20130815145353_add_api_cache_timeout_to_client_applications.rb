class AddApiCacheTimeoutToClientApplications < ActiveRecord::Migration
  def change
    add_column :client_applications, :api_current_version, :integer, default: 0, null: false
    add_column :client_applications, :api_cache_timeout, :integer, default: 600, null: false
  end
end
