class AddRowViewRefToClientRepresentations < ActiveRecord::Migration
  def change
    add_reference :client_representations, :row_view, index: true
  end
end
