class AddCombinerIdToTag < ActiveRecord::Migration
  def change
    add_belongs_to :tags, :combiner, index: true
  end
end