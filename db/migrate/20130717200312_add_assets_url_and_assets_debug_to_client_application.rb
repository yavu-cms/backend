class AddAssetsUrlAndAssetsDebugToClientApplication < ActiveRecord::Migration
  def change
    add_column :client_applications, :assets_url, :string
    add_column :client_applications, :assets_debug, :boolean, default: false
  end
end
