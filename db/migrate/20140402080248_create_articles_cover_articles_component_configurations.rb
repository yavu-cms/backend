class CreateArticlesCoverArticlesComponentConfigurations < ActiveRecord::Migration
  def change
    create_table :articles_cover_articles_component_configurations do |t|
      t.references :article, index: { name: 'acacc_article_id' }, null: false
      t.references :cover_articles_component_configuration, index: { name: 'acacc_cover_articles_component_configuration_id'}, null: false
      t.references :edition, index: {name: 'acacc_edition_id' }, null: false
      t.integer :order, :integer, default: 0

      t.timestamps
    end
    add_index :articles_cover_articles_component_configurations, [:edition_id, :article_id, :cover_articles_component_configuration_id], unique: true, name: 'acacc_unique'
  end
end
