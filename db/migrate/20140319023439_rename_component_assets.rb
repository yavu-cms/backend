class RenameComponentAssets < ActiveRecord::Migration
  def change
    rename_column :component_assets, :client_component_id, :component_id
  end
end
