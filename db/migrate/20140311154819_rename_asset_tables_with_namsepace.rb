class RenameAssetTablesWithNamsepace < ActiveRecord::Migration
  def change
    rename_table :client_assets, :component_assets
  end
end
