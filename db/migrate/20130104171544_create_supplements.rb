class CreateSupplements < ActiveRecord::Migration
  def change
    create_table :supplements do |t|
      t.string  :name,        null: false, unique: true
      t.boolean :is_default,  null: false, default: false

      t.timestamps
    end
  end
end
