class AddComponentTranslationsDatabaseRestrictions < ActiveRecord::Migration
  def change
    change_column :component_translations, :component_id, :integer, null: false
    change_column :component_translations, :locale, :string, null: false
  end
end
