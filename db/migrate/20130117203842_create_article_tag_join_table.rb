class CreateArticleTagJoinTable < ActiveRecord::Migration
  def change
    create_join_table :articles, :tags do |t|
      t.index [:tag_id, :article_id], unique: true
    end
  end
end
