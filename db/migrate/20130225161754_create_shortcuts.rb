class CreateShortcuts < ActiveRecord::Migration
  def change
    create_table :shortcuts do |t|
      t.integer :user_id, null: false, index: true
      t.string  :title,   null: false
      t.string  :url,     null: false
      t.integer :priority

      t.timestamps
    end
  end
end
