class AddOrderToMediumMediaGalleries < ActiveRecord::Migration
  def change
    add_column :medium_media_galleries, :order, :integer, default: 0
  end
end
