class AddPrefixPathToClientApplicationAsset < ActiveRecord::Migration
  def change
    add_column :client_application_assets, :prefix_path, :string, default: ''
  end
end
