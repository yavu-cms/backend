class AddCaptionToArticleRelations < ActiveRecord::Migration
  def change
    add_column :article_relations, :caption, :text
  end
end