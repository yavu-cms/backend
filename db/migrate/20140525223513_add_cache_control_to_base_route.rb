class AddCacheControlToBaseRoute < ActiveRecord::Migration
  def change
    add_column :routes, :cache_control, :string, null: true
  end
end
