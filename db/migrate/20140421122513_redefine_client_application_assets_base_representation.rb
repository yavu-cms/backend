class RedefineClientApplicationAssetsBaseRepresentation < ActiveRecord::Migration
  def change
    drop_table :client_application_assets_base_representations
    create_table :client_application_assets_base_representations do |t|
      t.column :client_application_asset_id, :integer
      t.column :base_representation_id, :integer
      t.index [:base_representation_id, :client_application_asset_id], unique: true, name: "index_client_application_assets_representations"
    end
  end
end
