class AddErrorViewToRepresentation < ActiveRecord::Migration
  def change
    add_column :representations, :error_view_id, :integer, null: false
  end
end
