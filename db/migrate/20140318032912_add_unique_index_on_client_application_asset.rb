class AddUniqueIndexOnClientApplicationAsset < ActiveRecord::Migration
  def change
    remove_index :client_application_assets, :file
    add_index :client_application_assets, :full_name, unique: true
  end
end
