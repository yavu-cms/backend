class ImproveVarnishUrlsToClientApplication < ActiveRecord::Migration
  def change
    rename_column :client_applications, :varnish_urls, :external_varnish_urls
    add_column :client_applications, :internal_varnish_urls, :text
  end
end
