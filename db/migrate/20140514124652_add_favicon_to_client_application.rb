class AddFaviconToClientApplication < ActiveRecord::Migration
  def change
    add_reference :client_applications, :favicon, index: true
  end
end
