class AddUniqueIndexForComponentTranslation < ActiveRecord::Migration
  def change
    add_index :component_translations, [:component_id, :locale],unique: true
  end
end
