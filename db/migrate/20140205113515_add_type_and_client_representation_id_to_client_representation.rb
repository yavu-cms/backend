class AddTypeAndClientRepresentationIdToClientRepresentation < ActiveRecord::Migration
  def change
    add_column :client_representations, :type, :string
    add_reference :client_representations, :client_representation, index: true, null: true
  end
end