class AddContextToNewsletter < ActiveRecord::Migration
  def change
    add_column :newsletters, :context, :text, null: true
  end
end
