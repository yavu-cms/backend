class AddNotNullRestrictionToSectionInArticles < ActiveRecord::Migration
  def change
    change_column :articles, :section_id, :integer, null: false
  end
end
