class AddTypeToComponentConfiguration < ActiveRecord::Migration
  def change
    add_column :client_component_configurations, :type, :string
  end
end
