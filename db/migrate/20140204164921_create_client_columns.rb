class CreateClientColumns < ActiveRecord::Migration
  def change
    create_table :client_columns do |t|
      t.references :client_row
      t.integer :order
      t.text :configuration

      t.timestamps
    end
  end
end
