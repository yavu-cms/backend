class AddIdToArticleRelations < ActiveRecord::Migration
  def change
    add_column :article_relations, :id, :primary_key
  end
end
