class AddRepresentationToRoute < ActiveRecord::Migration
  def change
    add_column :routes, :representation_id, :string, null: false
  end
end
