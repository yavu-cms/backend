class AddClientViewToClientColumn < ActiveRecord::Migration
  def change
    add_reference :client_columns, :client_view, index: true
  end
end
