class RenameRouteClientRepresentationField < ActiveRecord::Migration
  def change
    rename_column :routes, :client_representation_id, :representation_id
  end
end
