class CreateExtraAttributes < ActiveRecord::Migration
  def change
    create_table :extra_attributes do |t|
      t.string :name, null: false, unique: true, index: true
      t.string :description
      t.string :attribute_type, null: false
      t.timestamps
    end
  end
end
