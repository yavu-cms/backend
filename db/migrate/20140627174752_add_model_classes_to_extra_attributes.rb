class AddModelClassesToExtraAttributes < ActiveRecord::Migration
  def change
    add_column :extra_attributes, :model_classes, :text
  end
end
