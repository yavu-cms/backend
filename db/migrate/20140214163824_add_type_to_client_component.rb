class AddTypeToClientComponent < ActiveRecord::Migration
  def change
    add_column :client_components, :type, :string
  end
end
