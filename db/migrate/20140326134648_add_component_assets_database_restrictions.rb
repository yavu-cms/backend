class AddComponentAssetsDatabaseRestrictions < ActiveRecord::Migration
  def change
    change_column :component_assets, :component_id, :integer, null: false
    change_column :component_assets, :type, :string, null: false
    change_column :component_assets, :file, :string, null: false
  end
end
