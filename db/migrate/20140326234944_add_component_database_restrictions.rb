class AddComponentDatabaseRestrictions < ActiveRecord::Migration
  def change
    change_column :components, :name, :string, null: false
    add_index :components, :name, unique: true
  end
end
