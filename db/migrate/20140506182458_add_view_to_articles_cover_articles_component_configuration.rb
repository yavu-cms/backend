class AddViewToArticlesCoverArticlesComponentConfiguration < ActiveRecord::Migration
  def change
    add_reference :articles_cover_articles_component_configurations, :view,
                  index: {name: 'articles_cover_articles_cc_view'},
                  null: false
  end
end
