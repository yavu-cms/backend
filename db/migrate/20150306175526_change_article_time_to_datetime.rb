class ChangeArticleTimeToDatetime < ActiveRecord::Migration
  def up
    add_column :articles, :newtime, :datetime
    offset = Time.now.in_time_zone(YavuSettings.i18n.time_zone).utc_offset / 3600
    Article.find_each(batch_size: 2000) do |a|
      a.update! newtime: DateTime.new(a.date.year, a.date.month, a.date.day, a.time.hour, a.time.min, a.time.sec, offset)
    end
    remove_column :articles, :time
    rename_column :articles, :newtime, :time

  end

  def down
    change_column :articles, :time, :time
  end
end
