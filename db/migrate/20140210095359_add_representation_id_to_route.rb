class AddRepresentationIdToRoute < ActiveRecord::Migration
  def change
    add_reference :routes, :client_representation, index: true
  end
end