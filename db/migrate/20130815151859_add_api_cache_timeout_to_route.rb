class AddApiCacheTimeoutToRoute < ActiveRecord::Migration
  def change
    add_column :routes, :api_cache_timeout, :integer, default: nil
  end
end
