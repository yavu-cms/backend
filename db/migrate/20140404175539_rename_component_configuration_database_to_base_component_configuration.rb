class RenameComponentConfigurationDatabaseToBaseComponentConfiguration < ActiveRecord::Migration
  def change
    rename_table :component_configurations, :base_component_configurations
  end
end
