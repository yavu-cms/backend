class CreateRoutes < ActiveRecord::Migration
  def change
    create_table :routes do |t|
      t.string :name
      t.string :pattern
      t.integer :type_id
      t.references :client_application, index: true

      t.timestamps
    end

    add_index :routes, :pattern
    add_index :routes, [:client_application_id, :pattern], unique: true
  end
end