class RemoveVisitsFromArticles < ActiveRecord::Migration
  def change
    remove_column :articles, :visits, :integer
  end
end
