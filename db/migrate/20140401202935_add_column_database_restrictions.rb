class AddColumnDatabaseRestrictions < ActiveRecord::Migration
  def change
    change_column :columns, :row_id, :int, null: false
    change_column :columns, :order, :int, null: false

    add_index :columns, [:row_id, :order], unique: true, name: 'row_id_order'
  end
end
