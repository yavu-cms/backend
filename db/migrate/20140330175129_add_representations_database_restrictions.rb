class AddRepresentationsDatabaseRestrictions < ActiveRecord::Migration
  def change
    change_column :representations, :client_application_id, :integer, null: false
    change_column :representations, :name, :string, null: false
    change_column :representations, :published, :boolean, null: false, default: false

    add_index :representations, [:client_application_id, :name], unique: true, name: 'unique_representation_per_ca'
  end
end
