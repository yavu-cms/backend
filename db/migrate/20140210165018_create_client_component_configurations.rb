class CreateClientComponentConfigurations < ActiveRecord::Migration
  def change
    create_table :client_component_configurations do |t|
      t.references :client_component_configuration, index: {name: 'ccc_id'}
      t.references :client_component, index: true
      t.references :client_column, index: true
      t.references :client_view, index: true
      t.boolean :hidden
      t.integer :order
      t.text :values

      t.timestamps
    end
  end
end
