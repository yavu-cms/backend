class DropColumnEditionToArticlesCoverArticlesComponentConfiguration < ActiveRecord::Migration
  def change
    remove_column :articles_cover_articles_component_configurations, :edition_id, :integer
  end
end
