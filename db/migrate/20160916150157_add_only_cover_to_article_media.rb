class AddOnlyCoverToArticleMedia < ActiveRecord::Migration
  def change
    add_column :article_media, :only_cover, :boolean, null: false, default: false
  end
end
