class AddHiddenToTag < ActiveRecord::Migration
  def change
    add_column :tags, :hidden, :boolean, default: false
  end
end
