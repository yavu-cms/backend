class RenameClientComponentConfiguration < ActiveRecord::Migration
  def change
    rename_table :client_component_configurations, :component_configurations
    rename_column :component_configurations, :client_component_configuration_id, :component_configuration_id
    rename_column :component_configurations, :client_component_id, :component_id
    rename_column :component_configurations, :client_column_id, :column_id
    rename_column :component_configurations, :client_view_id, :view_id
  end
end
