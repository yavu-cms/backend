class AddModelClassToComponentFields < ActiveRecord::Migration
  def change
    add_column :component_fields, :model_class, :string
  end
end
