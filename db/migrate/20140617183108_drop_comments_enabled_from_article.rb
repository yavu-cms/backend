class DropCommentsEnabledFromArticle < ActiveRecord::Migration
  def change
    remove_column :articles, :comments_enabled, :boolean
  end
end
