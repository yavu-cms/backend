class AddCacheControlToComponentService < ActiveRecord::Migration
  def change
    add_column :component_services, :cache_control, :string
  end
end
