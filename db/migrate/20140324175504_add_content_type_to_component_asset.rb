class AddContentTypeToComponentAsset < ActiveRecord::Migration
  def change
    add_column :component_assets, :content_type, :string
  end
end
