class DropPublishedFromBaseRepresentation < ActiveRecord::Migration
  def change
    remove_column :base_representations, :published
  end
end
