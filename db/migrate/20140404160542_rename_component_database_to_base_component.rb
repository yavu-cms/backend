class RenameComponentDatabaseToBaseComponent < ActiveRecord::Migration
  def change
    rename_table :components, :base_components
  end
end
