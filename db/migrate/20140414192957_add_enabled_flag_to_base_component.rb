class AddEnabledFlagToBaseComponent < ActiveRecord::Migration
  def change
    add_column :base_components, :enabled, :boolean, default: true
  end
end
