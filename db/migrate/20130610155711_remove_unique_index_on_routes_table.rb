class RemoveUniqueIndexOnRoutesTable < ActiveRecord::Migration
  def change
    remove_index :routes, [:client_application_id, :pattern]
  end
end
