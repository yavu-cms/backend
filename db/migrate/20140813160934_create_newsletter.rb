class CreateNewsletter < ActiveRecord::Migration
  def change
    create_table :newsletters do |t|
      t.string :name, null: false, unique: true
      t.text :generator, null: false
      t.boolean :enabled, default: true
      t.integer :frequency, null: false
      t.integer :expiration, null: false

      t.references :client_application, null: false
      t.index :name
      t.timestamps
    end
  end
end
