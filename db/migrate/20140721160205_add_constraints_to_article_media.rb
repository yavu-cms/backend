class AddConstraintsToArticleMedia < ActiveRecord::Migration
  def change
    ArticleMedium.where(article_id: nil).destroy_all
    ArticleMedium.where(medium_id: nil).destroy_all

    change_column_null :article_media, :article_id, false
    change_column_null :article_media, :medium_id, false
  end
end
