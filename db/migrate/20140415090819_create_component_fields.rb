class CreateComponentFields < ActiveRecord::Migration
  def change
    create_table :component_fields do |t|
      t.references :base_component, index: { name: 'csf_component_id'}
      t.references :base_component_configuration, index: { name: 'csf_cconfig_id'}
      t.string :description
      t.string :name, null: false
      t.string :type
      t.text :default
    end
  end
end
