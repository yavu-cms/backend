class AddSerializedServersToClientApplication < ActiveRecord::Migration
  def change
    add_column :client_applications, :serialized_servers, :text
  end
end