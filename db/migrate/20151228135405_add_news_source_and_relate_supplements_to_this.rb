class AddNewsSourceAndRelateSupplementsToThis < ActiveRecord::Migration
  def up
    ns = NewsSource.create! name: 'Default', is_default: true
    Supplement.update_all news_source_id: ns
    ClientApplication.update_all news_source_id: ns
    newsletter = Newsletter.find_by(name: 'Ultimas noticias')
    newsletter.update(context: "rep = @client_application.homepage_route.representation\r\n@news = rep.cover_articles('Bloque superior de noticias', 'Bloque medio') do |articles|\r\n  articles.on_active_edition\r\nend.first(10)\r\n@edition = NewsSource.default_supplement.active_edition\r\n\r\n# Cancelar el envio del newsletter si no hay noticias para mostrar\r\n@__no_send = true if @news.empty?") if newsletter
  end

  def down
    newsletter = Newsletter.find_by(name: 'Ultimas noticias')
    newsletter.update(context: "rep = @client_application.homepage_route.representation\r\n@news = rep.cover_articles('Bloque superior de noticias', 'Bloque medio') do |articles|\r\n  articles.on_active_edition\r\nend.first(10)\r\n@edition = Supplement.default.active_edition\r\n\r\n# Cancelar el envio del newsletter si no hay noticias para mostrar\r\n@__no_send = true if @news.empty?") if newsletter
    ClientApplication.update_all news_source_id: nil
    Supplement.update_all news_source_id: nil
    NewsSource.destroy_all
  end
end
