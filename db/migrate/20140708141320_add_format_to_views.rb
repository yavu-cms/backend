class AddFormatToViews < ActiveRecord::Migration
  def change
    add_column :views, :format, :string, default: 'text/html', null: false
  end
end
