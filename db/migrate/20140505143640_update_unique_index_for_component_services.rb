class UpdateUniqueIndexForComponentServices < ActiveRecord::Migration
  def change
    remove_index :component_services, name: 'index_component_services_on_method_and_name'
    add_index :component_services, [:method, :name, :component_id], unique: true
  end
end
