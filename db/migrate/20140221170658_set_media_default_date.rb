class SetMediaDefaultDate < ActiveRecord::Migration
  def change
    Medium.connection.execute("UPDATE media SET date = created_at WHERE date IS NULL")
  end
end
