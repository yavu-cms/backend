class DropDefaultsFromBaseComponent < ActiveRecord::Migration
  def change
    remove_column :base_components, :defaults, :text
  end
end
