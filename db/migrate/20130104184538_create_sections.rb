class CreateSections < ActiveRecord::Migration
  def change
    create_table :sections do |t|
      t.string      :name,        null: false
      t.string      :slug,        null: false
      t.boolean     :is_visible,  null: false, default: true

      t.references  :supplement,  index: true
      t.belongs_to  :section,     index: true

      t.timestamps
    end

    add_index :sections, :slug, unique: true
  end
end
