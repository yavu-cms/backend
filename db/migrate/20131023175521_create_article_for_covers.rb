class CreateArticleForCovers < ActiveRecord::Migration
  def change
    create_table :article_for_covers do |t|
      t.references :article, index: true
      t.references :client_application, index: true

      t.timestamps
    end
  end
end
