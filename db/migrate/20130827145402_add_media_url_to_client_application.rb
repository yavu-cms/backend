class AddMediaUrlToClientApplication < ActiveRecord::Migration
  def change
    add_column :client_applications, :media_url, :string
  end
end
