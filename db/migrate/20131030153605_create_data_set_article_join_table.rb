class CreateDataSetArticleJoinTable < ActiveRecord::Migration
  def change
    create_join_table :data_sets, :articles do |t|
      t.index [:data_set_id, :article_id], unique: true
    end
  end
end
