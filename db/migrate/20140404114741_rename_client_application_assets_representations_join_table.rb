class RenameClientApplicationAssetsRepresentationsJoinTable < ActiveRecord::Migration
  def change
    rename_table :client_application_assets_representations, :client_application_assets_base_representations
  end
end
