class MakeNewsletterGeneratedDataSlugIndexUnique < ActiveRecord::Migration
  def up
    remove_index :newsletter_generated_data, :slug
    add_index :newsletter_generated_data, :slug, unique: true
  end
  def down
    remove_index :newsletter_generated_data, :slug
    add_index :newsletter_generated_data, :slug
  end
end
