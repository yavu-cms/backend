class AddBaseRepresentationUserJoinTable < ActiveRecord::Migration
  def change
    create_join_table :base_representations, :users do |t|
      t.index :base_representation_id
      t.index :user_id
    end
  end
end
