class AddViewsDatabaseRestrictions < ActiveRecord::Migration
  def change
    change_column :views, :name, :string, null: false
  end
end
