class AddVisibilityToArticle < ActiveRecord::Migration
  def change
    add_column :articles, :is_visible, :boolean, default: true
    add_column :articles, :hide_from_cover, :boolean, default: false
  end
end
