class RemoveUniqueOrderFromColumns < ActiveRecord::Migration
  def change
    remove_index :columns, name: 'row_id_order'
  end
end
