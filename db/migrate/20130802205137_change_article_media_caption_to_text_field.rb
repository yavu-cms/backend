class ChangeArticleMediaCaptionToTextField < ActiveRecord::Migration
  def change
    change_column :article_media, :caption, :text
  end
end
