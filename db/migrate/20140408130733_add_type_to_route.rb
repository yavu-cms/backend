class AddTypeToRoute < ActiveRecord::Migration
  def change
    add_column :routes, :type, :string
  end
end
