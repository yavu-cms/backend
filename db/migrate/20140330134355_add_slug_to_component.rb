class AddSlugToComponent < ActiveRecord::Migration
  def change
    add_column :components, :slug, :string, null: false
  end
end
