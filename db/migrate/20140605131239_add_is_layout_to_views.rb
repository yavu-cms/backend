class AddIsLayoutToViews < ActiveRecord::Migration
  def change
    add_column :views, :is_layout, :boolean, default: false, null: false
  end
end
