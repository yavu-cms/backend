class RenameTableClientTranslations < ActiveRecord::Migration
  def change
    rename_table :client_translations, :component_translations
    rename_column :component_translations, :client_component_id, :component_id
  end
end
