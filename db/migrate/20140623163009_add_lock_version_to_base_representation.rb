class AddLockVersionToBaseRepresentation < ActiveRecord::Migration
  def change
    add_column :base_representations, :lock_version, :integer, default: 0, null: false
  end
end
