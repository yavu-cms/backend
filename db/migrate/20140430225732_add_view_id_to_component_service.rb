class AddViewIdToComponentService < ActiveRecord::Migration
  def change
    add_reference :component_services, :view, index: true, null: false
  end
end
