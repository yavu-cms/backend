class AddColumnViewRefToClientRepresentations < ActiveRecord::Migration
  def change
    add_reference :client_representations, :column_view, index: true
  end
end
