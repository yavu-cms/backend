class CreateClientComponents < ActiveRecord::Migration
  def change
    create_table :client_components do |t|
      t.text :name, null: false, unique: true
      t.text :extras
      t.text :context
      t.text :description
      t.text :defaults
      t.text :readme

      t.timestamps
    end
  end
end
