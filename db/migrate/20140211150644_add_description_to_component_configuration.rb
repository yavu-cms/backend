class AddDescriptionToComponentConfiguration < ActiveRecord::Migration
  def change
    add_column :client_component_configurations, :description, :string
  end
end
