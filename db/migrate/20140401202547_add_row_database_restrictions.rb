class AddRowDatabaseRestrictions < ActiveRecord::Migration
  def change
    change_column :rows, :representation_id, :int, null: false
    change_column :rows, :order, :int, null: false

    add_index :rows, [:representation_id, :order], unique: true, name: 'representation_id_order'
  end
end
