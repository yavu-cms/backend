class AddRepresentationTypeToClientRow < ActiveRecord::Migration
  def change
    add_column :client_rows, :representation_type, :string
  end
end
