class AddAvailableParametersToComponentService < ActiveRecord::Migration
  def change
    add_column :component_services, :available_parameters, :text
  end
end
