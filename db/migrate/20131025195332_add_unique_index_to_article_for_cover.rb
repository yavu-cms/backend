class AddUniqueIndexToArticleForCover < ActiveRecord::Migration
  def change
    change_table :article_for_covers do |t|
      t.index [:article_id, :client_application_id], unique: true
    end
  end
end
