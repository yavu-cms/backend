class RemoveLockVersionToArticle < ActiveRecord::Migration
  def change
    remove_column :articles, :lock_version
  end
end
