class DropDebugFromClientApplication < ActiveRecord::Migration
  def change
    remove_column :client_applications, :debug, :boolean
  end
end
