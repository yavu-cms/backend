class CreateClientTranslations < ActiveRecord::Migration
  def change
    create_table :client_translations do |t|
      t.string :locale
      t.text :values
      t.references :client_component, index: true

      t.timestamps
    end
  end
end
