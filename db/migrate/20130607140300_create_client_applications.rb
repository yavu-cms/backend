class CreateClientApplications < ActiveRecord::Migration
  def change
    create_table :client_applications do |t|
      t.string  :name,       limit: 50,     null: false
      t.string  :url,        limit: 255,    null: false
      t.string  :identifier, limit: 50,     null: false

      t.timestamps
    end

    add_index :client_applications, :identifier, unique: true
    add_index :client_applications, :name,       unique: true
    add_index :client_applications, :url,        unique: true
  end
end
