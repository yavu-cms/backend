class ChangeRepresentationIdOnRouteToNullable < ActiveRecord::Migration
  def change
    change_column :routes, :representation_id, :string, null: true
  end
end
