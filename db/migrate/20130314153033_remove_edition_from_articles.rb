class RemoveEditionFromArticles < ActiveRecord::Migration
  def change
    remove_reference :articles, :edition, index: true
  end
end
