class AddShowInMenuToShortcut < ActiveRecord::Migration
  def change
    add_column :shortcuts, :show_in_menu, :boolean, default: true
    remove_column :shortcuts, :priority, :integer
  end
end
