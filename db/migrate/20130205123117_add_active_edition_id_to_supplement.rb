class AddActiveEditionIdToSupplement < ActiveRecord::Migration
  def change
    add_reference :supplements, :active_edition, index: true
  end
end
