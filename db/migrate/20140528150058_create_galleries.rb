class CreateGalleries < ActiveRecord::Migration
  def change
    create_table :galleries do |t|
      t.string :title, null: false, unique: true, index: true
      t.string :type

      t.timestamps
    end
  end
end
