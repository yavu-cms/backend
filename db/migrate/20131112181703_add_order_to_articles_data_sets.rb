class AddOrderToArticlesDataSets < ActiveRecord::Migration
  def change
    add_column :articles_data_sets, :id, :primary_key
    add_column :articles_data_sets, :order, :integer, default: 0
  end
end
