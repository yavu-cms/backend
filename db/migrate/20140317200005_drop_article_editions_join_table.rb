class DropArticleEditionsJoinTable < ActiveRecord::Migration
  def change
    drop_join_table :articles, :editions
  end
end
