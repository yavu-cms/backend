class CreateExtraAttributeModels < ActiveRecord::Migration
  def change
    create_table :extra_attribute_models do |t|
      t.integer :model_id, null: false
      t.string  :model_type, null: false
      t.text    :values
      t.timestamps
    end
  end
end
