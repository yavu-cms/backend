class AddEditionDatabaseRestrictions < ActiveRecord::Migration
  def change
    change_column :editions, :supplement_id, :integer, null: false
  end
end
