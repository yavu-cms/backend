class AddDebugToClientApplication < ActiveRecord::Migration
  def change
    add_column :client_applications, :debug, :boolean, default: false
  end
end
