class AddCommentsEnabledToArticle < ActiveRecord::Migration
  def change
    add_column :articles, :comments_enabled, :boolean
  end
end
