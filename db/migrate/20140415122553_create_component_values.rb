class CreateComponentValues < ActiveRecord::Migration
  def change
    create_table :component_values do |t|
      t.references :component_field, index: true, null: false
      t.integer :order, default: 0, null: false
      t.integer :referable_id, null: false
      t.string :referable_type, null: false
    end
  end
end
