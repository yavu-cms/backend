class DropViewIdNotNullToComponentConfiguration < ActiveRecord::Migration
  def change
    change_column :component_configurations, :view_id, :integer, null: true, index: true
  end
end
