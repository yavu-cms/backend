class CreateRole < ActiveRecord::Migration
  def change
    create_table :roles do |t|
      t.string :name, null: false, unique: true, index: true
      t.text :permissions, null: true, unique: false, index: false
      t.boolean :enabled, null: false, unique: false, index: false, default: true

      t.timestamps
    end
  end
end
