class AddPriorityToRoute < ActiveRecord::Migration
  def change
    add_column :routes, :priority, :integer, default: 0
  end
end
