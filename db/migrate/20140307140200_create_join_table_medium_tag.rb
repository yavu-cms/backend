class CreateJoinTableMediumTag < ActiveRecord::Migration
  def change
    create_join_table :media, :tags do |t|
       t.index [:tag_id, :medium_id], unique: true
    end
  end
end
