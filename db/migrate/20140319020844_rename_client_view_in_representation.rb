class RenameClientViewInRepresentation < ActiveRecord::Migration
  def change
    rename_column :representations, :client_view_id, :view_id
  end
end
