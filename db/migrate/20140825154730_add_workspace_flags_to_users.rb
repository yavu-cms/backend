class AddWorkspaceFlagsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :all_client_applications, :boolean, default: false, null: false
    add_column :users, :all_sections, :boolean, default: false, null: false
  end
end
