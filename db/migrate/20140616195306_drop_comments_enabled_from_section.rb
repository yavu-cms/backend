class DropCommentsEnabledFromSection < ActiveRecord::Migration
  def change
    remove_column :sections, :comments_enabled, :boolean
  end
end
