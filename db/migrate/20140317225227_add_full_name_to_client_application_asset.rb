class AddFullNameToClientApplicationAsset < ActiveRecord::Migration
  def change
    add_column :client_application_assets, :full_name, :string
  end
end
