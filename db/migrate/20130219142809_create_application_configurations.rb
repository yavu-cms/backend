class CreateApplicationConfigurations < ActiveRecord::Migration
  def change
    create_table :application_configurations do |t|
      t.string :key
      t.string :value

      t.timestamps
    end
    add_index :application_configurations, :key, unique: true
    add_index :application_configurations, :value
  end
end
