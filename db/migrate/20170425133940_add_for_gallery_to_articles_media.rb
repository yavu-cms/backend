class AddForGalleryToArticlesMedia < ActiveRecord::Migration
  def change
    add_column :article_media, :for_gallery, :boolean, default: true
  end
end
