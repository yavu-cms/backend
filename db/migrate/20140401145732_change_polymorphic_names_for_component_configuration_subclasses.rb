class ChangePolymorphicNamesForComponentConfigurationSubclasses < ActiveRecord::Migration
  def change
    remove_column :component_configurations, :component_configuration_id, :integer
  end
end
