class AddEnabledToClientApplications < ActiveRecord::Migration
  def change
    add_column :client_applications, :enabled, :boolean, default: true, null: false
  end
end
