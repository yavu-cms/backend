class CreateEditions < ActiveRecord::Migration
  def change
    create_table :editions do |t|
      t.string      :name
      t.date        :date,        index: true
      t.boolean     :is_visible,  default: true
      t.boolean     :is_active,  default: false

      t.belongs_to  :supplement

      t.timestamps
    end
  end
end
