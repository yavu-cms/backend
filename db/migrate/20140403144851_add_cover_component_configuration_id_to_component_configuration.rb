class AddCoverComponentConfigurationIdToComponentConfiguration < ActiveRecord::Migration
  def change
    add_column :component_configurations, :cover_component_configuration_id, :integer, index: true
  end
end
