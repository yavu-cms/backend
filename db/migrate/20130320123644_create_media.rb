class CreateMedia < ActiveRecord::Migration
  def change
    create_table :media do |t|
      t.string :type
      t.string :file
      t.string :content
      t.string :small_version
      t.string :medium_version
      t.string :big_version

      t.timestamps
    end
  end
end
