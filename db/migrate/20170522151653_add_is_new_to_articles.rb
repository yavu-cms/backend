class AddIsNewToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :is_new, :boolean, default: false
  end
end
