class CreateSectionsUsersJoinTable < ActiveRecord::Migration
  def change
    create_join_table :sections, :users do |t|
      t.index [:section_id, :user_id], unique: true, name: 'section_id_user_id'
    end
  end
end
