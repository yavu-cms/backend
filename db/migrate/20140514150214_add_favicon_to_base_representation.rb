class AddFaviconToBaseRepresentation < ActiveRecord::Migration
  def change
    add_reference :base_representations, :favicon, index: true
  end
end
