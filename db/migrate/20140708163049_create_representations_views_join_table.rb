class CreateRepresentationsViewsJoinTable < ActiveRecord::Migration
  def change
    create_join_table :base_representations, :views do |t|
      t.index [:base_representation_id, :view_id], unique: true, name: 'base_rep_id_view_id'
    end
  end
end
