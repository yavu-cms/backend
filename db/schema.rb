# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170921141328) do

  create_table "application_configurations", force: true do |t|
    t.string   "key"
    t.string   "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "application_configurations", ["key"], name: "index_application_configurations_on_key", unique: true, using: :btree
  add_index "application_configurations", ["value"], name: "index_application_configurations_on_value", using: :btree

  create_table "article_media", force: true do |t|
    t.integer  "article_id",                  null: false
    t.integer  "medium_id",                   null: false
    t.boolean  "main"
    t.text     "caption"
    t.integer  "order"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "alt"
    t.string   "title"
    t.boolean  "only_cover",  default: false, null: false
    t.boolean  "for_gallery", default: true
  end

  add_index "article_media", ["article_id"], name: "index_article_media_on_article_id", using: :btree
  add_index "article_media", ["medium_id"], name: "index_article_media_on_medium_id", using: :btree

  create_table "article_relations", force: true do |t|
    t.integer "article_a_id", null: false
    t.integer "article_b_id", null: false
    t.text    "caption"
  end

  add_index "article_relations", ["article_a_id", "article_b_id"], name: "index_article_relations_on_article_a_id_and_article_b_id", unique: true, using: :btree

  create_table "articles", force: true do |t|
    t.string   "slug",                                               null: false
    t.text     "heading"
    t.text     "title",                                              null: false
    t.text     "lead"
    t.text     "body",            limit: 2147483647
    t.text     "signature"
    t.integer  "section_id",                                         null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "edition_id"
    t.boolean  "is_visible",                         default: true
    t.boolean  "hide_from_cover",                    default: false
    t.datetime "time"
    t.boolean  "is_new",                             default: false
  end

  add_index "articles", ["edition_id"], name: "index_articles_on_edition_id", using: :btree
  add_index "articles", ["section_id"], name: "index_articles_on_section_id", using: :btree

  create_table "articles_cover_articles_component_configurations", force: true do |t|
    t.integer  "article_id",                                            null: false
    t.integer  "cover_articles_component_configuration_id",             null: false
    t.integer  "order",                                     default: 0
    t.integer  "integer",                                   default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "view_id",                                               null: false
  end

  add_index "articles_cover_articles_component_configurations", ["article_id", "cover_articles_component_configuration_id"], name: "acacc_unique", unique: true, using: :btree
  add_index "articles_cover_articles_component_configurations", ["article_id"], name: "acacc_article_id", using: :btree
  add_index "articles_cover_articles_component_configurations", ["cover_articles_component_configuration_id"], name: "acacc_cover_articles_component_configuration_id", using: :btree
  add_index "articles_cover_articles_component_configurations", ["view_id"], name: "articles_cover_articles_cc_view", using: :btree

  create_table "articles_tags", id: false, force: true do |t|
    t.integer "article_id", null: false
    t.integer "tag_id",     null: false
  end

  add_index "articles_tags", ["tag_id", "article_id"], name: "index_articles_tags_on_tag_id_and_article_id", unique: true, using: :btree

  create_table "base_component_configurations", force: true do |t|
    t.integer  "component_id",                     null: false
    t.integer  "column_id"
    t.integer  "view_id"
    t.boolean  "hidden"
    t.integer  "order"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "description",                      null: false
    t.string   "type"
    t.integer  "cover_component_configuration_id"
  end

  add_index "base_component_configurations", ["column_id"], name: "index_base_component_configurations_on_column_id", using: :btree
  add_index "base_component_configurations", ["component_id"], name: "index_base_component_configurations_on_component_id", using: :btree
  add_index "base_component_configurations", ["view_id"], name: "index_base_component_configurations_on_view_id", using: :btree

  create_table "base_components", force: true do |t|
    t.string   "name",                       null: false
    t.text     "extras"
    t.text     "context"
    t.text     "description"
    t.text     "readme"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "type"
    t.string   "slug",                       null: false
    t.boolean  "enabled",     default: true
  end

  add_index "base_components", ["name"], name: "index_base_components_on_name", unique: true, using: :btree

  create_table "base_representations", force: true do |t|
    t.integer  "client_application_id",             null: false
    t.string   "name",                              null: false
    t.text     "configuration"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "type"
    t.integer  "representation_id"
    t.integer  "view_id"
    t.integer  "column_view_id"
    t.integer  "row_view_id"
    t.integer  "error_view_id",                     null: false
    t.integer  "favicon_id"
    t.integer  "lock_version",          default: 0, null: false
  end

  add_index "base_representations", ["client_application_id", "name"], name: "index_base_representations_on_client_application_id_and_name", unique: true, using: :btree
  add_index "base_representations", ["client_application_id", "name"], name: "unique_representation_per_ca", unique: true, using: :btree
  add_index "base_representations", ["client_application_id"], name: "index_base_representations_on_client_application_id", using: :btree
  add_index "base_representations", ["column_view_id"], name: "index_base_representations_on_column_view_id", using: :btree
  add_index "base_representations", ["favicon_id"], name: "index_base_representations_on_favicon_id", using: :btree
  add_index "base_representations", ["representation_id"], name: "index_base_representations_on_representation_id", using: :btree
  add_index "base_representations", ["row_view_id"], name: "index_base_representations_on_row_view_id", using: :btree
  add_index "base_representations", ["view_id"], name: "index_base_representations_on_view_id", using: :btree

  create_table "base_representations_users", id: false, force: true do |t|
    t.integer "base_representation_id", null: false
    t.integer "user_id",                null: false
  end

  add_index "base_representations_users", ["base_representation_id"], name: "index_base_representations_users_on_base_representation_id", using: :btree
  add_index "base_representations_users", ["user_id"], name: "index_base_representations_users_on_user_id", using: :btree

  create_table "base_representations_views", id: false, force: true do |t|
    t.integer "base_representation_id", null: false
    t.integer "view_id",                null: false
  end

  add_index "base_representations_views", ["base_representation_id", "view_id"], name: "base_rep_id_view_id", unique: true, using: :btree

  create_table "client_application_assets", force: true do |t|
    t.string   "type"
    t.string   "file"
    t.integer  "client_application_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "prefix_path",           default: ""
    t.string   "content_type"
  end

  add_index "client_application_assets", ["client_application_id", "type", "prefix_path", "file"], name: "client_application_file_unique", unique: true, using: :btree
  add_index "client_application_assets", ["client_application_id"], name: "index_client_application_assets_on_client_application_id", using: :btree

  create_table "client_application_assets_base_representations", force: true do |t|
    t.integer "client_application_asset_id"
    t.integer "base_representation_id"
  end

  add_index "client_application_assets_base_representations", ["base_representation_id", "client_application_asset_id"], name: "index_client_application_assets_representations", unique: true, using: :btree

  create_table "client_applications", force: true do |t|
    t.string   "name",                  limit: 50,                null: false
    t.string   "url",                                             null: false
    t.string   "identifier",            limit: 50,                null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "enabled",                          default: true, null: false
    t.string   "assets_url"
    t.string   "media_url"
    t.integer  "favicon_id"
    t.string   "route_cache_control"
    t.string   "service_cache_control"
    t.integer  "logo_id"
    t.text     "external_varnish_urls"
    t.text     "internal_varnish_urls"
    t.integer  "news_source_id"
  end

  add_index "client_applications", ["favicon_id"], name: "index_client_applications_on_favicon_id", using: :btree
  add_index "client_applications", ["identifier"], name: "index_client_applications_on_identifier", unique: true, using: :btree
  add_index "client_applications", ["logo_id"], name: "index_client_applications_on_logo_id", using: :btree
  add_index "client_applications", ["name"], name: "index_client_applications_on_name", unique: true, using: :btree
  add_index "client_applications", ["news_source_id"], name: "index_client_applications_on_news_source_id", using: :btree
  add_index "client_applications", ["url"], name: "index_client_applications_on_url", unique: true, using: :btree

  create_table "client_applications_users", id: false, force: true do |t|
    t.integer "client_application_id", null: false
    t.integer "user_id",               null: false
  end

  add_index "client_applications_users", ["client_application_id", "user_id"], name: "capp_id_user_id", unique: true, using: :btree

  create_table "columns", force: true do |t|
    t.integer  "row_id",        null: false
    t.integer  "order",         null: false
    t.text     "configuration"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "view_id"
  end

  add_index "columns", ["view_id"], name: "index_columns_on_view_id", using: :btree

  create_table "component_assets", force: true do |t|
    t.string   "type",         null: false
    t.string   "file",         null: false
    t.integer  "component_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "content_type"
  end

  add_index "component_assets", ["component_id", "file", "type"], name: "index_component_assets_on_component_id_and_file_and_type", unique: true, using: :btree
  add_index "component_assets", ["component_id"], name: "index_component_assets_on_component_id", using: :btree

  create_table "component_fields", force: true do |t|
    t.integer "base_component_id"
    t.integer "base_component_configuration_id"
    t.string  "description"
    t.string  "name",                            null: false
    t.string  "type"
    t.text    "default"
    t.string  "model_class"
  end

  add_index "component_fields", ["base_component_configuration_id"], name: "csf_cconfig_id", using: :btree
  add_index "component_fields", ["base_component_id"], name: "csf_component_id", using: :btree

  create_table "component_services", force: true do |t|
    t.string   "http_method",          null: false
    t.string   "name",                 null: false
    t.text     "body"
    t.integer  "component_id",         null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "view_id",              null: false
    t.text     "available_parameters"
    t.string   "cache_control"
  end

  add_index "component_services", ["http_method", "name", "component_id"], name: "index_component_services_on_method_and_name_and_component_id", unique: true, using: :btree
  add_index "component_services", ["view_id"], name: "index_component_services_on_view_id", using: :btree

  create_table "component_translations", force: true do |t|
    t.string   "locale",       null: false
    t.text     "values"
    t.integer  "component_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "component_translations", ["component_id", "locale"], name: "index_component_translations_on_component_id_and_locale", unique: true, using: :btree
  add_index "component_translations", ["component_id"], name: "index_component_translations_on_component_id", using: :btree

  create_table "component_values", force: true do |t|
    t.integer "component_field_id",             null: false
    t.integer "order",              default: 0, null: false
    t.integer "referable_id",                   null: false
    t.string  "referable_type",                 null: false
  end

  add_index "component_values", ["component_field_id"], name: "index_component_values_on_component_field_id", using: :btree

  create_table "editions", force: true do |t|
    t.string   "name"
    t.date     "date"
    t.boolean  "is_visible",    default: true
    t.boolean  "is_active",     default: false
    t.integer  "supplement_id",                 null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "extra_attribute_models", force: true do |t|
    t.integer  "model_id",   null: false
    t.string   "model_type", null: false
    t.text     "values"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "extra_attributes", force: true do |t|
    t.string   "name",           null: false
    t.string   "description"
    t.string   "attribute_type", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "model_classes"
  end

  create_table "galleries", force: true do |t|
    t.string   "title",      null: false
    t.string   "type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "media", force: true do |t|
    t.string   "type"
    t.string   "file"
    t.text     "content"
    t.string   "small_version"
    t.string   "medium_version"
    t.string   "big_version"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.date     "date"
    t.string   "credits"
    t.text     "dimensions"
    t.string   "xbig_version"
    t.string   "xsmall_version"
  end

  add_index "media", ["date"], name: "index_media_on_date", using: :btree
  add_index "media", ["name"], name: "index_media_on_name", using: :btree

  create_table "media_tags", id: false, force: true do |t|
    t.integer "medium_id", null: false
    t.integer "tag_id",    null: false
  end

  add_index "media_tags", ["tag_id", "medium_id"], name: "index_media_tags_on_tag_id_and_medium_id", unique: true, using: :btree

  create_table "medium_media_galleries", force: true do |t|
    t.integer  "media_gallery_id"
    t.integer  "medium_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "order",            default: 0
  end

  add_index "medium_media_galleries", ["media_gallery_id"], name: "index_medium_media_galleries_on_media_gallery_id", using: :btree
  add_index "medium_media_galleries", ["medium_id"], name: "index_medium_media_galleries_on_medium_id", using: :btree

  create_table "news_sources", force: true do |t|
    t.string  "name",                       null: false
    t.boolean "is_default", default: false, null: false
  end

  create_table "newsletter_generated_data", force: true do |t|
    t.string   "slug",          null: false
    t.text     "content",       null: false
    t.integer  "newsletter_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "newsletter_generated_data", ["slug"], name: "index_newsletter_generated_data_on_slug", unique: true, using: :btree

  create_table "newsletters", force: true do |t|
    t.string   "name",                                 null: false
    t.text     "generator",                            null: false
    t.boolean  "enabled",               default: true
    t.integer  "expiration",                           null: false
    t.integer  "client_application_id",                null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "context"
    t.string   "frequency"
    t.string   "subject"
  end

  add_index "newsletters", ["name"], name: "index_newsletters_on_name", unique: true, using: :btree

  create_table "roles", force: true do |t|
    t.string   "name",                       null: false
    t.text     "permissions"
    t.boolean  "enabled",     default: true, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "roles_users", id: false, force: true do |t|
    t.integer "role_id", null: false
    t.integer "user_id", null: false
  end

  add_index "roles_users", ["role_id", "user_id"], name: "role_id_user_id", unique: true, using: :btree

  create_table "routes", force: true do |t|
    t.string   "name",                                  null: false
    t.string   "pattern",                               null: false
    t.integer  "type_id"
    t.integer  "client_application_id",                 null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "redirect_url"
    t.text     "constraints"
    t.integer  "priority",              default: 0
    t.integer  "representation_id"
    t.string   "type"
    t.string   "cache_control"
    t.boolean  "default_route",         default: false, null: false
  end

  add_index "routes", ["client_application_id", "name"], name: "cli_app_route_name", unique: true, using: :btree
  add_index "routes", ["client_application_id", "pattern"], name: "cli_app_route_pattern", unique: true, using: :btree
  add_index "routes", ["client_application_id"], name: "index_routes_on_client_application_id", using: :btree
  add_index "routes", ["pattern"], name: "index_routes_on_pattern", using: :btree
  add_index "routes", ["representation_id"], name: "index_routes_on_representation_id", using: :btree

  create_table "rows", force: true do |t|
    t.integer  "representation_id", null: false
    t.integer  "order",             null: false
    t.text     "configuration"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "view_id"
  end

  add_index "rows", ["view_id"], name: "index_rows_on_view_id", using: :btree

  create_table "sections", force: true do |t|
    t.string   "name",                           null: false
    t.string   "slug",                           null: false
    t.boolean  "is_visible",      default: true, null: false
    t.integer  "supplement_id",                  null: false
    t.integer  "parent_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "main_article_id"
  end

  add_index "sections", ["main_article_id"], name: "index_sections_on_main_article_id", using: :btree
  add_index "sections", ["parent_id"], name: "index_sections_on_parent_id", using: :btree
  add_index "sections", ["supplement_id"], name: "index_sections_on_supplement_id", using: :btree

  create_table "sections_users", id: false, force: true do |t|
    t.integer "section_id", null: false
    t.integer "user_id",    null: false
  end

  add_index "sections_users", ["section_id", "user_id"], name: "section_id_user_id", unique: true, using: :btree

  create_table "settings", force: true do |t|
    t.string   "var",                   null: false
    t.text     "value"
    t.integer  "thing_id"
    t.string   "thing_type", limit: 30
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "settings", ["thing_type", "thing_id", "var"], name: "index_settings_on_thing_type_and_thing_id_and_var", unique: true, using: :btree

  create_table "shortcuts", force: true do |t|
    t.integer  "user_id",                     null: false
    t.string   "title",                       null: false
    t.string   "url",                         null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "show_in_menu", default: true
  end

  create_table "supplements", force: true do |t|
    t.string   "name",                              null: false
    t.boolean  "is_default",        default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "active_edition_id"
    t.integer  "logo_id"
    t.integer  "news_source_id"
  end

  add_index "supplements", ["active_edition_id"], name: "index_supplements_on_active_edition_id", using: :btree
  add_index "supplements", ["logo_id"], name: "index_supplements_on_logo_id", using: :btree
  add_index "supplements", ["news_source_id"], name: "index_supplements_on_news_source_id", using: :btree

  create_table "tags", force: true do |t|
    t.string   "name",                        null: false
    t.string   "slug",                        null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "combiner_id"
    t.boolean  "hidden",      default: false
  end

  add_index "tags", ["combiner_id"], name: "index_tags_on_combiner_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "username",                default: "",    null: false
    t.string   "email",                   default: "",    null: false
    t.string   "encrypted_password",      default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",           default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "all_client_applications", default: false, null: false
    t.boolean  "all_sections",            default: false, null: false
    t.boolean  "all_representations",     default: false, null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["username"], name: "index_users_on_username", unique: true, using: :btree

  create_table "version_associations", force: true do |t|
    t.integer "version_id"
    t.string  "foreign_key_name", null: false
    t.integer "foreign_key_id"
  end

  add_index "version_associations", ["foreign_key_name", "foreign_key_id"], name: "index_version_associations_on_foreign_key", using: :btree
  add_index "version_associations", ["version_id"], name: "index_version_associations_on_version_id", using: :btree

  create_table "versions", force: true do |t|
    t.string   "item_type",                         null: false
    t.integer  "item_id",                           null: false
    t.string   "event",                             null: false
    t.string   "whodunnit"
    t.text     "object",         limit: 2147483647
    t.datetime "created_at"
    t.integer  "transaction_id"
  end

  add_index "versions", ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree
  add_index "versions", ["transaction_id"], name: "index_versions_on_transaction_id", using: :btree

  create_table "views", force: true do |t|
    t.string   "name",                               null: false
    t.text     "value",                              null: false
    t.integer  "component_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_layout",    default: false,       null: false
    t.string   "format",       default: "text/html", null: false
    t.boolean  "default",      default: false
  end

  add_index "views", ["component_id"], name: "index_views_on_component_id", using: :btree

end
