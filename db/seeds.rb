# encoding: utf-8
# Initialize default settings

# NOT LOAD IF THERE ARE SOMETHING IN DATABASE
if Role.find_by(name: 'Super admin')
  puts "There are something in the DB. Therefore, the seed file will not be loaded."
  exit 1
end

## Articles
Setting['articles.autotag'] = 'yes'

## Pagination
Setting['pagination.per_page'] = 20

## Search
Setting['search.limit'] = 20

## Media versions
Setting['media_versions.xbig']   = '932x932'
Setting['media_versions.big']    = '576x576'
Setting['media_versions.medium'] = '392x392'
Setting['media_versions.small']  = '272x272'
Setting['media_versions.xsmall'] = '212x212'

## Media processing
Setting['media_processing.quality']    = 75
Setting['media_processing.resolution'] = 72

## Disqus
Setting['disqus.base_url']        = 'disqus.com/api'
Setting['disqus.api_version']     = '3.0'
Setting['disqus.forum_shortname'] = 'yavu-cms'
Setting['disqus.public_key']      = 'PublicKeyPlease'
Setting['disqus.secret_key']      = 'SecretKeyPlease'
Setting['disqus.locale']          = 'es_ES'

## Weather
Setting['weather.url_to_scrap']       = 'http://www.smn.gov.ar/?mod=pron&id=4&provincia=Buenos%20Aires&ciudad=La%20Plata'
Setting['weather.frequency']          = '*/20 * * * *'
Setting['weather.records_keep_limit'] = 10

## SEO
Setting['defaults_seo.title']       = 'Yavu - CMS'
Setting['defaults_seo.description'] = 'Yavu is a CMS developed by UNLP'

# Check if some hardcored migrations inserted very-dependent data
# TODO: Clean theses migrations because it should not to stay here, in this generic
# version of the CMS
ns = NewsSource.find_by name: 'Yavu', is_default: true
ns.destroy if ns

# Create basic roles
role_super_admin = Role.create! name: 'Super admin', permissions: %w[ super_admin ]

# Create an administrator user
User.create! username: 'admin', new_password: 'yavu!', email: 'admin@example.net',
  roles: [role_super_admin], all_client_applications: true, all_sections: true

# Create news source
ns = NewsSource.find_or_create_by! name: 'Default', is_default: true

# Create a default client application
c = ClientApplication.create! name: 'Frontend app', url: 'http://localhost:9292', enabled: true, news_source: ns

s = Supplement.create! name: 'Default', is_default: true, news_source: ns
Edition.create! name: 'Default', supplement: s
Edition.last.become_active!

# Creating some basic extra attributes
ExtraAttribute.create! name: 'open_in_new_tab', description: 'Abrir en nueva pestaña', attribute_type: 'boolean', model_classes: { 'article' => '1' }
ExtraAttribute.create! name: 'redirect_to_external_link', description: 'Redireccionar a sitio externo', attribute_type: 'string', model_classes: { 'article' => '1' }
ExtraAttribute.create! name: 'color', description: 'Color/identidad visual', attribute_type: 'color', model_classes: { 'section' => '1' }
ExtraAttribute.create! name: 'priority', description: 'Prioridad', attribute_type: 'string', model_classes: { 'section' => '1' }

# Resolve integration between client app & supplement in news_source
ns.client_applications << c
ns.supplements << s

