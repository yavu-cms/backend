# This file is used by Rack-based servers to start the application.

require 'faye'
require 'faye/redis'
require 'redis/namespace'

#= Redis URL
uri = URI.parse(ENV["REDIS_URL"] || 'redis://localhost:6379/2')
raise "Redis url incomplete." unless uri.host && uri.port

#= Redis Connection
redis_connection = {host: uri.host, port: uri.port, password: uri.password}
$redis           = Redis::Namespace.new 'yavu.faye', redis: Redis.new(redis_connection)

#= Faye logging
Faye.logger = Logger.new(STDOUT)
Faye.logger.level = Logger.const_get(ENV['LOG_LEVEL'].upcase) rescue Logger::INFO

#= Faye App
Faye::WebSocket.load_adapter('thin')
app = Faye::RackAdapter.new(mount: '/wsock', timeout: 30, engine: { type: Faye::Redis, host: uri.host, port: uri.port })

run app
