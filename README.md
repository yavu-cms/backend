# Guia de instalación - Yavu CMS

Yavu CMS consta de diferentes componentes, cada una de las cuales podría instalarse en servidores separados o en un mismo servidor. Para asegurar la instalación, se aconseja que cada componente instalada, emplee usuarios de sistema diferente, sobre todo cuando todas las componentes se instalan en un mismo servidor

Las componentes son:

  * Frontend
  * Backend

## Instalación usando Capistrano

[Capistrano](https://github.com/capistrano/capistrano) es un producto desarrollado para simplificar la tarea de instalar y actualizar aplicaciones. Para ello debe entenderse el concepto básico detrás de la filosofía propuesta por Capistrano.
Con Capistrano existirán dos entidades: el *servidor* y la *estación de trabajo* del administrador.

### Capistrano: El servidor

El servidor será quien finalmente instalará las componentes de la aplicación desarrollada. Este servidor debe cumplir los prerrequisitos básicos (esta guía pretende indicar cómo instalar estos prerrequisitos) para luego poder instalar las componentes de la aplicación.

### Capistrano: La estación de trabajo del administrador

La estación de trabajo del administrador deberá disponer de acceso por medio de ssh al servidor con el usuario con el que correrá cada componente de nuestra aplicación.  En esta máquina deberá instalarse la gema *capistrano*. Capistrano es una gema de ruby, por lo cuál la instalación y configuración de la estación de trabajo tendrá un apartado específico.

# Instalación del Servidor

## Precondiciones

* **TODO: cambiar upstart por systemd**

Se recomienda utilizar un sistema Operativo GNU Linux que soporte [Upstart](http://upstart.ubuntu.com/cookbook/) como por ejemplo:

  * Ubuntu
  * Fedora
  * RedHat

Otras distribuciones lo incluyen de forma opcional como por ejemplo:

  * Debian
  * OpenSUSE

Más información en [este enlace](http://en.wikipedia.org/wiki/Upstart#Adoption).  La siguiente guía indicará las instrucciones para preparar un servidor Ubuntu 12.04 LTS El usuario que instalará el servidor deberá disponer de acceso privilegiado (con password de sudoers)

## Preparando el servidor para instalar aplicaciones Ruby

### Instalación de fuentes de paquetes necesaria

El proyecto del backend depende de varias librerías y servicios, algunos de los cuales dependen de versiones específicas que son más actuales que las provistas por los manejadores de paquetes del sistema operativo subyacente.

Cabe destacar que los paquetes en cuestión son:

* Redis server version 2.0 o superior
* MongoDB version 2.2 o superior
* Elasticsearch version 1.2 o superior


### Instalando MongoDB

[MongoDB](http://www.mongodb.org/) es una base de datos NoSQL. Dado que los paquetes incluidos en la distribución se encuentran desactualizados respecto de la dependencia del proyecto, se debe instalar la última versión disponible.

#### Ubuntu 12.04

Agregamos la clave para mongodb:

```bash
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
```

Agregamos la fuente de paquetes:

```bash
echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen'\
 | sudo tee /etc/apt/sources.list.d/mongodb.list
```

Actualizamos las definiciones de paquetes del sistema:

```bash
sudo apt-get update
```

* **NOTA: Considerar que el primer comando: apt-key, utiliza el puerto 11371**


### Instalando Elasticsearch

[Elasticsearch](http://elasticsearch.org/) es un motor de indexación para búsquedas *full text* basado en Apache Lucene.

#### Ubuntu 12.04

Agregamos la clave para el repositorio de elasticsearch:

```bash
wget -O - http://packages.elasticsearch.org/GPG-KEY-elasticsearch | sudo apt-key add -
```

Agregamos el repositorio:

```bash
echo 'deb http://packages.elasticsearch.org/elasticsearch/1.2/debian stable main'\
 | sudo tee /etc/apt/sources.list.d/elasticsearch.list
```

Actualizamos las definiciones de paquetes del sistema:

```bash
sudo apt-get update
```

* **NOTA: Considerar que el primer comando: apt-key, utiliza el puerto 11371**


### Instalar los paquetes necesarios:

```bash
sudo apt-get install build-essential libssl-dev lsb-release lsb-core nodejs libreadline-dev zlib1g-dev bison \
libmagickwand-dev libxml2-dev libxslt1-dev git-core git openssh-server vim redis-server memcached mongodb-10gen \
libmysqlclient-dev openjdk-7-jre mysql-server elasticsearch optipng jpegoptim
```

**NOTA: el paquete `jpegoptim` en Ubuntu 12.04 no soporta la conversión a JPEG progessive, por lo que se recomienda desinstalar el paquete de la distribución y descargar la [versión 1.4 del producto](http://www.kokkonen.net/tjko/src/jpegoptim-1.4.1.tar.gz) compilarla e instalarla en `/usr/local/bin`**

### Resumen de los paquetes instalados

Los paquetes considerados en las instrucciones previas ofrecen:

  * Herramientas de compilación empleadas para la instalación posterior de ruby y herramientas necesarias por gemas empleadas
  * Servicios
    * Mysql Server
    * MongoDB
    * Elasticsearch
    * Redis
    * Memcached: *opcional*
    * SSH
  * NodeJS

## Configuración del servidor

### Creación de usuarios

El proyecto desarrollado requiere de diferentes usuarios para cada componente.  Asumimos que la componente del frontend correrá con el usuario frontend y la componente de backend correrá con el usuario backend.

Entonces creamos los usuarios:

```bash
sudo useradd -m -s /bin/bash frontend
sudo useradd -m -s /bin/bash backend
```

Para cada usuario seteamos una contraseña:

```bash
sudo passwd frontend
sudo passwd backend
```

Para cada usuario generamos sus claves ssh:

```bash
sudo su - frontend -c 'ssh-keygen -N "" -f /home/frontend/.ssh/id_rsa'
sudo su - backend -c 'ssh-keygen -N "" -f /home/backend/.ssh/id_rsa'
```

Estas claves generadas, permitirán al administrador poder conectarse desde su
estación de trabajo sin requerir contraseña.

### Preparando el espacio para upstart

* **TODO: cambiar por systemd**

Para que las componentes de las aplicaciones puedan escribir su configuración de upstart en el directorio /etc/init crearemos dos directorios con el nombre de los usuarios:

```bash
sudo mkdir /etc/init/backend /etc/init/frontend
sudo chown backend /etc/init/backend
sudo chown frontend /etc/init/frontend
```

Luego permitimos que el usuario backend y el usuario frontend puedan hacer sudo backend/application start|restart|stop agregando la siguiente configuración en /etc/sudoers:

```bash
backend  ALL=(ALL) NOPASSWD:/sbin/start backend/application
backend  ALL=(ALL) NOPASSWD:/sbin/stop backend/application
backend  ALL=(ALL) NOPASSWD:/usr/sbin/service backend/application *
backend  ALL=(ALL) NOPASSWD:/sbin/restart backend/application
frontend ALL=(ALL) NOPASSWD:/sbin/start frontend/application
frontend ALL=(ALL) NOPASSWD:/sbin/stop frontend/application
frontend ALL=(ALL) NOPASSWD:/usr/sbin/service frontend/application *
frontend ALL=(ALL) NOPASSWD:/sbin/restart frontend/application
```

### Configuración de la base de datos

Creamos una base datos para el proyecto. Asumimos que la base de datos se llama yavu:

```bash
mysql -uroot -p
mysql> create database yavu;
mysql> grant all privileges on yavu.* to yavu@localhost identified by 'yavupass';
```

### Observaciones

Notamos que no instalamos ruby en ningún momento. Esto se debe a que con Capistrano se automatiza esta tarea, así como también crear los scripts de inicio de la aplicación en el sistema.

Sin embargo, hay una instancia donde podremos definir determinadas particularidades de ésta instalación, donde por ejemplo definiremos entre otras cosas:

  * Configuración a la base de datos
  * Configuración del puerto donde atenderá la aplicación
  * Configuración de parámetros específicos de la aplicación

Esta instancia se explicará en el apartado siguiente


## Estación de trabajo del administrador

### Precondiciones

La siguiente guía indicará las instrucciones para preparar una estación basada en Ubuntu 12.04 o superior.

El usuario que instalará la estación de trabajo deberá disponer de acceso privilegiado (con password de sudoers).

Se recomienda correr todos los pasos como un usuario común, y nunca utilizar el usuario root.

### Para hacer más cómodo el trabajo

Asumiendo que ya es posible conectarse al servidor utilizando ssh con los usuarios frontend y backend, a continuación se indica cómo conectarse al servidor vía ssh sin solicitar contraseña desde la estación de trabajo.
Debemos correr los siguientes comandos, asumiendo que el nombre o IP del servidor es IP_NOMBRE_SERVER :

```bash
ssh-copy-id frontend@IP_NOMBRE_SERVER
ssh-copy-id backend@IP_NOMBRE_SERVER
```

Luego de ejecutar estos comandos, verificamos que la conexión ssh con el
servidor con los usuarios frontend y backend no requieren contraseña.

### Instalando los paquetes necesarios


Los mismos paquetes indicados en la sección de prerequisitos

#### Instalando ruby usando rbenv

Los pasos pueden leerse directamente desde [rbenv](https://github.com/sstephenson/rbenv)

```bash
git clone https://github.com/sstephenson/rbenv.git ~/.rbenv
```

En ubuntu se usa ~/.bashrc, pero en otros sistemas es ~/.bash_profile

```bash
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(rbenv init -)"' >> ~/.bashrc
```

Al finalizar, abrir una nueva terminal y verificar si todo se instaló de forma correcta.

```bash
type rbenv
=> "rbenv is a function"
```

Luego es necesario instalar un plugin de rbenv, llamado ruby-build que agrega el comando rbenv install:

```bash
git clone https://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build
```

Los siguientes comandos instalarán y setearán por defecto la versión de Ruby 2.0.0-p481

```bash
rbenv install 2.0.0-p481
rbenv global 2.0.0-p481
```

* **NOTA: el proyecto dispone de un archivo .ruby-version que obliga disponer de la versión de ruby indicada por el mismo proyecto. En este caso, al ingresar al directorio, rbevn nos indicará que debemos instalar la versión de ruby especificada, para lo cuál se utilizarán los mismos comandos especificados arriba pero con la versión de ruby que indique el proyecto**

### Verificamos que la instalación de ruby ha funcionado bien.

```bash
ruby -v
```

El comando anterior debe devolver algo similar a esto: `ruby 2.0.0p481 (2014-05-08 revision 45883) [x86_64-linux]`

# Instalando la componente del Backend

En la estación de trabajo del administrador, clonamos el proyecto del backend
con el siguiente comando:

```bash
git clone https://git.cespi.unlp.edu.ar/desarrollo/el-dia-backend.git
cd backend
bundle
```

## Configuración del Backend

La configuración completa del producto depende de:

* Configuración global del producto: `config/settings.yml`
* Configuración de la base de datos: `config/database.yml`
* Meta configuraciones a partir de variables de ambiente: `.env`
  * Servicios:
  * Configuración del servidor WEB: `config/puma-config.rb`
  * Configuración de tareas asíncronas: `config/sidekiq.yml`
* Configuración de inicio de los servicios: `Procfile`

Todas las configuraciones del producto consideran que se trabaje en algún *entorno*. Los entornos son ambientes que se corresponden generalmente a:

  * **production**: el ambiente definitivo que se encuentra en producción.  Generalmente se obtiene mayor performance en este ambiente
  * **test**: ambiente utilizado para correr los tests
  * **development**: el ambiente utilizado durante el desarrollo. Generalmente los logs son más abultados y descriptivos, pero la performance es baja

### Configuración de la base de datos

La base de datos se configura editando el archivo: `config/database.yml`. Este archivo considera una configuración diferente para cada ambiente.


A continuación se da como ejemplo la configuración para MySQL

```yml
development:
  adapter:  mysql2
  encoding: utf8
  database: yavu_backend_development
  pool:     5
  username: root
  password:
  host:     localhost

test:
  adapter:  mysql2
  encoding: utf8
  database: yavu_backend_test
  pool:     5
  username: root
  password:
  host:     localhost

production:
  adapter:  mysql2
  encoding: utf8
  database: yavu_backend_production
  pool:     5
  username: root
  password:
  host:     localhost
```

### Configuración global

La configuración del global se realiza completamente desde el archivo `config/settings.yml`. Este archivo es autodescriptivo y contiene valores por defecto para todo los valores configurables de la aplicación

Para configurar distintos parámetros para cada ambiente, se recomienda **no editar** el archivo mencionado en el párrafo anterior, sino crear un archivo bajo el directorio `config/settings` con el nombre del ambiente. Por ejemplo, para el ambiente *production*, es posible *pisar* valores por defecto, creando el archivo: `config/settings/production.yml` con el contenido particular de aquellos valores a definir. No es necesario especificar los valores que no cambian, sólo especificaremos los cambios.

Por ejemplo:

```yml
mongoid:
  # Configure available database sessions. (required)
  sessions:
    # Defines the default session. (required)
    default:
      # Defines the name of the default database that Mongoid can connect to.
      # (required).
      database: yavu_backend_accounting
```

Pisaría sólo la configuración del nombre de la base de datos mongo

### Variables de ambiente

Es necesario setear algunas variables de ambiente para así poder correr la aplicación de forma adecuada. El archivo de configuración `config/settings.yml` utiliza algunas de estas variables directamente desde ruby usando `ENV['var_name']`

Al momento las necesarias son:

Para setear estas variables se promueve el uso de [dotenv](https://github.com/bkeepers/dotenv) que permite setearlas desde un archivo en la raíz del proyecto llamado `.env` (ver que en el proyecto se provee un archivo `.env-sample`). Por ejemplo:

```bash
PORT=3000
RACK_ENV=development
REDIS_NAMESPACE=yavu-backend

REDIS_CLIENT_FRONTEND_CHANNEL=client-frontend-channel
REDIS_CLIENT_FRONTEND_HEARTBEAT_NAMESPACE=heartbeat-frontend-server
```

## Configuración del deploy con capistrano

La configuración de capistrano se realiza mediante el archivo `config/deploy.rb` pero además, se personaliza la configuración de diferentes servidores usando archivos en el directorio `config/deploy`

Asumimos que el servidor a instalar se llama production, entonces creamos `config/deploy/production.rb` con el siguiente contenido:

```ruby
set :application, 'backend'
set :deploy_to,   "/home/#{application}"
set :user,        application
set :branch,      "development"
set :domain,      'IP_SERVER'

server domain, :app, :web, :db, :primary => true
```

Una vez creado el archivo, podremos avanzar el deploy con capistrano

## Usando capistrano

Capistrano se utiliza desde el directorio que clonamos anteriormente, de la siguiente forma: `bundle exec cap ...`

En realidad, el comando cap posee varias opciones que pueden verse con el siguiente comando:

```
bundle exec cap -T
```

A los comandos devueltos, lo debemos preceder con el nombre del ambiente, en nuestro caso production, por ejemplo, para correr deploy:setup, deberíamos usar:

```
bundle exec cap production deploy:setup
```

### Setup inicial con capistrano

Corremos:

```bash
bundle exec cap production deploy:setup
```

Esto instalará las dependencias en el servidor y creará una estructura de directorios en donde hayamos especificado que se instale la aplicación con la siguiente estructura:

```bash
+ ROOT
    |--- releases/
    |--+ shared/
        |--+ assets/
        |--+ data/
        |--+ files/
        |--+ frontend/
        |--+ log/
        |--+ tmp/
```

Lo que hará capistrano, es descargar la última versión bajo el directorio releases/ y al finalizar creará links simbólicos a las configuraciones y directorios en el directorio shared/

Cada directorio se usa para:

  * **assets:** assets compilados para la aplicación del backend
  * **data:** datos de configuración propios de cada cliente (frontend) configurado desde el backend
  * **files:** archivos compartidos entre releases como configuraciones de bases de datos, configuración global, etc
  * **frontend:** assets y medios de cada cliente (frontend). De aquí se deberían servir estáticamente estos archivos
  * **log:** logs de la aplicación
  * **tmp:** directorio temporal donde se almacenan los PIDS de los servicios, así como archivos que componen la cache del backend

Al finalizar el comando `cap production deploy:setup` debemos subir por única vez dos archivos:

```bash
scp config/database.yml-sample backend@IP_SERVER:/home/backend/shared/files/config/database.yml
scp config/settings/production.yml backend@IP_SERVER:/home/backend/shared/files/config/settings/production.yml
scp config/puma-config.rb backend@IP_SERVER:/home/backend/shared/files/config/puma-config.rb
scp config/sidekiq.yml backend@IP_SERVER:/home/backend/shared/files/config/sidekiq.yml
scp .env-sample backend@IP_SERVER:/home/backend/shared/.env
```

Nos conectamos al servidor y editamos las configuraciones de base de datos y global.

Luego editamos ambos archivos en el servidor según la configuración realizada para la base de datos y la configuración del sistema.


Finalmente editamos el archivo shared/.env personalizando su contenido:

```bash
PORT=3000
RACK_ENV=production
REDIS_NAMESPACE=yavu-backend

REDIS_CLIENT_FRONTEND_CHANNEL=client-frontend-channel
REDIS_CLIENT_FRONTEND_HEARTBEAT_NAMESPACE=heartbeat-frontend-server
```

Recordar que todos los comandos anteriores se corren en el servidor con el usuario backend.

### Instalación final

Corremos en la estación de trabajo del administrador:

```bash
bundle exec cap production deploy
```

* **NOTA: hasta acá el sistema quedó instalado en la última versión, pero aun no se ha creado la base de datos e insertado dato alguno.**

### Aplicando cambios en la base de datos

En caso de ser la primer instalación, o cuando se realicen cambios en la aplicación y la base de datos cambie, se debe correr:

```bash
bundle exec cap production deploy:migrations
```

Sólo si queremos inicializar la base de datos con datos de prueba, deberíamos correr en el servidor (conectarse con ssh backend@IP_SERVER):

```bash
cd current
RAILS_ENV=production bundle exec rake db:seed
```

y si queremos cargar las componentes visuales del sitio:

```bash
cd current
RAILS_ENV=production bundle exec rake components:load_all[var/extensions]
```

* **TODO: el direcotrio var/extensions debe cambiar a otro más representativo**

Finalmente podremos acceder al servidor con [http://IP_SERVER:3000](http://ip_server:3000)
El usuario por defecto es admin con password *yavu!*


# Configuración de los servicios Ruby

El backend provee dos servicios:

* Web server basado en Unicorn
* Gestor de tareas asíncronas usando Sidekiq
* Gestor de web sockets basado en Faye y Thin

## El servidor WEB

El servidor ruby empleado es [Unicorn](http://unicorn.bogomips.org), y la configuración del  backend puede cambiarse editando `config/puma-config.rb`

## El servidor sidekiq

La configuración de sidekiq se realiza editando `config/sidekiq.yml`

## Todos los servicios 

Todos los servicios se inician usando foreman según se indica en el archivo `Procfile`

# Integración con Frontend

Seguir la [documentación del proyecto](https://git.cespi.unlp.edu.ar/desarrollo/yavu-frontend-sample/tree/development)

# Compresión de las respuestas

Por defecto la aplicación utiliza **Rack::Deflater** configurado en `config/application.rb`. Si se desea utilizar otro mecanismo, como por ejemplo utilizar un web server u otra herramienta para comprimir los resultados, tenga en cuenta deshabilitarlo desde esta opción
