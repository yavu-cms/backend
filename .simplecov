# Custom options and filters for SimpleCov
require 'simplecov-console'
require 'simplecov-rcov'
require 'colorize'

# Aim high :)
MINIMUM_COVERAGE_PERCENT = 90

class SimpleCov::Formatter::AccurateFormatter
  def format(result)
    return if ENV['TEST'].blank?

    test_file = ENV['TEST']
    src_path = test_path_to_source(test_file)

    return if src_path.blank?

    info = result.files.detect do |f|
      relativize(f.filename) == src_path
    end

    return unless info

    colorize(
      "\nTests cover %6.2f%% of %s\n" % [info.covered_percent, relativize(info.filename)],
      info.covered_percent
    )
  end

  protected

  def colorize(str, percent)
    puts str.colorize(decide_color(percent))
  end

  def decide_color(percent)
    case percent
    when 90..100
      :green
    when MINIMUM_COVERAGE_PERCENT...90
      :yellow
    else
      :red
    end
  end

  def relativize(path)
    path = Pathname.new path
    path = path.relative_path_from(Rails.root) if path.absolute?
    path
  end

  def test_path_to_source(path)
    search_source(relativize(path))
  end

  def search_source(path)
    basename = path.basename.to_s.gsub /_test\.rb$/, '.rb'
    %w(app lib).map do |root|
      path.sub('test', root).dirname.join(basename)
    end.detect &:exist?
  end
end

SimpleCov.command_name 'test'
SimpleCov.minimum_coverage MINIMUM_COVERAGE_PERCENT

# Ignore files with less than 5 lines
SimpleCov.add_filter do |source_file|
  source_file.lines.count < 5
end
# Ignore rake tasks
SimpleCov.add_filter '/lib/tasks/'

# Create a Renderer group
SimpleCov.add_group 'API', 'lib/api'

# Provide output for both the Console and the Browser
formatters = [SimpleCov::Formatter::HTMLFormatter, SimpleCov::Formatter::AccurateFormatter]
formatters << SimpleCov::Formatter::RcovFormatter if ENV['COVERAGE']
formatters << SimpleCov::Formatter::Console unless ENV['CI']
SimpleCov.formatter = SimpleCov::Formatter::MultiFormatter[*formatters]
