source 'https://rubygems.org'

# Extensions for particular Yavu's distributions
if File.exists? 'Gemfile.extensions'
  instance_eval File.read('Gemfile.extensions')
end

gem 'rails',                      '~> 4.0.0'

# Base extensions for Yavu
# gem 'yavu_surveys', git: 'https://gitlab.com/yavu-cms/surveys-gem', tag: 'v1.2.0'

# Gems used for assets management
gem 'sprockets-rails',            '~> 2.0.0'
gem 'sass-rails',                 '~> 4.0.0'
gem 'zurb-foundation',            '=  4.3.0'
gem 'uglifier',                   '~> 2.2.1'
gem 'compass-rails',              '~> 1.1.7'
gem 'lograge',                    '~> 0.3.0'
# Only asset gems
source 'https://rails-assets.org' do
  gem 'rails-assets-jquery-ujs',    '~> 1.0.0'
  gem 'rails-assets-modernizr',     '~> 2.8.2'
  gem 'rails-assets-typeahead.js',  '~> 0.10.2'
  gem 'rails-assets-codemirror',    '~> 4.3.0'
  gem 'rails-assets-handlebars',    '~> 1.3.0'
  gem 'rails-assets-dropzone',      '~> 3.10'
end

gem 'ckeditor',                   '~> 4.1.2'
gem 'select2-rails',              '~> 3.5.0'

# Rails config to globalize the custom services settings
gem 'rails_config',               '~> 0.4.2'
gem 'rails-settings-cached',      '~> 0.4.1'

# Allow deep-cloning AR models
gem 'deep_cloneable',             '~> 1.6.0'

gem 'mysql2',                     '~> 0.3.13'
gem 'enumerize',                  '~> 0.7.0'
gem 'transaction_retry',          '~> 1.0.2'
gem 'demoji',                     '~> 0.0.5'

# Coderay: a syntax highlighting viewer written in Ruby
gem 'coderay'
gem 'differ',                     '~> 0.1.2'

# Image processing & file uploading
gem 'mini_magick',                '~> 4.0.0.rc', require: false
gem 'carrierwave',                '~> 0.9.0'
gem 'carrierwave-imageoptimizer', '~> 1.2.1'
# Nested forms
gem 'nested_form',                '~> 0.3.2'

# Provisional, awaiting a merge request
gem 'kaminari',                   github: 'Desarrollo-CeSPI/kaminari', branch: 'pluralize-locale'

# Auth(entic|oriz)ation libraries
gem 'devise',                     '~> 3.3.0'
gem 'cancancan',                  '~> 1.8'

# JSON processors
gem 'multi_json',                 '~> 1.7.9'
gem 'oj',                         '~> 2.1.4'

# RESTful services client
gem 'rest-client',                '~> 1.6.7'

# Unicorn as the app server
gem 'unicorn',                    '~> 4.8.3'
gem 'unicorn-worker-killer',      '~> 0.4.3'
# Thin as websockets server
gem 'thin',                       '~> 1.6.3'

gem 'foreman',                    tag: 'v0.63.0',  github: 'ddollar/foreman'

# Sidekiq: Simple, efficient background processing for Ruby. + Retry on Error
gem 'sidekiq',                    '= 3.4.2'
gem 'sidekiq-cron',               '~> 0.2'
gem 'sidekiq-unique-jobs',        '= 3.0.14' # It's the last version of 3.x.x branch
gem 'celluloid',                  '= 0.16.0'
gem 'rufus-scheduler',            '= 3.2.1'
gem 'redis',                      '~> 3.0'
gem 'redis-namespace',            '~> 1.4'
gem 'redis-rails',                '~> 4.0.0'

# Elasticsearch
gem 'elasticsearch-model', '~> 0.1.2'
gem 'elasticsearch-rails', '~> 0.1.2'

# Web sockets notification
gem 'faye',                '~> 1.1.1'
gem 'faye-redis',          '~> 0.2.0'

# Model management and tracking
gem 'paper_trail', '~> 4.0.0.rc1'

# For calendars
gem 'google_calendar', '~> 0.4.4'

#########################################################
# Audio and video tags
gem 'mediaelement_rails',       '~> 0.8.0'

# Mongoid for accounting information
gem 'mongoid',                  '~> 5.2.1'
gem 'activemodel',              '~> 4.0.0'
# Use to encode string to html to match filters
gem 'htmlentities',             '~> 4.3.1'
gem 'sanitize',                 '~> 2.0.0',  require: false

gem 'yavu-api-resource',      git: 'https://gitlab.com/yavu-cms/api-resource-gem', tag: 'v1.2.0'
gem 'yavu-frontend',          git: 'https://gitlab.com/yavu-cms/frontend-gem', tag: '1.11.0'

group :development do
  # Better errors
  gem 'better_errors',            '~> 0.9.0'
  gem 'binding_of_caller',        '~> 0.7.2'
  gem 'dotenv-rails',             '~> 0.11.1'
  gem 'quiet_assets',             '~> 1.0'
  gem 'pry-rails'
end

group :test do
  gem 'test_after_commit',        '~> 0.2.3'
  gem 'minitest',                 '~> 4.7.5'
  gem 'minitest-reporters',       '~> 0.14.24'
  gem 'mocha',                    '~> 0.14.0', require: false
  gem 'factory_girl_rails',       '~> 4.2.1',  require: false
  gem 'simplecov',                '~> 0.8.2',  require: false
  gem 'simplecov-console',        '~> 0.1.3',  require: false
  gem 'simplecov-rcov',           '~> 0.2.3',  require: false
  gem 'database_cleaner',         '~> 1.5.3',  require: false
end

gem 'exception_notification', github: 'smartinez87/exception_notification', ref: '85a1ae90be438e5a465316e7a02813de9198a29b'
