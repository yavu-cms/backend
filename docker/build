#!/bin/bash
set -e

help_message()
{
  echo
  echo "Construye la imagen para el repo actual. Uso:"
  echo
  echo "$0 -v DOCKER-TAG -t GIT-TAG"
  echo
  echo "  -v DOCKER_TAG    Crea la imagen con el tag indicado"
  echo "  -t GIT_TAG       Crea la imagen a partir del tag GIT indicado."
  echo "                   En caso de no especificarlo, se usa master"
  echo
}

if [ `uname | grep -q Linux` ]; then
  TEMP=`getopt -o v:t:h -n $0 -- "$@"`
else
  TEMP=`getopt v:t:h "$@"`
fi

eval set -- "$TEMP"

git_tag=HEAD

while true ; do
  case "$1" in
    -v) version=$2; shift 2 ;;
    -t) git_tag=$2; shift 2 ;;
    --) shift; break ;;
    h|*) help_message ; exit 1 ;;
  esac
done

GIT_REF="$(git rev-parse $git_tag)"

[ -z "$GIT_REF" ] && ( echo "Tag $git_tag incorrecto: 'git rev-parse $git_tag' no devuelve resultados" && exit 1)
[ -z "$version" ] && ( echo "Debe indicar la version del TAG docker. Vea el uso con -h" && exit 1)

rocker build --var version="$version" --build-arg GIT_REF="$GIT_REF" --push
