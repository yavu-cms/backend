# El Dia - Engines

Este documento explica los pasos a seguir para crear un Mountable Engine que extienda la funcionalidad del **Backend** del **CMS El Día**.

## Inicialización y configuración

Cada engine debe implementar su configuración en una clase específica ```<EngineName>::Configuration``` y proveer acceso a dicha configuración de la siguiente manera:

Por ejemplo, considerando un engine ```Surveys```:

```ruby

# surveys/lib/surveys/configuration.rb

module Surveys
  class Configuration
    attr_accessor :mounting_point, :api_mounting_point

    # Set engine's configuration default values
    def initialize
      @api_mounting_point = '/extensions/api/surveys'
      @mounting_point = '/extensions/surveys'
    end
  end
end
```

Al igual que el resto de las clases del engine, ```configuration.rb``` debe requerirse en ```/lib/<engine_name>.rb``` y definir el modulo mismo con su metodo de configuracion.
Por ejemplo:

```ruby
require 'voting/api'
require 'voting/configuration'
require 'voting/engine'
require 'voting/version'

module Voting
  mattr_accessor :configuration

  def self.configure
    Voting.configuration ||= Configuration.new
    yield Voting.configuration if block_given?
  end
end
```

Para todos aquellos parámetros de configuración que resulten indispensables para el funcionamiento del engine deben setearse valores por defecto tales que permitan que el backend funcione correctamente sin customizar ninguna configuración.

Dichos valores se especifican en el método ```<EngineName>::Configuration#initialize```.
Para que la configuración se instancie e inicialice con dichos valores se debe agregar un llamado a  ```<EngineModule>::configure``` en ```/lib/<engine_name>/engine.rb```. Por ejemplo, para un engine Voting:

```ruby
#<engine_name>/lib/<engine_name>/engine.rb

module Voting
  class Engine < ::Rails::Engine
    isolate_namespace Voting

    # Make the extension to configure itself with the default values
    initializer :configure_engine do
      Voting.configure
    end

#(...)

  end
end

```

Para todos los __Mountable Engines__, debe definirse un parámetro de configuración ```mounting_point``` que especifica el punto de montaje en el cual será accesible el engine.
Si el engine expone una **API**, con el mismo propósito, deberá definirse un parámetro ```api_mounting_point```

(NOTA: Estos parámetros serán utilizados en la sección siguiente, dónde se define el montaje "automático" del engine.)


#### Cambiando los parámetros por defecto

La customización de la configuración de todos los engines que se utilicen en el _**Backend**_ se realiza en el initializer ```/config/initializers/engines.rb```.
utilizando el método ```<EngineName>::configure```.
Por ejemplo:

```ruby
  #/config/initializers/engines.rb

  Surveys.configure do |config|
    config.api_mounting_point = '/api/surveys'
    config.mounting_point = '/surveys'
  end

  OtherEngine.configure do |config|
    config.mounting_point = '/other_engine'
  end
```

## Routeo

### Auto-montaje

Para evitar contaminar las rutas del backend con rutas específicas del engine, cada engine debe montarse a sí mismo en el **Backend**. Esto se logra escribiendo las rutas correspondientes en el esquema de ruteo del backend de la siguiente forma:

En la configuración de ruteo del engine (```<engine_name>/config/routes.rb```), debajo de las rutas propias del engine, se declara el punto de montaje del mismo sobre el __**Backend**__:


```ruby

# <engine_name>/config/routes.rb

  Surveys::Engine.routes.draw do
    # rutas propias del engine...
  end

  #se abre el bloque de configuración de rutas del backend
  Rails.application.routes.draw do
    mount Surveys::Engine => Surveys.configuration.mounting_point, as: :surveys
    mount Surveys::API => Surveys.configuration.api_mounting_point
  end

```

Como se observa en el ejemplo anterior, los paths que se utilizan para configurar los puntos de montaje se toman de la configuración del Engine.
De esta manera, se permite que desde el __**backend**__ puedan controlarse todos los parámetros y rutas utilizadas en el engine.


## Migraciones
Si el engine trabaja con un modelo propio (cualquier clase que herede de ActiveRecord::Base), debe poseer migraciones. Rails provee una tarea __Rake__ que copia estas migraciones en el __**Backend**__.


**¡¡__Esta tarea__ NO __debe ser utilizada__!!**.


Genera problemas durante el deploy ya que las migraciones que copia se crean con un timestamp nuevo, por lo tanto en producción capistrano las quiere volver a correr. Para lograr mantener las migraciones propias del engine en el engine y poder manejarlas independientemente, pero permitir que el __**Backend**__ pueda ejecutarlas, debe agregarse el siguiente initializer en el engine:


```ruby
#<engine_name>/lib/<engine_name>/engine.rb
# (...)

  initializer :append_migrations do |app|
    unless app.root.to_s.match root.to_s
    config.paths['db/migrate'].expanded.each do |expanded_path|
      app.config.paths['db/migrate'] << expanded_path
    end
  end

```

## Instalación

Teniendo en cuenta todo lo anteriormente explicado, para instalar un engine simplemente se debe:

1. Agregar la gema al Gemfile.
2. Configurar el engine (si hace falta personalizar algún parámetro de configuración) en el initializer ```engines.rb```
3. Correr las migraciones (si corresponde)
4. Ejecutar ```bundle install```
