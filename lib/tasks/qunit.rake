namespace :test do
  desc 'javascRun Qunit test suite using phantomjs'
  task :js, [:path] => :environment do |t, args|
    failed = false
    driver = 'phantomjs'
    path = args.path || 'test/javascripts/**/*.html'
    FileList[path].each do |test_path|
      puts "# Running Qunit tests at #{test_path}"
      sh "#{driver} test/javascripts/lib/qunit/runner.js #{test_path}", verbose: false do |ok, res|
        unless ok
          failed = true
          puts "\033[31m- Failed!\033[0m"
        end
      end
    end

    if failed
      puts "\n\033[31mSome test(s) failed. Please review them.\033[0m"
    else
      puts "\n\033[32mAll tests are passing. Good job!\033[0m"
    end
  end
end
