namespace :background do
  desc 'Compile assets for the given client application, optionally notifying a user about the outcome'
  task :compile_assets, [:id, :user, :url] => :environment do |t, args|
    args.with_defaults user: nil, url: nil
    client_application = ClientApplication.find(args.id)
    publish_url = args.url.presence || default_announcements_url
    begin
      tries ||= 2
      client_application.compile_assets
      if args.user.present?
        Announcement.publish I18n.t('client_applications.compile_assets_worker.messages.success'), user: args.user, url: publish_url
      end
    rescue Exception => e
      Rails.logger.error('CompileAssets') { "Error compiling assets - Retrying message=#{e.message} trace=#{e.backtrace.join(';')}" }
      client_application.representations.each(&:update_assets!)
      retry unless (tries -= 1).zero?
      if args.user.present?
        Announcement.publish I18n.t('client_applications.compile_assets_worker.messages.failed'), detail: e.message, type: :error, user: args.user, url: publish_url
      end
    end
  end

  desc 'Create a draft for the given representation'
  task :create_draft, [:representation_id] => :environment do |t, args|
    begin
      representation = Representation.find(args.representation_id)
      representation.generate_draft
    rescue Exception => e
      Rails.logger.error('DraftsCreation') do
        "Error creating draft representation_id=#{args.representation_id} message=#{e.message} trace=#{e.backtrace.join(';')}"
      end
    end
  end

  def default_announcements_url
    URI(YavuSettings.announcements.hostspec).tap do |uri|
      uri.path = YavuSettings.announcements.path
    end.to_s
  end
end