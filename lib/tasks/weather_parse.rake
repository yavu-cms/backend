namespace :weather do
  desc "Parse SMN website and get the weather"
  task :parse, [:url] => :environment do |t, args|
    args.with_defaults(:url => 'http://www.smn.gov.ar/?mod=pron&id=4&provincia=Buenos%20Aires&ciudad=La%20Plata')

    begin
      require_relative File.join('..', '..', 'var', 'extensions', 'weather', 'extras.rb')
      require 'mongoid'
      page = Nokogiri::HTML(open(args.url))
      # Span tag with description and temp
      span = page.css("#divPronostico div span")
      description = span.first.text
      temp = span.last.text
      wind = page.css("#divPronostico div div").last.text
      wind = wind.slice(/ .* /).strip + " Km/h"
      hu = page.css("div.font1")[2].text
      # Processing humidity
      hu.slice!(/Vis: \d* km Hr:/)
      hu.strip!
      Weather.create(temperature: temp, wind: wind, humidity: hu, description: description)
    rescue Exception => e
      Rails.logger.error "Falló el algoritmo de scrapping del clima"
      Rails.logger.error "#{e.message}"
    end

  end
end