namespace :stress do
  desc 'Generates a URL set for stress testing the site. Available env variables: ID, HOSTNAME'
  task :urls, [:path] => :environment do |t, args|
    args.with_defaults path: '/tmp/stress-urls'
    FileUtils.touch args[:path]
    client_application = if ENV['ID']
                           ClientApplication.find ENV['ID']
                         else
                           ClientApplication.where(enabled: true).first!
                         end
    hostname = ENV['HOSTNAME'].to_s
    entries = {
      articles: Article.count / 3,
      sections: Section.count,
      tags: Tag.count / 2,
      random_queries: 150,
      random_dates: 150,
      random_urls: 150,
      combined: 150
    }

    # Application routes
    home_route    = client_application.routes.where(type: 'HomepageRoute').first
    article_route = client_application.routes.where(type: 'ArticleRoute').first
    section_route = client_application.routes.where(type: 'SectionRoute').first
    search_route  = client_application.routes.where(type: 'SearchRoute').first
    other_routes = client_application.routes.where(type: ['CustomRoute', 'RedirectRoute'])

    sections_sample = sections(entries[:sections])
    urls = []
    # Home page
    urls << home_route.pattern if home_route.present?
    # Custom & Redirect routes
    urls += other_routes.pluck :pattern
    # Section examples
    urls += sections_sample.map { |s| section_route.pattern.gsub(':section', s.slug) } if section_route.present?
    # Article examples
    urls += articles(entries[:articles]).map { |a| article_route.pattern.gsub(':article', a.slug) } if article_route.present?
    if search_route.present?
      # Tag (search) examples
      urls += tags(entries[:tags]).map { |t| [search_route.pattern, search_query(t.name)].join('?') }
      # Random query searches
      urls += random_queries(entries[:random_queries]).map { |q| [search_route.pattern, q].join('?') }
      # Random date searches
      urls += random_dates(entries[:random_dates]).map { |d| [search_route.pattern, d].join('?') }
      # Random section searches
      urls += sections_sample.map { |s| [search_route.pattern, section_query(s)].join('?') }
      # Random combined searches
      urls += combined_queries(entries[:combined], sections_sample).map { |q| [search_route.pattern, q].join('?') }
    end
    # Random 404 examples
    urls += random_urls(entries[:random_urls])

    IO::write args[:path], urls.map {|u| "#{hostname}#{u}"}.join("\n")
    puts "# URL set generated on #{Time.now} with #{urls.count} entries.\nURLs list written to #{args[:path]}"
  end

  def articles(count)
    Article.order(created_at: :asc).sample(count)
  end

  def sections(count)
    Section.all.sample(count)
  end

  def tags(count)
    Tag.all.sample(count)
  end

  def random_queries(count)
    count.times.map { random_query }
  end

  def random_query
    search_query random_words(rand(1..10)).join(' ')
  end

  def search_query(phrase)
    "query=#{URI::escape(phrase)}"
  end

  def random_words(count)
    %w[
      in malesuada turpis et vehicula varius metus erat tincidunt metus vel luctus enim ligula ac est proin
      ullamcorper neque vel tellus pellentesque sed vestibulum metus venenatis proin elementum tortor lacinia turpis
      posuere euismod maecenas nisl lacus porttitor et faucibus ut commodo a neque mauris a risus ac felis egestas
      rhoncus iaculis scelerisque odio quisque luctus est id magna sodales malesuada pellentesque sodales sollicitudin
      tempus sed turpis libero sollicitudin eget bibendum a consectetur quis turpis nunc consectetur nulla quis
      egestas adipiscing mi ligula ullamcorper sapien id lobortis tortor felis eget nisl aenean commodo lectus non
      fringilla volutpat dolor orci malesuada ante ut euismod lorem ligula nec magna nulla facilisi nam eu enim eu
      nunc hendrerit fermentum porttitor quis velit duis ante lorem elementum ac nisi nec commodo venenatis lacus
      curabitur rutrum eros eu aliquam venenatis integer sit amet faucibus sem
    ].sample(count)
  end

  def random_dates(count)
    count.times.map { random_date }
  end

  def random_date
    "date=#{URI::escape(Time.at(rand * Time.now.to_i).to_date.to_s)}"
  end

  def random_section_query(sections)
    section_query sections.sample
  end

  def section_query(section)
    "section=#{URI::escape(section.name)}"
  end

  def random_urls(count)
    count.times.map { random_url }
  end

  def random_url
    "/#{random_words(5).join(rand(5) % 3 == 1 ? '-' : '/')}"
  end

  def combined_queries(count, sections)
    count.times.map do
      parts = []
      parts << random_query if rand(15) % 3 == 0
      parts << random_date if rand(15) % 3 == 1
      parts << random_section_query(sections) if rand(15) % 3 == 2
      parts.join('&')
    end.reject(&:blank?)
  end
end