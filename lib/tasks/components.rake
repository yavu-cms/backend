namespace :components do
  desc 'Load all components from disk to database. If environment variable FLUSH is set, all components will be destroyed'
  task :load_all, [ :path, :try_find ] => :initialize do |t, args|
    args.with_defaults path: Rails.root.join('var', 'extensions')

    if ENV['FLUSH']
      BaseComponentConfiguration.destroy_all
      BaseComponent.destroy_all
      ComponentField.destroy_all
    end

    source_dir = Pathname.new(args[:path])
    raise "ERROR: Component directory is not valid" unless File.directory?(source_dir)
    Dir[source_dir.join '*'].each do |dir|
      next unless File.directory?(dir)
      slug = dir.split('/').last
      begin
        Rake::Task['components:load'].execute slug: slug, path: dir, try_find: args[:try_find].present?, from_load_all: true
      rescue Exception => e
        puts '---------------------------------------------------'
        puts "Error loading componente: #{e.message}"
        print e.backtrace.first(5).join("\n")
        puts '---------------------------------------------------'
        next
      end
    end
    Representation.all.each(&:update_assets!)
  end

  desc 'Load a single component from disk to database'
  task :load, [ :path, :slug, :try_find ] => :initialize do |t, args|
    args.reverse_merge! from_load_all: false

    component_slug = args[:slug]
    component_dir   = Rails.root.join args[:path]
    print "Loading component from #{component_slug} ... "

    ActiveRecord::Base.transaction do
      yaml = YAML.load(File.open(component_dir.join('core.yml'), 'r') { |file| file.read })
      if args[:try_find]
        component = BaseComponent.find_by slug: component_slug
        component.update yaml
      end
      component ||= BaseComponent.new yaml

      # View validations need component_id is present. This dummy view
      # is created only for the sake of doing a premature saving of component.
      component.views << View.new(name: 'NULL', value: '', format: 'text/html')
      component.save!

      component.views        = load_views        component_dir, component
      component.translations = load_translations component_dir, component
      component.services     = load_services     component_dir, component
      component.fields       = load_fields       component_dir, component

      component.fields.each       { |field| field.component = component }
      component.translations.each { |translation| translation.component = component }
      component.services.each     { |service| service.component = component }

      component.save!
    end

    component = BaseComponent.find_by slug: component_slug
    component.assets = load_assets(component_dir, component)
    component.save!
    Representation.all.each(&:update_assets!) unless args[:from_load_all]
    puts " Done"
  end

  desc 'Dump a single component from database to disk'
  task :dump, [ :path, :name, :force ] => :initialize do |t, args|
    print "Dumping #{args[:name]}..."

    component = BaseComponent.find_by slug: args[:name]
    if component
      component_dir = Pathname(File.expand_path(args[:path]))
      if component_dir.exist?
        if args[:force]
          component_dir.rmtree
        else
          raise "#{args[:name]} already exists!"
        end
      end

      FileUtils.mkdir component_dir

      # Write the component basics:
      File.open(component_dir.join('core.yml'), 'w') { |file| file.write(valid_attributes(component).to_yaml) }

      write_views(component, component_dir)        unless component.views.empty?
      write_translations(component, component_dir) unless component.translations.empty?
      write_services(component, component_dir)     unless component.services.empty?
      write_fields(component, component_dir)       unless component.fields.empty?
      write_assets(component, component_dir)       unless component.assets.empty?

      puts " Done"
    else
      raise "Component #{args[:name]} not found in database"
    end
  end

  desc 'Dump all components to disk'
  task :dump_all, [ :path ] => :initialize do |t, args|
    args.with_defaults path: Rails.root.join('var', 'extensions')

    components_dir = Pathname.new(args[:path])

    BaseComponent.all.each do |component|
      begin
        Rake::Task['components:dump'].execute path: components_dir.join(component.slug), name: component.slug, force: ENV['FLUSH'].present?
      rescue Exception => e
        puts " #{e.message}"
        next
      end
    end
  end

  task :initialize => :environment do
    STDOUT.sync = true
  end

  # === VIEWS
  #

  def load_views(component_dir, component)
    safe_file_read(component_dir.join('views.yml')) do |file|
      yaml = YAML.load(file.read)
      to_delete = component.views.pluck(:name) - yaml.map { |v_h| v_h['name'] }
      deleted   = component.views.where(name: to_delete).delete_all
      raise 'Cant delete some unused translations' unless to_delete.count == deleted
      yaml.map do |v_h|
        component.views.find_or_initialize_by(name: v_h['name']).tap do |view|
          view.new_record? ? view.attributes = v_h : view.update_columns(v_h)
        end
      end
    end || []
  end

  def write_views(component, component_dir)
    File.open(component_dir.join('views.yml'), 'w') do |file|
      file.write(component.views.map { |view| valid_attributes(view) }.to_yaml)
    end
  end

  # === TRANSLATIONS

  def load_translations(component_dir, component)
    safe_file_read(component_dir.join('i18n.yml')) do |file|
      yaml = YAML.load(file.read)
      to_delete = component.translations.pluck(:locale) - yaml.map { |t_h| t_h['locale'] }
      deleted   = component.translations.where(locale: to_delete).delete_all
      raise 'Cant delete some unused translations' unless to_delete.count == deleted
      yaml.map do |t_h|
        component.translations.find_or_initialize_by(locale: t_h['locale']).tap do |translation|
          val = t_h['values']
          translation.new_record? ? translation.values = val : translation.update_column(:values, val)
        end
      end
    end || []
  end

  def write_translations(component, component_dir)
    File.open(component_dir.join('i18n.yml'), 'w') do |file|
      file.write(component.translations.map { |translation| valid_attributes(translation) }.to_yaml)
    end
  end

  # === SERVICES
  def load_services(component_dir, component)
    safe_file_read(component_dir.join('services.yml')) do |file|
      yaml = YAML.load(file.read)
      to_delete = component.services.pluck(:name) - yaml.map { |s_h| s_h['name'] }
      deleted   = component.services.where(name: to_delete).delete_all
      raise 'Cant delete some unused services' unless to_delete.count == deleted
      yaml.map do |s_h|
        component.services.find_or_initialize_by(name: s_h['name']).tap do |service|
          service.view = component.views.detect { |v| v.name == s_h['view'] }
          service.new_record? ? service.attributes = s_h.except('view') : service.update_columns(s_h.except('view'))
        end
      end
    end || []
  end

  def write_services(component, component_dir)
    File.open(component_dir.join('services.yml'), 'w') do |file|
      file.write(component.services.map { |service| valid_attributes(service).merge('view' => service.view.name) }.to_yaml)
    end
  end

  # === ASSETS

  def load_assets(component_dir, component)
    component.assets.each { |a| a.delete }
    component.reload
    Dir[component_dir.join('assets', '*')].inject([]) do |component_assets, assets_type|
      assets_type_folder = Pathname.new(assets_type)
      component_assets + Dir[(assets_type_folder.join(component_dir.basename, '*'))].map do |asset|
        component.assets.new(skip_update_component_assets: true).tap do |a|
          a.file = File.open(asset)
        end
      end
    end
  end

  def write_assets(component, component_dir)
    assets_dir = component_dir.join('assets')
    FileUtils.mkdir assets_dir
    component.assets.each do |asset|
      asset_type_dir = assets_dir.join(asset.type)
      asset_place    = asset_type_dir.join(component.slug)
      unless asset_type_dir.directory?
        FileUtils.mkdir(asset_type_dir)
        FileUtils.mkdir(asset_place)
      end
      FileUtils.cp asset.file.file.file, asset_place
    end
  end

  # === FIELDS

  def load_fields(component_dir, component)
    safe_file_read(component_dir.join('fields.yml')) do |file|
      yaml = YAML.load(file.read)
      to_delete = component.fields.pluck(:name) - yaml.map { |s_h| s_h['name'] }
      deleted   = component.fields.where(name: to_delete).delete_all
      raise 'Cant delete some unused fields' unless to_delete.count == deleted
      yaml.map do |s_f|
        s_f = s_f.reject { |_, value| value.blank? }
        component.fields.find_or_initialize_by(name: s_f['name']).tap do |field|
          field.new_record? ? field.attributes = s_f : field.update_columns(s_f)
        end
      end
    end || []
  end

  def write_fields(component, component_dir)
    File.open(component_dir.join('fields.yml'), 'w') do |file|
      file.write(component.fields.map { |field| valid_attributes(field) }.to_yaml)
    end
  end

  # === MISC

  def safe_file_read(path, &block)
    File.open(path,'r') { |file| yield file if block_given? } if File.exists?(path)
  end

  def valid_attributes(object)
    object.attributes.except('id', 'updated_at', 'created_at', 'component_id', 'view_id', 'base_component_id', 'base_component_configuration_id')
  end
end
