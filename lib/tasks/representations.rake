namespace :representations do
  desc 'Load initial alternative views'
  task :initialize_alternative_views => :environment do
    title       = 'Diario El Día - RSS'
    description = 'Noticias de La Plata - Argentina'
    copyright   = "(c) #{Date.today.year}, Diario El Día, todos los derechos reservados"

    rss = View.create!(name: 'rss standard', format: 'application/rss+xml', value: <<-XML)
      <?xml version="1.0" encoding="utf-8"?>
      <rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:media="http://search.yahoo.com/mrss/" xmlns:news="http://   www.diariosmoviles.com.ar/news-rss/">
      <channel>
        <title>#{title}</title>
        <link><%= root_path %></link>
        <description>#{description}</description>
        <copyright>#{copyright}</copyright>
        <pubDate><%= DateTime.now %></pubDate>
        <image>
          <title>#{title}</title>
          <url><%= image_url(asset_path(logo.file), absolute: true) %></url>
          <link><%= root_path %></link>
        </image>
        <ttl>10</ttl>
        #{View::CONTENT_PLACEHOLDER}
      </channel>
      </rss>
      XML

    atom = View.create!(name: 'rss atom', format: 'application/atom+xml', value: <<-XML)
    <?xml version="1.0" encoding="utf-8"?><rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:media="http://search.yahoo.com/mrss/" xmlns:news="http://www.diariosmoviles.com.ar/news-rss/">
    <channel>
      <title>#{title}</title>
      <link><%= root_path %></link>
      <description>#{description}</description>
      <copyright>#{copyright}</copyright>
      <pubDate><%= DateTime.now %></pubDate>
      <image>
        <title>#{title}</title>
        <url><%= image_url(asset_path(logo.file), absolute: true) %></url>
        <link><%= root_path %></link>
      </image>
      <ttl>10</ttl>
      #{View::CONTENT_PLACEHOLDER}
    </channel>
    </rss>
    XML
  end
end
