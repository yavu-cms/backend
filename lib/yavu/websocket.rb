require 'faye/websocket'
require 'faye/redis'
require 'redis/namespace'

module Yavu
  class Websocket
    KEEPALIVE_TIME = 15

    def initialize
      @clients          = []
      uri               = URI.parse(ENV["REDIS_URL"] || 'redis://localhost:6379/0')
      redis_connection  = {host: uri.host, port: uri.port, password: uri.password}
      @redis            = Redis::Namespace.new 'yavu.websockets', redis: Redis.new(redis_connection)
      Thread.new do
        redis_sub = Redis::Namespace.new 'yavu.websockets', redis: Redis.new(redis_connection)
        # This new redis connection will be blocked inside thread when subscribing
        redis_sub.subscribe(CHANNEL) do |on|
          on.message do |channel, msg|
            @clients.each {|ws| ws.send(msg) }
          end
        end
      end

      def call(env)
        if Faye::WebSocket.websocket?(env)
          ws = Faye::WebSocket.new(env, nil, {ping: KEEPALIVE_TIME })
          ws.on :open do |event|
            p [:open, ws.object_id]
            @clients << ws
          end
          ws.on :message do |event|
            p [:message, event.data]
            @redis.publish(CHANNEL, event.data)
          end
          ws.on :close do |event|
            p [:close, ws.object_id, event.code, event.reason]
            @clients.delete(ws)
            ws = nil
          end
          ws.rack_response
        else
          content = 'Not found'
          [404, {'Content-Type' => 'text/html', 'Content-Length' => content.length}, [content]]
        end
      end
    end
  end
end
