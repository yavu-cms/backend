;(function($) {
  'use strict';

  /**
   * This plugin allows for having indicators (a.k.a. Ajax loaders) that will
   * be bound to JS events on a given context (defaults to the document's body)
   * in order to be automatically hidden, shown and/or toggled.
   * The default behavior of this plugin is to bind the indicator element
   * (the plugin target) to both `ajaxStart` and `ajaxStop` events on the container
   * and show and hide -respectively- the indicator.
   * See below the `defaults` variable for a comprehensive set of the available
   * options and their default values.
   *
   * Usage example:
   *
   *     // With the default options
   *     $('.indicator').backendPlugin('indicators');
   *     // With some configuration
   *     $('.indicator').backendPlugin('indicators', { duration: 0, hideOnInit: false });
   *
   * Or with the Initializer backend plugin:
   *
   *     <!--
   *      With the default options:
   *       -->
   *     <span data-plugins="{&quot;indicators&quot;:{}}">Loading...</span>
   *     <!--
   *      With some configuration -- HTML-safe version of:
   *        {
   *          indicators: {
   *            duration: 0,
   *            hideOnInit: false
   *          }
   *        }
   *       -->
   *     <span data-plugins="{&quot;indicators&quot;:{&quot;duration&quot;:0,&quot;hideOnInit&quot;:false}}">Loading...</span>
   */

  var pluginName = 'indicators',
    indicators = {},
    defaults = {
      // Method to call on the indicators to hide them
      hide:       'hide',
      // Method to call on the indicators to show them
      show:       'show',
      // Method to call on the indicators to toggle them on/off
      toggle:     'toggle',
      // Animation duration in milliseconds. Set to 0 for no animation.
      duration:   250,
      // Whether the indicators should be hidden on plugin initialization
      hideOnInit: true,
      // Events to bind to as listener, grouped by the action they will perform: show, hide and toggle
      bindTo:     { show: ['ajaxStart'], hide: ['ajaxStop'], toggle: [] },
      // The context in which the events will be trapped. This should be a selector.
      context:    'body'
    },
    // Some constants specific to this plugin
    NAMESPACE         = '.' + pluginName,
    OPTIONS_ATTRIBUTE = pluginName + '-options';

  // Set the plugin options to $el
  function _setOptions($el, options) {
    return $el.data(OPTIONS_ATTRIBUTE, options);
  }

  // Get the plugin options from $el
  function _getOptions($el) {
    return $el.data(OPTIONS_ATTRIBUTE);
  }

  // Run an action on $el with the given options (if provided).
  // Action should either be 'show', 'hide' or 'toggle'.
  function run(action, $el, options) {
    options = options || _getOptions($el);
    $.fn[options[action]].apply($el, [options.duration]);
  }

  // Hide indicators
  indicators.hide = function() {
    run('hide', this);
  };

  // Show indicators
  indicators.show = function() {
    run('show', this);
  };

  // Toggle indicators on/off
  indicators.toggle = function() {
    run('toggle', this);
  };

  // Backend plugin initializer
  indicators.init = function(opts) {
    var $this  = this,
      options  = $.extend({}, defaults, opts),
      $context = $(options.context);

    _setOptions($this, options);

    // Hide indicators on init, if instructed to
    if (options.hideOnInit) {
      run('hide', $this, options);
    }

    // Register events listeners
    $.each(options.bindTo, function(action, eventTypes) {
      $.each(eventTypes, function(index, eventType) {
        $context.on(eventType + NAMESPACE, function() {
          indicators[action].apply($this, [options]);
        });
      });
    });
  };

  // Backend plugin destroyer
  indicators.destroy = function() {
    var options  = _getOptions(this),
      $context = $(options.context);

    $context.off(NAMESPACE);
  };

  // Backend plugin definition
  $.backendPlugin(pluginName, indicators);

})(jQuery);