;(function($) {
  'use strict';

  /**
   * This plugin enhances any HTML element with a WYSIWYG
   * (What You See Is What You Get) editor using the JS library CK-Editor
   * (http://ckeditor.com/).
   *
   * All the options provided for this plugin are passed-through *as received*
   * to the CK-Editor method, so all available options can be found in
   * their documentation.
   *
   */

  var pluginName = 'wysiwyg',
  wysiwyg = {},
  defaults = {},
  // Plugin-specific constants
  PLUGIN_CLASS = pluginName;

  // Backend plugin initializer
  wysiwyg.init = function(opts) {
    this.addClass(PLUGIN_CLASS);

    //Configure a basic toolbar
    CKEDITOR.config.toolbar_basic = [
      { name: 'basicstyles', items : [ 'Bold','Italic','Underline' ] },
      { name: 'styles', items : [ 'Styles' ] },
      { name: 'clipboard', items : [ 'Undo','Redo' ] },
      { name: 'tools', items : [ 'Maximize' ] },
      { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ]},
      { name: 'document', items: [ 'Source' ] },
      { name: 'insert', items: [ 'YavuMedia', 'YavuArticle' ] }
    ];

    // Allow extra classes and attributes
    CKEDITOR.config.allowedContent = true;

    // External text is pasted as plain text
    CKEDITOR.config.forcePasteAsPlainText = true;

    //Set by default the basic toolbar previously defined
    CKEDITOR.config.toolbar = 'basic';

    // enable native spell check
    CKEDITOR.config.disableNativeSpellChecker = false;

    //Set the toolbar over the textarea in "this"
    CKEDITOR.replace(this.attr("name"), opts);

    CKEDITOR.plugins.addExternal('yavu_media', '/assets/plugins/ckeditor/plugins/yavu_media', 'plugin.js');
    CKEDITOR.plugins.addExternal('yavu_article', '/assets/plugins/ckeditor/plugins/yavu_article', 'plugin.js');
    CKEDITOR.config.extraPlugins = 'YavuMedia,YavuArticle';
  };

  // Backend plugin destroyer
  wysiwyg.destroy = function() {
    this.removeClass(PLUGIN_CLASS);
    CKEDITOR.instances[this.attr("name")].destroy();
  };

  // Backend plugin definition
  $.backendPlugin(pluginName, wysiwyg);

})(jQuery);
