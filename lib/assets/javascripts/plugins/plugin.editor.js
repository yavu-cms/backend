;(function($) {
  'use strict';

  /**
   * Backend plugin that provides a generic editor behavior.
   * It enables a master-detail layout to work as such,
   *
   * Usage example:
   *
   * Initialization - you should at least provide a preview URL:
   *
   *     // With the default options
   *     $('.container').backendPlugin('editor');
   *     // With some configuration
   *     $('.container').backendPlugin(
   *       'editor', { previewUrl: '/articles/preview' }
   *     );
   *
   * Please, refer to the `defaults` variable for the complete documentation of
   * the available options for this plugin.
   *
   * After it's been initialized, the Editor plugin provides the following methods
   * to interact with it:
   *
   *   - `selected()` (getter version): The currently selected value (identifier) can be
   *     obtained with this method.
   *
   *          var selectedValue = $el.backendPlugin('editor', 'select');
   *
   *   - `selected(newValue)` (setter version): Set the currently selected value to a new value.
   *
   *          $el.backendPlugin('editor', 'select', newValue);
   *
   *   - `refresh`: Refreshes the currently selected item.
   *
   *          $el.backendPlugin('editor', 'refresh');
   *
   *   - `preview(value)`: Preview a value in the preview pane.
   *
   *          $el.backendPlugin('editor', 'preview', myValue);
   *
   *   - `disableDetail`: Disable the detail container. Adds the `options.classes.disabled`
   *     class to the detail container and prevents any click on it from working.
   *
   *          $el.backendPlugin('editor', 'disableDetail');
   *
   *   - `enableDetail`: Enable the detail container. Removes the disabled class and
   *     re-enables the click in the detail container.
   *
   *          $el.backendPlugin('editor', 'enableDetail');
   *
   * Whenever the selected value changes, a 'change.editor' event will be triggered
   * on the container (`$el`) with the new value as the additional argument for the
   * event. Also, when the preview pane is updated, a 'preview' event will be
   * triggered on the container.
   */

  var pluginName = 'editor',
    editor       = {},
    defaults     = {
      // URL and request options
      // URL to which preview requests will be sent
      previewUrl:          '#',
      // URL parameter in which the identifier will be sent to the preview URL
      urlParameterName:    'id',
      // Request method to use ('get' or 'post') for the preview request
      requestMethod:       'get',
      // Selectors
      // Selector for the master container
      master:              '.master',
      // Selector for the detail container
      detail:              '.detail',
      // Selector for the master filters
      filters:             '.filters',
      // Selector for the detail items
      items:               '.items',
      // Selector for an item in the detail items
      item:                '.item',
      // Selector for the preview area
      preview:             '.preview',
      // Attribute from an item that will be used to get the identifier upon selection
      identifierAttribute: 'id',
      // CSS classes to be added/removed
      classes: {
        // Class to add to any element that must be disabled
        disabled: 'disabled',
        // Class to add to any element that must be marked as selected
        selected: 'selected'
      },
      // Prefix to add to the title when updating it
      titlePrefix: 'Editor'
    },
    originalTitle  = document.title,
    // Plugin-specific constants
    NAMESPACE      = '.' + pluginName,
    OPTIONS_ATTR   = pluginName + '-options',
    SELECTED_ATTR  = pluginName + '-selected-item',
    CONTAINER_MARK = pluginName + '-container';

  // Generic event listener that prevents any event
  // it receives from bubbling
  function eventStopper(event) {
    event.preventDefault();
    return false;
  }

  // Find the container element for a given pivot
  function findContainer($pivot) {
    return $pivot.closest('.' + CONTAINER_MARK);
  }

  // Plugin initializer
  editor.init = function(opts) {
    var options = $.extend({}, defaults, opts),
      $master   = this.find(options.master),
      $items    = $master.find(options.items);

    this.data(OPTIONS_ATTR, options);

    // Mark the container with a class so that children elements
    // are able to easily find it
    this.addClass(CONTAINER_MARK);

    // Register event listeners
    // Selection event
    $items.on('click' + NAMESPACE, options.item, editor.select);
    // Ajax events
    this.on('ajaxStart' + NAMESPACE, editor.disableDetail);
    this.on('ajaxStop' + NAMESPACE, editor.enableDetail);

    // Initial value check and refresh
    this.backendPlugin(pluginName, 'refresh');
  };

  // Refresh using either data- attribute or location's hash
  editor.refresh = function() {
    var options = this.data(OPTIONS_ATTR),
      $items    = this.find(options.master + ' ' + options.items),
      $initial, initialValue;

    initialValue = (this.backendPlugin(pluginName, 'selected') || location.hash).replace(/^#/, '');
    if (initialValue != '') {
      $initial = $items.find('#' + initialValue);
      if ($initial.length > 0) {
        editor.select.apply($initial);
      }
    }
  };

  // Plugin destroyer
  editor.destroy = function() {
    var options = this.data(OPTIONS_ATTR),
      $items    = this.find(options.master + ' ' + options.items);

    // Remove container mark
    this.removeClass(CONTAINER_MARK);
    // Unregister event listeners
    this.off(NAMESPACE);
    $items.off(NAMESPACE);
    // Remove options data from container
    this.removeData(OPTIONS_ATTR);
  };

  // Item selection event handler
  editor.select = function() {
    var $item    = $(this),
      $container = findContainer($item),
      options    = $container.data(OPTIONS_ATTR),
      identifier = $item.attr(options.identifierAttribute);

    $container.backendPlugin(pluginName, 'selected', identifier);
    $container.backendPlugin(pluginName, 'preview', identifier);
  };

  // Preview a given identifier
  editor.preview = function(identifier) {
    var $this    = this,
      options    = $this.data(OPTIONS_ATTR),
      $preview   = this.find(options.preview),
      parameters = {};

    // Perform the item preview request
    parameters['data'] = {};
    parameters['data'][options.urlParameterName] = identifier;
    parameters['type'] = options.requestMethod;
    parameters['success'] = function(response) {
      $preview.html(response);
      $preview.backendPlugin('initializer');
      $this.trigger('preview');
    };

    $.ajax(options.previewUrl, parameters);
  };

  // Get or set the selected value.
  // This is a mutable function that works as follows:
  //   - As a getter: $el.backendPlugin('editor', 'selected');
  //   - As a setter: $el.backendPlugin('editor', 'selected', 'new-value');
  editor.selected = function(newValue) {
    var options = this.data(OPTIONS_ATTR),
      $items, $item;

    if (typeof newValue == 'undefined') {
      // Work as a getter
      return this.data(SELECTED_ATTR);
    } else {
      // Work as a setter: newValue is the newly selected value
      $items = this.find(options.items);
      $item  = $items.find('[' + options.identifierAttribute + '=' + newValue + ']');

      // Select the item
      $items
        .find('.' + options.classes.selected)
        .not($item)
        .removeClass(options.classes.selected);

      $item.addClass(options.classes.selected);
      this.data(SELECTED_ATTR, newValue);
      document.title = options.titlePrefix + ' - ' + $item.find('.title').text().trim() + ' | ' + originalTitle;
      this.trigger('change' + NAMESPACE, [newValue]);
    }
  };

  // Disable the detail section
  editor.disableDetail = function() {
    var $container = findContainer($(this)),
      options      = $container.data(OPTIONS_ATTR),
      $detail      = $container.find(options.detail);

    $detail
      .addClass(options.classes.disabled)
      .on('click' + NAMESPACE, '*', eventStopper);
  };

  // Enable the detail section
  editor.enableDetail = function() {
    var $container = findContainer($(this)),
      options      = $container.data(OPTIONS_ATTR),
      $detail      = $container.find(options.detail);

    $detail
      .removeClass(options.classes.disabled)
      .off('click' + NAMESPACE);
  };

  // Backend plugin definition
  $.backendPlugin(pluginName, editor);

})(jQuery);