;(function($) {
  'use strict';

  /**
   * Timepicker backend plugin.
   *
   * This plugin adds support for using jQuery UI's timepicker
   * widget and also brings new options to extend its default
   * functionality.
   *
   * Available options:
   *
   *  - `culture`: The culture (locale) to use for this timepicker.
   *    Defaults to English.
   *  - `trigger`: Whether an alternate trigger element must be used.
   *    This should be a selector for the trigger element. False by default,
   *    which means no additional trigger will be used.
   *
   * Usage examples:
   *
   *     <!-- Default use case: -->
   *     $input.backendPlugin('timepicker');
   *
   *     <!-- A timepicker with an alternative trigger button: -->
   *     <!-- In the HTML world -->
   *     <input type="text" id="timepicker">
   *     <a href="#" id="trigger">Select a date</a>
   *
   *     // In the JS world
   *     $('#timepicker').backendPlugin('timepicker', { trigger: '#trigger' });
   */

  var pluginName = 'timepicker',
    timepicker   = {},
    defaults     = {
      // Culture to use
      culture: 'en',
      // Optional alternative trigger for the timepicker popup
      trigger: false,
      // Render options with selects
      controlType: 'select',
      showTime: false,
      showButtonPanel: false,
    },
    // Constants
    NAMESPACE                  = '.' + pluginName,
    OPTIONS_ATTR               = pluginName + '-options',
    SELECTOR_TIMEPICKER_BUTTON = 'button.ui-timepicker-trigger',
    DIV_TIMEPICKER             = '#ui-datepicker-div';
  // Plugin initializer
  timepicker.init = function(opts) {
    var $this = this,
      options = $.extend({}, defaults, opts);
     
    $this.data(OPTIONS_ATTR, options);

    if (options.trigger) {
      // Register click event listener on the trigger
      $(options.trigger).on('click' + NAMESPACE, function() {
        var method = $(DIV_TIMEPICKER).is(":visible") ? 'hide' : 'show';
        $this.timepicker(method);
        return false;
      });
    }

    // Set the default culture
    $.timepicker.setDefaults($.timepicker.regional[options.culture]);

    // Remove unwanted jQuery UI timepicker options
    delete options.culture;
    delete options.trigger;

    $this.timepicker(options);
  };

  // Plugin destroyer
  timepicker.destroy = function() {
    var options = this.data(OPTIONS_ATTR);

    // Remove events listeners
    if (options.trigger) {
      $(options.trigger).off(NAMESPACE);
    }

    // Remove the options data attribute
    this.removeData(OPTIONS_ATTR);

    // Destroy the timepicker
    this.timepicker('destroy');
  };

  // Plugin definition
  $.backendPlugin(pluginName, timepicker);

})(jQuery);
