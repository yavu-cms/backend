;(function($) {
  'use strict';

  /**
   * This plugin enables the creation of collapsible elements in a page,
   * which can be collapsed (hidden) or risen (shown).
   * The plugin will add a toggle before the subject element (the one on
   * which the plugin has been invoked) that when clicked will toggle
   * the state of the collapsible element.
   *
   * Usage example:
   *
   *     // With the default options
   *     $('.indicator').backendPlugin('collapsible');
   *     // With some configuration
   *     var opts = {
   *       labels: { rise: 'Mostrar', collapse: 'Ocultar' },
   *       collapseOnInit: false
   *     };
   *     $('.indicator').backendPlugin('collapsible', opts);
   *
   * Or with the Initializer backend plugin:
   *
   *     <!--
   *      With the default options:
   *       -->
   *     <div data-plugins="{&quot;collapsible&quot;:{}}">Collapsible element</div>
   *     <!--
   *      With some configuration -- HTML-safe version of:
   *        {
   *          collapsible: {
   *            labels: {
   *              rise: 'Mostrar',
   *              collapse: 'Ocultar'
   *            },
   *            collapseOnInit: false
   *          }
   *        }
   *       -->
   *     <div data-plugins="{&quot;collapsible&quot;:{&quot;collapseOnInit&quot;:false,&quot;labels&quot;:{&quot;rise&quot;:&quot;Mostrar&quot;,&quot;collapse&quot;:&quot;Ocultar&quot;}}}">Collapsible element</div>
   */

  var pluginName = 'collapsible',
    collapsible  = {},
    defaults     = {
      // Indicates whether a new toggle should be added to the page (`false`) or
      // an existing one should be used (a non-falsey value, should be a selector).
      useToggle:      false,
      // Template to create the toggle. Text will be automatically added to it.
      // Only applicable if `useToggle` is `false`.
      toggleTemplate: '<a href="#" class="collapsible-toggle"></a>',
      // Labels to use inside the toggle.
      labels:         { collapse: 'Hide', rise: 'Show' },
      // Whether to collapse (true) or rise (false) on init.
      collapseOnInit: true,
      // CSS class names to add when collapsed or risen.
      classes:        { collapsed: 'collapsed', risen: 'risen' }
    },
    // Plugin-specific constants
    NAMESPACE    = '.' + pluginName,
    PLUGIN_CLASS = pluginName,
    OPTIONS_ATTR = pluginName + '-options',
    SUBJECT_ATTR = pluginName + '-subject',
    TOGGLE_ATTR  = pluginName + '-toggle';

  // Set the options for the toggle `$el`
  function _setOptions($el, options) {
    $el.data(OPTIONS_ATTR, options);
  }

  // Get `$el`'s (a toggle) options
  function _getOptions($el) {
    $el = $el instanceof jQuery ? $el : $($el);
    return $el.data(OPTIONS_ATTR);
  }

  // Set the subject for the toggle `$el`
  function _setSubject($el, subject) {
    $el = $el instanceof jQuery ? $el : $($el);
    $el.data(SUBJECT_ATTR, subject);
  }

  // Get `$el`'s (a toggle) subject
  function _getSubject($el) {
    $el = $el instanceof jQuery ? $el : $($el);
    return $el.data(SUBJECT_ATTR);
  }

  // Set the toggle for `$el`
  function _setToggle($el, toggle) {
    $el = $el instanceof jQuery ? $el : $($el);
    $el.data(TOGGLE_ATTR, toggle);
  }

  // Get the toggle for `$el`
  function _getToggle($el) {
    $el = $el instanceof jQuery ? $el : $($el);
    return $el.data(TOGGLE_ATTR);
  }

  // Backend plugin initialization
  collapsible.init = function(opts) {
    var $this    = this,
      options    = $.extend({}, defaults, opts),
      $toggle;

    if (options.useToggle) {
      $toggle = $(options.useToggle);
    } else {
      $toggle = $(options.toggleTemplate)
    }

    _setOptions($toggle, options);
    _setSubject($toggle, $this);
    _setToggle($this, $toggle);

    // Register the event listener
    $toggle.on('click' + NAMESPACE, collapsible.toggle);

    // Add the plugin class to the subject
    $this.addClass(PLUGIN_CLASS);

    // Add the new toggle to the DOM, if applicable
    if (!options.useToggle) {
      $this.before($toggle);
    }

    // Handle initial state
    if (options.collapseOnInit) {
      collapsible.collapse.apply($toggle);
    } else {
      collapsible.rise.apply($toggle);
    }
  };

  // Toggle the visibility of the collapsible element and change
  // the toggle label accordingly.
  collapsible.toggle = function() {
    var $this  = $(this),
      $toggle  = typeof _getOptions($this) == 'undefined' ? _getToggle($this) : $this,
      $subject = _getSubject($toggle),
      options  = _getOptions($toggle),
      handler  = $subject.hasClass(options.classes.collapsed)
                  ? collapsible.rise : collapsible.collapse;

    handler.apply($toggle);

    // Prevent the event from bubbling up the DOM
    return false;
  };

  // Collapse (hide) the collapsible subject of this (the toggle)
  collapsible.collapse = function() {
    var $this   = this,
      $toggle   = typeof _getOptions($this) == 'undefined' ? _getToggle($this) : $this,
      $subject  = _getSubject($toggle),
      options   = _getOptions($toggle),
      collapsed = options.classes.collapsed,
      risen     = options.classes.risen;

    $subject
      .removeClass(risen)
      .addClass(collapsed);
    $toggle.html(options.labels.rise);
  };

  // Rise (show) the collapsible subject of `this` (the toggle)
  collapsible.rise = function() {
    var $toggle   = typeof _getOptions(this) == 'undefined' ? _getToggle(this) : this,
        $subject  = _getSubject($toggle),
        options   = _getOptions($toggle),
        collapsed = options.classes.collapsed,
        risen     = options.classes.risen;

    $subject
      .removeClass(collapsed)
      .addClass(risen);
    $toggle.html(options.labels.collapse);
  };

  // Destroyer method
  collapsible.destroy = function() {
    var $toggle = _getToggle(this),
      options   = _getOptions($toggle),
      collapsed = options.classes.collapsed,
      risen     = options.classes.risen;

    $toggle.remove();
    this.removeClass([collapsed, risen, PLUGIN_CLASS].join(' '));
  };

  // Backend plugin definition
  $.backendPlugin(pluginName, collapsible);

})(jQuery);