;(function($) {
  'use strict';

  /**
   * MediaElement backend plugin.
   *
   * This plugin adds support for using <audio> tags
   * and also brings options to override defaults
   * functionality.
   *
   * Available options:
   *
   * `mode`:
   * - allows testing on HTML5, flash, silverlight
   * - auto: attempts to detect what the browser can do
   * - auto_plugin: prefer plugins and then attempt native HTML5
   * - native: forces HTML5 playback
   * - shim: disallows HTML5, will attempt either Flash or Silverlight
   * - none: forces fallback view
   * 
   * `plugins`:
   * - remove or reorder to change plugin priority and availability
   * `enablePluginDebug`:
   * - shows debug errors on screen
   * `httpsBasicAuthSite`:
   * - use plugin for browsers that have trouble with Basic Authentication on HTTPS sites
   * `type`:
   * - overrides the type specified, useful for dynamic instantiation
   * `pluginPath`:
   * - path to Flash and Silverlight plugins
   * `flashName`:
   * - name of flash file
   * `flashStreamer`:
   * - streamer for RTMP streaming
   * `enablePluginSmoothing`:
   * - turns on the smoothing filter in Flash
   * `enablePseudoStreaming`:
   * - enabled pseudo-streaming (seek) on .mp4 files
   * `pseudoStreamingStartQueryParam`:
   * - start query parameter sent to server for pseudo-streaming
   * `silverlightName`:
   * - name of silverlight file
   * `defaultVideoWidth`:
   * - default if the <video width> is not specified
   * `defaultVideoHeight`:
   * - default if the <video height> is not specified
   * `pluginWidth`:
   * - overrides <video width>
   * `pluginHeight`:
   * - overrides <video height>
   * `pluginVars`:
   * - additional plugin variables in 'key=value' form
   * `timerRate`: 250
   * - rate in milliseconds for Flash and Silverlight to fire the timeupdate event
   * - larger number is less accurate, but less strain on plugin->JavaScript bridge
   * `startVolume`:
   * - initial volume for player
   *
   * Usage examples:
   *
   *     <!-- Default use case: -->
   *     $input.backendPlugin('medialement');
   *
   *     <!-- In the HTML world -->
   *     <audio src="example.mp3" id="mediaelement-preview">
   *
   *    * - In the JS world
   *     $('#mediaelement-preview').backendPlugin('mediaelement', {});
   */

  var pluginName = 'mediaelement',
    mediaelement = {};

  // Plugin initializer
  mediaelement.init = function(opts) {
    this.mediaelementplayer(opts);
  };

  // Plugin destroyer is not currently implemented
  //mediaelement.destroy = function() {
  //};

  // Plugin definition
  $.backendPlugin(pluginName, mediaelement);

})(jQuery);