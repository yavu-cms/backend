;(function($) {
  'use strict';

  /**
   * This plugin enhances any HTML element with a CodeMirror editor for source
   * code editing (http://codemirror.net/).
   *
   * All the options provided for this plugin are passed-through *as received*
   * to the CodeMirror.fromTextArea method, so all available options can be found in
   * their documentation.
   */

  var pluginName = 'codemirror',
    codemirror   = {},
    defaults     = {
      theme: 'neat',
      tabSize: 2,
      smartIndent: true,
      indentWithTabs: false,
      tabMode: 'spaces',
      lineNumbers: true
    },
    PLUGIN_CLASS = 'js-plugin-' + pluginName;

  // Backend plugin initializer
  codemirror.init = function(opts) {
    var options = $.extend({}, defaults, opts);
    var textArea = this.get(0);
    var cm = CodeMirror.fromTextArea(textArea, options);

    cm.on("change", function(){
        //save is not triggering on_change on text area. but on blur is, except when value is blank. bug?
        // see hack and ticket below
        cm.save();
    });

    cm.on("blur", function(){
        cm.save();
        //FIXME: ugly hack to trigger on_change event on the text area when clearing all text.
        // for some reason, CodeMirror does not trigger a change event on underliying text area
        // when the text is cleared out. tracked in: https://redmine.desarrollo.cespi.unlp.edu.ar/issues/7790
        $(textArea).trigger('change');
    });
    this.data('cm-instance', cm);
    this.addClass(PLUGIN_CLASS);
  };

  // Backend plugin destroyer
  codemirror.destroy = function() {
    this.data('cm-instance').toTextArea();
    this.removeClass(PLUGIN_CLASS);
  };

  // Backend plugin definition
  $.backendPlugin(pluginName, codemirror);
})(jQuery);
