;(function($) {
  'use strict';

  var pluginNamespace = 'backendPlugin',
    definedPlugins    = {},

    // The following functions must be defined in every plugin:
    INITIALIZER = 'init'; // Plugin initializer function name

  /**
   * Base backend JS plugins logic. This allows for defining new plugins.
   *
   * Plugins can be defined providing a name for them and a definition
   * function which must return an Object with the API they provide:
   *
   *     $.backendPlugin('myNewPlugin', function() {
   *       // Plugin definition goes here.
   *       // This function must return a new Object
   *       // with the available methods.
   *     });
   *
   * Or they can be defined providing the name and the Object with
   * the API they offer:
   *
   *     $.backendPlugin('alerter', {
   *       init: function(options) {
   *         // Plugin initialization
   *       },
   *       // This plugin only provides one method: `alert`
   *       alert: function(message) {
   *         alert(message);
   *       },
   *       destroy: function(options) {
   *         // Plugin removal
   *       }
   *     });
   *
   * Once defined, plugins will be available through different
   * initialization methods:
   *
   *   - `data-plugins` attributes initialization and configuration
   *     via `backendPluginInitializer`:
   *
   *         // This will look for any element inside `#container`
   *         // that has the `data-plugins` attribute and will
   *         // initialize any desired plugins.
   *         // @see plugin.initializer.js
   *         $('#container').backendPluginInitializer();
   *
   *   - JS initialization and configuration:
   *
   *         // This will initialize the plugin 'myNewPlugin' with the
   *         // given options hash (if provided, as they are optional).
   *         $('.myElements').backendPlugin('myNewPlugin', options);
   *
   * Also, the plugins defined can expose an API that will be available
   * through the invocation method:
   *
   *     // This simply invokes the 'stop' method on the 'animations'
   *     // backend plugin:
   *     $('span.animated').backendPlugin('animations', 'stop');
   *
   *     // This invokes the 'alert' method on the 'alerter' backend
   *     // plugin with a message argument:
   *     $('body').backendPlugin('alerter', 'alert', 'The message');
   *
   *     // This invokes the method 'hide' on the 'hider' backend
   *     // plugin and provides an argument (an options Object) that
   *     // will be fed into the plugin's method:
   *     $('.myElements').backendPlugin(
   *       'hider', 'hide', { animate: true, duration: 500 }
   *     );
   */
  $[pluginNamespace] = function(pluginName, pluginDefinition) {
    var definitionType = typeof pluginDefinition,
      plugin;

    // Check if the plugin name is available
    if (definedPlugins.hasOwnProperty(pluginName)) {
      throw 'Error while trying to overwrite an existing backend plugin: ' + pluginName;
    }

    plugin = pluginDefinition;
    if (definitionType === 'function') {
      // If the plugin is a function, evaluate it
      plugin = plugin();
    }

    // Check if the definition is of a valid type
    if (typeof plugin.init !== 'function') {
      throw 'Error while trying to define a backend plugin: ' + pluginName
        + '. The provided definition does not yield a valid definition Object.'
        + 'It lacks the "init" attribute or it is present but not a function.';
    }

    // Lastly, add the plugin to the hash of defined plugins
    definedPlugins[pluginName] = plugin;

    return true;
  };

  // Return all the available plugins along with their definitions
  $[pluginNamespace].all = function() {
    return definedPlugins;
  };

  // Clear (remove) all the plugins - this is not undoable!
  // If plugins is provided, it should be an array with the plugin names to remove.
  // If plugins is not provided, ALL plugins will be removed.
  // This should be used only for testing purposes
  $[pluginNamespace].clear = function(plugins) {
    if (typeof plugins != 'undefined') {
      $.each(plugins, function() {
        delete definedPlugins[this];
      });
    } else {
      definedPlugins = {};
    }
  };

  // This performs the black magic that handles the plugins invocation
  $.fn[pluginNamespace] = function() {
    var self = this,
      argc   = arguments.length,
      argv   = [].slice.apply(arguments),
      plugin,
      method;

    if (argc > 0) {
      plugin = argv.shift();

      if (typeof plugin === 'string') {
        // Plugin invocation
        if (!plugin in definedPlugins) {
          throw 'Trying to use an undefined backend plugin: ' + plugin;
        }

        if (typeof argv[0] === 'string') {
          // Plugin method invocation:
          //
          //     $el.backendPlugin('alerter', 'alert', message);
          //                    // ^plugin    ^method  ^[arguments, ...]
          method = argv.shift();
        } else {
          // Plugin initialization:
          //
          //     $el.backendPlugin('myPlugin', options);
          //                    // ^plugin     ^[options]
          method = INITIALIZER;
        }

        // The method invoked is applied in the jQuery object (`$el`, in
        // the examples) context
        return definedPlugins[plugin][method].apply(self, argv);
      }
    }
  };
})(jQuery);