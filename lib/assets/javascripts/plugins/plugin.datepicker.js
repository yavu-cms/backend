;(function($) {
  'use strict';

  /**
   * Datepicker backend plugin.
   *
   * This plugin adds support for using jQuery UI's datepicker
   * widget and also brings new options to extend its default
   * functionality.
   *
   * Available options:
   *
   *  - `culture`: The culture (locale) to use for this datepicker.
   *    Defaults to English.
   *  - `trigger`: Whether an alternate trigger element must be used.
   *    This should be a selector for the trigger element. False by default,
   *    which means no additional trigger will be used.
   *
   * Usage examples:
   *
   *     <!-- Default use case: -->
   *     $input.backendPlugin('datepicker');
   *
   *     <!-- A datepicker with an alternative trigger button: -->
   *     <!-- In the HTML world -->
   *     <input type="text" id="datepicker">
   *     <a href="#" id="trigger">Select a date</a>
   *
   *     // In the JS world
   *     $('#datepicker').backendPlugin('datepicker', { trigger: '#trigger' });
   */

  var pluginName = 'datepicker',
    datepicker   = {},
    defaults     = {
      // Culture to use
      culture: 'en',
      // Optional alternative trigger for the datepicker popup
      trigger: false
    },
    // Constants
    NAMESPACE                  = '.' + pluginName,
    OPTIONS_ATTR               = pluginName + '-options',
    SELECTOR_DATEPICKER_BUTTON = 'button.ui-datepicker-trigger';

  // Plugin initializer
  datepicker.init = function(opts) {
    var $this = this,
      options = $.extend({}, defaults, opts),
      hideBtn = false;

    $this.data(OPTIONS_ATTR, options);

    if (options.trigger) {
      // Trick the datepicker into triggering itself through a button
      // but remove the button after it's been created
      options.showOn = 'button';
      hideBtn = true;

      // Register click event listener on the trigger
      $(options.trigger).on('click' + NAMESPACE, function() {
        var method = $this.datepicker('widget').is(':visible') ? 'hide' : 'show';

        $this.datepicker(method);

        return false;
      });
    }

    // Set the default culture
    $.datepicker.setDefaults($.datepicker.regional[options.culture]);

    // Remove unwanted jQuery UI datepicker options
    delete options.culture;
    delete options.trigger;

    $this.datepicker(options);

    if (hideBtn) {
      // Hide the trigger button jQuery added for us - we've got our own
      $this.nextAll(SELECTOR_DATEPICKER_BUTTON).remove();
    }
  };

  // Plugin destroyer
  datepicker.destroy = function() {
    var options = this.data(OPTIONS_ATTR);

    // Remove events listeners
    if (options.trigger) {
      $(options.trigger).off(NAMESPACE);
    }

    // Remove the options data attribute
    this.removeData(OPTIONS_ATTR);

    // Destroy the datepicker
    this.datepicker('destroy');
  };

  // Plugin definition
  $.backendPlugin(pluginName, datepicker);

})(jQuery);