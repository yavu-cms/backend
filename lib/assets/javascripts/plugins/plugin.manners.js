;(function($) {
  'use strict';

  /**
   * Manners backend plugin.
   *
   * Manners make you a cool boy that opens its links in a popup modal
   * instead of reloading the whole page.
   *
   * Available options:
   *
   *   - `url`: The URL to fetch (via an ajax GET request) to populate the modal.
   *     Its default value is `false`, which means that the URL should be taken
   *     from the `href` attribute of the target for this plugin.
   *
   * Usage examples:
   *
   *     $el.backendPlugin('manners'); // => will use `$el`'s `href` attribute as URL.
   *
   *     $el.backendPlugin('manners', { url: 'http://go.ril.la' });
   *       // ^> will use 'http://go.ril.la' as URL.
   */

  var pluginName = 'manners',
    manners      = {},
    defaults     = {
      // URL to open in the modal when clicked -- if false (default), the `href` attribute of
      // the target element for this plugin will be taken.
      url: false
    },
    // Constants
    NAMESPACE    = '.' + pluginName,
    MODAL_MARKUP       = '<div id="manners-modal" class="reveal-modal expand" data-animation="fadeAndPop"><div class="modal-body"></div><a class="close-reveal-modal">&#215;</a></div>',
    SELECTOR_MANNERS   = '#manners-modal',
    SELECTOR_CONTENT   = '.modal-body';

  // Create a modal and append it to the body.
  // This is a lazy initializer for the modal.
  function createModal() {
    var modal = $(SELECTOR_MANNERS);

    if (modal.length === 0) {
      modal = $(MODAL_MARKUP);
      modal.appendTo('body');
    }

    return modal;
  }

  // Plugin initializer
  manners.init = function(opts) {
    var options = $.extend({}, defaults, opts),
      $modal;

    // Update the URL
    options.url = options.url || this.attr('href');

    this.on('click' + NAMESPACE, function() {
      // Lazy initialization of the modal
      $modal = $modal || createModal();

      $.get(options.url, function(response) {
        var $body = $modal.find(SELECTOR_CONTENT).empty();

        // Initialize any backend plugin in the response
        $body.html(response).backendPlugin('initializer');

        $modal.foundation('reveal', 'open');
      });

      return false;
    });
  };

  // Plugin destroyer
  manners.destroy = function() {
    // Unregister event listeners
    this.off(NAMESPACE);
  };

  // Plugin definition
  $.backendPlugin(pluginName, manners);

})(jQuery);