;(function($) {
  'use strict';

  /**
   * Catch any click events on `<a>` elements, turn them into
   * Ajax GET requests, and update a target with the response.
   */

  var pluginName = 'updater',
    updater      = {},
    defaults     = {
      // Target element to update (required)
      target: ''
    },
    // Constants
    NAMESPACE    = '.' + pluginName;

  // Plugin initializer
  updater.init = function(opts) {
    var $this = this,
      options = $.extend({}, defaults, opts);

    $this.on('click' + NAMESPACE, function() {
      $.get($this.attr('href'), function(response) {
        var $target = $(options.target);

        // Initialize any backend plugin in the response
        $target.html(response).backendPlugin('initializer');
      });

      return false;
    });
  };

  // Plugin destroyer
  updater.destroy = function() {
    // Unregister event listeners
    this.off(NAMESPACE);
  };

  // Plugin definition
  $.backendPlugin(pluginName, updater);

})(jQuery);