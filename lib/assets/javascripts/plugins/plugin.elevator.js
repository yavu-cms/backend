;(function($) {
  'use strict';

  /**
   * Plugin that allows for adding a sort of 'Back to top' anchor on the page,
   * which will take the user to whatever the anchor's `href` value is (typically
   * the `id` selector of an element on the page.
   *
   * Usage example:
   *
   * Given the following markup:
   *
   *     <div class="container">
   *       <ul id="top">
   *         <li>Item 1</li>
   *         <li>Item 2</li>
   *         <li>...</li>
   *         <li>Item 50</li>
   *         <li>Item 51</li>
   *       </ul>
   *       <a href="#top" id="anchor">Go back to top</a>
   *     </div>
   *
   * In order to use the elevator plugin, you could:
   *
   *     $('#anchor').backendPlugin('elevator', { target: '.container' });
   *
   * After that, the `#anchor` element will be hidden if the `scrollTop` of
   * `.container` is less than `100px` (the default `threshold`), and it will scroll
   * `.container` to `#top` when clicked.
   */

  var pluginName = 'elevator',
    elevator     = {},
    defaults     = {
      // Selector to use as the target element - the one(s) which `scroll` events will be bound to.
      target:        'body',
      // A value in px indicating the scroll span at which the anchor will become visible.
      threshold:     100,
      // The duration in milliseconds of the fadeIn/Out animation of the anchor.
      fadeDuration:  250,
      // Whether to trigger the scroll event on initialization for proper setup
      triggerOnInit: true
    },
    // Plugin-specific constants
    NAMESPACE    = '.' + pluginName,
    TARGET_ATTR  = pluginName + '-target';

  // Plugin initializer
  elevator.init = function(opts) {
    var $this      = this,
      options      = $.extend({}, defaults, opts),
      $target      = $(options.target),
      $destination = $($this.attr('href'));

    // Store the target selector for future retrieval
    $this.data(TARGET_ATTR, options.target);

    // Register event listeners
    $this.on('click' + NAMESPACE, function() {
      $target.scrollTop($destination.scrollTop());

      return false;
    });

    $target.on('scroll' + NAMESPACE, function(e) {
      // Check if the anchor (`$this`) should or should not be visible
      if ($(e.target).scrollTop() < options.threshold) {
        $this.fadeOut(options.fadeDuration);
      } else {
        $this.fadeIn(options.fadeDuration);
      }
    });

    // Force the calculation of the visibility of the anchor
    if (options.triggerOnInit) {
      $target.trigger('scroll');
    }
  };

  // Plugin destroyer
  elevator.destroy = function() {
    var $target = $(this.data(TARGET_ATTR));

    // Unregister event listeners
    this.off(NAMESPACE);
    $target.off(NAMESPACE);

    // Remove stored data
    this.removeData(TARGET_ATTR);
  };

  // Backend plugin definition
  $.backendPlugin(pluginName, elevator);

})(jQuery);