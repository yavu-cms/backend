;(function($) {
  'use strict';

  /**
   * Postbuster Backend Plugin.
   * This plugin busts any form submit event (by default) and replaces it
   * with a POST Ajax request to the same action. If a successful response
   * is received, the content of the plugin target element (`$this`)
   * will be replaced with the content of the response and a
   * `postbuster:success` event will be triggered on the plugin target
   * element.
   *
   * Usage example:
   *
   *     $el.backendPlugin('postbuster');
   */

  var pluginName = 'postbuster',
    postBuster   = {},
    defaults     = {
      // Which event types should be busted
      bust:   'submit',
      update: null
    },
    // Constants
    NAMESPACE    = '.' + pluginName;

  // Plugin initializer
  postBuster.init = function(opts) {
    var $this = this,
      options = $.extend({}, defaults, opts);

    options.update = options.update ? $(options.update) : $this;

    // Register buster for submit events
    $this.on(options.bust + NAMESPACE, '*', function(e) {
      var $target = $(e.target),
        values    = $target.serializeArray();

      e.preventDefault();

      // Add a postbuster value to the request
      values.push({ name: 'postbuster', value: 'postbuster' });

      $.post($target.attr('action'), values, function(response) {
        options.update.html(response);

        $this.trigger('postbuster:success');
      });

      return false;
    });
  };

  // Plugin destroyer
  postBuster.destroy = function() {
    // Unregister event listener
    this.off(NAMESPACE);
  };

  // Plugin definition
  $.backendPlugin(pluginName, postBuster);

})(jQuery);