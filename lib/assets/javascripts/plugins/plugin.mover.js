;(function($) {
  'use strict';

  /**
   * Mover JS plugin.
   *
   * This plugin simply appends an element to a destination.
   */

  var pluginName = 'mover',
    mover        = {},
    defaults     = {
      destination: 'body'
    },
    // Constants
    ORIGINAL_POSITION_ATTR = pluginName + '-original-position';

  // Plugin initializer
  mover.init = function(opts) {
    var options = $.extend({}, defaults, opts);

    this.data(ORIGINAL_POSITION_ATTR, this.parent());
    this.appendTo(options.destination);
  };

  // Plugin destroyer
  mover.destroy = function() {
    this.appendTo(this.data(ORIGINAL_POSITION_ATTR));
    this.removeData(ORIGINAL_POSITION_ATTR);
  };

  // Plugin definition
  $.backendPlugin(pluginName, mover);

})(jQuery);