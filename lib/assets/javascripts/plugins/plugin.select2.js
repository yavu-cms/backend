;(function($) {
  'use strict';

  /**
   * Plugin for an easy integration of select2 JS library into the project.
   * For more information on select2 options, refer to:
   *   http://ivaynberg.github.com/select2/
   *
   * Usage example:
   *
   *     $select.backendPlugin('select2');
   */
  var pluginName = 'select2',
    select2      = {},
    defaults     = {
        containerCss: { width: '100%' },
        // agregado por i18n
        // https://github.com/ivaynberg/select2/blob/master/select2_locale_es.js
        formatNoMatches: function () { return "No se encontraron resultados"; },
        formatInputTooShort: function (input, min) { var n = min - input.length; return "Por favor, introduzca " + n + " car" + (n == 1? "á" : "a") + "cter" + (n == 1? "" : "es"); },
        formatInputTooLong: function (input, max) { var n = input.length - max; return "Por favor, elimine " + n + " car" + (n == 1? "á" : "a") + "cter" + (n == 1? "" : "es"); },
        formatSelectionTooBig: function (limit) { return "Sólo puede seleccionar " + limit + " elemento" + (limit == 1 ? "" : "s"); },
        formatLoadMore: function (pageNumber) { return "Cargando más resultados..."; },
        formatSearching: function () { return "Buscando..."; }  
    };

  // Plugin initializer
  select2.init = function(opts) {
    var options = $.extend({}, defaults, opts);

    if (typeof options.ajax !== 'undefined') {
      if (typeof options.ajax.data === 'undefined') {
        options.ajax.data = function(term, page) {
          return { q: term }
        };
      }

      if (typeof options.ajax.results === 'undefined') {
        options.ajax.results = function(data, page) {
          return { results: data }
        };
      }

      if (typeof options.initSelection === 'undefined') {
        options.initSelection = function(element, callback) {
          var $el = $(element),
            url = options.ajax.url,
            ids;

          if (typeof options.ajax.textUrl !== 'undefined') {
            url = options.ajax.textUrl;
          }

          ids = $el.val().split(',');

          $.getJSON(url, { ids: ids }, function(data) {
            if (options.multiple) {
              callback(data);
            } else {
              callback(data[0]);
            }
          });
        };
      }

      if (typeof options.minimumInputLength === 'undefined') {
        options.minimumInputLength = 3;
      }

      options.escapeMarkup = false;
    }

    if (options.escapeMarkup === false) {
      options.escapeMarkup = function(m) { return m; };
    }

    // Set the property of create tags from input search,
    // only for tagging select2 helper
    if (typeof options.tags !== 'undefined') {
      if (typeof options.createSearchChoice === 'undefined') {
        options.createSearchChoice = function(term, date) {
          return { id: term, text: term };
        };
      }
    }

    // Initialize select2
    this.select2(options);
  };

  // Plugin destroyer
  select2.destroy = function() {
    // Destroy the select2 instance.
    this.select2('destroy');
  };

  // Plugin definition
  $.backendPlugin(pluginName, select2);

})(jQuery);
