;(function($) {
  'use strict';

  /**
   * Twitter Autocomplete JS plugin.
   *
   * This plugin uses Bloodhound as suggestion engine
   */

  var pluginName = 'autocomplete',
    autocomplete = {},
    defaults     = { 
      queryParam: 'q',
      object_method: 'name'
    };

  // Plugin initializer
  autocomplete.init = function(opts) {
    var options = $.extend({}, defaults, opts),
        searchUrl = [ options.searchUrl,
                      options.searchUrl.indexOf('?') >= 0 ? '&':'?',
                      options.queryParam + "=%QUERY" ].join('');
    
    // instantiate the bloodhound suggestion engine
    var source = new Bloodhound({
      datumTokenizer: Bloodhound.tokenizers.obj.whitespace('itemName'),
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      remote: {
        url: searchUrl,
        filter: function(list) {
          
          return $.map(list, function(item) { return { itemName: item[options.object_method] }; });
        }
      }
    });

    // initialize the bloodhound suggestion engine
    source.initialize();

    this.typeahead(null, {
      displayKey: 'itemName',
      source: source.ttAdapter()
    });
  };

  // Plugin destroyer
  autocomplete.destroy = function() {

  };

  // Plugin definition
  $.backendPlugin(pluginName, autocomplete);

})(jQuery);
