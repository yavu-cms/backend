(function($) {

  var pluginName = 'crudfilters',
    crudFilters = {},
    NAMESPACE = '.' + pluginName;

  function buildFilterSelector(key) {
    return '#filter_' + key;
  }

  crudFilters.init = function(opts) {
    var $this = this,
      $filtersContainer = $(opts.filtersSelector);

    $this.on('click' + NAMESPACE, '.remove-label', function(e) {
      var $el = $(this);

      e.preventDefault();

      // find field and delete value
      $filtersContainer
        .find(buildFilterSelector($el.data('key')))
        .val('')
        .trigger('change');

      // remove label for view
      $el.closest('.label').fadeOut(350);
      // submit form with new values
      $filtersContainer.find('.filters-form').trigger('submit');

      return false;
    });
  };

  crudFilters.destroy = function() {
    var $this = this;

    $this.off(NAMESPACE);
  };

  $.backendPlugin(pluginName, crudFilters);

})(jQuery);