;(function($) {
  'use strict';

  /**
   * This plugin allows for enhancing a regular form or text input
   * so that it performs a live-filtering of a set of elements, based on
   * the query entered by the user.
   *
   * The filtering is based on the `text` content of the element nodes,
   * and any matching element will get a specific class that indicates
   * its condition, as will also happen with the elements that do not
   * match, which will get a different class that indicates they aren't
   * amongst the matches.
   *
   * The filtering will be triggered by specific events which may be
   * emitted from the plugin target element or an inner element (a `delegate`).
   * Once the filtering is triggered, all the `elements` inside the `searchIn`
   * container will be processed.
   *
   * Please note that by default, all matching elements will be bubbled up to
   * the top of their container in order to raise some attention on them.
   * As this behavior may end up shuffling the pre-existing order of the elements
   * you might want to set the `reorderOnMatch` option to `false`.
   *
   * Usage example:
   *
   *     // Initialization with the defaults
   *     $input.backendPlugin('livefilter');
   *     // Initialization with some options
   *     $input.backendPlugin(
   *       'livefilter', { searchIn: '.container', elements: '.item' }
   *     );
   *
   *     // Manually triggering the search with a query string value
   *     $input.backendPlugin('livefilter', 'search', 'my query string');
   *
   *     // Clearing all the matching and non-matching marks
   *     // (sort of a "reset" thing)
   *     $input.backendPlugin('livefilter', 'clear');
   */

  var pluginName = 'livefilter',
    livefilter   = {},
    defaults     = {
      // Class to be added to indicate a matching element
      matchingClass:    'lf-match',
      // Class to be added to indicate a non-matching element
      nonMatchingClass: 'lf-no-match',
      // Where to search in. This should be a selector, and may match more than one element
      searchIn:         'body',
      // Optional selector to scope the `searchIn` element (for cases where it's not unique on the page).
      // This option allows special values: if the selector starts with '^', the closest matching element from the
      // plugin target will be used as the scope. For instance, '^.container' will use the closest '.container' element
      // from the target of the plugin using `$.fn.closest()`.
      // False means no scoping (default).
      searchScope:      false,
      // Element selector to use inside the `searchIn` container
      elements:         '> *',
      // When to trigger the search method. These events are expected to be received on the plugin target element
      // or on the delegates -- see `delegate` option
      triggerOn:        ['keyup'],
      // Optional selector for delegate (inner) elements that will be triggering the expected events
      delegate:         null,
      // Whether the submit event should be prevented from performing its default behavior
      preventSubmit:    false,
      // Milliseconds to wait before triggering the search. This aims at avoiding choking on too many events at the same time
      responseDelay:    500,
      // If true, the matching elements will be moved above the non-matching ones
      reorderOnMatch:   false,
      // If true, the filtering input will be hidden if no results are available
      hideWhenEmpty:    true
    },
    // Plugin-specific constants
    OPTIONS_ATTR  = pluginName + '-options',
    NAMESPACE     = '.' + pluginName,
    SPECIAL_SCOPE = '^';

  // Filter an element, returning `true` if its text matches `query`
  function filter($element, query) {
    return $element.text().match(new RegExp(query, 'i'));
  }

  function searchScope(selector, pivot) {
    if (selector) {
      if (selector.match(/^\^/)) {
        return pivot.closest(selector.slice(1));
      } else {
        return $(selector);
      }
    }
  }

  // Plugin initializer
  livefilter.init = function(opts) {
    var $this = this,
      options = $.extend({}, defaults, opts),
      respond = true,
      listenFn;

    // Function to be used to save the context of an event
    // when it is `setTimeout()`'d.
    function contextSaverFn(target) {
      var query = target.val().trim();

      if (query != '') {
        // Perform a search with the given query
        this.backendPlugin(pluginName, 'filter', query);
      } else {
        // If the query string is empty, clean up everything
        this.backendPlugin(pluginName, 'clear');
      }

      // Unlock the semaphore
      respond = true;
    }

    // Proxy function that handles an Event and delegates the
    // searching logic on the appropriate plugin method.
    function filterProxy(e) {
      var proxiedFn;

      if (respond) {
        // Lock the semaphore -- avoid duplicate work in parallel
        respond = false;

        proxiedFn = $.proxy(contextSaverFn, $this, $(e.target));

        setTimeout(proxiedFn, options.responseDelay);
      }
    }

    // Store the options for future reuse
    $this.data(OPTIONS_ATTR, options);

    // Prevent form submission if specified to do so
    if (options.preventSubmit) {
      $this.closest('form').on('submit' + NAMESPACE, function(e) {
        e.preventDefault();

        return false;
      });
    }

    // Register event listeners for the triggering events
    if (options.delegate) {
      listenFn = function(i, eventType) {
        $this.on(eventType + NAMESPACE, options.delegate, filterProxy);
      };
    } else {
      listenFn = function(i, eventType) {
        $this.on(eventType + NAMESPACE, filterProxy);
      };
    }

    $.each(options.triggerOn, listenFn);

    $this.backendPlugin(pluginName, 'reload');
  };

  // Search method. This will search and highlight any matching
  // elements in the `options.searchIn` element(s).
  // Usage example:
  //   $el.backendPlugin('livefilter', 'filter', 'Search query');
  livefilter.filter = function(query) {
    var options = this.data(OPTIONS_ATTR) || defaults,
      $search   = $(options.searchIn, searchScope(options.searchScope, this)),
      elements  = $search.find(options.elements),
      matching,
      nonMatching;

    this.trigger('filterStart');

    // Find all the matching elements
    matching = elements.filter(function() { return filter($(this), query); });
    // Calculate the ones that were left out
    nonMatching = elements.not(matching);

    // Apply classes
    matching.removeClass(options.nonMatchingClass).addClass(options.matchingClass);
    nonMatching.removeClass(options.matchingClass).addClass(options.nonMatchingClass);

    // Reorder if desired
    if (options.reorderOnMatch) {
      matching.prependTo($search);
    }

    this.trigger('filterEnd');
  };

  // Clear method.
  livefilter.clear = function() {
    var options = this.data(OPTIONS_ATTR) || defaults,
      $search   = $(options.searchIn, searchScope(options.searchScope, this));

    // Remove all matching and non-matching classes from the elements
    $search.find(options.elements)
      .removeClass([options.matchingClass, options.nonMatchingClass].join(' '));
  };

  // Reload method
  livefilter.reload = function() {
    var options = this.data(OPTIONS_ATTR) || defaults,
      $search   = $(options.searchIn, searchScope(options.searchScope, this));

    if (options.hideWhenEmpty) {
      if ($search.find(options.elements).length == 0) {
        this.hide();
      } else {
        this.show();
      }
    }
  };

  // Plugin destroyer
  livefilter.destroy = function() {
    var options = this.data(OPTIONS_ATTR) || defaults;

    // Remove the options `data-*` attribute
    this.removeData(OPTIONS_ATTR);

    // If submit was prevented, re-enable it
    if (options.preventSubmit) {
      this.closest('form').off(NAMESPACE);
    }

    // Unregister all event listeners
    this.off(NAMESPACE);
  };

  // Plugin definition
  $.backendPlugin(pluginName, livefilter);

})(jQuery);