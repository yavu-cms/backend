;(function($) {
  'use strict';

  /**
   * Backend plugins initializer that allows for declaring via a
   * `data-plugins` attribute in the HTML the plugins that any
   * element will use.
   *
   * For example, let's assume the following HTML is on the page:
   *
   *     <section id="container">
   *       <header>
   *         <h1>Some title</h1>
   *         <!--
   *           The following is the JSON-encoded and
   *           HTML-escaped version of:
   *           {
   *             animations: {
   *               animation: "marquee",
   *               duration: 500
   *             }
   *           }
   *           -->
   *         <h2 data-plugins="{&quot;animations&quot;:{&quot;animation&quot;:&quot;marquee&quot;,&quot;duration&quot;:500}}">
   *            Some animated subtitle
   *         </h2>
   *       </header>
   *       <article>
   *         <!--
   *           The following is the JSON-encoded and
   *           HTML-escaped version of:
   *           {
   *             hider: {},
   *             alerter: {
   *               log: false
   *             }
   *           }
   *           -->
   *         <p data-plugins="{&quot;hider&quot;:{},&quot;alerter&quot;:{&quot;log&quot;:false}}">
   *           Some hidden paragraph that will be visible under some
   *           circumstances and will alert without logging some stuff.
   *         </p>
   *       </article>
   *     </section>
   *
   * By using the Initializer backend plugin we can automatically setup
   * the plugins for the two elements inside `#container` which specify
   * that they will use backend plugins via the `data-plugins` attribute:
   *
   *     $('#container').backendPlugin('initializer');
   */
  $.backendPlugin('initializer', function() {
    var initializer = {},
      SELECTOR      = '*[data-plugins]';

    // Find the plugins inside the `$container` applying the function `fn`
    // to each of them.
    function crawl($container, fn) {
      $container.find(SELECTOR).each(function() {
        var $this = $(this),
          plugins = $this.data('plugins'),
          pluginName;

        for (pluginName in plugins) {
          if (plugins.hasOwnProperty(pluginName)) {
            fn($this, pluginName, plugins[pluginName]);
          }
        }
      });
    }

    // Find any `data-plugins` marked element inside `this`
    // and initialize the plugins they require
    initializer.init = function() {
      crawl(this, function($el, pluginName, pluginArguments) {
        $el.backendPlugin(pluginName, pluginArguments);
      });
    };

    // Find any `data-plugins` marked element inside `this`
    // and destroy (remove) the plugins
    initializer.destroy = function() {
      crawl(this, function($el, pluginName) {
        try {
          $el.backendPlugin(pluginName, 'destroy');
        } catch (error) {
          // Do nothing, ignore any errors here as `destroy` is not
          // a required member of the plugins per se.
        }
      });
    };

    return initializer;
  });

})(jQuery);