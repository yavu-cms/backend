;(function($) {
  'use strict';

  /**
   * Dropzone JS plugin.
   *
   * This plugin is a wrapper for Dropzone JS library.
   *
   * Usage:
   *
   * On any `<form>` tag you can apply this plugin like follows:
   *
   *     // With the defaults
   *     $form.backendPlugin('dropzone');
   *     // Or with some options
   *     $form.backendPlugin('dropzone', {});
   *
   * This will automatically add the required attributes and
   * elements to the form element.
   *
   * All the available options are the ones exposed by Dropzone.
   */

  var pluginName = 'dropzone',
    dropzone     = {},
    defaults     = {
      // Default message to show in the element. Set to false to avoid adding it.
      message:  'Drop your files here or click to select a file',
      // CSS class to add to the target element
      cssClass: 'plugin-dropzone',
      // Max Thumbnail file size (in MB) - Files over this limit won't get a thumbnail
      maxThumbnailFilesize: 4
    },
    // Constants
    CLASS_MESSAGE = 'message';

  // Plugin initializer
  dropzone.init = function(opts) {
    var options = $.extend({}, defaults, opts);

    // Add the message, if provided
    if (options.message) {
      this.append($('<span></span>').addClass(CLASS_MESSAGE).html(options.message));
    }
    delete options.message;

    this.addClass(options.cssClass);
    delete options.cssClass;

    // Initialize Dropzone with the given options
    this.dropzone(options);

    if (options.token) {
      this.backendPlugin(pluginName, 'on', 'sending', function(file, xhr, formData) {
        formData.append("authenticity_token", options.token);
      });
    }
  };

  // Plugin destroyer
  dropzone.destroy = function() {
    this.backendPlugin(pluginName, 'element').disable();
  };

  // Get the Dropzone element
  dropzone.element = function() {
    return Dropzone.forElement(this.get(0));
  };

  // Listen to an event on the Dropzone element
  dropzone.on = function(eventType, callback) {
    this.backendPlugin(pluginName, 'element').on(eventType, callback);
  };

  // Plugin definition
  $.backendPlugin(pluginName, dropzone);
})(jQuery);