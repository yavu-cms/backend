# Backend application

## JavaScript plugins framework

There's an ad-hoc JS plugins framework that enable anyone to define reusable components that can be used across the entire application.

The purpose of those is to settle a common ground and idiom for any UX interaction that must be developed through JavaScript.

The framework itself is thoroughly documented, but here's a summary of what it is and how it is used. For further reference, refer to the `lib/assets/javascripts/plugins/*.js` files.

Plugins can be defined providing a name for them and a definition function which must return an Object with the API they provide:

```javascript
$.backendPlugin('myNewPlugin', function() {
  // Plugin definition goes here.
  // This function must return a new Object
  // with the available methods.
});
```

Or they can be defined providing the name and the Object with the API they offer:

```javascript
$.backendPlugin('alerter', {
  init: function(options) {
    // Plugin initialization
  },
  // This plugin only provides one method: `alert`
  alert: function(message) {
    alert(message);
  },
  destroy: function(options) {
    // Plugin removal
  }
});
```

Once defined, plugins will be available through different initialization methods:

  - `data-plugins` attributes initialization and configuration via `backendPluginInitializer`:

```javascript
// This will look for any element inside `#container`
// that has the `data-plugins` attribute and will
// initialize the desired plugins.
// @see plugin.initializer.js
$('#container').backendPluginInitializer();
```

  - JS initialization and configuration:

```javascript
// This will initialize the plugin 'myNewPlugin' with
// the given options hash (if provided, as they are optional).
$('.myElements').backendPlugin('myNewPlugin', options);
```

Also, the plugins defined can expose an API that will be available through the invocation method:

```javascript
// This simply invokes the 'stop' method on the 'animations' backend plugin:
$('span.animated').backendPlugin('animations', 'stop');

// This invokes the 'alert' method on the 'alerter' backend plugin with
// a message argument:
$('body').backendPlugin('alerter', 'alert', 'This is the message to alert');

// This invokes the method 'hide' on the 'hider' backend plugin and provides
// an argument (an options Object) that will be fed into the plugin's method:
$('.myElements').backendPlugin('hider', 'hide', { animate: true, duration: 500 });
```

### Easing the use of the Initializer plugin: Backend Plugin View Helper

There are 2 view helpers that allow for the inclusion of Backend Plugins configuration in the HTML elements in an easy and convenient way: `plugins` and `escape_configuration`.

#### plugins view helper

The `plugins` helper returns a `data-plugins` attribute which can be directly output inside an HTML element, like follows:

```erb
<span <%= plugins indicators: { duration: 500 } %> class="shiny-element">
  Whoah! This truly <em>is</em> a shiny element!
</span>
```

Which will end up rendered like this:

```html
<span data-plugins="{&quot;indicators&quot;:{&quot;duration&quot;:500}}" class="shiny-element">
  Whoah! This truly <em>is</em> a shiny element!
</span>
```

#### escape_configuration view helper

If you just want to have your configuration escaped, and want to manually add the `data-plugins` attribute into your view, you can use the `escape_configuration` view helper:

```erb
<span data-plugins="<%= escape_configuration indicators: { duration: 500 } %>" class="shiny-element">
  Whoah! This truly <em>is</em> a shiny element!
</span>
```

Will in turn render something similar to this:

```html
<span data-plugins="{&quot;indicators&quot;:{&quot;duration&quot;:500}}" class="shiny-element">
  Whoah! This truly <em>is</em> a shiny element!
</span>
```

### Initializing the Initializer

As the `initializer` plugin is the desired way of configuring JS plugins - hence, adding UX interactions to pages -, `application.js` has been gifted with a dead-simple snippet that will run the `initializer` plugin on any element in the page that has the class `auto-initialize`:

```javascript
// In application.js
jQuery(function($) {
  // Automatically call the Initializer backend plugin on .auto-initialize elements
  $('.auto-initialize').backendPlugin('initializer');
});
```

By doing this, there's *not even* need for a `<script>` tag that calls `$('.container').backendPlugin('initializer');`.

## JS Unit testing

A Qunit-based test suite for the JS framework is included.

In order to run it, you must have [phantomjs installed](http://phantomjs.org/download.html) and then execute:

```bash
$ rake test:js
```

Or open any of the `*.test.html` inside the `test/javascripts/*` directories in a browser.

## Custom Rake tasks

Some custom Rake tasks have been added to the project, tailored to our specific needs. Here is a comprehensive list of them:

### test:js

Runs the unit tests suite for our JS codebase in a headless WebKit browser.

**Options**:

- `path`: The path or glob that will be used to choose which tests will be run.

Usage:

```bash
# Runs the entire test suite
$ rake test:js

# Or just a specific test
$ rake "test:js[test/javascripts/plugins/plugin.livefilter/plugin.livefilter.test.html]"
```

### doc:js

Generates (and updates) the annotated source HTML files for JS scripts.

**Options**:

- `output`: The output directory to which HTML files should be written.
  May not exist and if that's the case, it will be created.
- `path`: The path or glob that will be used to choose which files will be processed by Rocco.

Usage:

```bash
# With the defaults:
$ rake doc:js

# Or with some options:
$ rake "doc:js[doc/javascripts/page-specific,app/assets/javascripts/*.js]"
```

### populate:all

Populates the database with stub data. This is a wrapper for the other `populate:*` tasks.
**Be careful**, this task does a `db:reset` before populating the database**.

**Options**:

This task takes no options.

Usage:

```bash
$ rake populate:all
```

### populate:articles

Populates Article model with stub data.

**Options**:

This task takes no options.

Usage:

```bash
$ rake populate:articles
```

### populate:tags

Populates Tag model with stub data.

**Options**:

This task takes no options.

Usage:

```bash
$ rake populate:tags
```


## SQL Server Configuration

To work with SQL Server add this lines to your `database.yml`:

```yaml
development:
  adapter:  sqlserver
  encoding: utf8
  database: el_dia_backend_development
  pool:     5
  username: desarrollo
  password: desarrollo
  host:     163.10.41.223
  mode:     dblib
```

## Further reading

You may want to take a look at the **Guides** available at [this project's Wiki](/desarrollo/el-dia-backend/wikis/index).

The annotated source for the JS plugins [is kept at `doc/javascripts`](/desarrollo/el-dia-backend/tree/master/doc/javascripts/plugins).
There you'll find some examples on implementing plugins.

The proposed structure for this CMS' REST API along with the guides on implementing the API controllers [resides at doc/api](/desarrollo/el-dia-backend/tree/master/doc/api#README).
