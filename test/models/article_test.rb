require 'test_helper'
require 'mocha/setup'

class ArticleTest < ActiveSupport::TestCase
  describe Article do
    describe '#set_defaults' do
      describe 'before validation should call the #set_defaults to set some default values' do
        after do
          Time.unstub :current
        end

        it 'sets the default values for new instances' do
          stub_time = Time.new(2002, 10, 31, 2, 2, 2)
          Time.stubs(:current).returns(stub_time)

          article = Article.new
          article.time.must_be_nil
          article.instance_variable_get('@autotagger').must_be_nil
          # Trigger the callback
          article.valid?
          article.time.must_equal stub_time
          article.instance_variable_get('@autotagger').must_be_instance_of SimpleTagging
        end

        it 'should not overwrite existing values with defaults' do
          stub_time = Time.new(1999, 1, 1, 10, 59, 59)
          Time.stubs(:current).returns(stub_time)

          time = Time.new(2000, 7, 7, 2, 2, 2)
          article = build(:article, title: 'article-no-defaults', time: time)
          # Trigger the callback
          article.valid?
          article.time.must_equal time
        end
      end
    end

    describe '#slug' do
      let(:article) { create :article, title: 'article for test' }
      let(:another_article) { create :article, title: 'article for test' }

      it 'is unique' do
        article.slug.wont_equal another_article.slug
      end

      describe 'when title is too long' do
        let(:max_length) { Article::MAX_SLUG_LENGTH }
        let(:article) { create(:article, title: 'ab' * max_length) }
        let(:expeced_length) { max_length + "-#{article.id}".length }

        it 'is truncated to `MAX_SLUG_LENGTH` chars' do
          expected_length = max_length + "-#{article.id}".length
          article.slug.length.must_equal expected_length
        end
      end
    end

    describe '#initial_visits' do
      describe 'when is the first time' do
        it 'calls to Accounting::MostVisitedArticles.initial_visits_for'
      end
      describe 'when is the nth time' do
        it 'returns the cached initial visits'
      end
    end

    describe '#initial_likes' do
      describe 'when is the first time' do
        it 'calls to Accounting::MostVisitedArticles.initial_votes_for and #first'
      end
      describe 'when is the nth time' do
        it 'returns the cached initial likes'
      end
    end

    describe '#initial_dislikes' do
      describe 'when is the first time' do
        it 'calls to Accounting::MostVisitedArticles.initial_votes_for and #last'
      end
      describe 'when is the nth time' do
        it 'returns the cached initial dislikes'
      end
    end

    describe 'initial_visits=' do
      it 'sets initial_visits_changed flag in true when there was a change value'
    end
    describe 'initial_likes=' do
      it 'sets initial_likes_changed flag in true when there was a change value'
    end
    describe 'initial_dislikes=' do
      it 'sets initial_dislikes_changed flag in true when there was a change value'
    end

    describe '#set_initial_votes' do
      it 'calls Voting::ArticleVote.set_initial_votes_for'
    end

    describe '#set_initial_visits' do
      it 'calls Accounting::MostVisitedArticles.set_initial_visits_for'
    end

    describe '#serialized_related=' do
      before do
        article_1 = create :article, title: 'First article'
        article_2 = create :article, title: 'Second article'
        article_3 = create :article, title: 'Third article'
        @article = create :article, title: 'SUT'
        @articles = [article_1, article_2, article_3]
        @article_ids = @articles.map(&:id).join(',')
      end

      it 'sets related articles from a comma-delimited string' do
        @article.serialized_related = @article_ids
        @article.articles.sort.must_equal @articles.sort
      end

      it 'ignores duplicates' do
        @article.serialized_related = [@article_ids, @article_ids].join(',')
        @article.articles.sort.must_equal @articles.sort
      end

      it 'avoids circular relations' do
        @article.serialized_related = @article.id.to_s
        @article.articles.must_be_empty
      end
    end

    describe '#serialized_related' do
      let(:article_1) { build(:article) }
      let(:article_2) { build(:article) }
      let(:article_3) { build(:article) }
      let(:articles) { [article_1, article_2, article_3] }
      let(:article) { build(:article, articles: articles) }

      it 'returns a comma-delimited string with the related tags' do
        article.serialized_related.must_match Regexp.new articles.collect(&:id).join(',\s?')
      end
    end

    describe '#serialized_tags=' do
      before do
        tag_1 = create(:tag, name: 'Tag 1')
        tag_2 = create(:tag, name: 'Tag 2')
        tag_3 = create(:tag, name: 'Tag 3')
        @article = create :article
        @tags = [tag_1, tag_2, tag_3]
        @tag_names = @tags.map(&:name).join(',')
      end

      it 'sets tags from a comma-delimited string' do
        @article.serialized_tags = @tag_names
        @article.tags.sort.must_equal @tags.sort
      end

      it 'ignores duplicates' do
        @article.serialized_tags = [@tag_names, @tag_names].join(',')
        @article.tags.sort.must_equal @tags.sort
      end

      it 'only uses root tags to relate with the article' do
        root_tag = @tags.first
        combined = @tags.last
        root_tag.combines! combined
        @article.serialized_tags = combined.name
        @article.tags.must_equal [root_tag]
      end
    end

    describe '#serialized_tags' do
      let(:tag_1) { create :tag, name: 'Tag 1' }
      let(:tag_2) { create :tag, name: 'Tag 2' }
      let(:tag_3) { create :tag, name: 'Tag 3' }
      let(:tags) { [tag_1, tag_2, tag_3] }
      let(:article) { create :article, tags: tags }

      it 'returns a comma-delimited string with the related tags' do
        article.serialized_tags.must_match Regexp.new tags.join(',\s?')
      end
    end

    describe '#autotag' do
      before do
        Setting['articles.autotag'] = true
      end

      let(:tag_1) { create :tag, name: 'Tag 1' }
      let(:tag_2) { create :tag, name: 'Tag 2' }
      let(:tags) { [tag_1, tag_2] }
      let(:article) { create :article, title: 'ejemplo', body: 'Necesito que guarde, guarde, guarde, guarde esto.', tags: tags }

      it 'must save all tags found in an article body' do
        (%w(guarde) - article.tags.map(&:name)).must_be_empty
      end

      it 'must keep previously-saved tags' do
        (tags.map(&:name) - article.tags.map(&:name)).must_be_empty
      end
    end

    describe '#copy' do
      let(:stub_time) { Time.new 2000, 1, 1, 12, 0, 0 }

      before do
        Time.stubs(:current).returns(stub_time)
      end

      after do
        Time.unstub(:current)
      end

      it 'creates a new record without saving it' do
        original_article = create :article

        copy = original_article.copy
        Article.count.must_equal 1
        copy.must_be :new_record?
      end

      it 'must update the copied article attributes with new params' do
        original_article = create :article, title: 'old title', body: 'old body'
        article_params = { title: 'new title', heading: 'old heading', body: 'new body' }

        original_article.title.must_equal 'old title'
        original_article.body.must_equal 'old body'
        copy = original_article.copy(article_params)
        copy.title.must_equal 'new title'
        copy.body.must_equal 'new body'
        copy.heading.must_equal 'old heading'
      end

      it 'has a diferent time' do
        original_article = create :article

        copy = original_article.copy
        copy.time.must_equal stub_time
      end

      # Relations copy check
      it 'must assign the same tags' do
        original_article = create :copyable_article

        copy = original_article.copy
        copy.tags.must_equal original_article.tags
      end

      it 'must assign the same related articles' do
        original_article = create :copyable_article

        copy = original_article.copy
        copy.articles.must_equal original_article.articles
      end

      it 'must assign the same article_media' do
        original_article = create :copyable_article

        copy = original_article.copy
        mapper = proc { |am| [am.caption, am.medium_id, am.main, am.order] }
        copy.article_media.map(&mapper).must_equal original_article.article_media.map(&mapper)
      end
    end

    describe '#to_s' do
      let(:article) { build :article }

      it 'returns the title of the article' do
        article.to_s.must_equal article.title
      end
    end


    describe 'Validations' do
      describe 'when title is not present' do
        it 'fails' do
          article = Article.new
          article.wont_be :valid?
          article.errors[:title].must_include I18n.t('errors.messages.blank')
          article.errors[:section].must_include I18n.t('errors.messages.blank')
        end
      end

      describe 'when title and section are present' do
        let(:section) { create :section }

        it 'succeeds' do
          article = Article.new title: 'Some title', section: section
          article.must_be :valid?
        end
      end

      describe 'when section supplement & edition supplement' do
        let(:supplement)         { create :supplement }
        let(:section)            { create :section, supplement: supplement }
        let(:edition)            { create :edition, supplement: supplement }
        let(:article)            { build :article, section: section, edition: edition }
        # unmatching data
        let(:another_supplement) { create :supplement }
        let(:another_edition)    { create :edition, supplement: another_supplement }

        it 'succeeds when they match' do
          article.must_be :valid?
        end

        it 'fails when they dont match' do
          article.edition = another_edition
          article.errors[:edition].include? I18n.t('activerecord.errors.models.article.attributes.edition.different_supplement')
          article.wont_be :valid?
        end
      end

    end

    describe '#related' do
      let(:article) { create(:article, title: 'Article A') }
      let(:another_article) { create :article, title: 'Article B' }

      before do
        article.articles << another_article
      end

      it 'should relate articles in both directions' do
        article.related.must_include another_article
        another_article.related.must_include article
      end
    end

    describe '.search' do
      describe 'when no parameters are provided' do
        it 'returns all articles' do
          Article.search.must_equal Article.all
        end
      end

      describe 'when a query is provided' do
        before do
          @article1 = create(:article, title: 'AAAA')
          @article2 = create(:article, title: 'BBBB')
          @article3 = create(:article, title: 'CCCC')
          @article4 = create(:article, title: 'Yet another article')
        end

        it 'only returns the matching articles' do
          results = Article.search query: 'BBB'
          results.count.must_equal 1
          results.first.must_equal @article2
        end

        it 'returns an empty set if there are no matches' do
          results = Article.search query: 'jajqr'
          results.must_be_empty
        end

        it 'excludes any articles in params[:except] if provided' do
          results = Article.search except: @article1
          expected = Article.all.but @article1
          results.must_equal expected
        end
      end
    end

    describe '.but' do
      before do
        @article1 = create(:article, title: 'AAAA')
        @article2 = create(:article, title: 'BBBB')
        @article3 = create(:article, title: 'CCCC')
      end

      it 'returns all articles except the one to be excluded' do
        Article.count.must_equal Article.but(@article1).count + 1
        Article.but(@article1).wont_include @article1
      end

      it 'only takes persisted objects into account' do
        article = build(:article, title: 'Non-persisted article')
        Article.count.must_equal Article.but(article).count
      end
    end

    describe '#visible?' do
      describe 'when associated to a visible edition' do
        let(:edition) { build :edition, is_visible: true }
        let(:article) { build :article, edition: edition }

        it 'returns true' do
          article.must_be :visible?
        end
      end

      describe 'when associated to a non-visible edition' do
        let(:edition) { build :edition, is_visible: false }
        let(:article) { build :article, edition: edition }

        it 'returns true' do
          article.wont_be :visible?
        end
      end

      describe 'when associated to a visible section' do
        let(:section) { build :section, is_visible: true }
        let(:article) { build :article, section: section }
        it 'returns true' do
          article.must_be :visible?
        end

      end

      describe 'when associated to a non-visible section' do
        let(:section) { build :section, is_visible: false }
        let(:article) { build :article, section: section }
        it 'returns false' do
          article.wont_be :visible?
        end

      end

    end

    describe '#time' do
      let(:article) { build :article, time: Time.current }
      it 'returns article time with current timezone' do
        article.time.wont_be :utc?
      end
    end

    describe '#time=' do
      let(:article) { build :article }
      describe 'when passing a time string' do
        it 'set a ActiveSupport::TimeWithZone instance' do
          article.time = '16:07'
          article.attributes['time'].must_be_instance_of ActiveSupport::TimeWithZone
        end
      end
      describe 'when passing a time object' do
        it 'set a ActiveSupport::TimeWithZone instance' do
          article.time = Time.current
          article.attributes['time'].must_be_instance_of ActiveSupport::TimeWithZone
        end
      end
    end

    describe '#date' do
      describe 'when has edition' do
        let(:article) { create :article }

        describe 'when Setting["use_edition_date"] is on' do
          before { Setting['use_edition_date'] = true }
          it 'returns the edition date' do
            article.date.must_equal article.edition.date
          end
        end

        describe 'when Setting["use_edition_date"] is off' do
          before { Setting['use_edition_date'] = false }

          it 'returns the article date from its time field' do
            article.date.must_equal article.time.to_date
          end
        end
      end
      describe 'when has no edition or edition has not date' do
        before { Setting['use_edition_date'] = true }

        describe 'when is persisted' do
          let(:article) { create :article_without_edition }

          it 'returns created_at article attribute' do
            article.date.must_equal article.created_at.to_date
          end
        end

        describe 'when is a new record' do
          let(:article) { build :article_without_edition }

          it 'returns today date' do
            stub_date = Date.new(2002, 10, 31)
            Date.stubs(:today).returns stub_date
            article.date.must_equal stub_date
            Date.unstub(:today)
          end
        end
      end
    end

    describe '#full_date' do
      let(:article)   { build :article }
      let(:stub_time) { Time.current   }
      let(:stub_date) { Date.today     }
      before do
        Setting['use_edition_date'] = true
        article.expects(:date).returns(stub_date).times(3)
        article.expects(:time).returns(stub_time).times(3)
      end

      it 'must return an instance of Datetime' do
        article.full_date.must_be_instance_of DateTime
      end

      it 'must return correct date and time' do
        datetime = article.full_date
        datetime.year.must_equal    stub_date.year
        datetime.month.must_equal   stub_date.month
        datetime.day.must_equal     stub_date.day
        datetime.hour.must_equal    stub_time.hour
        datetime.minute.must_equal  stub_time.min
        datetime.second.must_equal  stub_time.sec
      end

    end

    describe '#vote' do
      before do
        @article = build :article
      end
      it 'must call to Voting::ArticleVote.make' do
        Accounting::MostVotedArticles.expects(:make).with(@article, 'user1', 'like').once()
        @article.vote('like', 'user1')
        Accounting::MostVotedArticles.unstub(:make)

      end
    end

    describe '#votes' do
      before do
        @article = build :article
      end
      it 'must call Voting::Vote.votes' do
        Accounting::MostVotedArticles.expects(:votes).with(@article, only_real_votes: false).once()
        @article.votes
        Accounting::MostVotedArticles.unstub(:votes)
      end
    end

    describe '#visit!' do
      before do
        @article1 = create(:article, title: 'AAAA')
      end

      describe 'when accounting engine is working' do
        before do
          Accounting::MostVisitedArticles.stubs(:visit!).with(@article1, 1).returns(true)
        end

        after do
          Accounting::MostVisitedArticles.unstub(:visit!)
        end

        it 'returns true' do
          @article1.visit!.must_equal true
        end
      end

      describe 'when accounting engine is no working' do
        before do
          Accounting::MostVisitedArticles.stubs(:visit!).with(@article1, 1).raises
        end

        after do
          Accounting::MostVisitedArticles.unstub(:visit!)
        end

        it 'returns false' do
          @article1.visit!.must_equal false
        end
      end

    end

    describe '#visits' do
      before do
        @article1 = create(:article, title: 'AAAA')
      end

      describe 'when accounting engine is working' do
        before do
          Accounting::MostVisitedArticles.stubs(:visits).with(@article1, only_real_visits: false).returns(100)
        end

        after do
          Accounting::MostVisitedArticles.unstub(:visits)
        end

        it 'returns amount of visits' do
          @article1.visits.must_equal 100
        end
      end

      describe 'when accounting engine is no working' do
        before do
          Accounting::MostVisitedArticles.stubs(:visits).with(@article1, only_real_visits: false).raises
        end

        after do
          Accounting::MostVisitedArticles.unstub(:visits)
        end

        it 'returns 0 visits' do
          @article1.visits.must_equal 0
        end
      end
    end

    describe '#main_image' do
      describe 'when it has a main image' do
        let(:image) { create :article_medium }
        let(:article_medium_1) { create :article_medium, main: true, medium: build(:file_medium) }
        let(:article_medium_2) { create :article_medium, main: false }
        let(:article_with_main_image) { create :article, article_media: [image, article_medium_1, article_medium_2] }

        it 'returns the main image' do
          article_with_main_image.main_image.must_equal image
        end

        it 'returns an image medium' do
          article_with_main_image.main_image.medium.must_be_instance_of ImageMedium
        end
      end

      describe 'when it doesn\'t have a main image' do
        let(:article_without_image) { create :article }

        it 'returns nil' do
          article_without_image.main_image.must_be_nil
        end
      end
    end

    it 'wont be destroyed if there are associated article_cover_articles_component_configurations' do
      article = create(:article)
      config = create(:cover_articles_component_configuration)
      view = config.component.views.create name: 'This view rocks'
      ArticlesCoverArticlesComponentConfiguration.create! article: article,
        cover_articles_component_configuration: config,
        view: view
      article = Article.find(article.id)
      article.wont_be :destroy
    end

    describe 'Elasticsearch support' do
      let(:article) { build :article }

      describe '.perform_search' do
        describe 'when a section is provided' do
          it 'only filters by section.name'
        end

        describe 'when a date is provided' do
          it 'only filters by edition.date'
        end

        describe 'when a query_string is provided' do
          it 'only filters by its text/html fields'
        end

        describe 'when no arguments are provided' do
          it 'uses an empty query'
        end

        describe 'when all arguments are provided' do
          it 'filters by all of them in a must query'
        end
      end

      describe '#indexable?' do
        describe 'when the article is visible' do
          before { article.expects(:visible?).returns(true).once }

          it 'is indexable' do
            article.must_be :indexable?
          end
        end

        describe 'when the article is not visible' do
          before { article.expects(:visible?).returns(false).once }

          it 'is not indexable' do
            article.wont_be :indexable?
          end
        end
      end

      describe '#as_indexed_json' do
        let(:expected) { { 'some' => 'hash', 'section' => { 'color' => nil } } }
        let(:args) do
          Hash[only: [:slug, :title, :heading, :lead, :time],
               include: {
                 tags: { only: [:name] },
                 edition: { methods: [:name_or_date], only: [:name, :date] },
                 section: { only: [:slug, :name] }
               }]
        end
        before { article.expects(:as_json).with(args).returns(expected).once }

        it 'uses #as_json to build a hash with the relevant data' do
          article.as_indexed_json.must_equal expected
        end
      end

      describe '#stripped_html' do
        before do
          article.body = "<p>Some paragraph.\r\n\r\n\t</p><iframe src='sarasa'></iframe><p>Hola hola hola.\r\n</p>"
        end

        let(:expected_body) { "Some paragraph.\n\tHola hola hola.\n" }

        it 'must clean the article body from html tags and control characters' do
          article.stripped_body.must_equal expected_body
        end
      end
    end

    describe 'as_api_resource' do
      describe 'when limit is set' do
        before do
          @tag_1 = create(:tag, name: 'Tag 1').reload
          @tag_2 = create(:tag, name: 'Tag 2').reload

          @tags = [@tag_1, @tag_2]

          @related_article_1 = build(:article)
          @related_article_2 = build(:article)
          @related_articles = [@related_article_1, @related_article_2]

          @article = create :article, tags: @tags, articles: @related_articles
          # Main image for this article
          create :article_medium, main: true, article: @article

          @embedded = create(:embedded_medium)
          create(:article_medium, article: @article, medium: @embedded)

          @audio = create(:audio_medium)
          create(:article_medium, article: @article, medium: @audio)

          @file = create(:file_medium)
          create(:article_medium, article: @article, medium: @file)

          @article.api_resource_settings = { media_limit: 5, related_limit: 5, tags_limit: 5 }
        end

        it 'must return expected attributes' do
          @resource = @article.as_api_resource

          @resource.file_media.must_equal @article.article_media.file.map(&:as_api_resource)
          @resource.image_media.must_equal @article.article_media.image.map(&:as_api_resource)
          @resource.audio_media.must_equal @article.article_media.audio.map(&:as_api_resource)
          @resource.embedded_media.must_equal @article.article_media.embedded.map(&:as_api_resource)

          @resource.tags.must_equal @article.tags.map(&:as_api_resource)
          @resource.related.must_equal @article.related.map(&:as_api_resource)
        end
      end

      describe 'when limit is not set' do
        before do
          @article = create :article
          create :article_medium, main: true, article: @article
        end

        it 'must return expected attributes' do
          @resource = @article.as_api_resource
          @resource.id.must_equal @article.slug
          @resource.slug.must_equal @article.slug
          @resource.lead.must_equal @article.lead
          @resource.body.must_equal @article.body
          @resource.title.must_equal @article.title
          @resource.heading.must_equal @article.heading
          @resource.signature.must_equal @article.signature
          @resource.section_id.must_equal @article.section.slug
          @resource.main_image.must_equal @article.main_image.as_api_resource
          @resource.date.must_equal @article.date
          @resource.time.must_equal @article.time

          @resource.embedded_media.must_equal nil
          @resource.audio_media.must_equal nil
          @resource.image_media.must_equal nil
          @resource.file_media.must_equal nil
          @resource.related.must_equal nil
          @resource.tags.must_equal nil
        end
      end
    end

    describe 'Destruction' do
      describe 'when it is referenced by a component value' do
        let(:component_value) { create :component_value_article, component_field: create(:component_field_model_article, with_component: true) }
        let(:article) { component_value.referable }

        it "can't be destroyed" do
          article.destroy.must_equal false
          article.wont_be :destroyed?
        end
      end

      describe "when it isn't referenced by a component value" do
        let(:article) { create :article }

        it 'can be destroyed' do
          article.destroy
          article.must_be :destroyed?
          ->{ article.reload }.must_raise ActiveRecord::RecordNotFound
        end
      end
    end

    describe 'scopes' do
      describe '.all_them' do
        it 'must return eager load and ordered by date and time desc' do
          object = Object.new
          object.expects(:ordered).once
          Article.expects(:eager_load).with(:edition,:section).returns(object).once
          Article.all_them
          Article.unstub(:eager_load)
        end
      end

      describe '.unscoped_visible' do
        it 'must return all_them where is_visible true and edtion and sections are visible too' do
          object = Object.new
          object.expects(:where).with(is_visible: true).once
          editions = Object.new
          editions.expects(:merge).with(Edition.unscoped.visible).returns(object).once
          sections = Object.new
          sections.expects(:merge).with(Section.unscoped.visible).returns(editions).once
          Article.expects(:unscoped).returns(sections).once
          Article.unscoped_visible
          Article.unstub(:unscoped)
        end
      end

      describe '.visible' do
        it 'must return all_them where is_visible true and edtion and sections are visible too' do
          object = Object.new
          object.expects(:merge).with(Article.unscoped_visible).once
          Article.expects(:all_them).returns(object).once
          Article.visible
          Article.unstub(:all_them)
        end
      end

      describe '.for_cover' do
        it 'must return all_them where hide_from_cover is false' do
          object = Object.new
          object.expects(:where).with(hide_from_cover: false).once
          Article.expects(:visible).returns(object).once
          Article.for_cover
          Article.unstub(:visible)
        end
      end

      describe '.with_main_image' do
        before do
          @article_medium_1 = create :article_medium, main: true
          article_medium_2 = create :article_medium, main: false
        end

        it 'returns articles with main images' do
          Article.with_main_image.must_equal [@article_medium_1.article]
        end
      end

      describe '.on_active_edition' do
        before do
          supplement = create :supplement
          section = create :section, supplement: supplement
          @edition = create :edition, supplement: supplement
          old_edition = create :edition, supplement: supplement

          @edition.become_active!

          @article_1 = create :article, section: section, edition: @edition
          @article_2 = create :article, section: section, edition: @edition
          article_3 = create :article, section: section, edition: old_edition
        end

        it 'returns articles whose edition is active' do
          Article.on_active_edition.must_equal [@article_1, @article_2]
        end
      end

      describe '.with_edition_date' do
        before do
          @edition = create :edition_with_articles, date: '2013-03-01'
          @another_edition = create :edition_with_articles, date: '2013-03-02'
        end
        it 'returns articles for a given date' do
          Article.all_them.with_edition_date('01/03/2013').to_a.must_equal @edition.articles.to_a
        end
        it 'returns empty array if no articles found' do
          Article.all_them.with_edition_date('01/04/2013').must_equal []
        end
        it 'raises an error given an incorrect date string' do
          # Raises invalid date
          ->{ Article.all_them.with_edition_date('01-03-2013') }.must_raise ArgumentError
        end
      end

      describe '.with_section' do
        # Supplements:
        let(:supplement)         { create :supplement }
        let(:another_supplement) { create :supplement }
        # Sections:
        let(:sports_section)     { create :section, name: 'sports', supplement: supplement }
        let(:politics_section)   { create :section, name: 'politics', supplement: another_supplement }
        let(:technology_section) { create :section, name: 'technology', supplement: supplement }
        # Editions:
        let(:sports_edition)   { create :edition, supplement: supplement }
        let(:politics_edition) { create :edition, supplement: another_supplement }

        before do
          # Articles:
          @sports_articles   = create_list(:article, 2, section: sports_section, edition: sports_edition)
          @politics_articles = create_list(:article, 2, section: politics_section, edition: politics_edition)
        end

        it 'returns articles for a section based on section id' do
          Article.all_them.with_section(sports_section.id).must_equal @sports_articles
        end

        it 'returns empty array if no articles found for a given section' do
          Article.all_them.with_section(technology_section.id).must_equal []
        end
      end
    end

    describe 'as ExtraAttributeModelable' do
      before do
        ExtraAttribute.any_instance.stubs(:model_classes).returns({'section' => '1', 'article' => '1', 'edition' => '1'})
      end

      after do
        ExtraAttribute.any_instance.unstub(:model_classes)
      end

      describe 'when string extra attributes' do
        let(:article) { build :article }
        let(:edition) { build :edition }
        let(:section) { build :section }
        before do
          create :extra_attribute_string, name: 'string_value'
          edition.extra_attribute_model.string_value = 'by edition'
          section.extra_attribute_model.string_value = 'by section'
        end

        describe '#calculated_extra_attribute' do

          describe 'when nil at attribute' do

            it 'must return nil if nil at section and edition' do
              article.extra_attribute_model.string_value.must_be_nil
              article.calculated_extra_attribute('string_value').must_be_nil
            end

            it 'must return by edition if edition sets attribute' do
              article.edition = edition
              article.extra_attribute_model.string_value.must_be_nil
              article.calculated_extra_attribute('string_value').must_equal 'by edition'
            end

            it 'must return by section if section sets attribute' do
              article.section = section
              article.extra_attribute_model.string_value.must_be_nil
              article.calculated_extra_attribute('string_value').must_equal 'by section'
            end

          end

          describe 'when value is by article at attribute' do
            before do
              article.extra_attribute_model.string_value = 'by article'
            end
            it 'must return by_article if nil at section and edition' do
              article.extra_attribute_model.string_value.must_equal 'by article'
              article.calculated_extra_attribute('string_value').must_equal 'by article'
            end

            it 'must return by article if edition sets attribute' do
              article.edition = edition
              article.calculated_extra_attribute('string_value').must_equal 'by article'
            end

            it 'must return by article if section sets attribute' do
              article.section = section
              article.calculated_extra_attribute('string_value').must_equal 'by article'
            end

          end
        end

      end

      describe 'when boolean extra attributes' do
        let(:article) { build :article }
        let(:edition) { build :edition }
        let(:section) { build :section }
        before do
          create :extra_attribute_boolean, name: 'boolean_value'
        end

        describe '#calculated_extra_attribute' do

          describe 'when nil at attribute' do

            it 'must return false if nil at section and edition' do
              article.extra_attribute_model.boolean_value.must_be_nil
              article.calculated_extra_attribute('boolean_value').must_be_nil
            end

            describe 'when edition and section are true' do
              before do
                edition.extra_attribute_model.boolean_value = true
                section.extra_attribute_model.boolean_value = true
                article.edition = edition
                article.section = section
              end
              it 'must return true if edition sets attribute' do
                article.extra_attribute_model.boolean_value.must_be_nil
                article.calculated_extra_attribute('boolean_value').must_equal true
              end

              it 'must return true if section sets attribute' do
                article.extra_attribute_model.boolean_value.must_be_nil
                article.calculated_extra_attribute('boolean_value').must_equal true
              end
            end

            describe 'when edition and section are false' do
              before do
                edition.extra_attribute_model.boolean_value = false
                section.extra_attribute_model.boolean_value = false
                article.section = section
                article.edition = edition
              end

              it 'must return false if edition sets attribute' do
                article.extra_attribute_model.boolean_value.must_be_nil
                article.calculated_extra_attribute('boolean_value').must_equal false
              end

              it 'must return false if section sets attribute' do
                article.extra_attribute_model.boolean_value.must_be_nil
                article.calculated_extra_attribute('boolean_value').must_equal false
              end
            end

            describe 'when edition is true and section is false' do
              before do
                edition.extra_attribute_model.boolean_value = true
                section.extra_attribute_model.boolean_value = false
                article.edition = edition
                article.section = section
              end
              it 'must return false if edition sets attribute' do
                article.extra_attribute_model.boolean_value.must_be_nil
                article.calculated_extra_attribute('boolean_value').must_equal true
              end

              it 'must return false if section sets attribute' do
                article.extra_attribute_model.boolean_value.must_be_nil
                article.calculated_extra_attribute('boolean_value').must_equal true
              end
            end

            describe 'when edition is false and section is true' do
              before do
                edition.extra_attribute_model.boolean_value = false
                section.extra_attribute_model.boolean_value = true
                article.edition = edition
                article.section = section
              end
              it 'must return false if edition sets attribute' do
                article.extra_attribute_model.boolean_value.must_be_nil
                article.calculated_extra_attribute('boolean_value').must_equal true
              end

              it 'must return false if section sets attribute' do
                article.extra_attribute_model.boolean_value.must_be_nil
                article.calculated_extra_attribute('boolean_value').must_equal true
              end
            end
          end

          describe 'when value is true at article' do
            before do
              article.extra_attribute_model.boolean_value = true
            end
            it 'must return true if nil at section and edition' do
              article.extra_attribute_model.boolean_value.must_equal true
              article.calculated_extra_attribute('boolean_value').must_equal true
            end

            it 'must return true if edition is set whetever value it has' do
              edition.extra_attribute_model.boolean_value = false
              article.edition = edition
              article.calculated_extra_attribute('boolean_value').must_equal true
            end

            it 'must return true if section is set whetever valu it has' do
              section.extra_attribute_model.boolean_value = false
              article.section = section
              article.calculated_extra_attribute('boolean_value').must_equal true
            end
          end
        end
      end
    end
    describe '#update_with_extra_attributes' do
      before do
        @article     = create :article
        eam          = create :extra_attribute_model, model: @article
        ea           = create :extra_attribute, name: 'disable_ads', model_classes: Hash['Article', 1]
        @good_params = Hash['extra_attribute_model_attributes', Hash['disable_ads', true]]
      end

      it 'succeeds when proper params are given' do
        @article.update_with_extra_attributes(@good_params).must_equal true
      end
    end
  end
end
