require 'test_helper'

class TagTest < ActiveSupport::TestCase
  describe Tag do
    describe 'Validations' do
      describe 'when name is not present' do
        it 'fails' do
          tag = Tag.new
          tag.wont_be :valid?
          tag.errors[:name].must_include I18n.t('errors.messages.blank')
        end
      end

      describe 'when name has less of three chars' do
        it 'fails' do
          tag = Tag.new(name: 'ab')
          tag.wont_be :valid?
          tag.errors[:name].must_include I18n.t('errors.messages.too_short', count: Tag::MINIMUM_NAME_LENGTH)
        end
      end

      describe 'when name is not unique' do
        it 'fails' do
          create(:tag, name: 'unique tag')

          tag = Tag.new(name: 'unique tag')
          tag.wont_be :valid?
          tag.errors[:name].must_include I18n.t('errors.messages.taken')
        end
      end

      describe 'when slug is not unique' do
        it 'fails' do
          create(:tag, name: 'tag name', slug: 'slug unique')

          tag = Tag.new(name: 'tag another name', slug: 'slug unique')
          tag.wont_be :valid?
          tag.errors[:slug].must_include I18n.t('errors.messages.taken')
        end
      end
    end

    describe 'to_s' do
      it 'returns name' do
        name = 'TAG'
        build(:tag, name: name).to_s.must_equal name
      end

      describe 'when tag has no name' do
        let(:tag) { build(:tag, name: nil) }
        it 'returns a string' do
          tag.to_s.must_be_instance_of String
        end
      end
    end

    describe 'slug' do
      let(:tag) { create(:tag) }

      it 'appends id to name' do
        tag.slug.split('-').last.must_equal tag.id.to_s
      end

      it 'a slug is assigned if none set when creating a new tag' do
        tag.slug.wont_be_nil
      end

      describe 'when slug is too long' do
        let(:max_length) { Tag::MAX_SLUG_LENGTH }
        let(:tag) { create(:tag, slug: 'ab' * max_length) }
        let(:expected_length) { max_length + "-#{tag.id}".length }

        it 'is truncated to `MAX_SLUG_LENGTH` chars' do
          tag.slug.length.must_equal expected_length
        end
      end
    end

    describe 'squish_name' do
      describe 'when name is not nil' do
        it 'must remove whitespaces on both ends' do
          tag = build(:tag, name: '  Name  ')

          tag.squish_name.must_equal 'Name'
        end

        it 'change consecutive whitespace groups into one space each' do
          tag = build(:tag, name: 'Name   of   the    tag')

          tag.squish_name.must_equal 'Name of the tag'
        end
      end

      describe 'when name is nil' do
        let(:tag) { build :tag, name: nil}

        it 'do nothing' do
          tag.squish_name.must_be_nil
        end
      end
    end

    describe 'combine_with' do
      let(:tag) { create(:tag) }
      let(:tag_1) { build(:tag, name: 'Tag 1') }
      let(:tag_2) { build(:tag, name: 'Tag 2') }
      let(:tag_3) { build(:tag, name: 'Tag 3') }

      it 'combine the articles' do
        another_tag = tag_1
        one_tag     = tag
        articles    = [create(:article), create(:article)]
        articles.each { |article| article.tags = [one_tag] }

        one_tag.combine_with! another_tag

        another_tag.articles.must_equal articles
        one_tag.articles.must_be_empty
      end

      it 'combine given tag with the combined' do
        combined     = [tag_2, tag_3]
        tag.combined = combined

        tag.combine_with! tag_1

        tag_1.combined.must_equal combined
      end
    end

    describe 'combine' do
      let(:tag) { create(:tag) }
      let(:tag_1) { build(:tag, name: 'Tag 1') }
      let(:tag_2) { build(:tag, name: 'Tag 2') }
      let(:tag_3) { build(:tag, name: 'Tag 3') }

      it 'combines a single tag' do
        tag.combines! tag_1

        tag_1.combiner.must_equal tag
        tag.combined.must_include tag_1
      end

      it 'combines multiple tags' do
        combined = [tag_1, tag_2]
        tag.combines! combined

        combined.map(&:combiner).uniq.must_equal [tag]
        tag.combined.must_include tag_1
        tag.combined.must_include tag_2
      end

      it 'does not overwrite combined tags' do
        tag.combines! [tag_1, tag_2]

        tag.combines! [tag_2, tag_3]

        tag.combined.must_equal [tag_1, tag_2, tag_3]
      end

      it 'updates all related articles tags' do
        articles = [create(:article), create(:article)]
        articles.each { |article| article.tags = [tag_1] }

        tag.combines! tag_1

        tag.articles.must_equal articles
        tag_1.articles.must_be_empty
      end

      it 'combines the tags that has combined tags already' do
        tag_2.combines! tag_3

        tag.combines! tag_2

        tag.combined.must_equal [tag_2, tag_3]
      end

      it 'collects all combined tags articles on the combiner tag' do
        articles1 = [create(:article), create(:article)]
        articles1.each { |article| article.tags = [tag_1] }

        articles2 = [create(:article), create(:article)]
        articles2.each { |article| article.tags = [tag_2] }

        articles3 = [create(:article), create(:article)]
        articles3.each { |article| article.tags = [tag_3] }

        all_articles = articles1 + articles2 + articles3

        tag_3.combines! tag_1

        tag_2.combines! tag_3

        tag.combines! tag_2

        tag.articles.must_equal all_articles
      end

      it 'combines the tag that has tagged articles and its combined tags has the same tagged articles' do
        articles = [build(:article), build(:article)]
        tag.articles = articles

        tag.combines! [tag_2, tag_3]

        tag.combines! [tag_1, tag_2]

        tag.articles.must_equal articles
      end
    end

    describe 'separate!' do
      let(:tag) { build(:tag) }
      let(:tag_1) { build(:tag, name: 'Tag 1') }

      it 'separate a tag from its combiner' do
        tag.combines! tag_1

        tag_1.separate!

        tag_1.combiner.must_be_nil
      end
    end

    describe 'similar' do
      let(:tag) { build(:tag, name:'Roar tag') }

      it 'get similar tags that starts with Ro' do
        create(:tag, name: 'Rock tag')
        create(:tag, name: 'Root tag')
        create(:tag, name: 'Rool tag')

        tag.similar(7).map(&:name).to_set.must_equal ['Rock tag', 'Root tag', 'Rool tag'].to_set
      end
    end

    describe 'search' do
      let(:tag) { create :tag, name: 'Tag hello' }
      let(:another_tag) { create :tag, name: 'Another hello tag' }

      before do
        @tag         = tag
        @another_tag = another_tag
      end

      after do
        Tag.unstub :all, :where, :but
      end

      it 'must filter result with query and except when are not blank' do
        params = { query: 'hello', except: @tag }

        Tag.expects(:all).returns(Tag).once
        Tag.expects(:where).with('name like :query', {query: '%hello%'}).returns(Tag).once
        Tag.expects(:but).with(@tag).returns([@another_tag]).once

        Tag.search(params).must_equal [@another_tag]
      end

      it 'must filter only for where when except param is blank' do
        params = { query: 'hello', except: nil }

        Tag.expects(:all).returns(Tag).once
        Tag.expects(:where).with('name like :query', {query: '%hello%'}).returns([@another_tag, @tag]).once

        Tag.search(params).must_equal [@another_tag, @tag]
      end

      it 'must filter only for but when query param is blank' do
        params = { query: nil, except: @tag }

        Tag.expects(:all).returns(Tag).once
        Tag.expects(:but).with(@tag).returns([@another_tag]).once

        Tag.search(params).must_equal [@another_tag]
      end

      it 'must returns all when params are blank' do
        params = { query: nil, except: nil }

        Tag.search(params).to_set.must_equal [@another_tag, @tag].to_set
      end
    end

    describe 'serialized_combined' do
      let(:tag) { build(:tag, name: 'My_tag_name', slug: 'My_tag_slug') }
      let(:tag_1) { build(:tag, name: 'Tag_1_name', slug: 'Tag_1_slug') }
      let(:tag_2) { build(:tag, name: 'Tag_2_name', slug: 'Tag_2_slug') }
      let(:tag_3) { build(:tag, name: 'Tag_3_name', slug: 'Tag_3_slug') }

      before do
        @tag = tag
      end

      it 'must return a string with names of tags separated by comma' do
        @tag.combined = [tag_1, tag_2, tag_3]
        @tag.save

        @tag.serialized_combined.must_equal 'Tag_1_name,Tag_2_name,Tag_3_name'
      end

      it 'must return am empty string when no have combined tags' do
        @tag.combined = []
        @tag.save

        @tag.serialized_combined.must_equal ''
      end
    end

    describe 'as_api_resource' do
      let(:tag) { create :tag }

      it 'must return expected attributes' do
        @resource = tag.as_api_resource
        @resource.id.must_equal tag.slug
        @resource.name.must_equal tag.name
      end
    end

    describe 'when create a new tag' do
      let(:tag) { Tag.new }

      it 'must not be hidden' do
        tag.hidden.must_equal false
      end
    end
  end
end
