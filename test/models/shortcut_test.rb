require 'test_helper'

class ShortcutTest < ActiveSupport::TestCase
  describe Shortcut do
    describe 'Validations' do
      describe 'when user_id is not present' do
        it 'fails' do
          shortcut = Shortcut.new(title: 'one title', url: 'http://www.example.com')
          shortcut.wont_be :valid?
          shortcut.errors[:user_id].must_include I18n.t('errors.messages.blank')
        end
      end

      describe 'when title is not present' do
        it 'fails' do
          shortcut = Shortcut.new(user_id: '1', url: 'http://www.example.com')
          shortcut.wont_be :valid?
          shortcut.errors[:title].must_include I18n.t('errors.messages.blank')
        end
      end

      describe 'when url is not present' do
        it 'fails' do
          shortcut = Shortcut.new(user_id: '1', title: 'one title')
          shortcut.wont_be :valid?
          shortcut.errors[:url].must_include I18n.t('errors.messages.blank')
        end
      end

      describe 'when url is already used for an user' do
        it 'fails' do
          create(:shortcut, user_id: '1', title: 'one title', url: 'http://www.example.com')

          shortcut = Shortcut.new(user_id: '1', title: 'another title', url: 'http://www.example.com')
          shortcut.wont_be :valid?
          shortcut.errors[:url].must_include I18n.t('errors.messages.taken')
        end
      end
    end

    describe 'sorted' do
      after do
        Shortcut.unstub :order
      end

      it 'gets menu shortcuts order by title' do
        second = build(:shortcut, show_in_menu: true, title: 'B Second title')
        first  = build(:shortcut, show_in_menu: true, title: 'A First title')
        third  = build(:shortcut, show_in_menu: true, title: 'C Third title')
        expected = [first, second, third]

        Shortcut.expects(:order).with('title ASC').returns(expected)
        Shortcut.sorted.must_equal expected
      end
    end

    describe 'for_menu' do
      after do
        Shortcut.unstub :where
      end

      it 'gets only shortcuts with show_in_menu property in true' do
        shortcut = build(:shortcut, show_in_menu: true)

        Shortcut.expects(:where).with(show_in_menu: true).returns([shortcut]).twice
        Shortcut.for_menu.count.must_equal 1
        Shortcut.for_menu.first.must_equal shortcut
      end
    end

    describe 'for' do
      after do
        Shortcut.unstub :where
      end

      it 'gets shortcuts for the user' do
        admin_shortcut = build(:admin_shortcut)

        Shortcut.expects(:where).with(user_id: admin_shortcut.user).returns([admin_shortcut]).twice
        Shortcut.for(admin_shortcut.user).count.must_equal 1
        Shortcut.for(admin_shortcut.user).first.must_equal admin_shortcut
      end
    end

    describe 'shown_in_menu!' do
      it 'should set the property in true' do
        show_me = build(:shortcut, show_in_menu: true)
        show_me.show_in_menu!

        show_me.must_be :show_in_menu
      end
    end

    describe 'remove_from_menu!' do
      it 'should set the property in false' do
        no_show_me = build(:shortcut, show_in_menu: true)
        no_show_me.remove_from_menu!

        no_show_me.wont_be :show_in_menu
      end
    end
  end
end