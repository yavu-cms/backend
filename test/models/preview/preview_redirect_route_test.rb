require 'test_helper'

class PreviewRedirectRouteTest < ActiveSupport::TestCase
  describe PreviewRedirectRoute do
    describe 'common logic' do
      it 'includes PreviewRoute' do
        PreviewRedirectRoute.included_modules.must_include PreviewRoute
      end
    end

    describe '#as_api_resource' do
      it 'returns RedirectRoute' do
        PreviewRedirectRoute.new.api_resource_class.must_equal Yavu::API::Resource::RedirectRoute
      end
    end
  end
end
