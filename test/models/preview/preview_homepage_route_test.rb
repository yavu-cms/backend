require 'test_helper'

class PreviewHomepageRouteTest < ActiveSupport::TestCase
  describe PreviewHomepageRoute do
    describe 'common logic' do
      it 'includes PreviewRoute' do
        PreviewHomepageRoute.included_modules.must_include PreviewRoute
      end
    end

    describe '#as_api_resource' do
      it 'returns HomepageRoute' do
        PreviewHomepageRoute.new.api_resource_class.must_equal Yavu::API::Resource::HomepageRoute
      end
    end
  end
end
