require 'test_helper'

class PreviewSectionRouteTest < ActiveSupport::TestCase
  describe PreviewSectionRoute do
    describe 'common logic' do
      it 'includes PreviewRoute' do
        PreviewSectionRoute.included_modules.must_include PreviewRoute
      end
    end

    describe '#as_api_resource' do
      it 'returns SectionRoute' do
        PreviewSectionRoute.new.api_resource_class.must_equal Yavu::API::Resource::SectionRoute
      end
    end
  end
end
