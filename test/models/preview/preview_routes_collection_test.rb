require 'test_helper'

class PreviewRoutesCollectionTest < ActiveSupport::TestCase
  describe PreviewRoutesCollection do
    describe 'Enumerable' do
      it 'is Enumerable' do
        PreviewRoutesCollection.included_modules.must_include Enumerable
        PreviewRoutesCollection.instance_methods.must_include :each
      end
    end

    describe 'initialization' do
      let(:preview_routes_collection) { PreviewRoutesCollection.new @routes }

      before do
        @routes = %i( article_route homepage_route section_route redirect_route ).map { |factory| create factory }
      end

      it 'builds a hash with the routes it receives, converting each of them to their #for_preview counterpart' do
        preview_routes_collection.routes.wont_be_empty
        preview_routes_collection.routes.each do |id, preview_route|
          route = Route.find(id)
          preview_route.must_equal route.for_preview
        end
      end
    end

    describe '#find' do
      let(:preview_routes_collection) { PreviewRoutesCollection.new @routes }

      before do
        @route = create :article_route
        @routes = [@route]
      end

      it 'retrieves a Preview*Route by its id' do
        preview_routes_collection.find(@route.id).must_be_instance_of PreviewArticleRoute
      end
    end

    describe '#each' do
      let(:preview_routes_collection) { PreviewRoutesCollection.new @routes }

      before do
        @routes = [create(:homepage_route), create(:homepage_route)]
      end

      it 'iterates over the values of the routes hash' do
        preview_routes_collection.each do |preview_route|
          preview_route.must_be_instance_of PreviewHomepageRoute
        end
      end
    end
  end
end