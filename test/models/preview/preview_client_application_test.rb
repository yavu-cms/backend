require 'test_helper'

class PreviewClientApplicationTest < ActiveSupport::TestCase
  describe PreviewClientApplication do
    describe '#as_api_resource' do
      let(:client_application) { create :client_application }

      before do
        @resource = client_application.as_api_resource
      end

      it 'must return expected attributes' do
        @resource.service_prefix.must_equal client_application.service_prefix
        @resource.assets_configuration.must_equal client_application.assets_configuration
      end
    end
  end
end