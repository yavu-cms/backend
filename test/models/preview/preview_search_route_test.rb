require 'test_helper'

class PreviewSearchRouteTest < ActiveSupport::TestCase
  describe PreviewSearchRoute do
    describe 'common logic' do
      it 'includes PreviewRoute' do
        PreviewSearchRoute.included_modules.must_include PreviewRoute
      end
    end

    describe '#as_api_resource' do
      it 'returns SearchRoute' do
        PreviewSearchRoute.new.api_resource_class.must_equal Yavu::API::Resource::SearchRoute
      end
    end
  end
end
