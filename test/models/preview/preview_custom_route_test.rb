require 'test_helper'

class PreviewCustomRouteTest < ActiveSupport::TestCase
  describe PreviewCustomRoute do
    describe 'common logic' do
      it 'includes PreviewRoute' do
        PreviewCustomRoute.included_modules.must_include PreviewRoute
      end
    end

    describe '#as_api_resource' do
      it 'returns CustomRoute' do
        PreviewCustomRoute.new.api_resource_class.must_equal Yavu::API::Resource::CustomRoute
      end
    end
  end
end
