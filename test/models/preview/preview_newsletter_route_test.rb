require 'test_helper'

class PreviewNewsletterRouteTest < ActiveSupport::TestCase
  describe PreviewNewsletterRoute do
    describe 'common logic' do
      it 'includes PreviewRoute' do
        PreviewNewsletterRoute.included_modules.must_include PreviewRoute
      end
    end

    describe '#as_api_resource' do
      it 'returns NewsletterRoute' do
        PreviewNewsletterRoute.new.api_resource_class.must_equal Yavu::API::Resource::NewsletterRoute
      end
    end
  end
end
