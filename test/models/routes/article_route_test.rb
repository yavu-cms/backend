require 'test_helper'

class ArticleRouteTest < ActiveSupport::TestCase
  describe ArticleRoute do
    describe 'validations' do
      describe 'representation' do
        it 'validates representation' do
          route = build :article_route
          route.representation = nil
          route.wont_be :valid?
          route.errors.must_include :representation
        end
      end
    end
    describe '.available_parameters' do
      it 'returns its parameters' do
        ArticleRoute.available_parameters.sort.must_equal ['article', 'section'].sort
      end
    end

    describe '#constrained_article=' do
      let(:route) { ArticleRoute.new pattern: '/', constraints: { c1: 'c1', c2: 'c2'} }
      let(:article) { mock }
      describe 'when article is not blank' do
        it 'sets the article as a constraint' do
          route.constrained_article = article
          route.constrained_article.must_equal article
        end
      end
      describe 'when article is blank' do
        it 'removes the article constraint' do
          route.constraints[:article] = article
          route.constrained_article   = nil
          route.constrained_article.must_be_nil
        end
      end
    end

    describe '#accounting' do
      it 'needs tests' do
        skip
      end
    end

    describe '#populate_evaluated_context' do
      let(:evaluated_context) { Yavu::API::Resource::PreprocessedContext.new }
      let(:article) { create :article }
      let(:section) { article.section }
      let(:route)   { build :article_route }

      before do
        route.client_application.news_source = article.news_source
      end

      describe 'without route constraints' do
        it 'must set every extra parameter' do
          route.populate_evaluated_context(evaluated_context, 'article' => article.slug, 'section' => section.slug, 'one' => 1, 'two' => 2)
          evaluated_context[:article].id.must_equal article.id
          evaluated_context[:one].must_equal 1
          evaluated_context[:two].must_equal 2
        end

        it 'must set article when :section and :article are setted' do
          route.populate_evaluated_context(evaluated_context, 'article' => article.slug, 'section' => section.slug)
          evaluated_context[:article].id.must_equal article.id
        end

        it 'must set article when :section is not set and :article is setted' do
          route.populate_evaluated_context(evaluated_context, 'article' => article.slug)
          evaluated_context[:article].id.must_equal article.id
        end

        it 'must raise ActiveRecord::RecordNotFound when article (or section) are not part of the route news source' do
          -> do
            route.populate_evaluated_context(evaluated_context, 'article' => article.slug, 'section' => 'not exists')
          end.must_raise ActiveRecord::RecordNotFound
        end

        it 'must raise ActiveRecord::RecordNotFound when section is not found' do
          sup = section.supplement
          sup.update(news_source: create(:news_source)) # Section is now in different news source
          -> do
            route.populate_evaluated_context(evaluated_context, 'article' => article.slug, 'section' => section.slug)
          end.must_raise ActiveRecord::RecordNotFound
        end

        it 'must raise ActiveRecord::RecordNotFound when article is not found' do
          -> do
            route.populate_evaluated_context(evaluated_context, 'article' => 'not exists')
          end.must_raise ActiveRecord::RecordNotFound
        end

        it 'must raise ActiveRecord::RecordNotFound when article is not found within section' do
          section = create :section
          -> do
            route.populate_evaluated_context(evaluated_context, 'article' => article.slug, 'section' => section.slug)
          end.must_raise ActiveRecord::RecordNotFound
        end

        it 'must raise ActiveRecord::RecordNotFound when edition is not visible' do
          section = create :section
          edition = create :edition, is_visible: false, supplement: section.supplement
          article = create :article, section: section, edition: edition
          -> do
            route.populate_evaluated_context(evaluated_context, 'article' => article.slug, 'section' => section.slug)
          end.must_raise ActiveRecord::RecordNotFound
        end

        it 'must raise ActiveRecord::RecordNotFound when section is not visible' do
          section = create :invisible_section
          article = create :article, section: section, supplement: section.supplement
          -> do
            route.populate_evaluated_context(evaluated_context, 'article' => article.slug, 'section' => section.slug)
          end.must_raise ActiveRecord::RecordNotFound
        end

        it 'must raise ActiveRecord::RecordNotFound when article is not visible' do
          section = create :section
          article = create :article, is_visible: false, section: section, supplement: section.supplement
          -> do
            route.populate_evaluated_context(evaluated_context, 'article' => article.slug, 'section' => section.slug)
          end.must_raise ActiveRecord::RecordNotFound
        end
      end
    end

    describe '_set_seo' do
      let(:route) { create :article_route }
      let(:article) { create :visible_article, title: 'Nice Article' }
      let(:evaluated_context) { Yavu::API::Resource::PreprocessedContext.new }
      let(:expected_keys) { %w(tag1 tag2 tag3 tag4 tag5).map { |n| Tag.new(name: n) } }

      before do
        #stubs static info
        eam = mock
        eam.stubs(:seo_title).returns('SEO title in article')
        eam.stubs(:seo_description).returns('SEO description in article')
        eam.stubs(:seo_keywords).returns('SEO keywords in article')
        section = build :section
        section.stubs(:extra_attribute_model).returns(eam)
        article.stubs(:section).returns(section)

        #stubs dynamic info
        article.tags << expected_keys
      end

      it 'must set seo information correctly' do
        route.send :_set_seo, evaluated_context, article
        evaluated_context[:seo].must_equal({
          title: article.title + ' ' + 'SEO title in article',
          description: article.section.name + ' ' + article.full_date.to_s + ' ' + article.lead + ' ' + 'SEO description in article',
          keywords: article.tags.map(&:name).join(', ') + ' ' + 'SEO keywords in article'
        })
      end
    end
  end
end
