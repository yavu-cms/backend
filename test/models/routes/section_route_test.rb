require 'test_helper'

class SectionRouteTest < ActiveSupport::TestCase
  describe SectionRoute do
    describe 'validations' do
      describe 'representation' do
        it 'validates representation' do
          route = build :section_route
          route.representation = nil
          route.wont_be :valid?
          route.errors.must_include :representation
        end
      end
    end
    describe '.available.parameters' do
      it 'returns its parameters' do
        SectionRoute.available_parameters.must_equal ['section']
      end
    end

    describe '#constrained_section=' do
      let(:route) { build :section_route, pattern: '/', constraints: { c1: 'c1', c2: 'c2'} }
      let(:section) { mock }
      describe 'when section is not blank' do
        it 'sets the section as a constraint' do
          route.constrained_section = section
          route.constrained_section.must_equal section
        end
      end
      describe 'when section is blank' do
        it 'removes the article constraint' do
          route.constraints[:section] = section
          route.constrained_section   = nil
          route.constrained_section.must_be_nil
        end
      end
    end

    describe '#populate_evaluated_context' do
      let(:route) { create :section_route, pattern: '/' }
      let(:evaluated_context) { Yavu::API::Resource::PreprocessedContext.new }

      before do
        route.stubs(:_set_seo).returns nil
      end

      describe 'without route constraints' do
        it 'must set every extra parameter' do
          section = create :section
          route.news_source.supplements << section.supplement
          route.populate_evaluated_context(evaluated_context, 'section' => section.slug, 'one' => 1, 'two' => 2)
          evaluated_context[:section].id.must_equal section.id
          evaluated_context[:one].must_equal 1
          evaluated_context[:two].must_equal 2
        end

        it 'must set :section' do
          section = create :section
          route.news_source.supplements << section.supplement
          route.populate_evaluated_context(evaluated_context, 'section' => section.slug)
          evaluated_context[:section].id.must_equal section.id
        end

        it 'must raise ActiveRecord::RecordNotFound when section is not visible' do
          section = create :invisible_section
          -> do
            route.populate_evaluated_context(evaluated_context, 'section' => section.slug)
          end.must_raise ActiveRecord::RecordNotFound
        end

        it 'must raise ActiveRecord::RecordNotFound when section is not found' do
          -> do
            route.populate_evaluated_context(evaluated_context, 'section' => 'not exists')
          end.must_raise ActiveRecord::RecordNotFound
        end
      end
    end

    describe '_set_seo' do
      let(:route) { create :section_route }
      let(:section) { create :visible_section, name: 'Nice Section' }
      let(:evaluated_context) { Yavu::API::Resource::PreprocessedContext.new }
      let(:expected_keys_and_desc) { %w(t1 t2 t3 t4 t5).map { |n| Tag.new(name: n) } }
      let(:expected_articles) { create_list :article, 2 }

      before do
        #stubs static info
        eam = mock
        eam.stubs(:seo_title).returns('SEO title in section')
        eam.stubs(:seo_description).returns('SEO description in section')
        eam.stubs(:seo_keywords).returns('SEO keywords in section')
        section.stubs(:extra_attribute_model).returns(eam)

        #stubs dynamic info
        representation = build :representation
        expected_articles.first.tags << expected_keys_and_desc
        client_app = build :client_application
        client_app.stubs(:home_articles).returns(expected_articles)
        route.stubs(:client_application).returns(client_app)
      end

      it 'must set seo information correctly' do
        route.send :_set_seo, evaluated_context, section
        evaluated_context[:seo].must_equal({
          title: expected_articles.map(&:title).join(', ') + ' ' + 'SEO title in section',
          description: section.name + ' ' + Date.today.to_s + ' ' + 'SEO description in section',
          keywords: expected_keys_and_desc.map(&:name).join(', ') + ' ' + 'SEO keywords in section'
        })
      end
    end
  end
end
