require 'test_helper'

class RedirectRouteTest < ActiveSupport::TestCase
  describe RedirectRoute do
    describe 'validations' do
      describe 'redirect_url' do
        it 'validates redirect_url' do
          route = build :redirect_route, redirect_url: nil
          route.wont_be :valid?
          route.errors.must_include :redirect_url
        end
      end
      describe 'representation' do
        it 'does NOT validate representation' do
          route = build :redirect_route, representation: nil
          route.must_be :valid?
        end
      end
    end

    describe '#requires_redirect_url?' do
      it 'returns true' do
        build(:redirect_route).must_be :requires_redirect_url?
      end
    end

    describe '#requires_representation?' do
      it 'returns false' do
        build(:redirect_route).wont_be :requires_representation?
      end
    end
  end
end