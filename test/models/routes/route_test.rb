require 'test_helper'
require 'mocha/setup'

class RouteTest < ActiveSupport::TestCase
  describe Route do
    describe 'validations' do
      before do
        @client_application_1 = create :client_application
        @client_application_2 = create :client_application
      end

      describe 'name' do
        it 'validates name presence' do
          route = build :route, name: nil
          route.wont_be :valid?
          route.errors.must_include :name
        end

        it 'validates name uniqueness by client application' do
          create :route, name: 'A route', client_application: @client_application_1, pattern: '/section'
          create :route, name: 'A route', client_application: @client_application_2, pattern: '/supplement'
          ->{ create :route, name: 'A route', client_application: @client_application_1, pattern: '/article' }.must_raise ActiveRecord::RecordInvalid
        end
      end

      describe 'pattern' do
        it 'validates pattern presence' do
          route = build :route, pattern: nil
          route.wont_be :valid?
          route.errors.must_include :pattern
        end

        it 'validates pattern uniqueness by client application' do
          create :route, name: 'A route 1', client_application: @client_application_1, pattern: '/section'
          create :route, name: 'A route 2', client_application: @client_application_2, pattern: '/section'
          ->{ create :route, name: 'A route 3', client_application: @client_application_1, pattern: '/section' }.must_raise ActiveRecord::RecordInvalid
        end

        it 'validates pattern parameters availability' do
          route = build :route, pattern: "/weird/:unused_param_1/:unused_param_2/:unused_param_3"
          route.wont_be :valid?
          route.errors.must_include :pattern
        end
      end
    end

    describe '.types' do
      it 'returns all route types classes in an array' do
        Route.types.must_equal [ArticleRoute, HomepageRoute, RedirectRoute, SectionRoute, SearchRoute, CustomRoute, NewsletterRoute]
      end
    end

    describe '#component_configuration_services' do
      it 'must be implemented'
    end


    describe '#calculated_cache_control' do
      it 'must return cache_control when set' do
        route = Route.new cache_control: 'a'
        route.calculated_cache_control.must_equal 'a'
      end

      it 'must return client_application cache control when cache_control is blank' do
        client_application = build(:client_application)
        client_application.expects(:calculated_route_cache_control)
        route = Route.new client_application: client_application
        route.calculated_cache_control
      end
    end

    describe '#to_s' do
      let(:route) { Route.new name: 'A route' }
      it 'returns the route name' do
        route.to_s.must_equal 'A route'
      end
    end

    describe '#slug' do
      let(:route) { Route.new name: "Main page route" }
      it 'returns the route name underscored' do
        route.slug.must_equal "main_page_route"
      end
    end

    describe '#available_parameters' do
      it 'returns an empty array by default' do
        build(:route).available_parameters.must_be_empty
      end
    end

    describe '#requires_redirect_url?' do
      it 'returns false by default' do
        build(:route).wont_be :requires_redirect_url?
      end
    end

    describe '#requires_representation?' do
      it 'returns true by default' do
        build(:route).must_be :requires_representation?
      end
    end

    describe '#full_url' do
      let(:route) {build :homepage_route}
      let(:fake_url) { 'http://lalala.com' }

      it 'returns full url' do
        route.full_url(fake_url).must_equal 'http://lalala.com/homepage'
      end
    end

    describe 'conditions and constraints' do
      let(:representation_mock) { mock.responds_like_instance_of Representation }
      let(:route) { Route.new pattern: '/' }
      let(:route_type_mock) { mock }
      let(:input) { {default: 'input', key: 'another value'} }
      let(:constraints) { { key: 'value' } }
      let(:conditions) { { key: 'condition' } }

      describe 'default values' do
        it 'has no constraints by default' do
          route = Route.new
          route.constrained?.must_equal false
        end
      end

      describe 'applying constraints' do
        it 'correctly answers true when a constraint is applied' do
          route.expects(:constraints).returns(constraints).once
          route.constrained?.must_equal true
        end

        describe 'Constrained items (getters and setters)' do
          let(:route) { Route.new pattern: '/', constraints: { c1: 'c1', c2: 'c2'} }

          describe '#constrained=' do
            it 'deletes all constrained keys when a value is received' do
              route.constraints.must_equal c1: 'c1', c2: 'c2'
              route.constrained = 'delete_all_for_example'
              route.constraints.must_equal Hash.new
            end
          end

          describe '#set_constrained_item=' do
            let(:item) { mock }
            describe 'when item constrain is not blank' do
              it 'sets the item object in that constrain' do
                route.send :set_constrained_item, :c1, item
                route.constraints.must_equal c1: item, c2: 'c2'
              end
            end

            describe 'when item constrain is blank' do
              it 'removes the item object under this constraint' do
                route.send :set_constrained_item, :c1, ''
                route.constraints.must_equal c2: 'c2'
              end
            end
          end
        end
      end
    end

    describe '#populate_evaluated_context' do
      let(:route) { Route.new }
      let(:evaluated_context) { Yavu::API::Resource::PreprocessedContext.new }
      it 'must add literally every received parameter to evaluated context' do
        route.populate_evaluated_context(evaluated_context, one: 1, two: 2)
        evaluated_context[:one].must_equal 1
        evaluated_context[:two].must_equal 2
      end
    end

    describe '#build_context' do
      let(:route) { Route.new }
      it 'must return a API::Resource::PreprocessedContext instance' do
        route.build_context.must_be_instance_of Yavu::API::Resource::ProcessedContext
      end
    end

    describe '_set_seo' do
      let(:route) { build :route }
      let(:evaluated_context) { Yavu::API::Resource::PreprocessedContext.new }

      before do
        eam = mock
        eam.stubs(:seo_title).returns('SEO title of client app')
        eam.stubs(:seo_description).returns('SEO description of client app')
        eam.stubs(:seo_keywords).returns('SEO keywords of client app')
        route.client_application.stubs(:extra_attribute_model).returns(eam)
      end

      it 'its preprocesses context must contains a seo key with a correct hash structure' do
        route.populate_evaluated_context(evaluated_context, {})
        evaluated_context[:seo].must_be_instance_of Hash
        evaluated_context[:seo].keys.must_equal %i(title description keywords)
      end
    end

    describe 'touch relations' do
      it 'must touch client_application'
      it 'must touch representation'
    end
  end
end
