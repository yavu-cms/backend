require 'test_helper'

class HomepageRouteTest < ActiveSupport::TestCase
  describe HomepageRoute do
    describe 'validations' do
      describe 'representation' do
        it 'validates representation' do
          route = build :homepage_route
          route.representation = nil
          route.wont_be :valid?
          route.errors.must_include :representation
        end
      end
    end

    describe '_set_seo' do
      let(:route) { create :homepage_route }
      let(:evaluated_context) { Yavu::API::Resource::PreprocessedContext.new }
      let(:expected_keys_and_desc) { %w(tag1 tag2 tag3 tag4 tag5).map { |n| Tag.new(name: n) } }
      let(:expected_articles) { create_list :article, 3 }

      before do
        #stubs static info
        eam = mock
        eam.stubs(:seo_title).returns('SEO title in home')
        eam.stubs(:seo_description).returns('SEO description in home')
        eam.stubs(:seo_keywords).returns('SEO keywords in home')
        route.client_application.stubs(:extra_attribute_model).returns(eam)

        #stubs dynamic info
        representation = mock
        expected_articles.first.tags << expected_keys_and_desc
        representation.stubs(:cover_articles).returns(expected_articles)
        route.stubs(:representation).returns(representation)
      end

      it 'must set seo information correctly' do
        route.populate_evaluated_context(evaluated_context, {})
        evaluated_context[:seo].must_equal({
          title: expected_articles.map(&:title).join(', ') + ' ' + 'SEO title in home',
          description: expected_keys_and_desc.map(&:name).join(', ') + ' ' + 'SEO description in home',
          keywords: expected_keys_and_desc.map(&:name).join(', ') + ' ' + 'SEO keywords in home'
        })
      end
    end
  end
end
