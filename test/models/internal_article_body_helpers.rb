require 'test_helper'

class InternalArticleBodyHelpersTest < ActiveSupport::TestCase
  describe InternalArticleBodyHelpers do
    describe '.fronterize_internal_tags' do
      describe 'with articles medias' do
        describe 'when article_medium is not found' do
          let(:body) { "This is an example body that has the next tag: {{article_media:1}}" }
          let(:article) { create(:article, body: body) }

          it 'must return a String' do
            InternalArticleBodyHelpers.fronterize_internal_tags(article).must_be_instance_of String
          end

          it 'must replace that instance with nothing because it does not exist' do
            InternalArticleBodyHelpers.fronterize_internal_tags(article).must_equal "This is an example body that has the next tag: "
          end
        end

        describe 'when article_medium is found' do
          describe 'and is an image' do
            let(:article_media) { create(:article_medium) }
            let(:body) { "This is an example body that has the next tag: {{article_media:#{article_media.id}}}" }
            let(:article) { create(:article, body: body, article_media: [article_media]) }
            let(:expected_base_result) { /This is an example body that has the next tag: <%= medium_tag medias.find { |r| r.id == #{article_media.id} } %>/ }

            it 'must return a String' do
              InternalArticleBodyHelpers.fronterize_internal_tags(article).must_be_instance_of String
            end

            it 'must replace that instance with a properly image tag' do
              InternalArticleBodyHelpers.fronterize_internal_tags(article)
                .must_match expected_base_result
            end

            describe 'with options' do
              describe 'null options' do
                let(:body) { "This is an example body that has the next tag: {{article_media:#{article_media.id}|}}" }

                it 'must replace that instance with a properly image tag (no options)' do
                  InternalArticleBodyHelpers.fronterize_internal_tags(article).must_match expected_base_result
                end
              end
              describe 'some blankies' do
                let(:body) { "This is an example body that has the next tag: {{article_media:#{article_media.id}|            }}" }

                it 'must replace that instance with a properly image tag (no options)' do
                  InternalArticleBodyHelpers.fronterize_internal_tags(article).must_match expected_base_result
                end
              end
              describe 'some junk' do
                let(:body) { "This is an example body that has the next tag: {{article_media:#{article_media.id}|sfsaf sfasf sadfasfas}}" }

                it 'must replace that instance with a properly image tag (no options)' do
                  InternalArticleBodyHelpers.fronterize_internal_tags(article).must_match expected_base_result
                end
              end
              describe 'wrong options' do
                let(:body) { "This is an example body that has the next tag: {{article_media:#{article_media.id}|caption:lalala}}" }

                it 'must replace that instance with a properly image tag (no options)' do
                  InternalArticleBodyHelpers.fronterize_internal_tags(article).must_match expected_base_result
                end
              end
              describe 'size' do
                let(:body) { "This is an example body that has the next tag: {{article_media:#{article_media.id}|size:xbig}}" }
                let(:expected_result_with_custom_size) do
                  /This is an example body that has the next tag: <%= medium_tag medias.find { |r| r.id == #{article_media.id} }, { size: 'xbig' } %>/
                end

                it 'must replace that instance with a properly image tag (with custom size)' do
                  InternalArticleBodyHelpers.fronterize_internal_tags(article).must_match expected_result_with_custom_size
                end
              end
            end
          end
          describe 'and is a file' do
            let(:article_media) { create(:article_medium, medium: create(:file_medium)) }
            let(:body) { "This is an example body that has the next tag: {{article_media:#{article_media.id}}}" }
            let(:article) { create(:article, body: body, article_media: [article_media]) }

            it 'must return a String' do
              InternalArticleBodyHelpers.fronterize_internal_tags(article).must_be_instance_of String
            end

            it 'must replace that instance with a properly file tag (which shall be a link)' do
              InternalArticleBodyHelpers.fronterize_internal_tags(article)
                .must_match(/This is an example body that has the next tag: <%= medium_tag medias.find { |r| r.id == #{article_media.id} } %>/)
            end
          end
        end
      end
      describe 'with related articles' do
        describe 'when article is not found' do
          let(:body) { "This is an example body that has the next tag: {{article:777}}" }
          let(:article) { create(:article, body: body) }

          it 'must return a String' do
            InternalArticleBodyHelpers.fronterize_internal_tags(article).must_be_instance_of String
          end

          it 'must replace that instance with nothing because it does not exist at all' do
            InternalArticleBodyHelpers.fronterize_internal_tags(article).must_equal "This is an example body that has the next tag: "
          end
        end
        describe 'when article is found, but there is not in the article relation' do
          let(:unrelated_article) { create(:article) }
          let(:body) { "This is an example body that has the next tag: {{article:#{unrelated_article.id}}}" }
          let(:article) { create(:article, body: body) }

          it 'must return a String' do
            InternalArticleBodyHelpers.fronterize_internal_tags(article).must_be_instance_of String
          end

          it 'must replace that instance with nothing because it does not exist in the article related articles' do
            InternalArticleBodyHelpers.fronterize_internal_tags(article).must_equal "This is an example body that has the next tag: "
          end
        end
        describe 'when article is found and there is in the article relation' do
          let(:body) { "This is an example body that has the next tag: {{article:#{related_article.id}}}" }
          let(:article) { create(:article, body: body, articles: [related_article]) }

          describe 'and the related article is invisible' do
            let(:related_article) { create(:article, is_visible: false) }

            it 'must return a String' do
              InternalArticleBodyHelpers.fronterize_internal_tags(article).must_be_instance_of String
            end

            it 'must replace that instance with nothing because the related article is inaccessible' do
              InternalArticleBodyHelpers.fronterize_internal_tags(article).must_equal "This is an example body that has the next tag: "
            end
          end

          describe 'and the related article is visible but not the section' do
            let(:supplement) { create :supplement }
            let(:edition) { create :edition, supplement: supplement }
            let(:invisible_section) { create :invisible_section, supplement: supplement }
            let(:related_article) { create(:article, is_visible: true, section: invisible_section, edition: edition) }

            it 'must return a String' do
              InternalArticleBodyHelpers.fronterize_internal_tags(article).must_be_instance_of String
            end

            it 'must replace that instance with nothing because the related section is inaccessible' do
              InternalArticleBodyHelpers.fronterize_internal_tags(article).must_equal "This is an example body that has the next tag: "
            end
          end

          describe 'and the related article is visible' do
            let(:related_article) { create(:article, title: 'Hello!') }
            let(:expected_base_result) do
              "This is an example body that has the next tag: <%= link_to \"Hello!\", related.find { |r| r.article_id == #{related_article.id} } %>"
            end

            it 'must return a String' do
              InternalArticleBodyHelpers.fronterize_internal_tags(article).must_be_instance_of String
            end

            it 'must replace that instance with a properly yavu frontend link tag' do
              InternalArticleBodyHelpers.fronterize_internal_tags(article).must_equal expected_base_result
            end

            describe 'considering options' do
              describe 'null options' do
                let(:body) { "This is an example body that has the next tag: {{article:#{related_article.id}|}}" }

                it 'must replace that instance with a properly yavu frontend link tag (no options)' do
                  InternalArticleBodyHelpers.fronterize_internal_tags(article).must_equal expected_base_result
                end
              end
              describe 'some blankies' do
                let(:body) { "This is an example body that has the next tag: {{article:#{related_article.id}|              }}" }

                it 'must replace that instance with a properly yavu frontend link tag (no options)' do
                  InternalArticleBodyHelpers.fronterize_internal_tags(article).must_equal expected_base_result
                end
              end
              describe 'some junk' do
                let(:body) { "This is an example body that has the next tag: {{article:#{related_article.id}|asfsadf sfasfasdfasf}}" }

                it 'must replace that instance with a properly yavu frontend link tag (no options)' do
                  InternalArticleBodyHelpers.fronterize_internal_tags(article).must_equal expected_base_result
                end
              end
              describe 'wrong options' do
                let(:body) { "This is an example body that has the next tag: {{article:#{related_article.id}|wrong_caption:lalalala}}" }

                it 'must replace that instance with a properly yavu frontend link tag (no options)' do
                  InternalArticleBodyHelpers.fronterize_internal_tags(article).must_equal expected_base_result
                end
              end
              describe 'caption' do
                let(:body) { "This is an example body that has the next tag: {{article:#{related_article.id}|caption:Goodbye world!!!}}" }
                let(:expected_result_with_custom_caption) do
                  "This is an example body that has the next tag: <%= link_to \"Goodbye world!!!\", related.find { |r| r.article_id == #{related_article.id} } %>"
                end

                it 'must replace that instance with a properly yavu frontend link tag (with custom caption)' do
                  InternalArticleBodyHelpers.fronterize_internal_tags(article).must_equal expected_result_with_custom_caption
                end
              end
            end
          end
        end
      end
    end
  end
end
