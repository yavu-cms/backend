require 'test_helper'

class SupplementTest < ActiveSupport::TestCase
  describe Supplement do
    describe '#to_s' do
      it 'returns name' do
        supplement = build(:supplement, name: nil)
        supplement.to_s.must_be :blank?
        supplement = build(:supplement, name: 'Supplement')
        supplement.to_s.must_equal supplement.name
      end
    end

    describe '#become_default!' do
      let(:supplement) { build(:non_default_supplement) }
      let(:news_source) { supplement.news_source }

      describe 'when supplement has active edition' do
        let(:edition) { build(:edition, supplement: supplement, is_active: true) }
        before do
          supplement.active_edition = edition
        end

        it 'sets the supplement as default within a  news source' do
          news_source.default_supplement.wont_equal supplement
          supplement.become_default!
          news_source.default_supplement.must_equal supplement
        end

        it 'ensures there is only 1 default supplement in the same news source at a time' do
          default = create(:default_supplement, news_source: news_source)
          supplement.become_default!
          news_source.supplements.where(is_default: true).count.must_equal 1
          default.reload
          default.wont_be :is_default
        end
      end

    end

    describe '#replace_active_edition_with!' do
      let(:another_edition) { create :edition, name: 'Another edition' }
      let(:supplement) { create :supplement }
      let(:edition) { create :edition, supplement: supplement }

      describe 'when there is an active edition already' do
        before do
          supplement.active_edition = another_edition
          supplement.save
        end

        it 'makes it inactive before making the new one active' do
          supplement.active_edition.must_equal another_edition
          supplement.replace_active_edition_with! edition
          supplement.active_edition.must_equal edition
        end
      end

      describe 'when there is no active edition' do
        it 'makes the new one active' do
          supplement.active_edition.must_be_nil
          supplement.replace_active_edition_with! edition
          supplement.active_edition.must_equal edition
        end
      end
    end

    describe '#clear_active_edition!' do
      describe 'when there is no active edition' do
        let(:supplement) { create :supplement }

        it 'succeeds' do
          supplement.clear_active_edition!
          supplement.active_edition.must_be_nil
        end
      end

      describe 'when there is active edition' do
        let(:supplement) { create :supplement }
        let(:edition) { create :edition }

        before do
          supplement.active_edition = edition
          supplement.save
        end

        it 'succeeds' do
          supplement.clear_active_edition!
          supplement.active_edition.must_be_nil
        end
      end
    end

    describe '#active_edition' do
      let(:edition) { create :edition }
      let(:supplement) { build :supplement }

      it 'returns the active edition of the supplement' do
        supplement.active_edition_id = edition.id
        supplement.save
        supplement.active_edition.must_equal edition
      end

      describe "when there's no active edition" do
        it 'returns nil' do
          supplement.active_edition.must_be_nil
        end
      end
    end

    describe 'Validations' do
      describe 'active_edition' do
        let(:edition) { create :edition }
        let(:supplement) { create :supplement, name: 'My supplement' }
        let(:another_supplement) { create :supplement, name: 'Another supplement' }

        describe 'when it belongs to the same supplement' do
          before do
            edition.supplement = supplement
            supplement.active_edition = edition
          end

          it 'succeeds' do
            supplement.must_be :valid?
          end
        end

        describe "when it doesn't belong to the same supplement" do
          before do
            edition.supplement = another_supplement
            supplement.active_edition = edition
          end

          it 'fails' do
            supplement.wont_be :valid?
            supplement.errors[:active_edition].must_include I18n.t('activerecord.errors.models.supplement.active_edition')
          end
        end
      end
    end

    describe 'supplement deletion' do
      describe 'when the supplement has related elements' do
        describe 'when it has at least one edition' do
          before do
            @supplement = create :supplement
            @edition = create :edition, supplement: @supplement
          end

          it 'cannot be destroyed' do
            @supplement.destroy.must_equal false
            @supplement.wont_be :destroyed?
            @supplement.errors[:base].must_include I18n.t('activerecord.errors.models.supplement.attributes.base.restrict_dependent_destroy.many')
          end
        end

        describe 'when it has at least one section' do
          before do
            @supplement = create :supplement
            @section = create :section, supplement: @supplement
          end

          it 'cannot be destroyed' do
            @supplement.destroy.must_equal false
            @supplement.wont_be :destroyed?
            @supplement.errors[:base].must_include I18n.t('activerecord.errors.models.supplement.attributes.base.restrict_dependent_destroy.many')
          end
        end

        describe 'when it has both editions and sections' do
          before do
            @supplement = create :supplement
            @section = create :section, supplement: @supplement
            @edition = create :edition, supplement: @supplement
          end

          it 'cannot be destroyed' do
            @supplement.destroy.must_equal false
            @supplement.wont_be :destroyed?
            @supplement.errors[:base].must_include I18n.t('activerecord.errors.models.supplement.attributes.base.restrict_dependent_destroy.many')
          end
        end
      end

      describe "when it doesn't have any related elements" do
        let(:supplement) { create :supplement, editions: [], sections: [] }

        it 'can be destroyed' do
          supplement.destroy.wont_equal false
          supplement.must_be :destroyed?
        end
      end
    end
  end
end