require 'test_helper'

class ArticleMediumTest < ActiveSupport::TestCase
  describe ArticleMedium do
    describe 'methods delegation' do
      let(:medium) { build :image_medium }
      let(:article_medium) { build :article_medium, medium: medium }
      let(:expected) { mock }

      describe '#type' do
        before do
          medium.expects(:type).returns(expected).once
        end

        after do
          medium.unstub :type
        end

        it 'delegates the #type method to its related Medium' do
          article_medium.type.must_equal expected
        end
      end

      describe '#file' do
        before do
          medium.expects(:file).returns(expected).once
        end

        after do
          medium.unstub :file
        end

        it 'delegates the #file method to its related Medium' do
          article_medium.file.must_equal expected
        end
      end

      describe '#tags' do
        before do
          medium.expects(:tags).returns(expected).once
        end

        after do
          medium.unstub :tags
        end

        it 'delegates the #tags method to its related Medium' do
          article_medium.tags.must_equal expected
        end
      end
    end

    describe '#medium' do
      describe 'when medium has a name' do
        let(:medium)         { create :image_medium,   name: 'A example medium' }
        let(:article_medium) { create :article_medium, medium: medium }

        it 'mantains it' do
          article_medium.medium.name.must_equal 'A example medium'
        end
      end
      describe 'when medium has not a name' do
        let(:medium)         { create :image_medium,   name: '' }
        let(:article_medium) { create :article_medium, caption: 'A nice caption', medium: medium }

        it 'will set with caption value' do
          article_medium.medium.name.must_equal 'A nice caption'
        end
      end
    end

    describe '#calculated_caption' do
      before do
        @article_medium_caption = create(:article_medium)
        @article_medium_no_caption = create(:article_medium, caption: '')
      end

      it 'returns article title if caption is nil' do
        @article_medium_no_caption.calculated_caption.must_equal @article_medium_no_caption.article.title
      end

      it 'returns caption if it is present' do
        @article_medium_caption.calculated_caption.must_equal @article_medium_caption.caption
      end
    end

    describe 'as_api_resource' do
      before do
        @article_medium = create(:article_medium)
      end

      it 'must return expected attributes' do
        @resource = @article_medium.as_api_resource
        @resource.id.must_equal @article_medium.id
        @resource.caption.must_equal @article_medium.caption
        @resource.order.must_equal @article_medium.order
        @resource.medium.must_equal @article_medium.medium.as_api_resource
      end
    end

    describe 'Scopes' do
      before do
        @article = create(:article)

        @image_1 = create(:image_medium)
        @image_2 = create(:image_medium, file: upload_test_file('image_2.jpg'))
        @art_img1 = create(:article_medium, article: @article, medium: @image_1)
        @art_img2 = create(:article_medium, article: @article, medium: @image_2)

        @embedded_1 = create(:embedded_medium)
        @embedded_2 = create(:embedded_medium, content: 'other content')
        @art_emb1 = create(:article_medium, article: @article, medium: @embedded_1)
        @art_emb2 = create(:article_medium, article: @article, medium: @embedded_2)

        @audio_1 = create(:audio_medium)
        @audio_2 = create(:audio_medium, file: upload_test_file('mario_bros.mp3'))
        @art_aud1 = create(:article_medium, article: @article, medium: @audio_1)
        @art_aud2 = create(:article_medium, article: @article, medium: @audio_2)

        @file_1 = create(:file_medium)
        @file_2 = create(:file_medium, file: upload_test_file('javascript.js'))
        @art_file1 = create(:article_medium, article: @article, medium: @file_1)
        @art_file2 = create(:article_medium, article: @article, medium: @file_2)
      end

      it 'must return image_media' do
        ArticleMedium.image.must_equal [@art_img1, @art_img2]
      end

      it 'must return embedded_media' do
        ArticleMedium.embedded.must_equal [@art_emb1, @art_emb2]
      end

      it 'must return audio_media' do
        ArticleMedium.audio.must_equal [@art_aud1, @art_aud2]
      end

      it 'must return file_media' do
        ArticleMedium.file.must_equal [@art_file1, @art_file2]
      end
    end

    describe 'relations' do
      let(:article_medium) { create :article_medium }
      it 'must touch article when updated' do
        article_medium.article.expects(:touch).once
        article_medium.save
      end
    end
  end
end
