require 'test_helper'

class NewsletterTest < ActiveSupport::TestCase

  describe "Validations" do
    describe 'name field' do
      it 'fails when empty' do
        newsletter = build :newsletter, name: ''
        newsletter.must_be :invalid?
        newsletter.errors[:name].must_include I18n.t('errors.messages.blank')
      end

      it 'fails when is not unique' do
        create :newsletter, name: 'n1'
        newsletter = build :newsletter, name: 'n1'
        newsletter.must_be :invalid?
        newsletter.errors[:name].must_include I18n.t('errors.messages.taken')
      end

      it 'succeds when not empty and it is unique' do
        create :newsletter
        newsletter = Newsletter.new(name: 'Other name')
        newsletter.valid?
        newsletter.errors[:name].must_be_empty
      end
    end

    describe 'frequency field' do
      it 'fails when empty' do
        newsletter = build :newsletter, frequency: nil
        newsletter.must_be :invalid?
        newsletter.errors[:frequency].must_include I18n.t('errors.messages.blank')
      end

      it 'succeds when not empty' do
        create :newsletter
        newsletter = Newsletter.new(frequency: '*/1 * * * *')
        newsletter.valid?
        newsletter.errors[:frequency].must_be_empty
      end
    end

    describe 'expiration field' do
      it 'fails when empty' do
        newsletter = build :newsletter, expiration: nil
        newsletter.must_be :invalid?
        newsletter.errors[:expiration].must_include I18n.t('errors.messages.blank')
      end

      it 'succeds when not empty' do
        create :newsletter
        newsletter = Newsletter.new(expiration: 1)
        newsletter.valid?
        newsletter.errors[:expiration].must_be_empty
      end
    end

    describe 'client_application_id field' do
      it 'fails when empty' do
        newsletter = build :newsletter, client_application: nil
        newsletter.must_be :invalid?
        newsletter.errors[:client_application].must_include I18n.t('errors.messages.blank')
      end

      it 'succeds when not empty' do
        newsletter = Newsletter.new(client_application_id: create(:client_application).id)
        newsletter.valid?
        newsletter.errors[:client_application_id].must_be_empty
      end
    end
  end

  describe '#do_generation!' do
    describe 'when there is a problem with the context' do
      let(:newsletter) { create :newsletter, context: '@f = 1Error'}
      it 'must raise an exception with a context error' do
        exception = ->{ newsletter.send :do_generation! }.must_raise(StandardError)
        exception.message.must_include 'Hay problemas con el contexto'
      end
    end
    describe 'when there is a problem with the view' do
      let(:newsletter) { create :newsletter, context: '@a = 100', generator: 'a.each { |e| "I am not Enumerable"}' }
      it 'must raise an exception with a view error' do
        # TODO: Tiene problemas en el CI. Revisar
        # exception = ->{ newsletter.send :do_generation! }.must_raise(SyntaxError)
        # exception.message.must_include 'Hay problemas con la vista'
        skip
      end
    end
    describe 'when there is no previous problems' do
      let(:generator) { "<div><ul><% arr.each do |w| %><li><%= w %></li><% end %></ul></div>" }
      let(:newsletter) { create :newsletter, context: '@arr = %w( uno dos tres )', generator: generator}
      it 'must return a view string which is the result of rendering process' do
        # TODO: No funciona bien porque por alguna extraña razón fake_route.as_api_resource no agrega la vista en partials
        # (pone la clave de la vista y el contenido con nil)
        # expected_view =  "<div><ul><li>uno</li><li>dos</li><li>tres</li></ul></div>"
        # newsletter.send(:do_generation!).must_equal expected_view
        skip
      end
    end
  end

end
