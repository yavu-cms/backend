require 'test_helper'

class BaseModelFilterTest < ActiveSupport::TestCase
  describe BaseModelFilter do
    before do
      @filter = BaseModelFilter.new
      # Overriding #model method in order to make the class testable
      class << @filter
        def model
          Article
        end
      end
    end

    after do
      @filter.clear!
    end

    describe '#model' do
      it 'should fail because there is no default model attached to the filter' do
        @filter = BaseModelFilter.new

        proc { @filter.model }.must_raise NotImplementedError
      end
    end

    describe '#all' do
      it 'should return all records by default' do
        @filter.all.must_equal Article.all
      end

      it 'should apply the filters when it values for them provided' do
        create(:article)
        @filter[:slug] = 'title'

        @filter.all.count.must_equal 1
      end
    end

    describe '#apply_to' do
      it 'should not modify the matches ARRelation if no non-nil values are set' do
        @filter[:slug] = nil
        @filter[:title] = nil

        arr = Article.all

        @filter.apply_to(arr).must_be_same_as arr
      end

      it 'should use custom filter methods when they are available' do
        @filter[:heading] = 'heading'

        class << @filter
          def filter_by_heading(partial, value)
            partial.where(heading: "not #{value}")
          end
        end

        arr = Article.all

        @filter.apply_to(arr).count.must_equal 0
      end

      it 'should filter string values using a LIKE query by default' do
        # This scenario should only match one title: 'title one'
        create(:article, title: 'title one')
        create(:article, title: 'title two')
        @filter[:title] = 'one'
        arr = Article.all

        @filter.apply_to(arr).count.must_equal 1

        @filter.clear!

        # This scenario should match both of the titles: 'title one' and 'title two'
        @filter[:title] = 'title'
        @filter.apply_to(arr).count.must_equal 2
      end

      it 'should use a default filtering strategy when not using String values' do
        a = create(:article)
        create(:article_without_edition)
        arr = Article.all

        @filter[:edition] = a.edition
        @filter.apply_to(arr).count.must_equal 1

        @filter.clear!
      end
    end
  end
end
