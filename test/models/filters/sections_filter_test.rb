require 'test_helper'

class SectionsFilterTest < ActiveSupport::TestCase
  describe SectionsFilter do
    let(:filter) { SectionsFilter.new }

    describe '#model' do
      it 'should return a Section class' do
        filter.model.must_be_same_as Section
      end
    end

    describe '#defaults' do
      it 'should include :name field with nil value' do
        (SectionsFilter.defaults.key? :name).must_equal true
        SectionsFilter.defaults[:name].must_be_nil
      end

      it 'should include :supplement_id field with nil value' do
        (SectionsFilter.defaults.key? :supplement_id).must_equal true
        SectionsFilter.defaults[:supplement_id].must_be_nil
      end
    end

    describe 'filter by various fields' do
      before do
        @supplement = create(:supplement)
        @section = create(:section, name: 'section', supplement_id: @supplement.id )
        @section_filter = SectionsFilter.new(values: {name: 'section', supplement_id: @section.supplement_id})
      end

      it 'must return a valid array' do
        @section_filter.all.must_equal [@section]
        @section_filter.all.count.must_equal 1
      end

      it 'must return an empty array if some value does not match' do
        @section_filter[:name] = 'Seeccction'

        @section_filter.all.must_equal []
        @section_filter.all.count.must_equal 0
      end
    end
  end
end