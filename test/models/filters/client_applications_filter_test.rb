require 'test_helper'
require 'mocha/setup'

class ClientApplicationsTest < ActiveSupport::TestCase
  describe ClientApplicationsFilter do
    let (:filter) { ClientApplicationsFilter.new }

    describe '#defaults' do
      it 'should include a set of default values' do
        defaults = ClientApplicationsFilter.defaults

        defaults.has_key?(:name).must_equal true
        defaults.has_key?(:url).must_equal true
        defaults.has_key?(:enabled).must_equal true
        defaults.has_key?(:query).must_equal true
      end
    end

    describe '#model' do
      it 'should return the ClientApplication class' do
        filter.model.must_be_same_as ClientApplication
      end
    end

    describe '#sanitize' do
      it "correctly excludes :containing from the other filters" do
        values = { name: 'Some value' }
        sanitized = filter.sanitize values

        sanitized[:url].must_be_nil
        sanitized[:query].must_be_nil
        sanitized[:enabled].must_be_nil
        sanitized[:name].wont_be_nil
      end
    end

    describe '#filter_by_enabled' do
      let(:partial) { mock }
      let(:clientapp1) { create(:client_application, enabled: true) }
      let(:clientapp2) { create(:client_application, enabled:false) }

      it 'must return disabled client applications if enabled is false' do
        partial.expects(:where).with(enabled: false).returns(clientapp2)
        filter.filter_by_enabled(partial, 'disabled').must_equal clientapp2
      end

      it 'must return enabled client applications if enabled is true' do
        partial.expects(:where).with(enabled: true).returns(clientapp1)
        filter.filter_by_enabled(partial, 'enabled').must_equal clientapp1
      end

      it 'must return all client applications otherwise' do
        filter.filter_by_enabled(partial, 'whatever').must_equal partial
      end
    end

    describe 'Not mocked tests' do
      describe '#filter_by_enabled' do
        before do
          @clientapp1 = create(:client_application, enabled: true)
          @clientapp2 = create(:client_application, enabled: false)
        end

        it 'returns a valid array' do
          filter.filter_by_enabled(ClientApplication.all, 'enabled').must_equal [@clientapp1]
          filter.filter_by_enabled(ClientApplication.all, 'disabled').must_equal [@clientapp2]
          filter.filter_by_enabled(ClientApplication.all, 'sarasa').must_equal [@clientapp1, @clientapp2]
        end
      end

      describe 'filter by various fields' do
        before do
          @clientapp1 = create(:client_application, name: 'app one', url: 'www.someurl.com', enabled: true)
          @clientapp2 = create(:client_application, name: 'app two', url: 'www.blahblah.com', enabled: false)
          @client_app_filter = ClientApplicationsFilter.new(values: {name: 'app one', url: 'www.someurl.com', enabled: true})
        end

        it 'must return a valid array' do
          @client_app_filter.all.must_equal [@clientapp1]
          @client_app_filter.all.count.must_equal 1
        end

        it 'must return an empty array if some value does not match' do
          @client_app_filter.values[:url] = 'www.someurl555.com'
          @client_app_filter.all.must_equal []
          @client_app_filter.all.count.must_equal 0
        end
      end

      describe '#filter by query' do
        before do
          @clientapp1 = create(:client_application, name: 'app one', url: 'www.someurl.com', enabled: true)
          @clientapp2 = create(:client_application, name: 'app two', url: 'www.blahblah.com', enabled: false)
        end

        it 'returns a valid array' do
          filter.filter_by_query(ClientApplication.all, 'someurl').must_equal [@clientapp1]
          filter.filter_by_query(ClientApplication.all, 'app two').must_equal [@clientapp2]
          filter.filter_by_query(ClientApplication.all, 'www.').must_equal [@clientapp1, @clientapp2]
        end
      end
    end
  end
end