require 'test_helper'

class SupplementsFilterTest < ActiveSupport::TestCase
  describe SupplementsFilter do
    let(:filter) { SupplementsFilter.new }

    describe '#model' do
      it 'should return a Supplement class' do
        filter.model.must_be_same_as Supplement
      end
    end

    describe '#defaults' do
      it 'should include only :name field' do
        expected = { name: nil }
        SupplementsFilter.defaults.must_equal expected
      end
    end

    describe 'filter by name' do
      before do
        @supplement = create(:supplement, name: 'funny name')
        @supplement_filter = SupplementsFilter.new(values: {name: 'funny name'})
      end

      it 'must return a valid array' do
        @supplement_filter.all.must_equal [@supplement]
        @supplement_filter.all.count.must_equal 1
      end

      it 'must return an empty array if name does not match' do
        @supplement_filter.values[:name] = 'not so funny name'
        @supplement_filter.all.must_equal []
        @supplement_filter.all.count.must_equal 0
      end
    end
  end
end