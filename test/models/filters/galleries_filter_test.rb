require 'test_helper'

class GalleriesFilterTest < ActiveSupport::TestCase
  describe GalleriesFilter do
    let (:filter) { GalleriesFilter.new }

    describe '#model' do
      it "should return a Gallery class" do
        filter.model.must_be_same_as Gallery
      end
    end

    describe '#defaults' do
      it "should include only :title and :type field" do
        expected = { title: nil, type: nil }
        expected.must_equal GalleriesFilter.defaults
      end
    end

    describe 'filter by name' do
      before do
        @gallery = MediaGallery.create(title: 'funny title')
        @gallery_filter = GalleriesFilter.new(values: {title: 'funny title'})
      end

      it 'must return a valid array' do
        @gallery_filter.all.must_equal [@gallery]
        @gallery_filter.all.count.must_equal 1
      end

      it 'must return an empty array if name does not match' do
        @gallery_filter.values[:title] = 'not so funny gallery'
        @gallery_filter.all.must_equal []
        @gallery_filter.all.count.must_equal 0
      end
    end

    describe 'filter by type and title' do
      before do
        @gallery = MediaGallery.create(title: 'namenameNamE')
        @gallery_filter = GalleriesFilter.new(values: {title: 'ameNa', type: 'MediaGallery'})
      end

      it 'must return a valid array' do
        @gallery_filter.all.must_equal [@gallery]
      end
    end
  end
end