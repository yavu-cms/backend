require 'test_helper'
require 'mocha/setup'

class EditionsFilterTest < ActiveSupport::TestCase
  describe EditionsFilter do
    let (:filter) { EditionsFilter.new }

    describe '#defaults' do
      let(:defaults) { EditionsFilter.defaults }

      it "should include a set of default values" do
        defaults.has_key?(:name).must_equal true
        defaults.has_key?(:supplement_id).must_equal true
      end

      it "should include :name field with nil value" do
        defaults[:name].must_be_nil
      end

      it "should include :supplement_id field with nil value" do
        defaults[:supplement_id].must_be_nil
      end
    end

    describe '#model' do
      it "should return a Edition class" do
        filter.model.must_be_same_as Edition
      end
    end

    describe '#sanitize' do
      it 'correctly excludes :name from the other filters' do
        values = { name: 'Some value' }
        sanitized = filter.sanitize values

        sanitized[:supplement_id].must_be_nil
        sanitized[:name].wont_be_nil
      end
    end

    describe '#filter_by_name' do
      let(:partial) { mock }
      let(:result) { [ { id: 1, date: "1990-10-19 00:00:00", name: 'Edicion 1' } ] }
      let(:query) { 'date = :date OR name like :value' }
      let(:date) { '19/10/2013' }
      let(:name) { 'Edicion 1' }

      it 'must be return a valid array if value is a date' do
        partial.expects(:where).with(query, date: "#{date}".to_date, value: "%#{date}%").returns(result)
        filter.filter_by_name(partial, date).must_equal result
      end

      it 'must be return a valid array if value is a name' do
        partial.expects(:where).with(query, date: nil, value: "%#{name}%").returns(result)
        filter.filter_by_name(partial, name).must_equal result
      end
    end

    describe 'Not mocked tests' do
      describe '#filter_by_name' do
        before do
          @edition1 = create(:edition, name: 'edition one', date: "31/01/1999")
          @edition2 = create(:edition, name: 'ediyon', date: "31/01/2005")
        end

        it 'must return a valid array' do
          filter.filter_by_name(Edition.all, 'ediyon').must_equal [@edition2]
          filter.filter_by_name(Edition.all, Date.parse('31/01/2005')).must_equal [@edition2]
          filter.filter_by_name(Edition.all, 'edition one').must_equal [@edition1]
        end
      end

      describe 'filter by various fields' do
        before do
          @edition = create(:supplement_edition)
          @edition_filter = EditionsFilter.new(values: {name: 'Edition', supplement_id: @edition.supplement_id})
        end

        it 'must return a valid array' do
          @edition_filter.all.must_equal [@edition]
          @edition_filter.all.count.must_equal 1
        end

        it 'must return an empty array if some value does not match' do
          @edition_filter[:name] = 'EditionNN'

          @edition_filter.all.must_equal []
          @edition_filter.all.count.must_equal 0
        end
      end
    end
  end
end
