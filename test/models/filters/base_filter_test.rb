require 'test_helper'

class BaseFilterTest < ActiveSupport::TestCase
  describe BaseFilter do
    let (:filter) { BaseFilter.new }

    describe 'Creation and basics' do
      describe '#defaults' do
        it 'should return a hash with default filter values' do
          BaseFilter.defaults.must_be_instance_of Hash
          BaseFilter.defaults.must_be_empty
        end
      end

      describe '#key' do
        it 'should return a representative symbol' do
          BaseFilter.key.must_be_same_as :BaseFilter
        end
      end

      describe '#[]=' do
        it 'should set values under :key key' do
          filter[:name] = 'test value'

          filter[:name].must_equal 'test value'
        end
      end

      describe '#[]' do
        it 'should access the values under :key key' do
          filter[:surname] = 'some test value'

          filter[:surname].must_equal 'some test value'
        end

        it 'should return nil if :key is not set' do
          filter[:non_existing_key].must_be_nil
        end
      end

      describe '#any?' do
        it 'should return false if there\'s no filter applied' do
          filter.any?.must_equal false
        end

        it 'should return true if there\'s a filter applied' do
          filter[:some] = 'value'
          filter.any?.must_equal true
        end
      end

      describe '#values' do
        it 'should return a hash representation of the values applied to the filter' do
          filter[:some] = 'value'
          filter[:test] = 'data'

          expected = { some: 'value', test: 'data' }

          filter.values.must_equal expected
        end
      end

      describe '#persist!' do
        it 'should store filter values in the provided store' do
          session_mock = {}
          filter[:name] = 'value'
          filter.persist! session_mock

          expected = { name: 'value' }

          session_mock.has_key?(BaseFilter.global_key).must_equal true
          session_mock[BaseFilter.global_key].has_key?(BaseFilter.key).must_equal true
          session_mock[BaseFilter.global_key][BaseFilter.key].must_equal filter.values
        end
      end

      describe '#initialize' do
        it 'should not take values from store if these are invalid' do
          expected = {}
          session_mock = { filter: { BaseFilter: { invalid_key: 'no valid value' } } }

          filter = BaseFilter.new store: session_mock

          filter.values.must_equal expected
        end

        it 'should take values from defaults if the store doesn\'t have any values' do
          session_mock = nil

          filter = BaseFilter.new store: session_mock

          filter.values.must_equal BaseFilter.defaults
        end

        it 'should not prioritize passed in values over any other value source if these are invalid' do
          expected = {}
          value = { invalid_key: 'invalid value' }
          session_mock = { filter: { BaseFilter: {} } }

          filter = BaseFilter.new store: session_mock, values: value

          filter.values.must_equal expected
        end

        it 'should not persist values by default even when a :store option is provided' do
          store_mock = {}
          expected = {}

          filter = BaseFilter.new values: { this_value: 'will not be persisted' }, store: store_mock

          store_mock.must_equal expected
        end

        it 'should not persist values if a valid store is provided and instructed to but the passed value is invalid' do
          store_mock = {}
          expected = {}
          value = { look_me: "I will fail" }

          filter = BaseFilter.new values: value, store: store_mock, persist: true

          store_mock.has_key?(BaseFilter.global_key).must_equal true
          store_mock[BaseFilter.global_key].has_key?(BaseFilter.key).must_equal true
          store_mock[BaseFilter.global_key][BaseFilter.key].must_equal expected
        end
      end

      describe '#clear!' do
        it 'should reset the filter values to :defaults' do
          filter = BaseFilter.new values: { other: 'values' }
          filter.clear!

          filter.values.must_equal BaseFilter.defaults
        end

        it 'should persist the reset filters if filter was created with persist option set to true' do
          store_mock = { filter: { BaseFilter: { look_ma: "I'm gonna disappear!" } } }

          filter = BaseFilter.new store: store_mock, persist: true
          filter.clear!

          store_mock[:filter][:BaseFilter].must_equal BaseFilter.defaults
        end

        it 'shouldn\'t persist the reset filters if filter was created with persist option set to false' do
          expected = { look_ma: "I'm gonna disappear!" }
          store_mock = { filter: { BaseFilter: expected } }

          filter = BaseFilter.new store: store_mock, persist: false
          filter.clear!

          filter.values.must_equal BaseFilter.defaults
          store_mock[:filter][:BaseFilter].must_equal expected
        end
      end

      describe '#all' do
        it 'is delegated to subclasses' do
          ->{ BaseFilter.new.all }.must_raise NotImplementedError
        end
      end

      describe '#simple_filter_key' do
        it 'is delegated to subclasses' do
          ->{ BaseFilter.new.simple_filter_key }.must_raise NotImplementedError
        end
      end

      describe '#values_to_print' do
        describe 'whit simple values' do
          it 'return not nil attriburtes' do
            expected = { body: I18n.t(:body, scope: [:articles , :filter]) + ': sarasa' }
            filter.values = { attr_nil: nil, body: 'sarasa' }
            filter.values_to_print(:articles).must_equal expected
          end
        end

        describe 'whit foreign key values' do
          before do
            @section = create(:section, id: 1)
          end

          it 'initially should return empty hash' do
            expected = {}
            filter.values_to_print(:articles).must_equal expected
          end

          it 'should remplace foreign key with correct object' do
            expected = { section_id: I18n.t(:section, scope: [:articles , :filter]) + ": " + @section.to_s }
            filter.values = { section_id: 1 }
            filter.values_to_print(:articles).must_equal expected
          end
        end

        describe 'custom method values' do
          it 'should call custom method defined' do
            class << filter
              def value_for_body(value)
                "custom " + value
              end
            end
            expected = { body: "En el cuerpo: custom value" }
            filter.values = { body: "value" }
            filter.values_to_print(:articles).must_equal expected
          end
        end
      end
    end
  end
end
