require 'test_helper'
require 'mocha/setup'

class MediaFilterTest < ActiveSupport::TestCase
  describe MediaFilter do
    let (:filter) { MediaFilter.new }

    describe '#defaults' do
      it 'should include a set of default values' do
        defaults = MediaFilter.defaults

        defaults.has_key?(:query).must_equal true
        defaults.has_key?(:article_id).must_equal true
        defaults.has_key?(:type).must_equal true
        defaults.has_key?(:date).must_equal true
      end
    end

    describe '#model' do
      it 'should return the Medium class' do
        filter.model.must_be_same_as Medium
      end
    end


    describe '#sanitize' do
      it 'correctly excludes :article_id from the other filters' do
        values = { article_id: 'Some value' }
        sanitized = filter.sanitize values

        sanitized[:type].must_be_nil
        sanitized[:date].must_be_nil
        sanitized[:query].must_be_nil
        sanitized[:article_id].wont_be_nil
      end
    end

    describe 'Filters' do
      let(:partial) { mock }

      describe '#filter_by_date_from' do
        let(:result) { [ { id: 1, date: '1990-10-19 00:00:00' },
                         { id: 2, date: '2008-08-30 00:00:00' }
                       ]}
        let(:query) { 'media.date >= ?'  }
        let(:value) { '19/10/1990' }

        it 'must return a valid array' do
          partial.expects(:where).with(query, Date.parse(value)).returns(result)
          filter.filter_by_date_from(partial, value).must_equal result
        end
      end

      describe '#filter_by_date_to' do
        let(:result) { [ { id: 1, date: '1990-10-19 00:00:00' },
                         { id: 2, date: '2008-08-30 00:00:00' }
                       ]}
        let(:query) { 'media.date <= ?'  }
        let(:value) { '19/10/2013' }

        it 'must return a valid array' do
          partial.expects(:where).with(query, Date.parse(value)).returns(result)
          filter.filter_by_date_to(partial, value).must_equal result
        end
      end

      describe '#filter_by_article_id' do
        let(:result) {[ { id: 1, articles: [{ id: 12, title: 'Articulo 12' }, { id: 15, title: 'Articulo 15' }] },
                        { id: 2, articles: [{ id: 17, title: 'Articulo 17' }, { id: 12, title: 'Articulo 12' }] }
                      ]}
        let(:value) { '12' }

        it 'must return a valid array' do
          partial.expects(:joins).with(:articles).returns(partial)
          partial.expects(:where).with(article_media: { article_id: value }).returns(result)
          filter.filter_by_article_id(partial, value).must_equal result
        end
      end

      describe '#filter_by_query' do
        let(:article_medium_one) { create(:article_medium) }
        let(:article_medium_two) { create(:article_medium) }
        let(:article_medium_three) { create(:article_medium) }
        let(:article_one) { create(:article) }
        let(:article_two) { create(:article) }
        let(:article_three) { create(:article) }
        let(:articles) { [ article_one, article_two, article_three ] }
        let(:articles_ids) { articles.collect {|x| x.id } }
        let(:article_media) { [ article_medium_one, article_medium_two, article_medium_three ] }
        let(:media_ids) { article_media.map &:medium_id }
        let(:result) { [ { id: 1, name: 'Imagen 1', content: '<iframe id="ytplayer" type="text/html" width="640" height="390"
                                                              src="http://www.youtube.com/embed/M7lc1UVf-VE?autoplay=1&origin=http://example.com"
                                                              frameborder="0"/>' } ] }
        let(:query) { 'name like :value OR media.id IN(:ids)' }

        after do
          Article.unstub :search
          ArticleMedium.unstub :where
        end

        before do
          article_media.expects(:pluck).with(:medium_id).returns(media_ids)
          article_media.expects(:uniq).returns(article_media)
          ArticleMedium.expects(:where).with(article_id: articles_ids).returns(article_media)
          articles.expects(:ids).returns(articles_ids)
        end

        it 'must return a valid array matching with id value' do
          Article.expects(:search).with(query: "%#{article_medium_one.id}%").returns(articles)
          partial.expects(:where).with(query, value: "%#{article_medium_one.id}%", ids: media_ids).returns(result)
          filter.filter_by_query(partial, article_medium_one.id).must_equal result
        end

        it 'must return a valid array matching with content value' do
          Article.expects(:search).with(query: '%youtube%').returns(articles)
          partial.expects(:where).with(query, value: '%youtube%', ids: media_ids).returns(result)
          filter.filter_by_query(partial, 'youtube').must_equal result
        end

        it 'must return a valid array matching with id value' do
          Article.expects(:search).with(query: '%Imagen 1%').returns(articles)
          partial.expects(:where).with(query, value: '%Imagen 1%', ids: media_ids).returns(result)
          filter.filter_by_query(partial, 'Imagen 1').must_equal result
        end
      end
    end

    describe 'Not mocked tests' do
      describe '#filter_by_date_from' do
        before do
          @medium1 = create(:audio_medium, date: "31/01/1999")
          @medium2 = create(:embedded_medium, date: "31/01/2005")
        end

        it 'must return a valid array' do
          filter.filter_by_date_from(Medium.all, '31/01/2003').must_equal [@medium2]
        end
      end

      describe 'filter by various fields' do
        before do
          @medium1  = create(:audio_medium, date: "31/01/1999", name: 'algoblabla')
          @article1 = create(:article, id: 15)
          @article_medium = create(:article_medium, medium: @medium1, article: @article1)
          @mediafilter = MediaFilter.new(values: {article_id: 15, query: 'algo', type: 'AudioMedium', date_from: "25/01/1999", date_to: "25/02/1999"})
        end

        it 'must return a valid array' do
          @mediafilter.all.must_equal [@medium1]
          @mediafilter.all.count.must_equal 1
        end

        it 'must return an empty array if any value does not match' do
          @mediafilter.values[:query] = 'algozzzz'
          @mediafilter.all.must_equal []
          @mediafilter.all.count.must_equal 0

          @mediafilter.values[:query] = 'algo'
          @mediafilter.values[:date_from] = '25/01/2000'
          @mediafilter.values[:date_to] = '25/02/2000'
          # Query should match again, but date will not
          @mediafilter.all.must_equal []
          @mediafilter.all.count.must_equal 0
        end
      end
    end
  end
end
