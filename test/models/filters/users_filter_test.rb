require 'test_helper'
require 'mocha/setup'

class UsersFilterTest < ActiveSupport::TestCase
  describe UsersFilter do
    let (:filter){ UsersFilter.new }

    describe '#defaults' do
      it 'should include a set of default values' do
        defaults = UsersFilter.defaults

        defaults.has_key?(:user).must_equal true
      end
    end

    describe '#model' do
      it "should return the User class" do
        filter.model.must_be_same_as User
      end
    end

    describe '#filter_by_user' do
      let(:result) { [ create(:editor) ] }
      let(:query) { 'username like ? OR email like ?' }
      let(:value) { 'editor' }
      let(:partial) { mock }

      it 'must be return a valid array' do
        partial.expects(:where).with(query, "%editor%", "%editor%").returns(result)
        filter.filter_by_user(partial, value).must_equal result
      end
    end

    describe 'Not mocked tests' do
      describe '#filter_by_user' do
        before do
          @user1 = create(:admin)
          @user2 = create(:editor)
        end

        it 'must return a valid array' do
          filter.filter_by_user(User.all, 'editor').must_equal [@user2]
        end
      end
    end
  end
end