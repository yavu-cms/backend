require 'test_helper'

class RolesFilterTest < ActiveSupport::TestCase
  describe RolesFilter do
    let (:filter) { RolesFilter.new }

    describe '#model' do
      it "should return a Role class" do
        filter.model.must_be_same_as Role
      end
    end

    describe '#defaults' do
      it "should include only :name field" do
        expected = { name: nil }
        expected.must_equal RolesFilter.defaults
      end
    end

    describe 'filter by name' do
      before do
        @role = create(:role, name: 'some role')
        @role_filter = RolesFilter.new(values: {name: 'some role'})
      end

      it 'must return a valid array' do
        @role_filter.all.must_equal [@role]
        @role_filter.all.count.must_equal 1
      end

      it 'must return an empty array if name does not match' do
        @role_filter.values[:name] = 'another role'
        @role_filter.all.must_equal []
        @role_filter.all.count.must_equal 0
      end
    end
  end

  describe '#simple_filter_key' do
    it 'is :name' do
      RolesFilter.new.simple_filter_key.must_equal :name
    end
  end
end
