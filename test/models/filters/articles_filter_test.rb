require 'test_helper'
require 'mocha/setup'

class ArticlesFilterTest < ActiveSupport::TestCase
  describe ArticlesFilter do
    let (:filter) { ArticlesFilter.new }

    describe 'Creation' do
      describe '#defaults' do
        it 'should include a set of default values' do
          defaults = ArticlesFilter.defaults

          defaults.must_equal({ body: nil, containing: nil, edition_date_from: nil, edition_date_to: nil,
                                created_time_from: nil, created_time_to: nil, last_updated_from: nil, last_updated_to: nil,
                                section_id: nil, edition_id: nil, tag: nil })
        end
      end

      describe '#model' do
        it 'should return the Article class' do
          Article.must_be_same_as filter.model
        end
      end
    end

    describe '#Sanitize' do
      it 'correctly excludes :containing from the other filters' do
        values = { containing: 'Some value' }
        sanitized = filter.sanitize values

        sanitized[:body].must_be_nil
        sanitized[:date_from].must_be_nil
        sanitized[:date_to].must_be_nil
        sanitized[:last_updated_from].must_be_nil
        sanitized[:last_updated_to].must_be_nil
        sanitized[:edition_id].must_be_nil
        sanitized[:section_id].must_be_nil
        sanitized[:tag_id].must_be_nil
        sanitized[:containing].wont_be_nil
      end
    end

    describe '#encode' do
      it 'should return a string object' do
        filter.encode('hola').must_be_instance_of String
      end

      it 'should return the same string given if it not has html entities' do
        filter.encode('hola').must_equal 'hola'
      end

      it 'should return an empty string if params is nil' do
        filter.encode.must_equal ''
      end

      it 'should escapes all entities that have names' do
        filter.encode('<h1>h&oacute;la</h1>').must_equal '&lt;h1&gt;h&amp;oacute;la&lt;/h1&gt;'
      end
    end

    describe 'Filters' do
      let(:partial) { mock }

      describe '#filter_by_body' do
        let(:result) { [ { id: 1, body: "<span>hola, como andas?</span>" }, { id: 2, body: "<span>hola, bien vos?</span>" }] }
        let(:query) { 'body like ? OR body like ?' }
        let(:value) { 'hola' }

        it 'must return a valid array' do
          partial.expects(:where).with(query, "%hola%", "%hola%").returns(result)
          filter.filter_by_body(partial, value).must_equal result
        end
      end

      describe '#filter_by_containing' do
        let(:result) { [ { id: 1, heading: "<span>hola, como andas?</span>" }, { id: 2, lead: "<span>hola, bien vos?</span>" }] }
        let(:query) { 'heading like :heading OR title like :title OR lead like :lead OR signature like :signature' }
        let(:value) { 'hola' }

        it 'must return a valid array' do
          partial.expects(:where).with(query, heading: "%hola%", title: "%hola%", lead: "%hola%", signature: "%hola%").returns(result)
          filter.filter_by_containing(partial, value).must_equal result
        end
      end

      describe '#filter_by_edition_date_from' do
        let(:result) {[ { id: 1, editions: [{ id: 1, date: '1990-10-19 00:00:00' }, { id: 2, date: '1998-08-30 00:00:00' }] },
                        { id: 2, editions: [{ id: 1, date: '1990-10-19 00:00:00' }, { id: 3, date: '2008-08-30 00:00:00' }] }
                      ]}
        let(:query) { 'editions.date >= ?' }
        let(:value) { '19/10/1999' }

        it 'must return a valid array' do
          partial.expects(:joins).with(:edition).returns(partial)
          partial.expects(:where).with(query, Date.parse(value)).returns(result)
          filter.filter_by_edition_date_from(partial, value).must_equal result
        end
      end

      describe '#filter_by_edition_date_to' do
        let(:result) {[ { id: 1, editions: [{ id: 1, date: '1990-10-19 00:00:00' }, { id: 2, date: '1998-08-30 00:00:00' }] },
                        { id: 2, editions: [{ id: 1, date: '1990-10-19 00:00:00' }, { id: 3, date: '2013-10-19 00:00:00' }] }
                      ]}
        let(:query) { 'editions.date <= ?' }
        let(:value) { '19/10/2013' }

        it 'must return a valid array' do
          partial.expects(:joins).with(:edition).returns(partial)
          partial.expects(:where).with(query, Date.parse(value)).returns(result)
          filter.filter_by_edition_date_to(partial, value).must_equal result
        end
      end

      describe '#filter_by_created_time_from' do
        let(:result) {[ { id: 1, time: '09:00:00' }, { id: 3, time: '10:01:30' },
                        { id: 2, time: '09:30:00' }, { id: 4, time: '11:02:19' }
                      ]}
        let(:query) { 'articles.time >= ?' }
        let(:value) { '09:00:00' }

        it 'must return a valid array' do
          partial.expects(:where).with(query, Time.parse(value)).returns(result)
          filter.filter_by_created_time_from(partial, value).must_equal result
        end
      end

      describe '#filter_by_created_time_to' do
        let(:result) {[ { id: 1, time: '09:00:00' }, { id: 3, time: '10:01:30' },
                        { id: 2, time: '09:30:00' }, { id: 4, time: '11:02:19' }
                      ]}
        let(:query) { 'articles.time <= ?' }
        let(:value) { '10:00:00' }

        it 'must return a valid array' do
          partial.expects(:where).with(query, Time.parse(value)).returns(result)
          filter.filter_by_created_time_to(partial, value).must_equal result
        end
      end
      describe '#filter_by_last_updated_from' do
        let(:result) {[ { id: 1, updated_at: '1990-10-19 00:00:00' }, { id: 3, updated_at: '1998-08-30 00:00:00' },
                        { id: 2, updated_at: '1998-10-19 00:00:00' }, { id: 4, updated_at: '2004-10-19 00:00:00' }
                      ]}
        let(:query) { 'articles.updated_at >= ?' }
        let(:value) { '19/10/1990' }

        it 'must return a valid array' do
          partial.expects(:where).with(query, Date.parse(value)).returns(result)
          filter.filter_by_last_updated_from(partial, value).must_equal result
        end
      end

      describe '#filter_by_last_updated_to' do
        let(:result) {[ { id: 1, updated_at: '1990-10-19 00:00:00' }, { id: 3, updated_at: '1998-08-30 00:00:00' },
                        { id: 2, updated_at: '1800-10-19 00:00:00' }, { id: 4, updated_at: '2013-10-19 00:00:00' }
                      ]}
        let(:query) { 'articles.updated_at <= ?' }
        let(:value) { '19/10/2013' }

        it 'must return a valid array' do
          partial.expects(:where).with(query, Date.parse(value)).returns(result)
          filter.filter_by_last_updated_to(partial, value).must_equal result
        end
      end

      describe '#filter_by_tag' do
        let(:result) { stub }
        let(:value) { 'Deportes' }

        it 'must return a valid array' do
          partial.expects(:joins).with(:tags).returns(partial)
          partial.expects(:where).with(tags: { name: value } ).returns(result)
          filter.filter_by_tag(partial, value).must_equal result
        end
      end

      describe '#filter_by_section_id' do
        let(:result) {[ { id: 1, section: { id: 12, name: 'Pesca', is_visible: true } },
                        { id: 2, section: { id: 12, name: 'Pesca', is_visible: true } }
                      ]}
        let(:value) { '12' }

        it 'must return a valid array' do
          partial.expects(:joins).with(:section).returns(partial)
          partial.expects(:where).with(sections: { id: value, is_visible: true }).returns(result)
          filter.filter_by_section_id(partial, value).must_equal result
        end
      end

      describe '#sort_fields' do
        it 'must return sortable fields' do
          filter.sort_fields.must_equal Article.column_names
        end
      end
    end

    describe 'Not mocked tests' do
      describe '#filter_by_containing' do
        before do
          @article1 = create(:article, title: 'AAAA')
          @article2 = create(:article, title: 'BBBB AAAA')
          @article3 = create(:article, title: 'CCCC')
        end

        it 'must return a valid array' do
          filter.filter_by_containing(Article.all, 'AAAA').must_equal [@article1, @article2]
        end
      end

      describe '#filter_by_body' do
        before do
          @article1 = create(:article, body: 'lalala')
          @article2 = create(:article, body: 'something lalala')
          @article3 = create(:article, body: 'hehehe')
        end

        it 'must return a valid array' do
          filter.filter_by_body(Article.all, 'lalala').must_equal [@article1, @article2]
        end
      end

      describe 'filter by various fields' do
        before do
          user = create :admin
          @article = create(:copyable_article)
          @article_filter = ArticlesFilter.new(user: user, values: {heading:   'heading one',
                                                        title:     'title one',
                                                        lead:      'lead one',
                                                        body:      'body one',
                                                        signature: 'signature one',
                                                        date_from: '01/01/2013',
                                                        date_to:   '25/03/2013',
                                                        edition_id: @article.edition_id,
                                                        section_id: @article.section_id} )
        end

        it 'must return a valid array' do
          @article_filter.all.must_equal [@article]
          @article_filter.all.count.must_equal 1
        end

        it 'must return an empty array if some value does not match' do
          @article_filter[:body] = 'qqqwwwwrrr'
          @article_filter.all.must_equal []
          @article_filter.all.count.must_equal 0
        end
      end
    end
  end
end
