require 'test_helper'

class TagsFilterTest < ActiveSupport::TestCase
  describe TagsFilter do
    let (:filter) { TagsFilter.new }

    describe '#model' do
      it "should return a Tag class" do
        filter.model.must_be_same_as Tag
      end
    end

    describe '#defaults' do
      it "should include only :name field" do
        expected = { name: nil }
        expected.must_equal TagsFilter.defaults
      end
    end

    describe 'filter by name' do
      before do
        @tag = create(:tag, name: 'funny tag')
        @tag_filter = TagsFilter.new(values: {name: 'funny tag'})
      end

      it 'must return a valid array' do
        @tag_filter.all.must_equal [@tag]
        @tag_filter.all.count.must_equal 1
      end

      it 'must return an empty array if name does not match' do
        @tag_filter.values[:name] = 'not so funny tag'
        @tag_filter.all.must_equal []
        @tag_filter.all.count.must_equal 0
      end
    end
  end
end