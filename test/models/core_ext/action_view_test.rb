require 'test_helper'

class ActionViewTest < ActionView::TestCase
  describe ActionView do
    describe "#pluralize" do
    it "returns a correct translate" do
      "sección".pluralize(:es).must_equal "secciones"
      "árbol".pluralize(:es).must_equal "árboles"
      "plantas".pluralize(:es).must_equal "plantas"
      "pez".pluralize(:es).must_equal "peces"
      "maní".pluralize(:es).must_equal "maníes"
      "catamarán".pluralize(:es).must_equal "catamaranes"
      "menú".pluralize(:es).must_equal "menúes"
      end
    end
  end
end