require 'test_helper'

class ExtraAttributeTest < ActiveSupport::TestCase
  describe ExtraAttribute do
    describe 'scopes' do
      describe 'describeault' do
        it 'must be ordered by name' do
          create :extra_attribute_boolean, name: 'zz'
          create :extra_attribute_boolean, name: 'aa'
          create :extra_attribute_boolean, name: 'mm'
          ExtraAttribute.all.pluck(:name).must_equal %w(aa mm zz)
        end
      end
    end

    describe '.model_class_name' do
      it 'must return a downcase string version of model class'
    end

    describe '#model_classes' do
      before do
        ExtraAttribute.stubs(:initial_model_classes).returns({ 'A' => '0', 'B' => '0', 'C' => '0' })
        @initial = ExtraAttribute.initial_model_classes
      end

      after do
        ExtraAttribute.unstub(:initial_model_classes)
      end

      describe 'when there are model classes in extra attribute that are not in initial' do
        before do
          @extra_attribute = build :extra_attribute_without_model_classes
          @extra_attribute.send :write_attribute, :model_classes, { 'D' => '1' }
        end
        it 'these model classes are not included' do
          @extra_attribute.model_classes.must_equal @initial
        end
      end
      describe 'when there model classes in initial that are not in extra attribute' do
        before do
          @extra_attribute = build :extra_attribute_without_model_classes
          @extra_attribute.send :write_attribute, :model_classes, { 'A' => '1', 'B' => '1' }
        end
        it 'these model classes are included with default values' do
          @extra_attribute.model_classes.must_equal({ 'A' => '1', 'B' => '1', 'C' => '0' })
        end
      end
      describe 'when the model classes in extra attribute are in initial' do
        before do
          @extra_attribute = build :extra_attribute_without_model_classes
          @extra_attribute.send :write_attribute, :model_classes, { 'C' => '1', 'B' => '1' }
        end
        it 'model class value in extra attribute takes precedence over initial value' do
          @extra_attribute.model_classes.must_equal @initial.merge({ 'C' => '1', 'B' => '1' })
        end
      end
    end

    describe '#serialized_model_classes' do
      it 'must return a serialized list of model classes'
    end

    describe '#is_for?' do
      let(:extra_attribute) { build :extra_attribute }

      describe 'when is allowed for that model class' do
        it 'must return true' do
          extra_attribute.send :write_attribute, :model_classes, { 'article' => '1' }
          extra_attribute.must_be :is_for?, Article
        end
      end
      describe 'when is not allowed for that model class' do
        it 'must return false' do
          extra_attribute.wont_be :is_for?, Tag
        end
      end
    end

    describe '.for' do
      let(:ea1) { create :extra_attribute }
      let(:ea2) { create :extra_attribute }
      let(:ea3) { create :extra_attribute }

      before do
        ea1.expects(:is_for?).with(Article).returns(false).once
        ea2.expects(:is_for?).with(Article).returns(true).once
        ea3.expects(:is_for?).with(Article).returns(true).once
        ExtraAttribute.expects(:all).returns([ea1,ea2,ea3]).once
      end

      after do
        ExtraAttribute.unstub(:all)
      end

      it 'must return extra attributes allowed for a certain model class' do
        ExtraAttribute.for(Article).must_equal [ ea2, ea3 ]
      end
    end

    describe '.available_model_classes' do
      it 'must return all available model classses names'
    end

    describe '.initial_model_classes' do
      it 'must return a hash with initial model classes names and their default values' do
        ExtraAttribute.initial_model_classes.must_equal({
          'article' => '0',
          'section' => '0',
          'edition' => '0',
          'supplement' => '0',
          'client_application' => '0'
        })
      end
    end
  end
end