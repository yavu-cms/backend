require 'test_helper'
require "mocha/setup"

class ArticlesOrderTest < ActiveSupport::TestCase
  describe ArticlesOrder do
    let (:order) { ArticlesOrder.new(sort:{ field: nil, order: nil }) }

    describe '#sort_fields' do
      it "should return a specific array of fields" do
        expected = Article.column_names + ['section']

        order.sort_fields.must_be_instance_of Array
        order.sort_fields.must_equal expected
      end
    end

    describe '#default_field' do
      it ":default_field should be return 'time'" do
        order.default_field.must_equal 'time'
      end
    end

    describe '#default_order' do
      it ":default_order should be return :desc" do
        order.default_order.must_equal :desc
      end
    end

    describe '#column_prefix' do
      it ":column_prefix should be return 'articles'" do
        order.column_prefix.must_equal 'articles'
      end
    end

    describe '#sort_list' do
      let(:matches) { mock }

      it "should return true ordering by field section in desc order" do
        order = ArticlesOrder.new(sort: { field: 'section', order: :desc })
        matches.expects(:joins).with(:section).returns(matches)
        matches.expects(:order).with('sections.name desc').returns(true)

        order.sort_list(matches).must_equal true
      end

      it "should return true ordering by field section in asc order" do
        order = ArticlesOrder.new(sort: { field: 'section', order: :asc })
        matches.expects(:joins).with(:section).returns(matches)
        matches.expects(:order).with('sections.name asc').returns(true)

        order.sort_list(matches).must_equal true
      end

      it "should return true ordering by field section in desc like order by default" do
        order = ArticlesOrder.new(sort: { field: 'section', order: nil })
        matches.expects(:joins).with(:section).returns(matches)
        matches.expects(:order).with('sections.name desc').returns(true)

        order.sort_list(matches).must_equal true
      end

      it "should return true ordering by field title in desc order" do
        order = ArticlesOrder.new(sort: { field: 'title', order: :desc })
        matches.expects(:order).with('articles.title desc').returns(true)

        order.sort_list(matches).must_equal true
      end

      it "should return true ordering by field title in asc order" do
        order = ArticlesOrder.new(sort: { field: 'title', order: :asc })
        matches.expects(:order).with('articles.title asc').returns(true)

        order.sort_list(matches).must_equal true
      end

      it "should return true ordering by field title in desc like order by default" do
        order = ArticlesOrder.new(sort: { field: 'title', order: nil })
        matches.expects(:order).with('articles.title desc').returns(true)

        order.sort_list(matches).must_equal true
      end

      it "should return true ordering by field time in desc order" do
        order = ArticlesOrder.new(sort: { field: 'time', order: :desc })
        matches.expects(:order).with('articles.time desc').returns(true)

        order.sort_list(matches).must_equal true
      end

      it "should return true ordering by field time in asc order" do
        order = ArticlesOrder.new(sort: { field: 'time', order: :asc })
        matches.expects(:order).with('articles.time asc').returns(true)

        order.sort_list(matches).must_equal true
      end

      it "should return true ordering by field time in desc like order by default" do
        order = ArticlesOrder.new(sort: { field: 'time', order: nil })
        matches.expects(:order).with('articles.time desc').returns(true)

        order.sort_list(matches).must_equal true
      end

      it "should return true ordering by field time by default in desc like order by default" do
        order = ArticlesOrder.new(sort: { field: nil, order: nil })
        matches.expects(:order).with('articles.time desc').returns(true)

        order.sort_list(matches).must_equal true
      end
    end
  end
end
