require 'test_helper'
require "mocha/setup"

class SectionsOrderTest < ActiveSupport::TestCase
  describe SectionsOrder do
    let (:order) { SectionsOrder.new(sort:{ field: nil, order: nil }) }

    describe '#sort_fields' do
      after do
        Section.unstub :column_names
      end

      it "should return an Array" do
        column_names = []
        Section.expects(:column_names).returns(column_names).twice

        assert_instance_of Array, order.sort_fields
      end

      it "should return a specific array of strings" do
        column_names = ["id", "name", "slug", "is_visible", "supplement_id", "parent_id", "created_at", "updated_at" ]
        Section.expects(:column_names).returns(column_names).twice

        assert_equal column_names, order.sort_fields, 'The column fields of Edition changed'
      end
    end

    describe '#default_field' do
      it "should return 'name'" do
        assert_equal 'name', order.default_field
      end
    end

    describe '#default_order' do
      it "should return :asc" do
        assert_equal :asc, order.default_order
      end
    end

    describe '#column_prefix' do
      it "should return 'tags'" do
        assert_equal 'sections', order.column_prefix
      end
    end

    describe '#sort_list' do
      let(:matches) { mock }

      it "should return true ordering by field name in desc order" do
        order = SectionsOrder.new(sort: { field: 'name', order: :desc })
        matches.expects(:order).with('sections.name desc').returns(true)

        assert order.sort_list(matches)
      end

      it "should return true ordering by field name in asc order" do
        order = SectionsOrder.new(sort: { field: 'name', order: :asc })
        matches.expects(:order).with('sections.name asc').returns(true)

        assert order.sort_list(matches)
      end

      it "should return true ordering by field name in asc like order by default" do
        order = SectionsOrder.new(sort: { field: 'name', order: nil })
        matches.expects(:order).with('sections.name asc').returns(true)

        assert order.sort_list(matches)
      end

      it "should return true ordering by field name by default in asc like order by default" do
        order = SectionsOrder.new(sort: { field: nil, order: nil })
        matches.expects(:order).with('sections.name asc').returns(true)

        assert order.sort_list(matches)
      end
    end
  end
end
