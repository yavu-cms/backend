require 'test_helper'

# Monkey-patching in order to make this class testeable
BaseOrder
class BaseOrder
  def default_field
    'default_field'
  end

  def sort_fields
    ['default_field', 'another_field']
  end
end


class BaseOrderTest < ActiveSupport::TestCase
  describe BaseOrder do
    before do
      @order = BaseOrder.new sort: { field: nil, order: nil }
    end

    describe '#sanitize' do
      describe 'when params are correct' do
        it 'should asign those params as order values' do
          order = BaseOrder.new sort: { field: 'another_field', order: :desc }
          order.sort[:field].must_equal 'another_field'
          order.sort[:order].must_equal :desc
        end
      end

      describe 'when params are not correct' do
        it 'should assign default values' do
          order = BaseOrder.new sort: { field: 'hack/hack?hack', order: 'hack?hack/hack' }
          order.sort[:field].must_equal 'default_field'
          order.sort[:order].must_equal :asc
        end
      end

      describe 'when params are empty' do
        it 'should assign default values' do
          order = BaseOrder.new sort: { field: nil, order: nil }
          order.sort[:field].must_equal 'default_field'
          order.sort[:order].must_equal :asc
        end
      end
    end

    describe '#no_sort_params?' do
      describe 'when there are sorting params' do
        it 'should return false' do
          order = BaseOrder.new sort: { field: 'name', order: :asc }
          order.no_sort_params?.must_equal false
        end
      end

      describe 'when there are not sorting params' do
        it 'should return true' do
          order = BaseOrder.new sort: { field: nil, order: nil }
          order.no_sort_params?.must_equal true
        end
      end
    end

    describe '#sorted_by?' do
      describe 'when order is set to that param' do
        it "should return true" do
          @order.sort = { field: 'name', order: :asc }

          @order.sorted_by?('name').must_equal true
        end
      end

      describe 'when order is not set to that param' do
        it 'should return false' do
          @order.sort = { field: 'name', order: :asc }

          @order.sorted_by?('time').must_equal false
        end
      end
    end

    describe '#order' do
      describe 'when order is set to given criteria' do
        it 'should return true' do
          @order.sort = { field: 'name', order: :asc }

          @order.order?(:asc).must_equal true
        end
      end

      describe 'when order is not set to given criteria' do
        it 'should return false' do
          @order.sort = { field: "name", order: :desc }

          @order.order?(:asc).must_equal false
        end
      end
    end

    describe '#ascendent?' do
      describe 'when order is ascendent' do
        it 'should return true' do
          @order.sort = { field: "name", order: :asc }

          @order.ascendent?.must_equal true
        end
      end

      describe 'when order is not ascendent' do
        it 'should return false' do
          @order.sort = { field: "name", order: :desc }

          @order.ascendent?.must_equal false
        end
      end
    end

    describe '#descendent?' do
      describe 'when order is descendent' do
        test ":descendent? Check if the order is descendent, should return true" do
          @order.sort = { field: "name", order: :desc }

          @order.descendent?.must_equal true
        end
      end

      describe 'when order is not descendent' do
        it 'should return false' do
          @order.sort = { field: "name", order: :asc }

          @order.descendent?.must_equal false
        end
      end
    end

    describe '#opposite_order' do
      describe 'when order is :asc' do
        it 'should return :desc' do
          @order.sort = { field: "name", order: :asc }

          @order.opposite_order.must_equal :desc
        end
      end

      describe 'when order is :desc' do
        it 'should return :asc' do
          @order.sort = { field: "name", order: :desc }

          @order.opposite_order.must_equal :asc
        end
      end

      describe 'when order is anything else' do
        it 'should return default order (:asc)' do
          @order.sort = { field: "name", order: 'sarasa' }

          @order.opposite_order.must_equal :asc
        end
      end
    end
  end
end
