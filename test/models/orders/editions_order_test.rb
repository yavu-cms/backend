require 'test_helper'
require "mocha/setup"

class EditionsOrderTest < ActiveSupport::TestCase
  describe EditionsOrder do
    let (:order) { EditionsOrder.new(sort:{ field: nil, order: nil }) }

    describe '#sort_fields' do
      after do
        Edition.unstub :column_names
      end

      it "should return an Array" do
        column_names = []
        Edition.expects(:column_names).returns(column_names).twice

        order.sort_fields.must_be_instance_of Array
      end

      it "should return an Array" do
        column_names = ["id", "name", "date", "is_visible", "is_active", "supplement_id", "created_at", "updated_at"]
        expected = column_names + ["supplement", "edition"]
        Edition.expects(:column_names).returns(column_names).twice

        order.sort_fields.must_equal expected
      end
    end

    describe '#default_field' do
      it "should return 'created_at'" do
        order.default_field.must_equal 'created_at'
      end
    end

    describe '#default_order' do
      it "should return :desc" do
        order.default_order.must_equal :desc
      end
    end

    describe '#column_prefix' do
      it "should return 'editions'" do
        order.column_prefix.must_equal 'editions'
      end
    end

    describe '#sort_list' do
      let(:matches) { mock }

      it "should return true ordering by field edition in desc order" do
        order = EditionsOrder.new(sort: { field: 'edition', order: :desc })

        matches.expects(:order).with('editions.date desc').returns( true )
        order.sort_list(matches).must_equal true
      end

      it "should return true ordering by field edition in asc order" do
        order = EditionsOrder.new(sort: { field: 'edition', order: :asc })

        matches.expects(:order).with('editions.date asc').returns( true )
        order.sort_list(matches).must_equal true
      end

      it "should return true ordering by field edition in desc like order by default" do
        order = EditionsOrder.new(sort: { field: 'edition', order: nil })

        matches.expects(:order).with('editions.date desc').returns( true )
        assert order.sort_list(matches)
      end

      it "should return true ordering by field supplement in desc order" do
        order = EditionsOrder.new(sort: { field: 'supplement', order: :desc })

        matches.expects(:joins).with(:supplement).returns(matches)
        matches.expects(:order).with('supplements.name desc').returns(true)
        order.sort_list(matches).must_equal true
      end

      it "should return true ordering by field supplement in asc order" do
        order = EditionsOrder.new(sort: { field: 'supplement', order: :asc })

        matches.expects(:joins).with(:supplement).returns(matches)
        matches.expects(:order).with('supplements.name asc').returns(true)
        order.sort_list(matches).must_equal true
      end

      it "should return true ordering by field supplement in desc order like default order" do
        order = EditionsOrder.new(sort: { field: 'supplement', order: nil })

        matches.expects(:joins).with(:supplement).returns(matches)
        matches.expects(:order).with('supplements.name desc').returns(true)
        order.sort_list(matches).must_equal true
      end

      it "should return true ordering by field is_visible in desc order" do
        order = EditionsOrder.new(sort: { field: 'is_visible', order: :desc })

        matches.expects(:order).with('editions.is_visible desc').returns(true)
        order.sort_list(matches).must_equal true
      end

      it "should return true ordering by field is_visible in asc order" do
        order = EditionsOrder.new(sort: { field: 'is_visible', order: :asc })

        matches.expects(:order).with('editions.is_visible asc').returns(true)
        order.sort_list(matches).must_equal true
      end

      it "should return true ordering by field is_visible in desc order like default order" do
        order = EditionsOrder.new(sort: { field: 'is_visible', order: nil })

        matches.expects(:order).with('editions.is_visible desc').returns(true)
        order.sort_list(matches).must_equal true
      end

      it "should return true ordering by field created_at by default in desc order" do
        order = EditionsOrder.new(sort: { field: nil, order: :desc })

        matches.expects(:order).with('editions.created_at desc').returns( true )
        order.sort_list(matches).must_equal true
      end

      it "should return true ordering by field created_at by default in asc order" do
        order = EditionsOrder.new(sort: { field: nil, order: :asc })

        matches.expects(:order).with('editions.created_at asc').returns( true )
        order.sort_list(matches).must_equal true
      end

      it "should return true ordering by field created_at by default in desc like order by default" do
        order = EditionsOrder.new(sort: { field: nil, order: nil })

        matches.expects(:order).with('editions.created_at desc').returns( true )
        order.sort_list(matches).must_equal true
      end
    end
  end
end