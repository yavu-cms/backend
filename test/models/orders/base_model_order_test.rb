require 'test_helper'
require "mocha/setup"

class BaseModelOrderTest < ActiveSupport::TestCase
  describe BaseModelOrder do
    describe 'With model associated' do
      before do
        @model = mock
        @model.stubs(:column_names).returns([])

        BaseModelOrder.any_instance.stubs(:model).returns(@model)
        @base_model_order = BaseModelOrder.new(sort:{ field: nil, order: nil })
      end

      after do
        BaseModelOrder.unstub :model
      end

      describe '#default_field' do
        before do
          @base_model_order.model.stubs(:column_names).returns(['column_one', 'column_two'])
        end

        it 'should return this class default order field' do
          @base_model_order.default_field.must_equal 'column_one'
        end
      end
    end

    describe 'Without model associated' do
      describe '#model' do
        before do
          BaseModelOrder.any_instance.stubs(:sort_fields).returns([])
          @base_model_order = BaseModelOrder.new(sort:{ field: nil, order: nil })
        end

        after do
          BaseModelOrder.unstub :sort_fields
        end

        it 'should raise an error' do
          ->{ @base_model_order.model }.must_raise NotImplementedError
        end
      end
    end
  end
end
