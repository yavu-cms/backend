require 'test_helper'

class RolesOrderTest  < ActiveSupport::TestCase
  describe RolesOrder do
    let (:order) { RolesOrder.new(sort:{ field: nil, order: nil }) }

    describe '#sort_fields' do
      after do
        Role.unstub :column_names
      end

      it "should return an Array" do
        column_names = []
        Role.expects(:column_names).returns(column_names).twice

        order.sort_fields.must_be_instance_of Array
      end

      it "should return a specific array of strings" do
        column_names = ["id", "name", "slug", "created_at", "updated_at", "combiner_id"]
        Role.expects(:column_names).returns(column_names).twice

        order.sort_fields.must_equal column_names
      end
    end

    describe '#default_field' do
      it "should return 'name'" do
        order.default_field.must_equal 'name'
      end
    end

    describe '#default_order' do
      it ":should return :asc" do
        order.default_order.must_equal :asc
      end
    end

    describe '#column_prefix' do
      it "should return 'roles'" do
        order.column_prefix.must_equal 'roles'
      end
    end

    describe '#sort_list' do
      let(:matches) { mock }

      it "should return true ordering by field name in desc order" do
        order = RolesOrder.new(sort: { field: 'name', order: :desc })
        matches.expects(:order).with('roles.name desc').returns(true)

        order.sort_list(matches).must_equal true
      end

      it "should return true ordering by field name in asc order" do
        order = RolesOrder.new(sort: { field: 'name', order: :asc })
        matches.expects(:order).with('roles.name asc').returns(true)

        order.sort_list(matches).must_equal true
      end

      it "should return true ordering by field name in asc like order by default" do
        order = RolesOrder.new(sort: { field: 'name', order: nil })
        matches.expects(:order).with('roles.name asc').returns(true)

        order.sort_list(matches).must_equal true
      end
    end
  end
end
