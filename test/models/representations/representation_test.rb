require 'test_helper'

class RepresentationTest < ActiveSupport::TestCase
  describe Representation do
    describe '#touch_routes' do
      let(:representation) { create :representation }
      it 'must call routes.update_all with :updated_at Time.now' do
        representation.routes.expects(:update_all).once
        representation.touch_routes
      end
      it 'must call routes.update_all with :updated_at Time.now' do
        representation.client_application.expects(:touch).once
        representation.touch_routes
      end
    end

    describe 'Draft management' do
      it 'must include Representation::DraftManagement concern' do
        Representation.included_modules.must_include Representations::DraftManagement
      end
    end

    describe 'Backup management' do
      it 'must include Representation::BackupManagement concern' do
        Representation.included_modules.must_include Representations::BackupManagement
      end
    end

    describe 'Destruction' do
      describe 'when it has a draft' do
        let(:representation) { create :representation }
        let(:draft) { create :representation_draft }

        before do
          representation.draft = draft
        end

        it 'deletes the draft' do
          representation.draft.must_equal draft
          representation.destroy
          representation.must_be :destroyed?
          draft.must_be :destroyed?
        end
      end

      describe 'when it has backups' do
        let(:representation) { create :representation }
        let(:backup) { create :representation_backup, representation: representation }

        before do
          representation.backups << backup
        end

        it 'deletes the backups' do
          representation.backups.must_equal [backup]
          representation.destroy
          representation.must_be :destroyed?
          backup.must_be :destroyed?
        end
      end
    end

    describe '#copy_and_save_as' do
      describe 'regarding :being_copied flag' do
        let(:representation) { create :representation }

        describe 'when the copy type is "Representation"' do
          before do
            Representation.any_instance.expects(:being_copied=).with(true)
            Representation.any_instance.expects(:being_copied=).with(false)
          end

          it "doesn't set the :being_copied flag" do
            representation.copy_and_save_as Representation, 'Cool name'
          end
        end

        describe 'when the copy type is "RepresentationBackup"' do
          before { Representation.any_instance.expects(:being_copied=).with(true).twice }

          it 'sets the :being_copied_flag to true to avoid generating drafts for the copied object' do
            representation.copy_and_save_as RepresentationBackup, 'Cool name'
          end
        end

        describe 'when the copy type is "RepresentationDraft"' do
          before { Representation.any_instance.expects(:being_copied=).with(true).twice }

          it 'sets the :being_copied_flag to true to avoid generating drafts for the copied object' do
            representation.copy_and_save_as RepresentationDraft, 'Cool name'
          end
        end
      end

      describe 'when creating a new backup' do
        let(:representation) { create :representation }

        it 'copies the representation, persists the copy with a new type and returns the new instance' do
          backup = representation.copy_and_save_as(RepresentationBackup, 'Nice name')
          backup.must_be :persisted?
          backup.name.must_equal 'Nice name'
          backup.type.must_equal 'RepresentationBackup'
          backup.must_be_instance_of RepresentationBackup
          backup.representation.must_equal representation
        end
      end

      describe 'when creating a new draft' do
        let(:representation) { create :representation }

        it 'copies the representation, persists the copy with a new type and returns the new instance' do
          draft = representation.copy_and_save_as(RepresentationDraft, 'A nicer name')
          draft.must_be :persisted?
          draft.name.must_equal 'A nicer name'
          draft.type.must_equal 'RepresentationDraft'
          draft.must_be_instance_of RepresentationDraft
          draft.representation.must_equal representation
        end
      end
    end

    describe '#copy' do
      describe 'when the representation has a draft' do
        let(:representation) { create :representation }

        before do
          representation.draft = build(:representation_draft, representation: representation)
        end

        it "doesn't copy the draft" do
          new_one = representation.copy
          new_one.wont_be :has_draft?
        end
      end

      describe 'when the representation has backups' do
        let(:representation) { create :representation }

        before do
          representation.backups << build(:representation_backup, representation: representation)
        end

        it "doesn't copy the backups" do
          new_one = representation.copy
          new_one.wont_be :has_backups?
        end
      end
    end

    describe '#change_references_to' do
      let(:client_application) { create :client_application }
      let(:representation) { create :representation, client_application: client_application }
      let(:another_representation) { create :representation, client_application: client_application }

      describe 'associated routes' do
        let(:home) { create :homepage_route, representation: representation, client_application: client_application }
        let(:article) { create :article_route, representation: representation, client_application: client_application }
        let(:section) { create :section_route, representation: representation, client_application: client_application }

        before do
          @routes = [home, article, section]
          representation.send(:change_references_to, another_representation)
        end

        it 'updates any associated route so that it points at another representation' do
          @routes.each do |route|
            route.reload
            route.representation.must_equal another_representation
          end
          representation.reload
          representation.routes.must_be_empty
        end
      end

      describe 'associated backups' do
        before do
          @backups = 2.times.map { |i| create :representation_backup, representation: representation, name: "Backup #{i}" }
          representation.backups = @backups
          representation.send(:change_references_to, another_representation)
        end

        it 'updates any associated backup so that it points at another representation' do
          @backups.wont_be_empty
          @backups.each do |backup|
            backup.reload
            backup.representation.must_equal another_representation
          end
          representation.reload
          representation.backups.must_be_empty
        end
      end
    end

    describe '#replace_with' do
      let(:representation) { create :representation }

      describe '... a draft' do
        let(:a_draft)  { create :representation_draft, representation: representation }

        describe 'common steps' do
          after do
            Representation.unstub :touch_routes, :trigger_draft_creation
          end

          it "turns the draft into a Representation with this representation's name" do
            rep_name = representation.name
            representation.send :replace_with, a_draft
            a_new_rep = Representation.find(a_draft.id)
            a_new_rep.must_be_instance_of Representation
            a_new_rep.name.must_equal rep_name
          end
          it 'calls #change_references_to with the new representation' do
            representation.expects(:change_references_to).with(instance_of(Representation)).once
            representation.send :replace_with, a_draft
          end
          it 'calls #touch_routes on the new representation' do
            Representation.any_instance.expects(:touch_routes).once
            representation.send :replace_with, a_draft
          end
          it 'calls #trigger_draft_creation on the new representation' do
            Representation.any_instance.expects(:trigger_draft_creation).times(3)
            representation.send :replace_with, a_draft
          end
        end

        describe 'becomes: parameter - changing the type of the representation' do
          it 'changes the type of the representation' do
            representation.send :replace_with, a_draft, become: RepresentationBackup
            RepresentationBackup.find(representation.id).must_be_instance_of RepresentationBackup
          end
          it 'takes precedence over destroy' do
            # If +become+ and +destroy+ parameters are set, representation wont be destroyed
            representation.send :replace_with, a_draft, become: RepresentationBackup, destroy: true
            # If representation was destroyed, the next line would raise an ActiveRecord::RecordNotFound exception
            # But it wont, because +become+ has more priority over +destroy+
            RepresentationBackup.find(representation.id).wont_be_nil
          end
          it 'when actual representation turns into a backup, it must have to belongs to the backups collection of the new one' do
            representation.send :replace_with, a_draft, become: RepresentationBackup
            Representation.find(a_draft.id).backups.to_a.must_equal [ RepresentationBackup.find(representation.id) ]
          end

          it 'trims the backups to avoid storing too many of them' do
            representation.backups = create_list :representation_backup, 8, representation: representation, created_at: 5.minutes.ago
            representation.send :replace_with, a_draft, become: RepresentationBackup
            RepresentationBackup.find(representation.id).representation.backups.count.must_equal 5
          end
        end

        describe 'destroy: parameter - destroying the representation' do
          it 'destroys the representation' do
            representation.send :replace_with, a_draft
            ->{ representation.reload }.must_raise ActiveRecord::RecordNotFound
          end
        end
      end

      describe '... a backup' do
        let(:a_backup)  { create :representation_backup }

        describe 'common steps' do
          after do
            Representation.unstub(:touch_routes)
          end

          it "turns the backup into a Representation with this representation's name" do
            rep_name = representation.name
            representation.send :replace_with, a_backup
            a_new_rep = Representation.find(a_backup.id)
            a_new_rep.must_be_instance_of Representation
            a_new_rep.name.must_equal rep_name
          end
          it 'calls #change_references_to with the new representation' do
            representation.expects(:change_references_to).with(instance_of(Representation)).once
            representation.send :replace_with, a_backup
          end
          it 'calls #touch_routes on the new representation' do
            Representation.any_instance.expects(:touch_routes).once
            representation.send :replace_with, a_backup
          end
        end

        describe 'becomes: parameter - changing the type of the representation' do
          it 'changes the type of the representation' do
            representation.send :replace_with, a_backup, become: RepresentationDraft
            RepresentationDraft.find(representation.id).must_be_instance_of RepresentationDraft
          end
          it 'takes precedence over destroy' do
            # If +become+ and +destroy+ parameters are set, representation wont be destroyed
            representation.send :replace_with, a_backup, become: RepresentationBackup, destroy: true
            # If representation was destroyed, the next line would raise an ActiveRecord::RecordNotFound exception
            # But it wont, because +become+ has more priority over +destroy+
            RepresentationBackup.find(representation.id).wont_be_nil
          end
          it 'when actual representation turns into a backup, it must have to belongs to the backups collection of the new one' do
            representation.send :replace_with, a_backup, become: RepresentationBackup
            Representation.find(a_backup.id).backups.to_a.must_equal [ RepresentationBackup.find(representation.id) ]
          end
        end

        describe 'destroy: parameter - destroying the representation' do
          it 'destroys the representation' do
            representation.send :replace_with, a_backup
            ->{ representation.reload }.must_raise ActiveRecord::RecordNotFound
          end
        end
      end
    end
  end
end