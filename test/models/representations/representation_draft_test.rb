require 'test_helper'

class RepresentationDraftTest < ActiveSupport::TestCase
  describe RepresentationDraft do
    describe '#apply!' do
      let(:representation) { build :representation }
      let(:draft) { create :representation_draft }

      before do
        draft.expects(:representation).returns(representation)
        representation.expects(:apply_draft!).once
      end

      it 'applies itself onto the representation using #apply_draft!' do
        draft.apply!
      end
    end

    describe 'Validations' do
      describe 'representation presence' do
        describe 'when a draft is built without a representation' do
          let(:draft) { build :representation_draft, representation: nil }

          it 'fails to validate' do
            draft.wont_be :valid?
            draft.errors[:representation].must_include I18n.t('errors.messages.blank')
          end
        end

        describe 'when a backup is built for a representation' do
          let(:representation) { build :representation }
          let(:draft) { build :representation_draft, representation: representation }

          it 'succeeds' do
            draft.must_be :valid?
          end
        end
      end
    end
  end
end