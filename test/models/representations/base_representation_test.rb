require 'test_helper'

class BaseRepresentationTest < ActiveSupport::TestCase
  describe BaseRepresentation do
    describe '#to_s' do
      let(:representation) { build :representation }

      it 'returns the name of the representation' do
        representation.to_s.must_equal representation.name
      end
    end

    describe '#component_assets' do
      it 'needs tests'
    end

    describe '#cover_articles' do
      let(:representation) { create :representation }
      let(:article1) { create :article }
      let(:article2) { create :article }
      let(:article3) { create :article }

      before do
        representation.rows << (row = build(:row, representation: representation))
        row.columns << (column = build(:column, row: row))
        column.component_configurations << (ccc = build(:cover_component_configuration, column: column))
        ccc.cover_articles_component_configurations = (cacc1, cacc2 = build_list(:cover_articles_component_configuration, 2))
        # Add article2 and article1 to cacc1
        cacc1.articles_cover_articles_component_configurations = [article2, article1].map.with_index do |article, index|
          create(:articles_cover_articles_component_configuration_no_view, cover_articles_component_configuration: cacc1, article: article)
        end
        # Add article2 and article3 to cacc2
        cacc2.articles_cover_articles_component_configurations = [article2, article3].map.with_index do |article, index|
          create :articles_cover_articles_component_configuration_no_view, cover_articles_component_configuration: cacc2, article: article
        end
        representation.reload
      end

      it 'must return all published articles in this representation without duplicates and in correct order' do
        representation.cover_articles.must_equal [article2, article1, article3]
      end
    end

    describe '#cover?' do
      describe 'when the representation has cover components' do
        let(:representation) { create :representation }
        let(:row) { create :row, representation: representation, order: 1 }
        let(:column) { create :column, row: row, order: 1 }
        let(:cover_component_configuration) { create :cover_component_configuration, column: column }

        before do
          column.component_configurations << cover_component_configuration
          row.columns << column
          representation.rows << row
        end

        it 'returns true' do
          representation.must_be :cover?
        end
      end

      describe "when the representation doesn't have any cover components" do
        let(:representation) { create :representation }
        let(:row) { create :row, representation: representation, order: 1 }
        let(:column) { create :column, row: row, order: 1 }
        let(:component_configuration) { create :component_configuration }

        before do
          column.component_configurations << component_configuration
          row.columns << column
          representation.rows << row
        end

        it 'returns false' do
          representation.wont_be :cover?
        end
      end
    end

    describe '#can_have_logo?' do
      let(:representation) { create :representation }

      it 'must return false' do
        representation.can_have_logo?.must_equal false
      end
    end

    describe '#update_assets!' do
      let(:representation) { create :representation }
      let(:row) { create :row, representation: representation, order: 1 }
      let(:column) { create :column, row: row, order: 1 }
      let(:component) { create(:component)}
      let(:component_js_asset)  { create(:component_asset_javascript, component: component) }
      let(:component_css_asset) { create(:component_asset_stylesheet, component: component) }
      let(:client_application_js_asset) { create(:client_application_asset_javascript, client_application: representation.client_application) }
      let(:client_application_css_asset) { create(:client_application_asset_stylesheet, client_application: representation.client_application) }
      let(:component_configuration) { create :component_configuration, component: component, column: column }

      describe 'file generation' do
        before do
          component_js_asset  # force asset creation
          component_css_asset # force asset creation
          client_application_js_asset
          client_application_css_asset
          component_configuration # force component_configuration creation and so, representation population
          representation.reload
          representation.assets << client_application_js_asset
          representation.assets << client_application_css_asset
          File.unlink(representation.send(:main_javascript_file)) rescue nil
          File.unlink(representation.send(:main_stylesheet_file)) rescue nil
        end

        it 'creates both the JS and CSS files requiring every asset associated to the representation' do
          # it creates both main files
          js_name = representation.send :main_javascript_file
          css_name = representation.send :main_stylesheet_file
          File.wont_be :exists?, js_name
          File.wont_be :exists?, css_name
          # Trigger assets update
          representation.update_assets!
          # Check for files existence
          File.must_be :exists?, js_name
          File.must_be :exists?, css_name

          # it requires every javascript associated to the representation & its components
          lines = File.readlines(representation.send(:main_javascript_file))
          lines.grep(/^\/\/= require #{component_js_asset.relative_url_without_extension}$/).wont_be_empty
          lines.grep(/^\/\/= require #{client_application_js_asset.relative_url_without_extension}$/).wont_be_empty

          # it requires every component stylesheet & those associated to the representation
          lines = File.readlines(representation.send(:main_stylesheet_file))
          lines.grep(/^ \*= require #{component_css_asset.relative_url_without_extension}$/).wont_be_empty
          lines.grep(/^ \*= require #{client_application_css_asset.relative_url_without_extension}$/).wont_be_empty
        end
      end

      describe 'when an asset is added' do
        before do
          representation.assets.destroy_all
        end

        it 'must be called' do
          Representation.any_instance.expects(:update_assets!).once
          representation.assets << client_application_js_asset
        end
      end

      describe 'when an asset is removed' do
        before do
          representation.assets.destroy_all
        end

        it 'must be called' do
          Representation.any_instance.expects(:update_assets!).twice
          representation.assets << client_application_js_asset
          representation.assets.destroy client_application_js_asset
        end
      end

      describe 'when all assets are removed' do
        before do
          representation.assets << client_application_js_asset
        end

        it 'must be called' do
          representation.assets.count.must_be :>=, 1
          Representation.any_instance.expects(:update_assets!).times(representation.assets.count)
          representation.assets.destroy_all
        end
      end

      describe 'when component_configuration is added to some column' do
        it 'must be called' do
          Representation.any_instance.expects(:update_assets!).once
          representation = create :representation
          row = create :row, representation: representation
          column = create :column, row: row

          create :cover_component_configuration, column: column
        end
      end

      describe 'when component_configuration is removed from column' do
        it 'must be called' do
          Representation.any_instance.expects(:update_assets!).twice
          representation = create :representation_with_component
          representation.component_configurations.first.destroy
        end
      end

      describe 'when component_asset is added to some component which configuration is used by this representation' do
        it 'must be called' do
          Representation.any_instance.expects(:update_assets!).times(3)
          representation = create :representation_with_component
          component = representation.component_configurations.first.component
          create :component_asset, component: component
        end
      end

      describe 'when component_asset is removed from some component which configuration is used by this representation' do
        it 'must be called' do
          Representation.any_instance.expects(:update_assets!).times(4)
          representation = create :representation_with_component
          component = representation.component_configurations.first.component
          create :component_asset, component: component
          component.reload
          component.assets.first.destroy
        end
      end
    end

    describe '#favicon' do
      describe 'when representation has a favicon' do
        before do
          @client_application = create :client_application
          @representation = create :representation, client_application: @client_application
          @asset = create :client_application_asset, client_application: @client_application
          @representation.favicon = @asset
        end

        it 'should return its asset' do
          @representation.favicon.must_equal @asset
        end
      end

      describe 'when representation doesn\'t have a favicon' do
        before do
          @client_application = create :client_application
          @representation = create :representation, client_application: @client_application
          @asset = create :client_application_asset, client_application: @client_application
          @client_application.favicon = @asset
        end

        describe 'when strict is set to false (default)' do
          it 'should return its client application\'s favicon, if any' do
            @representation.favicon.must_equal @client_application.favicon
          end
        end

        describe 'when strict is set to true' do
          it "should not return its client app's favicon" do
            @representation.favicon(strict: true).must_be_nil
          end
        end
      end
    end

    describe '#copy_rows' do
      before do
        @rows = (1..4).map { |i| build(:row, order: i) }
        @representation = create :representation,
          rows: @rows,
          view: build(:view_with_placeholders, name: 'main view name'),
          default_row_view: build(:view, name: 'row view name', value: 'row view value'),
          default_column_view: build(:view, name: 'column view name', value: 'column view value'),
          error_view: build(:view, name: 'error view name', value: 'error view value')
        @copy_representation = create :representation,
          view: build(:view_with_placeholders, name: 'main view name'),
          default_row_view: build(:view, name: 'row view name', value: 'row view value'),
          default_column_view: build(:view, name: 'column view name', value: 'column view value'),
          error_view: build(:view, name: 'error view name', value: 'error view value')
      end
      it 'must call Row#copy as many times as rows have present in the representation' do
        row = mock.responds_like Row.new
        row.stubs(:representation=)
        @representation.rows.wont_be_empty
        @representation.rows.each { |r| r.expects(:copy).returns(row).once }
        @representation.send :copy_rows, @copy_representation
        @representation.rows.each { |r| r.unstub(:copy) }
      end
      it 'must set representation row with representation passed as parameter' do
        @representation.rows.wont_be_empty
        copied_rows = @representation.send :copy_rows, @copy_representation
        copied_rows.each { |r| r.representation.must_equal @copy_representation }
      end
    end

    describe '#copy_client_application_assets_base_representations' do
      before do
        @representation = create :representation,
          client_application_assets_base_representations: (1..4).map { build(:client_application_assets_base_representation) },
          view: build(:view_with_placeholders, name: 'main view name'),
          default_row_view: build(:view, name: 'row view name', value: 'row view value'),
          default_column_view: build(:view, name: 'column view name', value: 'column view value'),
          error_view: build(:view, name: 'error view name', value: 'error view value')
        @copy_representation = create :representation,
          view: build(:view_with_placeholders, name: 'main view name'),
          default_row_view: build(:view, name: 'row view name', value: 'row view value'),
          default_column_view: build(:view, name: 'column view name', value: 'column view value'),
          error_view: build(:view, name: 'error view name', value: 'error view value')
      end
      it 'must call ClientApplicationAssetsBaseRepresentation#copy as many times as rows have present in the representation' do
        ra = mock.responds_like ClientApplicationAssetsBaseRepresentation.new
        @representation.client_application_assets_base_representations.wont_be_empty
        @representation.client_application_assets_base_representations.each { |r| r.expects(:copy).returns(ra).once }
        @representation.send :copy_client_application_assets_base_representations, @copy_representation
      end
    end

    describe '#copy' do
      describe 'all but rows' do
        let(:configuration) { Hash[a:1, b:2, c:3] }
        let(:representation) do
          create :representation,
            configuration: configuration,
            view: build(:view_with_placeholders, name: 'main view name'),
            default_row_view: build(:view, name: 'row view name', value: 'row view value'),
            default_column_view: build(:view, name: 'column view name', value: 'column view value'),
            error_view: build(:view, name: 'error view name', value: 'error view value'),
            favicon: build(:client_application_asset),
            views: [build(:view, format: 'application/rss+xml'), build(:view, format: 'application/json')]
        end
        let(:copy) { representation.copy }

        it 'must return a new record with the same associations' do
          # it must return a new instance with name changed to I18n representation.copy.prefix
          copy.must_be :new_record?
          copy.name.must_equal I18n.t('activerecord.attributes.representation.copy.prefix', default: 'Copy of %{name}', name: representation.name)
          # it must preserve same client_application
          copy.client_application.must_equal representation.client_application
          copy.client_application.must_be :persisted?
          # it must copy the configuration
          copy.configuration.must_equal configuration
          # it must copy the main view
          copy.view.must_be :new_record?
          copy.view.name.must_equal representation.view.name
          copy.view.value.must_equal representation.view.value
          # it must copy the default row view
          copy.default_row_view.must_be :new_record?
          copy.default_row_view.value.must_equal representation.default_row_view.value
          # it must copy the default column view
          copy.default_column_view.must_be :new_record?
          copy.default_column_view.value.must_equal representation.default_column_view.value
          # it must copy the error view
          copy.error_view.must_be :new_record?
          copy.error_view.value.must_equal representation.error_view.value
          # it must copy the favicon
          copy.favicon.must_be :persisted?
          copy.favicon.must_equal representation.favicon
          # it must copy the views
          copy.views.wont_be_empty
          copy.views.must_equal representation.views
        end

        describe 'when specifying a name' do
          it 'must return a new instance with specified name' do
            rep = Representation.new name: 'some name'
            rep.copy('other name').name.must_equal 'other name'
          end
        end

        describe 'copy assets' do
          let(:representation_with_assets) { create(:representation_with_assets) }

          it 'must copy same assets but be different from the original' do
            copy = representation_with_assets.copy
            copy.client_application_assets_base_representations.map(&:client_application_asset_id).sort.must_equal representation_with_assets.client_application_assets_base_representations.map(&:client_application_asset_id).sort
            copy.save!
            copy.reload
            copy.assets.must_equal representation_with_assets.assets
            copy.assets.destroy(copy.assets.first)
            copy.reload
            representation_with_assets.reload
            representation_with_assets.assets.count.must_equal copy.assets.count + 1
          end
        end
      end

      describe 'row copy' do
        before do
          @representation = create :representation,
            view: build(:view_with_placeholders, name: 'main view name'),
            default_row_view: build(:view, name: 'row view name', value: 'row view value'),
            default_column_view: build(:view, name: 'column view name', value: 'column view value'),
            error_view: build(:view, name: 'error view name', value: 'error view value')
          # Create 3 rows:
          #   1st with 1 column with 1 component_configuration and 1 cover_component_configuration each
          #   2nd with 2 columns with 2 component_configurations and 2 cover_component_configurations each
          #   3rd with 3 columns with 3 component_configurations and 3 cover_component_configurations each
          1.upto(3) do |i|
            @representation.rows << (row = build(:row, representation: @representation, view: create(:view_with_placeholders)))
            row.columns = build_list :column_with_comp_configs, i, row: row, cc_counts: i, cover_cc_counts: i
          end
          @representation.save!
          @representation.reload
          @copy = @representation.copy
        end

        it 'must copy rows as new_records with all chain (columns, component_configurations, etc) copied' do
          @copy.rows.select(&:persisted?).must_be_empty
          @copy.rows.length.must_equal @representation.rows.count

          #copy.columns is not accesible since copy is not persisted yet
          columns = @copy.rows.map(&:columns).flatten
          columns.select(&:persisted?).must_be_empty
          columns.length.must_equal @representation.columns.count

          #copy.component_configurations is not accesible since copy is not persisted yet
          component_configurations = columns.map(&:component_configurations).flatten
          component_configurations.select(&:persisted?).must_be_empty
          component_configurations.length.must_equal @representation.component_configurations.count

          #copy.component_configurations is not accesible since copy is not persisted yet
          cover_component_configurations = component_configurations.select(&:cover?)
          cover_component_configurations.select(&:persisted?).must_be_empty
          cover_component_configurations.length.wont_equal 0
          cover_component_configurations.length.must_equal @representation.cover_component_configurations.count

          #copy.component_configurations is not accesible since copy is not persisted yet
          components = component_configurations.map(&:component_id).sort
          components.must_equal @representation.components.map(&:id).sort

          @copy.must_be :save
        end
      end
    end

    describe '#can_have_view?' do
      let(:client_app) { create :client_application }
      let(:rss_view) { create :view_with_placeholders, format: 'application/rss+xml' }
      let(:another_rss_view) { create :view_with_placeholders, format: 'application/rss+xml' }
      let(:atom_view) { create :view_with_placeholders, format: 'application/atom+xml' }
      let(:representation_with_rss) { create :representation, client_application: client_app, views: [rss_view] }
      let(:representation_with_atom) { create :representation, client_application: client_app, views: [atom_view] }
      let(:representation_without_views) { create :representation, client_application: client_app }

      it 'checks if the representation does not already have a view with the format of the passed in view (note this method looks only at persisted data)' do
        representation_with_rss.can_have_view?(rss_view).must_equal false
        representation_with_rss.can_have_view?(another_rss_view).must_equal false
        representation_with_rss.can_have_view?(atom_view).must_equal true
        representation_with_atom.can_have_view?(rss_view).must_equal true
        representation_with_atom.can_have_view?(another_rss_view).must_equal true
        representation_with_atom.can_have_view?(atom_view).must_equal false
        representation_without_views.can_have_view?(rss_view).must_equal true
        representation_without_views.can_have_view?(another_rss_view).must_equal true
        representation_without_views.can_have_view?(atom_view).must_equal true
      end
    end

    describe '#has_view?' do
      let(:client_app) { create :client_application }
      let(:view) { create :view_with_placeholders, format: 'application/rss+xml' }
      let(:associated_representation) { build :representation, views: [view], client_application: client_app }
      let(:non_associated_representation) { build :representation, client_application: client_app }

      it 'checks if the representation has the passed in view' do
        associated_representation.has_view?(view).must_equal true
        non_associated_representation.has_view?(view).must_equal false
      end
    end

    describe 'Rendering' do
      it 'must include Representations::Rendering concern' do
        BaseRepresentation.included_modules.must_include Representations::Rendering
      end
    end

    describe 'FromJson' do
      it 'must include Representations::FromJson concern' do
        BaseRepresentation.included_modules.must_include Representations::FromJson
      end
    end

    describe 'CoverFromJson' do
      it 'must include Representations::CoverFromJson concern' do
        BaseRepresentation.included_modules.must_include Representations::CoverFromJson
      end
    end

    describe 'Validations' do
      describe 'name uniqueness' do
        describe 'when the same name is in use for the same client application' do
          let(:name) { 'Representation name' }
          let(:client_application) { create :client_application }
          let(:existing_representation) { build :representation, name: name, client_application: client_application }
          let(:new_representation) { build :representation, name: name, client_application: client_application }

          before do
            existing_representation.save
          end

          it 'fails to validate' do
            new_representation.wont_be :valid?
            new_representation.errors[:name].must_include I18n.t('errors.messages.taken')
          end
        end

        describe 'when the same name is in use for different client applications' do
          let(:name) { 'Representation name' }
          let(:client_application_1) { create :client_application }
          let(:client_application_2) { create :client_application }
          let(:existing_representation) { build :representation, name: name, client_application: client_application_1 }
          let(:new_representation) { build :representation, name: name, client_application: client_application_2 }

          before do
            existing_representation.save
          end

          it 'succeeds' do
            new_representation.must_be :valid?
          end
        end
      end

      describe 'View presence' do
        let(:client_application)   { build :client_application }

        describe 'when view is present' do
          let(:view) { build :view_with_placeholders }
          let(:representation) { build :representation, view: view, client_application: client_application }

          it 'succeeds' do
            representation.must_be :valid?
          end
        end

        describe 'when view is not present' do
          let(:representation) { build :representation, view: nil, client_application: client_application }

          it 'uses the default view and validates its presence' do
            representation.must_be :valid?
          end
        end
      end

      describe 'View content placeholder validation' do
        describe 'when view is present' do
          let(:blank_view)   { build :view}
          let(:content_view) { build :view_with_placeholders}
          let(:client_application)   { build :client_application }
          let(:representation_blank_view)   { build :representation, client_application: client_application, view: blank_view }
          let(:representation_content_view) { build :representation, client_application: client_application, view: content_view }
          let(:representation_non_set_view) { create :representation, client_application: client_application  }

          it 'validates if view has a content placeholder' do
            representation_blank_view.wont_be :valid?
            representation_blank_view.errors.must_include :view
            representation_content_view.must_be :valid?
            representation_non_set_view.must_be :valid?
          end
        end
      end

      describe 'Alternative views validation' do
        let(:rss_view_1) { create :view_with_placeholders, format: 'application/rss+xml' }
        let(:rss_view_2) { create :view_with_placeholders, format: 'application/rss+xml' }
        let(:client_app) { build :client_application }
        let(:representation_with_repetitions) { build :representation, client_application: client_app }
        let(:representation_without_repetitions) { build :representation, client_application: client_app }
        let(:representation_without_alternative_views) { build :representation, client_application: client_app, views: [] }

        before do
          representation_with_repetitions.views << rss_view_1
          representation_with_repetitions.views << rss_view_2
          representation_without_repetitions.views << rss_view_2
        end

        it 'rejects repeated formats in the alternative views' do
          representation_with_repetitions.wont_be :valid?
          representation_with_repetitions.errors.must_include :views
          representation_without_repetitions.must_be :valid?
          representation_without_alternative_views.must_be :valid?
        end
      end
    end

    describe 'Callbacks' do
      describe 'Before validating' do
        describe 'views configuration' do
          let(:view) { create :view_with_placeholders }

          describe 'when no views are set' do
            let(:representation) { create :representation }

            it 'sets default ones for the representation' do
              representation.view.must_be_instance_of View
              representation.default_row_view.must_be_instance_of View
              representation.default_column_view.must_be_instance_of View
              representation.error_view.must_be_instance_of View
            end
          end

          describe 'when a representation view is set' do
            let(:representation) { create :representation, view: view }

            it 'keeps it' do
              representation.view.must_equal view
            end
          end

          describe 'when a default row view is set' do
            let(:representation) { create :representation, default_row_view: view }

            it 'keeps it' do
              representation.default_row_view.must_equal view
            end
          end

          describe 'when a default column view is set' do
            let(:representation) { create :representation, default_column_view: view }

            it 'keeps it' do
              representation.default_column_view.must_equal view
            end
          end

          describe 'when an error view is set' do
            let(:representation) { create :representation, error_view: view }

            it 'keeps it' do
              representation.error_view.must_equal view
            end
          end
        end
      end

      describe 'before save' do
        it 'must update asset filenames when name changes' do
          representation = create :representation_with_assets, name: 'my_name'
          old_js  = representation.send :main_javascript_file
          old_css = representation.send :main_stylesheet_file
          File.basename(old_js).must_equal 'my_name.js'
          File.basename(old_css).must_equal 'my_name.css'
          File.must_be :exists?,  old_js
          File.must_be :exists?,  old_css

          representation.name = 'other_name'
          representation.save!

          js  = representation.send :main_javascript_file
          css = representation.send :main_stylesheet_file
          File.basename(js).must_equal 'other_name.js'
          File.basename(css).must_equal 'other_name.css'
          File.wont_be :exists?,  old_js
          File.wont_be :exists?,  old_css
          File.must_be :exists?,  js
          File.must_be :exists?,  css
        end
      end

      describe 'after destroy' do
        it 'must delete asset filenames when destroyed' do
          representation = create :representation_with_assets, name: 'my_name'
          js  = representation.send :main_javascript_file
          css = representation.send :main_stylesheet_file
          File.basename(js).must_equal 'my_name.js'
          File.basename(css).must_equal 'my_name.css'
          File.must_be :exists?,  js
          File.must_be :exists?,  css

          representation.destroy

          File.wont_be :exists?,  js
          File.wont_be :exists?,  css
        end
      end
    end

    describe 'Deletion' do
      describe 'when it has rows' do
        let(:representation) { create :representation }
        let(:row_1) { build :row, order: 1 }
        let(:row_2) { build :row, order: 2 }
        let(:rows) { [row_1, row_2] }

        before do
          representation.rows = rows
        end

        it 'deletes the associated rows' do
          representation.rows.must_equal rows
          representation.destroy
          representation.must_be :destroyed?
          rows.each { |r| r.must_be :destroyed? }
        end
      end

      describe 'when it is in use by any route' do
        let(:client_application) { create :client_application }
        let(:representation) { create :representation, client_application: client_application }
        let(:route) { create :homepage_route, representation: representation, client_application: client_application }

        it "can't be deleted" do
          route.representation.must_equal representation
          representation.destroy
          representation.wont_be :destroyed?
        end
      end

      describe 'regarding its associated views' do
        let(:representation) { create :representation }
        let(:view) { representation.view }
        let(:error_view) { representation.error_view }
        let(:row_view) { representation.default_row_view }
        let(:column_view) { representation.default_column_view }
        let(:views) { [view, error_view, row_view, column_view] }

        it 'deletes all the associated views' do
          views.each { |v| v.wont_be_nil }
          representation.destroy
          representation.must_be :destroyed?
          views.each { |v| v.must_be :destroyed? }
        end
      end

      describe 'regarding the related client application' do
        let(:client_application) { create :client_application }
        let(:representation) { create :representation, client_application: client_application }

        it 'should not be deleted' do
          representation.destroy
          representation.must_be :destroyed?
          client_application.wont_be :destroyed?
        end
      end

      describe 'regarding the whole representation structure' do
        let(:representation) { create :representation }
        let(:row) { build :row, representation: representation }
        let(:column) { build :column, row: row }
        let(:component_configuration) { build :component_configuration, component: component }
        let(:component) { build :component }

        before do
          representation.rows << row
          row.columns << column
          column.component_configurations << component_configuration
        end

        it 'should delete it from end to end, except for the component' do
          representation.destroy
          representation.must_be :destroyed?
          row.must_be :destroyed?
          column.must_be :destroyed?
          component_configuration.must_be :destroyed?
          component.wont_be :destroyed?
        end
      end
    end
  end
end
