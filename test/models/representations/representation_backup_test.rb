require 'test_helper'

class RepresentationBackupTest < ActiveSupport::TestCase
  describe RepresentationBackup do
    describe '#restore!' do
      let(:representation) { create :representation }
      let(:backup) { create :representation_backup, representation: representation }

      before do
        representation.expects(:restore_backup!).with(backup).once
      end

      it 'restores onto the representation using #restore_backup!' do
        backup.restore!
      end
    end

    describe 'Validations' do
      describe 'representation presence' do
        describe 'when a backup is built without a representation' do
          let(:backup) { build :representation_backup, representation: nil }

          it 'fails to validate' do
            backup.wont_be :valid?
            backup.errors[:representation].must_include I18n.t('errors.messages.blank')
          end
        end

        describe 'when a backup is built for a representation' do
          let(:representation) { build :representation }
          let(:backup) { build :representation_backup, representation: representation }

          it 'succeeds' do
            backup.must_be :valid?
          end
        end
      end
    end

    describe 'scopes' do
      describe 'default' do
        before do
          @backup_1 = create :representation_backup, created_at: 3.days.ago
          @backup_2 = create :representation_backup, created_at: 10.days.ago
          @backup_3 = create :representation_backup, created_at: Date.current
        end

        it 'returns backups ordered from newer to older' do
          RepresentationBackup.all.must_equal [@backup_3, @backup_1, @backup_2]
        end
      end
    end
  end
end