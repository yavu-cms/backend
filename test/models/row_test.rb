require 'test_helper'

class RowTest < ActiveSupport::TestCase
  describe Row do
    describe '#copy' do
      let(:order) { 11 }
      let(:configuration) { Hash[a: 1, b: 4] }
      let(:columns_count) { 10 }
      let(:columns) { build_list :column, columns_count }
      let(:row) do
        create :row,
          representation: create(:representation),
          view: create(:view_with_placeholders),
          order: order,
          configuration: configuration,
          columns: columns
      end
      let(:copy) { row.copy }

      it 'must be a new_record'  do
        row.wont_be :new_record?
        copy.must_be :new_record?
      end

      it 'must set representation to nil' do
        row.representation.wont_be_nil
        row.representation_id.wont_be_nil
        copy.representation.must_be_nil
        copy.representation_id.must_be_nil
      end

      it 'must copy order' do
        copy.order.must_equal order
      end

      it 'must copy configuration' do
        copy.configuration.must_equal configuration
      end

      it 'must copy view' do
        row.view.wont_be :new_record?
        copy.view.must_be :new_record?
        copy.view.name.must_equal row.view.name
        copy.view.value.must_equal row.view.value
      end

      it 'must copy each column' do
        row.columns.count.must_equal columns_count
        copy.columns.length.must_equal columns_count
        copy.columns.select(&:persisted?).must_be_empty
        copy.columns.select do |x|
          x.row.nil? ||
          x.row != copy
        end.must_be_empty
      end
    end

    describe 'Validations' do
      describe 'representation' do
        it 'checks presence' do
          row_wrong = build :row, representation: nil
          row_wrong.wont_be :valid?
          row_wrong.errors.must_include :representation
        end
      end
      describe 'order' do
        it 'checks presence' do
          row_wrong = build :row, order: nil
          row_wrong.wont_be :valid?
          row_wrong.errors.must_include :order
        end
      end
    end

    describe 'Implemented hot spots from RenderableWithChildren' do
      describe '#default_view' do
        after do
          Representation.any_instance.unstub(:default_row_view)
        end

        it 'must call to default row from representation' do
          Representation.any_instance.expects(:default_row_view).twice
          build(:app1_representation1_row1).default_view
        end
      end

      describe '#children' do
        it 'must return component configurations' do
          row = build :app1_representation1_row1, columns: [ build(:column), build(:column) ]
          row.children.each { |column| column.must_be_instance_of Column }
        end
      end

      describe '#children=' do
        it 'must set columns' do
          row = build :app1_representation1_row1
          column = build :column
          row.children = [column]
          row.columns.must_equal [column]
        end
      end

      describe 'parent' do
        it 'must return a row' do
          row = build :app1_representation1_row1
          row.parent.must_be_instance_of Representation
        end
      end
      describe 'parent=' do
        it 'must set a row' do
          row = build :app1_representation1_row1
          new_rep = build :representation
          row.parent = new_rep
          row.representation.must_equal new_rep
        end
      end
    end

    describe 'Callbacks' do
        it 'must delete related view' do
          row = create :row, view: FactoryGirl.build(:view_with_placeholders)
          view_id = row.view.id
          row.destroy
          View.find_by(id: view_id).must_be_nil
        end
    end
  end
end