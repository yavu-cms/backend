require 'test_helper'

class ServiceContextPorcessorTest < ActiveSupport::TestCase
  describe ServiceContextProcessor do
    describe '.logger' do
      it 'must returns an instance of Logger'
    end

    describe '#initialize' do
      it 'must fail without a component_service'
      it 'must fail without a component_service with no component_configuration'
    end

    describe '#build' do
      describe 'as_api_resource' do
        it 'must call as_api_resource'
      end

      it 'must return a API::Resource::ProcessedContext object'

      describe 'with any parameter' do
        it 'must add each request parameter when service allows it as value for Context::Processed'

        it 'wont add new parameters when service dont allows it'
      end

      it 'must set @user if there is a header with user'

      it 'must return at component context every instance variable used inside ComponentService#body'

      it 'must return at component context every instance variable setted inside Component#extras called from ComponentService#body'

      it 'must set Context::Processed#errors? when ComponentService#body fails and tell exact line error'

      it 'must set Context::Processed#errors? when Component#extras fails and tell exact line error'
    end
  end
end
