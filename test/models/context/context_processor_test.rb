require 'test_helper'

class ContextProcessorTest < ActiveSupport::TestCase
  describe ContextProcessor do
    # Helper method to ensure all objects (articles, sections and routes)
    # be found in the same news source domain
    def ensure_same_news_source_scope(*models)
      news_source = create :news_source, name: 'The some NS domain'
      models.each do |model|
        case model
        when Article
          model.section.news_source = news_source
        when Section
          model.news_source = news_source
        when Route
          model.client_application.news_source = news_source
        end
      end
    end

    describe '.logger' do
      it 'must returns an instance of Logger' do
        ContextProcessor.logger.must_be_instance_of Logger
      end
    end

    describe '#initialize' do
      it 'must fail without a route' do
        -> { ContextProcessor.new }.must_raise ArgumentError
      end
    end

    describe '#build' do
      let(:context_processor) {  ContextProcessor.new(build(:route)) }

      describe 'as_api_resource' do
        before do
          Yavu::API::Resource::PreprocessedContext.any_instance.expects(:stop_processing).once
        end

        after do
          Yavu::API::Resource::PreprocessedContext.any_instance.unstub(:stop_processing)
        end

        it 'must call as_api_resource' do
          context_processor.build
        end
      end

      it 'must return a API::Resource::ProcessedContext object' do
        context_processor.build.must_be_instance_of Yavu::API::Resource::ProcessedContext
      end

      describe 'with any route do' do
        let(:route) {  build(:route) }
        let(:context_processor) {  ContextProcessor.new(route) }

        it 'must add each request parameter when route allows it as value for Context::Processed'  do
          route.stubs(:available_parameters).returns(%w(some_param other))
          processed = context_processor.build(some_param: 'some value', other: 10)
          processed[:some_param ].must_equal 'some value'
          processed[:other ].must_equal 10
        end

        it 'wont add new parameters when route dont allows it' do
          route.stubs(:available_parameters).returns(%w(some_param))
          processed = context_processor.build(some_param: 'some value', other: 10)
          processed[:some_param ].must_equal 'some value'
          processed[:other ].must_be_nil
        end

        it 'must set instance variable for services inside Component#context with any component service objects'
        describe 'processed context route field' do
          it 'must set instance variable for route inside Component#context see route#build_context'
          it 'must return a processed_context instance without route api resource see route#build_context'
        end

        it 'must set @user if there is a header with user'
      end

      describe 'with article route' do
        let(:article) {  create(:article) }
        let(:article_route) { build(:article_route) }
        let(:context_processor) {  ContextProcessor.new(article_route) }

        it 'must set :article value to API::Resource::Article' do
          ensure_same_news_source_scope(article, article_route)
          processed = context_processor.build(article: article.slug)
          processed.shared_refs[processed[:article]].must_be_instance_of Yavu::API::Resource::Article
          processed.shared_refs[processed[:article]].id.must_equal article.slug
        end

        it 'must raise RecordNotFound when parameter does not exists' do
          ensure_same_news_source_scope(article, article_route)
          -> { context_processor.build(article: 'it does not exists') }.must_raise ActiveRecord::RecordNotFound
        end

        it 'when route has constraints it will have priority over request parameter' do
          article_constrained = create(:article, title: 'fixed')
          article             = create(:article, title: 'some title')
          article_route       = build(:article_route)
          article_route.constrained_article = article_constrained.slug
          ensure_same_news_source_scope(article, article_constrained, article_route)
          context_processor   =  ContextProcessor.new(article_route)
          processed = context_processor.build(article: article.slug)
          processed.shared_refs[processed[:article]].id.must_equal article_constrained.slug
        end
      end

      describe 'with section route' do
        let(:section) {  create(:section) }
        let(:section_route) { build(:section_route) }
        let(:context_processor) {  ContextProcessor.new(section_route) }

        it 'must set :section value to API::Resource::Article' do
          ensure_same_news_source_scope(section, section_route)
          processed = context_processor.build(section: section.slug)
          processed.shared_refs[processed[:section]].must_be_instance_of Yavu::API::Resource::Section
          processed.shared_refs[processed[:section]].id.must_equal section.slug
        end

        it 'must raise RecordNotFound when parameter does not exists' do
          -> { context_processor.build(section: 'it does not exists') }.must_raise ActiveRecord::RecordNotFound
        end

        it 'when route has constraints it will have priority over request parameter' do
          section_constrained = create(:section, name: 'fixed')
          section             = create(:section, name: 'some name')
          section_route = build(:section_route)
          ensure_same_news_source_scope(section, section_constrained, section_route)
          section_route.constrained_section = section_constrained.slug
          context_processor   =  ContextProcessor.new(section_route)
          processed = context_processor.build(section: section.slug)
          processed.shared_refs[processed[:section]].id.must_equal section_constrained.slug
        end
      end

      describe 'route representation' do
        describe 'with no components' do

          it 'must return an API::Resource::ProcessedContext object without components' do
            route = create(:article_route)
            article = create(:article)
            ensure_same_news_source_scope(article, route)
            route.representation.components.must_be_empty
            context_processor = ContextProcessor.new(route)
            context_processor.build(article: article.slug).components.must_be_empty
          end

        end

        describe 'with components' do
          it 'must return at component context every configured value inside settings' do
            defaults = {a: 1, b: 10, c: 100}
            fields = defaults.map { |(k,v)| build(:component_field_integer, name: k, default: v) }
            component = create(:component)
            component.fields = fields.each { |f| f.component = component }

            route = create_route_with_component(route: :article_route, component: component)
            article = create(:article)
            ensure_same_news_source_scope(article, route)

            context_processor = ContextProcessor.new(route)
            processed = context_processor.build(article: article.slug)

            component_configuration = route.representation.component_configurations.first

            processed.components[component_configuration.id].wont_be :errors?
            settings_key = processed.components[component_configuration.id][:settings]
            defaults.each do |k, v|
              processed.shared_refs[settings_key][k].must_equal v
            end
          end

          it 'must return at component context every instance variable used inside Component#context' do
            context = <<-CONTEXT
            @one = 1
            @two = 2
            @ten = 10
            CONTEXT
            component = create(:component, context: context)

            route = create_route_with_component(route: :article_route, component: component)
            article = create(:article)
            ensure_same_news_source_scope(article, route)

            context_processor = ContextProcessor.new(route)
            processed = context_processor.build(article: article.slug)

            component_configuration = route.representation.component_configurations.first

            processed.components[component_configuration.id].wont_be :errors?
            processed.components[component_configuration.id][:one].must_equal 1
            processed.components[component_configuration.id][:two].must_equal 2
            processed.components[component_configuration.id][:ten].must_equal 10

          end
          it 'must return at component context every instance variable setted inside Component#extras called from Component#context' do
            context = <<-CONTEXT
            call_extras
            CONTEXT

            extras = <<-EXTRAS
            def call_extras
              @an_extra_value = 'some value'
            end
            EXTRAS

            component = create(:component, context: context, extras: extras)

            route = create_route_with_component(route: :article_route, component: component)
            article = create(:article)
            ensure_same_news_source_scope(article, route)

            context_processor = ContextProcessor.new(route)
            processed = context_processor.build(article: article.slug)

            component_configuration = route.representation.component_configurations.first

            processed.components[component_configuration.id].wont_be :errors?
            processed.components[component_configuration.id][:an_extra_value].must_equal 'some value'
          end

          it 'must update API::Resource::Article global value if component updates object' do
            context = <<-CONTEXT
              @article.title = 'a new title'
            CONTEXT

            component = create(:component, context: context)
            route = create_route_with_component(route: :article_route, component: component)
            article = create(:article)
            ensure_same_news_source_scope(article, route)

            context_processor = ContextProcessor.new(route)
            processed = context_processor.build(article: article.slug)

            component_configuration = route.representation.component_configurations.first

            article_key = processed.components[component_configuration.id][:article]
            article_in_context = processed.shared_refs[article_key]
            article_in_context.title.must_equal 'a new title'
            processed.components[component_configuration.id].wont_be :errors?
          end

          it 'must set Context::Processed#errors? when Component#context fails and tell exact line error' do
            #The line 1=2 is an error
            context = <<-CONTEXT
            @article.new_field = 'testing'
            1=2
            @b = 'some value'
            CONTEXT

            component = create(:component, context: context)
            route = create_route_with_component(route: :article_route, component: component)
            article = create(:article)

            ensure_same_news_source_scope(article, route)
            context_processor = ContextProcessor.new(route)
            processed = context_processor.build(article: article.slug)

            component_configuration = route.representation.component_configurations.first

            processed.components[component_configuration.id].must_be :errors?
            processed.components[component_configuration.id].errors.first.must_match(/^Component\#context\[#{component_configuration.component.name}\]:2/)
          end

          it 'must set Context::Processed#errors? when Component#extras fails and tell exact line error' do
            context = <<-CONTEXT
            call_extras
            CONTEXT

            extras = <<-EXTRAS
            def call_extras
              @an_extra_value = 'some value'
            end

            def @badname
            end
            EXTRAS

            component = create(:component, context: context, extras: extras)

            route = create_route_with_component(route: :article_route, component: component)
            article = create(:article)
            ensure_same_news_source_scope(article, route)

            context_processor = ContextProcessor.new(route)
            processed = context_processor.build(article: article.slug)

            component_configuration = route.representation.component_configurations.first

            processed.components[component_configuration.id].must_be :errors?
            processed.components[component_configuration.id].errors.first.must_match(/^Component\#extras\[#{component_configuration.component.name}\]:5/)
          end

          it 'must preset as instance variables all attributes inherited from Context::Processed merged with Component#configurations' do

            context = <<-CONTEXT
            raise 'not setted one' unless @one == 1
            raise 'not setted two' unless @two == 2
            raise 'not setted ten' unless @ten == 10
            CONTEXT

            component = create(:component, context: context)

            route = create_route_with_component(route: :article_route, component: component)
            route.stubs(:available_parameters).returns(%w(article one two ten))

            context_processor = ContextProcessor.new(route)
            article = create(:article)
            ensure_same_news_source_scope(article, route)
            processed = context_processor.build(article: article.slug, one: 1, two: 2, ten: 10)

            component_configuration = route.representation.component_configurations.first

            processed.components[component_configuration.id].wont_be :errors?
          end


          it 'must consider cover? component_configurations' do
            first_values = {a:1, b:2}
            last_values  = {c:3, d:4}
            component_configuration = build(:cover_component_configuration,cover_articles_count: 2)
            first = component_configuration.cover_articles_component_configurations.first
            last = component_configuration.cover_articles_component_configurations.last
            first.values = first_values
            last.values  = last_values
            representation = build(:representation_with_component, component_configuration: component_configuration)
            route = create(:homepage_route, representation: representation)

            BaseComponentConfiguration.subclasses.each do |sc|
              sc.any_instance.stubs(:available_attributes).
                returns(first_values).
                then.returns(last_values)
            end

            context_processor = ContextProcessor.new(route)
            processed = context_processor.build

            component_configuration = route.component_configurations.first
            first = component_configuration.cover_articles_component_configurations.first
            last = component_configuration.cover_articles_component_configurations.last

            processed.components[component_configuration.id].wont_be :errors?
            settings_key_for_first = processed.components[component_configuration.id].components[first.id][:settings]
            settings_key_for_last = processed.components[component_configuration.id].components[last.id][:settings]
            processed.shared_refs[settings_key_for_first].must_equal first_values.with_indifferent_access
            processed.shared_refs[settings_key_for_last].must_equal last_values.with_indifferent_access
          end
        end
      end
    end
  end
end
