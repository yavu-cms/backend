require 'test_helper'

class MediumMediaGalleryTest < ActiveSupport::TestCase
  describe MediumMediaGallery do
    describe 'scopes' do
      describe 'default' do
        before do
          gallery = MediaGallery.create(title: 'Test gallery')
          medium_1  = create :image_medium
          medium_2  = create :image_medium, file: upload_test_file('image_2.jpg')
          @mmg_1 = MediumMediaGallery.create(media_gallery: gallery, medium: medium_2, order: 1)
          @mmg_2 = MediumMediaGallery.create(media_gallery: gallery, medium: medium_1, order: 0)
        end

        it 'returns backups ordered from lower order to higher order' do
          MediumMediaGallery.all.must_equal [@mmg_2, @mmg_1]
        end
      end
    end
  end
end