require 'test_helper'

class ComponentAssetUploaderTest < ActiveSupport::TestCase
  describe ComponentAssetUploader do
    before do
      image_asset = create :component_asset
      Component.any_instance.stubs(:slug).returns('component_slug')
      @asset_uploader = ComponentAssetUploader.new(image_asset)
    end

    after do
      Component.any_instance.unstub(:slug)
    end

    describe '.prefix_for_type' do
      it 'must return type pluralized and parameterized' do
        ComponentAssetUploader.prefix_for_type('some type').must_equal 'some_types'
      end
    end

    describe '.absolute_prefix_for_type' do
      after do
        ComponentAssetUploader.unstub(:root)
      end

      it 'must return root joined prefix_for_type class method' do
        ComponentAssetUploader.expects(:root).returns('some_dir/with/subdir').once
        ComponentAssetUploader.absolute_prefix_for_type('some type').must_equal 'some_dir/with/subdir/some_types'
      end
    end

    describe '#store_dir_prefix' do
      it 'must return asset type pluralized' do
        @asset_uploader.store_dir_prefix.must_equal 'images'
      end
    end

    describe '#store_dir_suffix' do
      it 'must return asset\'s associated component slug' do
        @asset_uploader.store_dir_suffix.must_equal 'component_slug'
      end
    end

    describe '#store dir' do
      it 'must return a valid dir' do
        @asset_uploader.store_dir.must_equal "images/component_slug"
      end
    end
  end
end