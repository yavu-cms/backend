require 'test_helper'

class ImageUploaderTest < ActiveSupport::TestCase
  describe ImageUploader do
    let(:image_medium) { build :image_medium }
    let(:uploader) { image_medium.file }

    describe 'without processing' do
      before do
        ImageUploader.enable_processing = false
      end

      after do
        uploader.remove!
      end

      describe '#extension_white_list' do
        it 'must return a list with allowed image types' do
          uploader.extension_white_list.must_equal %w(jpg jpeg gif png svg)
        end
      end
    end

    describe 'with processing' do
      let(:image_medium) { create :image_medium }

      before do
        ImageUploader.enable_processing = true
      end

      after do
        ImageUploader.enable_processing = false
        uploader.remove!
      end

      describe '#store_dimensions' do
        let(:keys) { %i(xsmall small medium big xbig) }

        it "must store the dimensions for each version on the model's :dimensions attribute" do
          image_medium.dimensions.keys.count.must_equal keys.count
          keys.each do |key|
            image_medium.dimensions[key].must_be_instance_of Hash
            image_medium.dimensions[key].keys.must_equal %i(width height)
            image_medium.dimensions[key][:width].wont_be_nil
            image_medium.dimensions[key][:height].wont_be_nil
          end
        end

        it 'must persist the dimensions' do
          image_medium.reload
          image_medium.dimensions.keys.must_equal keys
        end
      end
    end
  end
end

