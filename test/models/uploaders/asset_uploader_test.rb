require 'test_helper'
require 'mocha/setup'

class AssetUploaderTest < ActiveSupport::TestCase
  describe AssetUploader do
    describe '.directory_for' do
      describe 'when type is valid' do
        it 'must return storage directory if key has a directory set' do
          AssetUploader.directory_for('stylesheet').must_equal 'stylesheets'
        end

        it 'must return storage directory if key doesn\t have a directory set' do
          AssetUploader.directory_for('javascript').must_equal 'javascripts'
        end
      end

      describe 'when type is invalid' do
        it 'must raise an exception if key doesn\t exist' do
          -> { AssetUploader.directory_for('not_a_key') }.must_raise ArgumentError
        end
      end
    end

    describe '#types' do
      before do
        # clear types cache
        AssetUploader.class_variable_set '@@types', nil
        YavuSettings.assets.types.merge!(configuration: {allowed_extensions: ['yml']})
      end

      it 'must return types set on YavuSettings' do
        AssetUploader.types.must_equal ["stylesheet", "javascript", "image", "font", "other", "configuration"]
        end

      it 'must return extensions set on YavuSettings' do
        AssetUploader.extension_white_list.sort.must_equal ["css", "scss", "sass", "js", "png", "ico", "jpg", "jpeg",
                                                            "gif", "otf", "woff", "ttf", "yml", "eot", "svg", "swf"].sort
      end
    end

    describe '#store_dir_prefix/suffix' do
      it 'should raise an exception' do
        @asset_uploader = AssetUploader.new()
        -> { @asset_uploader.store_dir_prefix }.must_raise NotImplementedError
        -> { @asset_uploader.store_dir_suffix }.must_raise NotImplementedError
      end
    end
  end
end
