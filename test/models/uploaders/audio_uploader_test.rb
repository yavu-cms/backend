require 'test_helper'

class AudioUploaderTest < ActiveSupport::TestCase
  describe AudioUploader do
    describe '#extension_white_list' do
      let(:stub_audio_extensions) { %w(ogg mp3 au wav mid) }

      before do
        stub_media = mock
        stub_allowed_extensions = mock
        YavuSettings.expects(:media).returns(stub_media).once
        stub_media.expects(:allowed_extensions).returns(stub_allowed_extensions).once
        stub_allowed_extensions.expects(:audio).returns(stub_audio_extensions).once
      end

      it 'uses YavuSettings to determine which extensions should be allowed' do
        AudioUploader.new.extension_white_list.must_equal stub_audio_extensions
      end
    end
  end
end