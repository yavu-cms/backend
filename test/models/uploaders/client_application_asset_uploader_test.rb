require 'test_helper'

class ClientApplicationAssetUploaderTest < ActiveSupport::TestCase
  describe ClientApplicationAssetUploader do
    before do
      image_asset = create :client_application_asset
      image_asset.stubs(:prefix_path).returns("prefix")
      ClientApplication.any_instance.stubs(:identifier).returns('app_id')
      @client_app_asset_uploader = ClientApplicationAssetUploader.new(image_asset)
    end

    after do
      ClientApplication.any_instance.unstub(:identifier)
    end

    describe '.prefix_for_type' do
      it 'must return client_application.identifier join assets  join type pluralized, all parameterized' do
        ClientApplicationAssetUploader.prefix_for_type(ClientApplication.new, 'some type').must_equal 'app_id/assets/some_types'
      end
    end

    describe '.absolute_prefix_for_type' do
      after do
        ClientApplicationAssetUploader.unstub(:prefix_for_type)
        ClientApplicationAssetUploader.unstub(:root)
      end

      it 'must return root join prefix_for_type class method' do
        client_application = ClientApplication.new
        type = 'some type'
        ClientApplicationAssetUploader.expects(:prefix_for_type).with(client_application, type).returns('app_id/assets/some_types').once
        ClientApplicationAssetUploader.expects(:root).returns('some_prefix/with/dirs')
        ClientApplicationAssetUploader.absolute_prefix_for_type(client_application, type).must_equal 'some_prefix/with/dirs/app_id/assets/some_types'
      end
    end

    describe '.prefix_for_representation_type' do
      it 'must return client application identifier join representations join type pluralized, all parameterized' do
        ClientApplicationAssetUploader.prefix_for_representation_type(ClientApplication.new, 'some type').must_equal 'app_id/representations/some_types'
      end
    end

    describe '.absolute_prefix_for_representation_type' do
      after do
        ClientApplicationAssetUploader.unstub(:prefix_for_representation_type)
        ClientApplicationAssetUploader.unstub(:root)
      end

      it 'must return root joined prefix_for_representation_type class method' do
        client_application = ClientApplication.new
        type = 'some type'
        ClientApplicationAssetUploader.expects(:prefix_for_representation_type).with(client_application, type, nil).returns('app_id/representations/some_types').once
        ClientApplicationAssetUploader.expects(:root).returns('some_prefix/with/dirs')
        ClientApplicationAssetUploader.absolute_prefix_for_representation_type(client_application, type).must_equal 'some_prefix/with/dirs/app_id/representations/some_types'
      end
    end

    describe '#store_dir_suffix' do
      it 'must return model\'s prefix path' do
        @client_app_asset_uploader.store_dir_suffix.must_equal "prefix"
      end
    end

    describe '#store_dir_prefix' do
      it 'must return path to this asset' do
        @client_app_asset_uploader.store_dir_prefix.must_equal 'app_id/assets/images'
      end
    end

    describe '#store_dir' do
      it 'must return full store path (prefix + suffix)' do
        @client_app_asset_uploader.store_dir.must_equal 'app_id/assets/images/prefix'
      end
    end

    describe '.javascript_representation_store_filename' do
      before do
        @home_representation = Representation.where(name: 'Home').first
        ClientApplicationAssetUploader.stubs(:representation_store_filename).with(@home_representation, 'javascript', 'js',false).returns(Rails.root.join(*%w{tmp tests client_application_assets app_id representations javascripts home.js}))
      end

      after do
        ClientApplicationAssetUploader.unstub(:representation_store_filename)
      end

      it 'must return full representation path name for javascript' do
        ClientApplicationAssetUploader.javascript_representation_store_filename(@home_representation).must_equal (Rails.root.join(*%w{tmp tests client_application_assets app_id representations javascripts home.js}))
      end
    end

    describe '.stylesheet_representation_store_filename' do
      before do
        @search_representation = Representation.where(name: 'Search').first
        ClientApplicationAssetUploader.stubs(:representation_store_filename).with(@search_representation, 'stylesheet', 'css', false).returns(Rails.root.join(*%w{tmp tests client_application_assets app_id representations stylesheets search.css}))
      end

      after do
        ClientApplicationAssetUploader.unstub(:representation_store_filename)
      end

      it 'must return full representation path name for stylesheet' do
        ClientApplicationAssetUploader.stylesheet_representation_store_filename(@search_representation).must_equal (Rails.root.join(*%w{tmp tests client_application_assets app_id representations stylesheets search.css}))
      end
    end
  end
end
