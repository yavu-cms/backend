require 'test_helper'

class BaseUploaderTest < ActiveSupport::TestCase
  describe BaseUploader do
    before do
      image_asset = create :component_asset
      ComponentAsset.any_instance.stubs(:id).returns(150)
      @base_uploader = BaseUploader.new(image_asset)
    end

    after do
      ComponentAsset.any_instance.unstub(:id)
    end

    describe '#store_dir' do
      it 'must return a valid path' do
        @base_uploader.store_dir.must_equal "50/150"
      end
    end
  end
end
