require 'test_helper'

class ClientApplicationTest < ActiveSupport::TestCase
  describe ClientApplication do
    describe '.search' do
      before do
        4.times do |i|
          create :client_application, name: "Client App #{i + 1}"
        end
      end

      describe 'when no query is provided' do
        it 'returns all the client applications' do
          ClientApplication.search({}).must_equal ClientApplication.all
        end
      end

      describe 'when a query is provided' do
        it 'only returns matching client applications' do
          results = ClientApplication.search({ query: '3' })
          results.count.must_equal 1
          results.first.name.must_equal 'Client App 3'
        end
      end
    end

    describe '.route_cache_control_default' do
      it 'must return expected value' do
        ClientApplication.route_cache_control_default.must_equal 'public, max-age=120, s-maxage=600'
      end
    end

    describe '.service_cache_control_default' do
      it 'must return expected value' do
        ClientApplication.service_cache_control_default.must_equal 'private, must-revalidate, max-age=0'
      end
    end

    describe '#calculated_route_cache_control' do
      it 'must return route_cache_control if setted' do
        build(:client_application, route_cache_control: 'a').calculated_route_cache_control.must_equal 'a'
      end

      it 'must return .route_cache_control_default when blank' do
        build(:client_application, route_cache_control: nil ).calculated_route_cache_control.must_equal ClientApplication.route_cache_control_default
      end
    end

    describe '#calculated_service_cache_control' do
      it 'must return service_cache_control if setted' do
        build(:client_application, service_cache_control: 'b').calculated_service_cache_control.must_equal 'b'
      end

      it 'must return .service_cache_control_default when blank' do
        build(:client_application, service_cache_control: nil ).calculated_service_cache_control.must_equal ClientApplication.service_cache_control_default
      end
    end

    describe '#assets_host' do
      it 'must be implemented'
    end

    describe '#can_have_logo?' do
      let(:client_application) { create :client_application }

      it 'must return true' do
        client_application.can_have_logo?.must_equal true
      end
    end

    describe '#to_s' do
      let(:client_application) { create :client_application }

      it 'returns the name of the application' do
        client_application.to_s.must_equal client_application.name
      end
    end

    describe 'manage varnish urls' do
      let(:client_application) { create :client_application }
      let(:varnish_urls)       { %w(first_url second_url) }
      let(:repeated_urls)      { %w(first_url second_url second_url) }

      describe '#internal_varnish_urls' do
        let(:client_application) { create :client_application }

        describe 'when no urls are present' do
          it 'returns an empty array' do
            client_application.internal_varnish_urls.must_equal []
          end
        end

        describe 'when urls are present' do
          before do
            client_application.internal_varnish_urls = varnish_urls
          end

          it 'returns an array of urls' do
            client_application.internal_varnish_urls.must_equal varnish_urls
          end
        end
      end

      describe '#internal_varnish_urls=' do
        it 'stores urls as a single string' do
          client_application.internal_varnish_urls = varnish_urls
          client_application.read_attribute(:internal_varnish_urls).must_equal "first_url,second_url"
        end

        it 'removes duplicate urls' do
          client_application.internal_varnish_urls = repeated_urls
          client_application.internal_varnish_urls.must_equal varnish_urls
        end
      end

      describe '#external_varnish_urls' do
        let(:client_application) { create :client_application }

        describe 'when no urls are present' do
          it 'returns an empty array' do
            client_application.external_varnish_urls.must_equal []
          end
        end

        describe 'when urls are present' do
          before do
            client_application.external_varnish_urls = varnish_urls
          end

          it 'returns an array of urls' do
            client_application.external_varnish_urls.must_equal varnish_urls
          end
        end
      end

      describe '#external_varnish_urls=' do
        it 'stores urls as a single string' do
          client_application.external_varnish_urls = varnish_urls
          client_application.read_attribute(:external_varnish_urls).must_equal "first_url,second_url"
        end

        it 'removes duplicate urls' do
          client_application.external_varnish_urls = repeated_urls
          client_application.external_varnish_urls.must_equal varnish_urls
        end
      end

    end

    describe '#assets_compile' do
      it 'must call sprocket manifest compile'
    end

    describe 'as_api_resource' do
      before do
        @client_application = create :client_application
        @resource = @client_application.as_api_resource
      end

      it 'must return expected attributes' do
        @resource.id.must_equal @client_application.identifier
        @resource.routes.must_equal @client_application.routes
        @resource.enabled.must_equal @client_application.enabled
        @resource.base_url.must_equal @client_application.url
        @resource.service_prefix.must_equal @client_application.service_prefix
        @resource.accounting_url.must_equal @client_application.accounting_url
        @resource.internal_media_url.must_equal BaseUploader.base_path
        @resource.external_media_url.must_equal @client_application.media_url
        @resource.assets_configuration.must_equal @client_application.assets_configuration
      end
    end

    describe '.assets_configuration' do
      it 'must be implemented'
    end

    describe '#assets_configuration' do
      let(:client_application) { create(:client_application) }
      let(:config) { client_application.assets_configuration }

      it 'must be an instance of BaseContainter' do
        config.must_be_instance_of Yavu::API::Resource::Base
      end

      it 'must preset all expected values' do
        keys = %w(root public_path prefix js_compressor css_compressor digest manifest_file append_paths)
        keys.each do |k|
          config[k].wont_be_nil
        end
        config['debug'].must_be_nil
      end

      it 'must set expected values' do
        config[:root].must_equal Rails.root.to_s
        config[:public_path].must_equal Rails.public_path.to_s
        config[:prefix].must_equal File.join(YavuSettings.assets.prefix, client_application.identifier).to_s
        config[:js_compressor].must_equal  YavuSettings.assets.js_compressor
        config[:css_compressor].must_equal YavuSettings.assets.css_compressor
        config[:digest].must_equal YavuSettings.assets.digest
        config[:manifest_file].must_equal File.join(config[:public_path], config[:prefix], 'manifest.json').to_s
        config[:append_paths].must_be_instance_of Array
      end

      it 'must be true if client application is in debug mode' do
        client_application.assets_configuration[:debug].must_be_nil
      end

      describe 'assets_append_paths' do
        before do
          AssetUploader.expects(:types).returns(['a', 'b'])
          ClientApplicationAssetUploader.expects(:absolute_prefix_for_type).with(client_application, 'a').returns('client/a').once
          ClientApplicationAssetUploader.expects(:absolute_prefix_for_type).with(client_application, 'b').returns('client/b').once
        end

        after do
          AssetUploader.unstub :types
          BaseRepresentation.unstub :kinds_for_assets
          ClientApplicationAssetUploader.unstub :absolute_prefix_for_representation_type, :absolute_prefix_for_type
        end

        describe 'when is for preview' do
          before do
            ClientApplicationAssetUploader.expects(:absolute_prefix_for_representation_type).with(client_application, 'a', 'drafts').returns("draft/a").once
            ClientApplicationAssetUploader.expects(:absolute_prefix_for_representation_type).with(client_application, 'b', 'drafts').returns("draft/b").once
          end

          it 'must append draft representations paths as configured' do
            client_application.assets_append_paths(for_preview: true).must_equal %w(draft/a client/a draft/b client/b)
          end
        end
        describe 'when is not for preview' do
          before do
            ClientApplicationAssetUploader.expects(:absolute_prefix_for_representation_type).with(client_application, 'a', 'representations').returns("rep/a").once
            ClientApplicationAssetUploader.expects(:absolute_prefix_for_representation_type).with(client_application, 'b', 'representations').returns("rep/b").once
          end

          it 'must append client real representations paths as configured' do
            client_application.assets_append_paths.must_equal %w(rep/a client/a rep/b client/b)
          end
        end
      end

      describe 'append_paths' do
        before do
          client_application.expects(:assets_append_paths).returns(%w(rep/a draft/a client/a rep/b draft/b client/b)).once
          BaseComponent.expects(:assets_append_paths).returns(%w(comp/a comp/b))
        end
        after do
          BaseComponent.unstub :assets_append_paths
        end
        it 'must append_paths as configured' do
          config[:append_paths].must_equal %w(comp/a comp/b rep/a draft/a client/a rep/b draft/b client/b)
        end
      end
    end

    describe '#homepage_route' do
      let(:client_application) { create :client_application_with_routes}

      it 'must return the first homepage route as the homepage default route' do
        client_application.homepage_route.must_be_instance_of HomepageRoute
      end
    end

    describe '#home_articles' do
      describe 'when is used in standalone fashion' do
        it 'must bring the articles set in the home' do
          skip
        end
      end
      describe 'when is used in standalone fashion' do
        it 'must bring the articles set in the home' do
          skip
        end
      end
    end

    describe '#cover_representations' do
      let(:cover_representation) { mock.responds_like_instance_of Representation }
      let(:regular_representation) { mock.responds_like_instance_of Representation }
      let(:client_application) { build :client_application }

      before do
        cover_representation.expects(:cover?).returns(true).once
        regular_representation.expects(:cover?).returns(false).once
        client_application.expects(:representations).
          returns([cover_representation, regular_representation]).once
      end

      it 'returns all the associated representations that are covers' do
        client_application.cover_representations.must_equal [cover_representation]
      end
    end

    describe '#for_preview' do
      let(:client_application) { create(:client_application).tap {|ca| ca.stubs(:updated_at).returns(3.days.ago)} }

      describe 'with representation' do
        let(:representation) { create :representation, client_application: client_application }

        before { @preview = client_application.for_preview representation }

        it 'returns a PreviewClientApplication with the data for this ClientApplication changing its updated_at timestamp' do
          @preview.must_be_instance_of PreviewClientApplication
          @preview.id.must_equal client_application.id
          @preview.updated_at.must_be :>, client_application.updated_at
          @preview.preview_representation.must_be_same_as representation
        end
      end

      describe 'without representation' do
        before { @preview = client_application.for_preview }

        it 'returns a PreviewClientApplication with the data for this ClientApplication changing its updated_at timestamp' do
          @preview.must_be_instance_of PreviewClientApplication
          @preview.id.must_equal client_application.id
          @preview.updated_at.must_be :>, client_application.updated_at
          @preview.preview_representation.must_be_nil
        end
      end
    end

    describe 'Validations' do

      it 'identifier must be unique'

      describe 'name' do
        it 'must be unique'
        it 'must be present'
      end

      describe 'url' do
        it 'must be unique'
        it 'must be present'
      end

    end

    describe 'Callbacks' do

      describe 'after creating' do
        describe 'default routes creation' do
          let(:client_application) { build :client_application_with_routes, routes: [] }

          it 'generates a default set of routes for the client application' do
            client_application.routes.must_be_empty
            client_application.save
            client_application.routes.wont_be_empty
            types = [ HomepageRoute, SectionRoute, ArticleRoute, SearchRoute, NewsletterRoute]
            Set.new(client_application.routes.map(&:class)).must_equal types.to_set
            types.each do |route_type|
              route = client_application.routes.detect {|x| x.is_a? route_type }
              route.wont_be_nil
              if route_type.requires_representation?
                route.representation.must_be_instance_of Representation
              end
            end
          end
        end

        describe 'identifier generation' do
          let(:client_application) { build :client_application, identifier: nil }

          it 'generates a new identifier for the client application' do
            client_application.identifier.must_be_nil
            client_application.save
            client_application.identifier.wont_be_nil
          end

          describe 'uniqueness of the generated identifier' do
            let(:another_client_application) { build :client_application, identifier: nil }

            before do
              client_application.save
              another_client_application.save
            end

            it 'generates different identifiers for different client applications' do
              client_application.identifier.wont_equal another_client_application
            end
          end
        end
      end
    end

    describe '#copy' do
      describe 'when the client application has a draft' do
        let(:client_application) { create :client_application }

        it "must be valid" do
          new_one = client_application.copy url: 'http://example.com'
          new_one.must_be :valid?
        end
      end
    end
  end
end
