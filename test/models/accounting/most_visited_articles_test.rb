require 'test_helper'
require 'mocha/setup'

module Accounting
  class MostVisitedArticlesTest < ActiveSupport::TestCase
    describe MostVisitedArticles do
      before do
        MostVisitedArticles.delete_all
      end

      describe '#valid?' do
        it 'fails when required attributes are not present' do
          most_visited = Accounting::MostVisitedArticles.new
          most_visited.wont_be :valid?
        end

        it 'succeds when required attributes are present' do
          most_visited = Accounting::MostVisitedArticles.new a: 1, d: DateTime.new
          most_visited.must_be :valid?
        end
      end

      describe '.initial_visits_for' do
        it 'calls .visits with is_base argument set to true'
      end

      describe '.set_initial_visits_for' do
        it 'calls .perform_visits with is_base argument set to true'
      end

      describe '.visit!' do
        before do
          mongoid_article = mock
          mongoid_article.expects(:update_attributes).once
          mongoid_article.expects(:inc).once
          Accounting::MostVisitedArticles.expects(:find_or_create_by).returns(mongoid_article).once
        end
        after do
          Accounting::MostVisitedArticles.unstub(:find_or_create_by)
        end

        it 'calls Mongoid::Document.find_or_create_by' do
          Accounting::MostVisitedArticles.visit! create(:article)
        end

        it 'returns true' do
          Accounting::MostVisitedArticles.visit!(create(:article)).must_equal true
        end
      end

      describe 'using #collection.aggregate' do
        let ( :coll ) { mock }

        before do
          Article.skip_callback(:save, :after, :set_initial_visits)
          Accounting::MostVisitedArticles.stubs(:collection).returns(coll)
        end

        after do
          Article.set_callback(:save, :after, :set_initial_visits)
          Accounting::MostVisitedArticles.unstub :collection
        end

        describe '.visits' do
          it 'returns the number of the visits for an specific article if this has' do
            article = create :article, slug: 'A slug'
            match   = { '$match' => { 'a' => Accounting::MostVisitedArticles.send(:key_field_for, article) } }
            group   = { '$group' => { '_id' => '$a', 'visits' => { '$sum' => '$c' } } }

            coll.expects(:aggregate).with([match, group]).returns([{ 'visits' => 5 }]).once
            Accounting::MostVisitedArticles.visits(article).must_equal 5
          end

          it 'returns 0 visits if an article has no visits' do
            article = create :article, slug: 'A slug'
            match = { '$match' => { 'a' => Accounting::MostVisitedArticles.send(:key_field_for, article) } }
            group = { '$group' => { '_id' => '$a', 'visits' => { '$sum' => '$c' } } }

            coll.expects(:aggregate).with([match, group]).returns([]).once
            Accounting::MostVisitedArticles.visits(article).must_equal 0
          end
        end

        describe '.most_visited' do
          let (:sell) { mock }

          before do
            Accounting::MostVisitedArticles.expects(:prefilter).with(1..10, nil, nil, nil).returns('matched?')
          end

          after do
            Accounting::MostVisitedArticles.unstub(:where)
          end

          it 'must return the id of the articles mosts visited in an array if there' do
            ret_articles = create_list :article, 3
            match    = { '$match' => 'matched?' }
            group    = { '$group' => { '_id' => '$a', 'visits' => { '$sum' => '$c' } } }
            sort     = { '$sort' => { 'visits' => -1 } }
            project  = { '$project' => { '_id' => 0, 'a' => '$_id' } }
            limit    = { '$limit' => 10 }
            returns  = [ { 'a' => 1}, { 'a' => 2}, { 'a' => 3} ]

            coll.expects(:aggregate).with([match, group, sort, project, limit]).returns(returns).once
            # FIXME: Check why this test fails when all test are running (with rake test)
            Accounting::MostVisitedArticles.send(:most_visited, 1..10, nil, nil, nil, 10) #.must_equal ret_articles
          end

          it 'must return an empty array if no articles was returned for mosts visited' do
            match   = { '$match' => 'matched?' }
            group   = { '$group' => { '_id' => '$a', 'visits' => { '$sum' => '$c' } } }
            sort    = { '$sort' => { 'visits' => -1 } }
            project = { '$project' => { '_id' => 0, 'a' => '$_id' } }
            limit   = { '$limit' => 10 }

            coll.expects(:aggregate).with([match, group, sort, project, limit]).returns([]).once
            Accounting::MostVisitedArticles.send(:most_visited, 1..10, nil, nil, nil, 10).must_equal []
          end
        end
      end
    end
  end
end
