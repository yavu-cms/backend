require 'test_helper'

module Accounting
  class MostCommentedArticlesTest < ActiveSupport::TestCase
    describe MostCommentedArticlesTest do
      describe '#new' do
        describe 'when required attributes are not present' do
          it 'fails' do
            most_commented = Accounting::MostCommentedArticles.new
            most_commented.valid?.must_equal false
            most_commented.errors.messages.must_include :a, :i
          end
        end

        describe 'when required attributes are present' do
          it 'succeeds' do
            most_commented = Accounting::MostCommentedArticles.new a: 'A example slug', i: '1d', d: DateTime.now, p: 100
            most_commented.valid?.must_equal true
            most_commented.errors.messages.must_equal Hash.new
          end
        end
      end

      describe '::persist' do
        before do
          mock_object = mock
          mock_object.expects(:delete).returns(true)
          Accounting::MostCommentedArticles.expects(:where).returns(mock_object)

          Accounting::MostCommentedArticles.expects(:create).returns(true).at_least(2)
        end

        let(:hash) { {"code"=>0,"response"=>[{"feed"=>"http=>//example.disqus.com/el_dia_39/latest.rss","category"=>"2467309","postsInInterval"=>2,"author"=>"36887483","forum"=>"example","title"=>"El Dia","userScore"=>0,"identifiers"=>["2"],"dislikes"=>0,"createdAt"=>"2013-09-17T17=>35=>23","slug"=>"el_dia_39","isClosed"=>false,"posts"=>2,"userSubscription"=>false,"link"=>"http=>//localhost=>9292/article/vicente-lopez-un-sorprendente-matungo-desde-la-costa-6263-2","likes"=>0,"message"=>"","id"=>"1770739057","isDeleted"=>false},{"feed"=>"http=>//example.disqus.com/el_dia_85/latest.rss","category"=>"2467309","postsInInterval"=>1,"author"=>"36887483","forum"=>"example","title"=>"El Dia","userScore"=>0,"identifiers"=>["1"],"dislikes"=>0,"createdAt"=>"2013-07-31T12=>40=>46","slug"=>"el_dia_85","isClosed"=>false,"posts"=>3,"userSubscription"=>false,"link"=>"http=>//localhost=>9292/article/breves-1","likes"=>0,"message"=>"","id"=>"1551074758","isDeleted"=>false}]} }

        it 'must call where, delete, and create' do
          Accounting::MostCommentedArticles.persist hash['response'], '1d'
        end
      end

      describe '::list_popular' do
        before do
  # FIXME: al remover los skips se puede descomentar estas lineas o replantear los tests: ldeleon
  #        mock_obj = mock()
  #        mock_obj.expects(:delete).returns(true)
  #        Accounting::MostCommentedArticles.expects(:where).returns(mock_obj)
  #        Accounting::MostCommentedArticles.expects(:create).returns(true).at_most(6)
        end
        let(:new_supplement) { Supplement.create name: 'Supplement' }

        let(:feed) do
          proc do |article_id|
            {
              "feed"=>"http=>//example.rss",
              "category"=>"2467309",
              "postsInInterval"=>2,
              "author"=>"36887483",
              "forum"=>"example",
              "title"=>"El Dia",
              "userScore"=>0,
              "identifiers"=>["#{article_id}"],
              "dislikes"=>0,"
              createdAt"=>"2013-09-17T17=>35=>23",
              "slug"=>"el_dia_39",
              "isClosed"=>false,
              "posts"=>2,
              "userSubscription"=>false,
              "link"=>"http=>//example.com",
              "likes"=>0,
              "message"=>"",
              "id"=>"1770739057",
              "isDeleted"=>false
            }
          end
        end

        it 'must return only visibles Articles'

        it 'must return only articles that belong to a specific section'

        it 'must return no articles because the edition is not visible'
      end
    end
  end
end
