require 'test_helper'
require 'mocha/setup'

module Accounting
  class MostVotedArticlesTest < ActiveSupport::TestCase
    describe MostVotedArticles do
      before do
        MostVotedArticles.delete_all
        WithExtraAttribute.delete_all
      end

      describe '.retrieve_user_vote' do
        let(:article)  { create :article }
        let(:article2) { create :article }
        describe 'when user did not vote in an article' do
          before do
            MostVotedArticles.perform_vote(article, 'user2', 1)
            MostVotedArticles.perform_vote(article2, 'user', 1)
          end
          it 'returns nil' do
            MostVotedArticles.retrieve_user_vote('user', article).must_be_nil
          end
        end
        describe 'when user voted in an article' do
          before do
            @v = MostVotedArticles.perform_vote(article, 'user', 1)
          end
          it 'returns a hash with the votes attributes and values' do
            v = MostVotedArticles.retrieve_user_vote('user', article)
            v.must_be_instance_of Hash
            v[:article].must_equal @v.article
            v[:user].must_equal @v.user
            v[:value].must_equal @v.value
          end
        end
      end

      describe '.already_voted?' do
        let(:article)  { create :article }
        let(:article2) { create :article }
        describe 'when user did not vote in an article' do
          before do
            MostVotedArticles.up(article, 'user2')
            MostVotedArticles.up(article2, 'user')
          end
          it 'returns false' do
            MostVotedArticles.already_voted?('user', article).must_equal false
          end
        end
        describe 'when user voted in an article' do
          before do
            MostVotedArticles.up(article, 'user')
          end
          it 'returns true' do
            MostVotedArticles.already_voted?('user', article).must_equal true
          end
        end
      end

      describe '.initial_votes_for' do
        it 'calls .retrieve_user_votes twice with a special user slug'
      end

      describe '.set_initial_votes_for' do
        it 'calls .perform_vote twice with a special user slug'
      end

      describe '.make' do
        let(:article) { build :article }
        describe 'when feeling = like' do
          it 'must call to .up and returns true' do
            MostVotedArticles.expects(:up).with(article,'user').once()
            MostVotedArticles.make(article, 'user', 'like').must_equal true
            MostVotedArticles.unstub(:up)
          end
        end
        describe 'when feeling = dislike' do
          it 'must call to .down and returns true' do
            MostVotedArticles.expects(:down).with(article,'user').once()
            MostVotedArticles.make(article, 'user', 'dislike').must_equal true
            MostVotedArticles.unstub(:down)
          end
        end
        describe 'when feeling = ???' do
          it 'must return false' do
            MostVotedArticles.make(article, 'user', 'ihateit').must_equal false
          end
        end
      end

      describe '.perform_vote' do
        let(:article) { create :article, slug: 'la capital' }

        describe 'when there is no vote for the article and user' do
          describe 'when value is 1' do
            it 'creates a new positive vote' do
              v = MostVotedArticles.perform_vote(article, 'user', 1)

              v.must_be :positive?
              v.user.must_equal 'user'
              v.article.must_equal article.slug
            end
          end
          describe 'when value is -1' do
            it 'creates a new negative vote' do
              v = MostVotedArticles.perform_vote(article, 'user', -1)

              v.must_be :negative?
              v.user.must_equal 'user'
              v.article.must_equal article.slug
            end
          end
        end
        describe 'when there is a vote for the article and user' do
          describe 'and that vote was positive' do
            before do
              @v = MostVotedArticles.perform_vote(article, 'user', 1)
            end
            it 'returns the same vote without changes' do
              MostVotedArticles.perform_vote(article, 'user', 1).must_equal @v
            end
          end
          describe 'and that vote was negative' do
            before do
              @pv = MostVotedArticles.perform_vote(article, 'user', -1)
            end
            it 'a positive vote changes the previous negative vote and returns it' do
              v = MostVotedArticles.perform_vote(article, 'user', 1)

              v.must_be :positive?
              v.user.must_equal @pv.user
              v.article.must_equal @pv.article
            end
          end
        end
      end

      describe '.up' do
        let(:article) { build :article }
        it 'calls to .perform_vote with value 1' do
          MostVotedArticles.expects(:perform_vote).with(article, 'user', 1).once
          MostVotedArticles.up(article, 'user')
          MostVotedArticles.unstub(:perform_vote)
        end
      end

      describe '.down' do
        let(:article) { build :article }
        it 'calls to .perform_vote with value -1' do
          MostVotedArticles.expects(:perform_vote).with(article, 'user', -1).once
          MostVotedArticles.down(article, 'user')
          MostVotedArticles.unstub(:perform_vote)
        end
      end

      describe '.votes' do
        let(:article) { build :article, slug: 'la capital' }

        describe 'when the article has no votes' do
          it 'returns 0 for total, 0 for up votes, 0 for down votes' do
            zero = { up: 0, down: 0 }
            MostVotedArticles.votes(article).must_equal zero
          end
        end

        describe 'when article has votes' do
          it 'counts each vote' do
            MostVotedArticles.create(article: article.slug, value: 1, date: DateTime.now)
            MostVotedArticles.create(article: article.slug, value: -1, date: DateTime.now)
            MostVotedArticles.create(article: article.slug, value: 1, date: DateTime.now)

            MostVotedArticles.votes(article).must_equal({ up: 2, down: 1 })
          end
        end
      end

      describe '.most_voted' do
        describe 'when articles are visibles' do
          describe 'when there are no votes in range' do
            it 'returns an empty array' do
              a1 = MostVotedArticles.create(article: 'article', value: 1, date: DateTime.now - 10.days)
              a2 = MostVotedArticles.create(article: 'article', value: 1, date: DateTime.now - 12.days)
              a3 = MostVotedArticles.create(article: 'article', value: 1, date: DateTime.now - 14.days)

              [a1,a2,a3].each { |a| WithExtraAttribute.create(article: a.article, invisible: false) }

              MostVotedArticles.most_voted(DateTime.now-5.days..DateTime.now).must_be :empty?
            end
          end

          describe 'when there are votes in range' do
            describe 'when there is only one voted article' do
              it 'returns just such article' do
                3.times do |i|
                  MostVotedArticles.create(article: 'article', value: 1, date: DateTime.now - i.days)
                end

                MostVotedArticles.most_voted(DateTime.now-4.days..DateTime.now).must_equal ['article']
              end
            end

            describe 'and limit is greater than voted articles count' do
              it 'returns only such voted articles in most voted order' do
                3.times do |i|
                  MostVotedArticles.create(article: 'article-1', value:  1, date: DateTime.now - i.days)
                  MostVotedArticles.create(article: 'article-2', value: -1, date: DateTime.now - i.days)
                  MostVotedArticles.create(article: 'article-3', value:  1, date: DateTime.now - i.days)
                end
                MostVotedArticles.create(article: 'article-1', value: 1, date: DateTime.now - 1.days)
                range = (DateTime.now - 5.days)..(DateTime.now + 1.days)

                articles = MostVotedArticles.most_voted(range, nil, 10)

                articles.must_equal ['article-1', 'article-3', 'article-2']
              end
            end

            describe 'and limit is lower than voted articles count' do
              it 'returns such number of articles in most voted first order' do
                4.times {|i| MostVotedArticles.create(article: 'article-1', value: 1, date: DateTime.now - i.days) }
                3.times do |i|
                  MostVotedArticles.create(article: 'article-2', value: 1, date: DateTime.now - i.days)
                  MostVotedArticles.create(article: 'article-3', value: -1, date: DateTime.now - i.days)
                end
                2.times {|i| MostVotedArticles.create(article: 'article-4', value: 1, date: DateTime.now - i.days) }
                MostVotedArticles.create(article: 'article-5', value: 1, date: DateTime.now - 1.days)
                MostVotedArticles.create(article: 'article-6', value: 1, date: DateTime.now - 1.days)

                range = DateTime.now-4.days..DateTime.now

                articles = MostVotedArticles.most_voted(range, nil, nil, nil, 3)

                articles.must_equal ['article-1', 'article-2', 'article-4']
              end
            end
          end

          describe 'when there are articles invisibles' do
            before do
              5.times do
                a = MostVotedArticles.create(article: 'article-1', value: 1, date: DateTime.now)
                WithExtraAttribute.create(article: a.article, invisible: true)
              end
              3.times do
                a = MostVotedArticles.create(article: 'article-2', value: 1, date: DateTime.now)
                WithExtraAttribute.create(article: a.article, invisible: false)
              end
              a = MostVotedArticles.create(article: 'article-3', value: 1, date: DateTime.now)
              WithExtraAttribute.create(article: a.article, invisible: false)
            end
            it 'they are not considered' do
              range = DateTime.now-4.days..DateTime.now
              articles = MostVotedArticles.most_voted(range, nil, 3)
              articles.must_equal ['article-2', 'article-3']
            end
          end
        end
      end
    end
  end
end
