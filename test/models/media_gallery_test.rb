require 'test_helper'

class MediaGalleryTest < ActiveSupport::TestCase
  describe MediaGallery do
    describe 'delete' do
      before do
        @gallery = MediaGallery.create(title: 'comic gallery')
        @medium_1 = create :image_medium
        @medium_2 = create :image_medium, file: upload_test_file('image_2.jpg')
        MediumMediaGallery.create(media_gallery: @gallery, medium: @medium_1)
        MediumMediaGallery.create(media_gallery: @gallery, medium: @medium_2)
        @gallery.save!
      end

      it 'should delete relations' do
        @medium_1.media_galleries.first.must_equal @gallery
        @medium_2.media_galleries.first.must_equal @gallery
        @gallery.destroy
        @medium_1.media_galleries.must_be_empty
        @medium_2.media_galleries.must_be_empty
      end
    end
  end
end