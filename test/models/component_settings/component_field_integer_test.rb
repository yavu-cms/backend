require 'test_helper'

class ComponentFieldIntegerTest < ActiveSupport::TestCase
  describe ComponentFieldInteger do
    describe '#value' do
      describe 'when the value is a valid integer' do
        let(:field) { build :component_field_integer, default: '23' }

        it 'returns the default value set' do
          field.value.must_equal 23
        end
      end

      describe 'when the value is not a valid integer yet can be sanitized' do
        let(:field) { build :component_field_integer, default: "I bet you can't deal with me" }

        it 'returns 0' do
          field.value.must_equal 0
        end
      end

      describe 'when the value is a blank string' do
        let(:field) { build :component_field_integer, default: '' }

        it 'returns nil' do
          field.value.must_be_nil
        end
      end

      describe 'when the value is nil' do
        let(:field) { build :component_field_integer, default: nil }

        it 'returns nil' do
          field.value.must_be_nil
        end
      end

      describe 'when the value cannot be sanitized' do
        let(:field) { build :component_field_integer, default: ['1', '2'] }

        it 'returns ComponentFieldInteger::DEFAULT' do
          field.value.must_equal ComponentFieldInteger::DEFAULT
        end
      end
    end

    describe '#field_type' do
      it 'has its own field type' do
        ComponentFieldInteger.new.field_type.must_equal :integer
      end
    end

    describe 'Default' do
      let(:field) { build :component_field_integer, default: nil }

      it 'has a nil default value' do
        field.value.must_be_nil
      end
    end
  end
end
