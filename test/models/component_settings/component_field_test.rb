require 'test_helper'

class ComponentFieldTest < ActiveSupport::TestCase
  describe ComponentField do
    describe '.types' do
      it 'returns all subclasses' do
        ComponentField.types.map(&:to_s).sort.must_equal(
          %w(ComponentFieldHash ComponentFieldInteger ComponentFieldModel ComponentFieldMultiModel ComponentFieldOrderedMultiModel ComponentFieldScalar)
        )
      end
    end

    describe '.build_from' do
      let(:name) { 'My field' }

      describe 'when the field type is not valid' do
        let(:info) { {type: :invalid} }

        it 'raises an error' do
          ->{ ComponentField.build_from(info, name) }.must_raise RuntimeError
        end
      end

      # Test .build_from for every possible type
      [
        [:scalar, ComponentFieldScalar],
        [:hash, ComponentFieldHash],
        [:integer, ComponentFieldInteger],
        [:model, ComponentFieldModel],
        [:multi_model, ComponentFieldMultiModel],
        [:ordered_multi_model, ComponentFieldOrderedMultiModel]
      ].each do |type, klass|
        describe "when the field to build is a #{type.inspect}" do
          let(:info) { {type: type} }

          before do
            klass.any_instance.expects(:value_from).with(info).once
          end

          after do
            klass.any_instance.unstub(:value_from)
          end

          it "returns a new #{klass} calling #value_from with the given info" do
            field = ComponentField.build_from(info, name)
            field.must_be_instance_of klass
          end
        end
      end
    end

    describe '.model_classes' do
      it 'only allows associated value models to be Article, Medium or Section' do
        model_classes = %w( Article Gallery Medium Section )
        model_classes << 'YavuSurveys::Survey' if defined?(YavuSurveys::Survey)
        ComponentField.model_classes.map(&:to_s).sort.must_equal model_classes
      end
    end

    describe '#to_s' do
      let(:name) { 'nice_little_name' }
      let(:description) { 'Nice little name' }

      describe 'when only the name is present' do
        let(:field) { build :component_field, name: name, description: '' }

        it 'returns the name' do
          field.to_s.must_equal name
        end
      end

      describe 'when only the description is present' do
        let(:field) { build :component_field, name: '', description: description }

        it 'returns the description' do
          field.to_s.must_equal description
        end
      end

      describe 'when both the name and description are present' do
        let(:field) { build :component_field, name: name, description: description }

        it 'returns the description' do
          field.to_s.must_equal description
        end
      end
    end

    describe '#value' do
      let(:field) { build :component_field, default: 'some default value' }

      it 'returns the default value set' do
        field.value.must_equal field.default
      end
    end

    describe '#value=' do
      let(:field) { build :component_field }
      let(:new_value) { 'new kid on the block' }

      it 'sets the value for the field' do
        field.value = new_value
        field.default.must_equal new_value
      end
    end

    describe '#model_class' do
      let(:fields) do
        %w(article Article).map { |mc| create :component_field, model_class: mc, with_component: true }
      end

      it 'must always return the model class name' do
        fields.map(&:model_class).uniq.must_equal [Article.name]
      end
    end

    describe '#model_class=' do
      let(:field) { create :component_field, with_component: true }
      describe 'when model classes exist' do
        let(:model_classes_from_view) { %w(article section medium) }
        let(:model_classes) { [Article, Section, Medium] }

        it 'must always record in :model_class attribute the model class name' do
          model_classes_from_view.each.with_index do |mc,i|
            field.model_class = mc
            field.send(:read_attribute, :model_class).must_equal model_classes[i].name
          end
        end
      end
      describe 'when a model class does not exist or it does not considered' do
        let(:no_model_class) { 'edition' }

        it 'blows' do
          ->{ field.model_class = no_model_class }.must_raise NoMethodError
        end
      end
    end

    describe 'copy' do
      let(:old_default) { 'old value' }
      let(:new_default) { 'shiny, new value' }
      let(:field) { create :component_field, default: old_default, with_component: true }

      it 'copies the field and overwrites its attributes with the provided ones' do
        copy = field.copy(default: new_default)
        copy.wont_be :persisted?
        copy.default.must_equal new_default
        %i( name component component_configuration ).each do |attr|
          copy.send(attr).must_equal field.send(attr)
        end
      end
    end

    describe '#value_from' do
      describe 'when the :default key is present' do
        let(:value) { 'some string' }
        let(:info) { {default: value} }
        let(:field) { ComponentField.new }

        before do
          field.value_from(info)
        end

        it 'sets the value as provided to the default attribute' do
          field.default.must_equal value
        end
      end

      describe 'when the :default key is not present' do
        let(:info) { {} }
        let(:field) { ComponentField.new }

        before do
          field.value_from(info)
        end

        it 'sets the default attribute to nil' do
          field.default.must_be_nil
        end
      end
    end

    describe '#default?' do
      describe 'when the field belongs to a component' do
        let(:field) { build :component_field_scalar, with_component: true }

        it 'returns true (is a default)' do
          field.must_be :default?
        end
      end

      describe "when the field doesn't belong to a component" do
        let(:component_configuration) { build :component_configuration }
        let(:field) { build :component_field_scalar, component_configuration: component_configuration }

        it "returns false (isn't a default)" do
          field.wont_be :default?
        end
      end
    end

    describe '#context_name' do
      let(:field) { ComponentField.new }

      before do
        field.expects(:view_name).returns("settings['name']").once
      end

      it 'returns an @-attribute version of the #view_name' do
        field.context_name.must_equal "@settings['name']"
      end
    end

    describe '#view_name' do
      let(:field) { ComponentField.new name: 'cool_field' }

      it 'returns how the field value might be accessed in views' do
        field.view_name.must_equal "settings['cool_field']"
      end
    end

    describe 'Validations' do
      describe 'presence of name' do
        let(:field) { build :component_field, name: '' }

        it 'requires the field to have a name' do
          field.wont_be :valid?
          field.errors[:name].must_include I18n.t('errors.messages.blank')
        end
      end

      describe 'uniqueness of name' do
        let(:name) { 'My field' }

        describe 'scoped by component' do
          let(:component) { create :component }

          before do
            @existing = create :component_field, name: name, component: component
          end

          it 'requires the name to be unique, scoped by component' do
            field = build(:component_field, name: name, component: component)
            field.wont_be :valid?
            field.errors[:name].must_include I18n.t('errors.messages.taken')
          end
        end

        describe 'scoped by component configuration' do
          let(:component_configuration) { create :component_configuration }

          before do
            @existing = create :component_field, name: name, component_configuration: component_configuration
          end

          it 'requires the name to be unique, scoped by component' do
            field = build(:component_field, name: name, component_configuration: component_configuration)
            field.wont_be :valid?
            field.errors[:name].must_include I18n.t('errors.messages.taken')
          end
        end
      end
    end
  end
  describe 'touch relations' do
    it 'must touch component'
    it 'must touch component_configuration'
  end
end
