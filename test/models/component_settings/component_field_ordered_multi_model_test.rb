require 'test_helper'

class ComponentFieldOrderedMultiModelTest < ActiveSupport::TestCase
  describe ComponentFieldOrderedMultiModel do
    describe '#value' do
      describe 'when no value is set' do
        let(:field) { build :component_field_ordered_multi_model }

        it 'returns an empty array' do
          field.value.must_equal []
        end
      end

      describe 'when only one value is set' do
        let(:referable) { field.component_values.first.referable }

        describe 'when the referable is a Section' do
          let(:field) { create :component_field_ordered_multi_model_section, values_count: 1, with_component: true }

          it 'returns the referable object set' do
            field.value.must_equal [referable]
          end
        end

        describe 'when the referable is an Article' do
          let(:field) { create :component_field_ordered_multi_model_article, values_count: 1, with_component: true }

          it 'returns the referable object set' do
            field.value.must_equal [referable]
          end
        end

        describe 'when the referable is a Medium' do
          let(:field) { create :component_field_ordered_multi_model_medium, values_count: 1, with_component: true }

          it 'returns the referable object set' do
            field.value.must_equal [referable]
          end
        end
      end

      describe 'when more than one value is set' do
        let(:article_1) { create :article }
        let(:article_2) { create :article }
        let(:article_3) { create :article }
        let(:article_4) { create :article }
        let(:component_value_1) { build :component_value, referable: article_1, order: 3, component_field: field }
        let(:component_value_2) { build :component_value, referable: article_2, order: 1, component_field: field }
        let(:component_value_3) { build :component_value, referable: article_4, order: 2, component_field: field }
        let(:component_value_4) { build :component_value, referable: article_3, order: 4, component_field: field }
        let(:field) { create :component_field_ordered_multi_model, model_class: 'Article', with_component: true }

        before do
          field.component_values = [component_value_1, component_value_2, component_value_3, component_value_4]
        end

        it 'returns all of them' do
          field.reload
          field.value.must_equal [article_2, article_4, article_1, article_3]
        end
      end
    end

    describe '#field_type' do
      it 'has its own field type' do
        ComponentFieldOrderedMultiModel.new.field_type.must_equal :ordered_multi_model
      end
    end

    describe '#value_from' do
      let(:article) { create :article }
      let(:field) { create :component_field_ordered_multi_model_section, values_count: 3, with_component: true }

      describe 'when the provided hash includes a :default key' do
        let(:info) { {model_class: 'Article', default: [{default: article.id, order: 1}]} }

        before do
          field.value_from(info)
          field.save!
        end

        it 'updates the associated values to match the ones passed in' do
          field.component_values.count.must_equal 1
          component_value = field.component_values.first
          component_value.referable.must_equal article
          component_value.order.must_equal 1
        end
      end

      describe 'when the provided hash does not include a :defaults key' do
        let(:info) { {model_class: 'Section'} }

        before do
          field.value_from(info)
          field.save!
        end

        it 'empties the associated values' do
          field.component_values.must_be_empty
        end
      end
    end

    describe 'Default' do
      let(:field) { build :component_field_ordered_multi_model }

      it 'has an empty array as default value' do
        field.value.must_equal []
      end
    end
  end
end
