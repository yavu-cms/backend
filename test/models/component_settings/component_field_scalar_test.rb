require 'test_helper'

class ComponentFieldScalarTest < ActiveSupport::TestCase
  describe ComponentFieldScalar do
    describe '#value' do
      let(:field) { build :component_field_scalar, default: 23 }

      it 'returns the default value set' do
        field.value.must_equal field.default
      end
    end

    describe '#field_type' do
      it 'has its own field type' do
        ComponentFieldScalar.new.field_type.must_equal :scalar
      end
    end

    describe 'Default' do
      let(:field) { build :component_field_scalar, default: nil }

      it 'has a nil default value' do
        field.value.must_be_nil
      end
    end
  end
end
