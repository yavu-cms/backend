require 'test_helper'

class ComponentFieldHashTest < ActiveSupport::TestCase
  describe ComponentFieldHash do
    describe '#value' do
      let(:default_value) { {some: 'value'} }
      let(:field) { build :component_field_hash, default: default_value }

      it 'returns the default value set as a HashWithIndifferentAccess' do
        field.value.must_be_instance_of HashWithIndifferentAccess
        field.value.must_equal field.default.with_indifferent_access
      end
    end

    describe '#field_type' do
      it 'has its own field type' do
        ComponentFieldHash.new.field_type.must_equal :hash
      end
    end

    describe '#value=' do
      describe 'value sanitizing' do
        it 'needs tests and most likely a refactor'
      end
    end

    describe 'Default' do
      let(:field) { create :component_field_hash, default: nil, with_component: true }

      it 'has an empty Hash as default value' do
        field.value.must_equal Hash.new
      end
    end
  end
end
