require 'test_helper'

class ComponentFieldModelTest < ActiveSupport::TestCase
  describe ComponentFieldModel do
    describe '#value' do
      describe 'when no value is set' do
        let(:field) { build :component_field_model, component_value: nil }

        it 'returns nil' do
          field.value.must_be_nil
        end
      end

      describe 'when a value is set' do
        let(:referable) { field.component_value.referable }

        describe 'when the referable is a Section' do

          describe 'when section is visible' do
            let(:field) { create :component_field_model_section, with_component: true }
            it 'returns the referable object set' do
              field.value.must_equal referable
            end
          end

          describe 'when section is not visible' do
            let(:field) { create :component_field_model_section, with_component: true, visible: false }
            it 'returns nil' do
              field.value.must_be_nil
            end
          end
        end

        describe 'when the referable is an Article' do

          describe 'when article is visible' do
            let(:field) { create :component_field_model_article, with_component: true }
            it 'returns the referable object set' do
              field.value.must_equal referable
            end
          end

          describe 'when article is not visible' do
            let(:field) { create :component_field_model_article, with_component: true, visible: false }
            it 'returns nil' do
              field.value.must_be_nil
            end
          end
        end
      end
    end

    describe '#field_type' do
      it 'has its own field type' do
        ComponentFieldModel.new.field_type.must_equal :model
      end
    end

    describe '#value_from' do
      let(:section) { create :section }
      let(:field) { create :component_field_model_section, with_component: true }

      before do
        field.value_from(info)
      end

      describe 'when the provided hash includes a :default and :model keys' do
        let(:info) { {model_class: 'Section', default: section.id} }

        it 'updates the associated values to match the ones passed in' do
          field.value.must_equal section
        end
      end

      describe 'when the provided hash does not include a :defaults key' do
        let(:info) { {model_class: 'Section'} }

        it 'empties the associated value' do
          field.value.must_be_nil
        end
      end
    end

    describe 'Default' do
      let(:field) { build :component_field_model }

      it 'has a nil default value' do
        field.value.must_be_nil
      end
    end
  end
end
