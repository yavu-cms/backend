require 'test_helper'

class ComponentFieldMultiModelTest < ActiveSupport::TestCase
  describe ComponentFieldMultiModel do
    describe '#value' do
      describe 'when no value is set' do
        let(:field) { build :component_field_multi_model_article }

        it 'returns an empty array' do
          field.value.must_equal []
        end
      end

      describe 'when only one value is set' do
        let(:referable) { field.component_values.first.referable }

        describe 'when the referable is a Section' do

          describe 'when section is visible' do
            let(:field) { create :component_field_multi_model_section, values_count: 1, with_component: true }
            it 'returns the referable object set' do
              field.value.must_equal [referable]
            end
          end

          describe 'when section is not visible' do
            let(:field) { create :component_field_multi_model_section, values_count: 1, with_component: true, visible: false }
            it 'returns empty array' do
              field.value.must_equal []
            end

          end
        end

        describe 'when the referable is an Article' do

          describe 'when article is visible' do
            let(:field) { create :component_field_multi_model_article, values_count: 1, with_component: true }

            it 'returns the referable object set' do
              field.value.must_equal [referable]
            end
          end

          describe 'when article is not visible' do
            let(:field) { create :component_field_multi_model_article, values_count: 1, with_component: true, visible: false }
            it 'must return empty array' do
              field.value.must_equal []
            end
          end
        end

        describe 'when the referable is a Medium' do
          let(:field) { create :component_field_multi_model_medium, values_count: 1, with_component: true }

          it 'returns the referable object set' do
            field.value.must_equal [referable]
          end
        end
      end

      describe 'when more than one value is set' do
        let(:field) { create :component_field_multi_model_article, values_count: 4, with_component: true }

        it 'returns all of them' do
          field.value.count.must_equal 4
          field.value.each { |v| v.must_be_instance_of Article }
        end
      end
    end

    describe '#field_type' do
      it 'has its own field type' do
        ComponentFieldMultiModel.new.field_type.must_equal :multi_model
      end
    end

    describe '#value_from' do
      let(:article) { create :article }
      let(:field) { create :component_field_multi_model_section, values_count: 3, with_component: true }

      describe 'when the provided hash includes a :default key' do
        let(:info) { {default: [article.id], model_class: 'Article'} }

        before do
          field.value_from(info)
          field.save!
        end

        it 'updates the associated values to match the ones passed in' do
          field.component_values.count.must_equal 1
          component_value = field.component_values.first
          component_value.referable.must_equal article
        end
      end

      describe 'when the provided hash does not include a :defaults key' do
        let(:info) { {model_class: 'Section'} }

        before do
          field.value_from(info)
          field.save!
        end

        it 'empties the associated values' do
          field.component_values.must_be_empty
        end
      end
    end

    describe 'Default' do
      let(:field) { build :component_field_multi_model }

      it 'has an empty array as default value' do
        field.value.must_equal []
      end
    end
  end
end
