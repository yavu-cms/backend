require 'test_helper'

class ComponentTranslationTest < ActiveSupport::TestCase
  describe ComponentTranslation do
    let (:translation) { build(:component_translation) }
    before do
      @valid_yaml = <<-YML
1:
  1_1: val1_1
  1_2:
    1_2_1: val1_2_1
      YML
    @invalid_yaml = <<-YML
1:
  1_1: val
    1_2: error because 1_1 has a key!!!
    YML
    end

    describe ".filetype" do
      it 'returns text/x-yaml' do
        ComponentTranslation.filetype.must_equal 'text/x-yaml'
      end
    end

    describe "#filetype" do
      it 'returns class defined filetype' do
        translation.filetype.must_equal ComponentTranslation.filetype
      end
    end

    describe "#to_s" do
      it 'returns the locale field' do
        translation.to_s.must_equal translation.locale
      end
    end

    describe "#to_hash" do
      it 'returns a YAML as hash' do
        translation = build(:component_translation, values: @valid_yaml)
        translation.to_hash.must_equal YAML.load(@valid_yaml)
      end

      it 'returns an empty hash when values is empty' do
        translation = build(:component_translation, values: nil)
        translation.to_hash.must_equal Hash.new
        translation = build(:component_translation, values: "")
        translation.to_hash.must_equal Hash.new
      end

    end

    describe "Validations" do
      describe 'values field' do
        it 'fails when it is not a valid yaml' do
          translation = build(:component_translation, values: @invalid_yaml)
          translation.valid?
          translation.errors[:values].must_include I18n.t('errors.messages.yml_format_error')
        end

        it 'succeds when it is a valid yaml' do
          translation = build(:component_translation, values: @valid_yaml)
          translation.valid?
          translation.errors[:values].must_be_empty
        end

        it 'succeds when it is empty' do
          translation = build(:component_translation, values: nil)
          translation.valid?
          translation.errors[:values].must_be_empty
        end
      end

      describe 'locale field' do
        it 'fails when empty' do
          translation = build(:component_translation, locale: nil)
          translation.must_be :invalid?
          translation.errors[:locale].must_include I18n.t('errors.messages.blank')
        end

        it 'succeds when not empty' do
          translation = ComponentTranslation.new(locale: :some_value)
          translation.valid?
          translation.errors[:locale].must_be_empty
        end
      end

      describe 'component_id field' do
        it 'fails when empty' do
          translation = build(:component_translation, component: nil)
          translation.must_be :invalid?
          translation.errors[:component].must_include I18n.t('errors.messages.blank')
        end

        it 'succeds when not empty' do
          translation = ComponentTranslation.new(component_id: create(:component).id)
          translation.valid?
          translation.errors[:component_id].must_be_empty
        end
      end

      describe 'uniqueness of locale and component' do
        it 'fails when duplicates same locale for same component' do
          t1 = create(:component_translation, locale: :es )
          t2 = build(:component_translation, component: t1.component, locale: :es )
          t2.must_be :invalid?
          t2.errors[:locale].must_include I18n.t('errors.messages.taken')
        end

        it 'must raises exception from database when dulicates same locale & component' do
          t1 = create(:component_translation, locale: :es )
          t2 = build(:component_translation, component: t1.component, locale: :es )
          proc do
            t2.save(validate: false)
          end.must_raise ActiveRecord::RecordNotUnique
        end
      end
    end

    describe 'touch relations' do
      it 'must touch component'
    end
  end
end