require 'test_helper'

class ArticlesCoverArticlesComponentConfigurationTest < ActiveSupport::TestCase
  describe ArticlesCoverArticlesComponentConfiguration do
    describe 'Validations' do
      describe 'article field' do
        it 'fails when empty' do
          article_cover_articles = build :articles_cover_articles_component_configuration, article: nil
          article_cover_articles.must_be :invalid?
          article_cover_articles.errors[:article].must_include I18n.t('errors.messages.blank')
        end

        it 'succeeds when not empty' do
          article_cover_articles = build :articles_cover_articles_component_configuration
          article_cover_articles.valid?
          article_cover_articles.errors[:article].must_be_empty
        end
      end

      describe 'cover_articles_component_configuration field' do
        it 'fails when empty' do
          article_cover_articles = build :articles_cover_articles_component_configuration, cover_articles_component_configuration: nil
          article_cover_articles.must_be :invalid?
          article_cover_articles.errors[:cover_articles_component_configuration].must_include I18n.t('errors.messages.blank')
        end

        it 'succeeds when not empty' do
          article_cover_articles = build :articles_cover_articles_component_configuration
          article_cover_articles.valid?
          article_cover_articles.errors[:cover_articles_component_configuration].must_be_empty
        end
      end

      describe 'view belongs to the association cover articles component' do
        describe 'when the chosen view belongs to the cover articles component associated to the configuration' do
          before do
            @configuration = create :cover_articles_component_configuration
            @article = create :article
            @view = @configuration.component.views.create name: 'some view'
            @article_cover_articles = build :articles_cover_articles_component_configuration,
                                            article: @article,
                                            cover_articles_component_configuration: @configuration,
                                            view: @view
          end

          it 'succeeds' do
            @article_cover_articles.must_be :valid?
          end
        end

        describe "when the view doesn't belong to the associated cover articles component" do
          before do
            @view = create :view
            @article_cover_articles = build :articles_cover_articles_component_configuration, view: @view
          end

          it 'fails' do
            @article_cover_articles.wont_be :valid?
            @article_cover_articles.errors[:view].must_include I18n.t('errors.messages.invalid')
          end
        end
      end

      describe 'article uniqueness' do
        before do
          @configuration = create :cover_articles_component_configuration
          @article = create :article
          @view = @configuration.component.views.create name: 'some view'
          @article_cover_articles = create :articles_cover_articles_component_configuration,
                                           article: @article,
                                           cover_articles_component_configuration: @configuration,
                                           view: @view
        end

        it 'fails when duplicates same article for same edition & cover_articles_component_configuration' do
          a2 = ArticlesCoverArticlesComponentConfiguration.new article: @article,
                                                               cover_articles_component_configuration: @configuration,
                                                               view: @view
          a2.must_be :invalid?
          a2.errors[:article].must_include I18n.t('errors.messages.taken')
        end

        it 'must raise exception from database when duplicates same file for same component & type' do
          a2 = ArticlesCoverArticlesComponentConfiguration.new article: @article,
                                                               cover_articles_component_configuration: @configuration,
                                                               view: @view
          ->{ a2.save(validate: false) }.must_raise ActiveRecord::RecordNotUnique
        end
      end

      describe 'as_api_resource' do
        before do
          @configuration = create :cover_articles_component_configuration
          @article = create :article
          @view = @configuration.component.views.create name: 'some view'
          @article_cover_articles = create :articles_cover_articles_component_configuration,
                                           article: @article,
                                           cover_articles_component_configuration: @configuration,
                                           view: @view
        end

        it 'returns expected attributes' do
          @resource = @article_cover_articles.as_api_resource
          @resource.id.must_equal @article_cover_articles.id
          @resource.article.must_equal @article_cover_articles.article.as_api_resource
          @resource.partial.must_equal @article_cover_articles.view.template_name
        end
      end
    end

    describe 'touch relation' do
      it 'must touch cover_articles_component_configuration when updated' do
        configuration = create :cover_articles_component_configuration
        article = create :article
        view = configuration.component.views.create name: 'some view'
        article_cover_articles = create :articles_cover_articles_component_configuration,
                                        article: article,
                                        cover_articles_component_configuration: configuration,
                                        view: view
        article_cover_articles.cover_articles_component_configuration.expects(:touch).once
        article_cover_articles.save
      end
    end
  end
end