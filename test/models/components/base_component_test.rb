require 'test_helper'

class BaseComponentTest < ActiveSupport::TestCase
  describe BaseComponent do
    before do
      ComponentAssetUploader.root = Rails.root.join(*%w{tmp tests component})
    end

    after do
      FileUtils.remove_dir(ComponentAssetUploader.root, true)
    end

    describe '.types' do
      it 'must return an array with subclasses' do
        BaseComponent.types.map(&:to_s).sort.must_equal BaseComponent.subclasses.map(&:to_s).sort
      end
    end

    describe '.search' do
      it 'must return an array with matched results' do
        one = create :component, name: "This component is the only one."
        create :component, name: "Other component"
        BaseComponent.search('only').to_a.must_equal [ one ]
      end

      it 'must return an empty array with no parameter' do
        BaseComponent.search('').must_equal []
        BaseComponent.search(nil).must_equal []
      end
    end

    describe '.available_for_editor' do
      before do
        @expected = [create(:component), create(:cover_component), create(:cover_component), create(:component)]
        # Some other CoverArticlesComponents that should not be returned:
        2.times { create :cover_articles_component }
      end

      it 'returns any component that is not a CoverArticlesComponent' do
        BaseComponent.available_for_editor.must_equal @expected
      end
    end

    describe '#cover?' do
      it 'must return false' do
        build(:component).wont_be :cover?
      end
    end

    describe '#cover_articles?' do
      it 'must return false' do
        build(:component).wont_be :cover_articles?
      end
    end

    describe '#takes_layout_views?' do
      it 'must return false' do
        build(:component).wont_be :takes_layout_views?
      end
    end

    describe '#destroy_view?' do

      it 'must return true if component has more than 1 view' do
        component = create :component, views_count: 2
        component.must_be :destroy_view?, component.views.first
      end

      it 'must return false if component has 1 view' do
        component = create :component, views_count: 1
        component.wont_be :destroy_view?, component.views.first
      end

      it 'wont destroy the last view of component' do
        component = create :component, views_count: 1
        component.views.first.wont_be :destroy
      end

      it 'must destroy some view if has more than one' do
        component = create :component, views_count: 2
        component.views.first.must_be :destroy
      end

      it 'wont destroy a view that is used by a component configuration' do
        component = create :component, views_count: 2
        view = component.views.first
        create :component_configuration, component: component, view: view
        view.wont_be :destroy
      end

      it 'must destroy a view that is used by a component configuration' do
        component = create :component, views_count: 2
        view = component.views.last
        create :component_configuration, component: component, view: view
        component.views.first.must_be :destroy
      end
    end

    describe '#to_s' do
      it 'must return the name of the component' do
        component = build :component, name: 'name'
        component.to_s.must_equal component.name
      end

      it 'must return an empty string when name is nil' do
        component = build :component, name: nil
        component.to_s.must_equal ''
      end
    end

    describe '#slug' do
      let(:component_name) { 'component Name' }
      let(:component) { build :component, name: component_name }

      it 'is is set when saved' do
        component.slug.must_be_nil
        component.save!
        component.slug.wont_be_nil
        component.slug.must_equal component_name.parameterize('_')
      end

      describe 'when title is too long' do
        let(:max_length) { BaseComponent::MAX_SLUG_LENGTH }
        let(:component) { create(:component, name: 'ab' * (max_length/2+1) ) }

        it 'is truncated to `MAX_SLUG_LENGTH` chars' do
          component.slug.length.must_equal max_length
        end
      end
    end

    describe '#new_configuration' do
      let(:component) { create :component }

      it 'must return a new ComponentConfiguration instance with component set' do
        configuration = component.new_configuration
        configuration.must_be :new_record?
        configuration.must_be_instance_of ComponentConfiguration
        configuration.component.must_equal component
        component.configurations.must_include configuration
      end

      it 'must accept ComponentConfiguration attributes as argument' do
        column = build(:column)
        configuration = build(:component).new_configuration(column: column)
        configuration.column.must_equal column

        view = build(:view)
        configuration = build(:component).new_configuration(view: view)
        configuration.view.must_equal view

        configuration = build(:component).new_configuration(hidden: true)
        configuration.must_be :hidden

        configuration = build(:component).new_configuration(order: 1)
        configuration.order.must_equal 1

        fields = [build(:component_field_scalar, name: 'a', default: '1')]
        configuration = build(:component).new_configuration(fields: fields)
        configuration.fields.must_equal fields
      end

      it 'must accept ComponentConfiguration attributes as argument but dont overwrite component' do
        view = build(:view)
        component = build(:component)
        other_component = build(:component)
        configuration = component.new_configuration(view: view, component: other_component)
        configuration.view.must_equal view
        configuration.component.must_equal component
      end
    end

    describe '#defaults' do
      it 'must returns an empty Hash object' do
        component = BaseComponent.new
        component.defaults.must_equal Hash.new
      end

      describe 'Component fields' do
        let(:field_1) { build :component_field_scalar }
        let(:field_2) { build :component_field_hash }
        let(:fields) { [field_1, field_2] }
        let(:expected) do
          {field_1.name => field_1.value, field_2.name => field_2.value}
        end

        %i[ component cover_component cover_articles_component ].each do |component_type|
          describe "When the component is a #{component_type}" do
            it 'takes the defaults from the component fields' do
              component = create component_type
              component.fields = fields.each { |f| f.component = component }
              component.defaults.must_equal expected
            end
          end
        end
      end
    end

    describe '#defaults=' do
      describe 'when the provided defaults are not processable' do
        let(:component) { create(:component) }
        let(:defaults) { {field: {}} }

        before do
          ComponentField.expects(:build_from).
            with({}, :field, {component: component}).
            raises(RuntimeError).once

          component.expects(:fields=).with([]).once
        end

        it 'empties the associated fields' do
          component.defaults = defaults
        end
      end

      describe 'when the provided defaults are processable' do
        let(:component) { create :component }
        let(:defaults) { {'field' => {im: 'good'}, 'other_field' => {unprocessable: true}} }
        let(:field_stub) { mock.responds_like_instance_of ComponentField }

        before do
          ComponentField.expects(:build_from).
            with({im: 'good'}, 'field', {component: component}).
            returns(field_stub).once
          ComponentField.expects(:build_from).
            with({unprocessable: true}, 'other_field', {component: component}).
            raises(RuntimeError).once

          component.expects(:fields=).with([field_stub]).once
        end

        it 'sets only those that are processable' do
          component.defaults = defaults
        end
      end
    end

    describe '#default_view' do
      let(:component) { create :component }

      before do
        another_view = create :view, component: component, default: false
        @default_view = create :view, component: component, default: true
      end

      it 'returns the default view for the component' do
        component.default_view.must_equal @default_view
      end
    end

    describe '#copy' do
      let(:views_count) { 5 }
      let(:translations_count) { 5 }

      it 'must return a new instance with specified name' do
        component = BaseComponent.new name: 'some name'
        component.copy('other name').name.must_equal 'other name'
      end

      describe 'General copying with associations' do
        def check_associated_objects(copy, original, association, count, &block)
          copy.send(association).wont_be_empty
          copy.send(association).length.must_equal count
          copy.send(association).select(&:persisted?).must_be_empty
          copy.send(association).zip(original.send(association)).detect(&block).must_be_nil
        end

        it 'must return a new instance with name changed to I18n components.copy.prefix' do
          component = create :component, name: 'some name', views_count: views_count, translations_count: translations_count, with_assets: true
          copy = component.copy
          copy.name.must_equal I18n.t('components.copy.prefix', default: 'Copy of %{name}', name: component.name)
          # Copy must not be persisted
          copy.must_be :new_record?
          # it must copy associated views
          check_associated_objects(copy, component, :views, views_count) do |cp, orig|
            cp.name != orig.name || cp.value != orig.value
          end
          # it must copy associated translations
          check_associated_objects(copy, component, :translations, translations_count) do |cp, orig|
            cp.locale != orig.locale || cp.values != orig.values
          end
          # it must copy associated assets
          check_associated_objects(copy, component, :assets, component.assets.count) do |cp, orig|
            cp.file.filename != orig.file.filename || cp.type != orig.type
          end
          # it must not copy the associated configurations
          copy.configurations.must_be_empty
        end
      end

      it 'must save the new instance with every associated asset' do
        component = create(:component, with_assets: true)
        copy = component.copy
        copy.assets.zip(component.assets).each do |cpy, original|
           cpy.file.current_path.wont_equal original.file.current_path
        end
        proc { copy.save! }.must_be_silent
      end
    end

    describe '#fields_hash' do
      let(:component) { create :component }

      before do
        @scalar_field = create :component_field_scalar, component: component
        @hash_field = create :component_field_hash, component: component
        component.fields = [@scalar_field, @hash_field]
        @expected = {@scalar_field.name => @scalar_field, @hash_field.name => @hash_field}.with_indifferent_access
      end

      it 'returns a hash with the field names as keys and the fields as values' do
        component.fields_hash.must_equal @expected
      end
    end

    describe '#update_representation_assets' do
      let(:component) { build :component }
      let(:configurations) { [mock.responds_like_instance_of(ComponentConfiguration), mock.responds_like_instance_of(ComponentConfiguration)] }
      let(:representation) { mock.responds_like_instance_of Representation }

      before do
        component.expects(:configurations).returns(configurations).once
        configurations.first.expects(:representation).returns(nil).once
        configurations.last.expects(:representation).returns(representation).once
        representation.expects(:update_assets!).once
      end

      it 'calls #update_assets! on the related configurations only if they have a representation' do
        component.update_representation_assets
      end
    end

    describe 'when name changes'do
      let(:name) { 'a_name'}
      let(:new_name) { 'a_new_name' }

      it 'must rename asset files' do
        component = create(:component, name: name, with_assets: true)
        component.assets.map{|x| File.dirname(x.file_url).split('/').last}.uniq.must_equal [ name ]
        component.reload # Needed so changes are noticed
        component.name = new_name
        component.save!
        component.assets.map{|x| File.dirname(x.file_url).split('/').last}.uniq.must_equal [ new_name ]
      end
    end

    describe 'default scope' do
      it 'must be sorted by name' do
        create(:component, name: 'BB')
        create(:component, name: 'ZZ')
        create(:component, name: 'AA')
        BaseComponent.all.map(&:name).must_equal %w( AA BB ZZ)
      end
    end

    describe 'Associations tests' do
      it 'must destroy views' do
        component = create(:component, views_count: 3)
        other_component = create(:component, views_count: 3)
        View.count.must_equal (component.views.length + other_component.views.length)
        component.must_be :destroy
        View.count.must_equal other_component.views.length
      end

      it 'must destroy translations' do
        component = create(:component, translations_count: 3)
        other_component = create(:component, translations_count: 3)
        ComponentTranslation.count.must_equal (component.translations.length + other_component.translations.length)
        component.must_be :destroy
        ComponentTranslation.count.must_equal other_component.translations.length
      end

      it 'must destroy assets' do
        component = create(:component, with_assets: true)
        other_component = create(:component, with_assets: true)
        ComponentAsset.count.must_equal (component.assets.length + other_component.assets.length)
        component.reload
        component.must_be :destroy
        ComponentAsset.count.must_equal other_component.assets.length
      end

      it 'wont destroy configurations' do
        component = create(:component)
        create(:component_configuration, component: component)
        component.reload
        component.wont_be :destroy
      end
    end

    describe 'Callbacks' do
      it 'should be deleted if it is not being used by any configuration' do
        component = create(:component)
        component.configurations.must_be :empty?
        component.must_be :destroy
      end

      describe 'assets relation' do
        before do
          @component = create :component
          representation = create :representation
          row = create :row, representation: representation
          column = create :column, row: row
          component_configuration = create :component_configuration, column: column, component: @component
          @component.reload
        end

        after do
          Representation.any_instance.unstub(:update_assets!)
        end

        it 'must call representation#update_assets! when adding a new element' do
          Representation.any_instance.expects(:update_assets!).once
          @component.assets << build(:component_asset, component: @component)
        end

        it 'must call representation#update_assets! when removing an element' do
          Representation.any_instance.expects(:update_assets!).twice
          @component.assets << build(:component_asset, component: @component)
          @component.reload
          @component.assets.first.destroy
        end
      end

      describe 'before_save' do
       it 'must set_slug'
      end

      describe 'after_save' do
       it 'must call #touch_configurations'
      end

      describe 'after_touch' do
       it 'must call touch_configurations'
      end

      describe 'touch' do
        it 'must touch all configurations'
        it 'must touch all representations'
        it 'must touch all routes'
        it 'must touch all client_applications'
      end
    end

    describe 'Validations' do
      describe 'name field' do
        it 'fails when empty' do
          component = build(:component, name: nil)
          component.wont_be :valid?
          component.errors[:name].must_include I18n.t('errors.messages.blank')
        end

        it 'succeeds when is not empty' do
          component = build(:component, name: 'not nil')
          component.valid?
          component.errors[:name].must_be_empty
        end

        it 'must be unique' do
          component = create(:component)
          other_component = build(:component, name: component.name)
          other_component.wont_be :valid?
          other_component.errors[:name].must_include I18n.t('errors.messages.taken')
        end
      end

      describe 'views association' do
        it 'fails when empty' do
          component = BaseComponent.new
          component.wont_be :valid?
          component.errors[:views].must_include I18n.t('errors.messages.blank')
        end

        it 'succeeds when not empty' do
          component = BaseComponent.new
          component.views.new name: 'a view'
          component.valid?
          component.errors[:views].must_be_empty
        end
      end

      describe 'alternative format uniqueness' do
        it 'does not allow associating the same alternative format more than once to the component'
      end
    end
  end
end
