require 'test_helper'

class CoverComponentConfigurationTest < ActiveSupport::TestCase
  describe CoverComponentConfiguration do
    describe '#templates' do
      it 'must return a hash with each components.view name as key and its value' do
        c1 = build(:cover_articles_component)
        c1.views = [ build(:view, name: 'name 1',value: 'value 1'),
                     build(:view, name: 'name 2', value: 'value 2')]
        c2 = build(:cover_articles_component)
        c2.views = [ build(:view, name: 'name 3',value: 'value 3'),
                     build(:view, name: 'name 4', value: 'value 4')]

        configuration = build :cover_component_configuration,
                              cover_articles_component_configurations: [c1.new_configuration,
                                                                        c2.new_configuration]
        configuration.templates.must_equal Hash['name_1' => 'value 1',
                                                'name_2' => 'value 2',
                                                'name_3' => 'value 3',
                                                'name_4' => 'value 4']
      end

      it 'must merge repreated views avoiding duplicated keys' do
        c1 = build(:cover_articles_component)
        c1.views = [ build(:view, name: 'name 1',value: 'value 1'),
                     build(:view, name: 'name 2', value: 'value 2')]
        c2 = build(:cover_articles_component)
        c2.views = [ build(:view, name: 'name 2', value: 'value 2'),
                     build(:view, name: 'name 3',value: 'value 3'),
                     build(:view, name: 'name 4', value: 'value 4')]

        configuration = build :cover_component_configuration,
                              cover_articles_component_configurations: [c1.new_configuration,
                                                                        c2.new_configuration]
        configuration.templates.must_equal Hash['name_1' => 'value 1',
                                                'name_2' => 'value 2',
                                                'name_3' => 'value 3',
                                                'name_4' => 'value 4']
      end
    end

    describe '#translations' do
      it 'must return a hash with each components.translation locale as key and its value' do
        c1 = build(:cover_articles_component)
        c1.translations = [ build(:component_translation, locale: 'name 1', values: 'value 1'),
                            build(:component_translation, locale: 'name 2', values: 'value 2')]
        c2 = build(:cover_articles_component)
        c2.translations = [ build(:component_translation, locale: 'name 3', values: 'value 3'),
                            build(:component_translation, locale: 'name 4', values: 'value 4')]

        configuration = build :cover_component_configuration,
                              cover_articles_component_configurations: [ c1.new_configuration,
                                                                         c2.new_configuration]
        configuration.translations.must_equal Hash['name 1' => 'value 1',
                                                   'name 2' => 'value 2',
                                                   'name 3' => 'value 3',
                                                   'name 4' => 'value 4']
      end

      it 'must merge repreated translations avoiding duplicated keys' do
        c1 = build(:cover_articles_component)
        c1.translations = [ build(:component_translation, locale: 'name 1', values: 'value 1'),
                            build(:component_translation, locale: 'name 2', values: 'value 2')]
        c2 = build(:cover_articles_component)
        c2.translations = [ build(:component_translation, locale: 'name 2', values: 'value 2'),
                            build(:component_translation, locale: 'name 3', values: 'value 3'),
                            build(:component_translation, locale: 'name 4', values: 'value 4')]

        configuration = build :cover_component_configuration,
                              cover_articles_component_configurations: [ c1.new_configuration,
                                                                         c2.new_configuration]
        configuration.translations.must_equal Hash['name 1' => 'value 1',
                                                   'name 2' => 'value 2',
                                                   'name 3' => 'value 3',
                                                   'name 4' => 'value 4']
      end
    end

    describe '#copy' do
      let(:description) { 'to be copied' }
      let(:order) { 3 }
      let(:values) { HashWithIndifferentAccess[key: 'value'] }
      let(:component) { create(:component) }
      let(:cover_articles_count) { 5 }
      let(:fields) { [build(:component_field_scalar, name: 'key', default: 'value')] }
      let(:configuration) { create :cover_component_configuration, description: description, component: component, column: create(:column), order: order, cover_articles_count: cover_articles_count }
      let(:copy) { configuration.copy }

      before do
        configuration.fields = fields.each { |f| f.component_configuration = configuration }
      end

      it 'must be a new_record?' do
        configuration.must_be :persisted?
        copy.must_be :new_record?
      end

      it 'must preserve same component' do
        copy.component.must_equal configuration.component
      end

      it 'must not copy column' do
        configuration.column.wont_be_nil
        copy.column.must_be_nil
      end

      it 'must copy same view' do
        copy.view.must_equal configuration.view
      end

      it 'must copy hidden property' do
        copy.wont_be :hidden?
        copy.hidden.must_equal configuration.hidden
        configuration.hidden = true
        configuration.save!
        configuration.copy.must_be :hidden?
      end

      it 'must copy order' do
        copy.order.must_equal order
      end

      it 'must copy values'  do
        copy.values.must_equal values
      end

      it 'must description' do
        copy.description.must_equal description
      end

      it 'must copy each cover_articles_component_configuration' do
        configuration.cover_articles_component_configurations.wont_be_empty
        copy.cover_articles_component_configurations.select(&:persisted?).must_be_empty
        copy.cover_articles_component_configurations.select do |x|
          x.cover_component_configuration.nil? ||
          x.cover_component_configuration != copy
        end.must_be_empty
        copy.cover_articles_component_configurations.length.must_equal cover_articles_count
      end
    end


    describe 'Associations' do
      before do
        @cover_configuration = create :cover_component_configuration
        10.downto(1) do |i|
          create :cover_articles_component_configuration, description: "Configuration #{i}",
            order: i,
            cover_component_configuration: @cover_configuration
        end
      end

      it 'must count 10 items' do
        CoverArticlesComponentConfiguration.count.must_equal 10
        @cover_configuration.cover_articles_component_configurations.count.must_equal 10
      end

      it 'articles are ordered by order for each edition' do
        ordered_names = []
        1.upto(10) { |x| ordered_names << "Configuration #{x}" }
        @cover_configuration.cover_articles_component_configurations.map(&:description).must_equal ordered_names
      end
    end

    describe 'Validations' do

      describe 'view field' do

        it 'suceeds when empty' do
          configuration = CoverComponentConfiguration.new component: build(:cover_component)
          configuration.must_be :cover?
          configuration.valid?
          configuration.errors[:view].must_be_empty
        end

        it 'succeeds when is not empty' do
          configuration = build(:cover_component_configuration, view: build(:view))
          configuration.must_be :cover?
          configuration.valid?
          configuration.errors[:view].must_be_empty
        end

        it 'suceeds if it does not belong to component.views' do
          component = build(:cover_component)
          configuration = build(:cover_component_configuration, component: component, view: build(:view))
          configuration.must_be :cover?
          configuration.valid?
          configuration.errors[:view].must_be_empty
        end

        it 'succeds if it belongs to component.views' do
          component = build(:cover_component)
          configuration = build(:component_configuration, component: component, view: component.views.sample)
          configuration.must_be :cover?
          configuration.valid?
          configuration.errors[:view].must_be_empty
        end
      end
    end
  end
end