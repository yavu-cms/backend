require 'test_helper'

class CoverArticlesComponentConfigurationTest < ActiveSupport::TestCase
  describe CoverArticlesComponentConfiguration do
    describe '#to_s' do
      describe 'when the configuration has a description' do
        let(:description) { 'Cool, huh?' }
        let(:configuration) { build :cover_articles_component_configuration, description: description }

        it 'returns the description' do
          configuration.to_s.must_equal description
        end
      end

      describe "when the configuration doesn't have a description" do
        let(:configuration) { build :cover_articles_component_configuration, description: nil }

        it 'returns an empty string' do
          configuration.to_s.must_equal ''
        end
      end
    end

    describe '#copy' do
      let(:description) { 'to be copied' }
      let(:order) { 3 }
      let(:values) { Hash[a:1, b:3] }
      let(:fields) { [build(:component_field_integer, name: 'a', default: 1), build(:component_field_integer, name: 'b', default: 3)] }
      let(:component) { create(:component) }
      let(:cover_component_configuration) { create :cover_component_configuration }
      let(:configuration) { create :cover_articles_component_configuration, description: description, component: component, column: create(:column), order: order, cover_component_configuration: cover_component_configuration }
      let(:copy) { configuration.copy }

      before do
        configuration.fields = fields.each { |f| f.component_configuration = configuration }
      end

      it 'must be a new_record?' do
        configuration.must_be :persisted?
        copy.must_be :new_record?
      end

      it 'must preserve same component' do
        copy.component.must_equal configuration.component
      end

      it 'must not copy column' do
        configuration.column.wont_be_nil
        copy.column.must_be_nil
      end

      it 'must copy same view' do
        copy.view.must_equal configuration.view
      end

      it 'must copy hidden property' do
        copy.wont_be :hidden?
        copy.hidden.must_equal configuration.hidden
        configuration.hidden = true
        configuration.save!
        configuration.copy.must_be :hidden?
      end

      it 'must copy order' do
        copy.order.must_equal order
      end

      it 'must copy values'  do
        copy.values.must_equal values.with_indifferent_access
      end

      it 'must description' do
        copy.description.must_equal description
      end

      it 'wont copy cover_component_configuration' do
        copy.cover_component_configuration.must_be_nil
        copy.cover_component_configuration_id.must_be_nil
        configuration.cover_component_configuration_id.wont_be_nil
        configuration.cover_component_configuration.wont_be_nil
      end
    end

    describe '#ordered_articles=' do
      describe "when a referenced article doesn't exist" do
        let(:configuration) { create :cover_articles_component_configuration }

        before do
          @article = create :article
          @busted_id = @article.id
          @article.destroy
          @article_2 = create :article
          @view = configuration.component.views.create name: 'some view'
        end

        it 'ignores it' do
          configuration.ordered_articles = [
            {id: @busted_id, view_id: @view.id },
            {id: @article_2.id, view_id: @view.id }
          ]

          configuration.reload
          configuration.articles.must_equal [@article_2]
          configuration.articles_cover_articles_component_configurations.first.view.must_equal @view
        end
      end

      describe 'when all articles exist' do

        let(:configuration) { create :cover_articles_component_configuration }
        let(:articles) { [@article_1, @article_3, @article_2, @article_4] }

        before do
          @article_1 = create :article, title: 'Article A'
          @article_2 = create :article, title: 'Article B'
          @article_3 = create :article, title: 'Article C'
          @article_4 = create :article, title: 'Article D'
          @view = configuration.component.views.create name: 'some view'
        end

        it 'correctly associates the articles in the expected order' do
          configuration.ordered_articles = articles.map { |a| Hash[id: a.id, view_id: @view.id] }

          configuration.reload
          configuration.articles.must_equal articles
          configuration.articles_cover_articles_component_configurations.each do |acacc|
            acacc.view.must_equal @view
          end
        end
      end

      describe 'when the configuration has associated articles' do
        let(:configuration) { create :cover_articles_component_configuration }
        let(:articles) { [@article_3, @article_4, @article_2] }

        before do
          @article_1 = create :article, title: 'Article A'
          @article_2 = create :article, title: 'Article B'
          @article_3 = create :article, title: 'Article C'
          @article_4 = create :article, title: 'Article D'
          @article_5 = create :article, title: 'Article E'
          @view = configuration.component.views.create name: 'some view'

          configuration.articles_cover_articles_component_configurations <<
            create(:articles_cover_articles_component_configuration, cover_articles_component_configuration: configuration, article: @article_1, view: @view)
          configuration.save!
        end

        it 'overwrites them with the ones passed in, correctly ordering them' do
          configuration.ordered_articles = articles.map { |a| Hash[id: a.id, view_id: @view.id] }

          configuration.reload
          configuration.articles.wont_include @article_1
          configuration.articles.must_equal articles
          configuration.articles_cover_articles_component_configurations.each do |acacc|
            acacc.view.must_equal @view
          end
        end
      end

      describe 'when the configuration has no associated articles' do
        let(:articles) { [@article_4, @article_2, @article_1] }
        let(:configuration) { create :cover_articles_component_configuration }

        before do
          @article_1 = create :article, title: 'Article A'
          @article_2 = create :article, title: 'Article B'
          @article_3 = create :article, title: 'Article C'
          @article_4 = create :article, title: 'Article D'
          @view = configuration.component.views.create name: 'some view'
        end

        it 'correctly associates the articles in the expected order' do
          configuration.ordered_articles = articles.map { |a| Hash[id: a.id, view_id: @view.id] }

          configuration.reload
          configuration.articles.must_equal articles
          configuration.articles_cover_articles_component_configurations.each do |acacc|
            acacc.view.must_equal @view
          end
        end
      end
    end

    describe 'Associations' do
      let(:ordered_titles) { [].tap { |arr| 1.upto(10) { |i| arr << "Article #{i}" }}}
      before do
        @cover_configuration = create(:cover_articles_component_configuration)
        @view = @cover_configuration.component.views.create name: 'some nice view'
        10.downto(1) do |i|
          ArticlesCoverArticlesComponentConfiguration.create! article: create(:article, title: "Article #{i}"),
            order: i,
            cover_articles_component_configuration: @cover_configuration,
            view: @view
        end
      end

      it 'must count 10 items in the correct order' do
        ArticlesCoverArticlesComponentConfiguration.count.must_equal 10
        @cover_configuration.articles.map{|x| x.title}.must_equal ordered_titles
      end
    end

    describe 'Validations' do
      describe 'cover_component_configuration field' do

        it 'fails when empty' do
          configuration = build(:cover_articles_component_configuration, cover_component_configuration: nil)
          configuration.wont_be :valid?
          configuration.errors[:cover_component_configuration].must_include I18n.t('errors.messages.blank')
        end

        it 'suceeds when it is not empty' do
          cover = build(:cover_component_configuration)
          configuration = build(:cover_articles_component_configuration, cover_component_configuration: cover)
          configuration.valid?
          configuration.errors[:cover_component_configuration].wont_include I18n.t('errors.messages.blank')
        end
      end

      describe 'description column' do

        it 'suceeds when empty' do
          configuration = build(:cover_articles_component_configuration, column: nil)
          configuration.valid?
          configuration.errors[:column].must_be_empty
        end

        it 'succeeds when is not empty' do
          configuration = build(:cover_articles_component_configuration, column: Column.new)
          configuration.valid?
          configuration.errors[:column].must_be_empty
        end
      end
    end
  end
end
