require 'test_helper'

class ComponentServiceTest < ActiveSupport::TestCase
  describe ComponentService do
    before do
      @component = create :component, name: 'component'
      @view = create :view, component: @component, name: 'service'

      @component_service = @component.services.new(view: @view,
        http_method: "GET", body: "#some_body", name: 'service name', available_parameters_string: ["a_param", "param_2"])
    end

    describe '#cache_key' do
      it 'must consider component_configuration_id'
    end


    describe "#to_s" do
      it 'must return HTTP method and slug concatenated' do
        @component_service.to_s.must_equal "GET service-name"
      end
    end

    describe '#calculated_cache_control' do
      it 'must return cache_control when set' do
        @component_service.cache_control = 'a'
        @component_service.calculated_cache_control.must_equal 'a'
      end

      it 'must return representation cache control when cache_control is blank' do
        client_application = build(:client_application)
        client_application.expects(:calculated_service_cache_control)
        representation = Object.new
        representation.expects(:client_application).returns(client_application)
        component_configuration = Object.new
        component_configuration.expects(:representation).returns(representation)
        @component_service.expects(:component_configuration).returns(component_configuration)
        @component_service.calculated_cache_control
      end
    end

    describe "#slug" do
      it 'must return name parameterized' do
        @component_service.slug.must_equal "service-name"
      end
    end

    describe "#available_parameters_string" do
      it 'must return available_parameters joined by comma' do
        @component_service.available_parameters_string.must_equal "a_param,param_2"
      end
    end

    describe "#available_parameters_string=" do
      it 'must set available_parameters from comma separated values into an array' do
        @component_service.available_parameters_string = "param_10,param_10,param_20,param_9000"
        @component_service.available_parameters.must_equal ["param_10", "param_20", "param_9000"]
      end
    end

    describe "#template" do
      it 'must return view#template_name' do
        @component_service.template.must_equal "component/service"
      end
    end

    describe "#component_configuration" do
      describe 'when component_configuration_id is not set' do
        it 'must return nil' do
          @component_service.component_configuration.must_equal nil
        end
      end

      describe 'when component_configuration_id is set' do
        before do
          @cc = create :cover_component_configuration
          @component_service = @cc.component.services.new(view: @view,
            http_method: "GET", name: 'service name')
          @component_service.component_configuration_id = @component_service.component.configurations.first
        end
        it 'must return component_configuration from component_configuration_id' do
          @component_service.component_configuration.must_equal @component_service.component.configurations.first
        end
      end
    end

    describe "#build_context" do
      before do
        @component_service.expects(:build_context).returns(ServiceContextProcessor.new(@component_service)).once
      end
      it 'must call ServiceContextProcessor#new' do
        @component_service.build_context.must_be_instance_of ServiceContextProcessor
      end
    end

    describe "#populate_evaluated_context" do
      it 'must set every parameter send as value for component context'
    end

    describe 'as_api_resource' do
      it 'must return expected attributes' do
        @resource = @component_service.as_api_resource
        @resource.id.must_equal @component_service.slug
        @resource.component_configuration_id.must_equal @component_service.component_configuration_id
        @resource.http_method.must_equal @component_service.http_method
        @resource.template.must_equal @component_service.template
        @resource.body.must_equal @component_service.body
      end
    end

    describe "Validations" do
      describe 'view field' do
        it 'fails when empty' do
          @component_service = @component.services.new(http_method: "GET", name: 'service name')

          @component_service.wont_be :valid?
          @component_service.errors[:view].must_include I18n.t('errors.messages.blank')
        end
      end

      describe 'http_method field' do
        it 'fails when empty' do
          @component_service = @component.services.new(view: @view, name: 'service name')

          @component_service.wont_be :valid?
          @component_service.errors[:http_method].must_include I18n.t('errors.messages.blank')
        end
      end

      describe 'name field' do
        it 'fails when empty' do
          @component_service = @component.services.new(view: @view, http_method: "GET")

          @component_service.wont_be :valid?
          @component_service.errors[:name].must_include I18n.t('errors.messages.blank')
        end

        it 'fails when name and method are repeated for a given component' do
          @component_service = @component.services.create(view: @view, http_method: "GET", name: 'service name')
          @component_service_2 = @component.services.new(view: @view, http_method: "GET", name: 'service name')

          @component_service_2.wont_be :valid?
          @component_service_2.errors[:name].must_include I18n.t('errors.messages.taken')
        end
      end

      describe 'when all fields are valid' do
        it 'succeeds' do
          @component_service = @component.services.new(view: @view, http_method: "GET", name: 'service name')

          @component_service.must_be :valid?
          @component_service.errors.must_be_empty
        end
      end
    end
  end

  describe 'touch relations' do
    it 'must touch component'
  end
end
