require 'test_helper'

class ComponentAssetTest < ActiveSupport::TestCase
  describe ComponentAsset do
    before do
      ComponentAssetUploader.root = Rails.root.join(*%w{tmp tests component_assets})
    end

    after do
      FileUtils.remove_dir(ComponentAssetUploader.root, true)
    end

    describe "Validations" do

      describe 'type field' do
        it 'fails when empty' do
          asset = build(:component_asset, type: nil)
          asset.must_be :invalid?
          asset.errors[:type].must_include I18n.t('errors.messages.blank')
        end

        it 'succeds when not empty' do
          asset = ComponentAsset.new(type: 'javascript')
          asset.valid?
          asset.errors[:type].must_be_empty
        end
      end

      describe 'component_id field' do

        it 'fails when empty' do
          asset = build(:component_asset, component: nil)
          asset.must_be :invalid?
          asset.errors[:component].must_include I18n.t('errors.messages.blank')
        end

        it 'succeds when not empty' do
          asset = ComponentAsset.new(component_id: create(:component).id)
          asset.valid?
          asset.errors[:component_id].must_be_empty
        end
      end

      describe 'file field' do
        it 'fails when empty' do
          asset = build(:component_asset, file: nil)
          asset.must_be :invalid?
          asset.errors[:file].must_include I18n.t('errors.messages.blank')
        end

        it 'succeds when not empty' do
          asset = ComponentAsset.new(file: upload_test_file('image_1.jpg'))
          asset.valid?
          asset.errors[:file].must_be_empty
        end
      end

      describe 'file uniqueness' do
        it 'fails when dulicates same file for same component & type' do
          a1 = create(:component_asset)
          a2 = build(:component_asset, component_id: a1.component_id)
          a2.must_be :invalid?
          a2.errors[:file].must_include I18n.t('errors.messages.taken')
        end

        it 'must raises exception from database when dulicates same file for same component & type' do
          a1 = create(:component_asset)
          a2 = build(:component_asset,
                     component_id: a1.component_id,
                     type: a1.type,
                     file: File.open(a1.file.current_path))
          proc do
            a2.save(validate: false)
          end.must_raise ActiveRecord::RecordNotUnique
        end
      end
    end

    describe '#absolute_url' do
      before do
        @asset = create :component_asset
        ComponentAssetUploader.expects(:absolute_prefix_for_type).with(@asset.type).returns('some_path')
      end

      it 'returns complete path to this asset\'s dir' do
        @asset.absolute_url.must_equal "some_path/#{@asset.component.slug}"
      end
    end

    describe 'touch relations' do
      it 'must touch component'
    end
  end
end