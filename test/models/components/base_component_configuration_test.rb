require 'test_helper'

class BaseComponentConfigurationTest < ActiveSupport::TestCase
  describe BaseComponentConfiguration do
    describe '#to_s' do
      describe 'when it has a description' do
        let(:component_configuration) { build :component_configuration, description: 'Cover Articles Component Configuration' }

        it 'returns the description' do
          component_configuration.to_s.must_equal component_configuration.description
        end
      end

      describe "when it doesn't have a description" do
        let(:component_configuration) { build :component_configuration, description: nil }

        it 'returns the description' do
          component_configuration.to_s.must_equal ''
        end
      end
    end

    describe '#configured_services' do
      it 'must set every service component_configuration to self'
      it 'must return a hash with indifferent access with each key being service name'
      it 'must duplicate services before changing component_configuration_id'
    end

    describe '#available_attributes' do
      let(:config_fields) do
        [
          build(:component_field_scalar, name: 'from_configuration', default: 'configuration'),
          build(:component_field_scalar, name: 'on_both', default: 'configuration'),
        ]
      end
      let(:component_fields) do
        [
          build(:component_field_scalar, name: 'from_component', default: 'component'),
          build(:component_field_scalar, name: 'on_both', default: 'component'),
        ]
      end
      let(:component) { create :component }
      let(:component_configuration) { create :component_configuration, component: component }
      let(:expected) { HashWithIndifferentAccess[from_component: 'component', on_both: 'configuration'] }

      before do
        component.fields = component_fields.each { |field| field.component = component }
        component_configuration.fields = config_fields.each { |field| field.component_configuration = component_configuration }
      end

      it 'overwrites the component fields with former with the latter on conflicts' do
        component_configuration.available_attributes.sort.must_equal expected.sort
      end
    end

    describe '#values' do
      let(:section) { create :section }
      let(:component_configuration) { create :component_configuration }

      before do
        field = create :component_field_model, name: 'section', model_class: 'Section', component_configuration: component_configuration
        value = create :component_value, component_field: field, referable: section
        component_configuration.fields << field
      end

      it 'returns a hash with the values specified by #all_fields_hash' do
        component_configuration.values.must_equal HashWithIndifferentAccess[section: section]
      end
    end

    describe '#values=' do
      describe 'when the provided values are empty or blank' do
        let(:component) { component_configuration.component }
        let(:component_configuration) { create :component_configuration }
        let(:values) { {field: ''} }

        before do
          component.fields = [
            build(:component_field_scalar, name: 'field', component: component),
            build(:component_field_scalar, name: 'another', component: component)
          ]
          component_configuration.expects(:fields=).with([]).once
        end

        it 'empties the associated fields' do
          component_configuration.values = values
        end
      end

      describe 'when the provided values are not blank' do
        let(:component) { component_configuration.component }
        let(:component_configuration) { create :component_configuration }
        let(:values) { {'field' => 'my field value', 'some_other_field' => 'with its value'} }
        let(:field_1) { build :component_field_scalar, name: 'field', component: component }
        let(:field_2) { build :component_field_scalar, name: 'another', component: component }
        let(:copied_field) { mock.responds_like_instance_of ComponentFieldScalar }

        before do
          component.fields = [field_1, field_2]
          field_1.expects(:copy).
            with(value: 'my field value', component: nil, component_configuration: component_configuration).
            returns(copied_field).once
          component_configuration.expects(:fields=).with([copied_field]).once
        end

        it 'sets only those whose name is present in the associated component and are not blank' do
          component_configuration.values = values
        end
      end
    end

    describe '#cover?' do
      it 'must delegate to component#cover?' do
        component = build(:component)
        component.expects(:cover?).returns('something').once
        build(:component_configuration, component: component).cover?.must_equal 'something'
      end

      it 'must return false when component nil' do
        build(:component_configuration, component: nil).wont_be :cover?
      end
    end

    describe '#cover_articles?' do
      it 'must delegate to component#cover_articles?' do
        component = build(:component)
        component.expects(:cover_articles?).returns('otherthing').once
        build(:component_configuration, component: component).cover_articles?.must_equal 'otherthing'
      end

      it 'must return false when component nil' do
        build(:component_configuration, component: nil).wont_be :cover_articles?
      end
    end

    describe '#templates' do
      it 'must return a hash with each components.view name as key and its value' do
        component = create(:component)
        component.views = [ build(:view, name: 'name 1',value: 'value 1'),
                            build(:view, name: 'name 2', value: 'value 2')]
        configuration = build(:component_configuration, component: component)
        configuration.templates.must_equal Hash["#{component.slug}/name_1" => 'value 1', "#{component.slug}/name_2" => 'value 2']
      end
    end

    describe '#translations' do
      it 'must return a hash with each components.translation locale as key and its value' do
        component = build(:component)
        component.translations = [  build(:component_translation, locale: 'name 1',values: 'value 1'),
                                    build(:component_translation, locale: 'name 2', values: 'value 2')]
        configuration = build(:component_configuration, component: component)
        configuration.translations.must_equal Hash['name 1' => 'value 1', 'name 2' => 'value 2']
      end
    end

    describe '#copy' do
      let(:description) { 'to be copied' }
      let(:order) { 3 }
      let(:values) { Hash[a: '1'] }
      let(:component) { create(:component) }
      let(:configuration) { create :component_configuration, description: description, component: component, column: create(:column), order: order }
      let(:copy) { configuration.copy }

      before do
        configuration.fields = [build(:component_field_scalar, name: 'a', default: '1', component_configuration: configuration)]
        configuration.reload
      end

      it 'must be a new_record?' do
        configuration.must_be :persisted?
        copy.must_be :new_record?
      end

      it 'must preserve same component' do
        copy.component.must_equal configuration.component
      end

      it 'must not copy column' do
        configuration.column.wont_be_nil
        copy.column.must_be_nil
      end

      it 'must copy same view' do
        copy.view.must_equal configuration.view
      end

      it 'must copy hidden property' do
        copy.wont_be :hidden?
        copy.hidden.must_equal configuration.hidden
        configuration.hidden = true
        configuration.save!
        configuration.copy.must_be :hidden?
      end

      it 'must copy order' do
        copy.order.must_equal order
      end

      it 'must copy fields' do
        copied_fields = copy.fields.map { |f| [f.type, f.name, f.value] }
        original_fields = configuration.fields.map { |f| [f.type, f.name, f.value] }
        copied_fields.must_equal original_fields
      end

      it 'must description' do
        copy.description.must_equal description
      end
    end

    describe '#fields_hash' do
      let(:component_configuration) { create :component_configuration }

      before do
        @scalar_field = create :component_field_scalar, component_configuration: component_configuration
        @hash_field = create :component_field_hash, component_configuration: component_configuration
        component_configuration.fields = [@scalar_field, @hash_field]
        @expected = {@scalar_field.name => @scalar_field, @hash_field.name => @hash_field}.with_indifferent_access
      end

      it 'returns a hash with the field names as keys and the fields as values' do
        component_configuration.fields_hash.must_equal @expected
      end
    end

    describe '#all_fields_hash' do
      describe 'when the component has no fields' do
        let(:component) { create :component, fields: [] }
        let(:component_configuration) { create :component_configuration, component: component }

        describe 'when the configuration has fields' do
          before do
            component_configuration.fields << create(:component_field_scalar, component_configuration: component_configuration)
          end

          it 'returns an empty hash' do
            component_configuration.all_fields_hash.must_equal Hash.new
          end
        end

        describe 'when the configuration has no fields' do
          let(:component) { create :component, fields: [] }
          let(:component_configuration) { create :component_configuration, component: component }

          it 'returns an empty hash' do
            component_configuration.all_fields_hash.must_equal Hash.new
          end
        end
      end

      describe 'when the component has fields' do
        let(:component) { create :component }
        let(:component_configuration) { create :component_configuration, component: component }
        let(:component_fields_desc) { {scalar: 'scalar_field', hash: 'hash_field' } }

        before do
          component.fields = component_fields_desc.map do |type, name|
            build :"component_field_#{type}", name: name, component: component
          end
        end

        describe 'when all of the fields are set for the configuration' do
          before do
            component_configuration.fields = component_fields_desc.map do |type, name|
              build :"component_field_#{type}", name: name, component_configuration: component_configuration
            end
          end

          it 'returns all the fields from the configuration' do
            component_configuration.all_fields_hash.must_equal component_configuration.fields_hash
          end
        end

        describe 'when only some fields are set for the configuration' do
          let(:field_type) { component_fields_desc.first.first }
          let(:field_name) { component_fields_desc.first.last }

          before do
            field = build :"component_field_#{field_type}", name: field_name, component_configuration: component_configuration
            component_configuration.fields << field
            @expected = component.fields_hash.merge field_name => field
          end

          it 'returns all the fields from both, taking the value for those set in the configuration from it rather than the component' do
            component_configuration.all_fields_hash.must_equal @expected
          end
        end

        describe 'when no fields are set for the configuration' do
          it 'returns all the fields from the component' do
            component_configuration.all_fields_hash.must_equal component.fields_hash
          end
        end

        describe 'when any field value is set for the configuration that is not present in the component' do
          before do
            component_configuration.fields << build(:component_field_scalar, name: 'another_field', component_configuration: component_configuration)
          end

          it 'ignores the extra field' do
            component_configuration.all_fields_hash.must_equal component.fields_hash
          end
        end
      end
    end

    describe 'default scope' do
      it 'must be sorted by order' do
        create(:component_configuration, order: 10)
        create(:component_configuration, order: 5)
        create(:component_configuration, order: 1)
        BaseComponentConfiguration.all.map(&:order).must_equal [1,5,10]
      end
    end

    describe 'Callbacks' do
      after do
        Representation.any_instance.unstub(:update_assets!)
      end

      it 'must call representation.update_assets! after_save when representation is persisted' do
        representation = create :representation
        representation.rows << (row = create :row)
        row.columns << (column = create :column)
        Representation.any_instance.expects(:update_assets!).once
        component_configuration = build :component_configuration, column: column
        component_configuration.save!
      end
    end

    describe 'Validations' do

      describe 'component field' do
        it 'fails when empty' do
          configuration = build(:component_configuration, component: nil)
          configuration.wont_be :valid?
          configuration.errors[:component].must_include I18n.t('errors.messages.blank')
        end

        it 'succeeds when is not empty' do
          component = build(:component)
          configuration = build(:component_configuration, component: component)
          configuration.valid?
          configuration.errors[:component].must_be_empty
        end
      end

      describe 'description field' do

        it 'fails when empty' do
          configuration = build(:component_configuration, description: nil)
          configuration.wont_be :valid?
          configuration.errors[:description].must_include I18n.t('errors.messages.blank')
        end

        it 'succeeds when is not empty' do
          configuration = build(:component_configuration, description: 'some description')
          configuration.valid?
          configuration.errors[:component].must_be_empty
        end
      end

      describe 'description column' do

        it 'fails when empty' do
          configuration = build(:component_configuration, column: nil)
          configuration.wont_be :valid?
          configuration.errors[:column].must_include I18n.t('errors.messages.blank')
        end

        it 'succeeds when is not empty' do
          configuration = build(:component_configuration, column: Column.new)
          configuration.valid?
          configuration.errors[:column].must_be_empty
        end
      end


      describe 'view field' do

        it 'fails when empty' do
          configuration = BaseComponentConfiguration.new
          configuration.wont_be :valid?
          configuration.errors[:view].must_include I18n.t('errors.messages.blank')
        end


        it 'succeeds when is not empty' do
          configuration = build(:component_configuration, view: build(:view))
          configuration.valid?
          configuration.errors[:view].wont_include I18n.t('errors.messages.blank')
        end

        it 'fails if it does not belong to component.views' do
          component = build(:component)
          configuration = build(:component_configuration, component: component, view: build(:view))
          configuration.valid?
          configuration.errors[:view].must_include I18n.t('errors.messages.must_be_a_component_view')

        end

        it 'succeeds if it belongs to component.views' do
          component = build(:component)
          configuration = build(:component_configuration, component: component, view: component.views.sample)
          configuration.valid?
          configuration.errors[:view].must_be_empty
        end
      end
    end
  end
end