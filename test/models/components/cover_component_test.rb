require 'test_helper'

class CoverComponentTest < ActiveSupport::TestCase
  describe CoverComponent do
    describe '#new_configuration' do
      let(:component) { create :cover_component }

      it 'must return a new CoverComponentConfiguration instance with component set' do
        configuration = component.new_configuration
        configuration.must_be_instance_of CoverComponentConfiguration
      end

      it 'must be associated to self' do
        configuration = component.new_configuration
        configuration.component.must_equal component
        component.configurations.must_include configuration
      end

      it 'must be a new record' do
        configuration = component.new_configuration
        configuration.must_be :new_record?
      end

    end

    describe '#cover?' do
      it 'must return true' do
        build(:cover_component).must_be :cover?
      end
    end

    describe '#destroy_view? has no sense for this class' do
      it 'must return true if component has more than 1 view' do
        component = create :cover_component, views_count: 2
        component.must_be :destroy_view?, component.views.first
      end

      it 'must return true ieven if component has 1 view' do
        component = create :cover_component, views_count: 1
        component.must_be :destroy_view?, component.views.first
      end

      it 'must destroy the last view of component' do
        component = create :cover_component, views_count: 1
        component.views.first.must_be :destroy
      end

      it 'must destroy some view if has more than one' do
        component = create :cover_component, views_count: 2
        component.views.first.must_be :destroy
      end
    end

    describe 'Validations' do
      describe 'views association' do
        it 'suceeds when empty' do
          component = CoverComponent.new
          component.wont_be :valid?
          component.errors[:views].must_be_empty
        end

        it 'succeeds when not empty' do
          component = CoverComponent.new
          component.views.new name: 'a view'
          component.valid?
          component.errors[:views].must_be_empty
        end
      end
    end
  end
end