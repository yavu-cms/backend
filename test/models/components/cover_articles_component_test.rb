require 'test_helper'

class CoverArticlesComponentTest < ActiveSupport::TestCase
  describe CoverArticlesComponent do
    describe '#new_configuration' do
      let(:component) { create :cover_articles_component }

      it 'must return a new CoverArticlesComponentConfiguration instance with component set' do
        configuration = component.new_configuration
        configuration.must_be_instance_of CoverArticlesComponentConfiguration
      end

      it 'must be associated to self' do
        configuration = component.new_configuration
        configuration.component.must_equal component
        component.configurations.must_include configuration
      end

      it 'must be a new record' do
        configuration = component.new_configuration
        configuration.must_be :new_record?
      end
    end

    describe '#cover_articles?' do
      it 'must return true' do
        build(:cover_articles_component).must_be :cover_articles?
      end
    end

    describe '#takes_layout_views?' do
      it 'must return true' do
        build(:cover_articles_component).must_be :takes_layout_views?
      end
    end
  end
end