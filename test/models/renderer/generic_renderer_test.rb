require 'test_helper'

class GenericRendererTest < ActiveSupport::TestCase
  describe GenericRenderer do
    describe '.build' do
      describe 'when invoked with an HTML format' do
        it 'yields an HtmlRenderer instance' do
          GenericRenderer.build('text/html').must_be_instance_of HtmlRenderer
          GenericRenderer.build(:html).must_be_instance_of HtmlRenderer
        end
      end

      describe 'when invoked with a format other than HTML' do
        it 'yields a GenericRenderer instance' do
          GenericRenderer.build('text/plain').must_be_instance_of GenericRenderer
          GenericRenderer.build('application/rss+xml').must_be_instance_of GenericRenderer
          GenericRenderer.build('image/jpg').must_be_instance_of GenericRenderer
          GenericRenderer.build('application/json').must_be_instance_of GenericRenderer
        end
      end
    end

    describe '#initialize' do
      it 'stores the provided format' do
        GenericRenderer.new('some-format').format.must_equal 'some-format'
      end
    end

    describe '#render' do
      let(:format) { 'application/rss+xml' }
      let(:renderer) { GenericRenderer.new(format) }

      describe 'when the representation has a view for the requested format' do
        describe 'when it has no rows' do
          let(:rendered_view) { 'A portion of a view [[content]]' }
          let(:view) { create :view, format: format, value: rendered_view }
          let(:views) { [view]}
          let(:representation) { create :representation, views: views }
          let(:main_view_locals) { {content: ''} }

          before do
            View.any_instance.expects(:render).once.with(main_view_locals).returns(rendered_view)
          end

          after do
            View.any_instance.unstub :render
          end

          it 'renders the view with no content for it' do
            renderer.render(representation)
          end
        end

        describe 'when it has rows' do
          before do
            main_view = create :view, format: format, value: View::CONTENT_PLACEHOLDER
            @representation = create :representation, views: [main_view]
            # Components & views
            component_1 = create :component
            component_2 = create :component
            component_3 = create :component
            component_1_view = create :view, component: component_1, format: format
            component_2_view = create :view, component: component_2, format: format
            component_3_view = create :view, component: component_3, format: 'another/format' # should be ignored - different format
            cover_component = create :cover_component
            cover_articles_component = create :cover_articles_component
            cover_articles_component_view = create :view, component: cover_articles_component, format: format

            # Row 1 - 2 columns
            r1 = @representation.rows.create(order: 1)
            r1c1 = r1.columns.create(order: 1)
            r1c2 = r1.columns.create(order: 2)
            r1c1cc2 = create :component_configuration, component: component_1, column: r1c1, order: 2
            r1c1cc1 = create :component_configuration, component: component_2, column: r1c1, order: 1
            r1c2cc1 = create :component_configuration, component: component_1, column: r1c2, order: 1
            r1c2cc2 = create :component_configuration, component: component_3, column: r1c2, order: 2 # should be ignored - no view available for format

            # Row 2 - 1 column
            r2 = @representation.rows.create(order: 2)
            r2c1 = r2.columns.create(order: 1)
            r2c1cc1 = create :component_configuration, component: component_2, column: r2c1, order: 1
            r2c1cc2 = create :component_configuration, component: component_3, column: r2c1, order: 2 # should be ignored - no view available for format

            # Row 3 - empty
            r3 = @representation.rows.create(order: 3)

            # Row 4 - 1 column with cover components
            r4 = @representation.rows.create(order: 4)
            r4c1 = r4.columns.create(order: 1)
            r4c1cc1 = create :cover_component_configuration, component: cover_component, column: r4c1, order: 1
            r4c1cc1cacc = create :cover_articles_component_configuration, component: cover_articles_component, cover_component_configuration: r4c1cc1
            r4c1cc2 = create :cover_component_configuration, component: cover_component, column: r4c1, order: 2 # should be ignored - has no child cover articles

            # Build the expected result string lines in the correct order
            ordered_components = [
              [r1c1cc1, component_2_view], [r1c1cc2, component_1_view], [r1c2cc1, component_1_view],
              [r2c1cc1, component_2_view],
            ]
            lines = ordered_components.map do |(cc, view)|
              %Q(<%= render_component '#{view.template_name}', _context.components[#{cc.id}] %>\n)
            end
            # Finally, add the cover component line
            lines << <<-ERB
<%= render_component '#{cover_articles_component_view.template_name}', _context.components[#{r4c1cc1.id}].components[#{r4c1cc1cacc.id}] %>
            ERB
            @expected_result = lines.join(GenericRenderer::GLUE)
          end

          it 'renders the main view with the component views in the correct order' do
            renderer.render(@representation).must_equal @expected_result
          end
        end
      end

      describe "when the representation doesn't have a view for the requested format" do
        let(:representation) { build :representation, views: [] }
        let(:rendered_html) { 'Default view' }

        before do
          HtmlRenderer.any_instance.expects(:render).once.with(representation).returns(rendered_html)
        end

        after do
          HtmlRenderer.any_instance.unstub(:render)
        end

        it 'builds an HTML renderer and uses it to render the default view' do
          renderer.render(representation).must_equal rendered_html
        end
      end
    end
  end
end
