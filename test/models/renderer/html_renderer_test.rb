require 'test_helper'

class HtmlRendererTest < ActiveSupport::TestCase
  describe HtmlRenderer do
    let(:renderer) { @renderer ||= GenericRenderer.build(:html) }

    describe '#format' do
      it 'always returns text/html' do
        HtmlRenderer.build('text/html').format.must_equal 'text/html'
        HtmlRenderer.build(:html).format.must_equal 'text/html'
        HtmlRenderer.new('text/html').format.must_equal 'text/html'
        HtmlRenderer.new('any/format').format.must_equal 'text/html'
      end
    end

    describe '#render' do
      describe 'when the representation has no rows' do
        let(:rendered_view) { 'A portion of a view' }
        let(:view) { create :view, value: rendered_view }
        let(:views) { [view]}
        let(:representation) { create :representation, views: views }
        let(:favicon) { 'FAVICON' }
        let(:js) { 'JAVASCRIPTS' }
        let(:css) { 'STYLESHEETS' }
        let(:seo) { 'SEO' }
        let(:image_link) { 'IMAGE_LINK' }
        let(:additional_headers) { 'ADDITIONAL_HEADERS' }
        let(:extra_js) { 'EXTRA JAVASCRIPTS' }

        before do
          main_view_locals = {content: '&nbsp;', favicon: favicon, javascripts: js, stylesheets: css, seo: seo, image_link: image_link, additional_headers: additional_headers, extra_javascripts: extra_js}
          renderer.expects(:render_favicon).once.with(representation).returns(favicon)
          renderer.expects(:render_assets).once.with(representation, :javascript).returns(js)
          renderer.expects(:render_assets).once.with(representation, :stylesheet).returns(css)
          renderer.expects(:render_seo).once.returns(seo)
          renderer.expects(:render_image_link).once.returns(image_link)
          renderer.expects(:render_additional_headers).once.returns(additional_headers)
          renderer.expects(:render_extra_javascripts).once.with(representation).returns(extra_js)
          View.any_instance.expects(:render).once.with(main_view_locals).returns(rendered_view)
        end

        after do
          View.any_instance.unstub :render
          renderer.unstub :render_favicon, :render_assets
        end

        it 'renders the view with no content for it' do
          renderer.render(representation)
        end
      end

      describe 'when it has rows' do
        before do
          main_view = create :view, value: [View::FAVICON_PLACEHOLDER, View::STYLESHEETS_PLACEHOLDER, View::JAVASCRIPTS_PLACEHOLDER, View::CONTENT_PLACEHOLDER].join("\n")
          clean_view = create :view, value: View::CONTENT_PLACEHOLDER
          @representation = create :representation, view: main_view
          # Components & views
          component_1 = create :component
          component_2 = create :component
          component_3 = create :component
          component_1_view = create :view, component: component_1
          component_2_view = create :view, component: component_2
          component_3_view = create :view, component: component_3
          cover_component = create :cover_component
          cover_component_view = create :view, component: cover_component
          cover_articles_component = create :cover_articles_component
          cover_articles_component_view = create :view, component: cover_articles_component

          # Row 1 - 2 columns
          r1 = @representation.rows.create(order: 1, view: clean_view)
          r1c1 = r1.columns.create(order: 1, view: clean_view)
          r1c2 = r1.columns.create(order: 2, view: clean_view)
          r1c1cc2 = create :component_configuration, component: component_1, column: r1c1, order: 2, view: component_1_view
          r1c1cc1 = create :component_configuration, component: component_2, column: r1c1, order: 1, view: component_2_view
          r1c2cc1 = create :component_configuration, component: component_1, column: r1c2, order: 1, view: component_1_view
          r1c2cc2 = create :component_configuration, component: component_3, column: r1c2, order: 2, view: component_3_view

          # Row 2 - 1 column
          r2 = @representation.rows.create(order: 2, view: clean_view)
          r2c1 = r2.columns.create(order: 1, view: clean_view)
          r2c1cc1 = create :component_configuration, component: component_2, column: r2c1, order: 1, view: component_2_view
          r2c1cc2 = create :component_configuration, component: component_3, column: r2c1, order: 2, view: component_3_view

          # Row 3 - empty
          r3 = @representation.rows.create(order: 3, view: clean_view)

          # Row 4 - 1 column with cover components
          r4 = @representation.rows.create(order: 4, view: clean_view)
          r4c1 = r4.columns.create(order: 1, view: clean_view)
          r4c1cc1 = create :cover_component_configuration, component: cover_component, column: r4c1, order: 1, view: cover_component_view
          r4c1cc1cacc = create :cover_articles_component_configuration, component: cover_articles_component, cover_component_configuration: r4c1cc1, view: cover_articles_component_view
          r4c1cc2 = create :cover_component_configuration, component: cover_component, column: r4c1, order: 2, view: cover_component_view

          # Build the expected result string lines in the correct order
          lines = [
            '', # [[FAVICON]]
            "<%= stylesheet_tag \"#{@representation.slug}.css\" %>", # [[STYLESHEETS]]
            "<%= javascript_tag \"#{@representation.slug}.js\" %>", # [[JAVASCRIPTS]]
          ]
          ordered_components = [
            [r1c1cc1, component_2_view], [r1c1cc2, component_1_view], [r1c2cc1, component_1_view], [r1c2cc2, component_3_view],
            [r2c1cc1, component_2_view], [r2c1cc2, component_3_view],
          ]
          lines += ordered_components.map do |(cc, view)|
            %Q(<%= render_component '#{view.template_name}', _context.components[#{cc.id}] %>\n)
          end
          # Finally, add the cover component lines
          lines << <<-ERB
<% if _context.components[#{r4c1cc1.id}].components[#{r4c1cc1cacc.id}].try(:cover_articles).try :any? %>
  <%= render_component '#{cover_articles_component_view.template_name}', _context.components[#{r4c1cc1.id}].components[#{r4c1cc1cacc.id}] %>
<% end %>
          ERB
          lines << '&nbsp;' # For the empty cover component
          @expected_result = lines.join(GenericRenderer::GLUE)
        end

        it 'renders the main view with the component views in the correct order' do
          renderer.render(@representation).must_equal @expected_result
        end
      end
    end

    describe '#default_empty_string' do
      it 'overrides it with an HTML non-breaking space' do
        renderer.send(:default_empty_string).must_equal '&nbsp;'
      end
    end

    describe '#render_assets' do
      describe 'when the type is invalid' do
        let(:representation) { build :representation }

        it 'returns nil' do
          renderer.send(:render_assets, representation, :i_guess_this_wont_work).must_be :blank?
        end
      end

      describe 'when the type is valid' do
        let(:js_1) { '/js-1.js' }
        let(:js_2) { '/js-2.js' }
        let(:all_js) { [js_1, js_2] }
        let(:css) { '/styles.css' }
        let(:all_css) { [css] }
        let(:representation) { build :representation, configuration: {javascripts: all_js.join(','), stylesheets: all_css.join(',')} }

        it 'accepts :javascript and :stylesheet as valid types' do
          renderer.send(:render_assets, representation, :javascript).wont_be :blank?
          renderer.send(:render_assets, representation, :stylesheet).wont_be :blank?
        end

        describe 'when called with javascript' do
          before { renderer.expects(:javascripts).once.returns('some_js.js') }
          after { renderer.unstub(:javascripts) }

          it 'returns the ERB tags for a single unique js' do
            expected = '<%= javascript_tag "some_js.js" %>'
            renderer.send(:render_assets, representation, :javascript).must_equal expected
          end
        end

        describe 'when called with stylesheet' do
          before { renderer.expects(:stylesheets).once.returns('some_css.css') }
          after { renderer.unstub(:stylesheets) }

          it 'returns the ERB tags for a single unique css' do
            expected = '<%= stylesheet_tag "some_css.css" %>'
            renderer.send(:render_assets, representation, :stylesheet).must_equal expected
          end
        end
      end
    end

    describe '#render_favicon' do
      describe 'when a favicon has been configured' do
        describe 'when it is a .ico file' do
          let(:representation) { build :representation, configuration: {favicon: '/favicon.png'} }
          let(:favicon) { create :client_application_asset }

          before do
            representation.stubs(:favicon).returns(favicon)
            favicon.stubs(:relative_url).returns('image.ico')
            favicon.stubs(:content_type).returns('image/jpg')
          end

          it 'returns an HTML snippet for it' do
            expected = "<link rel=\"icon\" type=\"image/x-icon\" href=\"<%= image_path \"image.ico\" %>\"/>"
            renderer.send(:render_favicon, representation).must_equal expected
          end
        end

        describe "when it isn't a .ico file" do
          let(:representation) { build :representation, configuration: {favicon: '/favicon.png'} }
          let(:favicon) { create :client_application_asset }

          before do
            representation.stubs(:favicon).returns(favicon)
            favicon.stubs(:relative_url).returns('image.png')
            favicon.stubs(:content_type).returns('image/png')
          end

          it 'returns an HTML snippet for it' do
            expected = "<link rel=\"icon\" type=\"image/png\" href=\"<%= image_path \"image.png\" %>\"/>"
            renderer.send(:render_favicon, representation).must_equal expected
          end
        end
      end

      describe 'when no favicon has been configured' do
        let(:representation) { build :representation, configuration: {} }

        it 'returns an empty string' do
          renderer.send(:render_favicon, representation).must_be :blank?
        end
      end
    end

    describe '#render_extra_javascripts' do
      let(:representation) { build :representation, configuration: {} }

      it 'returns a string with enclosing tags <script></script>' do
        rendered = renderer.send(:render_extra_javascripts, representation)
        rendered.class.must_equal String
        rendered.gsub(/\s/, '').first(8).must_equal "<script>"
        rendered.gsub(/\s/, '').last(9).must_equal "</script>"
      end
    end
  end
end
