require 'test_helper'

class FindableInOrderTest < ActiveSupport::TestCase
  describe FindableInOrder do
    before do
      class ::Article
        include FindableInOrder
      end
    end

    describe '.find_ordered' do
      let(:ordered_articles) { @articles.shuffle }
      let(:ordered_ids) { ordered_articles.map(&:id) + [-1] } # Injects an invalid ID

      before do
        @articles = create_list :article, 4
      end

      it 'returns the objects in the expected order and ignores missing ones' do
        Article.find_ordered(ordered_ids).must_equal ordered_articles
      end
    end

    describe '.find_ordered_where' do
      let(:heading) { 'testing' }
      let(:ordered_articles) { @articles.shuffle }
      let(:conditions) { {slug: ordered_articles.map(&:slug) + ['nope'], heading: [heading, 'not a heading']} }

      before do
        @articles = create_list :article, 5, heading: heading
      end

      it 'returns the objects in the expected order and ignores missing ones' do
        Article.find_ordered_where(conditions).must_equal ordered_articles
      end
    end
  end
end