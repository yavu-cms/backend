require 'test_helper'

class Representations::BackupManagementTest < ActiveSupport::TestCase
  describe Representations::BackupManagement do
    describe '#has_backups?' do
      let(:representation) { create :representation }

      describe 'when no backup has been created' do
        it 'returns false' do
          representation.wont_be :has_backups?
        end
      end

      describe 'when at least one backup has been created' do
        before do
          representation.backups << create(:representation_backup, representation: representation)
        end

        it 'returns true' do
          representation.must_be :has_backups?
        end
      end
    end

    describe '#restore_backup!' do
      let(:client_application) { build :client_application }
      let(:representation) { build :representation, client_application: client_application }
      let(:backup) { stub }

      before do
        representation.expects(:replace_with).with(backup, destroy: true).once
        client_application.expects(:notify_clients).once
      end

      it 'replaces the representation with the given backup, destroying the former & notifying associated clients' do
        representation.restore_backup! backup
      end
    end
  end
end