require 'test_helper'

class Representations::DraftManagementTest < ActiveSupport::TestCase
  describe Representations::DraftManagement do
    describe '#has_draft?' do
      let(:representation) { create :representation }

      describe 'when no draft is associated' do
        it 'returns false' do
          representation.wont_be :has_draft?
        end
      end

      describe 'when a draft is associated' do
        before do
          representation.draft = build(:representation_draft)
        end

        it 'returns true' do
          representation.must_be :has_draft?
        end
      end
    end

    describe '#generate_draft' do
      let(:representation) { create :representation }
      let(:row_1) { create :row, order: 1, representation: representation }
      let(:row_2) { create :row, order: 2, representation: representation }
      let(:rows) { [row_1, row_2] }

      describe "when the representationd doesn't have a draft" do
        before do
          representation.expects(:copy_rows).returns(rows).once
        end

        it 'creates a new draft for the representation, copying the related elements and returns the persisted draft' do
          draft = representation.generate_draft
          draft.must_be_instance_of RepresentationDraft
          draft.must_be :persisted?
        end
      end

      describe 'when the representation has a draft already' do
        let(:representation) { create :representation }

        before do
          representation.draft = create :representation_draft, representation: representation,
                                        client_application: representation.client_application
          representation.expects(:copy_rows).never
        end

        it 'does nothing' do
          old_draft = representation.draft
          new_draft = representation.generate_draft
          new_draft.must_be_same_as old_draft
        end
      end
    end

    describe '#discard_draft' do
      describe 'when no draft exists' do
        let(:representation) { create :representation }

        it 'does nothing' do
          representation.discard_draft
          representation.draft.must_be_nil
        end
      end

      describe 'when the representation has a draft' do
        let(:representation) { create :representation }

        before do
          @draft = representation.generate_draft
          representation.reload
        end

        it 'destroys the draft' do
          representation.discard_draft
          representation.draft(true).must_be_nil
          -> { @draft.reload }.must_raise ActiveRecord::RecordNotFound
        end
      end
    end

    describe '#apply_draft!' do
      let(:client_application) { build :client_application }
      let(:representation) { build :representation, client_application: client_application }
      let(:draft) { build :representation_draft }

      describe 'when it has a draft' do
        before do
          representation.expects(:has_draft?).returns(true).once
          representation.expects(:draft).returns(draft).twice
          representation.expects(:replace_with).with(draft, become: 'RepresentationBackup').once
          client_application.expects(:notify_clients).once
        end

        it 'replaces the representation with the given draft, turning the former into a backup & notifying associated clients' do
          representation.apply_draft!
        end
      end

      describe "when it doesn't have a draft" do
        before do
          representation.expects(:has_draft?).returns(false).once
          representation.expects(:draft).never
          representation.expects(:replace_with).never
          client_application.expects(:notify_clients).never
        end

        it 'does nothing' do
          representation.apply_draft!
        end
      end
    end

    describe 'sidekiq job' do
      describe '#trigger_draft_creation' do
        before do
          @representation = create :representation
        end

        after do
          DraftsCreationWorker.unstub(:perform_async)
        end

        describe 'when the representation has a draft' do
          before do
            @representation.expects(:has_draft?).returns(true).once
            DraftsCreationWorker.expects(:perform_async).never
          end

          it 'does nothing' do
            @representation.send :trigger_draft_creation
          end
        end

        describe 'when the representation is being copied' do
          before do
            @representation.being_copied = true
            DraftsCreationWorker.expects(:perform_async).never
          end

          it 'does nothing' do
            @representation.send :trigger_draft_creation
          end
        end

        describe 'when the representation is being copied and already has a draft' do
          before do
            @representation.being_copied = true
            @representation.stubs(:has_draft?).returns(true)
            DraftsCreationWorker.expects(:perform_async).never
          end

          it 'does nothing' do
            @representation.send :trigger_draft_creation
          end
        end

        describe "when the representation isn't being copied and it doesn't have a draft" do
          before do
            @representation.expects(:has_draft?).returns(false).once
            DraftsCreationWorker.expects(:perform_async).with(@representation.id).once
          end

          it 'yields an async job to a DraftsCreationWorker' do
            @representation.send :trigger_draft_creation
          end
        end
      end
    end
  end
end