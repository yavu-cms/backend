require 'test_helper'

class Representations::FromJsonTest < ActiveSupport::TestCase
  describe Representations::FromJson do
    describe 'when the provided information is correctly structured' do
      describe 'and it is valid' do
        let(:sut) { create :representation }
        let(:row) { create :row, representation: sut, view: row_view }
        let(:row_view) { create :view, name: 'Some existing row view', value: '[[content]]' }
        let(:col) { create :column, row: row }
        let(:cc) { create :component_configuration, column: col, component: component }
        let(:component) { create :component }
        let(:other_component) { create :component }
        let(:view) { create :view, component: component, name: 'default' }
        let(:other_view) { create :view, component: other_component, name: 'default' }
        let(:configuration) do
          {
            'configuration' => {'some' => 'value'},
            'views' => {
              'layout' => '<html><body>[[content]]</body></html>',
              'default_row_view' => '<section>[[content]]</section>',
              'default_column_view' => '<div>[[content]]</div>'
            },
            'rows' => [
              {
                '_id' => row.id,
                'configuration' => {'a' => 'b'},
                'view' => {'row' => '<existing-row>[[content]]</existing-row>'},
                'columns' => [
                  {
                    '_id' => col.id,
                    'view' => {'column' => '<existing-column>[[content]]</existing-column>'},
                    'component_configurations' => [
                      {
                        '_id' => cc.id,
                        'description' => 'Existing component configuration',
                        'component_configuration' => {'view' => view.name}
                      },
                      {
                        'description' => 'A new component configuration in the existing row',
                        'component_id' => other_component.id,
                        'component_configuration' => {'view' => other_view.name}
                      }
                    ]
                  },
                  {
                    'view' => {'column' => '<new-column>[[content]]</new-column>'}
                  }
                ]
              },
              {
                '_id' => nil,
                'configuration' => {'a' => false},
                'view' => {'row' => ''},
                'columns' => [
                  {
                    '_id' => nil,
                    'view' => {'column' => ''},
                    'component_configurations' => [
                      {
                        '_id' => nil,
                        'component_id' => component.id,
                        'description' => 'New component configuration',
                        'component_configuration' => {'view' => view.name}
                      }
                    ]
                  }
                ]
              },
              {
                '_id' => nil,
                'columns' => []
              },
              {
                'columns' => [
                  {
                    'component_configurations' => []
                  }
                ]
              }
            ]
          }
        end
        let(:json_string) { Hash['representation' => configuration].to_json }

        it 'returns true after saving the changes' do
          sut.from_json(json_string).must_equal true
          sut.reload
          # Check elements count first (fail fast)
          sut.rows.count.must_equal 4
          sut.columns.count.must_equal 4
          sut.component_configurations.count.must_equal 3

          # Representation
          sut.view.value.must_equal '<html><body>[[content]]</body></html>'
          sut.default_row_view.value.must_equal '<section>[[content]]</section>'
          sut.default_column_view.value.must_equal '<div>[[content]]</div>'
          sut.configuration.must_equal some: 'value'

          row_1, row_2, row_3, row_4 = sut.rows
          # Row 1
          row_1.must_equal row
          row_1.configuration.must_equal 'a' => 'b'
          row_1.order.must_equal 0
          row_1.view.name.must_equal row_view.name
          row_1.view.value.must_equal '<existing-row>[[content]]</existing-row>'
          columns_1 = row_1.columns
          columns_1.count.must_equal 2
          column_1_1, column_1_2 = columns_1
          #   - Col 1
          column_1_1.must_equal col
          column_1_1.order.must_equal 0
          column_1_1.view.value.must_equal '<existing-column>[[content]]</existing-column>'
          ccs_1_1 = column_1_1.component_configurations
          ccs_1_1.count.must_equal 2
          cc_1_1_1, cc_1_1_2 = ccs_1_1
          #     - Component configuration 1
          cc_1_1_1.order.must_equal 0
          cc_1_1_1.component.must_equal component
          cc_1_1_1.description.must_equal 'Existing component configuration'
          cc_1_1_1.view.must_equal view
          #     - Component configuration 2
          cc_1_1_2.order.must_equal 1
          cc_1_1_2.component.must_equal other_component
          cc_1_1_2.description.must_equal 'A new component configuration in the existing row'
          cc_1_1_2.view.must_equal other_view
          #   - Col 2
          column_1_2.order.must_equal 1
          column_1_2.view.value.must_equal '<new-column>[[content]]</new-column>'
          column_1_2.component_configurations.must_be_empty

          # Row 2
          row_2.configuration.must_equal 'a' => false
          row_2.order.must_equal 1
          row_2.view.must_be_nil
          columns_2 = row_2.columns
          columns_2.count.must_equal 1
          column_2_1 = columns_2.first
          #   - Col 1
          column_2_1.order.must_equal 0
          column_2_1.view.must_be_nil
          ccs_2_1 = column_2_1.component_configurations
          ccs_2_1.count.must_equal 1
          cc_2_1_1 = ccs_2_1.first
          #     - Component configuration 1
          cc_2_1_1.order.must_equal 0
          cc_2_1_1.component.must_equal component
          cc_2_1_1.description.must_equal 'New component configuration'
          cc_2_1_1.view.must_equal view

          # Row 3
          row_3.configuration.must_equal({})
          row_3.order.must_equal 2
          row_3.view.must_be_nil
          row_3.columns.must_be_empty

          # Row 4
          row_4.configuration.must_equal({})
          row_4.order.must_equal 3
          row_4.view.must_be_nil
          row_4.columns.count.must_equal 1
          row_4.columns.first.view.must_be_nil
          row_4.columns.first.order.must_equal 0
          row_4.columns.first.component_configurations.must_be_empty
        end
      end

      describe "and it isn't valid" do
        let(:sut) { build :representation }
        let(:configuration) do
          {
            'configuration' => {},
            'views' => {
              'layout' => '<html><body>[[content]]</body></html>',
              'default_row_view' => '<section>[[content]]</section>',
              'default_column_view' => '<div>[[content]]</div>',
            }
          }
        end
        let(:json_string) { Hash['representation' => configuration].to_json }
        let(:error_stub) { stub(errors: stub(full_messages: ['invalid'])) }

        before do
          sut.expects(:process_config).with(configuration).once
          sut.expects(:valid?).returns(false).once
        end

        it 'returns false without saving anything' do
          sut.from_json(json_string).must_equal false
        end
      end
    end

    describe "when the provided information isn't correctly structured" do
      let(:sut) { build :representation }
      let(:json_string) { '{}' }

      before do
        sut.expects(:transaction).never
        @result = sut.from_json(json_string)
      end

      it 'returns false without processing anything' do
        @result.must_equal false
      end
    end

    describe '#parse_and_validate' do
      let(:sut) { build :representation }

      describe 'when the provided string is not valid json' do
        let(:json_string) { '' }

        before { sut.send :parse_and_validate, json_string }

        it 'adds a validation error on the :base attribute' do
          sut.errors[:base].must_include I18n.t('errors.messages.invalid')
        end
      end

      describe 'when the provided string is valid json' do
        describe "when it doesn't respect the expected structure" do
          let(:json_string) { '{}' }

          before { sut.send :parse_and_validate, json_string }

          it 'adds a validation error on the :base attribute' do
            sut.errors[:base].must_include I18n.t('errors.messages.invalid')
          end
        end

        describe 'when it respects the expected structure' do
          let(:json_string) do
            {
              representation: {
                configuration: {},
                views: {
                  layout: '<html><body>[[content]]</body></html>',
                  default_row_view: '<section>[[content]]</section>',
                  default_column_view: '<div>[[content]]</div>',
                }
              }
            }.to_json
          end

          before { @response = sut.send :parse_and_validate, json_string }

          it 'returns a hash from the JSON information' do
            @response.must_be_instance_of Hash
          end
        end
      end
    end
  end
end