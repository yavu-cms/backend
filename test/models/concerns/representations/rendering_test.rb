require 'test_helper'

class Representations::RenderingTest < ActiveSupport::TestCase
  describe Representations::Rendering do
    describe '#layout' do
      let(:representation) { create :representation }
      let(:renderer_stub) { stub }
      let(:expected) { 'rendered view' }

      before do
        representation.expects(:renderer).with(:html).returns(renderer_stub).once
        renderer_stub.expects(:render).with(representation).returns(expected).once
      end

      it 'renders the template using the view, assets, favicon and rows' do
        representation.send(:layout).must_equal expected
      end
    end

    describe '#translations' do
      describe 'when it has no associated component configurations' do
        let(:representation) { build :representation, rows: [] }

        it 'returns an empty hash' do
          representation.translations.must_equal({})
        end
      end

      describe 'when it has component configurations' do
        let(:representation)  { build :representation }
        let(:configuration_1) { mock.responds_like_instance_of ComponentConfiguration }
        let(:configuration_2) { mock.responds_like_instance_of ComponentConfiguration }
        let(:configuration_3) { mock.responds_like_instance_of ComponentConfiguration }
        let(:component_1)     { mock.responds_like_instance_of Component }
        let(:component_2)     { mock.responds_like_instance_of Component }
        let(:c1_translations) { {es: {hello: 'Hola'}, en: {hello: 'Hello'}} }
        let(:c2_translations) { {en: {hello: 'Hi!'}, it: {hello: 'Ciao'}} }
        let(:c3_translations) { {en: {hello: 'Hi!'}, it: {hello: 'Ciao'}} }

        before do
          configurations = [configuration_1, configuration_2, configuration_3]
          component_1.expects(:slug).returns('my_component_1').once
          component_2.expects(:slug).returns('my_component_2').twice
          configuration_1.expects(:translations).returns(c1_translations).once
          configuration_1.expects(:component).returns(component_1).once
          configuration_2.expects(:translations).returns(c2_translations).once
          configuration_2.expects(:component).returns(component_2).once
          configuration_3.expects(:translations).returns(c3_translations).once
          configuration_3.expects(:component).returns(component_2).once
          representation.expects(:component_configurations).returns(configurations).once
        end

        it 'returns the translations of the component configurations, merged' do
          expected = {
            'my_component_1' => { es: { hello: 'Hola' }, en: { hello: 'Hello' } },
            'my_component_2' => { en: { hello: 'Hi!'  }, it: { hello: 'Ciao'  } }
          }
          representation.translations.must_equal expected
        end
      end
    end

    describe '#templates' do
      let(:error_view) { build :view }
      let(:layout_mock) { '<Layout>' }

      describe 'when it has no associated component configurations' do
        let(:representation) { build :representation, rows: [], error_view: error_view }

        before do
          representation.expects(:layout).returns(layout_mock).once
        end

        it 'returns its templates only' do
          expected = {:main => layout_mock, :error => error_view.value, :partials => {}}
          representation.templates.must_equal expected
        end
      end

      describe 'when it has component configurations' do
        let(:representation) { build :representation, error_view: error_view }
        let(:configuration_1) { mock.responds_like_instance_of ComponentConfiguration }
        let(:configuration_2) { mock.responds_like_instance_of ComponentConfiguration }
        let(:c1_templates) { {'c1' => '<C1 VIEW>'} }
        let(:c2_templates) { {'c2' => '<C2 VIEW>'} }

        before do
          representation.expects(:layout).returns(layout_mock).once
          configuration_1.expects(:templates).returns(c1_templates).once
          configuration_2.expects(:templates).returns(c2_templates).once
          representation.expects(:component_configurations).returns([configuration_1, configuration_2]).once
        end

        it 'returns the translations of the component configurations, merged' do
          expected = {:main => layout_mock, :error => error_view.value, :partials => c1_templates.merge(c2_templates) }
          representation.templates.must_equal expected
        end
      end
    end

    describe '#renderer' do
      let(:format) { 'some/format+string' }
      let(:renderer_stub) { stub }
      let(:representation) { build :representation }

      before do
        GenericRenderer.expects(:build).once.with(format).returns(renderer_stub)
      end

      after do
        GenericRenderer.unstub :build
      end

      it 'builds a GenericRenderer with the given format' do
        representation.send(:renderer, format).must_be_same_as(renderer_stub)
      end
    end

    describe '#formats' do
      let(:rss_view) { create :view_with_placeholders, format: 'application/rss+xml' }
      let(:plain_view) { create :view_with_placeholders, format: 'text/plain' }
      let(:views) { [rss_view, plain_view] }
      let(:representation) { create :representation, views: views }

      describe 'when force_html is true' do
        it 'returns all the formats associated to the representation including the default (HTML)' do
          representation.formats.must_equal views.map(&:format) + ['text/html']
        end
      end

      describe 'when force_html is false' do
        it 'returns all the formats associated to the representation' do
          representation.formats(false).must_equal views.map(&:format)
        end
      end
    end

    describe '#views_by_format' do
      let(:rss_view) { create :view_with_placeholders, format: 'application/rss+xml' }
      let(:plain_view) { create :view_with_placeholders, format: 'text/plain' }
      let(:views) { [rss_view, plain_view] }
      let(:representation) { create :representation, views: views }
      let(:rendered_rss) { 'RSS' }
      let(:rendered_plain) { 'PLAIN' }
      let(:rendered_html) { 'HTML' }

      describe 'when force_html is true' do
        before do
          # RSS
          rss_renderer_stub = stub.responds_like_instance_of GenericRenderer
          representation.expects(:renderer).once.with(rss_view.format).returns(rss_renderer_stub)
          rss_renderer_stub.expects(:render).once.with(representation).returns(rendered_rss)
          # PLAIN
          plain_renderer_stub = stub.responds_like_instance_of GenericRenderer
          representation.expects(:renderer).once.with(plain_view.format).returns(plain_renderer_stub)
          plain_renderer_stub.expects(:render).once.with(representation).returns(rendered_plain)
          # HTML
          html_renderer_stub = stub.responds_like_instance_of GenericRenderer
          representation.expects(:renderer).once.with('text/html').returns(html_renderer_stub)
          html_renderer_stub.expects(:render).once.with(representation).returns(rendered_html)
        end

        it 'returns all the views grouped by format formats associated to the representation - including HTML' do
          expected = {rss_view.format => rendered_rss, plain_view.format => rendered_plain, 'text/html' => rendered_html}
          representation.views_by_format.must_equal expected
        end
      end

      describe 'when force_html is false' do
        before do
          # RSS
          rss_renderer_stub = stub.responds_like_instance_of GenericRenderer
          representation.expects(:renderer).once.with(rss_view.format).returns(rss_renderer_stub)
          rss_renderer_stub.expects(:render).once.with(representation).returns(rendered_rss)
          # PLAIN
          plain_renderer_stub = stub.responds_like_instance_of GenericRenderer
          representation.expects(:renderer).once.with(plain_view.format).returns(plain_renderer_stub)
          plain_renderer_stub.expects(:render).once.with(representation).returns(rendered_plain)
          representation.expects(:renderer).with('text/html').never
        end

        it 'returns all the views grouped by format formats associated to the representation - excluding HTML' do
          expected = {rss_view.format => rendered_rss, plain_view.format => rendered_plain}
          representation.views_by_format(false).must_equal expected
        end
      end
    end
  end
end