require 'test_helper'

class Representations::CoverFromJsonTest < ActiveSupport::TestCase
  describe Representations::CoverFromJson do
    describe '#cover_from_json' do
      describe 'when the provided information is correctly structured' do
        describe 'and it is valid' do
          let(:sut) { create :representation }
          let(:row_1) { create :row, representation: sut }
          let(:row_2) { create :row, representation: sut }
          let(:col_1) { create :column, row: row_1 }
          let(:col_2) { create :column, row: row_2 }
          let(:cover_component) { create :cover_component }
          let(:cover_component_configuration_1) { create :cover_component_configuration, component: cover_component, column: col_1 }
          let(:cover_component_configuration_2) { create :cover_component_configuration, component: cover_component, column: col_2 }
          let(:cac_1_view_1) { create :view, name: 'View 1 for CAC 1', component: cover_articles_component_1 }
          let(:cac_1_view_2) { create :view, name: 'View 2 for CAC 1', component: cover_articles_component_1 }
          let(:cover_articles_component_1) { create :cover_articles_component }
          let(:cac_2_view_1) { create :view, name: 'View 1 for CAC 2', component: cover_articles_component_2 }
          let(:cover_articles_component_2) { create :cover_articles_component }
          let(:cacc_1) { create :cover_articles_component_configuration, component: cover_articles_component_1, cover_component_configuration: cover_component_configuration_1 }
          let(:cacc_2) { create :cover_articles_component_configuration, component: cover_articles_component_2, cover_component_configuration: cover_component_configuration_1 }
          let(:configuration) do
            {
              cover_component_configuration_1.id.to_s => {
                'modules' => {
                  cacc_1.id.to_s => {
                    'type_id' => cover_articles_component_1.id,
                    'description' => 'Some description',
                    'order' => '0',
                    'view_id' => cac_1_view_1.id,
                    'articles' => [
                      {id: @articles[3].id, view_id: cac_1_view_1.id},
                      {id: @articles[1].id, view_id: cac_1_view_2.id},
                      {id: @articles[2].id, view_id: cac_1_view_1.id}
                    ]
                  }
                },
                'new' => {}
              },
              cover_component_configuration_2.id.to_s => {
                'modules' => {},
                'new' => {
                  0 => {
                    'type_id' => cover_articles_component_1.id,
                    'description' => 'NEW',
                    'order' => '0',
                    'view_id' => cac_1_view_1.id,
                    'articles' => [
                      {id: @articles[0].id, view_id: cac_1_view_2.id}
                    ]
                  }
                }
              }
            }
          end
          let(:json_string) { configuration.to_json }

          before do
            @articles = create_list :article, 5
            # Forces the creation of the whole structure and reloads the SUT
            cacc_2; json_string; sut.reload
          end

          it 'returns true after saving the changes' do
            sut.cover_from_json(json_string).must_equal true
            sut.reload

            sut.cover_component_configurations.must_equal [cover_component_configuration_1, cover_component_configuration_2]
            # First cover component
            cacc_1.reload
            cover_component_configuration_1.cover_articles_component_configurations.must_equal [cacc_1]
            cacc_1.articles.must_equal [@articles[3], @articles[1], @articles[2]]
            cacc_1.articles_cover_articles_component_configurations.first.article.must_equal @articles[3]
            cacc_1.articles_cover_articles_component_configurations.first.view.must_equal cac_1_view_1
            cacc_1.articles_cover_articles_component_configurations.second.article.must_equal @articles[1]
            cacc_1.articles_cover_articles_component_configurations.second.view.must_equal cac_1_view_2
            cacc_1.articles_cover_articles_component_configurations.third.article.must_equal @articles[2]
            cacc_1.articles_cover_articles_component_configurations.third.view.must_equal cac_1_view_1
            cacc_1.description.must_equal 'Some description'
            cacc_1.order.must_equal 0
            cacc_1.view.must_equal cac_1_view_1
            cacc_1.component.must_equal cover_articles_component_1
            # Second cover component (the new one)
            new_cacc = cover_component_configuration_2.cover_articles_component_configurations.first
            new_cacc.articles.must_equal [@articles[0]]
            new_cacc.description.must_equal 'NEW'
            new_cacc.order.must_equal 0
            new_cacc.view.must_equal cac_1_view_1
            new_cacc.component.must_equal cover_articles_component_1
            # The existing cover component - should be destroyed now
            ->{ cacc_2.reload }.must_raise ActiveRecord::RecordNotFound
          end
        end

        describe "and it isn't valid" do
          let(:sut) { build :representation }
          let(:configuration) { Hash.new }
          let(:json_string) { configuration.to_json }
          let(:error_stub) { stub(errors: stub(full_messages: ['invalid'])) }

          before do
            sut.expects(:process_cover_config).with(configuration).once
            sut.expects(:valid?).returns(false).once
          end

          it 'returns false without saving anything' do
            sut.cover_from_json(json_string).must_equal false
          end
        end
      end

      describe "when the provided information isn't correctly structured" do
        let(:sut) { build :representation }
        let(:json_string) { '' }

        before do
          sut.expects(:transaction).never
          @result = sut.cover_from_json(json_string)
        end

        it 'returns false without processing anything' do
          @result.must_equal false
        end
      end
    end

    describe '#parse_and_validate_cover' do
      let(:sut) { build :representation }

      describe 'when the provided string is not valid json' do
        let(:json_string) { '' }

        before { sut.send :parse_and_validate_cover, json_string }

        it 'adds a validation error on the :base attribute' do
          sut.errors[:base].must_include I18n.t('errors.messages.invalid')
        end
      end

      describe 'when the provided string is valid json' do
        let(:json_string) { '{}' }

        before { @response = sut.send :parse_and_validate_cover, json_string }

        it 'adds a validation error on the :base attribute' do
          @response.must_be_instance_of Hash
        end
      end
    end
  end
end