require 'test_helper'

class RenderableWithChildrenTest < ActiveSupport::TestCase
  describe RenderableWithChildren do
    before do
      class Includer
        # Nullify ActiveRecord relationship and validations
        def self.belongs_to(relation, *args); end
        def self.validate(validation); end

        def initialize(view: nil, order: 1, children: [], configuration: {})
          @view = view
          @order = order
          @children = children
          @configuration = configuration
        end
        include RenderableWithChildren

        # Emulates #children with behaves like ActiveRecord order scope
        def children
          @children.sort { |c1, c2| c1.order <=> c2.order }
        end

        def transaction(&block)
          yield block
        end

        def children=(new_children)
          @children = new_children.sort { |c1, c2| c1.order <=> c2.order }
        end

        def parent=(new_parent)
          @parent = new_parent
        end

        attr_accessor :view, :order, :configuration, :parent
      end
    end

    describe 'Validations' do
      describe 'when view is present' do
        before do
          @inc_wrong = Includer.new(view: View.new(value: ''))
          @inc_ok = Includer.new(view: View.new(value: View::CONTENT_PLACEHOLDER))
          errors = mock
          errors.stubs(:add)
          @inc_wrong.stubs(:errors).returns(errors)
        end
        it 'validates if view has a content placeholder' do
          @inc_wrong.send(:view_has_content_placeholder).must_equal false
          @inc_ok.send(:view_has_content_placeholder).must_be_nil
        end
      end
      describe 'otherwise' do
        before do
          @inc_ok = Includer.new
        end
        it 'doesnt validate if there is no view' do
          @inc_ok.send(:view_has_content_placeholder).must_be_nil
        end
      end
    end

    describe 'Copying' do
      describe '#copy' do
        before do
          view_with_content = build :view_with_placeholders
          children = [1, 2, 3, 4].map { |order| Includer.new view: view_with_content, order: order }

          @inc = Includer.new view: view_with_content, children: children
          @parent = Includer.new

          Includer.stubs(:create).returns(Includer.new(order: 1, configuration: {}))
          Includer.any_instance.stubs(:save!).returns(true)
          @copy = @inc.copy @parent
        end

        after do
          Includer.any_instance.unstub(:create)
          @inc.unstub(:save!)
        end

        it 'should create a copy (a different record) with the same primary values' do
          @copy.order.must_equal 1
          @copy.configuration.must_equal({})
          @copy.parent.must_equal @parent
        end

        it 'should setup different children' do
          [0..3].each {|i| @copy.children[i].wont_equal @inc.children[i]}
        end
      end
    end

    describe 'View access' do
      before do
        @view = create :view
        @includer_with_view    = Includer.new view: @view
        @includer_without_view = Includer.new
      end

      describe '#renderable_view' do
        describe 'when view is blank' do
          before do
            @default_view = build :view
            @includer_without_view.stubs(:default_view).returns(@default_view)
          end
          after do
            @includer_without_view.unstub(:default_view)
          end
          it 'returns the default view for this object' do
            @includer_without_view.renderable_view.must_equal @default_view
          end
        end
        describe 'otherwise' do
          it 'returns the associated view' do
            @includer_with_view.renderable_view.must_equal @view
          end
        end
      end

      describe '#update_view' do
        describe 'when value from param is blank' do
          it 'destroys the view' do
            View.count.must_equal 1
            @includer_with_view.update_view ''
            View.count.must_equal 0
          end
          it 'returns nil' do
            @includer_with_view.update_view('').must_be :nil?
          end
        end
        describe 'otherwise' do
          it 'replaces the view value' do
            view = @includer_with_view.get_view
            old_view_value = view.value
            @includer_with_view.update_view '<div>Replaced view</div>'
            @includer_with_view.get_view.must_equal view
            @includer_with_view.get_view.value.wont_equal old_view_value
          end
          it 'returns the view object' do
            @includer_with_view.update_view('<div>Replaced view</div>').must_equal @includer_with_view.get_view
          end
        end
      end

      describe '#get_view' do
        describe 'when includer has a view' do
          it 'returns that view' do
            @includer_with_view.get_view.must_equal @view
          end
        end
        describe 'otherwise' do
          it 'returns a temporal blank view' do
            new_view = @includer_without_view.get_view
            new_view.must_be :blank_value?
          end
        end
      end
    end
  end
end