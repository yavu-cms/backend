require 'test_helper'

class AssetTest < ActiveSupport::TestCase
  describe Asset do
    describe "Enumerize :type is related to AssetUploader.types" do
      before do
        @types = %w{a b c d}
        AssetUploader.stubs(:types).returns(@types)
        class Includer
          include Asset
        end
      end

      after { AssetUploader.unstub(:types) }

      it "must return AssetUploader.types" do
        Includer.enumerized_attributes[:type].values.must_equal @types
      end

      it 'must add predicates' do
        i = Includer.new
        @types.each {|x| i.must_respond_to "#{x}?".to_s}
      end
    end

    describe "#to_s" do
      it 'must call once file_url' do
        class Includer
          include Asset
        end
        i = Includer.new
        i.expects(:relative_url).returns('some text').once
        i.to_s.must_equal 'some text'
      end
    end

    describe '#relative_url' do
      describe 'must return a relative url of [file_url] from [file.store_dir_prefix]' do
        before do
          class Includer
            include Asset
            def file_url
              '/some/path/with/many/directories'
            end
          end
          @includer = Includer.new
        end

        it 'must return the relative path when store_dir_prefix starts with /' do
          object = mock.responds_like_instance_of(AssetUploader)
          object.expects(:store_dir_prefix).returns ('/some/path')
          @includer.expects(:file).returns(object)
          @includer.relative_url.must_equal 'with/many/directories'
        end

        it 'must return the relative path when store_dir_prefix dont starts with /' do
          object = mock.responds_like_instance_of(AssetUploader)
          object.expects(:store_dir_prefix).returns ('some/path')
          @includer.expects(:file).returns(object)
          @includer.relative_url.must_equal 'with/many/directories'
        end

        it 'must return the relative path when store_dir_prefix ends with /' do
          object = mock.responds_like_instance_of(AssetUploader)
          object.expects(:store_dir_prefix).returns ('some/path/')
          @includer.expects(:file).returns(object)
          @includer.relative_url.must_equal 'with/many/directories'
        end
      end
    end

    describe '#relative_url_without_extension' do
      before do
        class Includer
          include Asset
        end
        @includer = Includer.new
      end

      it 'must return the relative path when relative_url contains directory' do
        @includer.expects(:relative_url).returns('some_prefix/with_directory/file.extension')
        @includer.relative_url_without_extension.must_equal 'some_prefix/with_directory/file'
      end

      it 'must return the relative path when relative_url does not contain a directory' do
        @includer.expects(:relative_url).returns('file.extension')
        @includer.relative_url_without_extension.must_equal 'file'
      end
    end

    describe '#rename' do
      describe 'when new filename doesn\t exist' do
        before do
          # image_1.jpg
          @asset = create(:component_asset)
        end

        it 'changes this asset\'s file name' do
          @asset.rename('new_name')
          # remove component namespace from asset's name
          @asset.relative_url(false).must_equal 'new_name.jpg'
        end
      end

      describe 'when filename exists' do
        before do
          @component = create(:component)
          # image_1.jpg
          @asset_1 = create(:component_asset, component: @component)
          @asset_2 = create(:component_asset, component: @component, file: upload_test_file('image_2.jpg'))
        end

        it 'doesn\'t change the filename' do
          @asset_2.rename('image_1')
          ->{ @asset_2.save! }.must_raise ActiveRecord::RecordInvalid
        end
      end
    end

    describe 'asuming Includer class includes Asset concern' do
      before do
        class Includer
          include Asset
        end
      end

      describe "#editable?" do

        it 'must return true when javascript' do
          i = Includer.new
          i.type = 'javascript'
          i.must_be :editable?
        end

        it 'must return true when stylesheet' do
          i = Includer.new
          i.type = 'stylesheet'
          i.must_be :editable?
        end

        it 'must return false when font' do
          i = Includer.new
          i.type = 'font'
          i.wont_be :editable?
        end

        it 'must return false when image' do
          i = Includer.new
          i.type = 'image'
          i.wont_be :editable?
        end

        it 'must return false when not set' do
          i = Includer.new
          i.wont_be :editable?
        end

      end

      describe "#to_s" do
        let(:file_url) { 'some url' }

        it 'must return model file_url' do
          i = Includer.new
          i.expects(:relative_url).returns(file_url)
          i.to_s.must_equal file_url
        end
      end

      describe "#content" do

        describe 'when compiled is false' do
          it 'succeeds when file exists' do
            filename = Rails.root.join('tmp','test.content')
            content = 'This is a test content'
            File.open(filename, 'w') { |x| x.write(content) }

            file_exists = Includer.new
            file_exists.stubs(:file).returns(stub(current_path: filename))

            file_exists.content.must_equal content

            File.unlink filename
          end

          it 'returns empty string when file does not exists' do
            file_not_exists = Includer.new
            file_not_exists.stubs(:file).returns(stub(current_path: 'not-exists'))

            file_not_exists.content.must_be_empty
          end
        end

        describe 'when compiled is true' do
          it 'must call assets_configuration' do
            includer = Includer.new
            includer.expects(:assets_configuration).once
            includer.content(true)
          end
          it 'must call sprockets_environment'  do
            includer = Includer.new
            includer.expects(:sprockets_environment).once
            includer.content(true)
          end
          it 'must call configure_sprockets_helpers' do
            includer = Includer.new
            includer.expects(:configure_sprockets_helpers).once
            includer.content(true)
          end
          it 'must catch exceptions and return error message' do
            includer = Includer.new
            includer.expects(:configure_sprockets_helpers).once.raises(RuntimeError, 'some text')
            includer.content(true).must_equal 'some text'
          end
        end
      end

      describe '#sprockets_environment' do
        it 'must call Frontend::Renderer::AssetConfigurationFactory.sprockets_environment' do
          includer = Includer.new
          Yavu::Frontend::Util::AssetConfigurationFactory.expects(:sprockets_environment).once
          includer.sprockets_environment nil
          Yavu::Frontend::Util::AssetConfigurationFactory.unstub(:sprockets_environment)
        end
      end

      describe '#configure_sprockets_helpers' do
        it 'must call Frontend::Renderer::AssetConfigurationFactory.configure_sprockets_helpers' do
          includer = Includer.new
          Yavu::Frontend::Util::AssetConfigurationFactory.expects(:configure_sprockets_helpers).once
          includer.configure_sprockets_helpers nil, nil
          Yavu::Frontend::Util::AssetConfigurationFactory.unstub(:configure_sprockets_helpers)
        end
      end

      describe '#assets_configuration' do
        it 'must call ClientApplication.assets_configuration with prefix: :client_identifier' do
          includer = Includer.new
          ClientApplication.expects(:assets_configuration).once.with(prefix: ':client_identifier')
          includer.assets_configuration
          ClientApplication.unstub(:assets_configuration)
        end
      end

      describe "#update_content!" do
        describe 'when asset is editable' do
          it 'must update existing file' do
            filename = Rails.root.join('tmp','test.content')
            File.open(filename, 'w') { |x| x.write('hello') }

            file_exists = Includer.new
            file_exists.type = 'javascript'
            file_exists.stubs(:file).returns(stub(current_path: filename))
            file_exists.expects(:update).with({}).once

            content = 'This is a test content'
            file_exists.update_content! content

            IO.read(filename).must_equal content

            File.unlink filename

          end
        end

        describe 'when asset is not editable' do
          it 'must fail' do
            proc do
              i = Includer.new
              i.update_content! 'new content'
            end.must_raise RuntimeError
          end
        end
      end
    end
  end
end
