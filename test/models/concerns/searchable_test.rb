require 'test_helper'

class SearchableTest < ActiveSupport::TestCase
  describe Searchable do
    describe 'when included' do
      let(:expected_index_name) { 'es-yavu' }

      before do
        @includer = Class.new

        es_stub = stub
        YavuSettings.expects(:elasticsearch).returns(es_stub).times(3)
        es_stub.expects(:index_name).returns(expected_index_name).once
        es_stub.expects(:default_settings).returns({index: {}}).twice

        @includer.expects(:after_commit).with(:index_document_callback, on: :create)
        @includer.expects(:after_commit).with(:update_document_callback, on: :update)
        @includer.expects(:after_commit).with(:delete_document_callback, on: :destroy)
        @includer.expects(:after_touch).with(:update_document_callback)
      end

      it 'defines callbacks' do
        @includer.send :include, Searchable
      end

      it 'includes Elasticsearch::Model' do
        @includer.send :include, Searchable

        @includer.included_modules.must_include Elasticsearch::Model
      end

      it 'takes index_name from the configuration' do
        @includer.send :include, Searchable

        @includer.index_name.must_equal expected_index_name
      end
    end

    describe '.es_search' do
      let(:query) { 'some query string' }
      let(:expected_response) { stub }

      before do
        @includer = Class.new do
          class << self
            define_method(:after_commit) { |callback, options| }
            define_method(:after_touch) { |callback| }
          end
          include Searchable
        end

        @includer.__elasticsearch__.expects(:search).with(query).returns(expected_response).once
      end

      it 'calls .search on the elasticsearch proxy' do
        @includer.es_search(query).must_equal expected_response
      end
    end

    describe '#indexable?' do
      before do
        @includer = Class.new do
          class << self
            define_method(:after_commit) { |callback, options| }
            define_method(:after_touch) { |callback| }
          end
          include Searchable
        end
      end

      it 'returns true by default' do
        @includer.new.must_be :indexable?
      end
    end

    describe 'Callbacks' do
      before do
        class Includer
          class << self
            define_method(:after_commit) { |callback, options| }
            define_method(:after_touch) { |callback| }
          end
          def id; 1; end

          include Searchable
        end
        @includer = Includer
        @instance = @includer.new
      end

      after do
        ElasticsearchIndexer.unstub(:perform_async)
      end

      describe '#index_document_callback' do
        describe 'when the object is #indexable?' do
          before do
            @instance.expects(:indexable?).returns(true).once
            ElasticsearchIndexer.expects(:perform_async).with(:index, 'SearchableTest::Includer', 1).once
          end

          it 'fires the indexing worker for it' do
            @instance.send :index_document_callback
          end
        end

        describe 'when the object is not #indexable?' do
          before do
            @instance.expects(:indexable?).returns(false).once
            ElasticsearchIndexer.expects(:perform_async).never
          end

          it "doesn't fire the indexing worker" do
            @instance.send :index_document_callback
          end
        end
      end

      describe '#update_document_callback' do
        describe 'when the object is #indexable?' do
          before do
            @instance.expects(:indexable?).returns(true).once
            ElasticsearchIndexer.expects(:perform_async).with(:update, 'SearchableTest::Includer', 1).once
          end

          it 'fires the indexing worker for it to update it' do
            @instance.send :update_document_callback
          end
        end

        describe 'when the object is not #indexable?' do
          before do
            @instance.expects(:indexable?).returns(false).once
            ElasticsearchIndexer.expects(:perform_async).with(:delete, 'SearchableTest::Includer', 1).once
          end

          it 'removes the document from the index' do
            @instance.send :update_document_callback
          end
        end
      end

      describe '#delete_document_callback' do
        before do
          ElasticsearchIndexer.expects(:perform_async).with(:delete, 'SearchableTest::Includer', 1).once
        end

        it 'removes the document from the index' do
          @instance.send :delete_document_callback
        end
      end
    end
  end
end