require 'test_helper'

class TaggableTest < ActiveSupport::TestCase
  class Includer
    class << self
    end

    def self.has_and_belongs_to_many(relation); end

    def initialize(tags: [])
      @tags = tags

    end

    include Taggable

    def tags=(new_tags)
      @tags = new_tags
    end

    def tags
      @tags
    end
  end

  describe Taggable do
    let(:tags) { [(create :tag, name: 'tag1'),
                  (create :tag, name: 'tag2'),
                  (create :tag, name: 'tag3')]}

    before do
      @includer = Includer.new(tags: tags)
      @includer.tags.stubs(:pluck).with(:name).returns(@includer.tags.map(&:name))
    end

    after do
      @includer.tags.unstub(:pluck)
    end


    describe '#seralized_tags' do
      it 'returns associated tags separated with a coma' do
        @includer.serialized_tags.must_equal "tag1,tag2,tag3"
      end
    end

    describe '#serialized_tags=' do
      before do
        @combiner_tag = create :tag, name: 'Buenos Aires'
        create :tag, name: 'buenos aires', combiner: @combiner_tag
        create :tag, name: 'buenos Aires', combiner: @combiner_tag
      end

      it 'assigns a collection of tags' do
        @includer.serialized_tags = "Buenos Aires,buenos aires,buenos Aires,tag1,tag2,faketag"
        @includer.tags.must_equal %w(Buenos\ Aires tag1 tag2 faketag).map {|name| Tag.find_by(name: name)  }
      end
    end
  end
end
