require 'test_helper'

class SectionTest < ActiveSupport::TestCase
  describe Section do
    describe '.but' do
      let(:excluded) { create :section, name: 'Excluded' }
      let(:another_section) { create :section, name: 'Not excluded 1' }
      let(:yet_another_section) { create :section, name: 'Not excluded 2' }
      before do
        @all_sections = [excluded, another_section, yet_another_section]
      end

      describe 'when a section is provided' do
        it 'returns all sections except the one passed in' do
          Section.but(excluded).must_equal @all_sections - [excluded]
        end
      end

      describe 'when nil is provided' do
        it 'returns all sections' do
          Section.but(nil).must_equal @all_sections
        end
      end
    end

    describe '.roots' do
      before do
        create :section
        create :child_section
        create :child_child_section
        create :another_child_section
      end

      it 'returns sections with no parent section' do
        Section.roots.must_equal Section.where(parent_id: nil)
      end
    end

    describe '.visible' do
      before do
        create :section, is_visible: true
        create :child_section, is_visible: false
        create :child_child_section, is_visible: false
        create :another_child_section, is_visible: false
      end

      it 'returns sections which are visible' do
        Section.visible.must_equal Section.where(is_visible: true)
      end
    end

    describe '.search' do
      before do
        @a = create :section, name: 'Section A'
        @b = create :section, name: 'Section B'
        @c = create :section, name: 'Section C'
        @d = create :section, name: 'Section D'
      end

      describe 'when no query is specified' do
        it 'returns all the sections' do
          Section.search.must_equal Section.where(nil)
        end
      end

      describe 'when a query is specified' do
        it 'returns the matching sections' do
          Section.search(query: 'B').must_equal [@b]
        end
      end
    end

    describe '#visible?' do
      it 'must be an alias of #_is_visible?' do
        edition = Section.new
        edition.expects(:is_visible?)
        edition.visible?
      end
    end


    describe '#to_s' do
      let(:news_source) { create :news_source, name: 'Ftastic NS' }
      let(:supplement) { create :supplement, name: 'Supplement', news_source: news_source }
      let(:section) { create :section, name: 'Section One', supplement: supplement }

      it 'returns a valid string representation (name + slug)' do
        section.to_s.must_equal 'Section One (section-one) [Ftastic NS]'
      end
    end

    describe '#root?' do
      let(:section) { create :section }
      let(:child_section) { create :child_section }

      describe 'when the section is a root one' do
        it 'returns true' do
          section.must_be :root?
        end
      end

      describe 'when the section is not a root one' do
        it 'returns false' do
          child_section.wont_be :root?
        end
      end
    end

    describe '#set_slug' do
      describe "when there's no slug" do
        it 'creates one from the value of name' do
          create(:slug_section).slug.must_equal 'slug-section'
        end
      end

      describe 'when there is a slug' do
        it "doesn't change the slug" do
          create(:slug_section, { slug: 'slug' }).slug.must_equal 'slug'
        end
      end

      it 'fails with same slug (same news_source)' do
        supplement    = create :supplement
        section       = Section.create name: 'slug', supplement_id: supplement.id
        other_section = Section.create name: 'slug', supplement_id: supplement.id

        section.news_source.must_equal other_section.news_source
        section.must_be :valid?
        other_section.wont_be :valid?
      end

      it 'passes with same slug (same news_source)' do
        supplement_1    = create :supplement
        supplement_2    = create :supplement
        section         = Section.create name: 'slug', supplement_id: supplement_1.id
        other_section   = Section.create name: 'slug', supplement_id: supplement_2.id

        section.news_source.wont_equal other_section.news_source
        section.must_be :valid?
        other_section.must_be :valid?
      end
    end

    describe '#offspring' do
      it 'recursively collects the children sections and returns a unique set including this section (the parent)'
    end

    describe 'Validations' do
      describe 'name' do
        it 'must have a name' do
          section = build(:section, name: nil)
          section.wont_be :valid?
          section.errors.must_include :name
        end
      end

      describe 'parent/children' do
        it 'forbids a section to be its own parent' do
          section = create :section
          section.parent = section
          section.wont_be :valid?
          section.errors.must_include :parent
        end
      end
    end

    describe 'Destruction' do
      describe 'when it is referenced by a component value' do
        let(:component_value) { create :component_value_section, component_field: create(:component_field_model_section, with_component: true) }
        let(:section) { component_value.referable }

        it "can't be destroyed" do
          section.destroy.must_equal false
          section.wont_be :destroyed?
        end
      end

      describe "when it isn't referenced by a component value" do
        let(:section) { create :section }

        it 'can be destroyed' do
          section.destroy
          section.must_be :destroyed?
          ->{ section.reload }.must_raise ActiveRecord::RecordNotFound
        end
      end
    end

    describe 'Callbacks' do
      describe 'after updated' do
        let(:section) { create :section }
        let(:articles) { create_list :article, 4, section: section, supplement: section.supplement }

        before do
          section.articles = articles
        end

        describe 'when the visibility of the section has changed' do
          before do
            section.expects(:touch_articles).once
          end

          it 'touches every related article' do
            section.update(name: 'Another test', is_visible: !section.is_visible)
          end
        end
      end
    end

    describe 'scopes' do
      describe 'default' do
        it 'must be ordered by name' do
          Section.expects(:order).with(:name).once
          Section.all
          Section.unstub(:order)
        end
      end

      describe 'with_supplement' do
        it 'must be eager_load :supplement' do
          Section.expects(:eager_load).with(:supplement).once
          Section.with_supplement
          Section.unstub(:eager_load)
        end
      end

      describe 'visible' do
        it 'must filter visible sections only' do
          Section.expects(:where).with(is_visible: true).once
          Section.visible
          Section.unstub(:where)
        end
      end
    end
  end
end