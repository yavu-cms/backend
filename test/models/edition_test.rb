require 'test_helper'

class EditionTest < ActiveSupport::TestCase
  describe Edition do
    describe '.search' do
      let(:match) { create :edition, name: 'Some Edition', date: Date.new(2000, 1, 1) }
      let(:other_match) { create :edition, name: 'Some other Edition', date: Date.new(2001, 1, 1) }
      let(:no_match) { create :edition, name: 'No match', date: Date.new(2001, 1, 1) }

      it 'searches for editions by their names and sorts results by date from newer to older' do
        Edition.search(query: 'Edition').must_equal [other_match, match]
      end

      it 'returns an empty set when no query is specified' do
        Edition.search.must_be_empty
      end
    end

    describe '.find_or_active' do
      let(:supplement) { create :supplement }
      let(:fallback_edition) { create :edition, is_active: true, supplement: supplement }

      describe 'when the edition exists' do
        before do
          @edition = create(:edition)
        end

        it 'returns the edition with the given id' do
          Edition.find_or_active(@edition.id, supplement).must_equal @edition
        end
      end

      describe "when the edition doesn't exist" do
        before do
          supplement.expects(:active_edition).returns(fallback_edition).once
        end

        it 'returns the active edition for the given supplement' do
          Edition.find_or_active(nil, supplement).must_equal fallback_edition
        end
      end
    end

    describe '#to_s' do
      let(:supplement) { create :supplement }

      describe 'when it has a name set' do
        let(:edition) { create :edition, name: 'Edition', supplement: supplement }

        it 'returns supplement and edition name' do
          edition.to_s.must_equal "#{supplement} - #{edition.name} (#{supplement.news_source})"
        end
      end

      describe "when it doesn't have a date set" do
        let(:edition) { build :edition, name: nil, supplement: supplement, date: Date.today }
        let(:date_with_format) { '2001-02-03' }

        before do
          edition.expects(:date_with_format).returns(date_with_format).once
        end

        it 'returns supplement name and edition date' do
          edition.to_s.must_equal "#{supplement} - #{date_with_format} (#{supplement.news_source})"
        end
      end
    end

    describe 'Validations' do
      describe 'name and date presence' do
        it 'fails when neither the name nor the date are set' do
          edition = build(:edition, name: nil, date: nil)
          edition.wont_be :valid?
          edition.errors[:name].must_include I18n.t('activerecord.errors.models.edition.name_or_date_blank')
        end

        it 'succeeds when at least one of the name and date attributes is present' do
          # When only +name+ is present
          edition = build(:edition, name: 'Edition', date: nil)
          edition.must_be :valid?
          # When only +date+ is present
          edition = build(:edition, name: nil, date: Date.today)
          edition.must_be :valid?
        end

        it 'succeeds when both the name and date attributes are set' do
          edition = build(:edition, name: 'Edition', date: Date.today)
          edition.must_be :valid?
        end
      end

      describe 'date format' do
        let(:edition) { build :edition }

        it "doesn't validate the date format when date is blank" do
          edition.date_string = ' '
          edition.valid?
          edition.errors.wont_include :date
        end

        it 'validates the format of date when it is not blank' do
          edition.date_string = 'INVALID DATE'
          edition.wont_be :valid?
          edition.errors[:date].must_include I18n.t('activerecord.errors.dates.format')
        end

        it 'validates the format of date to match %F (%Y-%m-%d)' do
          edition.date_string = '2011-12-13'
          edition.valid?
          edition.errors.wont_include :date
        end

        it 'validates the format of date is an existing date' do
          edition.date_string = '2014-25-12'
          edition.wont_be :valid?
          edition.errors[:date].must_include I18n.t('activerecord.errors.dates.format')
        end

        it 'validates the format of date when it is dd/mm/yyyy' do
          edition.date_string = '20-03-2014'
          edition.valid?
          edition.errors.wont_include :date
        end
      end
    end

    describe '#become_active!' do
      let(:edition) { build(:edition) }

      it 'sets the edition as active within its supplement' do
        edition.become_active!
        edition.supplement.active_edition.must_equal edition
      end

      it "doesn't deactivate an already active edition" do
        edition.become_active!
        edition.become_active!
        edition.supplement.active_edition.must_equal edition
      end
    end

    describe '#become_inactive!' do
      let(:supplement) { build :supplement }
      let(:edition) { build(:edition, supplement: supplement) }

      before do
        edition.become_active!
      end

      it 'makes an edition become inactive' do
        edition.become_inactive!
        edition.wont_be :is_active
        edition.supplement.active_edition.must_be_nil
      end

      it "doesn't activate an already deactivated edition" do
        edition.become_inactive!
        edition.become_inactive!
        edition.wont_be :is_active
        edition.supplement.active_edition.must_be_nil
      end

    end

    describe '#date_with_format' do
      let(:stub_date) { Date.today }
      let(:stub_response) { stub_date.strftime '%F' }
      let(:edition) { create :edition, date: stub_date }

      after do
        I18n.unstub(:l)
      end

      it 'returns a formatted date, using by default the :long format' do
        format = :long
        I18n.expects(:l).with(stub_date, format: format).returns(stub_response).once

        edition.date_with_format.must_equal stub_response
      end

      it 'allows specifying an alternate date format' do
        format = :short
        I18n.expects(:l).with(stub_date, format: format).returns(stub_response).once

        edition.date_with_format(format).must_equal stub_response
      end

      it 'is fault tolerant' do
        format = :non_existing_format
        I18n.expects(:l).with(stub_date, format: format).raises(TypeError).once

        ->{ edition.date_with_format format }.must_be_silent
      end
    end

    describe '.by_name_or_date' do
      before do
        @edition = create(:edition, { name: nil,  date: Date.today, is_visible: true })
        @edition_2 = create(:edition, { name: 'edition',  date: Date.today, is_visible: false })
      end

      describe 'when edition is visible' do
        it 'returns a visible edition' do
          Edition.by_name_or_date(Date.today).must_equal [@edition]
        end
        it 'returns an empty array' do
          Edition.by_name_or_date('edition').must_be_empty
        end
      end
    end

    describe '.visible' do
      it 'must get call where is_visible is true' do
        Edition.expects(:where).with(is_visible: true).once
        Edition.visible
        Edition.unstub(:where)
      end
    end

    describe '#visible?' do
      it 'must be an alias of #_is_visible?' do
        edition = Edition.new
        edition.expects(:is_visible?)
        edition.visible?
      end
    end

    describe 'Callbacks' do
      describe '#touch_articles' do
        before do
          articles = %W(article_1 article_2 article_3).map { |a| build :article, title: a }
          @edition = build :edition, articles: articles
        end

        it 'calls find_each in articles relation' do
          @edition.articles.expects(:find_each).once
          @edition.send(:touch_articles)
        end
      end

      describe 'after update' do
        let(:edition) { create :edition }

        describe 'when is_visible has changed' do
          before do
            edition.expects(:touch_articles).once
          end

          it 'triggers #touch_articles after an edition is updated' do
            edition.name = 'New edition name'
            edition.is_visible = !edition.is_visible
            edition.save
          end
        end

        describe 'when is_visible has changed' do
          before do
            edition.expects(:touch_articles).never
          end

          it 'triggers #touch_articles after an edition is updated' do
            edition.name = 'New edition name'
            edition.save
          end
        end
      end
    end
  end
end