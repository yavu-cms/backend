require 'test_helper'
require 'support/permissions_test_case'

class RolePermissionsTest < PermissionsTestCase
  describe RolePermissions do
    describe 'role/read' do
      describe 'when absent' do
        let(:permissions) { [] }

        it 'forbids from reading' do
          wont_be_able_to :read, Role
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ role/read ] }

        it 'allows reading' do
          must_be_able_to :read, Role
        end
      end
    end

    describe 'role/create' do
      describe 'when absent' do
        let(:permissions) { [] }

        it 'forbids from creating roles' do
          wont_be_able_to :create, Role
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ role/create ] }

        it 'allows creating roles' do
          must_be_able_to :create, Role
        end
      end
    end

    describe 'role/update' do
      describe 'when absent' do
        let(:permissions) { [] }

        it 'forbids from editing roles' do
          wont_be_able_to :update, Role
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ role/update ] }

        it 'allows editing roles' do
          must_be_able_to :update, Role
        end
      end
    end

    describe 'role/destroy' do
      describe 'when absent' do
        let(:permissions) { [] }

        it 'forbids from destroying roles' do
          wont_be_able_to :destroy, Role
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ role/destroy ] }

        it 'allows destroying roles' do
          must_be_able_to :destroy, Role
        end
      end
    end
  end
end