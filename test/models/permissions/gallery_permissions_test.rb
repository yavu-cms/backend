require 'test_helper'
require 'support/permissions_test_case'

class GalleryPermissionsTest < PermissionsTestCase
  describe GalleryPermissions do
    let(:component) { component_value.component }

    describe 'gallery/read' do
      describe 'when absent' do
        let(:permissions) { [] }

        it 'forbids from reading' do
          wont_be_able_to :read, Gallery
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ gallery/read ] }

        describe 'for an unused gallery' do
          it 'allows reading' do
            must_be_able_to :read, Gallery
          end
        end
      end
    end

    describe 'gallery/create' do
      describe 'when absent' do
        let(:permissions) { [] }

        it 'forbids from creating' do
          wont_be_able_to :create, Gallery
          wont_be_able_to :new_media, Gallery
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ gallery/create ] }

        it 'allows creating' do
          must_be_able_to :create, Gallery
          must_be_able_to :new_media, Gallery
        end
      end
    end

    describe 'gallery/update/unused' do
      let(:component_value) { create :component_value_gallery, with_component: true }
      let(:unused_gallery) { create :media_gallery }
      let(:gallery_in_use) { component_value.referable }

      describe 'when absent' do
        let(:permissions) { [] }

        it 'forbids from editing any gallery' do
          wont_be_able_to :update, unused_gallery
          wont_be_able_to :update, gallery_in_use
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ gallery/update/unused ] }

        it 'allows editing unused galleries' do
          must_be_able_to :update, unused_gallery
        end

        describe 'even if the user can update associated elements' do
          before do
            pretend_user_can :update, component, ability
          end

          it 'forbids from updating a gallery being used' do
            wont_be_able_to :update, gallery_in_use
          end
        end
      end
    end

    describe 'gallery/update/all' do
      let(:component_value) { create :component_value_gallery, with_component: true }
      let(:unused_gallery) { create :media_gallery }
      let(:gallery_in_use) { component_value.referable }

      describe 'when absent' do
        let(:permissions) { [] }

        it 'forbids from editing any gallery' do
          wont_be_able_to :update, unused_gallery
          wont_be_able_to :update, gallery_in_use
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ gallery/update/all ] }

        it 'allows editing unused galleries' do
          must_be_able_to :update, unused_gallery
        end

        describe 'and the user can update all associated elements' do
          before do
            pretend_user_can :update, component, ability
          end

          it 'allows editing a gallery being used' do
            must_be_able_to :update, gallery_in_use
          end
        end

        describe 'and the user cannot update any of the associated elements' do
          before do
            pretend_user_cannot :update, component, ability
          end

          it 'forbids from updating a gallery being used' do
            wont_be_able_to :update, gallery_in_use
          end
        end

        describe 'and the user cannot update at least one of the associated elements' do
          let(:another_component_value) { create :component_value_gallery, with_component: true, referable: gallery_in_use }
          let(:another_component) { another_component_value.component }

          before do
            pretend_user_cannot :update, another_component, ability
            pretend_user_can :update, component, ability
          end

          it 'forbids from updating a gallery being used' do
            wont_be_able_to :update, gallery_in_use
          end
        end
      end
    end

    describe 'gallery/destroy/unused' do
      let(:component_value) { create :component_value_gallery, with_component: true }
      let(:unused_gallery) { create :media_gallery }
      let(:gallery_in_use) { component_value.referable }

      describe 'when absent' do
        let(:permissions) { [] }

        it 'forbids from destroying any gallery' do
          wont_be_able_to :destroy, unused_gallery
          wont_be_able_to :destroy, gallery_in_use
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ gallery/destroy/unused ] }

        it 'allows destroying unused galleries' do
          must_be_able_to :destroy, unused_gallery
        end

        describe 'even if the user can update associated elements' do
          before do
            pretend_user_can :update, component, ability
          end

          it 'forbids from updating a gallery being used' do
            wont_be_able_to :destroy, gallery_in_use
          end
        end
      end
    end

    describe 'gallery/destroy/all' do
      let(:component_value) { create :component_value_gallery, with_component: true }
      let(:unused_gallery) { create :media_gallery }
      let(:gallery_in_use) { component_value.referable }

      describe 'when absent' do
        let(:permissions) { [] }

        it 'forbids from destroying any gallery' do
          wont_be_able_to :destroy, unused_gallery
          wont_be_able_to :destroy, gallery_in_use
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ gallery/destroy/all ] }

        it 'allows destroying unused galleries' do
          must_be_able_to :destroy, unused_gallery
        end

        describe 'and the user can update all associated elements' do
          before do
            pretend_user_can :update, component, ability
          end

          it 'allows destroying a gallery being used' do
            must_be_able_to :destroy, gallery_in_use
          end
        end

        describe 'and the user cannot update any of the associated elements' do
          before do
            pretend_user_cannot :update, component, ability
          end

          it 'forbids from destroying a gallery being used' do
            wont_be_able_to :destroy, gallery_in_use
          end
        end

        describe 'and the user cannot update at least one of the associated elements' do
          let(:another_component_value) { create :component_value_gallery, with_component: true, referable: gallery_in_use }
          let(:another_component) { another_component_value.component }

          before do
            pretend_user_cannot :update, another_component, ability
            pretend_user_can :update, component, ability
          end

          it 'forbids from destroying a gallery being used' do
            wont_be_able_to :destroy, gallery_in_use
          end
        end
      end
    end
  end
end