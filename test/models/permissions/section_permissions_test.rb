require 'test_helper'
require 'support/permissions_test_case'

class SectionPermissionsTest < PermissionsTestCase
  describe SectionPermissions do
    let(:associated_section) { create :section, name: 'Associated section' }
    let(:dissociate_section) { create :section, name: 'Dissociate section' }
    let(:child_associated_section) { create :section, name: 'Child from Associated section', parent: associated_section }
    let(:grand_child_associated_section) { create :section, name: 'Grand child from Associated section', parent: child_associated_section }

    describe 'section/read' do
      describe 'when absent' do
        let(:permissions) { [] }

        it 'forbids from reading' do
          wont_be_able_to :read, Section
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ section/read ] }

        it 'allows reading' do
          must_be_able_to :read, Section
        end
      end
    end

    describe 'section/create' do
      describe 'when absent' do
        let(:permissions) { [] }

        it 'forbids from creating sections' do
          wont_be_able_to :create, Section
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ section/create ] }

        it 'allows creating sections' do
          must_be_able_to :create, Section
        end
      end
    end

    describe 'section/update/associated' do
      let(:current_user) { create :user, sections: [associated_section] }

      describe 'when absent' do
        it 'forbids from editing any section' do
          wont_be_able_to :update, Section
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ section/update/associated ] }

        it 'allows editing only those sections associated to the user and their children' do
          must_be_able_to :update, grand_child_associated_section
          must_be_able_to :update, child_associated_section
          must_be_able_to :update, associated_section
          wont_be_able_to :update, dissociate_section
        end
      end
    end

    describe 'section/update/all' do
      let(:current_user) { create :user, sections: [associated_section] }

      describe 'when absent' do
        it 'forbids from editing any section' do
          wont_be_able_to :update, Section
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ section/update/all ] }

        it 'allows editing any section' do
          must_be_able_to :update, Section
        end
      end
    end

    describe 'section/destroy/associated' do
      let(:current_user) { create :user, sections: [associated_section] }

      describe 'when absent' do
        it 'forbids from destroying any section' do
          wont_be_able_to :destroy, Section
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ section/destroy/associated ] }

        it 'allows destroying only those sections associated to the user and their children' do
          must_be_able_to :destroy, grand_child_associated_section
          must_be_able_to :destroy, child_associated_section
          must_be_able_to :destroy, associated_section
          wont_be_able_to :destroy, dissociate_section
        end
      end
    end

    describe 'section/destroy/all' do
      let(:current_user) { create :user, sections: [associated_section] }

      describe 'when absent' do
        it 'forbids from destroying any section' do
          wont_be_able_to :destroy, Section
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ section/destroy/all ] }

        it 'allows destroying any section' do
          must_be_able_to :destroy, Section
        end
      end
    end
  end
end