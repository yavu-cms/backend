require 'test_helper'
require 'support/permissions_test_case'

class MediumPermissionsTest < PermissionsTestCase
  describe MediumPermissions do
    describe 'medium/read' do
      describe 'when absent' do
        it 'forbids from reading' do
          wont_be_able_to :read, Medium
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ medium/read ] }

        it 'allows reading' do
          must_be_able_to :read, Medium
        end
      end
    end

    describe 'medium/create' do
      describe 'when absent' do

        it 'forbids from creating' do
          wont_be_able_to :create, Medium
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ medium/create ] }

        it 'allows creating' do
          must_be_able_to :create, Medium
        end
      end
    end

    describe 'medium/update/unused' do
      let(:unused_medium) { create :image_medium }
      let(:medium_in_use_by_article) { create :image_medium_in_use_by_article }
      let(:medium_in_use_by_gallery) { create :embedded_medium_in_use_by_gallery }
      let(:medium_in_use_by_component_value) { create :embedded_medium_in_use_by_component_value }

      describe 'when absent' do
        it 'forbids from editing any medium' do
          wont_be_able_to :update, unused_medium
          wont_be_able_to :update, medium_in_use_by_article
          wont_be_able_to :update, medium_in_use_by_gallery
          wont_be_able_to :update, medium_in_use_by_component_value
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ medium/update/unused ] }

        it 'allows editing unused mediums' do
          must_be_able_to :update, unused_medium
        end

        describe 'even if the user can edit associated elements' do
          before do
            pretend_user_can :update, medium_in_use_by_article.articles.first, ability
            pretend_user_can :update, medium_in_use_by_gallery.media_galleries.first, ability
            pretend_user_can :update, medium_in_use_by_component_value.component_values.first, ability
          end

          it 'forbids from updating a medium being used' do
            wont_be_able_to :update, medium_in_use_by_article
            wont_be_able_to :update, medium_in_use_by_gallery
            wont_be_able_to :update, medium_in_use_by_component_value
          end
        end
      end
    end

    describe 'medium/update/all' do
      let(:unused_medium) { create :image_medium }
      let(:medium_in_use_by_article) { create :image_medium_in_use_by_article }
      let(:medium_in_use_by_gallery) { create :embedded_medium_in_use_by_gallery }
      let(:medium_in_use_by_component_value) { create :embedded_medium_in_use_by_component_value }
      let(:medium_in_use_by_article_and_gallery) { create :embedded_medium_in_use_by_article_and_gallery }

      describe 'when absent' do

        it 'forbids from editing any medium' do
          wont_be_able_to :update, unused_medium
          wont_be_able_to :update, medium_in_use_by_article
          wont_be_able_to :update, medium_in_use_by_gallery
          wont_be_able_to :update, medium_in_use_by_component_value
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ medium/update/all ] }

        it 'allows editing unused mediums' do
          must_be_able_to :update, unused_medium
        end

        describe 'and the user can edit all associated elements' do
          before do
            pretend_user_can :update, medium_in_use_by_article.articles.first, ability
            pretend_user_can :update, medium_in_use_by_gallery.media_galleries.first, ability
            pretend_user_can :update, medium_in_use_by_component_value.component_values.first, ability
            pretend_user_can :update, medium_in_use_by_article_and_gallery.articles.first, ability
            pretend_user_can :update, medium_in_use_by_article_and_gallery.media_galleries.first, ability
          end

          it 'allows editing a medium being used' do
            must_be_able_to :update, medium_in_use_by_article
            must_be_able_to :update, medium_in_use_by_gallery
            must_be_able_to :update, medium_in_use_by_component_value
            must_be_able_to :update, medium_in_use_by_article_and_gallery
          end
        end

        describe 'and the user cannot edit any of the associated elements' do
          before do
            pretend_user_cannot :update, medium_in_use_by_article.articles.first, ability
            pretend_user_cannot :update, medium_in_use_by_gallery.media_galleries.first, ability
            pretend_user_cannot :update, medium_in_use_by_component_value.component_values.first, ability
            pretend_user_cannot :update, medium_in_use_by_article_and_gallery.articles.first, ability
            pretend_user_cannot :update, medium_in_use_by_article_and_gallery.media_galleries.first, ability
          end

          it 'forbids from editing a medium being used' do
            wont_be_able_to :update, medium_in_use_by_article
            wont_be_able_to :update, medium_in_use_by_gallery
            wont_be_able_to :update, medium_in_use_by_component_value
            wont_be_able_to :update, medium_in_use_by_article_and_gallery
          end
        end

        describe 'and the user cannot edit at least one of the associated elements' do
          before do
            pretend_user_can :update, medium_in_use_by_article.articles.first, ability
            pretend_user_cannot :update, Gallery, ability
            pretend_user_cannot :update, ComponentValue, ability
            pretend_user_cannot :update, medium_in_use_by_article_and_gallery.articles.first, ability
            pretend_user_cannot :update, medium_in_use_by_article_and_gallery.media_galleries.first, ability
          end

          it 'forbids from editing a medium being used' do
            must_be_able_to :update, medium_in_use_by_article # Sanity check
            wont_be_able_to :update, medium_in_use_by_gallery
            wont_be_able_to :update, medium_in_use_by_component_value
            wont_be_able_to :update, medium_in_use_by_article_and_gallery
          end
        end
      end
    end

    describe 'medium/destroy/unused' do
      let(:unused_medium) { create :image_medium }
      let(:medium_in_use_by_article) { create :image_medium_in_use_by_article }
      let(:medium_in_use_by_gallery) { create :embedded_medium_in_use_by_gallery }
      let(:medium_in_use_by_component_value) { create :embedded_medium_in_use_by_component_value }

      describe 'when absent' do
        let(:permissions) { [] }

        it 'forbids from destroying any medium' do
          wont_be_able_to :destroy, unused_medium
          wont_be_able_to :destroy, medium_in_use_by_article
          wont_be_able_to :destroy, medium_in_use_by_gallery
          wont_be_able_to :destroy, medium_in_use_by_component_value
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ medium/destroy/unused ] }

        it 'allows destroying unused mediums' do
          must_be_able_to :destroy, unused_medium
        end

        describe 'even if the user can update associated elements' do
          before do
            pretend_user_can :update, medium_in_use_by_article.articles.first, ability
            pretend_user_can :update, medium_in_use_by_gallery.media_galleries.first, ability
            pretend_user_can :update, medium_in_use_by_component_value.component_values.first, ability
          end

          it 'forbids from updating a medium being used' do
            wont_be_able_to :destroy, medium_in_use_by_article
            wont_be_able_to :destroy, medium_in_use_by_gallery
            wont_be_able_to :destroy, medium_in_use_by_component_value
          end
        end
      end
    end

    describe 'medium/destroy/all' do
      let(:unused_medium) { create :image_medium }
      let(:medium_in_use_by_article) { create :image_medium_in_use_by_article }
      let(:medium_in_use_by_gallery) { create :embedded_medium_in_use_by_gallery }
      let(:medium_in_use_by_component_value) { create :embedded_medium_in_use_by_component_value }
      let(:medium_in_use_by_article_and_gallery) { create :embedded_medium_in_use_by_article_and_gallery }

      describe 'when absent' do
        let(:permissions) { [] }

        it 'forbids from destroying any medium' do
          wont_be_able_to :destroy, unused_medium
          wont_be_able_to :destroy, medium_in_use_by_article
          wont_be_able_to :destroy, medium_in_use_by_gallery
          wont_be_able_to :destroy, medium_in_use_by_component_value
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ medium/destroy/all ] }

        it 'allows destroying unused mediums' do
          must_be_able_to :destroy, unused_medium
        end

        describe 'and the user can update all associated elements' do
          before do
            pretend_user_can :update, medium_in_use_by_article.articles.first, ability
            pretend_user_can :update, medium_in_use_by_gallery.media_galleries.first, ability
            pretend_user_can :update, medium_in_use_by_component_value.component_values.first, ability
            pretend_user_can :update, medium_in_use_by_article_and_gallery.articles.first, ability
            pretend_user_can :update, medium_in_use_by_article_and_gallery.media_galleries.first, ability
          end

          it 'allows destroying a medium being used' do
            must_be_able_to :destroy, medium_in_use_by_article
            must_be_able_to :destroy, medium_in_use_by_gallery
            must_be_able_to :destroy, medium_in_use_by_component_value
            must_be_able_to :destroy, medium_in_use_by_article_and_gallery
          end
        end

        describe 'and the user cannot destroy any of the associated elements' do
          before do
            pretend_user_cannot :update, medium_in_use_by_article.articles.first, ability
            pretend_user_cannot :update, medium_in_use_by_gallery.media_galleries.first, ability
            pretend_user_cannot :update, medium_in_use_by_component_value.component_values.first, ability
            pretend_user_cannot :update, medium_in_use_by_article_and_gallery.articles.first, ability
            pretend_user_cannot :update, medium_in_use_by_article_and_gallery.media_galleries.first, ability
          end

          it 'forbids from destroying a medium being used' do
            wont_be_able_to :destroy, medium_in_use_by_article
            wont_be_able_to :destroy, medium_in_use_by_gallery
            wont_be_able_to :destroy, medium_in_use_by_component_value
            wont_be_able_to :destroy, medium_in_use_by_article_and_gallery
          end
        end

        describe 'and the user cannot destroy at least one of the associated elements' do
          before do
            pretend_user_can :update, medium_in_use_by_article.articles.first, ability
            pretend_user_cannot :update, Gallery, ability
            pretend_user_cannot :update, ComponentValue, ability
            pretend_user_cannot :update, medium_in_use_by_article_and_gallery.articles.first, ability
            pretend_user_cannot :update, medium_in_use_by_article_and_gallery.media_galleries.first, ability
          end

          it 'forbids from destroying a medium being used' do
            must_be_able_to :destroy, medium_in_use_by_article # Sanity check
            wont_be_able_to :destroy, medium_in_use_by_gallery
            wont_be_able_to :destroy, medium_in_use_by_component_value
            wont_be_able_to :destroy, medium_in_use_by_article_and_gallery
          end
        end
      end
    end
  end
end