require 'test_helper'
require 'support/permissions_test_case'

class CoverPermissionsTest < PermissionsTestCase
  describe CoverPermissions do
    let(:client_application) { create :client_application }
    let(:cover) { create :cover_representation, client_application: client_application }
    let(:cover_in_use) { create(:cover_representation, client_application: client_application).tap { |c| c.stubs(:unused?).returns(false) } }
    let(:cover_outside_of_workspace) { create :cover_representation, client_application: client_application }
    let(:non_cover_representation) { create :representation }

    before do
      pretend_user_workspace_includes [ client_application, cover, cover_in_use ]
    end

    describe 'cover/read' do
      describe 'when absent' do
        it 'forbids from reading or previewing cover representations' do
          wont_be_able_to :read, :cover
          wont_be_able_to :read, cover
          wont_be_able_to :preview, cover
          wont_be_able_to :draft_preview_and_publish, cover
          wont_be_able_to :read, cover_in_use
          wont_be_able_to :preview, cover_in_use
          wont_be_able_to :draft_preview_and_publish, cover_in_use
          wont_be_able_to :read, cover_outside_of_workspace
          wont_be_able_to :preview, cover_outside_of_workspace
          wont_be_able_to :draft_preview_and_publish, cover_outside_of_workspace
          wont_be_able_to :read, non_cover_representation
          wont_be_able_to :preview, non_cover_representation
          wont_be_able_to :draft_preview_and_publish, non_cover_representation
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ cover/read ] }

        it 'allows reading and previewing cover representations' do
          must_be_able_to :read, :cover
          must_be_able_to :read, cover
          must_be_able_to :preview, cover
          must_be_able_to :draft_preview_and_publish, cover
          must_be_able_to :read, cover_in_use
          must_be_able_to :preview, cover_in_use
          must_be_able_to :draft_preview_and_publish, cover_in_use
        end

        it 'forbids from reading/preview/publish covers outside workspace' do
          wont_be_able_to :read, cover_outside_of_workspace
          wont_be_able_to :preview, cover_outside_of_workspace
          wont_be_able_to :draft_preview_and_publish, cover_outside_of_workspace
        end

        it 'forbids from reading/preview/publish non-cover representations' do
          wont_be_able_to :read, non_cover_representation
          wont_be_able_to :preview, non_cover_representation
          wont_be_able_to :draft_preview_and_publish, non_cover_representation
        end
      end
    end

    describe 'cover/update/unused' do
      describe 'when absent' do
        it 'forbids from updating cover representations' do
          wont_be_able_to :update, :cover
          wont_be_able_to :update, cover
          wont_be_able_to :update, cover_in_use
          wont_be_able_to :update, cover_outside_of_workspace
          wont_be_able_to :update, non_cover_representation
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ cover/update/unused ] }

        it 'allows updating only unused cover representations' do
          must_be_able_to :update, :cover
          must_be_able_to :update, cover
          wont_be_able_to :update, cover_in_use
          wont_be_able_to :update, cover_outside_of_workspace
          wont_be_able_to :update, non_cover_representation
        end
      end
    end

    describe 'cover/update/all' do
      describe 'when absent' do
        it 'forbids from updating cover representations' do
          wont_be_able_to :update, :cover
          wont_be_able_to :update, cover
          wont_be_able_to :update, cover_in_use
          wont_be_able_to :update, cover_outside_of_workspace
          wont_be_able_to :update, non_cover_representation
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ cover/update/all ] }

        it 'allows updating both unused and in use cover representations' do
          must_be_able_to :update, :cover
          must_be_able_to :update, cover
          must_be_able_to :update, cover_in_use
        end

        it 'forbids from update non-cover representations' do
          wont_be_able_to :update, cover_outside_of_workspace
          wont_be_able_to :update, non_cover_representation
        end
      end
    end
  end
end