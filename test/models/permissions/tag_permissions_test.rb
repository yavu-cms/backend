require 'test_helper'
require 'support/permissions_test_case'

class TagPermissionsTest < PermissionsTestCase
  describe TagPermissions do
    describe 'tag/read' do
      let(:new_tag) { build :tag }
      let(:persisted_tag) { create :tag }

      describe 'when absent' do
        let(:permissions) { [] }

        it 'forbids from reading' do
          wont_be_able_to :read, new_tag
          wont_be_able_to :read, persisted_tag
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ tag/read ] }

        describe 'for an unused tag' do
          it 'allows reading' do
            must_be_able_to :read, new_tag
            must_be_able_to :read, persisted_tag
          end
        end

        describe 'for a tag in use' do
          describe 'by an article' do
            before do
              persisted_tag.articles << create(:article)
            end

            it 'allows reading' do
              must_be_able_to :read, persisted_tag
            end
          end

          describe 'by a medium' do
            before do
              persisted_tag.media << create(:image_medium)
            end

            it 'allows reading' do
              must_be_able_to :read, persisted_tag
            end
          end

          describe 'by both an article and a medium' do
            before do
              persisted_tag.articles << create(:article)
              persisted_tag.media << create(:image_medium)
            end

            it 'allows reading' do
              must_be_able_to :read, persisted_tag
            end
          end
        end
      end
    end

    describe 'tag/create' do
      describe 'when absent' do
        let(:permissions) { [] }

        it 'forbids from creating' do
          wont_be_able_to :create, Tag
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ tag/create ] }

        it 'allows creating' do
          must_be_able_to :create, Tag
        end
      end
    end

    describe 'tag/update/unused' do
      let(:article) { create :article }
      let(:medium) { create :image_medium }
      let(:unused_tag) { create :tag }
      let(:tag_in_use_by_article) { create :tag, articles: [article] }
      let(:tag_in_use_by_medium) { create :tag, media: [medium] }

      describe 'when absent' do
        let(:permissions) { [] }

        it 'forbids from editing any tag' do
          wont_be_able_to :update, unused_tag
          wont_be_able_to :update, tag_in_use_by_article
          wont_be_able_to :update, tag_in_use_by_medium
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ tag/update/unused ] }

        it 'allows editing unused tags' do
          must_be_able_to :update, unused_tag
        end

        describe 'even if the user can edit associated elements' do
          before do
            pretend_user_can :update, article, ability
            pretend_user_can :update, medium, ability
          end

          it 'forbids from updating a tag being used' do
            wont_be_able_to :update, tag_in_use_by_article
            wont_be_able_to :update, tag_in_use_by_medium
          end
        end
      end
    end

    describe 'tag/update/all' do
      let(:article) { create :article }
      let(:medium) { create :image_medium }
      let(:unused_tag) { create :tag }
      let(:tag_in_use_by_article) { create :tag, articles: [article] }
      let(:tag_in_use_by_medium) { create :tag, media: [medium] }
      let(:tag_in_use_by_article_and_medium) { create :tag, articles: [article], media: [medium] }

      describe 'when absent' do
        let(:permissions) { [] }

        it 'forbids from editing any tag' do
          wont_be_able_to :update, unused_tag
          wont_be_able_to :update, tag_in_use_by_article
          wont_be_able_to :update, tag_in_use_by_medium
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ tag/update/all ] }

        it 'allows editing unused tags' do
          must_be_able_to :update, unused_tag
        end

        describe 'and the user can edit all associated elements' do
          before do
            pretend_user_can :update, article, ability
            pretend_user_can :update, medium, ability
          end

          it 'allows editing a tag being used' do
            must_be_able_to :update, tag_in_use_by_article
            must_be_able_to :update, tag_in_use_by_medium
            must_be_able_to :update, tag_in_use_by_article_and_medium
          end
        end

        describe 'and the user cannot edit any of the associated elements' do
          before do
            pretend_user_cannot :update, article, ability
            pretend_user_cannot :update, medium, ability
          end

          it 'forbids from editing a tag being used' do
            wont_be_able_to :update, tag_in_use_by_article
            wont_be_able_to :update, tag_in_use_by_medium
            wont_be_able_to :update, tag_in_use_by_article_and_medium
          end
        end

        describe 'and the user cannot edit at least one of the associated elements' do
          before do
            pretend_user_cannot :update, article, ability
            pretend_user_can :update, medium, ability
          end

          it 'forbids from editing a tag being used' do
            wont_be_able_to :update, tag_in_use_by_article
            must_be_able_to :update, tag_in_use_by_medium # Sanity check
            wont_be_able_to :update, tag_in_use_by_article_and_medium
          end
        end
      end
    end

    describe 'tag/destroy/unused' do
      let(:article) { create :article }
      let(:medium) { create :image_medium }
      let(:unused_tag) { create :tag }
      let(:tag_in_use_by_article) { create :tag, articles: [article] }
      let(:tag_in_use_by_medium) { create :tag, media: [medium] }

      describe 'when absent' do
        let(:permissions) { [] }

        it 'forbids from destroying any tag' do
          wont_be_able_to :destroy, unused_tag
          wont_be_able_to :destroy, tag_in_use_by_article
          wont_be_able_to :destroy, tag_in_use_by_medium
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ tag/destroy/unused ] }

        it 'allows destroying unused tags' do
          must_be_able_to :destroy, unused_tag
        end

        describe 'even if the user can update associated elements' do
          before do
            pretend_user_can :update, article, ability
            pretend_user_can :update, medium, ability
          end

          it 'forbids from updating a tag being used' do
            wont_be_able_to :destroy, tag_in_use_by_article
            wont_be_able_to :destroy, tag_in_use_by_medium
          end
        end
      end
    end

    describe 'tag/destroy/all' do
      let(:article) { create :article }
      let(:medium) { create :image_medium }
      let(:unused_tag) { create :tag }
      let(:tag_in_use_by_article) { create :tag, articles: [article] }
      let(:tag_in_use_by_medium) { create :tag, media: [medium] }
      let(:tag_in_use_by_article_and_medium) { create :tag, articles: [article], media: [medium] }

      describe 'when absent' do
        let(:permissions) { [] }

        it 'forbids from destroying any tag' do
          wont_be_able_to :destroy, unused_tag
          wont_be_able_to :destroy, tag_in_use_by_article
          wont_be_able_to :destroy, tag_in_use_by_medium
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ tag/destroy/all ] }

        it 'allows destroying unused tags' do
          must_be_able_to :destroy, unused_tag
        end

        describe 'and the user can update all associated elements' do
          before do
            pretend_user_can :update, article, ability
            pretend_user_can :update, medium, ability
          end

          it 'allows destroying a tag being used' do
            must_be_able_to :destroy, tag_in_use_by_article
            must_be_able_to :destroy, tag_in_use_by_medium
            must_be_able_to :destroy, tag_in_use_by_article_and_medium
          end
        end

        describe 'and the user cannot update any of the associated elements' do
          before do
            pretend_user_cannot :update, article, ability
            pretend_user_cannot :update, medium, ability
          end

          it 'forbids from destroying a tag being used' do
            wont_be_able_to :destroy, tag_in_use_by_article
            wont_be_able_to :destroy, tag_in_use_by_medium
            wont_be_able_to :destroy, tag_in_use_by_article_and_medium
          end
        end

        describe 'and the user cannot update at least one of the associated elements' do
          before do
            pretend_user_cannot :update, article, ability
            pretend_user_can :update, medium, ability
          end

          it 'forbids from destroying a tag being used' do
            wont_be_able_to :destroy, tag_in_use_by_article
            must_be_able_to :destroy, tag_in_use_by_medium # Sanity check
            wont_be_able_to :destroy, tag_in_use_by_article_and_medium
          end
        end
      end
    end
  end
end