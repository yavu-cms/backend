require 'test_helper'
require 'support/permissions_test_case'

class UserPermissionsTest < PermissionsTestCase
  describe UserPermissions do
    describe 'user/read' do
      let(:new_user) { build :user }
      let(:persisted_user) { create :user }

      describe 'when absent' do
        let(:permissions) { [] }

        it 'forbids from reading' do
          wont_be_able_to :read, new_user
          wont_be_able_to :read, persisted_user
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ user/read ] }

        it 'allows reading' do
          must_be_able_to :read, new_user
          must_be_able_to :read, persisted_user
        end
      end
    end

    describe 'user/create' do
      describe 'when absent' do
        let(:permissions) { [] }

        it 'forbids from creating users' do
          wont_be_able_to :create, User
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ user/create ] }

        it 'allows creating users' do
          must_be_able_to :create, User
        end
      end
    end

    describe 'user/update' do
      let(:user) { create :user }

      describe 'when absent' do
        let(:permissions) { [] }

        it 'forbids from editing users' do
          wont_be_able_to :update, user
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ user/update ] }

        it 'allows editing users' do
          must_be_able_to :update, user
        end
      end
    end

    describe 'user/destroy' do
      let(:user) { create :user }
      let(:logged_in_user) { create :user }
      let(:current_user) { logged_in_user }

      describe 'when absent' do
        let(:permissions) { [] }

        it 'forbids from destroying users' do
          wont_be_able_to :destroy, user
          wont_be_able_to :destroy, logged_in_user
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ user/destroy ] }

        it 'allows destroying users (except for the logged in one)' do
          must_be_able_to :destroy, user
          wont_be_able_to :destroy, logged_in_user
        end
      end
    end
  end
end