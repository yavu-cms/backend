require 'test_helper'
require 'support/permissions_test_case'

class SuperAdminPermissionsTest < PermissionsTestCase
  describe SuperAdminPermissions do
    let(:permissions) { %w[ super_admin ] }

    describe 'super_admin' do
      it 'allows to manage everything' do
        must_be_able_to :manage, :all
      end
    end
  end
end