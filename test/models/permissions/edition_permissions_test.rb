require 'test_helper'
require 'support/permissions_test_case'

class EditionPermissionsTest < PermissionsTestCase
  describe EditionPermissions do
    let(:permissions) { [] }

    describe 'edition/read' do
      describe 'when absent' do
        it 'forbids from reading editions' do
          wont_be_able_to :read, Edition
          wont_be_able_to :articles, Edition
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ edition/read ] }

        it 'allows reading editions and accessing their articles' do
          must_be_able_to :read, Edition
          must_be_able_to :articles, Edition
        end
      end
    end

    describe 'edition/create' do
      describe 'when absent' do
        it 'forbids from creating new editions' do
          wont_be_able_to :create, Edition
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ edition/create ] }

        it 'allows creating new editions' do
          must_be_able_to :create, Edition
        end
      end
    end

    describe 'edition/update/non_active' do
      let(:active_edition) { create :active_edition }
      let(:non_active_edition) { create :inactive_edition }

      describe 'when absent' do
        it 'forbids from updating any edition' do
          wont_be_able_to :update, active_edition
          wont_be_able_to :update, non_active_edition
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ edition/update/non_active ] }

        it 'allows updating only editions that are not active' do
          wont_be_able_to :update, active_edition
          must_be_able_to :update, non_active_edition
        end
      end
    end

    describe 'edition/update/all' do
      let(:active_edition) { create :active_edition }
      let(:non_active_edition) { create :inactive_edition }

      describe 'when absent' do
        it 'forbids from updating any edition' do
          wont_be_able_to :update, active_edition
          wont_be_able_to :update, non_active_edition
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ edition/update/all ] }

        it 'allows updating both active and non-active editions' do
          must_be_able_to :update, active_edition
          must_be_able_to :update, non_active_edition
        end
      end
    end

    describe 'edition/change_active' do
      describe 'when absent' do
        it 'forbids from changing the active edition for any supplement' do
          wont_be_able_to :become_active, Edition
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ edition/change_active ] }

        it 'allows changing the active edition for any supplement' do
          must_be_able_to :become_active, Edition
        end
      end
    end

    describe 'edition/destroy/non_active' do
      let(:active_edition) { create :active_edition }
      let(:non_active_edition) { create :inactive_edition }

      describe 'when absent' do
        it 'forbids from destroying any edition' do
          wont_be_able_to :destroy, active_edition
          wont_be_able_to :destroy, non_active_edition
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ edition/destroy/non_active ] }

        it 'allows destroying only non-active editions' do
          wont_be_able_to :destroy, active_edition
          must_be_able_to :destroy, non_active_edition
        end
      end
    end

    describe 'edition/destroy/all' do
      let(:active_edition) { create :active_edition }
      let(:non_active_edition) { create :inactive_edition }

      describe 'when absent' do
        it 'forbids from destroying any edition' do
          wont_be_able_to :destroy, active_edition
          wont_be_able_to :destroy, non_active_edition
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ edition/destroy/all ] }

        it 'allows destroying both active and non-active editions' do
          must_be_able_to :destroy, active_edition
          must_be_able_to :destroy, non_active_edition
        end
      end
    end
  end
end