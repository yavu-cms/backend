require 'test_helper'
require 'support/permissions_test_case'

class SettingPermissionsTest < PermissionsTestCase
  describe SettingPermissions do
    let(:permissions) { %w[ setting/manage ] }

    describe 'setting/manage' do
      describe 'when absent' do
        let(:permissions) { [] }

        it 'forbids from reading' do
          wont_be_able_to :read, Setting
          wont_be_able_to :update_all, Setting
          wont_be_able_to :edit_all, Setting
        end
      end

      describe 'when present' do
        it 'allows reading' do
          must_be_able_to :read, Setting
          must_be_able_to :update_all, Setting
          must_be_able_to :edit_all, Setting
        end
      end
    end
  end
end