require 'test_helper'
require 'support/permissions_test_case'

class ClientApplicationPermissionsTest < PermissionsTestCase
  describe ClientApplicationPermissions do
    let(:client_application) { create :client_application }
    let(:client_application_outside_workspace) { create :client_application }

    before do
      pretend_user_workspace_includes client_application
    end

    describe 'client_application/read' do
      describe 'when absent' do
        it 'forbids from reading client application' do
          wont_be_able_to :read, client_application
          wont_be_able_to :read, client_application_outside_workspace
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ client_application/read ] }
        it 'allows reading client applications' do
          must_be_able_to :read, client_application
          wont_be_able_to :read, client_application_outside_workspace
        end
      end
    end

    describe 'client_application/manage' do
      describe 'when absent' do
        it 'forbids from update, remote_management, notify_clients, disable, enable a client application' do
          wont_be_able_to :update, client_application
          wont_be_able_to :update, client_application_outside_workspace
          wont_be_able_to :remote_management, client_application
          wont_be_able_to :remote_management, client_application_outside_workspace
          wont_be_able_to :notify_clients, client_application
          wont_be_able_to :notify_clients, client_application_outside_workspace
          wont_be_able_to :disable, client_application
          wont_be_able_to :disable, client_application_outside_workspace
          wont_be_able_to :enable, client_application
          wont_be_able_to :enable, client_application_outside_workspace
          wont_be_able_to :create, ClientApplication
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ client_application/manage ] }
        it 'allows update, remote_management, notify clients, disable and enable a client application' do
          must_be_able_to :update, client_application
          wont_be_able_to :update, client_application_outside_workspace
          must_be_able_to :remote_management, client_application
          wont_be_able_to :remote_management, client_application_outside_workspace
          must_be_able_to :notify_clients, client_application
          wont_be_able_to :notify_clients, client_application_outside_workspace
          must_be_able_to :disable, client_application
          wont_be_able_to :disable, client_application_outside_workspace
          must_be_able_to :enable, client_application
          wont_be_able_to :enable, client_application_outside_workspace
          must_be_able_to :create, ClientApplication
        end
      end
    end

    describe 'client_application/design_manage' do
      describe 'when absent' do
        it 'forbids from design, manage assets, manage representations, compile assets, manage newsletters and preview a client application' do
          wont_be_able_to :design, client_application
          wont_be_able_to :design, client_application_outside_workspace
          wont_be_able_to :manate_assets, client_application
          wont_be_able_to :manage_assets, client_application_outside_workspace
          wont_be_able_to :manage_representations, client_application
          wont_be_able_to :manage_representations, client_application_outside_workspace
          wont_be_able_to :compile_assets, client_application
          wont_be_able_to :compile_assets, client_application_outside_workspace
          wont_be_able_to :manage_newsletters, client_application
          wont_be_able_to :manage_newsletters, client_application_outside_workspace
          wont_be_able_to :preview, client_application
          wont_be_able_to :preview, client_application_outside_workspace
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ client_application/design_manage ] }
        it 'allows design, manage assets, manage representations, compile assets, manage newsletters and preview a client application' do
          must_be_able_to :design, client_application
          wont_be_able_to :design, client_application_outside_workspace
          must_be_able_to :manage_assets, client_application
          wont_be_able_to :manage_assets, client_application_outside_workspace
          must_be_able_to :manage_representations, client_application
          wont_be_able_to :manage_representations, client_application_outside_workspace
          must_be_able_to :compile_assets, client_application
          wont_be_able_to :compile_assets, client_application_outside_workspace
          must_be_able_to :manage_newsletters, client_application
          wont_be_able_to :manage_newsletters, client_application_outside_workspace
          must_be_able_to :preview, client_application
          wont_be_able_to :preview, client_application_outside_workspace
        end
      end
    end

    describe 'client_application/destroy' do
      describe 'when absent' do
        it 'forbids from destroy all client application' do
          wont_be_able_to :destroy, client_application
          wont_be_able_to :destroy, client_application_outside_workspace
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ client_application/destroy ] }
        it 'allows destroy all client application' do
          must_be_able_to :destroy, client_application
          wont_be_able_to :destroy, client_application_outside_workspace
        end
      end
    end

    describe 'client_application/invalidate_routes' do

      describe 'when absent' do
        it 'does not allow route invalidation' do
          wont_be_able_to :invalidate_routes, client_application
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ client_application/invalidate_routes ] }
        it 'allows route invalidation for client application' do
          must_be_able_to :invalidate_routes, client_application
          wont_be_able_to :invalidate_routes, client_application_outside_workspace
        end
      end

    end # client_application/invalidate_routes
  end # ClientApplicationPermissions
end # ClientApplicationPermissionsTest