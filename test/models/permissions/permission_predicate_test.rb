require 'test_helper'

class PermissionPredicateTest < ActiveSupport::TestCase
  describe PermissionPredicate do
    let(:predicate) { PermissionPredicate.new(definition) }

    describe '#initialize' do
      let(:definition) { stub }

      it 'stores the given definition' do
        predicate.definition.must_equal definition
      end
    end

    describe '#applicable' do
      describe 'when given an empty definition' do
        let(:definition) { [] }

        it 'returns false' do
          predicate.wont_be :applicable?
        end
      end

      describe 'when given a meaningful definition' do
        let(:definition) { [:read, :all] }

        it 'returns true' do
          predicate.must_be :applicable?
        end
      end
    end

    describe '#apply_to' do
      let(:ability) { stub.responds_like_instance_of Ability }
      let(:permission) { stub.responds_like_instance_of Permission }

      describe 'when not applicable' do
        let(:definition) { [] }

        before do
          ability.expects(:can).never
          ability.expects(:cannot).never
        end

        it 'does nothing' do
          predicate.apply_to(ability, permission)
        end
      end

      describe 'when applicable' do
        describe 'and a block is given in the definition' do
          let(:definition) { [:read, :some, block] }
          let(:block) { ->(x){ true } }

          before do
            predicate.expects(:apply_definition).once.with(ability, block)
          end

          it 'sends the block and the definition to the ability' do
            predicate.apply_to(ability, permission)
          end
        end

        describe 'and no block is given in the definition' do
          let(:definition) { [:read, :all] }

          before do
            predicate.expects(:apply_definition).once.with(ability, nil)
          end

          it 'sends the definition to the ability' do
            predicate.apply_to(ability, permission)
          end
        end
      end
    end

    describe '#can_or_cannot' do
      let(:definition) { [:does, :not, :care] }

      describe 'when the predicate is negative' do
        before { predicate.negative = true }

        it 'returns :cannot' do
          predicate.send(:can_or_cannot).must_equal :cannot
        end
      end

      describe 'when the predicate is affirmative' do
        before { predicate.negative = false }

        it 'returns :can' do
          predicate.send(:can_or_cannot).must_equal :can
        end
      end

      describe 'by default' do
        it 'returns :can' do
          predicate.send(:can_or_cannot).must_equal :can
        end
      end
    end

    describe '#block_provided?' do
      describe 'when the last element in the definition is a block' do
        let(:definition) { [:read, ->(x){ true }] }

        it 'returns true' do
          predicate.must_be :block_provided?
        end
      end

      describe 'when no block is given in the last element of the definition' do
        let(:definition) { [:read, :all] }

        it 'returns false' do
          predicate.wont_be :block_provided?
        end
      end
    end

    describe '#condition_provided?' do
      let(:permission) { stub.responds_like_instance_of Permission }

      describe 'when the definition only has two elements' do
        let(:definition) { [:read, :all] }

        it 'returns false' do
          predicate.send(:condition_provided?, permission).must_equal false
        end
      end

      describe 'when the definition has more than two elements' do
        describe 'but the last one is not a symbol' do
          let(:definition) { [:read, :all, ->(x) { true }] }

          it 'returns false' do
            predicate.send(:condition_provided?, permission).must_equal false
          end
        end

        describe 'and the last one is a symbol' do
          let(:definition) { [:read, Article, :my_condition_method] }

          describe 'but it is not a callable method on the permission' do
            it 'returns false' do
              predicate.send(:condition_provided?, permission).must_equal false
            end
          end

          describe 'and it is a callable method on the permission' do
            before do
              permission.expects(:respond_to?).once.with(:my_condition_method, true).returns(true)
            end

            it 'returns true' do
              predicate.send(:condition_provided?, permission).must_equal true
            end
          end
        end
      end
    end

    describe '#fetch_block_from_definition' do
      let(:permission) { stub }

      describe 'when a block is provided' do
        let(:definition) { [:read, :something, block] }
        let(:expected) { stub }
        let(:block) { ->{ expected } }

        it 'returns the block' do
          resulting_block = predicate.send(:fetch_block_from_definition, permission)
          resulting_block.call.must_be_same_as expected
        end

        it 'removes the block from the definition' do
          predicate.send(:fetch_block_from_definition, permission)
          predicate.definition.must_equal [:read, :something]
        end
      end

      describe 'when a condition is provided' do
        let(:definition) { [:read, Article, :my_condition_method] }
        let(:expected) { stub }
        let(:condition_block) { ->{ expected } }

        before do
          permission.expects(:respond_to?).once.with(:my_condition_method, true).returns(true)
          permission.expects(:my_condition_method).once.returns(condition_block)
        end

        it 'returns the condition block' do
          resulting_block = predicate.send(:fetch_block_from_definition, permission)
          resulting_block.call.must_be_same_as expected
        end

        it 'removes the condition method name from the definition' do
          predicate.send(:fetch_block_from_definition, permission)
          predicate.definition.must_equal [:read, Article]
        end
      end

      describe 'when no block nor condition is provided' do
        let(:definition) { [:read, :all] }

        it 'returns nil' do
          predicate.send(:fetch_block_from_definition, permission).must_be_nil
        end

        it 'leaves the definition as is' do
          original_definition = predicate.definition
          predicate.send(:fetch_block_from_definition, permission)
          predicate.definition.must_equal original_definition
        end
      end
    end

    describe '#apply_definition' do
      let(:definition) { [:create, :mayhem] }
      let(:ability) { FakeAbility.new }

      before do
        class FakeAbility
          %i( can cannot ).each do |meth|
            define_method meth do |*args, &block|
              @block_given = block.present?
              @invoked_method = __method__
            end
          end

          def given_a_block?
            @block_given
          end

          def last_invoked_method
            @invoked_method
          end
        end
      end

      describe 'when a block is provided' do
        let(:block) { proc { true } }

        describe 'when the predicate is negative' do
          before do
            predicate.negative = true
          end

          it "sends the definition and the block to the ability's :cannot method" do
            predicate.send(:apply_definition, ability, block)
            ability.must_be :given_a_block?
            ability.last_invoked_method.must_equal :cannot
          end
        end

        describe 'when the predicate is affirmative' do
          it "sends the definition and the block to the ability's :can method" do
            predicate.send(:apply_definition, ability, block)
            ability.must_be :given_a_block?
            ability.last_invoked_method.must_equal :can
          end
        end
      end

      describe 'when a block is not provided' do
        describe 'when the predicate is negative' do
          before do
            predicate.negative = true
          end

          it "sends the definition to the ability's :cannot method" do
            predicate.send(:apply_definition, ability)
            ability.wont_be :given_a_block?
            ability.last_invoked_method.must_equal :cannot
          end
        end

        describe 'when the predicate is affirmative' do
          it "sends the definition to the ability's :can method" do
            predicate.send(:apply_definition, ability)
            ability.wont_be :given_a_block?
            ability.last_invoked_method.must_equal :can
          end
        end
      end
    end
  end
end