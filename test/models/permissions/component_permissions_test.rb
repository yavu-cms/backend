require 'test_helper'
require 'support/permissions_test_case'

class ComponentPermissionsTest < PermissionsTestCase
  describe ComponentPermissions do
    let(:permissions) { [] }
    let(:all_component_types) { [BaseComponent] + BaseComponent.types }
    let(:associations) { [ComponentAsset, ComponentTranslation, ComponentService] }
    let(:component_view) { build :component_view }
    let(:regular_view) { build :view }

    describe 'component/read' do
      describe 'when absent' do
        it 'forbids from reading components and their associations' do
          all_component_types.each do |type|
            wont_be_able_to :read, type
            wont_be_able_to :views, type
            wont_be_able_to :assets, type
            wont_be_able_to :services, type
            wont_be_able_to :translations, type
          end
          # Associations permissions
          associations.each do |association|
            wont_be_able_to :read, association
          end
          # View-specific permissions
          wont_be_able_to :read, component_view
          wont_be_able_to :read, regular_view
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ component/read ] }

        it 'allows reading components and their associations' do
          all_component_types.each do |type|
            must_be_able_to :read, type
            must_be_able_to :views, type
            must_be_able_to :assets, type
            must_be_able_to :services, type
            must_be_able_to :translations, type
          end
          # Associations permissions
          associations.each do |association|
            must_be_able_to :read, association
            wont_be_able_to :manage, association
          end
          # View-specific permissions
          must_be_able_to :read, component_view
          wont_be_able_to :manage, component_view
          wont_be_able_to :read, regular_view
          wont_be_able_to :manage, regular_view
        end
      end
    end

    describe 'component/manage' do
      describe 'when absent' do
        it 'forbids from managing components' do
          all_component_types.each do |type|
            wont_be_able_to :read, type
            wont_be_able_to :create, type
            wont_be_able_to :update, type
            wont_be_able_to :destroy, type
            wont_be_able_to :views, type
            wont_be_able_to :assets, type
            wont_be_able_to :services, type
            wont_be_able_to :translations, type
          end
          # Associations permissions
          associations.each do |association|
            wont_be_able_to :manage, association
          end
          # View-specific permissions
          wont_be_able_to :manage, component_view
          wont_be_able_to :manage, regular_view
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ component/manage ] }

        it 'allows managing components and its associations' do
          all_component_types.each do |type|
            wont_be_able_to :read, type # Reading is a separate permission
            must_be_able_to :create, type
            must_be_able_to :update, type
            wont_be_able_to :destroy, type # Destroying is a separate permission
            must_be_able_to :views, type
            must_be_able_to :assets, type
            must_be_able_to :services, type
            must_be_able_to :translations, type
          end
          # Associations permissions
          associations.each do |association|
            must_be_able_to :manage, association
          end
          # View-specific permissions
          must_be_able_to :manage, component_view
          wont_be_able_to :manage, regular_view
        end
      end
    end

    describe 'component/destroy' do
      describe 'when absent' do
        it 'forbids from destroying a component' do
          all_component_types.each do |type|
            wont_be_able_to :destroy, type
          end
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ component/destroy ] }

        it 'allows destroying any component' do
          all_component_types.each do |type|
            must_be_able_to :destroy, type
          end
        end
      end
    end
  end
end