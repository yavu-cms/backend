require 'test_helper'

class PermissionTest < ActiveSupport::TestCase
  describe Permission do
    describe 'included modules' do
      it 'succeds' do 
        Permission.included_modules.must_include MediumPermissions
        Permission.included_modules.must_include ExtraAttributePermissions
        Permission.included_modules.must_include ClientApplicationPermissions
        Permission.included_modules.must_include ComponentPermissions
        Permission.included_modules.must_include CoverPermissions
        Permission.included_modules.must_include GalleryPermissions
        Permission.included_modules.must_include RolePermissions
        Permission.included_modules.must_include UserPermissions
        Permission.included_modules.must_include EditionPermissions
        Permission.included_modules.must_include ArticlePermissions
        Permission.included_modules.must_include SectionPermissions
        Permission.included_modules.must_include SettingPermissions
        Permission.included_modules.must_include TagPermissions
        Permission.included_modules.must_include SuperAdminPermissions
        Permission.included_modules.must_include PermissionsDSL
      end
    end

    describe '.all' do
      it 'return all permission' do
        Permission.expects(:table).once.returns({ test: { permissions: nil } })
        Permission.all.must_equal ['test/permissions']
        Permission.unstub(:table)
      end
    end

    describe '.establish_for' do
      let(:user) { build :user}
      let(:permissions) { %w(a b c d) }

      before do
        Permission.any_instance.expects(:apply).times permissions.size
        user.expects(:permissions).returns(permissions).once
      end

      after do
        Permission.unstub(:apply)
        user.unstub(:permissions)
      end

      it 'must call Permissions#apply for each user permissions' do
        Permission.establish_for user, Ability.new
      end
    end

    describe '.build' do
      before do
        SamplePermissions = Module.new do
          extend ActiveSupport::Concern
          included do
            def included?; end

            permissions_for(:sample) do
              {
                update: {
                  used: [],
                  all: [:a, :b]
                }
              }
            end
          end
        end

        Permission.send :include, SamplePermissions unless Permission.instance_methods.include? :included?
        Permission.send :include, PermissionsDSL    unless Permission.instance_methods.include? :included?
      end

      describe 'when invalid permission is passed' do
        before do
          Permission.expects(:handle_invalid_permission).with('sample/update/none').once
        end
        after do
          Permission.unstub(:handle_invalid_permission)
        end
        it 'will call .handle_invalid_permission' do
          Permission.build('sample/update/none')
        end
      end

      describe 'when a valid permission is passed' do
        it 'must initialize a Permission instance' do
          result = Permission.build('sample/update/all')
          result.must_be_instance_of Permission
          result.predicates.must_equal %i(a b) 
        end
      end
    end

    describe '.included_in_workspace?' do
      describe 'when receive array of resources' do
        before do
          user = build :user
          user.stubs(:workspace).returns(['resource', 'other_resource'])
          Permission.stubs(:user).returns(user)
        end

        after do
          Permission.unstub(:user)
        end

        it 'succeds' do
          Permission.must_be :included_in_workspace?, ['resource', 'other_resource']
        end
      end

      describe 'when receive one resource' do
        before do
          user = build :user
          user.stubs(:workspace).returns('resource')
          Permission.stubs(:user).returns(user)
        end

        after do
          Permission.unstub(:user)
        end

        it 'succeds' do
          Permission.must_be :included_in_workspace?, 'resource'
        end
      end
    end

    describe '.handle_invalid_permission' do
      describe 'when invalid permission id is received' do
        it 'return empty array' do
          Permission.handle_invalid_permission('invalid').must_equal []
        end
      end
    end

    describe '#initialize' do
      describe 'when receive one predicate' do
        it 'must create array of predicates' do
          predicate = stub
          permission = Permission.new predicate
          permission.predicates.must_equal [predicate] 
        end
      end

      describe 'when receive array of predicates' do
        it 'must create array of predicates' do
          predicate1 = stub
          predicate2 = stub
          permission = Permission.new [predicate1, predicate2]
          permission.predicates.must_equal [predicate1, predicate2] 
        end
      end
    end

    describe '#apply' do
      describe 'when have 2 predicates' do
        it 'call apply_to for each predicate' do
          predicate1 = stub
          predicate2 = stub
          permission = Permission.new [predicate1, predicate2]
          predicate1.expects(:apply_to).once.with(permission.ability, permission)
          predicate2.expects(:apply_to).once.with(permission.ability, permission)
          permission.apply
        end
      end
    end
  end
end