require 'test_helper'
require 'support/permissions_test_case'

class ArticlePermissionsTest < PermissionsTestCase
  describe ArticlePermissions do
    let(:article_invisible_from_workspace) { create :non_visible_article }
    let(:article_visible_from_workspace) { create :article }
    let(:article_invisible_outside_of_workspace) { create :non_visible_article }
    let(:article_visible_outside_of_workspace) { create :article }
    
    before do
      pretend_user_workspace_includes [article_invisible_from_workspace.section, article_visible_from_workspace.section]
    end

    describe 'article/read' do
      describe 'when absent' do
        it 'forbids from reading articles' do
          wont_be_able_to :read, Article
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ article/read ] }
        it 'allows reading articles' do
          must_be_able_to :read, Article
        end
      end
    end

    describe 'article/create/invisible' do
      describe 'when absent' do
        it 'forbids from create any article and find media' do
          wont_be_able_to :find, Medium
          wont_be_able_to :create, article_invisible_from_workspace
          wont_be_able_to :create, article_visible_from_workspace
          wont_be_able_to :create, article_invisible_outside_of_workspace
          wont_be_able_to :create, article_visible_outside_of_workspace
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ article/create/invisible ] }

        it 'allows create invisible article and find media' do
          must_be_able_to :find, Medium
          must_be_able_to :create, article_invisible_from_workspace
          wont_be_able_to :create, article_visible_from_workspace
          wont_be_able_to :create, article_invisible_outside_of_workspace
          wont_be_able_to :create, article_visible_outside_of_workspace
        end
      end
    end 

    describe 'article/create/all'do
      describe 'when absent' do
        it 'forbids from create articles and find media' do
          wont_be_able_to :find, Medium
          wont_be_able_to :create, article_invisible_from_workspace
          wont_be_able_to :create, article_visible_from_workspace
          wont_be_able_to :create, article_invisible_outside_of_workspace
          wont_be_able_to :create, article_visible_outside_of_workspace
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ article/create/all ] }

        it 'allows create all article' do
          must_be_able_to :find, Medium
          must_be_able_to :create, article_invisible_from_workspace
          must_be_able_to :create, article_visible_from_workspace
          wont_be_able_to :create, article_invisible_outside_of_workspace
          wont_be_able_to :create, article_visible_outside_of_workspace
        end
      end
    end

    describe 'article/update/invisible'do
      describe 'when absent' do
        it 'forbids from update invisible articles' do
          wont_be_able_to :find, Medium
          wont_be_able_to :update, article_invisible_from_workspace
          wont_be_able_to :update, article_visible_from_workspace
          wont_be_able_to :update, article_invisible_outside_of_workspace
          wont_be_able_to :update, article_visible_outside_of_workspace
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ article/update/invisible ] }

        it 'allows update invisible articles' do
          must_be_able_to :find, Medium
          must_be_able_to :update, article_invisible_from_workspace
          wont_be_able_to :update, article_visible_from_workspace
          wont_be_able_to :update, article_invisible_outside_of_workspace
          wont_be_able_to :update, article_visible_outside_of_workspace
        end
      end
    end

    describe 'article/update/all'do
      describe 'when absent' do
        it 'forbids from update all article' do
          wont_be_able_to :find, Medium
          wont_be_able_to :update, article_invisible_from_workspace
          wont_be_able_to :update, article_visible_from_workspace
          wont_be_able_to :update, article_invisible_outside_of_workspace
          wont_be_able_to :update, article_visible_outside_of_workspace
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ article/update/all ] }

        it 'allows update all article' do
          must_be_able_to :find, Medium
          must_be_able_to :update, article_invisible_from_workspace
          must_be_able_to :update, article_visible_from_workspace
          wont_be_able_to :update, article_invisible_outside_of_workspace
          wont_be_able_to :update, article_visible_outside_of_workspace
        end
      end
    end

    describe 'article/destroy/invisible'do
      describe 'when absent' do
        it 'forbids from destroy invisible articles' do
          wont_be_able_to :destroy, article_invisible_from_workspace
          wont_be_able_to :destroy, article_visible_from_workspace
          wont_be_able_to :destroy, article_invisible_outside_of_workspace
          wont_be_able_to :destroy, article_visible_outside_of_workspace
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ article/destroy/invisible ] }

        it 'allows destroy invisible articles' do
          must_be_able_to :destroy, article_invisible_from_workspace
          wont_be_able_to :destroy, article_visible_from_workspace
          wont_be_able_to :destroy, article_invisible_outside_of_workspace
          wont_be_able_to :destroy, article_visible_outside_of_workspace
        end
      end
    end

    describe 'article/destroy/all'do
      describe 'when absent' do
        it 'forbids from destroy all article' do
          wont_be_able_to :destroy, article_invisible_from_workspace
          wont_be_able_to :destroy, article_visible_from_workspace
          wont_be_able_to :destroy, article_invisible_outside_of_workspace
          wont_be_able_to :destroy, article_visible_outside_of_workspace
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ article/destroy/all ] }

        it 'allows destroy all article' do
          must_be_able_to :destroy, article_invisible_from_workspace
          must_be_able_to :destroy, article_visible_from_workspace
          wont_be_able_to :destroy, article_invisible_outside_of_workspace
          wont_be_able_to :destroy, article_visible_outside_of_workspace
        end
      end
    end
  end
end