require 'test_helper'
require 'support/permissions_test_case'

class SupplementPermissionsTest < PermissionsTestCase
  describe SupplementPermissions do
    describe 'supplement/read' do
      let(:new_supplement) { build :supplement }
      let(:persisted_supplement) { create :supplement }

      describe 'when absent' do
        let(:permissions) { [] }

        it 'forbids from reading' do
          wont_be_able_to :read, new_supplement
          wont_be_able_to :read, persisted_supplement
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ supplement/read ] }

        it 'allows reading' do
          must_be_able_to :read, new_supplement
          must_be_able_to :read, persisted_supplement
        end
      end
    end

    describe 'supplement/create' do
      describe 'when absent' do
        let(:permissions) { [] }

        it 'forbids from creating supplements' do
          wont_be_able_to :create, Supplement
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ supplement/create ] }

        it 'allows creating supplements' do
          must_be_able_to :create, Supplement
        end
      end
    end

    describe 'supplement/update' do
      let(:supplement) { create :supplement }

      describe 'when absent' do
        let(:permissions) { [] }

        it 'forbids from editing supplements' do
          wont_be_able_to :update, supplement
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ supplement/update ] }

        it 'allows editing supplements' do
          must_be_able_to :update, supplement
        end
      end
    end

    describe 'supplement/destroy' do
      let(:supplement) { create :supplement }

      describe 'when absent' do
        let(:permissions) { [] }

        it 'forbids from destroying supplements' do
          wont_be_able_to :destroy, supplement
        end
      end

      describe 'when present' do
        let(:permissions) { %w[ supplement/destroy ] }

        it 'allows destroying supplements' do
          must_be_able_to :destroy, supplement
        end
      end
    end
  end
end