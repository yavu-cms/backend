require 'test_helper'

class PermissionsDSLTest < ActiveSupport::TestCase
  describe PermissionsDSL do

    before do
      Included = Class.new do
        include ::PermissionsDSL
      end
    end

    describe 'upon inclusion' do
      it 'defines a class-level permissions table and initializes it' do
        Included.class_variable_get(:@@table).must_equal Hash.new
      end
    end

    describe 'DSL' do
      let(:expected) { ->{ } }

      describe '.permissions_for' do
        describe 'when trying to re-define permissions for an existing key' do
          it 'refuses to overwrite the existing permissions' do
            Included.permissions_for(:permission, &expected)
            -> { Included.permissions_for(:permission, &expected) }.must_raise RuntimeError
          end
        end

        describe 'when defining new permissions' do
          it 'adds them to the permissions table' do
            Included.permissions_for(:other_permission, &expected)
            Included.class_variable_get(:@@table)[:other_permission].must_equal &expected
          end
        end
      end

      describe '.condition' do
        describe 'when the condition key clashes with an existing method (or condition)' do
          it 'refuses to add the condition' do
            Included.condition(:condition, &expected)
            -> { Included.condition(:condition, &expected) }.must_raise RuntimeError
          end
        end

        describe 'when defining new conditions' do
          it 'adds them as methods to the including class' do
            Included.condition(:new_method_condition, &expected)
            Included.instance_methods.grep(/new_method_condition/).must_equal [:new_method_condition]
          end
        end
      end

      describe '.can' do
        it 'builds and returns an affirmative PermissionPredicate' do
          permission_predicate = Included.can(:permission, :resource)
          permission_predicate.definition.must_equal [:permission, :resource]
          permission_predicate.negative.must_equal nil 
        end
      end

      describe '.cannot' do
        it 'builds and returns a negative PermissionPredicate' do
          permission_predicate = Included.cannot(:permission, :resource)
          permission_predicate.definition.must_equal [:permission, :resource]
          permission_predicate.negative.must_equal true 
        end
      end
    end
  end
end