require 'test_helper'

class ColumnTest < ActiveSupport::TestCase
  describe Column do
    describe '#copy' do
      let(:order) { 10 }
      let(:configuration) { Hash[a: 1, b:4] }
      let(:cc_counts) { 5 }
      let(:column) do
        create :column_with_comp_configs,
          row: create(:row),
          view: create(:view_with_placeholders),
          order: order,
          configuration: configuration,
          cc_counts: cc_counts
      end
      let(:copy) { column.copy }

      it 'must be a new_record'  do
        column.wont_be :new_record?
        copy.must_be :new_record?
      end

      it 'must set row to nil' do
        column.row.wont_be_nil
        column.row_id.wont_be_nil
        copy.row.must_be_nil
        copy.row_id.must_be_nil
      end

      it 'must copy order' do
        copy.order.must_equal order
      end

      it 'must copy configuration' do
        copy.configuration.must_equal configuration
      end

      it 'must copy view' do
        column.view.wont_be :new_record?
        copy.view.must_be :new_record?
        copy.view.name.must_equal column.view.name
        copy.view.value.must_equal column.view.value
      end

      it 'must copy each component_configurations' do
        column.component_configurations.count.must_equal cc_counts
        copy.component_configurations.length.must_equal cc_counts
        copy.component_configurations.select(&:persisted?).must_be_empty
        copy.component_configurations.select do |x|
          x.column.nil? ||
          x.column != copy
        end.must_be_empty
      end

    end
    describe 'Validations' do
      describe 'row' do
        it 'checks presence' do
          column_wrong = build :column, row: nil
          column_wrong.wont_be :valid?
          column_wrong.errors.must_include :row
        end
      end
      describe 'order' do
        it 'checks presence' do
          column_wrong = build :column, order: nil
          column_wrong.wont_be :valid?
          column_wrong.errors.must_include :order
        end
      end
    end

    describe 'Implemented hot spots from RenderableWithChildren' do
      describe '#default_view' do
        after do
          Representation.any_instance.unstub(:default_column_view)
        end

        it 'must call to default column from representation' do
          Representation.any_instance.expects(:default_column_view).twice
          build(:column, row: build(:app1_representation1_row1), order: 1).default_view
        end
      end

      describe '#children' do
        it 'must return columns' do
          column = build :column_with_comp_configs
          column.children.each { |cc| cc.must_be_instance_of ComponentConfiguration }
        end
      end

      describe '#children=' do
        it 'must set component configurations' do
          column = build :column, row: build(:row), order: 1
          cc = build :component_configuration
          column.children = [cc]
          column.component_configurations.must_equal [cc]
        end
      end

      describe 'parent' do
        it 'must return a row' do
          column = build :column, row: build(:row), order: 1
          column.parent.must_be_instance_of Row
        end
      end
      describe 'parent=' do
        it 'must set a row' do
          column = build :column, row: build(:row), order: 1
          new_row = build :row
          column.parent = new_row
          column.row.must_equal new_row
        end
      end
    end

    describe 'Callbacks' do
      describe 'component_configurations relation' do
        after do
          Representation.any_instance.unstub(:update_assets!)
        end

        it 'must call representation#update_assets! when add new element' do
          Representation.any_instance.expects(:update_assets!).once
          column = create :column
          create :cover_component_configuration, column: column
        end

        it 'must call representation#update_assets! when remove element' do
          Representation.any_instance.expects(:update_assets!).twice
          column = create :column_with_comp_configs
          column.component_configurations.first.destroy
        end

        it 'must delete related view' do
          column = create :column, view: FactoryGirl.build(:view_with_placeholders)
          view_id = column.view.id
          column.destroy
          View.find_by(id: view_id).must_be_nil
        end
      end
    end
  end
end