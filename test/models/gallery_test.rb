require 'test_helper'

class GalleryTest < ActiveSupport::TestCase
  describe Gallery do
    describe '.search' do
      before do
        @gallery_1 = MediaGallery.create(title: 'some')
        @gallery_2 = MediaGallery.create(title: 'cool title')
      end

      describe 'when no parameters are provided' do
        it 'returns all galleries' do
          Gallery.search.must_equal Gallery.all
        end
      end

      describe 'when a query is provided' do
        it 'only returns the matching galleries' do
          results = Gallery.search query: 'some'
          results.count.must_equal 1
          results.first.must_equal @gallery_1
        end

        it 'returns an empty set if there are no matches' do
          results = Gallery.search query: 'jajqr'
          results.must_be_empty
        end
      end
    end

    describe '#unused' do
      describe 'when the gallery is being used by a component value' do
        let(:component_value) { create :component_value_gallery, with_component: true }
        let(:gallery) { component_value.referable }

        it 'returns false' do
          gallery.wont_be :unused?
        end
      end

      describe 'when the gallery is not being used by any component value' do
        let(:gallery) { create :media_gallery }

        it 'returns true' do
          gallery.must_be :unused?
        end
      end
    end

    describe "validations" do
      describe "title" do
        describe "when it is not present" do
          it 'fails' do
            gallery = Gallery.new(title: '')
            -> { gallery.save! }.must_raise ActiveRecord::RecordInvalid
          end
        end

        describe "when it is present" do
          it 'succeeds' do
            gallery = Gallery.new(title: 'Funny gallery')
            -> { gallery.save! }.must_be_silent
          end
        end
      end
    end

    describe "#visible?" do
      before do
        @gallery_1 = MediaGallery.new(title: 'galleryyy')
      end

      it 'should always return true' do
        @gallery_1.visible?.must_equal true
      end
    end
  end
end