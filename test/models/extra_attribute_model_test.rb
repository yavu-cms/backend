require 'test_helper'

class ExtraAttributeModelTest < ActiveSupport::TestCase
  describe ExtraAttributeModel do
    before do
      create :extra_attribute_boolean, name: 'one'
      create :extra_attribute_boolean, name: 'two'
      @extra_attribute_model = build :extra_attribute_model, values: {"one" => true, "two" => false }

      ExtraAttribute.any_instance.stubs(:model_classes).returns({'article' => '1', 'edition' => '0'})
    end

    after do
      ExtraAttribute.any_instance.unstub(:model_classes)
    end

    describe '#method_missing' do

      it 'must call super when no extra_attribute with method_name is received' do
        -> { @extra_attribute_model.three  }.must_raise NoMethodError
      end

      describe 'when extra attribute is not allowed for the model class' do
        let(:extra_attribute)       { create :extra_attribute_boolean, name: 'three' }
        let(:extra_attribute_model) { build :extra_attribute_model, model_type: 'Edition', values: { "one" => true, "two" => false, "three" => true } }

        it 'must call super when extra_attribute is not allowed for the model class' do
          -> { extra_attribute_model.three}.must_raise NoMethodError
        end
      end

      it 'must call getter when extra_attribute with method_name is received' do
        @extra_attribute_model.one.must_equal true
        @extra_attribute_model.two.must_equal false
      end

      it 'must call setter when extra_attribute with method_name= is received with one argument' do
        @extra_attribute_model.one = false
        @extra_attribute_model.two = true
        @extra_attribute_model.one.must_equal false
        @extra_attribute_model.two.must_equal true
      end

      it 'must call super when extra_attribute with method_name  is received with argument count different than one' do
        -> { @extra_attribute_model.one(12)  }.must_raise NoMethodError
      end
    end

    describe '#names' do

      it 'must return extra_attribute names of each attribute setted as value' do
        @extra_attribute_model.names.must_equal %w{one two}
      end
    end

    describe 'attributes with type' do
      describe 'when boolean' do
        before do
          create :extra_attribute_boolean, name: 'bool'
          @extra_attribute_model = build :extra_attribute_model
          @extra_attribute_model.bool.must_be_nil
        end

        it 'must set true when 1' do
          @extra_attribute_model.bool = 1
          @extra_attribute_model.bool.must_equal true
        end

        it 'must set true when 1 string' do
          @extra_attribute_model.bool = "1"
          @extra_attribute_model.bool.must_equal true
        end

        it 'must set true when t string' do
          @extra_attribute_model.bool = "t"
          @extra_attribute_model.bool.must_equal true
        end

        it 'must set true when true string' do
          @extra_attribute_model.bool = "true"
          @extra_attribute_model.bool.must_equal true
        end

        it 'must set true when true' do
          @extra_attribute_model.bool = true
          @extra_attribute_model.bool.must_equal true
        end

        it 'must set false when 0' do
          @extra_attribute_model.bool = 0
          @extra_attribute_model.bool.must_equal false
        end

        it 'must set false when 0 string' do
          @extra_attribute_model.bool = "0"
          @extra_attribute_model.bool.must_equal false
        end

        it 'must set false when f string' do
          @extra_attribute_model.bool = "f"
          @extra_attribute_model.bool.must_equal false
        end

        it 'must set false when false string' do
          @extra_attribute_model.bool = "false"
          @extra_attribute_model.bool.must_equal false
        end

        it 'must set false when false' do
          @extra_attribute_model.bool = false
          @extra_attribute_model.bool.must_equal false
        end

        it 'must remove attribute when nil' do
          @extra_attribute_model.bool = false
          @extra_attribute_model.values.must_equal({ "bool" => false})
          @extra_attribute_model.bool = nil
          @extra_attribute_model.values.must_equal({ })
        end
      end

      describe 'when string' do
        before do
          create :extra_attribute_string, name: 'str'
          @extra_attribute_model = build :extra_attribute_model
          @extra_attribute_model.str.must_be_nil
        end

        it 'must set string value' do
          @extra_attribute_model.str = 'some string'
          @extra_attribute_model.str.must_equal 'some string'
        end

        it 'must remove attribute when nil' do
          @extra_attribute_model.str = 'some string'
          @extra_attribute_model.str.must_equal 'some string'
          @extra_attribute_model.str = nil
          @extra_attribute_model.values.must_equal({})
        end

        it 'must remove attribute when empty string' do
          @extra_attribute_model.str = 'some string'
          @extra_attribute_model.str.must_equal 'some string'
          @extra_attribute_model.str = ""
          @extra_attribute_model.values.must_equal({})
        end
      end
    end

    describe '#merge' do
      describe 'when boolean attributes types' do
        before do
          create :extra_attribute_boolean, name: 'boolean_test'
          @extra_attribute_model = build :extra_attribute_model
          @other_true = build :extra_attribute_model, values: {'boolean_test' => true }
          @other_false = build :extra_attribute_model, values: {'boolean_test' => false}
        end
        describe 'when value is nil' do
          it 'returns other value: true' do
            @extra_attribute_model.boolean_test.must_be_nil
            @extra_attribute_model.merge(@other_true).boolean_test.must_equal true
          end

          it 'returns other value: false' do
            @extra_attribute_model.boolean_test.must_be_nil
            @extra_attribute_model.merge(@other_false).boolean_test.must_equal false
          end
        end

        describe 'when value is true' do
          before do
            @extra_attribute_model.values['boolean_test'] = true
          end
          it 'returns true value, when true' do
            @extra_attribute_model.boolean_test.must_equal true
            @extra_attribute_model.merge(@other_true).boolean_test.must_equal true
          end

          it 'returns true value, when false' do
            @extra_attribute_model.boolean_test.must_equal true
            @extra_attribute_model.merge(@other_false).boolean_test.must_equal true
          end
        end

        describe 'when value is false' do
          before do
            @extra_attribute_model.values['boolean_test'] = false
          end
          it 'returns true value, when true' do
            @extra_attribute_model.boolean_test.must_equal false
            @extra_attribute_model.merge(@other_true).boolean_test.must_equal true
          end

          it 'returns false value, when false' do
            @extra_attribute_model.boolean_test.must_equal false
            @extra_attribute_model.merge(@other_false).boolean_test.must_equal false
          end
        end
      end

      describe 'when string attributes types' do
        before do
          create :extra_attribute_string, name: 'string_test'
          @extra_attribute_model = build :extra_attribute_model
          @other = build :extra_attribute_model, values: {'string_test' => 'some text' }
        end

        describe 'when value is nil' do
          it 'returns other value' do
            @extra_attribute_model.string_test.must_be_nil
            @extra_attribute_model.merge(@other).string_test.must_equal 'some text'
          end
        end

        describe 'when value is original text' do
          before do
            @extra_attribute_model.values['string_test'] = 'original text'
          end

          it 'returns original text' do
            @extra_attribute_model.string_test.must_equal 'original text'
            @extra_attribute_model.merge(@other).string_test.must_equal 'original text'
          end
        end

      end
    end
  end
end