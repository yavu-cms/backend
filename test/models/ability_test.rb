require 'test_helper'

class AbilityTest < ActiveSupport::TestCase
  describe Ability do
    let(:authenticated) { create :user }
    let(:ability) { Ability.new(authenticated) }

    describe 'basic permissions' do
      let(:my_shortcut) { create :shortcut, user: authenticated }
      let(:other_users_shortcut) { create :shortcut }

      it 'defines a basic set of permissions for any logged in user' do
        must_be_able_to :profile, authenticated
        must_be_able_to :search, Tag
        must_be_able_to :search, Medium
        must_be_able_to :search, Edition
        must_be_able_to :autocomplete, Medium
        must_be_able_to :manage, my_shortcut
        wont_be_able_to :manage, other_users_shortcut
      end
    end

    describe 'permissions definition' do
      before do
        Permission.expects(:establish_for).once
      end

      after do
        Permission.unstub(:establish_for)
      end

      it 'uses the permissions proxy to establish permissions for the logged in user' do
        ability
      end
    end
  end
end