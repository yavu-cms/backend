require 'test_helper'

class ClientApplicationAssetsBaseRepresentationTest < ActiveSupport::TestCase
  describe ClientApplicationAssetsBaseRepresentation do
    describe '#copy' do
      it 'must be a new_record' do
        representation = create :representation_with_assets
        instance = representation.client_application_assets_base_representations.first
        instance.wont_be :new_record?
        instance.copy.must_be :new_record?
      end

      it 'must set base_representation to nil' do
        representation = create :representation_with_assets
        copy = representation.client_application_assets_base_representations.first.copy
        copy.base_representation_id.must_be_nil
      end

      it 'must copy client_application_asset' do
        representation = create :representation_with_assets
        original  = representation.client_application_assets_base_representations.first
        copy      = original.copy
        copy.client_application_asset.must_equal original.client_application_asset
      end
    end

    describe 'Callbacks' do
      after do
        Representation.any_instance.unstub(:update_assets!)
      end

      describe 'after create' do
        it 'must call base_representation#update_assets!' do
          representation = create :representation, client_application: create(:client_application_with_assets)
          Representation.any_instance.expects(:update_assets!).once
          representation.assets << representation.client_application.assets.first
        end
      end

      describe 'before destroy' do
        it 'must call base_representation#update_assets!' do
          representation = create :representation_with_assets
          Representation.any_instance.expects(:update_assets!).once
          representation.assets.destroy(representation.assets.first)
        end
      end
    end
  end
end
