require 'test_helper'

class SimpleTaggingTest < ActiveSupport::TestCase
  describe SimpleTagging do
    describe 'tags_from' do
      it 'must generate all tags from an article body (simple case)' do
        @article_body = 'Esto es un cuerpo muy divertido, que va a ser testeado por el algoritmo de autotagging.'
        @article_tags = SimpleTagging.new(ocurrences_limit: 0).tags_from @article_body
        @article_tags.must_equal %w(cuerpo divertido testeado algoritmo autotagging)
      end

      it 'must generate all tags from an article body (moderate case)' do
        @article_body = <<-END
          Esto es un cuerpo muy divertido, con un ¿cuerpo? muy muy muy muy divertido que va a
          ser testeado por el algoritmo de autotagging. Bueno, ahora que
          lo pienso, el ¡cuerpo! no me parece tan diver...¿¿¿¿¿¿¿!!!!!!!! Y ahora incluyo
          un par de números para ver cómo reacciona: 1 1 1 1 2 2 2 45 45 45 45 45 45 12345 56563 67847 253252.
          Ninguno de ellos tiene que ser taggeado, pero si el cuerpo del cuerpo.
        END
        @article_tags = SimpleTagging.new.tags_from @article_body
        @article_tags.must_equal %w(cuerpo)
      end

      it 'must generate compounded tags' do
        @article_body = <<-END
          Se empieza con una oración sin significado.
          Luego, Raul Fernandez va a la entrevista junto a Tito Ruíz, Alicia Mendez y Pepe.
          Raul Fernandez, junto a otro Raul Fernandez, van a la casa de Cinthia y celebran juntos.
          Sin embargo, Raul Fernandez no encontró al otro Raul Fernandez. Qué lástima.
        END
        @article_tags = SimpleTagging.new.tags_from @article_body
        @article_tags.must_equal ["Raul Fernandez", "Tito Ruíz", "Alicia Mendez", "Pepe", "Cinthia", "Raul", "Fernandez"]
      end
    end
  end
end
