require 'test_helper'

class UserTest < ActiveSupport::TestCase
  describe User do
    describe 'Users creation' do
      describe 'when it has username, email and a valid new_password' do
        it 'succeeds' do
          user = build :user
          user.must_be :valid?
        end
      end

      describe 'when it has missing attributes (username, email or password)' do
        it 'fails' do
          build(:user, username: nil).wont_be :valid?
          build(:user, email: nil).wont_be :valid?
          build(:user, new_password: nil).wont_be :valid?
          build(:user, new_password: 'password', new_password_confirmation: 'other-thing').wont_be :valid?
        end
      end

      describe 'when provided email is in use' do
        it "doesn't create the username" do
          create :user, email: 'mail@abc.com'
          build(:user, email: 'mail@abc.com').wont_be :valid?
        end
      end

      describe 'when provided username is in use' do
        it 'validates uniqueness of the username' do
          create :admin, username: 'administrator'
          build(:admin, username: 'administrator').wont_be :valid?
        end
      end
    end

    describe '#update' do
      it "doesn't change password if new_password is blank" do
        user = create(:user, new_password: 'password')
        old_password = user.encrypted_password
        user.update(username: 'other', new_password: '')
        user.reload
        new_password = user.encrypted_password

        new_password.must_equal old_password
      end
    end

    describe '#new_password' do
      it 'sets a new password' do
        user = create(:user, new_password: 'password')
        old_password = user.encrypted_password
        user.update(new_password: 'another_password', current_password: 'password')
        user.reload
        new_password = user.encrypted_password

        new_password.wont_equal old_password
      end

      it 'is ignored if blank' do
        user = create(:user, new_password: 'password')
        old_password = user.encrypted_password
        user.update(new_password: '', current_password: '')
        user.reload
        new_password = user.encrypted_password

        new_password.must_equal old_password
      end
    end

    describe '#check_current_password' do
      before do
        @user = create(:user, new_password: 'hello')
      end

      describe 'when password is ok' do
        it 'must not add an error' do
          @user.current_password = 'hello'
          @user.check_current_password
          @user.errors[:current_password].must_be_empty
        end
      end

      describe 'when password is not ok' do
        it 'must add an error on current_password' do
          @user.current_password = 'good bye'
          @user.check_current_password
          @user.errors[:current_password].must_include I18n.t('activerecord.errors.models.user.password')
        end
      end
    end

    describe '#accessible_sections' do
      it 'needs tests'
    end

    describe '#permissions' do
      it 'needs tests'
    end

    describe '#has_permission?' do
      it 'needs tests'
    end

    describe '#workspace' do
      it 'needs tests'
    end

    describe '#add_to_workspace' do
      it 'needs tests'
    end
  end
end
