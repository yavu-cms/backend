require 'test_helper'

class SettingTest < ActiveSupport::TestCase
  describe Setting do
    describe '.update_all' do
      let(:default_settings) { { "pagination.per_page" => 20, "media_versions.xbig" => "932x932", "media_versions.big" => "576x576", "media_versions.medium" => "392x392", "media_versions.small" => "272x272", "media_versions.xsmall" => "212x212" } }

      before do
        default_settings.each { |k, v| Setting[k] = v }
      end

      describe 'when call with valid params' do
        let(:valid_settings) { {"pagination.per_page"=>"30", "media_versions.xbig"=>"900x900", "media_versions.big"=>"500x500", "media_versions.medium"=>"400x400", "media_versions.small"=>"300x300", "media_versions.xsmall"=>"200x200"} }

        it 'update values' do
          Setting.update_all(valid_settings)
          Setting.get_all.must_equal valid_settings
        end
      end
    end
  end
end