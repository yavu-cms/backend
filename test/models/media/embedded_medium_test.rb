require 'test_helper'

class EmbeddedMediumTest < ActiveSupport::TestCase
  describe EmbeddedMedium do
    describe 'Validations' do
      describe 'file presence' do
        let(:medium) { build :embedded_medium, file: nil, content: 'some content' }

        it "doesn't validate file presence as it doesn't matter for this media type" do
          medium.must_be :valid?
        end
      end

      describe 'content presence' do
        describe 'when no content is provided' do
          let(:medium) { build :embedded_medium, content: nil}

          it 'fails to validate' do
            medium.wont_be :valid?
            medium.errors[:content].must_include I18n.t('errors.messages.blank')
          end
        end

        describe 'when content is provided' do
          let(:medium) { build :embedded_medium, content: 'Some nice content' }

          it 'succeeds' do
            medium.must_be :valid?
          end
        end
      end
    end

    describe 'Common methods' do
      let(:medium) { build :file_medium }
      it 'responds to url and thumbnail' do
        %i(url thumbnail).each do |common_method|
          medium.must_respond_to common_method
          ->{ medium.send common_method }.must_be_silent
        end
      end
    end

    describe 'Callbacks' do
      describe 'after destroy' do
        describe "as this media type doesn't use uploads" do
          let(:medium) { create :embedded_medium }

          before do
            EmbeddedMedium.any_instance.expects(:remove_upload_dir).never
          end

          after do
            EmbeddedMedium.any_instance.unstub(:remove_upload_dir)
          end

          it "doesn't remove the upload dir" do
            medium.destroy
            medium.must_be :destroyed?
          end
        end
      end
    end
  end
end