require 'test_helper'

class AudioMediumTest < ActiveSupport::TestCase
  describe AudioMedium do
    describe 'Common methods' do
      let(:medium) { build :audio_medium }
      it 'responds to url and thumbnail' do
        %i(url thumbnail).each do |common_method|
          medium.must_respond_to common_method
          ->{ medium.send common_method }.must_be_silent
        end
      end
    end

    describe 'Validations' do
      describe 'file presence' do
        describe 'when a file is not specified' do
          let(:audio_medium) { build :audio_medium, file: nil }

          it 'fails to validate' do
            audio_medium.wont_be :valid?
            audio_medium.errors[:file].must_include I18n.t('errors.messages.blank')
          end
        end

        describe 'when a file is specified' do
          let(:audio_medium) { build :audio_medium }

          it 'succeeds' do
            audio_medium.must_be :valid?
          end
        end
      end
    end
  end
end