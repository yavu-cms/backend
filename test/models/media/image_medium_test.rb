require 'test_helper'

class ImageMediumTest < ActiveSupport::TestCase
  describe ImageMedium do
    describe '#default_versions' do
      let(:setting_mock) { mock.responds_like_instance_of Setting }
      let(:medium) { build(:image_medium) }
      let(:expected) { { xbig:'15x15', big: '10x10', medium: '5x5', small: '2x2', xsmall: '1x1' } }

      before do
        Setting.expects(:get_all).returns(expected).once
      end

      after do
        Setting.unstub :get_all
      end

      describe 'on a first call' do
        it 'hits once the Setting key to build the defaults hash' do
          medium.send(:default_versions).must_equal expected
        end
      end

      describe 'on subsequent calls' do
        it "caches the defaults so that the Setting isn't hit again" do
          3.times do
            medium.send(:default_versions).must_equal expected
          end
        end
      end
    end

    describe 'Common methods' do
      let(:medium) { build :image_medium }
      it 'responds to url and thumbnail' do
        %i(url thumbnail).each do |common_method|
          medium.must_respond_to common_method
          ->{ medium.send common_method }.must_be_silent
        end
      end
    end

    describe 'Callbacks' do
      describe 'after initialize' do
        let(:defaults) { { 'media_versions.xbig' => '15x15', 'media_versions.big' => '10x10', 'media_versions.medium' => '5x5', 'media_versions.small' => '2x2', 'media_versions.xsmall' => '1x1' } }

        describe 'when not all versions are set' do
          before do
            ImageMedium.any_instance.expects(:fetch_default_versions).returns(defaults).once
          end

          after do
            ImageMedium.any_instance.unstub(:fetch_default_versions)
          end

          describe 'when no versions are set' do
            let(:medium) { ImageMedium.new xsmall_version: nil, small_version: nil, medium_version: nil, big_version: nil, xbig_version: nil }

            it 'sets all the versions to the default: small, medium and big' do
              medium.xsmall_version.must_equal defaults['media_versions.xsmall']
              medium.small_version.must_equal defaults['media_versions.small']
              medium.medium_version.must_equal defaults['media_versions.medium']
              medium.big_version.must_equal defaults['media_versions.big']
              medium.xbig_version.must_equal defaults['media_versions.xbig']
            end
          end

          describe 'when at least one version is set' do
            let(:medium_version) { '50x50' }
            let(:medium) { ImageMedium.new medium_version: medium_version }

            it 'only sets the default to the versions which are not set' do
              medium.xsmall_version.must_equal defaults['media_versions.xsmall']
              medium.small_version.must_equal defaults['media_versions.small']
              medium.medium_version.must_equal medium_version
              medium.big_version.must_equal defaults['media_versions.big']
              medium.xbig_version.must_equal defaults['media_versions.xbig']
            end
          end
        end

        describe 'when all versions are set' do
          before do
            ImageMedium.any_instance.expects(:fetch_default_versions).never
          end

          after do
            ImageMedium.any_instance.unstub(:fetch_default_versions)
          end

          it 'leaves them untouched' do
            medium = ImageMedium.new xsmall_version: '0x0', small_version: '1x1', medium_version: '50x50', big_version: '100x100', xbig_version: '200x200'
            medium.xsmall_version.must_equal '0x0'
            medium.small_version.must_equal '1x1'
            medium.medium_version.must_equal '50x50'
            medium.big_version.must_equal '100x100'
            medium.xbig_version.must_equal '200x200'
          end
        end
      end

      describe 'after creating' do
        let(:medium) { build(:image_medium) }

        it 'triggers the creation of the three versions: small, medium and big' do
          medium.expects(:create_versions).once
          medium.save
        end
      end

      describe 'after updating' do
        describe 'when any of the versions is changed' do
          let(:medium) { create(:image_medium) }

          it 'recreates its associated file' do
            medium.file.expects(:recreate_versions!).with(:medium).once
            medium.medium_version = '9x7'
            medium.save
          end
        end

        describe 'when none of the versions is changed' do
          let(:medium) { create(:image_medium) }

          it "doesn't recreate its associated file" do
            medium.file.expects(:recreate_versions!).never
            medium.content = 'an unrelated change'
            medium.save
          end
        end
      end
    end

    describe 'Validations' do
      describe 'versions format and presence' do
        describe 'when an invalid format is set' do
          let(:medium) { build :image_medium, xsmall_version: 'wrong', small_version: 'so tiny it hurts', medium_version: 'abc', big_version: '1010', xbig_version: 'error' }

          it 'fails to validate' do
            medium.wont_be :valid?
            %i(xsmall_version small_version medium_version big_version xbig_version).each do |field|
              medium.errors[field].wont_be_empty
            end
          end
        end

        describe 'when a valid <HEIGHT>x<WIDTH> format is set' do
          let(:medium) { build :image_medium, xsmall_version: '0x0', small_version: '1x1', medium_version: '5x5', big_version: '10x10', xbig_version: '15x15' }
          it 'succeeds' do
            medium.must_be :valid?
          end
        end

        describe 'when a version is not present' do
          let(:medium) { build :image_medium, small_version: '' }

          it 'fails to validate' do
            medium.wont_be :valid?
            medium.errors[:small_version].wont_be_empty
          end
        end
      end
    end

    describe 'as_api_resource' do
      let(:medium) { create :image_medium }

      it 'must return expected attributes' do
        @resource = medium.as_api_resource
        medium.id.wont_be_nil
        @resource.id.must_equal medium.id
        @resource.name.must_equal medium.name
        @resource.credits.must_equal medium.credits
        medium.url.wont_be_nil
        @resource.url.must_equal medium.url
        @resource.date.must_equal medium.date
        medium.small_url.wont_be_nil
        @resource.small_url.must_equal medium.small_url
        medium.xsmall_url.wont_be_nil
        @resource.xsmall_url.must_equal medium.xsmall_url
        medium.medium_url.wont_be_nil
        @resource.medium_url.must_equal medium.medium_url
        medium.big_url.wont_be_nil
        @resource.big_url.must_equal medium.big_url
        medium.xbig_url.wont_be_nil
        @resource.xbig_url.must_equal medium.xbig_url
        medium.small_dimensions.wont_be_nil
        @resource.small_dimensions.must_equal medium.small_dimensions.stringify_keys
        medium.xsmall_dimensions.wont_be_nil
        @resource.xsmall_dimensions.must_equal medium.xsmall_dimensions.stringify_keys
        medium.medium_dimensions.wont_be_nil
        @resource.medium_dimensions.must_equal medium.medium_dimensions.stringify_keys
        medium.big_dimensions.wont_be_nil
        @resource.big_dimensions.must_equal medium.big_dimensions.stringify_keys
        medium.xbig_dimensions.wont_be_nil
        @resource.xbig_dimensions.must_equal medium.xbig_dimensions.stringify_keys
      end
    end
  end
end