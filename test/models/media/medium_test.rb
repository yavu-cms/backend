require 'test_helper'

class MediumTest < ActiveSupport::TestCase
  describe Medium do
    describe '.types' do
      it 'returns every possible subtype of the class' do
        Medium.types.map(&:to_s).sort.must_equal Medium.subclasses.map(&:to_s).sort
      end
    end

    describe '.search' do
      before do
        @medium = create(:image_medium)
        @other_medium = create(:embedded_medium)

        @article = create(:article)
        @other_article = create(:article, title: 'other article')

        @article_medium = create(:article_medium, article: @article, medium: @medium, caption: 'caption')
        @other_article_medium = create(:article_medium, article: @other_article, medium: @other_medium, caption: 'other caption')
      end

      describe 'when search query and type are empty' do
        let(:params) { {query: '', type: ''} }

        it 'returns all media' do
          Medium.search(params).must_equal Medium.where(nil)
        end
      end

      describe 'when search query contains something' do
        let(:params) { {query: 'some', type: ''} }

        it 'returns media with its own matching tags' do
          @other_medium.update(serialized_tags: 'something')
          Medium.search(params).must_equal [@other_medium]
        end
      end

      describe 'when type is not empty' do
        let(:params) { {query: '', type: 'ImageMedium'} }

        it 'returns media of the selected type' do
          Medium.search(params).must_equal [@medium]
        end
      end
    end

    describe '#visible?' do
      it 'must return true' do
        medium = Medium.new
        medium.must_be :visible?
      end
    end

    describe '#in_use?' do
      describe 'when the medium has no associated elements' do
        let(:medium) { create :image_medium }

        it 'returns false' do
          medium.wont_be :in_use?
        end
      end

      describe 'when the medium has an associated article' do
        let(:medium) { create :image_medium_in_use_by_article }

        it 'returns true' do
          medium.must_be :in_use?
        end
      end

      describe 'when the medium has an associated gallery' do
        let(:medium) { create :image_medium_in_use_by_gallery }

        it 'returns true' do
          medium.must_be :in_use?
        end
      end

      describe 'when the medium has an associated component value' do
        let(:medium) { create :image_medium_in_use_by_component_value }

        it 'returns true' do
          medium.must_be :in_use?
        end
      end
    end

    describe '#serialized_tags' do
      describe 'when the medium has no tags' do
        it 'returns an empty string' do
          build(:image_medium, tags: []).serialized_tags.must_be :blank?
        end
      end

      describe 'when the medium has one tag' do
        let(:tag) { create :tag, name: 'Tag 1' }
        let(:medium) { create :image_medium, tags: [tag] }

        it 'returns the tag name' do
          medium.serialized_tags.must_equal tag.name
        end
      end

      describe 'when the medium has one tag' do
        let(:tag_1) { create :tag, name: 'Tag 1' }
        let(:tag_2) { create :tag, name: 'Tag 2' }
        let(:tag_3) { create :tag, name: 'Tag 3' }
        let(:medium) { create :image_medium, tags: [tag_1, tag_2, tag_3] }

        it 'returns the tag name' do
          medium.serialized_tags.must_equal [tag_1.name, tag_2.name, tag_3.name].join(',')
        end
      end
    end

    describe '#serialized_tags=' do
      before do
        @tags = [create(:tag, name: 'Tag 1'), create(:tag, name: 'Tag 2'), create(:tag, name: 'Tag 3')]
        @tag_names = @tags.collect(&:name).join(',')
        @medium = create(:image_medium)
      end

      it 'set tags from csv string' do
        @medium.serialized_tags = @tag_names
        @medium.tags.sort.must_equal @tags.sort
      end

      it "doesn't set duplicates" do
        @medium.serialized_tags = [@tag_names, @tag_names].join(',')
        @medium.tags.sort.must_equal @tags.sort
      end
    end


    describe '#filename' do
      it 'returns the filename for the medium' do
        image_medium = create(:image_medium)
        image_medium.filename.must_equal image_medium[:file]
      end
    end

    describe 'Validations' do
      describe 'date format' do
        let(:medium) { build :image_medium }

        it "doesn't validate the date format when date is blank" do
          medium.date_string = ' '
          medium.valid?
          medium.errors.wont_include :date
        end

        it 'validates the format of date when it is not blank' do
          medium.date_string = 'INVALID DATE'
          medium.wont_be :valid?
          medium.errors[:date].must_include I18n.t('activerecord.errors.dates.format')
        end

        it 'validates the format of date to match %F (%Y-%m-%d)' do
          medium.date_string = '2011-12-13'
          medium.valid?
          medium.errors.wont_include :date
        end

        it 'validates the format of date is an existing date' do
          medium.date_string = '2014-25-12'
          medium.wont_be :valid?
          medium.errors[:date].must_include I18n.t('activerecord.errors.dates.format')
        end
      end

      describe 'media type' do
        before do
          @medium = create :embedded_medium
        end

        describe "when an existing medium doesn't have its type changed" do
          it 'succeeds to update' do
            @medium.name = 'Changed name'
            @medium.must_be :valid?
          end
        end

        describe 'when an existing medium has its type changed' do
          it 'fails to validate on the type field and resets it immediately to avoid an inconsistent state' do
            @medium.type = 'ImageMedium'
            @medium.wont_be :valid?
            @medium.errors[:type].must_include I18n.t('activerecord.errors.models.media.attributes.type.wrong_type')
            @medium.type.must_equal 'EmbeddedMedium'
          end
        end

        describe 'when an existing medium has its type changed but the previous type is nil' do
          it 'no replace the type' do
            @medium.type = nil
            @medium.wont_be :valid?
            @medium.type.must_equal 'EmbeddedMedium'
          end
        end
      end
    end

    describe 'Destruction' do
      describe 'when the medium is referenced by a component value' do
        let(:medium) { create :image_medium_in_use_by_component_value }

        it "can't be destroyed" do
          medium.destroy.must_equal false
          medium.wont_be :destroyed?
        end
      end

      describe 'when medium is being used on a gallery' do
        it 'can\'t be destroyed' do
          medium = create :image_medium
          gallery = MediaGallery.create(title: 'g-allery')
          medium_medium_gallery = MediumMediaGallery.create(media_gallery: gallery, medium: medium)
          -> { medium.destroy }.must_raise ActiveRecord::DeleteRestrictionError
          medium.wont_be :destroyed?
        end
      end
    end

    describe 'Callbacks' do
      let(:article_medium){ create :article_medium }
      let(:medium){ article_medium.medium }
      let(:article){ article_medium.article }
      describe '#touch_related' do
        describe 'when saved' do
          it 'must call update_all on every article_media' do
            medium.article_media.each {|x| x.expects(:touch).once  }
            medium.save
          end
        end

        describe 'when touched' do
          it 'must call update_all on every article_media' do
            medium.article_media.each {|x| x.expects(:touch).once  }
            medium.touch
          end
        end
      end
      describe 'set_default_date' do
        let(:stub_date) { Date.new 2014, 11, 11 }

        after do
          Date.unstub(:today)
        end

        describe 'when no date is set' do
          it "sets the date to today's" do
            Date.expects(:today).returns(stub_date).once
            create(:image_medium).date.must_equal stub_date
          end
        end

        describe 'when no date is set' do
          let(:expected) { 10.days.ago.to_date }
          it 'leaves it as it is' do
            Date.expects(:today).never
            create(:image_medium, date: expected).date.must_equal expected
          end
        end
      end

      describe 'remove_upload_dir' do
        let(:medium) { create :file_medium }
        let(:path)   { medium.file.path }
        let(:stub_dir) { File.dirname(path) }

        before do
          FileUtils.expects(:remove_dir).with(stub_dir, force: true).once
        end

        after do
          FileUtils.unstub :remove_dir
        end

        it 'removes the upload directory for the medium' do
          medium.destroy
        end
      end
    end
  end
end
