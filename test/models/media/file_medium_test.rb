require 'test_helper'

class FileMediumTest < ActiveSupport::TestCase
  describe FileMedium do
    describe 'Common methods' do
      let(:medium) { build :file_medium }
      it 'responds to url and thumbnail' do
        %i(url thumbnail extension).each do |common_method|
          medium.must_respond_to common_method
          ->{ medium.send common_method }.must_be_silent
        end
      end
    end

    describe 'Validations' do
      describe 'file presence' do
        describe 'when a file is not specified' do
          let(:file_medium) { build :file_medium, file: nil }

          it 'fails to validate' do
            file_medium.wont_be :valid?
            file_medium.errors[:file].must_include I18n.t('errors.messages.blank')
          end
        end

        describe 'when a file is specified' do
          let(:file_medium) { build :file_medium }

          it 'succeeds' do
            file_medium.must_be :valid?
          end
        end
      end
    end
  end
end
