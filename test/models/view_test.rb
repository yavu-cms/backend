require 'test_helper'

class ViewTest < ActiveSupport::TestCase
  describe View do
    describe '.create_default_for' do
      describe 'for :representation' do
        it 'returns a default view for representations which includes the content, favicon, stylesheets and javascripts placeholders' do
          view = View.create_default_for(:representation)
          view.must_be_instance_of View
          view.wont_be :new_record?
          view.name.wont_be :blank?
          view.value.wont_be :blank?
          [View::CONTENT_PLACEHOLDER, View::FAVICON_PLACEHOLDER, View::STYLESHEETS_PLACEHOLDER, View::JAVASCRIPTS_PLACEHOLDER, View::EXTRA_JAVASCRIPTS_PLACEHOLDER].each do |placeholder|
            view.value.must_match /#{placeholder}/
          end
        end
      end

      describe 'for :row' do
        it 'returns a default view for rows which includes the content placeholder' do
          view = View.create_default_for(:row)
          view.must_be_instance_of View
          view.wont_be :new_record?
          view.name.wont_be :blank?
          view.value.wont_be :blank?
          view.value.must_match /#{View::CONTENT_PLACEHOLDER}/
        end
      end

      describe 'for :column' do
        it 'returns a default view for columns which includes the content placeholder' do
          view = View.create_default_for(:column)
          view.must_be_instance_of View
          view.wont_be :new_record?
          view.name.wont_be :blank?
          view.value.wont_be :blank?
          view.value.must_match /#{View::CONTENT_PLACEHOLDER}/
        end
      end

      describe 'for :error' do
        it 'returns a default view for errors' do
          view = View.create_default_for(:error)
          view.must_be_instance_of View
          view.wont_be :new_record?
          view.name.wont_be :blank?
          view.value.wont_be :blank?
        end
      end

      describe 'for anything else' do
        it 'throws a RuntimeError' do
          ->{ View.create_default_for :eye_of_the_tiger }.must_raise RuntimeError
        end
      end
    end

    describe '#to_s' do
      it 'must return the name of the view' do
        view = build :view, name: 'view_one'
        view.to_s.must_equal 'view_one'
      end

      it 'must return an empty string when name is nil of the view' do
        view = build :view, name: nil
        view.to_s.must_equal ''
      end
    end

    describe 'scopes' do
      describe 'CoverArticleComponents scopes' do
        before do
          @for_layout_1 = create :view, is_layout: true
          @for_layout_2 = create :view, is_layout: true
          @for_article_1 = create :view
          @for_article_2 = create :view
        end

        describe 'for_layout' do
          it 'returns views marked as for_layout' do
            View.for_layout.must_equal [@for_layout_1, @for_layout_2]
          end
        end

        describe 'for_article' do
          it 'returns views not marked as for_layout' do
            View.for_article.must_equal [@for_article_1, @for_article_2]
          end
        end
      end
    end

    describe '#has_placeholder?' do
      it 'must return if there is a specific placeholder in the view' do
        view = build :view, value: "<div #{View::STYLESHEETS_PLACEHOLDER}>#{View::CONTENT_PLACEHOLDER}</div>"
        view.must_be :has_placeholder?, View::CONTENT_PLACEHOLDER
        view.wont_be :has_placeholder?, View::JAVASCRIPTS_PLACEHOLDER
        view.must_be :has_placeholder?, View::STYLESHEETS_PLACEHOLDER
        view.wont_be :has_placeholder?, View::FAVICON_PLACEHOLDER
      end
    end

    describe '#has_content_placeholder' do
      it 'must return if there is a content placeholder in the view' do
        view_with_content    = build :view, value: "<div>#{View::CONTENT_PLACEHOLDER}</div>"
        view_without_content = build :view, value: "<div></div>"
        view_with_content.must_be    :has_content_placeholder?
        view_without_content.wont_be :has_content_placeholder?
      end
    end

    describe '#make_default!' do
      describe 'when the view can be default' do
        let(:component) { create :component }
        let(:view) { create :view, component: component }

        before do
          view.expects(:can_be_default?).once.returns(true)
        end

        describe 'when there is a default view' do
          before do
            @default_view = create :view, component: component, default: true
          end

          it 'marks the old default as non-default and makes this view the default one, returning true' do
            view.make_default!.must_equal true
            @default_view.reload
            view.reload
            @default_view.wont_be :default?
            view.must_be :default?
          end
        end

        describe 'when there is no default view' do
          it 'makes this view the default one for the component, returning true' do
            view.make_default!.must_equal true
            view.reload
            view.must_be :default?
          end
        end
      end

      describe 'when the view cannot be the default' do
        let(:view) { build :view }

        before do
          view.expects(:can_be_default?).once.returns(false)
        end

        it 'returns nil' do
          view.make_default!.must_be_nil
        end
      end
    end

    describe '#can_be_default?' do
      describe 'when the view is a partial' do
        let(:view) { build :view, name: '_partial' }

        it 'returns false' do
          view.wont_be :can_be_default?
        end
      end

      describe 'when the view is an alternative template' do
        let(:view) { build :view, format: 'text/plain' }

        it 'returns false' do
          view.wont_be :can_be_default?
        end
      end

      describe 'when the view is an HTML template' do
        let(:view) { build :view, format: 'text/html' }

        describe 'when the view already is the default one' do
          before do
            view.default = true
          end

          it 'returns false' do
            view.wont_be :can_be_default?
          end
        end

        describe 'when the view is not the default one' do
          before do
            view.default = false
          end

          it 'returns true' do
            view.must_be :can_be_default?
          end
        end
      end
    end

    describe '#alternative?' do
      describe "when the view's format is HTML" do
        let(:view) { build :view, format: 'text/html' }

        it 'returns false' do
          view.wont_be :alternative?
        end
      end

      describe "when the view's format isn't HTML" do
        let(:view) { build :view, format: 'text/plain' }

        it 'returns true' do
          view.must_be :alternative?
        end
      end
    end

    describe '#blank_value?' do
      it 'must return true when value is blank' do
        view = build :view, value: ''
        view.must_be :blank_value?
      end

      it 'must return true when value is nil' do
        view = build :view, value: nil
        view.must_be :blank_value?
      end

      it 'must returns false when value is not blank' do
        view = build :view, value: 'some value'
        view.wont_be :blank_value?
      end
    end

    describe '#template_name' do
      describe 'without component' do
        it 'must be name with parametrize & underscore' do
          view = build :view, name: 'a name with spaces'
          view.template_name.must_equal 'a name with spaces'.parameterize('_')
        end
      end

      describe 'with component' do
        it 'must be name with parametrize & underscore' do
          component = build :component, name: 'a component name'
          view = build :view, component: component, name: 'a name with spaces'
          view.template_name.must_equal "#{component.slug}/#{'a name with spaces'.parameterize('_')}"
        end
      end
    end

    describe '#partial?' do
      it 'must return true when name start with underscore' do
        view = build :view, name: '_partial'
        view.must_be :partial?
      end

      it 'must return false when name does not starts with underscore' do
        view = build :view, name: 'no_partial'
        view.wont_be :partial?
      end
    end

    describe '#render' do
      describe 'with no arguments and a view with no placeholders' do
        let(:view) { build :view_without_placeholders }

        it 'renders the view as it is' do
          view.render.must_equal view.value
        end
      end

      describe 'with no arguments and a view with placeholders' do
        let(:view) { build :view_with_placeholders }

        it 'renders the view as it is' do
          view.render.must_equal view.value
        end
      end

      describe 'with arguments' do
        let(:view) { build :view, value: "I'm a #{View::CONTENT_PLACEHOLDER} view" }

        it 'places each argument into its respective placeholder' do
          placeholders = {content: "flippin' awesome"}
          view.render(placeholders).must_equal "I'm a flippin' awesome view"
        end

        it 'ignores arguments for absent placeholders' do
          placeholders = {content: 'very exclusive', i_wasnt_invited: "Nope. I guess I wasn't"}
          view.render(placeholders).must_equal "I'm a very exclusive view"
        end
      end
    end

    describe 'Callbacks' do
      describe 'before_destroy' do
        it 'wont destroy the last view of a component' do
          # component factory creates a view
          view = create(:component).views.first
          view.component.views.count.must_equal 1
          view.wont_be :destroy
        end

        it 'must destroy the view of a component that has more than one view' do
          # component factory creates a view
          view = create(:component).views.first
          create :component_view, component: view.component
          view.component.views.count.must_equal 2
          view.must_be :destroy
        end

        it 'must destroy a view if has no component associated' do
          view = create :view
          view.component.must_be_nil
          view.must_be :destroy
        end
      end
    end

    describe 'Validations' do
      describe 'name field' do
        it 'fails when empty' do
          view = build(:view, name: nil)
          view.must_be :invalid?
          view.errors[:name].must_include I18n.t('errors.messages.blank')
        end

        it 'succeeds when not empty' do
          view = View.new(name: 'a name')
          view.valid?
          view.errors[:name].must_be_empty
        end

        describe 'uniqueness' do
          it 'must be unique when component is set' do
            view = create :component_view
            other_view = View.new name: view.name, component: view.component
            other_view.wont_be :valid?
            other_view.errors[:name].must_include I18n.t('errors.messages.taken')
          end

          it 'wont be unique when component is set' do
            view = create :view, component: nil
            other_view = View.new name: view.name
            other_view.must_be :valid?
            other_view.errors[:name].must_be_empty
          end
        end
      end

      describe 'format uniqueness' do
        describe 'when the view belongs to a component' do
          let(:component) { create :component }

          before do
            @html_view = create :view, format: 'text/html', component: component
            @another_html_view = build :view, format: 'text/html', component: component
            @plain_text_view = build :view, format: 'text/plain', component: component
            @another_plain_text_view = build :view, format: 'text/plain', component: component
          end

          it 'validates uniqueness of the format for the component only when format is not HTML' do
            # HTML views can co-exist
            @another_html_view.must_be :valid?
            @another_html_view.must_be :save
            # Plain text views cannot co-exist (when at least one of them is already persisted)
            @plain_text_view.must_be :valid?
            @another_plain_text_view.must_be :valid?
            @plain_text_view.must_be :save
            # Now the second plain text view won't be valid as there already exists one with that format for the component
            @another_plain_text_view.wont_be :valid?
            @another_plain_text_view.errors.must_include :format
          end
        end

        describe "when the view doesn't belong to a component" do
          let(:format) { 'text/plain' }
          let(:view) { build :view_with_placeholders, component_id: nil, format: format }

          before do
            old_view = create :view_with_placeholders, component_id: nil, format: format
          end

          it "doesn't validate uniqueness of the format" do
            view.must_be :valid?
          end
        end
      end

      describe 'default uniqueness' do
        describe 'when the view belongs to a component' do
          let(:component) { create :component, views: [] }

          before do
            @default_view = create :view, component: component, default: true
            @another_default_view = build :view, component: component, default: true
          end

          it 'validates uniqueness of the default flag for the component' do
            @another_default_view.wont_be :valid?
            @another_default_view.errors.must_include :default
            # A view for another component will still be valid
            another_component = create :component
            view_for_another_component = build :view ,component: another_component, default: true
            view_for_another_component.must_be :save
          end
        end

        describe "when the view doesn't belong to a component" do
          let(:view) { build :view, component_id: nil, default: true }

          before do
            old_view = create :view, component_id: nil, default: true
          end

          it "doesn't validate uniqueness of the default field" do
            view.must_be :valid?
          end
        end
      end
    end

    describe 'touch relations' do
      it 'touches its related component'
    end
  end
end