require 'test_helper'

class ClientApplicationAssetTest < ActiveSupport::TestCase
  describe ClientApplicationAsset do
    before do
      ClientApplicationAssetUploader.root = Rails.root.join(*%w{tmp tests client_application_assets})
    end

    after do
      FileUtils.remove_dir(ClientApplicationAssetUploader.root, true)
    end

    describe '#assets_configuration' do
      it 'must call client_application.assets_configuration' do
        c = create(:client_application_asset, prefix_path: 'some_path')
        c.expects(:assets_configuration).once
        c.send :assets_configuration
      end
    end

    describe '#prefix_path=' do
      describe 'when it is a new record' do
        it 'must only change prefix_path value' do
          c = create(:client_application_asset, prefix_path: 'some_path')
          c = ClientApplicationAsset.find(c.id)
          c.wont_be :prefix_path_changed?
          c.prefix_path = 'other_path'
          c.must_be :prefix_path_changed?
          proc { c.save! }.must_be_silent
        end
      end

      describe 'when it is not a new record' do
        it 'must move associated file to new prefix and delete the old one' do
          c = create(:client_application_asset, prefix_path: 'some_path')
          c = ClientApplicationAsset.find(c.id)
          path = c.file.current_path
          c.wont_be :prefix_path_changed?
          c.prefix_path = 'other_path'
          c.must_be :prefix_path_changed?
          proc { c.save! }.must_be_silent
          new_path = c.file.current_path
          new_path.wont_equal path
          File.wont_be :exist?, path
          File.must_be :exist?, new_path
        end
      end
    end

    describe 'Callbacks' do
      describe 'before_destroy' do
        let(:client_application) { create :client_application }
        let(:asset) { create :client_application_asset, client_application: client_application }
        let(:representation){ create :representation, client_application: client_application }

        it 'must be destroyed when it has no representations associated' do
          representation.assets.must_be_empty
          asset.must_be :destroy
        end

        it 'msut fail when it has at least one representation associated' do
          representation.assets << asset
          asset.wont_be :destroy
          representation.reload
          representation.assets.count.must_equal 1
        end
      end

      describe 'after create' do
        before do
          @client_application = create :client_application
          @representation = create :representation, client_application: @client_application
          @representation_1 = create :app1_representation1, client_application: @client_application
          @client_application.reload
        end

        it 'must call each representation update_assets!' do
          Representation.any_instance.expects(:update_assets!).twice

          @asset = create :client_application_asset, client_application: @client_application
        end
      end

      describe 'after destroy' do
        before do
          @client_application = create :client_application
          @representation = create :representation, client_application: @client_application
          @representation_1 = create :app1_representation1, client_application: @client_application
          @client_application.reload
        end

        it 'must call each representation update_assets!' do
          # Two representations updated twice, on save and destroy.
          Representation.any_instance.expects(:update_assets!).times(4)

          @asset = create :client_application_asset, client_application: @client_application
          @asset.destroy!
        end
      end
    end

    describe '#absolute_url' do
      before do
        @client_application = create :client_application
        @asset = create :client_application_asset, client_application: @client_application
        ClientApplicationAssetUploader.expects(:absolute_prefix_for_type).with(@client_application, @asset.type).returns('some_path')
      end

      it 'returns storage folder for this asset' do
        @asset.absolute_url.must_equal 'some_path'
      end
    end

    describe '#favicon_for?' do
      describe 'when it is set as favicon' do
        before do
          @client_application = create :client_application
          @asset = create :client_application_asset, client_application: @client_application
        end

        it 'should return true' do
          @client_application.favicon = @asset
          @asset.favicon_for?(@client_application).must_equal true
        end
      end

      describe 'when it is not set as favicon' do
        before do
          @client_application = create :client_application
          @asset = create :client_application_asset, client_application: @client_application
          @asset_2 = create :client_application_asset, client_application: @client_application, file: upload_test_file('image_2.jpg')
        end

        it 'should return false' do
          @client_application.favicon = @asset
          @asset_2.favicon_for?(@client_application).must_equal false
        end
      end
    end

    describe '#logo_for?' do
      describe 'when it is set as logo' do
        before do
          @client_application = create :client_application
          @asset = create :client_application_asset, client_application: @client_application
        end

        it 'should return true' do
          @client_application.logo = @asset
          @asset.logo_for?(@client_application).must_equal true
        end
      end

      describe 'when it is not set as logo' do
        before do
          @client_application = create :client_application
          @asset = create :client_application_asset, client_application: @client_application
          @asset_2 = create :client_application_asset, client_application: @client_application, file: upload_test_file('image_2.jpg')
        end

        it 'should return false' do
          @client_application.logo = @asset
          @asset_2.logo_for?(@client_application).must_equal false
        end
      end
    end

    describe '#can_be_favicon?' do
      describe 'when it is an image' do
        before do
          @client_application = create :client_application
          @asset = create :client_application_asset, client_application: @client_application
        end

        it 'must return true' do
          @asset.can_be_favicon?.must_equal true
        end
      end

      describe 'when it is not an image' do
        before do
          @client_application = create :client_application
          @asset_js = create :client_application_asset_editable, client_application: @client_application
        end

        it 'must return false' do
          @asset_js.can_be_favicon?.must_equal false
        end
      end
    end

    describe '#can_be_logo?' do
      describe 'when it is an image' do
        before do
          @client_application = create :client_application
          @asset = create :client_application_asset, client_application: @client_application
        end

        it 'must return true' do
          @asset.can_be_logo?.must_equal true
        end
      end

      describe 'when it is not an image' do
        before do
          @client_application = create :client_application
          @asset_js = create :client_application_asset_editable, client_application: @client_application
        end

        it 'must return false' do
          @asset_js.can_be_logo?.must_equal false
        end
      end
    end

    describe 'as_api_resource' do
      before do
        @asset = create :client_application_asset
      end

      it 'must return expected attributes' do
        @resource = @asset.as_api_resource
        @resource.type.must_equal @asset.type
        @resource.file.must_equal @asset.relative_url
      end
    end

    describe "Validations" do
      describe 'type field' do
        it 'fails when empty' do
          asset = build(:client_application_asset, type: nil)
          asset.must_be :invalid?
          asset.errors[:type].must_include I18n.t('errors.messages.blank')
        end

        it 'succeds when not empty' do
          asset = ClientApplicationAsset.new(type: 'javascript')
          asset.valid?
          asset.errors[:type].must_be_empty
        end
      end

      describe 'client_application_id field' do
        it 'fails when empty' do
          asset = build(:client_application_asset, client_application: nil)
          asset.must_be :invalid?
          asset.errors[:client_application].must_include I18n.t('errors.messages.blank')
        end

        it 'succeds when not empty' do
          asset = ClientApplicationAsset.new(client_application_id: create(:client_application).id)
          asset.valid?
          asset.errors[:client_application_id].must_be_empty
        end
      end

      describe 'file field' do
        it 'fails when empty' do
          asset = build(:client_application_asset, file: nil)
          asset.must_be :invalid?
          asset.errors[:file].must_include I18n.t('errors.messages.blank')
        end

        it 'succeds when not empty' do
          asset = ClientApplicationAsset.new(file: upload_test_file('image_1.jpg'))
          asset.valid?
          asset.errors[:file].must_be_empty
        end

      end

      describe 'prefix_path field' do
        it 'defaults to empty string' do
          asset = ClientApplicationAsset.new
          asset.prefix_path.must_equal ''
        end
      end

      describe 'file uniqueness' do
        it 'fails when dulicates same file for same client_application, prefix_path & type' do
          a1 = create(:client_application_asset)
          a2 = build(:client_application_asset,
                     client_application_id: a1.client_application_id,
                     type: a1.type,
                     file: File.open(a1.file.current_path),
                     prefix_path: a1.prefix_path
                    )
          a2.must_be :invalid?
          a2.errors[:file].must_include I18n.t('errors.messages.taken')
        end

        it 'must raises exception from database when dulicates same file for same client_application, prefix_path & type' do
          a1 = create(:client_application_asset)
          a2 = build(:client_application_asset,
                     client_application_id: a1.client_application_id,
                     type: a1.type,
                     file: File.open(a1.file.current_path),
                     prefix_path: a1.prefix_path
                    )
          proc do
            a2.save(validate: false)
          end.must_raise ActiveRecord::RecordNotUnique
        end
      end
    end
  end
end