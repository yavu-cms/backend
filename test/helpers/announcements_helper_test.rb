require 'test_helper'

class AnnouncementsHelperTest < ActionView::TestCase
  include AnnouncementsHelper

  describe AnnouncementsHelper do
    describe '#announcements_url' do
      let(:announcements_stub) { stub }

      before do
        YavuSettings.stubs(:announcements).returns(announcements_stub)
        announcements_stub.stubs(:path).returns('/wsock')
        announcements_stub.stubs(:hostspec).returns('http://test.host:5000')
      end

      after do
        self.unstub(:root_url)
        YavuSettings.unstub(:announcements)
      end

      describe 'when no given_url is provided' do
        before do
          self.expects(:root_url).never
        end

        it 'builds an absolute URL to use for the announcements WS connection' do
          announcements_url.must_equal('http://test.host:5000/wsock')
        end
      end

      describe 'when a given_url is provided' do
        before do
          self.expects(:root_url).never
        end

        it 'uses that url as the base hostspec' do
          announcements_url('http://www.yavu.org').must_equal('http://www.yavu.org/wsock')
        end
      end
    end

    describe '#announcements_timeout' do
      let(:announcements_stub) { stub }

      before do
        YavuSettings.expects(:announcements).returns(announcements_stub).once
        announcements_stub.expects(:timeout).returns(6).once
      end

      after { YavuSettings.unstub(:announcements) }

      it 'takes the timeout for the announcements from the settings' do
        announcements_timeout.must_equal(6)
      end
    end

    describe '#announcements_options' do
      let(:url) { 'http://www.example.org/wsock' }
      let(:time_out) { 12 }
      let(:user) { build :user }
      let(:error_message) { ':(' }
      let(:global_channel) { '/a/all' }
      let(:user_channel) { '/a/user' }
      let(:channels) { Hash[global: global_channel, user: user_channel] }
      let(:messages) { Hash[error: error_message] }

      before do
        self.expects(:announcements_url).returns(url).once
        self.expects(:announcements_timeout).returns(time_out).once
        self.expects(:current_user).returns(user).once
        self.expects(:channel_path).with(:global).returns(global_channel).once
        self.expects(:channel_path).with(user).returns(user_channel).once
        I18n.stubs(:t).with('errors.websocket.connection').returns(error_message)
      end

      after do
        self.unstub :announcements_url, :channel_path, :announcements_timeout, :current_user
        I18n.unstub :t
      end

      it 'returns a full set of options for the announcements plugin' do
        result = announcements_options
        result.delete(:url).must_equal(url)
        result.delete(:timeout).must_equal(time_out)
        result.delete(:channels).must_equal(channels)
      end
    end

    describe '#channel_path' do
      describe 'without a namespace' do
        describe 'when the resource is :global' do
          it 'builds a channel path for the global channel' do
            channel_path(:global).must_equal('/announcement/all')
          end
        end

        describe 'when the resource is a User' do
          let(:user) { build :user, username: 'bob' }

          it 'builds a channel path for the user' do
            channel_path(user).must_equal('/announcement/bob')
          end
        end

        describe 'when the resource is an Article' do
          let(:article) { build :article }

          before { article.expects(:to_param).returns('XXX').once }

          it 'builds a channel path for the article' do
            channel_path(article).must_equal('/article/XXX')
          end
        end

        describe 'when the resource is a RepresentationDraft' do
          let(:representation_draft) { build :representation_draft }

          before { representation_draft.expects(:to_param).returns('YYY').once }

          it 'builds a channel path for the representation_draft' do
            channel_path(representation_draft).must_equal('/representation/YYY')
          end
        end

        describe 'when the resource is any other thing' do
          it 'builds a default channel path for it' do
            channel_path('some-other-thing').must_equal('/announcement/some-other-thing')
            channel_path(42).must_equal('/announcement/42')
          end
        end
      end

      describe 'with a namespace' do
        let(:ns) { '/namespace' }

        def namespaced(url)
          "#{ns}#{url}"
        end

        describe 'when the resource is :global' do
          it 'builds a channel path for the global channel' do
            channel_path(:global, namespace: ns).must_equal(namespaced('/announcement/all'))
          end
        end

        describe 'when the resource is a User' do
          let(:user) { build :user, username: 'bob' }

          it 'builds a channel path for the user' do
            channel_path(user, namespace: ns).must_equal(namespaced('/announcement/bob'))
          end
        end

        describe 'when the resource is an Article' do
          let(:article) { build :article }

          before do
            article.expects(:to_param).returns('XXX').once
          end

          it 'builds a channel path for the article' do
            channel_path(article, namespace: ns).must_equal(namespaced('/article/XXX'))
          end
        end

        describe 'when the resource is a RepresentationDraft' do
          let(:representation_draft) { build :representation_draft }

          before { representation_draft.expects(:to_param).returns('YYY').once }

          it 'builds a channel path for the representation_draft' do
            channel_path(representation_draft, namespace: ns).must_equal(namespaced('/representation/YYY'))
          end
        end

        describe 'when the resource is any other thing' do
          let(:resource) { 'some-other-thing' }

          it 'builds a default channel path for it' do
            channel_path(resource, namespace: ns).must_equal(namespaced("/announcement/#{resource}"))
          end
        end
      end
    end
  end
end
