require 'test_helper'

class ComponentAssetsHelperTest < ActionView::TestCase
  include ComponentAssetsHelper

  describe ComponentAssetsHelper do
    describe '#form_component_asset_path' do
      let(:component) { create :component }

      before do
        self.stubs(:component_assets_path).with(component).returns('CREATION ROUTE')
        self.stubs(:component_asset_path).with(component, component_asset).returns('UPDATE ROUTE')
      end

      describe 'when provided with a new component asset' do
        let(:component_asset) { build :component_asset, component: component }

        it 'returns the creation route' do
          form_component_asset_path(component, component_asset).must_equal 'CREATION ROUTE'
        end
      end

      describe 'when provided with an already persisted component asset' do
        let(:component_asset) { create :component_asset, component: component }

        it 'returns the creation route' do
          form_component_asset_path(component, component_asset).must_equal 'UPDATE ROUTE'
        end
      end
    end
  end
end
