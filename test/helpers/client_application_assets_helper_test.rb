require 'test_helper'

class ClientApplicationAssetsHelperTest < ActionView::TestCase
  include ClientApplicationAssetsHelper

  describe ClientApplicationAssetsHelper do
    describe '#representations_which_is_present' do
      describe 'when the asset is present in at least one representation' do
        let(:representation_1) { build :representation, name: 'Representation 1' }
        let(:representation_2) { build :representation, name: 'Representation 2' }
        let(:asset) { build :client_application_asset }

        before do
          asset.expects(:representations).returns([representation_1, representation_2]).twice
        end

        it 'returns a comma-separated string of them' do
          representations_which_is_present(asset).must_equal 'Representation 1, Representation 2'
        end
      end

      describe 'when the asset is not present in any representation' do
        let(:asset) { build :client_application_asset }

        before do
          asset.expects(:representations).returns([]).once
        end

        it 'returns a dash' do
          representations_which_is_present(asset).must_equal '-'
        end
      end
    end
  end
end
