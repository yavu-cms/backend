require 'test_helper'

class BackendPluginHelperTest < ActionView::TestCase
  include BackendPluginHelper
  include ERB::Util

  describe BackendPluginHelper do
    describe '#escape_configuration' do
      let(:expected) { '{&quot;initializer&quot;:{}}' }
      let(:value) { {initializer: {}} }

      it 'correctly escapes a hash' do
        escape_configuration(value).must_equal expected
      end
    end

    describe '#unescape_configuration' do
      let(:expected) { {initializer: {}} }
      let(:value) { '{&quot;initializer&quot;:{}}' }

      it 'correctly un-escapes the provided (escaped) configuration' do
        unescape_configuration(value).must_equal expected
      end
    end

    describe '#plugins' do
      let(:expected) { 'data-plugins="{&quot;initializer&quot;:{}}"' }
      let(:value) { {initializer: {}} }

      it 'returns a valid data-plugins attribute' do
        plugins(value).must_equal expected
      end
    end
  end
end