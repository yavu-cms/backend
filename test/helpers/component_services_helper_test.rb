require 'test_helper'

class ComponentServicesHelperTest < ActionView::TestCase
  include ComponentServicesHelper

  describe ComponentServicesHelper do
    describe '#form_component_service_path' do
      let(:component) { create :component }

      before do
        self.stubs(:component_services_path).with(component).returns('CREATION ROUTE')
        self.stubs(:component_service_path).with(component, component_service).returns('UPDATE ROUTE')
      end

      describe 'when provided with a new component service' do
        let(:component_service) { build :component_service, component: component }

        it 'returns the creation route' do
          form_component_service_path(component, component_service).must_equal 'CREATION ROUTE'
        end
      end

      describe 'when provided with an already persisted component service' do
        let(:component_service) { create :component_service, component: component }

        it 'returns the creation route' do
          form_component_service_path(component, component_service).must_equal 'UPDATE ROUTE'
        end
      end
    end
  end
end
