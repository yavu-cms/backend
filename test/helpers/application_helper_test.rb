require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  include ApplicationHelper

  describe ApplicationHelper do
    it 'needs tests'

    describe '#date_range_with_format' do
      let(:date_range) { DateTime.new(2001,2,3)..DateTime.new(2001,3,3) }
      let(:some_date) { stub }

      before { self.expects(:date_with_format).returns(some_date).twice }
      after  { self.unstub(:date_with_format) }

      it 'returns proper string when called for between_and' do
        date_range_with_format(date_range, 'between_and').must_equal "entre #{some_date} y #{some_date}"
      end

      it 'returns proper string when called for from_to' do
        date_range_with_format(date_range).must_equal "desde #{some_date} hasta #{some_date}"
      end
    end
  end
end
