require 'test_helper'

class ComponentsHelperTest < ActionView::TestCase
  include ComponentsHelper

  describe ComponentsHelper do
    describe '#form_component_path' do
      before do
        self.stubs(:components_path).returns('CREATION ROUTE')
        self.stubs(:component_path).with(component).returns('UPDATE ROUTE')
      end

      describe 'when provided with a new component' do
        let(:component) { build :component }

        it 'returns the creation route' do
          form_component_path(component).must_equal 'CREATION ROUTE'
        end
      end

      describe 'when provided with an already persisted component' do
        let(:component) { create :component }

        it 'returns the creation route' do
          form_component_path(component).must_equal 'UPDATE ROUTE'
        end
      end
    end

    describe '#autocomplete_field_options_path' do
      describe 'when the provided model is "article"' do
        it 'needs tests'
      end

      describe 'when the provided model is "gallery"' do
        it 'needs tests'
      end

      describe 'when the provided model is "medium"' do
        it 'needs tests'
      end

      describe 'when the provided model is "section"' do
        it 'needs tests'
      end

      describe 'when the provided model is an invalid value' do
        it 'raises an exception'
      end
    end
  end
end