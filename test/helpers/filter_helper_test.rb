require 'test_helper'

class FilterHelperTest < ActionView::TestCase
  include FilterHelper
  # Including +RenderingHelper+ in order to be able to stub the +render+ method
  include ActionView::Helpers::RenderingHelper

  describe FilterHelper do
    describe '#render_simple_filter' do
      it 'use correct partial and locals' do
        action = articles_path
        filter_key = :name
        options = {}
        self.expects(:render).with(partial: 'shared/filters/simple', locals: { action: action, filter_key: :name, options: options }).once
        render_simple_filter(action, :name, options)
      end
    end

    describe '#render_filter_labels' do
      it 'use correct partial' do
        scope = :articles
        self.expects(:render).with(partial: 'shared/filters/labels', locals: { scope: scope }).once
        render_filter_labels(scope)
      end
    end
  end
end