require 'test_helper'

class ClientApplicationsHelperTest < ActionView::TestCase
  include ClientApplicationsHelper

  describe ClientApplicationsHelper do
    let(:asset) { build :client_application_asset }

    describe '#client_application_image_tag' do
      before do
        self.expects(:client_asset_download_path).
          with(asset).
          returns('the-src').once
      end

      it 'returns an image tag for the download client asset path' do
        client_application_image_tag(asset).must_equal '<img alt="The src" src="/images/the-src" />'
      end
    end

    describe '#client_asset_download_path' do
      before do
        self.expects(:serve_client_application_application_asset_path).
          with(asset.client_application, asset).
          returns('the path').once
      end

      it 'returns the download path of the asset' do
        client_asset_download_path(asset).must_equal 'the path'
      end
    end
  end
end
