require 'test_helper'

class AssetHelperTest < ActionView::TestCase
  include AssetHelper

  describe AssetHelper do
    describe '#render_asset' do
      describe 'when asset type is image' do
        let(:image) { create :component_asset_image }
        before do
          self.expects(:render_image).with(image, false).once.returns('rendered image')
        end
        it 'calls render image' do
          render_asset(image).must_equal 'rendered image'
        end
      end

      describe 'when asset type is javascript' do
        let(:js_asset) { create :component_asset_javascript }
        before do
          self.expects(:render_javascript).with(js_asset, false).once.returns('rendered js')
        end
        it 'calls render_javascript' do
          render_asset(js_asset).must_equal 'rendered js'
        end
      end
    end

    describe '#render_image' do
      let(:image) { create :component_asset_image }
      before do
        self.expects(:asset_preview_path).with(image).once.returns('asset path')
        self.expects(:image_tag).with('asset path').once.returns('an image tag')
      end
      it 'correctly renders an image with a proper URL' do
        render_image(image, nil).must_equal 'an image tag'
      end
    end

    describe '#render_javascript' do
      let(:js_asset) { create :component_asset_javascript }
      let(:coderay_wrapper) { stub }

      before do
        CodeRay.expects(:scan).with(js_asset.content(false), :js).returns(coderay_wrapper).once
        coderay_wrapper.expects(:div).once.returns('parsed html')
      end

      it 'it calls CodeRay scan' do
        render_javascript(js_asset, false).must_equal 'parsed html'
      end

      after do
        CodeRay.unstub(:coderay_wrapper)
      end
    end

    describe '#render_css' do
      let(:css_asset) { create :component_asset_stylesheet }
      let(:coderay_wrapper) { stub }

      before do
        CodeRay.expects(:scan).with(css_asset.content(false), :css).returns(coderay_wrapper).once
        coderay_wrapper.expects(:div).once.returns('parsed html')
      end

      it 'it calls CodeRay scan' do
        render_css(css_asset, false).must_equal 'parsed html'
      end

      after do
        CodeRay.unstub(:coderay_wrapper)
      end
    end

    describe '#asset_preview_path' do
      let(:asset) { create :component_asset }
      let(:asset_serve_path) { '/bogus/url' }

      describe 'when valid asset given' do
        before do
          self.expects(:component_asset_serve_path).with(asset).once.returns(asset_serve_path)
        end

        it 'returns serve path for given asset' do
          asset_preview_path(asset).must_equal asset_serve_path
        end
      end

      describe 'when invalid asset given' do
        let(:invalid_asset) { 'invalid_asset' }

        it 'doesnt break when invalid data is given' do
          asset_preview_path(invalid_asset).must_equal '#'
        end
      end

    end
  end
end
