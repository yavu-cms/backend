require 'test_helper'

class MediaHelperTest < ActionView::TestCase
  include MediaHelper
  # Including +RenderingHelper+ in order to be able to stub the +render+ method
  include ActionView::Helpers::RenderingHelper

  describe MediaHelper do
    describe '#upload_type_select_tag' do
      let(:types) { ["ImageMedium", "FileMedium", "EmbeddedMedium", "AudioMedium"]  }
      let(:expected_types) { ["ImageMedium", "FileMedium", "AudioMedium"] }

      describe 'normal usage' do
        it 'must call i18n_select with all types' do
          self.expects(:i18n_select).with(:medium, :type, types, {:i18n_key => 'media.types', :include_blank => true}).once
          upload_type_select_tag :medium, :type
        end
      end

      describe 'excluding some types' do
        it 'must call i18n_select with some types' do
          self.expects(:i18n_select).with(:medium, :type, expected_types, {:i18n_key => 'media.types', :include_blank => true}).once
          upload_type_select_tag :medium, :type, exclude: ['EmbeddedMedium']
        end
      end
    end

    describe '#preview_medium' do
      let(:media_partials) do
        {
          build(:audio_medium) => 'audio_medium/preview',
          build(:embedded_medium) => 'embedded_medium/preview',
          build(:file_medium) => 'file_medium/preview',
          build(:image_medium) => 'image_medium/preview',
        }
      end

      it 'uses the correct partial for each of the media types and passes the media object to it' do
        media_partials.each do |(object, partial)|
          self.expects(:render).with(partial: partial, locals: { medium: object }, formats: [:html]).once
          preview_medium(object)
        end
      end

      describe 'prefixing' do
        let(:medium) { build(:image_medium) }

        it 'ignores the prefix if it is blank' do
          prefix = ''
          partial = 'image_medium/preview'
          self.expects(:render).with(partial: partial, locals: { medium: medium }, formats: [:html]).once
          preview_medium(medium, prefix: prefix)
        end

        it 'prefixes the view name with the prefix' do
          prefix = 'index'
          partial = "image_medium/#{prefix}_preview"
          self.expects(:render).with(partial: partial, locals: { medium: medium }, formats: [:html]).once
          preview_medium(medium, prefix: prefix)
        end
      end

      describe 'specifying a controller' do
        let(:medium) { build(:file_medium) }

        it 'ignores the controller if it is blank' do
          controller = ''
          partial = 'file_medium/preview'
          self.expects(:render).with(partial: partial, locals: { medium: medium }, formats: [:html]).once
          preview_medium(medium, controller: controller)
        end

        it 'adds a controller reference to the partial name' do
          controller = 'articles'
          partial = "#{controller}/file_medium/preview"
          self.expects(:render).with(partial: partial, locals: { medium: medium }, formats: [:html]).once
          preview_medium(medium, controller: controller)
        end
      end

      describe 'combining prefix and controller' do
        let(:medium) { build(:embedded_medium) }

        it 'returns the correct path to the partial' do
          controller = 'other_controller'
          prefix = 'my_custom'
          partial = "#{controller}/embedded_medium/#{prefix}_preview"
          self.expects(:render).with(partial: partial, locals: { medium: medium }, formats: [:html]).once
          preview_medium(medium, prefix: prefix, controller: controller)
        end
      end
    end

    describe '#form_upload_medium_path' do
      describe 'when a new medium is passed' do
        let(:non_persisted_medium) { build :image_medium }

        it 'uses the media#create route' do
          form_upload_medium_path(non_persisted_medium).must_equal media_path
        end
      end

      describe 'when an already persisted medium is passed' do
        let(:persisted_medium) { create :image_medium }

        it 'uses the media#update route' do
          form_upload_medium_path(persisted_medium).must_equal medium_path(persisted_medium)
        end
      end
    end
  end
end