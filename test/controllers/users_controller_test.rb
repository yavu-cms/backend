require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  describe UsersController do
    let(:override_login_user) { nil }
    let(:permissions) { %w[ user/read user/create user/update user/destroy ] }

    before do
      login(override_login_user, permissions: permissions)
      @controller = UsersController.new
    end

    describe 'Permissions sanity checks' do
      let(:permissions) { %w[ user/read ] }

      describe 'creating a new user' do
        describe 'when trying to get new' do
          it 'must be forbidden' do
            must_be_forbidden ->{ get :new }
          end
        end

        describe 'when trying to post create' do
          it 'must be forbidden' do
            must_be_forbidden ->{ post :create, user: attributes_for(:user) }
          end
        end
      end

      describe 'updating users' do
        describe 'without the :update permission' do
          describe 'when editing a user different from the logged in one' do
            let(:user) { create :user }

            it 'must be forbidden' do
              must_be_forbidden ->{ get :edit, id: user }
            end
          end

          describe 'when editing a the logged in user' do
            it 'must be forbidden' do
              must_be_forbidden ->{ get :edit, id: authenticated }
            end
          end

          describe 'when updating the profile' do
            describe 'trying to get :profile' do
              it 'must succeed' do
                get :profile
                must_render :profile
              end
            end

            describe 'trying to patch :update_profile' do
              it 'must succeed' do
                patch :update_profile, user: {email: 'another-email@test.com' }
                must_redirect_to profile_path
              end
            end
          end
        end

        describe 'with the :update permission' do
          let(:permissions) { %w[ user/update ] }

          describe 'when editing a user different from the logged in one' do
            let(:user) { create :user }

            it 'must succeed' do
              get :edit, id: user
              must_render :edit
            end
          end

          describe 'when editing a the logged in user' do
            it 'must succeed' do
              get :edit, id: authenticated
              must_render :edit
            end
          end

          describe 'when updating the profile' do
            describe 'trying to get :profile' do
              it 'must succeed' do
                get :profile
                must_render :profile
              end
            end

            describe 'trying to patch :update_profile' do
              it 'must succeed' do
                patch :update_profile, user: {email: 'another-email@test.com' }
                must_redirect_to profile_path
              end
            end
          end
        end
      end

      describe 'destroying users' do
        describe 'without :destroy permission' do
          describe 'when trying to destroy another user' do
            let(:user) { create :user }

            it 'must be forbidden' do
              must_be_forbidden ->{ delete :destroy, id: user }
            end
          end

          describe 'when trying to destroy the logged in user' do
            it 'must be forbidden' do
              must_be_forbidden ->{ delete :destroy, id: authenticated }
            end
          end
        end

        describe 'with :destroy permission' do
          let(:permissions) { %w[ user/destroy ] }

          describe 'when trying to destroy another user' do
            let(:user) { create :user }

            it 'must succeed' do
              delete :destroy, id: user
              must_redirect_to users_path
            end
          end

          describe 'when trying to destroy the logged in user' do
            it 'must be forbidden' do
              must_be_forbidden ->{ delete :destroy, id: authenticated }
            end
          end
        end
      end
    end

    describe 'get index' do
      describe 'on a regular request' do
        it 'succeeds' do
          get :index
          must_succeed
          assigns(:users).wont_be_nil
        end
      end

      describe 'when specifying a page' do
        before do
          # there is always an 'admin' user
          @user_1 = create :user, created_at: 3.minutes.ago
          @user_2 = create :user, created_at: 2.minutes.ago
          @user_3 = create :user, created_at: 1.minutes.ago
        end

        it 'shows records for that page' do
          get :index, { page: 4, per_page: 1 }
          must_render :index
          assigns(:users).count.must_equal 1
          assigns(:users).must_equal [@user_3]
        end
      end

      describe 'when specifying the number of elements per page to show' do
        before do
          10.times { create :user }
        end

        it 'shows at most that number of records' do
          get :index, { per_page: 5 }
          must_render :index
          assigns(:users).count.must_equal 5
        end
      end

      describe 'when filtering results' do
        before do
          @user_1 = create :user, username: 'John'
          @user_2 = create :user, username: 'Rick Astley'
          @user_3 = create :user, username: 'Rickrolled'
        end

        it 'show the matching records' do
          get :index, { filter: { user: 'Rick' } }
          assigns(:users).to_a.must_equal [@user_2, @user_3]
        end
      end

      describe 'when reset is submitted' do
        before do
          get :index, { commit: I18n.t('activerecord.actions.shared.clear_filter') }
        end

        it 'clear the filters and redirects to index' do
          assigns(:filter).values.must_equal UsersFilter.defaults
          must_redirect_to users_url
        end
      end
    end

    describe 'get new' do
      it 'succeeds' do
        get :new
        must_succeed
      end
    end

    describe 'create' do
      describe 'when submitted values are valid' do
        before do
          post :create, user: attributes_for(:user, { username: 'other' })
        end

        it 'adds a new user and redirects to the show page' do
          User.find_by(username: 'other').wont_be_nil
          assigns(:user).wont_be :new_record?
          must_redirect_to user_path(assigns(:user))
        end
      end

      describe 'when submitted values are invalid' do
        describe 'when username is empty' do
          it 'returns an error' do
            post :create, user: attributes_for(:user, { username: nil })
            must_render :new
            must_display_form_errors
            assigns(:user).must_be :new_record?
          end
        end
        describe 'when email is in use' do
          let(:email_in_use) { 'test@test.com' }

          before do
            create :user, email: email_in_use
          end

          it 'renders errors' do
            post :create, user: attributes_for(:user, email: email_in_use)
            must_render :new
            must_display_form_errors
            assigns(:user).must_be :new_record?
          end
        end
      end
    end

    describe 'show' do
      it 'succeds' do
        get :show, id: create(:user)
        must_succeed
      end
    end

    describe 'edit' do
      it 'succeeds' do
        get :edit, id: create(:user)
        must_succeed
      end
    end

    describe 'update' do
      let(:user) { create :user, new_password: 'passw0rd' }

      describe 'when submitted values are valid' do
        it 'updates the user and redirects to the show page' do
          patch :update, id: user, user: { username: 'changed user' }

          assigns(:user).username.must_equal 'changed user'
          must_redirect_to user_url(assigns(:user))
        end
      end

      describe 'when no password is provided' do
        it "doesn't change the current password" do
          user.expects(:password=).never

          patch :update, id: user, user: { new_password: '', new_password_confirmation: '' }

          assigns(:user).new_password.must_be_nil
        end
      end

      describe 'when a new password is provided' do
        let(:new_password) { 'yavu123!' }

        before do
          @old_password = user.encrypted_password
        end

        describe 'when the password confirmation is correct' do
          it 'changes the current password' do
            patch :update, id: user, user: { current_password: 'passw0rd', new_password: new_password, new_password_confirmation: new_password }
            must_redirect_to user_url(assigns(:user))
            assigns(:user).encrypted_password.wont_equal @old_password
          end
        end

        describe 'when the password confirmation is not correct' do
          it "doesn't change the current password" do
            patch :update, id: user, user: { current_password: 'passw0rd', new_password: new_password, new_password_confirmation: new_password.reverse }

            assigns(:user).errors.wont_be_empty
            assigns(:user).encrypted_password.must_equal @old_password
          end
        end
      end

      describe 'when submitted values are invalid' do
        it 'returns an error' do
          patch :update, id: create(:user), user: { username: nil, new_password: nil }
          assigns(:user).errors.wont_be_empty
        end
      end
    end

    describe 'get profile' do
      it 'assigns current_user to @user' do
        get :profile
        assigns(:user).must_equal(authenticated)
      end
    end

    describe 'patch profile' do
      describe 'when email is changed' do
        it 'modifies user' do
          patch :update_profile, id: authenticated, user: { email: 'new_email@mail.com' }
          assigns(:user).email.must_equal 'new_email@mail.com'
        end
      end

      describe 'when new password is specified' do
        let(:passwd) { 'abcdefgh123!' }
        let(:new_passwd) { passwd.reverse }
        let(:override_login_user) { create :admin, new_password: passwd }

        before do
          @old_crypted_password = authenticated.encrypted_password
        end

        describe 'when the current password is correct' do
          it 'changes the password' do
            patch :update_profile, id: authenticated, user: { current_password: passwd, new_password: new_passwd, new_password_confirmation: new_passwd }
            assigns(:user).encrypted_password.wont_equal @old_crpyted_password
          end
        end

        describe 'when the current password isn\'t correct' do
          before do
            @old_crypted_password = authenticated.encrypted_password
          end

          it "doesn't change the password" do
            patch :update_profile, user: { current_password: 'not the current password', new_password: new_passwd, new_password_confirmation: new_passwd }
            assigns(:user).encrypted_password.must_equal @old_crypted_password
          end
        end

        describe 'when submitted values are invalid' do
          it 'returns an error' do
            patch :update_profile, user: { new_password: 'aaba' }
            assigns(:user).errors.wont_be_empty
          end
        end

        describe 'when current_password is empty' do
          before do
            @user = authenticated
            @old_crypted_password = @user.encrypted_password
          end

          it "doesn't change the password" do
            patch :update_profile, user: { new_password: 'abcd', new_password_confirmation: 'abcd' }
            @user.reload
            @user.encrypted_password.must_equal @old_crypted_password
          end
        end
      end
    end

    describe 'destroy' do
      let(:user) { create :user }

      describe 'when the user can be destroyed' do
        it 'removes the user' do
          delete :destroy, id: user
          must_redirect_to users_path
          must_have_notice 'users.destroy.messages.success'
          ->{ user.reload }.must_raise ActiveRecord::RecordNotFound
        end
      end

      describe 'when the user cannot be destroyed' do
        before { User.any_instance.expects(:destroy).once.returns(false) }
        after { User.any_instance.unstub(:destroy) }

        it 'does not remove the user' do
          delete :destroy, id: user
          must_redirect_to users_path
          must_have_alert 'users.destroy.messages.alert'
          user.wont_be :destroyed?
        end
      end
    end
  end
end
