require 'test_helper'

class ComponentsControllerTest < ActionController::TestCase
  describe ComponentsController do
    let(:permissions) { %w[ component/read component/manage component/destroy ] }

    before do
      login permissions: permissions
      @controller = ComponentsController.new
    end

    describe 'Permissions sanity checks' do
      let(:permissions) { %w[ component/read ] }
      let(:component) { create :component }

      describe 'create a new component' do
        describe 'when trying to get new' do
          it 'must be forbidden' do
            must_be_forbidden ->{ get :new }
          end
        end

        describe 'when trying to post create' do
          it 'must be forbidden' do
            must_be_forbidden ->{ post :create, component: attributes_for(:component) }
          end
        end
      end

      describe 'update a component' do
        describe 'when trying to get edit' do
          it 'must be forbidden' do
            must_be_forbidden ->{ get :edit, id: component }
          end
        end

        describe 'when trying to put update' do
          it 'must be forbidden' do
            must_be_forbidden ->{ put :update, id: component }
          end
        end

        describe 'when trying to get enable' do
          it 'must be forbidden' do
            must_be_forbidden ->{ get :enable, id: component }
          end
        end

        describe 'when trying to get disable' do
          it 'must be forbidden' do
            must_be_forbidden ->{ get :disable, id: component }
          end
        end
      end

      describe 'destroy a component' do
        describe 'when trying to delete destroy' do
          it 'must be forbidden' do
            must_be_forbidden ->{ delete :destroy, id: component }
          end
        end
      end
    end

    describe 'get index' do
      describe 'on a regular request' do
        describe 'when succeeds' do
          before do
            @component = create :component
            get :index
          end

          it 'must succeed' do
            must_render :index
            assigns(:components).wont_be_nil
          end

          it 'must render common actions' do
            assert_select "#action-dropdown-#{@component.id} a[href=#{component_path(@component)}]", I18n.t('activerecord.actions.shared.show')
            assert_select "#action-dropdown-#{@component.id} a[href=#{edit_component_path(@component)}]"
            assert_select "#action-dropdown-#{@component.id} a[href=#{component_path(@component)}]", I18n.t('activerecord.actions.shared.destroy')
          end
        end

        describe 'when cover?' do
          before do
            @cover = create :cover_component
            get :index
          end

          it 'wont show views action' do
            assert_select "#action-dropdown-#{@cover.id} a[href=#{component_translations_path(@cover)}]", 0
          end
          it 'wont show assets action' do
            assert_select "#action-dropdown-#{@cover.id} a[href=#{component_translations_path(@cover)}]", 0
          end
          it 'wont show translations action' do
            assert_select "#action-dropdown-#{@cover.id} a[href=#{component_translations_path(@cover)}]", 0
          end
        end
      end

      describe 'when specifying a sort criterion' do
        before do
          @component_1 = create :component, name: 'Ahhlalala'
          @component_2 = create :component, name: 'Boahahaha'
          @component_3 = create :component, name: 'ZZLast one'
        end

        it 'sorts results using it' do
          get :index, { sort: { field: 'name', order: 'asc' } }
          assigns(:components).must_equal [@component_1, @component_2, @component_3]
          must_render :index
        end
      end

      describe 'when specifying a page' do
        let(:per_page) { 2 }
        let(:page_number) { 3 }

        before do
          10.times { |x| create(:component, name: "Component name #{"%02d" % (x+1) }", created_at: (10 - x).minutes.ago) }
        end

        it 'shows records for that page' do
          get :index, per_page: per_page, page: page_number
          assigns(:components).first.name.must_equal "Component name 05"
          assigns(:components).last.name.must_equal "Component name 06"
        end
      end

      describe 'when specifying the number of elements per page to show' do
        let(:per_page) { 3 }

        before do
          create_list(:component, 8)
        end

        it 'shows at most that number of records' do
          get :index, per_page: per_page
          must_render :index
          assigns(:components).count.must_equal per_page
        end
      end

      describe 'when filtering results' do
        before do
          @component1 = create :component, name: 'A title'
          @component2 = create :component, name: 'Other title'
          @component3 = create :component, name: 'Bla bla title'
        end

        it 'show the matching records' do
          get :index, { filter: { name: 'Bla' } }
          assigns(:components).to_a.must_equal [@component3]
        end
      end

      describe 'when reset is submitted' do
        before do
          get :index, { commit: I18n.t('activerecord.actions.shared.clear_filter') }
        end

        it 'clear the filters and redirects to index' do
          assigns(:filter).values.must_equal ComponentsFilter.defaults
          must_redirect_to components_url
        end
      end
    end


    describe 'GET new' do
      it 'must succeed' do
        get :new
        must_render :new
        assigns(:component).must_be :new_record?
        assigns(:component).must_be :instance_of?, BaseComponent
        assert_select 'h1', I18n.t('components.new.title', component_type: Component.model_name.human)
      end
    end

    describe 'POST create' do
      describe 'when type is Component' do
        describe 'when succeeds' do
          it "adds a component" do
            it_adds_a Component, options: { component: attributes_for(:component, type: 'Component') }
            assigns(:component).wont_be :new_record?
            must_redirect_to component_url(assigns(:component))
            must_have_notice 'components.new.messages.success'
          end
        end

        describe 'when fails' do
          it "shows again new page and displays the validation errors" do
            # Must raise an error because views name is nil
            post :create, component: attributes_for(:component, type: 'Component', name: nil),
                          component_id: @component
            must_render :new
            assigns(:component).must_be :new_record?
            assigns(:component).must_be_instance_of Component
            must_display_form_errors
            assert_select 'h1', I18n.t('components.new.title', component_type: Component.model_name.human)
          end
        end
      end

      describe 'when type is CoverComponent' do
        describe 'when succeeds' do
          it "adds a component" do
            it_adds_a BaseComponent, factory: :component, options: { component: attributes_for(:component, type: 'CoverComponent') }
            assigns(:component).wont_be :new_record?
            assigns(:component).must_be_instance_of CoverComponent
            must_redirect_to component_url(assigns(:component))
            must_have_notice 'components.new.messages.success'
          end
        end
      end

      describe 'when type is CoverArticlesComponent' do
        describe 'when succeeds' do
          it "adds a component" do
            it_adds_a BaseComponent, factory: :component, options: { component: attributes_for(:component, type: 'CoverArticlesComponent') }
            assigns(:component).wont_be :new_record?
            assigns(:component).must_be_instance_of CoverArticlesComponent
            must_redirect_to component_url(assigns(:component))
            must_have_notice 'components.new.messages.success'
          end
        end
      end
    end

    describe 'GET show' do
      it 'must succeed' do
        get :show, id: create(:component)
        must_render :show
        component = assigns(:component)
        component.wont_be :new_record?
        component.must_be :instance_of?, Component
        assert_select 'h1', I18n.t('components.show.title', component_type: Component.model_name.human, component_name: component.name)
      end

      describe 'when CoverComponent' do
        it 'must assigns a CoverComponent' do
          get :show, id: create(:cover_component)
          component = assigns(:component)
          component.must_be :instance_of?, CoverComponent
          assert_select 'h1', I18n.t('components.show.title', component_type: CoverComponent.model_name.human, component_name: component.name)
        end
      end

      describe 'when CoverArticlesComponent' do
        it 'must assigns a CoverArticlesComponent' do
          get :show, id: create(:cover_articles_component)
          component = assigns(:component)
          component.must_be :instance_of?, CoverArticlesComponent
          assert_select 'h1', I18n.t('components.show.title', component_type: CoverArticlesComponent.model_name.human, component_name: component.name)
        end
      end

    end

    describe 'GET edit' do
      it 'must succeed' do
        get :edit, id: create(:component)
        must_render :edit
        assigns(:component).wont_be :new_record?
      end
    end

    describe "PATCH update" do
      describe 'when succeeds' do
        it 'updates the component and redirects to edit' do
          component = create(:component, name: 'a name')

          patch :update,
            id: component,
            component: { name: 'other name'}

          must_redirect_to edit_component_url(assigns(:component))
          assigns(:component).wont_be :new_record?
          assigns(:component).name.must_equal 'other name'
          must_have_notice 'components.edit.messages.success'
        end
      end

      describe 'when fails' do
        it "shows again edit page and displays the validation errors" do
          component = create(:component)
          # try to edit without name
          patch :update,
            id: component,
            component: {name: nil}

          must_render :edit
          assigns(:component).wont_be :new_record?
          must_display_form_errors
        end
      end
    end

    describe 'get enable' do
      let(:component) { create :component, enabled: false }

      it 'enables the component' do
        get :enable, id: component
        component.reload.must_be :enabled?
      end

      it 'redirects to component index' do
        get :enable, id: component
        must_redirect_to components_path
        must_have_notice 'components.enable.messages.success', component_name: component.name
      end
    end

    describe 'get disable' do
      let(:component) { create :component }

      it 'disables the component' do
        get :disable, id: component
        component.reload.wont_be :enabled?
      end

      it 'redirects to component index' do
        get :disable, id: component
        must_redirect_to components_path
        must_have_notice 'components.disable.messages.success', component_name: component.name
      end
    end

    describe 'delete destroy' do
      it 'deletes the component' do
        it_deletes_a Component, factory: :component
        must_redirect_to components_url
        must_have_notice 'components.destroy.messages.success'
      end

      it 'wont deletes the component if it is used' do
        component = create :component
        create(:component_configuration, component: component)
        it_doesnt_delete_a Component, factory: component
        must_redirect_to components_url
        must_have_alert 'components.destroy.messages.fail'
      end
    end

    describe 'GET copy' do
      it 'must succeed' do
        get :copy, id: create(:component)
        must_render :copy
        assigns(:component).must_be :new_record?
      end
    end

    describe 'POST create_copy' do
      describe 'when succeeds' do
        it 'creates the copied compoennt and redirects to show' do
          component = create(:component, name: 'a name')

          post :create_copy, id: component, component: { name: 'other name'}

          must_redirect_to component_url(assigns(:component))
          assigns(:component).wont_be :new_record?
          assigns(:component).name.must_equal 'other name'
          must_have_notice 'components.copy.messages.success'
        end
      end

      describe 'when fails' do
        it "shows again copy page and displays the validation errors" do
          component = create(:component)
          # try to edit without name
          post :create_copy, id: component, component: {name: nil}
          must_render :copy
          assigns(:component).must_be :new_record?
          must_display_form_errors
        end
      end
    end

    describe 'GET search' do
      before do
        @component_1 = create :component, name: 'this should match'
        create :component, name: 'this not'
        create :component, name: 'nope nope'
        @controller.expects(:search_limit).returns(908).once
      end

      it 'should return matching components in a JSON' do
        get :search, q: 'match'
        JSON.parse(response.body).first['name'].must_equal 'this should match'
      end
    end

    describe 'GET configuration_form' do
      before do
        pretend_user_can :configuration_form, BaseComponent
      end

      describe 'when the requested component is a Component' do
        before do
          @component = create :component
          xhr :get, :configuration_form, id: @component
        end

        it 'succeeds' do
          must_render 'components/configuration/_configuration_form'
        end
      end

      describe 'when the requested component is a CoverComponent' do
        before do
          @component = create :cover_component
          xhr :get, :configuration_form, id: @component
        end

        it 'succeeds' do
          must_render 'components/configuration/_configuration_form'
        end
      end

      describe 'when the requested component is a CoverArticlesComponent' do
        before do
          @component = create :cover_articles_component
          xhr :get, :configuration_form, id: @component
        end

        it 'succeeds' do
          must_render 'components/configuration/_configuration_form'
        end
      end
    end
  end
end
