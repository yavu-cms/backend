require 'test_helper'

class MediaControllerSTITest < ActionController::TestCase
  describe MediaController do
    let(:permissions) { %w[ medium/read medium/create medium/update/all medium/destroy/all ] }

    before do
      login permissions: permissions
      @controller = MediaController.new
    end

    describe 'Image' do
      let(:image_medium) { create :image_medium }

      describe 'Actions' do
        describe 'GET show' do
          it 'succeeds' do
            get :show, id: image_medium
            must_render :show
            assigns(:medium).must_equal image_medium
          end
        end

        describe 'GET new' do
          it 'must succeed' do
            get :new_image
            must_render :new
            assigns(:medium).must_be :new_record?
            assigns(:medium).must_be_instance_of ImageMedium
          end
        end

        describe 'POST create' do
          it 'must succeed' do
            post :create, medium: attributes_for(:image_medium)
            must_redirect_to medium_path(assigns(:medium))
          end
        end

        describe 'GET edit' do
          it 'must succeed' do
            get :edit, id: image_medium
            must_render :edit
            assigns(:medium).wont_be :new_record?
          end
        end

        describe 'PATCH update' do
          describe 'when a valid file is provided' do
            it 'must succeed' do
              old_file = image_medium.file.file
              patch :update, id: image_medium, medium: attributes_for(:image_medium, file: upload_test_file('image_2.jpg'))
              image_medium.reload
              image_medium.file.file.wont_equal old_file
              must_redirect_to medium_path(assigns(:medium))
            end
          end

          describe 'when an invalid file is provided' do
            it 'must fail' do
              old_file = image_medium.file.file
              patch :update, id: image_medium, medium: attributes_for(:image_medium, file: upload_test_file('canario.mp3'))
              image_medium.reload
              image_medium.file.file.must_equal old_file
              must_render :edit
              must_display_form_errors
            end
          end
        end

        describe 'DELETE destroy' do
          it 'must succeed' do
            delete :destroy, id: image_medium
          end
        end
      end
    end

    describe 'File' do
      let(:file_medium) { create :file_medium }

      describe 'Actions' do
        describe 'show' do
          it 'must succeed' do
            get :show, id: file_medium
            must_render :show
            assigns(:medium).must_equal file_medium
          end
        end

        describe 'GET new' do
          it 'must succeed' do
            get :new_file
            must_render :new
            assigns(:medium).must_be :new_record?
            assigns(:medium).must_be_instance_of FileMedium
          end
        end

        describe 'POST create' do
          it 'must succeed' do
            post :create, medium: attributes_for(:file_medium)
            must_redirect_to medium_path(assigns(:medium))
          end
        end

        describe 'GET edit' do
          it 'must succeed' do
            get :edit, id: file_medium
            must_render :edit
            assigns(:medium).wont_be :new_record?
          end
        end

        describe 'PATCH update' do
          describe 'when a valid file is provided' do
            it 'must succeed' do
              old_file = file_medium.file.file
              patch :update, id: file_medium, medium: attributes_for(:file_medium, file: upload_test_file('canario.mp3'))
              file_medium.reload
              file_medium.file.file.wont_equal old_file
              must_redirect_to medium_path(assigns(:medium))
            end
          end
        end

        describe 'DELETE destroy' do
          it 'must succeed' do
            delete :destroy, id: file_medium
          end
        end
      end
    end

    describe 'Embedded' do
      let(:embedded_medium) { create :embedded_medium }

      describe 'Actions' do
        describe 'show' do
          it 'must succeed' do
            get :show, id: embedded_medium
            must_render :show
            assigns(:medium).must_equal embedded_medium
          end
        end

        describe 'GET new' do
          it 'must succeed' do
            get :new_embedded
            must_render :new
            assigns(:medium).must_be :new_record?
            assigns(:medium).must_be_instance_of EmbeddedMedium
          end
        end

        describe 'POST create' do
          it 'must succeed' do
            post :create, medium: attributes_for(:embedded_medium)
            must_redirect_to medium_path(assigns(:medium))
          end
        end

        describe 'PATCH update' do
          describe 'when content is not blank' do
            it 'must succeed' do
              patch :update, id: embedded_medium, medium: attributes_for(:embedded_medium, content: 'new content')
              embedded_medium.reload
              embedded_medium.content.must_equal 'new content'
              must_redirect_to medium_url(assigns(:medium))
            end
          end

          describe 'when content is blank' do
            it 'must fail' do
              patch :update, id: embedded_medium, medium: attributes_for(:embedded_medium, content: '')
              must_render :edit
              must_display_form_errors
            end
          end
        end

        describe 'GET edit' do
          it 'must succeed' do
            get :edit, id: embedded_medium
            must_render :edit
            assigns(:medium).wont_be :new_record?
          end
        end

        describe 'destroy' do
          it 'must succeed' do
            delete :destroy, id: embedded_medium
          end
        end
      end
    end

    describe 'Audio' do
      let(:audio_medium) { create :audio_medium }

      describe 'Actions' do
        describe 'show' do
          it 'must succeed' do
            get :show, id: audio_medium
            must_render :show
            assigns(:medium).must_equal audio_medium
          end
        end

        describe 'GET new' do
          it 'must succeed' do
            get :new_audio
            must_render :new
            assigns(:medium).must_be :new_record?
            assigns(:medium).must_be_instance_of AudioMedium
          end
        end

        describe 'POST create' do
          it 'must succeed' do
            post :create, medium: attributes_for(:audio_medium)
            must_redirect_to medium_path(assigns(:medium))
          end
        end

        describe 'GET edit' do
          it 'must succeed' do
            get :edit, id: audio_medium
            must_render :edit
            assigns(:medium).wont_be :new_record?
          end
        end

        describe 'PATCH update' do
          describe 'when a valid file is provided' do
            it 'must succeed' do
              old_file = audio_medium.file.file
              patch :update, id: audio_medium, medium: attributes_for(:audio_medium, file: upload_test_file('mario_bros.mp3'))
              audio_medium.reload
              audio_medium.file.file.wont_equal old_file
              must_redirect_to medium_path(assigns(:medium))
            end
          end

          describe 'when an invalid file is provided' do
            it 'must fail' do
              old_file = audio_medium.file.file
              patch :update, id: audio_medium, medium: attributes_for(:audio_medium, file: upload_test_file('image_2.jpg'))
              audio_medium.reload
              audio_medium.file.file.must_equal old_file
              must_render :edit
              must_display_form_errors
            end
          end
        end

        describe 'DELETE destroy' do
          it 'must succeed' do
            delete :destroy, id: audio_medium
            must_redirect_to media_path, true
            must_have_notice 'media.destroy.messages.success'
          end
        end
      end
    end
  end
end
