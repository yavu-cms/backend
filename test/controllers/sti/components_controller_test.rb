require 'test_helper'

class ComponentsControllerSTITest < ActionController::TestCase
  describe ComponentsController do
    let(:permissions) { %w[ component/read component/manage component/destroy ] }

    before do
      login permissions: permissions
      @controller = ComponentsController.new
    end

    describe 'Component' do
      let (:component) { create :component }

      describe 'GET show' do
        it 'must succeed' do
          get :show, id: component
          must_render :show
          assigns(:component).must_equal component
        end
      end

      describe 'POST create' do
        it 'must succeed' do
          post :create, component: attributes_for(:component)
          must_redirect_to component_path(assigns(:component))
        end
      end

      describe 'GET edit' do
        it 'must succeed' do
          get :edit, id: component
          must_render :edit
        end
      end

      describe 'PATCH update' do
        it 'must succeed' do
          patch :update, id: component, component: { readme: 'r e a d m e' }
          must_redirect_to edit_component_path(assigns(:component))
          assigns(:component).readme.must_equal 'r e a d m e'
        end
      end

      describe 'DELETE destroy' do
        it 'must succeed' do
          delete :destroy, id: component
          must_redirect_to components_path
          must_have_notice 'components.destroy.messages.success'
        end
      end
    end

    describe 'Cover Article Component' do
      let (:cover_article_component) { create :cover_articles_component }

      describe 'GET show' do
        it 'must succeed' do
          get :show, id: cover_article_component
          must_render :show
          assigns(:component).must_equal cover_article_component
        end
      end

      describe 'POST create' do
        it 'must succeed' do
          post :create, component: attributes_for(:cover_articles_component)
          must_redirect_to component_path(assigns(:component))
        end
      end

      describe 'GET edit' do
        it 'must succeed' do
          get :edit, id: cover_article_component
          must_render :edit
        end
      end

      describe 'PATCH update' do
        it 'must succeed' do
          patch :update, id: cover_article_component, component: { description: 'some desc' }
          must_redirect_to edit_component_path(assigns(:component))
          assigns(:component).description.must_equal 'some desc'
        end
      end

      describe 'DELETE destroy' do
        it 'must succeed' do
          delete :destroy, id: cover_article_component
          must_redirect_to components_path
          must_have_notice 'components.destroy.messages.success'
        end
      end
    end

    describe 'Cover Component' do
      let (:cover) { create :cover_component }

      describe 'GET show' do
        it 'must succeed' do
          get :show, id: cover
          must_render :show
          assigns(:component).must_equal cover
        end
      end

      describe 'POST create' do
        it 'must succeed' do
          post :create, component: attributes_for(:cover_component)
          must_redirect_to component_path(assigns(:component))
        end
      end

      describe 'GET edit' do
        it 'must succeed' do
          get :edit, id: cover
          must_render :edit
        end
      end

      describe 'PATCH update' do
        it 'must succeed' do
          patch :update, id: cover, component: { context: 'a context' }
          must_redirect_to edit_component_path(assigns(:component))
          assigns(:component).context.must_equal 'a context'
        end
      end

      describe 'DELETE destroy' do
        it 'must succeed' do
          delete :destroy, id: cover
          must_redirect_to components_path
          must_have_notice 'components.destroy.messages.success'
        end
      end
    end
  end
end