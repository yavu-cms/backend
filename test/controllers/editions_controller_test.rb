require 'test_helper'

class EditionsControllerTest < ActionController::TestCase
  describe EditionsController do
    let(:permissions) { %w[ edition/read edition/create edition/update/all edition/change_active edition/destroy/all ] }
    before do
      login permissions: permissions
      @controller = EditionsController.new
    end

    describe 'Permissions sanity checks' do
      describe 'create a new edition' do
        describe 'without permissions' do
          let(:permissions) { [] }

          describe 'when trying to get new' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :new }
            end
          end

          describe 'when trying to post create' do
            it 'must be forbidden' do
              must_be_forbidden -> { it_doesnt_add_a Edition, factory: :edition }
            end
          end
        end
      end

      describe 'read edition' do
        describe 'without permissions' do
          let(:permissions) { [] }
          let(:active_edition) { create :active_edition }
          let(:non_active_edition) { create :inactive_edition }
          
          describe 'when trying to get index' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :index }
            end
          end

          describe 'when trying to get show' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :show, id: active_edition }
            end
          end

          describe 'when trying to get search' do
            let(:query) { 'Lovely query' }
            let(:edition) { create :edition }
            let(:matches) { stub }
            let(:search_limit) { 99 }

            # Basic permissions for users
            it 'succeeds' do
              @controller.expects(:search_limit).returns(search_limit).once
              matches.expects(:limit).with(search_limit).returns([edition]).once
              Edition.expects(:search).with(query: query).returns(matches).once
              get :search, q: query
              must_succeed
              results = JSON.parse(response.body)
              results.wont_be_empty
              results.first['text'].must_equal edition.to_s
            end
          end

          describe 'when trying to get articles' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :articles, id: non_active_edition }
            end
          end
        end
      end

      describe 'update edition' do
        let(:active_edition) { create :active_edition }
        let(:non_active_edition) { create :inactive_edition }

        describe 'without permissions' do
          let(:permissions) { [] }  
          
          describe 'when trying to get edit' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :edit, id: active_edition }
            end
          end

          describe 'when trying to put update' do
            it 'must be forbidden' do
              active_edition.name = 'sarasa'
              non_active_edition.name = 'otra sarasa'
              must_be_forbidden -> { put :update, id: active_edition, article: active_edition.attributes }
              must_be_forbidden -> { put :update, id: non_active_edition, article: non_active_edition.attributes }
            end
          end
        end

        describe 'when have :non_active permission' do
          let(:permissions) { %w[ edition/update/non_active ] }

          describe 'when trying to get edit active edition' do
            it 'it be forbidden' do
              must_be_forbidden -> { get :edit, id: active_edition }
            end
          end

          describe 'when trying to get edit inactive edition' do
            it 'succeed' do
              get :edit, id: non_active_edition
              must_render :edit
              assigns(:edition).must_equal non_active_edition
            end
          end

          describe 'when trying to put update active edition' do
            it 'it be forbidden' do
              active_edition.name = 'sarasa'
              must_be_forbidden -> { put :update, id: active_edition, article: active_edition.attributes }
            end
          end

          describe 'when trying to put update inactive edition' do
            it 'succeed' do
              non_active_edition.name = 'otra sarasa'
              put :update, id: non_active_edition, edition: non_active_edition.attributes
              assigns(:edition).name.must_equal 'otra sarasa'
              must_redirect_to edition_url(assigns(:edition))
            end
          end
        end
      end

      describe 'destroy edition' do
        let(:active_edition) { create :active_edition }
        let(:non_active_edition) { create :inactive_edition }
        
        describe 'without permissions' do
          let(:permissions) { [] }
          
          describe 'when trying to destroy edition' do
            it 'must be forbidden' do
              must_be_forbidden -> { it_doesnt_delete_a Edition, factory: non_active_edition }
              must_be_forbidden -> { it_doesnt_delete_a Edition, factory: active_edition }
            end
          end
        end

        describe 'when have :non_active permission' do
          let(:permissions) { %w[ edition/destroy/non_active ] }

          describe 'when trying to destroy active edition' do
            it 'must be forbidden' do
              must_be_forbidden -> { it_doesnt_delete_a Edition, factory: active_edition }
            end
          end

          describe 'when trying to destroy inactive edition' do
            it 'succeeds' do
              it_deletes_a Edition, factory: non_active_edition
              must_redirect_to editions_url, true
              must_have_notice 'editions.destroy.messages.success'
            end
          end
        end
      end
    end

    describe 'get index' do
      before do
        @edition_1 = create :edition, name: 'Boahahaha', created_at: 1.minutes.ago
        @edition_2 = create :edition, name: 'ZZLast one', created_at: 2.minutes.ago
        @edition_3 = create :edition, name: 'Ahhlalala', created_at: 3.minute.ago
      end

      it 'succeeds' do
        get :index
        must_render :index
        assigns(:editions).wont_be_nil
      end

      describe 'when specifying a sort criterion' do
        it 'sorts results using it' do
          get :index, { sort: { field: 'name', order: 'asc' } }
          assigns(:editions).must_equal [@edition_3, @edition_1, @edition_2]
          must_render :index
        end
      end

      describe 'when specifying a page' do
        it 'shows records for that page' do
          get :index, { page: 3, per_page: 1 }
          must_render :index
          assigns(:editions).count.must_equal 1
          assigns(:editions).must_equal [@edition_3]
        end
      end

      describe 'when specifying the number of elements per page to show' do
        before do
          10.times { create :edition }
        end

        it 'shows at most that number of records' do
          get :index, { per_page: 5 }
          must_render :index
          assigns(:editions).count.must_equal 5
        end
      end

      describe 'when filter criteria is submitted' do
        before do
          create(:edition, name: '123', date: Date.today, is_active: false)
          create(:edition, name: 'xyz', date: Date.today, is_active: false)
        end

        it 'show the matching records' do
          get :index, { filter: { name: 'xyz', supplement_id: nil } }
          must_succeed
          assigns(:editions).wont_be_nil
          assigns(:editions).count.must_be :<, Edition.count
        end
      end

      describe 'when reset is submitted' do
        before do
          get :index, { commit: I18n.t('activerecord.actions.shared.clear_filter') }
        end

        it 'clears the filters and redirects to index' do
          assigns[:filter].values.must_equal EditionsFilter.defaults
          must_redirect_to editions_url
        end
      end
    end

    describe 'get new' do
      it 'succeeds' do
        get :new
        must_render :new
      end
    end

    describe 'create' do
      it 'redirects to show' do
        supplement = create :supplement, name: 'supplement1'
        post :create, edition: attributes_for(:edition, supplement_id: supplement.id)

        assigns(:edition).wont_be_nil
        must_redirect_to edition_url(assigns(:edition))
      end

      describe 'when submitted values are not valid' do
        it 'returns an error' do
          post :create, edition: { name: nil, date_string: nil, supplement_id: nil }
          must_display_form_errors
          must_render :new
        end
      end
    end

    describe 'get show' do
      let(:edition) { create :edition }

      it 'succeeds' do
        get :show, id: edition
        must_render :show
      end
    end

    describe 'get edit' do
      let(:edition) { create :edition }

      it 'succeeds' do
        get :edit, id: edition
        must_render :edit
      end
    end

    describe 'patch update' do
      let(:edition) { create :edition }

      describe 'when submitted values are valid' do
        before do
          patch :update, id: edition, edition: attributes_for(:edition, name: 'changed name')
        end

        it 'updates the edition and redirects to the show page' do
          assigns(:edition).name.must_equal 'changed name'
          must_redirect_to edition_url(assigns(:edition))
        end
      end

      describe 'when submitted values are invalid' do
        before do
          patch :update, id: edition, edition: { name: '', date_string: 'invalid date' }
        end

        it 'fails' do
          must_display_form_errors
          must_render :edit
        end
      end
    end

    describe 'put become_active' do
      before do
        @edition = create :edition
        put :become_active, id: @edition
        @edition.reload
      end

      it 'activates the edition in its supplement' do
        @edition.is_active.must_equal true
        @edition.supplement.active_edition.must_equal @edition
      end

      it 'redirects to index with a flash message' do
        must_redirect_to editions_url
        must_have_notice 'editions.become_active.messages.success'
      end
    end

    describe 'destroy' do
      describe 'when edition does not have articles' do
        it 'removes the edition' do
          it_deletes_an Edition, factory: :non_default_supplement_edition
        end

        it 'redirects to index' do
          delete :destroy, id: create(:non_default_supplement_edition)
          must_redirect_to editions_url, true
          must_have_notice 'editions.destroy.messages.success'
        end
      end

      describe 'when edition has articles' do
        it 'does not remove the edition' do
          it_doesnt_delete_an Edition, factory: :edition_with_articles
        end
        it 'redirects to index with an alert message' do
          delete :destroy, id: create(:edition_with_articles)
          must_redirect_to editions_url, true
          must_have_alert 'editions.destroy.messages.error'
        end
      end
    end

    describe 'search' do
      describe 'when a search is being made' do
        let(:query) { 'Lovely query' }
        let(:edition) { create :edition }
        let(:matches) { stub }
        let(:search_limit) { 99 }

        before do
          @controller.expects(:search_limit).returns(search_limit).once
          matches.expects(:limit).with(search_limit).returns([edition]).once
          Edition.expects(:search).with(query: query).returns(matches).once
          get :search, q: query
        end

        after do
          Edition.unstub :search
        end

        it 'returns the matching elements in a JSON response' do
          must_succeed
          results = JSON.parse(response.body)
          results.wont_be_empty
          results.first['text'].must_equal edition.to_s
        end
      end

      describe 'when specific ids are being requested' do
        let(:edition_1) { create :edition, name: 'First edition' }
        let(:edition_2) { create :edition, name: 'Second edition' }
        let(:ids) { [edition_1.id, edition_2.id] }

        before do
          get :search, ids: ids
        end

        it 'returns the requested editions' do
          must_succeed
          results = JSON.parse(response.body)
          results.count.must_equal 2
          results.first['text'].must_equal edition_1.to_s
          results.last['text'].must_equal edition_2.to_s
        end
      end
    end

    describe 'GET articles' do
      let(:edition) { create :edition }
      describe 'when get articles' do
        it 'succeeds' do
          get :articles, id: edition
          must_redirect_to articles_path, true
        end
      end
    end
  end
end
