require 'test_helper'

class RepresentationAssetsControllerTest < ActionController::TestCase
  describe RepresentationAssetsController do
    let(:permissions) { %w[ client_application/read client_application/design_manage ] }

    before do
      login permissions: permissions
      @controller = RepresentationAssetsController.new
      @representation = create :representation_draft
      @client_application = @representation.client_application
      pretend_user_workspace_includes @client_application
      ClientApplicationAssetUploader.root = Rails.root.join(*%w{tmp tests representation_assets_controller})
    end

    after do
      FileUtils.remove_dir(ClientApplicationAssetUploader.root, true)
    end

    describe 'Permissions sanity checks' do
      let(:representation_outside_workspace) { create :representation_draft }
      let(:client_application_outside_workspace) { representation_outside_workspace.client_application }
      let(:asset_outside_workspace) { create :client_application_asset, client_application: client_application_outside_workspace }
      let(:asset) { create :client_application_asset, client_application: @client_application }

      describe 'without client application permissions' do
        let(:permissions) { [] }

        describe 'when trying to get index' do
          it 'must be forbidden' do
            must_be_forbidden -> { get :index, client_application_id: @client_application, representation_id: @representation }
            must_be_forbidden -> { get :index, client_application_id: client_application_outside_workspace, representation_id: representation_outside_workspace }
          end
        end

        describe 'when trying to get show' do
          it 'must be forbidden' do
            must_be_forbidden -> { get :show, client_application_id: @client_application, representation_id: @representation, id: asset }
            must_be_forbidden -> { get :show, client_application_id: client_application_outside_workspace, representation_id: representation_outside_workspace, id: asset_outside_workspace }
          end
        end

        describe 'when trying to get new' do
          it 'must be forbidden' do
            must_be_forbidden -> { get :new, client_application_id: @client_application, representation_id: @representation }
            must_be_forbidden -> { get :new, client_application_id: client_application_outside_workspace, representation_id: representation_outside_workspace }
          end
        end

        describe 'when trying to get new empty' do
          it 'must be forbidden' do
            must_be_forbidden -> { get :new_empty, client_application_id: @client_application, representation_id: @representation }
            must_be_forbidden -> { get :new_empty, client_application_id: client_application_outside_workspace, representation_id: representation_outside_workspace }
          end
        end

        describe 'when trying to get edit' do
          it 'must be forbidden' do
            must_be_forbidden -> { get :edit, client_application_id: @client_application, representation_id: @representation, id: asset }
            must_be_forbidden -> { get :edit, client_application_id: client_application_outside_workspace, representation_id: representation_outside_workspace, id: asset_outside_workspace }
          end
        end

        describe 'when trying to get edit content' do
          it 'must be forbidden' do
            must_be_forbidden -> { get :edit_content, client_application_id: @client_application, representation_id: @representation, id: asset }
            must_be_forbidden -> { get :edit_content, client_application_id: client_application_outside_workspace, representation_id: representation_outside_workspace, id: asset_outside_workspace }
          end
        end

        describe 'when trying to get rename' do
          it 'must be forbidden' do
            must_be_forbidden -> { get :rename, client_application_id: @client_application, representation_id: @representation, id: asset }
            must_be_forbidden -> { get :rename, client_application_id: client_application_outside_workspace, representation_id: representation_outside_workspace, id: asset_outside_workspace }
          end
        end

        describe 'when trying to get toggle into representation' do
          it 'must be forbidden' do
            must_be_forbidden -> { post :toggle_into_representation, client_application_id: @client_application, representation_id: @representation, id: asset }
            must_be_forbidden -> { post :toggle_into_representation, client_application_id: client_application_outside_workspace, representation_id: representation_outside_workspace, id: asset_outside_workspace }
          end
        end

        describe 'when trying to post toggle as favicon' do
          it 'must be forbidden' do
            must_be_forbidden -> { post :toggle_as_favicon, client_application_id: @client_application, representation_id: @representation, id: asset }
            must_be_forbidden -> { post :toggle_as_favicon, client_application_id: client_application_outside_workspace, representation_id: representation_outside_workspace, id: asset_outside_workspace }
          end
        end

        describe 'when trying to patch batch action' do
          it 'must be forbidden' do
            must_be_forbidden -> { patch :batch_action, client_application_id: @client_application, representation_id: @representation, id: asset, batch_action: 1 }
            must_be_forbidden -> { patch :batch_action, client_application_id: client_application_outside_workspace, representation_id: representation_outside_workspace, id: asset_outside_workspace, batch_action: 1 }
          end
        end
      end

      describe 'when have client application permissions' do
        let(:permissions) { %w[ client_application/read client_application/design_manage ] }

        describe 'when trying to get index' do
          it 'must be forbidden' do
            must_be_forbidden -> { get :index, client_application_id: client_application_outside_workspace, representation_id: representation_outside_workspace }
          end
        end

        describe 'when trying to get show' do
          it 'must be forbidden' do
            must_be_forbidden -> { get :show, client_application_id: client_application_outside_workspace, representation_id: representation_outside_workspace, id: asset_outside_workspace }
          end
        end

        describe 'when trying to get new' do
          it 'must be forbidden' do
            must_be_forbidden -> { get :new, client_application_id: client_application_outside_workspace, representation_id: representation_outside_workspace }
          end
        end

        describe 'when trying to get new empty' do
          it 'must be forbidden' do
            must_be_forbidden -> { get :new_empty, client_application_id: client_application_outside_workspace, representation_id: representation_outside_workspace }
          end
        end

        describe 'when trying to get edit' do
          it 'must be forbidden' do
            must_be_forbidden -> { get :edit, client_application_id: client_application_outside_workspace, representation_id: representation_outside_workspace, id: asset_outside_workspace }
          end
        end

        describe 'when trying to get edit content' do
          it 'must be forbidden' do
            must_be_forbidden -> { get :edit_content, client_application_id: client_application_outside_workspace, representation_id: representation_outside_workspace, id: asset_outside_workspace }
          end
        end

        describe 'when trying to get rename' do
          it 'must be forbidden' do
            must_be_forbidden -> { get :rename, client_application_id: client_application_outside_workspace, representation_id: representation_outside_workspace, id: asset_outside_workspace }
          end
        end

        describe 'when trying to get toggle into representation' do
          it 'must be forbidden' do
            must_be_forbidden -> { post :toggle_into_representation, client_application_id: client_application_outside_workspace, representation_id: representation_outside_workspace, id: asset_outside_workspace }
          end
        end

        describe 'when trying to post toggle as favicon' do
          it 'must be forbidden' do
            must_be_forbidden -> { post :toggle_as_favicon, client_application_id: client_application_outside_workspace, representation_id: representation_outside_workspace, id: asset_outside_workspace }
          end
        end

        describe 'when trying to patch batch action' do
          it 'must be forbidden' do
            must_be_forbidden -> { patch :batch_action, client_application_id: client_application_outside_workspace, representation_id: representation_outside_workspace, id: asset_outside_workspace, batch_action: 1 }
          end
        end
      end
    end
    
    describe 'get index' do

      it 'must succeed' do
        # An editable asset
        client_application_asset_editable = create :client_application_asset_editable,
          client_application: @client_application
        # A not editable asset
        client_application_asset_not_editable = create :client_application_asset_not_editable,
          client_application: client_application_asset_editable.client_application
        # Make the request: we expect to get two rows
        get :index, client_application_id: @client_application, representation_id: @representation

        must_render :index
        assigns(:application_assets).count.must_equal 2

        assert_select "#action-dropdown-#{client_application_asset_editable.id} a[href=?]",
          edit_client_application_representation_asset_path( @client_application, @representation, client_application_asset_editable)

        assert_select "#action-dropdown-#{client_application_asset_not_editable.id} a[href=?]",
          edit_content_client_application_representation_asset_path(@client_application, @representation,
                                                     client_application_asset_not_editable), false
      end
    end

    describe 'get new' do
      before do
        get :new, client_application_id: @client_application, representation_id: @representation
      end

      it 'must succeed' do
        must_render 'client_application_assets/new'
        assert_select '#new_client_application_asset[action=?]',
          client_application_assets_path(@client_application)
        assigns(:asset).must_be :new_record?
      end

      # This view is shared with other module so we must check go back action
      it 'must set go back action to client_application_assets_path' do
        assert_select '.context-actions a[href=?]',
          client_application_representation_assets_path(@client_application, @representation), t_action(:representation_assets, :back)
      end

      it 'must set session[client_application_asset.*_path]' do
        session['client_application_asset.list_path'].must_equal client_application_representation_assets_path(@client_application, @representation)
        session['client_application_asset.edit_path'].must_be_nil
        session['client_application_asset.edit_content_path'].must_be_nil
        session['client_application_asset.rename_path'].must_be_nil
        session['client_application_asset.show_path'].must_be_nil
      end
    end

    describe 'get new_empty' do
      before do
        get :new_empty, representation_id: @representation, client_application_id: @client_application
      end

      it 'must succeed' do
        must_render :new_empty
        assigns(:asset).must_be :new_record?
      end

      it 'must set go back action to client_application_assets_path' do
        assert_select '.context-actions a[href=?]',
          client_application_representation_assets_path(@client_application, @representation), t_action(:representation_assets, :back)
      end

      it 'must set session[client_application_asset.*_path]' do
        session['client_application_asset.list_path'].must_equal client_application_representation_assets_path(@client_application, @representation)
        session['client_application_asset.edit_path'].must_be_nil
        session['client_application_asset.edit_content_path'].must_be_nil
        session['client_application_asset.rename_path'].must_be_nil
        session['client_application_asset.show_path'].must_be_nil
      end
    end

    describe 'get show' do
      it 'must succeed when not editable? and wont add action to edit content' do
        asset = create :client_application_asset_not_editable, client_application: @client_application
        get :show, client_application_id: asset.client_application, representation_id: @representation, id: asset
        must_render 'client_application_assets/show'
        assigns(:asset).wont_be :new_record?
        assert_select ".context-actions li a[href=?]",
          edit_content_client_application_representation_asset_path(@client_application, @representation, asset), false
      end

      it 'must succeed when editable? and add action to edit content' do
        asset = create :client_application_asset_editable, client_application: @client_application
        get :show, client_application_id: asset.client_application, representation_id: @representation, id: asset
        must_render 'client_application_assets/show'
        assigns(:asset).wont_be :new_record?
        assert_select ".context-actions li a[href=?]",
          edit_content_client_application_representation_asset_path(@client_application, @representation, asset)
      end

      # This view is shared with other module so we must check go back action
      it 'must set go back action to client_application_assets_path' do
        asset = create :client_application_asset, client_application: @client_application
        get :show, client_application_id: asset.client_application, representation_id: @representation, id: asset
        assert_select '.context-actions a[href=?]',
          client_application_representation_assets_path(@client_application, @representation), t_action(:representation_assets, :back)
      end

      it 'must set session[client_application_asset.*_path]' do
        asset = create :client_application_asset, client_application: @client_application
        get :show, client_application_id: asset.client_application, representation_id: @representation, id: asset
        session['client_application_asset.list_path'].must_equal client_application_representation_assets_path(@client_application, @representation)
        session['client_application_asset.edit_path'].must_equal edit_client_application_representation_asset_path(@client_application, @representation, assigns(:asset))
        session['client_application_asset.edit_content_path'].must_equal edit_content_client_application_representation_asset_path(@client_application, @representation, assigns(:asset))
        session['client_application_asset.rename_path'].must_equal rename_client_application_representation_asset_path(@client_application, @representation, assigns(:asset))
        session['client_application_asset.show_path'].must_equal client_application_representation_asset_path(@client_application, @representation, assigns(:asset))
      end

    end

    describe 'get edit' do
      before do
        asset = create :client_application_asset, client_application: @client_application
        get :edit, client_application_id: asset.client_application, representation_id: @representation, id: asset
      end

      it 'must succeed' do
        must_render 'client_application_assets/edit'
        assigns(:asset).wont_be :new_record?
        assert_select "#edit_client_application_asset_#{assigns(:asset).id}[action=?]",
          client_application_asset_path(assigns(:asset).client_application, assigns(:asset))
      end

      # This view is shared with other module so we must check go back action
      it 'must set go back action to client_application_assets_path' do
        assert_select '.context-actions a[href=?]',
          client_application_representation_assets_path(@client_application, @representation), t_action(:representation_assets, :back)
      end

      it 'must set session[client_application_asset.*_path]' do
        session['client_application_asset.list_path'].must_equal client_application_representation_assets_path(@client_application, @representation)
        session['client_application_asset.edit_path'].must_equal edit_client_application_representation_asset_path(@client_application, @representation, assigns(:asset))
        session['client_application_asset.edit_content_path'].must_equal edit_content_client_application_representation_asset_path(@client_application, @representation, assigns(:asset))
        session['client_application_asset.rename_path'].must_equal rename_client_application_representation_asset_path(@client_application, @representation, assigns(:asset))
        session['client_application_asset.show_path'].must_equal client_application_representation_asset_path(@client_application, @representation, assigns(:asset))
      end
    end

    describe 'get rename' do
      before do
        asset = create :client_application_asset, client_application: @client_application
        get :rename, client_application_id: asset.client_application, representation_id: @representation, id: asset
      end

      it 'must succeed' do
        must_render 'client_application_assets/rename'
        assigns(:asset).wont_be :new_record?
      end

      # This view is shared with other module so we must check go back action
      it 'must set go back action to client_application_assets_path' do
        assert_select '.context-actions a[href=?]',
          client_application_representation_assets_path(@client_application, @representation), t_action(:representation_assets, :back)
      end

      it 'must set session[client_application_asset.*_path]' do
        session['client_application_asset.list_path'].must_equal client_application_representation_assets_path(@client_application, @representation)
        session['client_application_asset.edit_path'].must_equal edit_client_application_representation_asset_path(@client_application, @representation, assigns(:asset))
        session['client_application_asset.edit_content_path'].must_equal edit_content_client_application_representation_asset_path(@client_application, @representation, assigns(:asset))
        session['client_application_asset.rename_path'].must_equal rename_client_application_representation_asset_path(@client_application, @representation, assigns(:asset))
        session['client_application_asset.show_path'].must_equal client_application_representation_asset_path(@client_application, @representation, assigns(:asset))
      end
    end

    describe 'get edit content' do

      describe 'when asset is editable' do
        before do
          asset_editable = create :client_application_asset_editable, client_application: @client_application
          get :edit_content, client_application_id: asset_editable.client_application, representation_id: @representation, id: asset_editable
        end

        it 'must succeed' do
          must_render 'client_application_assets/edit_content'
          asset = assigns(:asset)
          asset.wont_be :new_record?
          assert_select "#edit_client_application_asset_#{asset.id}[action=?]",
            update_content_client_application_asset_path(asset.client_application, asset)
        end
        #
        # This view is shared with other module so we must check go back action
        it 'must set go back action to client_application_assets_path' do
          assert_select '.context-actions a[href=?]',
            client_application_representation_assets_path(@client_application, @representation), t_action(:representation_assets, :back)
        end

        it 'must set session[client_application_asset.*_path]' do
          session['client_application_asset.list_path'].must_equal client_application_representation_assets_path(@client_application, @representation)
          session['client_application_asset.edit_path'].must_equal edit_client_application_representation_asset_path(@client_application, @representation, assigns(:asset))
          session['client_application_asset.edit_content_path'].must_equal edit_content_client_application_representation_asset_path(@client_application, @representation, assigns(:asset))
          session['client_application_asset.rename_path'].must_equal rename_client_application_representation_asset_path(@client_application, @representation, assigns(:asset))
          session['client_application_asset.show_path'].must_equal client_application_representation_asset_path(@client_application, @representation, assigns(:asset))
        end
      end

      it 'must fail for not editable?' do
        asset_not_editable = create :client_application_asset_not_editable, client_application: @client_application

        get :edit_content, client_application_id: asset_not_editable.client_application, representation_id: @representation, id: asset_not_editable
        must_redirect_to client_application_representation_asset_path(@client_application, @representation, asset_not_editable)
        assigns(:asset).wont_be :new_record?
        must_have_alert 'client_application_assets.edit_content.messages.error'
      end
    end

    describe 'post toggle_as_favicon' do
      describe 'set as favicon' do
        describe 'when image asset is given' do
          before do
            @asset = create :client_application_asset, client_application: @client_application
            @representation.assets << @asset
          end

          it 'must succeed' do
            post :toggle_as_favicon, client_application_id: @client_application, representation_id: @representation, id: @asset
            assigns(:representation).favicon.must_equal @asset
            must_redirect_to client_application_representation_assets_path(@client_application, assigns(:representation))
            must_have_notice 'client_application_assets.set_as_favicon.messages.success'
          end
        end

        describe 'when other asset type is given' do
          before do
            @asset = create :client_application_asset_stylesheet, client_application: @client_application
            @representation.assets << @asset
          end

          it 'must fail' do
            post :toggle_as_favicon, client_application_id: @client_application, representation_id: @representation, id: @asset
            assigns(:representation).favicon.must_equal nil
            must_redirect_to client_application_representation_assets_path(@client_application, assigns(:representation))
            must_have_alert 'client_application_assets.set_as_favicon.messages.error'
          end
        end
      end

      describe 'unset as favicon' do
        before do
          @asset = create :client_application_asset, client_application: @client_application
          @representation.assets << @asset
        end

        it 'must succeed' do
          post :toggle_as_favicon, client_application_id: @client_application, representation_id: @representation, id: @asset
          assigns(:representation).favicon.must_equal @asset

          post :toggle_as_favicon, client_application_id: @client_application, representation_id: @representation, id: @asset
          assigns(:representation).favicon.must_equal nil
          must_redirect_to client_application_representation_assets_path(@client_application, assigns(:representation))
          must_have_notice 'client_application_assets.unset_as_favicon.messages.success'
        end
      end

      describe 'set paths' do
        it 'must set session[client_application_asset.*_path]' do
          asset = create :client_application_asset, client_application: @client_application
          @representation.assets << asset
          get :toggle_as_favicon, client_application_id: @client_application, representation_id: @representation, id: asset
          session['client_application_asset.list_path'].must_equal client_application_representation_assets_path(@client_application, @representation)
          session['client_application_asset.edit_path'].must_equal edit_client_application_representation_asset_path(@client_application, @representation, assigns(:asset))
          session['client_application_asset.edit_content_path'].must_equal edit_content_client_application_representation_asset_path(@client_application, @representation, assigns(:asset))
          session['client_application_asset.rename_path'].must_equal rename_client_application_representation_asset_path(@client_application, @representation, assigns(:asset))
          session['client_application_asset.show_path'].must_equal client_application_representation_asset_path(@client_application, @representation, assigns(:asset))
        end
      end
    end

    describe 'POST toggle_into_representation' do
      it 'must delete an asset when it is associated' do
        asset_not_editable = create :client_application_asset_not_editable, client_application: @client_application
        @representation.assets << asset_not_editable


        post :toggle_into_representation, client_application_id: @client_application, representation_id: @representation, id: asset_not_editable

        must_redirect_to client_application_representation_assets_path(@client_application, @representation)
        must_have_notice 'representation_assets.toggle_into_representation.delete'
        assigns(:representation).assets.wont_include asset_not_editable
      end

      it 'must add an asset when it is not associated' do
        asset_not_editable = create :client_application_asset_not_editable, client_application: @client_application

        post :toggle_into_representation, client_application_id: @client_application, representation_id: @representation, id: asset_not_editable

        must_redirect_to client_application_representation_assets_path(@client_application, @representation)
        must_have_notice 'representation_assets.toggle_into_representation.add'
        assigns(:representation).assets.must_include asset_not_editable
      end

      it 'must set session[client_application_asset.*_path]' do
        asset_not_editable = create :client_application_asset_not_editable, client_application: @client_application
        post :toggle_into_representation, client_application_id: @client_application, representation_id: @representation, id: asset_not_editable
        session['client_application_asset.list_path'].must_equal client_application_representation_assets_path(@client_application, @representation)
        session['client_application_asset.edit_path'].must_equal edit_client_application_representation_asset_path(@client_application, @representation, assigns(:asset))
        session['client_application_asset.edit_content_path'].must_equal edit_content_client_application_representation_asset_path(@client_application, @representation, assigns(:asset))
        session['client_application_asset.rename_path'].must_equal rename_client_application_representation_asset_path(@client_application, @representation, assigns(:asset))
        session['client_application_asset.show_path'].must_equal client_application_representation_asset_path(@client_application, @representation, assigns(:asset))
      end
    end

    describe 'PATCH batch_action' do
      it 'must add selected assets' do
        asset_1 = create :client_application_asset_not_editable, client_application: @client_application
        asset_2 = create :client_application_asset_editable, client_application: @client_application
        assets = [asset_1.id, asset_2.id]
        @representation.assets.must_be_empty

        patch :batch_action, client_application_id: @client_application, representation_id: @representation, batch_action: RepresentationAssetsController::ADD_TO_REPRESENTATION_BATCH, assets: assets

        must_redirect_to client_application_representation_assets_path(@client_application, @representation)
        must_have_notice 'representation_assets.batch_action.add.message'
        @representation.reload
        @representation.assets.pluck(:id).sort.must_equal assets.sort
      end

      it 'wont add duplicated assets' do
        asset_1 = create :client_application_asset_not_editable, client_application: @client_application
        asset_2 = create :client_application_asset_editable, client_application: @client_application
        @representation.assets << asset_1
        assets = [asset_1.id, asset_2.id]

        patch :batch_action, client_application_id: @client_application, representation_id: @representation, batch_action: RepresentationAssetsController::ADD_TO_REPRESENTATION_BATCH, assets: assets

        must_redirect_to client_application_representation_assets_path(@client_application, @representation)
        must_have_notice 'representation_assets.batch_action.add.message'
        assigns(:representation).assets.pluck(:id).sort.must_equal assets.sort
      end

      it 'must deleted selected assets' do
        asset_1 = create :client_application_asset_not_editable, client_application: @client_application
        asset_2 = create :client_application_asset_editable, client_application: @client_application
        @representation.assets << asset_1
        @representation.assets << asset_2
        assets = [asset_1.id, asset_2.id]

        patch :batch_action, client_application_id: @client_application, representation_id: @representation, batch_action: RepresentationAssetsController::DELETE_FROM_REPRESENTATION_BATCH, assets: assets

        must_redirect_to client_application_representation_assets_path(@client_application, @representation)
        must_have_notice 'representation_assets.batch_action.delete.message'
        assigns(:representation).assets.wont_include asset_1
        assigns(:representation).assets.wont_include asset_2
      end

      it 'wont delete selected assets twice' do
        asset_1 = create :client_application_asset_not_editable, client_application: @client_application
        asset_2 = create :client_application_asset_editable, client_application: @client_application
        @representation.assets.wont_include asset_1
        @representation.assets.wont_include asset_2
        assets = [asset_1.id, asset_2.id]

        patch :batch_action, client_application_id: @client_application, representation_id: @representation, batch_action: RepresentationAssetsController::DELETE_FROM_REPRESENTATION_BATCH, assets: assets

        must_redirect_to client_application_representation_assets_path(@client_application, @representation)
        must_have_notice 'representation_assets.batch_action.delete.message'
        assigns(:representation).assets.wont_include asset_1
        assigns(:representation).assets.wont_include asset_2
      end

      it 'will do nothing when action is unknown' do
        asset_1 = create :client_application_asset_not_editable, client_application: @client_application
        asset_2 = create :client_application_asset_editable, client_application: @client_application
        @representation.assets << asset_1
        @representation.assets << asset_2
        assets = [asset_1.id, asset_2.id]

        patch :batch_action, client_application_id: @client_application, representation_id: @representation, batch_action: -1, assets: assets

        must_redirect_to client_application_representation_assets_path(@client_application, @representation)
        assigns(:representation).assets.count.must_equal 2

      end
    end

    describe 'when client_application is not found' do
      it 'must redirect to client_applicaion index alerting error' do
        get :index, client_application_id: -1, representation_id: @representation
        must_redirect_to client_applications_path
        must_have_alert 'representations.errors.missing_client_application'
      end
    end

    describe 'when representation is not found' do
      it 'must redirect to representations index alerting error' do
        get :index, client_application_id: @client_application, representation_id: -1
        must_redirect_to client_application_representations_path(@client_application)
        must_have_alert 'representations.errors.draft_unavailable'
      end
    end
  end
end
