require 'test_helper'

class SettingsControllerTest < ActionController::TestCase

  describe SettingsController do
    let(:permissions) { %w[ super_admin ] }
    before do
      login permissions: permissions
      @controller = SettingsController.new
    end

    describe 'Permissions sanity checks' do
      describe 'without permissions' do
        let(:permissions) { [] }

        describe 'whent trying to get index' do
          it 'must be forbidden' do
            must_be_forbidden -> { get :index }
          end
        end

        describe 'whent trying to get edit_all' do
          it 'must be forbidden' do
            must_be_forbidden -> { get :edit_all }
          end
        end

        describe 'whent trying to put update_all' do
          let(:settings) { { "pagination.per_page" => 12, "media_versions.xbig" => "15x15", "media_versions.big" => "10x10", "media_versions.medium" => "5x5", "media_versions.small" => "2x2", "media_versions.xsmall" => "1x1" } }

          it 'must be forbidden' do
            must_be_forbidden -> { put :update_all, settings: settings }
          end
        end
      end
    end

    describe "get index" do
      it 'succedded' do
        get :index
        must_render :index
        assigns(:settings).wont_be_nil
      end
    end

    describe "get edit_all" do
      it 'succeeded' do
        get :edit_all
        must_render :edit_all
        assigns(:settings).wont_be_nil
      end
    end

    describe "put update_all" do
      let(:default_settings) { { "pagination.per_page" => 12, "media_versions.xbig" => "15x15", "media_versions.big" => "10x10", "media_versions.medium" => "5x5", "media_versions.small" => "2x2", "media_versions.xsmall" => "1x1" } }

      describe 'when any value is empty' do
        let(:valid_settings) { {"pagination.per_page"=>"20", "media_versions.xbig"=>"932x932", "media_versions.big"=>"576x576", "media_versions.medium"=>"392x392", "media_versions.small"=>"272x272", "media_versions.xsmall"=>"212x212"} }

        before do
          default_settings.each { |k, v| Setting[k] = v }
        end

        it 'modified values, redirect to index and display notice' do
          put :update_all, settings: valid_settings
          Setting["pagination.per_page"] = valid_settings["pagination.per_page"]
          must_redirect_to :settings
          must_have_notice 'settings.update_all.messages.success'
        end
      end
    end

  end

end
