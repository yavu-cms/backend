require 'test_helper'

class FrontendServersControllerTest < ActionController::TestCase
  describe FrontendServersController do
    describe 'get index' do
      it 'must be implemented'
    end

    describe 'post configure' do
      it 'must be implemented'
    end

    describe 'put restart' do
      it 'must be implemented'
    end
  end
end
