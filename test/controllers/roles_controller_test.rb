require 'test_helper'

class RolesControllerTest < ActionController::TestCase
  describe RolesController do
     let(:permissions) { %w[ role/read role/create role/update role/destroy ] }

    before do
      login permissions: permissions
      # NOTE: this merhod create a new role
      # See login method on mini_test_helpers.rb
      @controller = RolesController.new
    end

    describe 'Permissions sanity checks' do
      describe 'whitout permissions' do
        let(:permissions) { [] }
        let(:role) { create :role }

        describe 'create a new role' do
          describe 'when trying to get new' do  
            it 'must be forbidden' do
              must_be_forbidden -> { get :new }
            end
          end

          describe 'when trying to post create' do
            it 'must be forbidden' do
               must_be_forbidden ->{ post :create, role: attributes_for(:role) }
            end
          end
        end

        describe 'updating roles' do
          describe 'when trying to get edit' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :edit, id: role }
            end
          end

          describe 'when trying to put update' do
            it 'must be forbidden' do
              role.name = 'updated name'
              must_be_forbidden -> { put :update, id: role, role: role.attributes }
            end
          end
        end

        describe 'read roles' do
          describe 'when trying to get index' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :index }
            end
          end

          describe 'when trying to get show' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :show, id: role }
            end
          end
        end

        describe 'destroing role' do
          describe 'when trying to delete destroy' do
            it 'must be forbidden' do
              must_be_forbidden -> { delete :destroy, id: role }
            end
          end
        end
      end
    end

    describe 'get index' do
      it 'succeds' do
        get :index
        must_render :index
        assigns(:roles).wont_be_nil
        assigns(:order).wont_be_nil
        assigns(:filter).wont_be_nil
      end

      describe 'when specifying a sort criterion' do
        before do
          @last  = create :role, name: 'xyz'
          @first = create :role, name: 'abc'  
        end

        it 'sorts results using it' do
          get :index, { sort: { field: 'name', order: :asc } }
          must_render :index
          assigns(:roles).first.must_equal @first
          assigns(:roles).last.must_equal @last
        end
      end

      describe 'when specifying a page' do
        before do
          @role_1 = create :role, name: 'XXX', created_at: 3.minutes.ago
          @role_2 = create :role, name: 'YYY', created_at: 2.minutes.ago
          @role_3 = create :role, name: 'ZZZ', created_at: 1.minutes.ago
        end

        it 'shows records for that page' do
          get :index, { page: 4, per_page: 1, sort: { field: 'name', order: :asc } }
          must_render :index
          assigns(:roles).count.must_equal 1
          assigns(:roles).must_equal [@role_3]
        end
      end

      describe 'when specifying the number of elements per page to show' do
        before do
          10.times { |i| create :role }
        end

        it 'shows at most that number of records' do
          get :index, { per_page: 4 }
          must_render :index
          assigns(:roles).count.must_equal 4
        end
      end

      describe 'when filter criteria is submitted' do
        before do
          @role_1 = create :role, name: 'fgh'
          @role_2 = create :role, name: 'xyz'
        end

        it 'renders index action and filters the results' do
          get :index, { filter: { name: 'a' } }
          must_render :index
          assigns(:roles).count.must_equal 1 # role of authenticated user
          assigns(:roles).first.must_equal authenticated.roles.first
        end
      end

      describe 'when reset filter is submitted' do
        before do
          get :index, { commit: I18n.t('activerecord.actions.shared.clear_filter') }
        end

        it 'redirects to index, resetting the filters' do
          must_redirect_to roles_path
          assigns(:filter).values.must_equal RolesFilter.defaults
        end
      end
    end

    describe 'get new' do
      before do
        get :new
      end

      it 'render new action' do
        must_render :new
        assigns(:role).must_be :new_record?
      end
    end

    describe 'post create' do
      describe 'when valid values are submitted' do
        it 'adds a role and redirects to the show page and show notice message' do
          it_adds_a Role
          must_redirect_to role_path(assigns(:role))
          assigns(:role).wont_be :new_record?
          must_have_notice 'roles.new.messages.success'
        end
      end

      describe 'when invalid values are submitted' do
        before do
          post :create, role: { name: nil }
        end

        it 'renders back new action and displays errors' do
          must_render :new
          assigns(:role).must_be :new_record?
          must_display_form_errors
        end
      end
    end
  end
end