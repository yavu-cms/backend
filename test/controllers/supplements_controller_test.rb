require 'test_helper'

class SupplementsControllerTest < ActionController::TestCase
  describe SupplementsController do
    let(:permissions) { %w[ supplement/read supplement/create supplement/update supplement/destroy ] }

    before do
      login permissions: permissions
      @controller = SupplementsController.new
    end

    describe 'Permissions sanity checks' do
      let(:supplement) { create :supplement }

      describe 'create a new supplement' do
        let(:permissions) { %w[ supplement/read ] }

        describe 'when trying to get new' do
          it 'must be forbidden' do
            must_be_forbidden ->{ get :new }
          end
        end

        describe 'when trying to post create' do
          it 'must be forbidden' do
            must_be_forbidden ->{ post :create, supplement: attributes_for(:supplement) }
          end
        end
      end

      describe 'update a supplement' do
        let(:permissions) { %w[ supplement/read ] }

        describe 'when trying to get edit' do
          it 'must be forbidden' do
            must_be_forbidden ->{ get :edit, id: supplement }
          end
        end

        describe 'when trying to put update' do
          it 'must be forbidden' do
            must_be_forbidden ->{ put :update, id: supplement }
          end
        end

        describe 'when trying to post become_default' do
          it 'must be forbidden' do
            must_be_forbidden ->{ post :become_default, id: supplement }
          end
        end
      end

      describe 'destroy a supplement' do
        let(:permissions) { %w[ supplement/read ] }

        describe 'when trying to delete destroy' do
          it 'must be forbidden' do
            must_be_forbidden ->{ delete :destroy, id: supplement }
          end
        end
      end
    end

    describe 'get index' do
      it 'succeeds' do
        get :index
        must_render :index
        assigns(:supplements).wont_be_nil
      end

      describe 'when specifying a page' do
        before do
          @supplement_1 = create :supplement, name: 'Number one', created_at: 3.minutes.ago
          @supplement_2 = create :supplement, name: 'Number two', created_at: 2.minutes.ago
          @supplement_3 = create :supplement, name: 'Number three', created_at: 1.minutes.ago
        end

        it 'shows records for that page' do
          get :index, { page: 3, per_page: 1 }
          must_render :index
          assigns(:supplements).count.must_equal 1
          assigns(:supplements).must_equal [@supplement_3]
        end
      end

      describe 'when specifying the number of elements per page to show' do
        before do
          10.times { create :supplement }
        end

        it 'shows at most that number of records' do
          get :index, { per_page: 5 }
          must_render :index
          assigns(:supplements).count.must_equal 5
        end
      end

      describe 'when filtering results' do
        before do
          @supplement_1 = create :supplement, name: 'A name'
          @supplement_2 = create :supplement, name: 'Other name'
          @supplement_3 = create :supplement, name: 'Bla bla title'
        end

        it 'show the matching records' do
          get :index, { filter: { name: 'Bla' } }
          assigns(:supplements).to_a.must_equal [@supplement_3]
        end
      end

      describe 'when reset is submitted' do
        before do
          get :index, { commit: I18n.t('activerecord.actions.shared.clear_filter') }
        end

        it 'clear the filters and redirects to index' do
          assigns(:filter).values.must_equal SupplementsFilter.defaults
          must_redirect_to supplements_url
        end
      end
    end

    describe 'get new' do
      it 'succeeds' do
        get :new
        must_render :new
      end
    end

    describe 'filter' do
      before do
        create(:supplement, name: 'abc')
        create(:supplement, name: 'xyz')
      end

      describe 'when filter criteria is submitted' do
        before do
          get :index, { filter: { name: 'a' } }
        end

        it 'filters the results' do
          must_succeed
          assigns(:supplements).wont_be_nil
          assigns(:supplements).count.must_be :<, Supplement.count
        end
      end

      describe 'when reset is submitted' do
        before do
          get :index, { commit: I18n.t('activerecord.actions.shared.clear_filter') }
        end

        it 'clear the filters and redirects to index' do
          assert_equal SupplementsFilter.defaults, assigns(:filter).values
          must_redirect_to supplements_url
        end
      end
    end

    describe 'create' do
      describe 'when submitting valid values' do
        it 'adds a supplement and redirects to the show page' do
          post :create, supplement: { name: 'Great supplement', news_source_id: create(:news_source) }
          must_redirect_to supplement_url(assigns[:supplement])
        end
      end

      describe 'when submitting invalid values' do
        describe 'when no submit name' do
          it 'returns an error' do
            post :create, supplement: { name: nil, news_source_id: create(:news_source) }
            must_display_form_errors
            must_render :new
          end
        end
        describe 'when no submit news source' do
          it 'returns an error' do
            post :create, supplement: { name: 'Great supplement', news_source_id: nil }
            must_display_form_errors
            must_render :new
          end
        end
      end
    end

    describe 'show' do
      it 'succeeds' do
        get :show, id: create(:supplement)
        must_render :show
      end
    end

    describe 'get edit' do
      it 'succeeds' do
        get :edit, id: create(:supplement)
        must_render :edit
      end
    end

    describe 'update' do
      let(:supplement) { create :supplement }

      describe 'when values are valid' do
        before do
          patch :update, id: supplement, supplement: { name: 'changed supplement'}
        end

        it 'updates the supplement and redirects to show' do
          assigns(:supplement).name.must_equal 'changed supplement'
          must_redirect_to supplement_url(assigns(:supplement))
        end
      end

      describe 'when submitted values are invalid' do
        it 'returns with errors' do
          patch :update, id: supplement, supplement: { name: nil }
          must_display_form_errors
        end
      end
    end

    describe 'destroy' do
      describe "it isn't related to any edition or section" do
        it 'deletes the supplement' do
          it_deletes_a Supplement
        end

        it 'redirects to index' do
          delete :destroy, id: create(:supplement)
          must_redirect_to supplements_url, true
          must_have_notice 'activerecord.actions.supplement.destroyed'
        end
      end

      describe 'it is related to at least one edition or section' do
        it 'does not delete the supplement' do
          it_doesnt_delete_a Supplement, factory: :supplement_with_section
        end

        it 'redirects to index' do
          delete :destroy, id: create(:supplement_with_section)
          must_redirect_to supplements_url, true
          must_have_alert 'supplements.destroy.messages.has_editions_or_sections_associated'
        end
      end
    end

    describe 'as default' do
      before do
        @supplement  = create(:non_default_supplement_with_active_edition)
        @news_source = @supplement.news_source
        post :become_default, id: @supplement
      end
      it 'makes supplement default in the same news source scope and redirects to index' do
        @news_source.default_supplement.must_equal @supplement
        must_redirect_to supplements_url
        must_have_notice 'supplements.become_default.messages.success'
      end

    end

    describe 'active edition' do
      describe 'when supplement has an active edition' do
        before do
          @supplement = create :active_edition_supplement
        end
        it 'redirects to show edition' do
          get :active_edition, id: @supplement
          must_redirect_to edition_url(@supplement.active_edition)
        end
      end

      describe 'when supplement does not have an active edition' do
        before do
          @supplement = create :non_default_supplement
          get :active_edition, id: @supplement
        end

        it 'redirects to index with a flash message' do
          must_redirect_to supplements_url
          must_have_alert 'supplements.active_edition.messages.failed'
        end
      end
    end

    describe 'editions' do
      let(:supplement) { create :supplement }

      it 'redirects to the editions controller pre-setting the filters to match the current supplement' do
        get :editions, id: supplement
        response.location.starts_with?(editions_url(filter: { supplement_id: supplement.id, name: nil })).must_equal true
      end
    end

    describe 'sections' do
      let(:supplement) { create :supplement }

      it 'redirects to the sections controller pre-setting the filters to match the current supplement' do
        get :sections, id: supplement
        response.location.starts_with?(sections_url(filter: { supplement_id: supplement.id, name: nil })).must_equal true
      end
    end
  end
end
