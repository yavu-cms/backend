require 'test_helper'

class VersionsControllerTest < ActionController::TestCase
  describe VersionsController do
    let(:permissions) { %w[ version/read  version/update ] }

    before do
      login permissions: permissions
      @controller = VersionsController.new
    end

    describe 'Permissions sanity checks' do
      describe 'create a new version' do
        let(:permissions) { [] }

        describe 'when trying to get new' do
          it 'must be forbidden' do
            must_be_forbidden ->{ get :index }
          end
        end

        describe 'when trying to post restore' do
          let(:version) { create :version }
          it 'must be forbidden' do
            must_be_forbidden ->{ post :restore,  id: version }
          end
        end
      end
    end
  end
end
