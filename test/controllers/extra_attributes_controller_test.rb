require 'test_helper'

class ExtraAttributesControllerTest < ActionController::TestCase
  describe ExtraAttributesController do
    let(:permissions) { %w[ extra_attribute/manage ] }
    before do
      login permissions: permissions
      @controller = ExtraAttributesController.new
    end

    describe 'Permissions sanity checks' do
      describe 'without permissions' do
        let(:permissions) { [] }

        describe 'whent trying to get index' do
          it 'must be forbidden' do
            must_be_forbidden -> { get :index }
          end
        end

        describe 'whent trying to get new' do
          it 'must be forbidden' do
            must_be_forbidden -> { get :new }
          end
        end

        describe 'whent trying to get edit' do
          it 'must be forbidden' do
            extra_attribute = create :extra_attribute
            must_be_forbidden -> { get :edit, id: extra_attribute }
          end
        end

        describe 'whent trying to post create' do
          it 'must be forbidden' do
            must_be_forbidden -> { post :create, extra_attribute: attributes_for(:extra_attribute) }
          end
        end

        describe 'whent trying to patch update' do
          it 'must be forbidden' do
            extra_attribute = create :extra_attribute
            extra_attribute.name = "edited_name"
            must_be_forbidden -> { patch :update, id: extra_attribute, extra_attribute: extra_attribute.attributes }
          end
        end

        describe 'whent trying to dekete destroy' do
          it 'must be forbidden' do
            extra_attribute = create :extra_attribute
            must_be_forbidden -> { delete :destroy, id: extra_attribute }
          end
        end
      end
    end

    describe 'get index' do
      it 'succeeds' do
        get :index
        must_render :index
        assigns(:extra_attributes).wont_be_nil
      end

      describe 'when specifying a page' do
        before do
          @attr_1 = create :extra_attribute_boolean, name: 'XXX', created_at: 3.minutes.ago
          @attr_2 = create :extra_attribute_boolean, name: 'YYY', created_at: 2.minutes.ago
          @attr_3 = create :extra_attribute_boolean, name: 'ZZZ', created_at: 1.minutes.ago
        end

        it 'shows records for that page' do
          get :index, { page: 3, per_page: 1 }
          must_render :index
          assigns(:extra_attributes).count.must_equal 1
          assigns(:extra_attributes).must_equal [@attr_3]
        end
      end

      describe 'when specifying the number of elements per page to show' do
        before do
          create_list :extra_attribute_boolean, 10
        end

        it 'shows at most that number of records' do
          get :index, { per_page: 4 }
          must_render :index
          assigns(:extra_attributes).count.must_equal 4
        end
      end
    end

    describe 'get new' do
      before do
        get :new
      end

      it 'render new action' do
        must_render :new
        assigns(:extra_attribute).must_be :new_record?
      end
    end

    describe 'post create' do
      describe 'when valid values are submitted' do
        it 'adds an extra_attribute and redirects to the index page and show notice message' do
          it_adds_a ExtraAttribute
          must_redirect_to extra_attributes_path
          assigns(:extra_attribute).wont_be :new_record?
          must_have_notice 'extra_attributes.new.messages.success'
        end
      end

      describe 'when invalid values are submitted' do
        before do
          post :create, extra_attribute: { name: nil }
        end

        it 'renders back new action and displays errors' do
          must_render :new
          assigns(:extra_attribute).must_be :new_record?
          must_display_form_errors
        end
      end
    end

    describe 'get edit' do
      let(:extra_attribute) { create :extra_attribute_boolean }

      before do
        get :edit, id: extra_attribute
      end

      it 'renders edit action for the requested extra_attribute' do
        must_render :edit
        assigns(:extra_attribute).must_equal extra_attribute
      end
    end

    describe 'patch/put update' do
      let(:extra_attribute) { create :extra_attribute }

      describe 'when valid values are submitted' do
        before do
          patch :update, id: extra_attribute, extra_attribute: { name: 'changed_attribute' }
        end

        it 'modifies the extra_attribute, redirects to index and display notice message' do
          assigns(:extra_attribute).name.must_equal 'changed_attribute'
          must_redirect_to extra_attributes_url
          must_have_notice 'extra_attributes.edit.messages.success'
        end
      end

      describe 'when invalid values are submitted' do
        before do
          put :update, id: extra_attribute, extra_attribute: { name: nil }
        end

        it 'renders back edit action and displays errors' do
          must_render :edit
          assigns(:extra_attribute).must_equal extra_attribute
          must_display_form_errors
        end
      end
    end

    describe 'delete destroy' do
      it 'removes the extra_attribute, redirects to the index page and shows a notice' do
        it_deletes_a ExtraAttribute
        must_redirect_to extra_attributes_url
        must_have_notice 'extra_attributes.destroy.messages.success'
      end
    end
  end
end
