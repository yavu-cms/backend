require 'test_helper'

class MediaControllerTest < ActionController::TestCase
  include MediaHelper
  # Including +RenderingHelper+ in order to be able to stub the +render+ method
  include ActionView::Helpers::RenderingHelper
  describe MediaController do
    let(:permissions) { %w[ medium/read medium/create medium/update/all medium/destroy/all ] }

    before do
      login permissions: permissions
      @controller = MediaController.new
    end

    describe 'Permissions sanity checks' do
      let(:permissions) { %w[ medium/read ] }

      describe 'create a new medium' do
        describe 'when trying to get new_*' do
          it 'must be forbidden' do
            %i[ new_audio new_embedded new_file new_image].each do |new_action|
              must_be_forbidden ->{ get new_action }
            end
          end
        end

        describe 'when trying to post create' do
          it 'must be forbidden' do
            must_be_forbidden ->{ post :create, medium: attributes_for(:image_medium) }
          end
        end

        describe 'when create from media modal' do
          describe 'success' do
            it 'must call find'
          end
          describe 'error' do
            it 'must return json error'
          end
        end
      end

      describe 'updating media' do
        describe 'with :unused permission' do
          let(:permissions) { %w[ medium/update/unused ] }

          describe 'when editing an unused medium' do
            let(:medium) { create :image_medium }

            it 'succeeds' do
              get :edit, id: medium
              must_render :edit
            end
          end

          describe 'when editing a medium being used' do
            let(:medium) { create :embedded_medium_in_use_by_article }

            it 'must be forbidden' do
              must_be_forbidden ->{ get :edit, id: medium }
            end
          end
        end

        describe 'with :all permission' do
          let(:permissions) { %w[ medium/update/all ] }

          describe 'when editing an unused medium' do
            let(:medium) { create :image_medium }

            it 'succeeds' do
              get :edit, id: medium
              must_render :edit
            end
          end

          describe 'when editing a medium being used by an article that the user can update' do
            let(:medium) { create :embedded_medium_in_use_by_article }

            before do
              pretend_user_can(:update, medium.articles.first)
            end

            it 'succeeds' do
              get :edit, id: medium
              must_render :edit
            end
          end

          describe 'when editing a medium being used by an article that the user cannot update' do
            let(:medium) { create :embedded_medium_in_use_by_article }

            before do
              pretend_user_cannot(:update, medium.articles.first)
            end

            it 'must be forbidden' do
              must_be_forbidden ->{ get :edit, id: medium }
            end
          end
        end
      end

      describe 'destroying media' do
        describe 'with :unused permission' do
          let(:permissions) { %w[ medium/destroy/unused ] }

          describe 'when destroying an unused medium' do
            let(:medium) { create :image_medium }

            it 'succeeds' do
              delete :destroy, id: medium
              must_redirect_to media_path, true
            end
          end

          describe 'when destroying a medium being used' do
            let(:medium) { create :embedded_medium_in_use_by_article }

            it 'must be forbidden' do
              must_be_forbidden ->{ delete :destroy, id: medium }
            end
          end
        end

        describe 'with :all permission' do
          let(:permissions) { %w[ medium/destroy/all ] }

          describe 'when destroying an unused medium' do
            let(:medium) { create :image_medium }

            it 'succeeds' do
              delete :destroy, id: medium
              must_redirect_to media_path, true
            end
          end

          describe 'when destroying a medium being used by an article that the user can destroy' do
            let(:medium) { create :embedded_medium_in_use_by_article }

            before do
              pretend_user_can(:update, medium.articles.first)
            end

            it 'succeeds' do
              delete :destroy, id: medium
              must_redirect_to media_path, true
            end
          end

          describe 'when destroying a medium being used by an article that the user cannot destroy' do
            let(:medium) { create :embedded_medium_in_use_by_article }

            before do
              pretend_user_cannot(:update, medium.articles.first)
            end

            it 'must be forbidden' do
              must_be_forbidden ->{ delete :destroy, id: medium }
            end
          end
        end
      end
    end

    describe 'GET index' do
      describe 'on a regular request' do
        it 'must succeed' do
          get :index
          must_render :index
          assigns(:media).wont_be_nil
        end
      end

      describe 'when specifying a page' do
        let(:per_page) { 2 }
        let(:page_number) { 3 }

        before do
          10.times { |i| create :embedded_medium, name: "Medium #{i + 1}", created_at: (10 - i).minutes.ago }
        end

        it 'shows records for that page' do
          get :index, per_page: per_page, page: page_number
          assigns(:media).first.name.must_equal 'Medium 5'
          assigns(:media).last.name.must_equal 'Medium 6'
        end
      end

      describe 'when specifying the number of elements per page to show' do
        let(:per_page) { 3 }

        before do
          8.times { create :embedded_medium }
        end

        it 'shows at most that number of records' do
          get :index, per_page: per_page
          must_render :index
          assigns(:media).count.must_equal per_page
        end
      end

      describe 'when filtering results' do
        before do
          @medium_1 = create :image_medium, name: 'Cool tagline'
          @medium_2 = create :embedded_medium, name: 'A rather dull one'
          @matching = [@medium_1]
        end

        it 'shows the matching records' do
          get :index, filter: { query: 'cool' }
          must_render :index
          assigns(:media).must_equal @matching
        end
      end

      describe 'when clearing the filters' do
        let(:filter_mock) { mock }
        before do
          @controller.expects(:create_filter).
            with(MediaFilter, {values: nil}).returns(filter_mock).once
          filter_mock.expects(:clear!).once
          @medium_1 = create :image_medium, name: 'Cool tagline'
          @medium_2 = create :embedded_medium, name: 'A rather dull one'
        end

        it 'shows again all the records' do
          get :index, commit: I18n.t('activerecord.actions.shared.clear_filter')
          must_redirect_to media_path
        end
      end
    end

    describe 'GET new' do
      describe 'regular new' do
        it 'must redirect to the index page and tell the user the type is not selected' do
          get :new
          must_redirect_to media_path
          must_have_alert 'media.new.messages.missing_type'
        end
      end

      describe 'type-specific requests' do
        describe 'new audio medium' do
          it 'must succeed' do
            get :new_audio
            must_render :new
            assigns(:medium).must_be :new_record?
            assigns(:medium).must_be_instance_of AudioMedium
          end
        end

        describe 'new embedded medium' do
          it 'must succeed' do
            get :new_embedded
            must_render :new
            assigns(:medium).must_be :new_record?
            assigns(:medium).must_be_instance_of EmbeddedMedium
          end
        end

        describe 'new file medium' do
          it 'must succeed' do
            get :new_file
            must_render :new
            assigns(:medium).must_be :new_record?
            assigns(:medium).must_be_instance_of FileMedium
          end
        end

        describe 'new image medium' do
          it 'must succeed' do
            get :new_image
            must_render :new
            assigns(:medium).must_be :new_record?
            assigns(:medium).must_be_instance_of ImageMedium
          end
        end
      end
    end

    describe 'POST create' do
      describe 'when valid attributes are submitted' do
        it 'adds any type of media' do
          %i(audio_medium embedded_medium file_medium image_medium).each do |factory|
            it_adds_a Medium, factory: factory
          end
        end

        it 'redirects to show page' do
          post :create, medium: attributes_for(:image_medium)
          must_redirect_to medium_path(assigns(:medium))
        end

        describe 'when receiving a JSON request' do
          before do
            post :create, medium: attributes_for(:embedded_medium), format: :json
          end

          it 'creates the medium and returns its JSON representation' do
            expected = {
              'id' => assigns(:medium).id,
              'src' => assigns(:medium).thumbnail,
              'type' => assigns(:medium).type,
              'filename' => assigns(:medium).filename,
            }
            JSON.parse(response.body).must_equal expected
          end
        end
      end

      describe 'when invalid attributes are submitted' do
        before do
          post :create, medium: attributes_for(:embedded_medium, content: '')
        end

        it 'shows again the new page and displays the validation errors' do
          must_render :new
          assigns(:medium).must_be :new_record?
          must_display_form_errors
        end

        describe 'when receiving a JSON request' do
          before do
            post :create, medium: attributes_for(:embedded_medium, content: ''), format: :json
          end

          it 'returns a JSON document with the errors' do
            JSON.parse(response.body)['errors'].wont_be_empty
          end
        end
      end

      describe 'when an invalid media type is submitted' do
        let(:invalid_type) { 'Gotcha' }

        it "doesn't allow injecting invalid media types" do
          post :create, medium: attributes_for(:embedded_medium, type: invalid_type)
          must_redirect_to media_path
          must_have_alert 'media.new.messages.missing_type'
        end
      end
    end

    describe 'GET show' do
      let(:medium) { create :embedded_medium }

      it 'succeeds' do
        get :show, id: medium
        must_render :show
        assigns(:medium).must_equal medium
      end
    end

    describe 'GET edit' do
      let(:medium) { create :embedded_medium }

      it 'succeeds' do
        get :edit, id: medium
        must_render :edit
        assigns(:medium).must_equal medium
      end
    end

    describe 'GET find' do
      let(:medium) { create :image_medium }
      let(:article) { create :article }

      it 'succeeds' do
      #   get :find, id: medium, article_id: article.id, format: :json
      #   expected = {
      #     'preview' => preview_medium(medium, prefix: 'modal', controller: 'articles'),
      #     'caption' => article.try(:title),
      #     'medium_id' => medium.id
      #   }
      #   JSON.parse(response.body).must_equal expected
      skip
      end
    end

    describe 'PATCH update' do
      let(:medium) { create :embedded_medium }

      describe 'when valid information is sent' do
        let(:new_content) { 'modified content' }

        before do
          patch :update, id: medium, medium: attributes_for(:embedded_medium, content: new_content)
        end

        it 'updates the medium and redirects to show' do
          medium.reload
          medium.content.must_equal new_content
          must_redirect_to medium_url(assigns(:medium))
        end
      end

      describe 'when invalid information is sent' do
        let(:new_content) { '' }

        before do
          patch :update, id: medium, medium: attributes_for(:embedded_medium, content: new_content)
        end

        it 'fails to update, shows the errors and renders the edit view' do
          must_render :edit
          must_display_form_errors
          assigns(:medium).must_equal medium
        end
      end

      describe 'if the user tries to change the media type' do
        let(:medium) { create :embedded_medium }

        it 'refuses to change it, informing that to the user' do
          patch :update, id: medium, medium: attributes_for(:embedded_medium, type: 'FileMedium')
          must_render :edit
          must_display_form_errors
          assigns(:medium).type.must_equal medium.type
        end
      end
    end

    describe 'DELETE destroy' do
      describe 'when the medium is not in use' do
        it 'deletes the medium' do
          it_deletes_a Medium, factory: :image_medium
          must_redirect_to media_url, true
          must_have_notice 'media.destroy.messages.success'
        end
      end

      describe 'when medium is in use' do
        before do
          # Avoid permission checks, as we're only testing that the medium in use is not destroyed
          pretend_user_can(:update, Article)
        end

        it 'should not delete the medium and let the user know why the medium cannot be deleted' do
          it_doesnt_delete_a Medium, factory: :embedded_medium_in_use_by_article
          must_redirect_to media_url, true
          must_have_alert 'media.destroy.messages.already_in_use'
        end
      end
    end

    describe 'GET search' do
      let(:query) { {query: 'query'} }
      let(:page) { 1 }
      let(:per_page) { 10 }

      before do
        @expected = create :embedded_medium, name: 'are you querying me?'
      end

      it 'uses Medium#search to find matching media' do
        get :search, search: query, page: page, per_page: per_page
        must_render :search
        assigns(:media).must_equal [@expected]
      end
    end
  end
end

