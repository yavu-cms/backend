require 'test_helper'
require 'mocha/setup'

class BackupsControllerTest < ActionController::TestCase
  describe BackupsController do
    let(:permissions) { %w[ client_application/read client_application/design_manage ] }

    before do
      login permissions: permissions
      @controller = BackupsController.new
      @client_application = create :client_application
      @representation = create :representation, client_application: @client_application
      pretend_user_workspace_includes @client_application
      @backup = create :representation_backup, name: "2014-05-05 10:11:07 -0300", representation: @representation, client_application: @client_application
    end

    describe 'Permissions sanity checks' do
      let(:client_application_outside_workspace) { create :client_application }
      let(:representation_outside_workspace) { create :representation, client_application: client_application_outside_workspace }
      let(:backup_outside_workspace) { create :representation_backup, name: "2014-05-05 10:11:07 -0300", representation: representation_outside_workspace, client_application: client_application_outside_workspace }
      
      describe 'without permissions' do
        let(:permissions) { [] }

        describe 'when trying to get index' do
          it 'must be forbidden' do
            must_be_forbidden -> { get :index, client_application_id: @backup.client_application, representation_id: @backup.representation }
            must_be_forbidden -> { get :index, client_application_id: backup_outside_workspace.client_application, representation_id: backup_outside_workspace.representation }
          end
        end

        describe 'when trying to get show' do
          it 'must be forbidden' do
            must_be_forbidden -> { get :show, id: @backup, client_application_id: @backup.client_application, representation_id: @backup.representation }
            must_be_forbidden -> { get :show, id: backup_outside_workspace, client_application_id: backup_outside_workspace.client_application, representation_id: backup_outside_workspace.representation }
          end
        end

        describe 'when trying to delete destroy' do
          it 'must be forbidden' do
            must_be_forbidden -> { delete :destroy, client_application_id: @backup.client_application, representation_id: @backup.representation, id: @backup }
            must_be_forbidden -> { delete :destroy, client_application_id: backup_outside_workspace.client_application, representation_id: backup_outside_workspace.representation, id: backup_outside_workspace }
          end
        end

        describe 'when trying to post restore' do
          it 'must be forbidden' do
            must_be_forbidden -> { post :restore, client_application_id: @backup.client_application, representation_id: @backup.representation, id: @backup }
            must_be_forbidden -> { post :restore, client_application_id: backup_outside_workspace.client_application, representation_id: backup_outside_workspace.representation, id: backup_outside_workspace }
          end
        end
      end

      describe 'when have client application permissions' do
        let(:permissions) { %w[ client_application/read client_application/design_manage ] }

        describe 'when trying to get index' do
          it 'must be forbidden' do
            must_be_forbidden -> { get :index, client_application_id: backup_outside_workspace.client_application, representation_id: backup_outside_workspace.representation }
          end
        end

        describe 'when trying to get show' do
          it 'must be forbidden' do
            must_be_forbidden -> { get :show, id: backup_outside_workspace, client_application_id: backup_outside_workspace.client_application, representation_id: backup_outside_workspace.representation }
          end
        end

        describe 'when trying to delete destroy' do
          it 'must be forbidden' do
            must_be_forbidden -> { delete :destroy, client_application_id: backup_outside_workspace.client_application, representation_id: backup_outside_workspace.representation, id: backup_outside_workspace }
          end
        end

        describe 'when trying to post restore' do
          it 'must be forbidden' do
            must_be_forbidden -> { post :restore, client_application_id: backup_outside_workspace.client_application, representation_id: backup_outside_workspace.representation, id: backup_outside_workspace }
          end
        end
      end
    end

    describe 'GET index' do
      it 'must succeed' do
        get :index, client_application_id: @backup.client_application, representation_id: @backup.representation
        assigns(:client_application).wont_be_nil
        assigns(:representation).wont_be_nil
      end
    end

    describe 'GET show' do
      it 'must render correctly' do
        get :show, id: @backup, client_application_id: @backup.client_application, representation_id: @backup.representation
        must_render :show
      end
    end

    describe 'DELETE destroy' do
      it 'must destroy current backup' do
        delete :destroy, client_application_id: @backup.client_application, representation_id: @backup.representation, id: @backup
        must_redirect_to client_application_representation_backups_path(@backup.client_application, @backup.representation)
        must_have_notice 'representations/backups.actions.destroy.messages.success'
      end
    end

    describe 'POST restore' do
      before do
        RepresentationBackup.any_instance.expects(:restore!).returns(true).once
      end

      it 'restores this backup and shows current representation' do
        get :restore,
          client_application_id: @backup.client_application, representation_id: @backup.representation, id: @backup
        must_redirect_to client_application_representation_path(@backup.client_application, @backup.representation)
      end
    end
  end
end
