require 'test_helper'

class ComponentViewsControllerTest < ActionController::TestCase
  describe ComponentViewsController do
    let(:permissions) { %w[ component/manage ] }

    before do
      login permissions: permissions
      @controller = ComponentViewsController.new
      @component = create :component
    end

    describe 'Permissions sanity checks' do
      describe 'without permission' do
        let(:permissions) { [] }

        describe 'read component views' do
          describe 'when trying to get index' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :index, component_id: @component }
            end
          end

          describe 'when trying to get show' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :show, component_id: @component, id: @component.views.first }
            end
          end
        end

        describe 'create a component view' do
          describe 'when trying to get new' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :new, component_id: @component }
            end
          end

          describe 'when trying to post create' do
            it 'must be forbidden' do
              must_be_forbidden -> { it_adds_a View, options: { component_id: @component } }
            end
          end
        end

        describe 'updating a component view' do
          describe 'when trying to get edit' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :edit, component_id: @component, id: @component.views.first }
            end
          end

          describe 'when trying to put update' do
            it 'must be forbidden' do
              view = @component.views.first
              view.name = 'component name'
              must_be_forbidden -> { put :update, component_id: @component, id: view, view: view.attributes }
            end
          end

          describe 'when trying to post make default' do
            it 'must be forbidden' do
              must_be_forbidden -> { post :make_default, component_id: @component, id: @component.views.first }
            end
          end
        end

        describe 'delete a component view' do
          describe 'when trying to delete destroy' do
            it 'must be forbidden' do
              view = create :component_view, component: @component
              must_be_forbidden -> { it_deletes_a View, factory: view, options: { component_id: @component }  }
            end
          end
        end
      end
    end

    describe 'get index' do
      before do
        create :component_view, component: @component
      end

      it 'must succeed' do
        get :index, component_id: @component
        must_render :index
        @component.views.wont_be :empty?
        assigns(:component_views).count.must_equal @component.views.count
      end
    end

    describe 'get new' do
      it 'must succeed' do
        get :new, component_id: @component
        must_render :new
        assigns(:component_view).must_be :new_record?
      end
    end

    describe 'post create' do
      describe 'when succeeds' do
        it 'adds a component_view' do
          it_adds_a View, options: { component_id: @component }
          assigns(:component_view).wont_be :new_record?
          must_redirect_to component_view_url(@component, assigns(:component_view))
          must_have_notice 'component/views.new.messages.success'
        end
      end

      describe 'when fails' do
        it 'shows again new page and displays the validation errors' do
          # Must raise an error because views name is nil
          post :create, view: attributes_for(:component_view, name: nil), component_id: @component
          must_render :new
          must_display_form_errors
          assigns(:component_view).must_be :new_record?
        end
      end
    end

    describe 'get show' do
      it 'must succeed' do
        view = @component.views.first
        get :show, component_id: @component, id: view
        must_render :show
      end
    end

    describe 'get edit' do
      it 'must succeed' do
        view = @component.views.first
        get :edit, component_id: @component, id: view
        must_render :edit
        assigns(:component_view).must_equal view
      end
    end

    describe 'patch update' do
      describe 'when succeeds' do
        it 'updates the view and redirects to show' do
          view = @component.views.first

          modified_value = 'Goodbye cruel world'

          patch :update, id: view, component_id: @component, view: {name: view.name, value: modified_value }

          must_redirect_to component_view_url(@component, view)
          assigns(:component_view).wont_be :new_record?
          assigns(:component_view).value.must_equal modified_value
          must_have_notice 'component/views.update.messages.success'
        end
      end

      describe 'when fails' do
        it 'shows again edit page and displays the validation errors' do
          view = @component.views.first
          # try to edit without a name
          patch :update, id: view, component_id: view.component, view: {name: nil}

          must_render :edit
          must_display_form_errors
          assigns(:component_view).wont_be :new_record?
        end
      end
    end

    describe 'POST make_default' do
      it 'needs tests'
    end

    describe 'delete destroy' do
      it 'deletes the view' do
        view = create :component_view, component: @component
        it_deletes_a View, factory: view, options: { component_id: @component }
        must_redirect_to component_views_url(@component)
        must_have_notice 'component/views.destroy.messages.success'
      end

      it "won't delete the view if it is the last one for its parent component" do
        view = @component.views.first
        @component.views.count.must_equal 1
        it_doesnt_delete_a View, factory: view, options: { component_id: @component }
        must_redirect_to component_views_url(@component)
        must_have_alert 'component/views.destroy.messages.fail'
      end
    end
  end
end
