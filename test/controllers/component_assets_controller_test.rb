require 'test_helper'

class ComponentAssetsControllerTest < ActionController::TestCase
  describe ComponentAssetsController do
    let(:permissions) { %w[ component/manage ] }

    before do
      login permissions: permissions
      @controller = ComponentAssetsController.new
      @component = create :component
      ComponentAssetUploader.root = Rails.root.join(*%w{tmp tests component_assets_controller})
    end

    after do
      FileUtils.remove_dir(ComponentAssetUploader.root, true)
    end

    describe 'Permissions sanity checks' do
      describe 'without permissions' do
        let(:permissions) { [] }
        describe 'create a component asset' do
          describe 'when trying to get new' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :new, component_id: @component }
            end
          end

          describe 'when trying to post create' do
            it 'must be forbidden' do
              must_be_forbidden -> { it_adds_a ComponentAsset, options: { component_id: @component }  }
            end
          end

          describe 'when trying to get new empty' do
            it 'must be must be forbidden' do
              must_be_forbidden -> { get :new_empty, component_id: @component }
            end
          end

          describe 'when trying to post create empty' do
            it 'must be forbidden' do
              must_be_forbidden -> { post :create_empty, component_asset: {type: 'js', file: 'some_name'}, component_id: @component }
            end
          end
        end

        describe 'read a component asset' do
          describe 'when trying to get index' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :index, component_id: @component }
            end
          end

          describe 'when trying to get show' do
            it 'must be forbidden' do
              component_asset = create :component_asset_editable
              must_be_forbidden -> { get :show, component_id: component_asset.component, id: component_asset }
            end
          end
        end

        describe 'updating component assets' do
          describe 'when trying to get edit' do
            it 'must be forbidden' do
              component_asset = create :component_asset_editable
              must_be_forbidden -> { get :edit, component_id: component_asset.component , id: component_asset }
            end
          end

          describe 'when trying to patch update' do
            it 'must be forbidden' do
              component_asset = create :component_asset
              @other_file = upload_test_file('image_2.jpg')
              must_be_forbidden -> { patch :update, id: component_asset, component_asset: attributes_for(:component_asset, file: @other_file), component_id: component_asset.component }
            end
          end

          describe 'when trying to get edit content' do
            it 'must be forbidden' do
              component_asset = create :component_asset
              must_be_forbidden -> { get :edit, component_id: component_asset.component, id: component_asset }
            end
          end

          describe 'when trying to patch update content' do
            it 'must be forbidden' do
              component_asset_editable = create :component_asset_editable
              @content = 'new content'
              must_be_forbidden -> { patch :update_content,
                id: component_asset_editable,
                component_asset: attributes_for(:component_asset_editable, content: @content),
                component_id: component_asset_editable.component }
            end
          end

          describe 'when trying to get rename' do
            it 'must be forbidden' do
              component_asset = create :component_asset
              must_be_forbidden -> { get :rename, component_id: component_asset.component, id: component_asset }
            end
          end

          describe 'when trying to patch update name' do
            it 'must be forbidden' do
              component_asset_editable = create :component_asset, component: @component
              must_be_forbidden -> { patch :update_name, id: component_asset_editable, component_id: @component, component_asset: { file: 'new_name' } }
            end
          end
        end

        describe 'delete a component asset' do
          describe 'when trying to delete destroy' do
            it 'must be forbidden' do
              component_asset = create :component_asset
              must_be_forbidden-> { it_deletes_a ComponentAsset, factory: component_asset, options: { component_id: component_asset.component } }
            end
          end
        end
      end
    end

    describe 'get index' do
      it 'must succeed' do
        component_asset_editable = create :component_asset_editable
        component_asset_not_editable = create :component_asset_not_editable, component: component_asset_editable.component
        get :index, component_id: component_asset_editable.component
        must_render :index
        assigns(:component_assets).count.must_equal 2

        assert_select "#action-dropdown-#{component_asset_editable.id} a[href=?]",
          edit_content_component_asset_path(component_asset_editable.component, component_asset_editable)

        assert_select "#action-dropdown-#{component_asset_not_editable.id} a[href=?]",
          edit_content_component_asset_path(component_asset_not_editable.component, component_asset_not_editable), false
      end
    end

    describe 'get new' do
      it 'must succeed' do
        get :new, component_id: @component
        must_render :new
        assert_select '#new_component_asset[action=?]', component_assets_path(@component)
        assigns(:component_asset).must_be :new_record?
      end
    end

    describe 'get new_empty' do
      it 'must succeed' do
        get :new_empty, component_id: @component
        must_render :new_empty
        assigns(:component_asset).must_be :new_record?
      end
    end

    describe 'get rename' do
      before do
        @component_asset = create :component_asset, component: @component
      end

      it 'must succeed' do
        get :rename, component_id: @component, id: @component_asset
        must_render :rename
        assigns(:component_asset).wont_be :new_record?
      end
    end

    describe 'post create_empty' do
      describe 'when file name is correct' do
        it 'must succeed' do
          post :create_empty, component_asset: {type: 'js', file: 'some_name'}, component_id: @component
          must_redirect_to component_asset_path(@component, assigns(:component_asset))
          assigns(:component_asset).wont_be :new_record?
          must_have_notice 'component/assets.new_empty.success'
        end
      end

      describe 'when file name is being used' do
        it 'must fail' do
          # Factory for component_asset has a file named 'image_1.jpg'
          create :component_asset, component: @component
          post :create_empty, component_asset: {type: 'jpg', file: 'image_1'}, component_id: @component
          must_display_form_errors
          must_render :new_empty
        end
      end
    end

    describe 'post create' do
      describe 'when succeeds' do
        it "adds a component_asset" do
          it_adds_a ComponentAsset, options: { component_id: @component }
          assigns(:component_asset).wont_be :new_record?
          must_redirect_to component_asset_url(@component,assigns(:component_asset))
          must_have_notice 'component/assets.new.messages.success'
        end
      end

      describe 'when fails' do
        it "shows again new page and displays the validation errors" do
          component_asset = create :component_asset
          # Must raise an error because it just exists
          post :create,
            component_asset: attributes_for(:component_asset),
            component_id: component_asset.component
          must_render :new
          assigns(:component_asset).must_be :new_record?
          must_display_form_errors
        end
      end
    end

    describe 'get show' do
      it 'must succeed when not editable? and wont add action to edit content' do
        component_asset = create :component_asset_not_editable
        get :show, component_id: component_asset.component, id: component_asset
        must_render :show
        assigns(:component_asset).wont_be :new_record?
        assert_select ".context-actions li a[href=?]",
          edit_content_component_asset_path(component_asset.component, component_asset), false
      end

      it 'must succeed when editable? and add action to edit content' do
        component_asset = create :component_asset_editable
        get :show, component_id: component_asset.component, id: component_asset
        must_render :show
        assigns(:component_asset).wont_be :new_record?
        assert_select ".context-actions li a[href=?]",
          edit_content_component_asset_path(component_asset.component, component_asset)
      end

    end

    describe 'get edit' do
      it 'must succeed' do
        component_asset = create :component_asset
        get :edit, component_id: component_asset.component, id: component_asset
        must_render :edit
        assert_select "#edit_component_asset_#{component_asset.id}[action=?]",
          component_asset_path(component_asset.component, component_asset)
      end

    end

    describe "patch update" do
      describe 'when succeeds' do
        it 'updates the asset and redirects to show' do
          component_asset = create :component_asset
          @other_file = upload_test_file('image_2.jpg')
          patch :update,
            id: component_asset,
            component_asset: attributes_for(:component_asset, file: @other_file),
            component_id: component_asset.component

          assigns(:component_asset).wont_be :new_record?
          must_redirect_to component_asset_url(component_asset.component, assigns(:component_asset))
          assigns(:component_asset).file_identifier.must_equal @other_file.original_filename
          must_have_notice 'component/assets.update.messages.success'
        end
      end

      describe 'when fails' do
        it "shows again edit page and displays the validation errors" do
          component_asset = create :component_asset
          file_exists = upload_test_file('image_2.jpg')
          create(:component_asset, component: component_asset.component, file: file_exists)
          # A component_asset can't be saved when duplicates other entry
          patch :update,
            id: component_asset ,
            component_asset: attributes_for(:component_asset, file: file_exists),
            component_id: component_asset.component
          must_render :edit
          assigns(:component_asset).wont_be :new_record?
          must_display_form_errors
        end
      end
    end

    describe 'get edit content' do
      it 'must succeed for editable?' do
        component_asset_editable = create :component_asset_editable
        get :edit_content, component_id: component_asset_editable.component, id: component_asset_editable
        must_render :edit_content
        component_asset = assigns(:component_asset)
        component_asset.wont_be :new_record?
        assert_select "#edit_component_asset_#{component_asset.id}[action=?]",
          update_content_component_asset_path(component_asset.component, component_asset)
      end


      it 'must fail for not editable?' do
        component_asset_not_editable = create :component_asset_not_editable

        get :edit_content, component_id: component_asset_not_editable.component, id: component_asset_not_editable
        must_redirect_to component_asset_url(component_asset_not_editable.component, component_asset_not_editable)
        assigns(:component_asset).wont_be :new_record?
        must_have_alert 'component/assets.edit_content.messages.error'
      end
    end

    describe "patch update content" do
      describe 'when succeeds' do
        it 'updates content of component asset' do
          component_asset_editable = create :component_asset_editable
          @content = 'new content'

          patch :update_content,
            id: component_asset_editable,
            component_asset: attributes_for(:component_asset_editable, content: @content),
            component_id: component_asset_editable.component

          must_redirect_to component_asset_url(component_asset_editable.component, assigns(:component_asset))

          assigns(:component_asset).wont_be :new_record?

          IO.read(assigns(:component_asset).file.current_path).must_equal @content

          must_have_notice 'component/assets.update.messages.success'
        end
      end
    end

    describe 'patch update name' do
      describe 'when succeeds' do
        before do
          @asset = create :component_asset, component: @component
        end

        it 'redirects to component assets' do
          patch :update_name, id: @asset, component_id: @asset.component, component_asset: { file: 'new_name' }

          must_redirect_to component_asset_path(@asset.component, assigns(:component_asset))
        end
      end

      describe 'when fails' do
        before do
          @asset = create :component_asset, component: @component
          @asset_2 = create :component_asset, file: upload_test_file('image_2.jpg'), component: @component
        end

        it 'renders rename action' do
          patch :update_name, id: @asset_2, component_id: @asset_2.component, component_asset: { file: 'image_1' }

          must_render :rename
        end
      end
    end

    describe 'delete destroy' do
      it 'deletes the component_asset' do
        component_asset = create :component_asset
        it_deletes_a ComponentAsset, factory: component_asset, options: { component_id: component_asset.component }
        must_redirect_to component_assets_url(component_asset.component)
        must_have_notice 'component/assets.destroy.messages.success'
      end
    end
  end
end
