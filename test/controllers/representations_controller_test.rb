require 'test_helper'

class RepresentationsControllerTest < ActionController::TestCase
  describe RepresentationsController do
    let(:permissions) { %w[ client_application/read client_application/design_manage ] }

    before do
      login permissions: permissions
      @controller = RepresentationsController.new
      @client_application = create :client_application
      pretend_user_workspace_includes @client_application
    end

    describe 'Permissions sanity checks' do
      describe 'design manage a client application' do
        let(:client_application_outside_workspace) { create :client_application }
        let(:representation_outside_workspace) { create :representation, client_application: client_application_outside_workspace }

        describe 'without permissions' do
          let(:permissions) { [] }
          let(:representation) { create :representation, client_application: @client_application }

          describe 'when trying to get index' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :index, client_application_id: @client_application }
              must_be_forbidden -> { get :index, client_application_id: client_application_outside_workspace }
            end
          end

          describe 'when trying to get show' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :show, client_application_id: @client_application, id: representation }
              must_be_forbidden -> { get :show, client_application_id: client_application_outside_workspace, id: representation_outside_workspace }
            end
          end

          describe 'when trying to get edit' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :edit, client_application_id: @client_application, id: representation }
              must_be_forbidden -> { get :edit, client_application_id: client_application_outside_workspace, id: representation_outside_workspace }
            end
          end

          describe 'when trying to put update' do
            it 'must be forbidden' do
              updated_representation = representation
              updated_representation_ouside_workspace = representation_outside_workspace
              updated_representation.name = 'updated name'
              updated_representation_ouside_workspace.name = 'other updated name'
              must_be_forbidden -> { put :update, client_application_id: @client_application, id: representation, representation: updated_representation }
              must_be_forbidden -> { put :update, client_application_id: client_application_outside_workspace, id: representation_outside_workspace, representation: updated_representation_ouside_workspace }
            end
          end

          describe 'when trying to delete destroy' do
            it 'must be forbidden' do
              must_be_forbidden -> { delete :destroy, client_application_id: @client_application, id: representation }
              must_be_forbidden -> { delete :destroy, client_application_id: client_application_outside_workspace, id: representation_outside_workspace }
            end
          end

          describe 'when trying to get new' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :new, client_application_id: @client_application }
              must_be_forbidden -> { get :new, client_application_id: client_application_outside_workspace }
            end
          end

          describe 'when trying to post create' do
            it 'must be forbidden' do
              must_be_forbidden -> { post :create, client_application_id: @client_application, representation: attributes_for(:representation) }
              must_be_forbidden -> { post :create, client_application_id: client_application_outside_workspace, representation: attributes_for(:representation) }
            end
          end

          describe 'when trying to get rename' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :rename, client_application_id: @client_application, id: representation }
              must_be_forbidden -> { get :rename, client_application_id: client_application_outside_workspace, id: representation_outside_workspace }
            end
          end

          describe 'when trying to patch update name' do
            it 'must be forbidden' do
              must_be_forbidden -> { patch :update_name, client_application_id: @client_application, id: representation, representation: { name: 'new_name' } }
              must_be_forbidden -> { patch :update_name, client_application_id: client_application_outside_workspace, id: representation_outside_workspace, representation: { name: 'new_name' } }
            end
          end

          describe 'whe trying to get copy' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :copy, client_application_id: @client_application, id: representation }
              must_be_forbidden -> { get :copy, client_application_id: client_application_outside_workspace, id: representation_outside_workspace }
            end
          end

          describe 'when trying to post create copy' do
            it 'must be forbidden' do
              must_be_forbidden -> { post :create_copy, client_application_id: @client_application, id: representation, representation: attributes_for(:representation) }
              must_be_forbidden -> { post :create_copy, client_application_id: client_application_outside_workspace, id: representation_outside_workspace, representation: attributes_for(:representation) }
            end
          end

          describe 'when trying to delete discard draft' do
            it 'must be forbidden' do
              must_be_forbidden -> { delete :discard_draft, client_application_id: @client_application, id: representation }
              must_be_forbidden -> { delete :discard_draft, client_application_id: client_application_outside_workspace, id: representation_outside_workspace }
            end
          end

          describe 'when trying to get preview' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :preview, client_application_id: @client_application, id: representation }
              must_be_forbidden -> { get :preview, client_application_id: client_application_outside_workspace, id: representation_outside_workspace }
            end
          end

          describe 'when trying to post apply' do
            it 'must be forbidden' do
              must_be_forbidden -> { post :apply, client_application_id: @client_application, id: representation }
              must_be_forbidden -> { post :apply, client_application_id: client_application_outside_workspace, id: representation_outside_workspace }
            end
          end

          describe 'when trying to get manage assets' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :manage_assets, client_application_id: @client_application, id: representation }
              must_be_forbidden -> { get :manage_assets, client_application_id: client_application_outside_workspace, id: representation_outside_workspace }
            end
          end
        end

        describe 'when have design_manage and read client application permission' do
          let(:permissions) { %w[ client_application/read client_application/design_manage ] }

          describe 'when trying to get index' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :index, client_application_id: client_application_outside_workspace }
            end
          end

          describe 'when trying to get show' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :show, client_application_id: client_application_outside_workspace, id: representation_outside_workspace }
            end
          end

          describe 'when trying to get edit' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :edit, client_application_id: client_application_outside_workspace, id: representation_outside_workspace }
            end
          end

          describe 'when trying to put update' do
            it 'must be forbidden' do
              updated_representation_ouside_workspace = representation_outside_workspace
              updated_representation_ouside_workspace.name = 'other updated name'
              must_be_forbidden -> { put :update, client_application_id: client_application_outside_workspace, id: representation_outside_workspace, representation: updated_representation_ouside_workspace }
            end
          end

          describe 'when trying to delete destroy' do
            it 'must be forbidden' do
              must_be_forbidden -> { delete :destroy, client_application_id: client_application_outside_workspace, id: representation_outside_workspace }
            end
          end

          describe 'when trying to get new' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :new, client_application_id: client_application_outside_workspace }
            end
          end

          describe 'when trying to post create' do
            it 'must be forbidden' do
              must_be_forbidden -> { post :create, client_application_id: client_application_outside_workspace, representation: attributes_for(:representation) }
            end
          end

          describe 'when trying to get rename' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :rename, client_application_id: client_application_outside_workspace, id: representation_outside_workspace }
            end
          end

          describe 'when trying to patch update name' do
            it 'must be forbidden' do
              must_be_forbidden -> { patch :update_name, client_application_id: client_application_outside_workspace, id: representation_outside_workspace, representation: { name: 'new_name' } }
            end
          end

          describe 'whe trying to get copy' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :copy, client_application_id: client_application_outside_workspace, id: representation_outside_workspace }
            end
          end

          describe 'when trying to post create copy' do
            it 'must be forbidden' do
              must_be_forbidden -> { post :create_copy, client_application_id: client_application_outside_workspace, id: representation_outside_workspace, representation: attributes_for(:representation) }
            end
          end

          describe 'when trying to delete discard draft' do
            it 'must be forbidden' do
              must_be_forbidden -> { delete :discard_draft, client_application_id: client_application_outside_workspace, id: representation_outside_workspace }
            end
          end

          describe 'when trying to get preview' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :preview, client_application_id: client_application_outside_workspace, id: representation_outside_workspace }
            end
          end

          describe 'when trying to post apply' do
            it 'must be forbidden' do
              must_be_forbidden -> { post :apply, client_application_id: client_application_outside_workspace, id: representation_outside_workspace }
            end
          end

          describe 'when trying to get manage assets' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :manage_assets, client_application_id: client_application_outside_workspace, id: representation_outside_workspace }
            end
          end
        end
      end
    end

    describe 'before filters' do
      describe 'when no valid client application id is provided' do
        before do
          get :index, client_application_id: -@client_application.id
        end

        it 'should redirect to client applications controller with a proper flash error message' do
          must_redirect_to client_applications_path
          must_have_alert 'representations.errors.invalid_client_application_or_representation'
        end
      end

      describe 'when the requested representation belongs to the client application' do
        before do
          @representation = create :representation, client_application: @client_application
        end

        it 'succeeds showing the representation' do
          scoped_get :show, params: { id: @representation }
          must_render :show
          assigns(:representation).must_equal @representation
        end
      end

      describe 'when the requested representation belongs to another client application' do
        before do
          @another_client_app = create :client_application
          @representation = create :representation, client_application: @another_client_app
        end

        it 'should redirect to the representations list and show an alert' do
          scoped_get :show, params: { id: @representation }
          must_redirect_to client_applications_path
          must_have_alert 'representations.errors.invalid_client_application_or_representation'
        end
      end
    end

    describe 'GET index' do
      describe 'on a regular request' do
        before do
          create :representation, client_application: @client_application
          create :representation, client_application: @client_application
          # This representation will belong to another client application, and thus should not be shown
          create :representation, client_application: create(:client_application)
        end

        it 'must succeed' do
          scoped_get :index
          must_render :index
          assigns(:representations).count.must_equal 2
        end
      end

      describe 'when specifying a sort criterion' do
        before do
          @rep2 = create :representation, name: 'I go second', client_application: @client_application
          @rep4 = create :representation, name: 'Zap! I go last', client_application: @client_application
          @rep1 = create :representation, name: 'I go first!', client_application: @client_application
          @rep3 = create :representation, name: 'Mmm.. I go third', client_application: @client_application
        end

        it 'sorts results using it' do
          scoped_get :index, params: { sort: { field: 'name', order: 'asc' } }
          assigns(:representations).must_equal [@rep1, @rep2, @rep3, @rep4]
          must_render :index
        end
      end

      describe 'when specifying a page' do
        let(:per_page) { 2 }
        let(:page) { 2 }

        before do
          @rep1 = create :representation, name: 'Rep. 1', client_application: @client_application, created_at: 3.minutes.ago
          @rep2 = create :representation, name: 'Rep. 2', client_application: @client_application, created_at: 2.minutes.ago
          @rep3 = create :representation, name: 'Rep. 3', client_application: @client_application, created_at: 1.minutes.ago
        end

        it 'shows records for that page' do
          scoped_get :index, params: { page: page, per_page: per_page }
          must_render :index
          assigns(:representations).count.must_equal 1
          assigns(:representations).first.must_equal @rep3
        end
      end

      describe 'when specifying the number of elements per page to show' do
        let(:per_page) { 3 }

        before do
          10.times { create :representation, client_application: @client_application }
        end

        it 'shows at most that number of records' do
          scoped_get :index, params: { per_page: per_page }
          must_render :index
          assigns(:representations).count.must_equal per_page
        end
      end

      describe 'when filtering results' do
        before do
          @representation_1 = create :representation, name: 'Superb Representation', client_application: @client_application
          @representation_2 = create :representation, name: 'Cool representation', client_application: @client_application
          @representation_3 = create :representation, name: 'The other thing', client_application: @client_application
        end

        it 'show the matching records' do
          scoped_get :index, params: { filter: { name: 'representation' } }
          must_render :index
          assigns(:representations).sort.must_equal [@representation_1, @representation_2].sort
        end

        it 'ignores bad params' do
          scoped_get :index, params: { filter: { name: 'representation', sneaky: 'some value' } }
          must_render :index
          assigns(:representations).sort.must_equal [@representation_1, @representation_2].sort
        end
      end

      describe 'when reset is submitted' do
        before do
          scoped_get :index, params: { commit: I18n.t('activerecord.actions.shared.clear_filter') }
        end

        it 'clears the filters and redirects to index' do
          assigns(:filter).values.must_equal RepresentationsFilter.defaults
          must_redirect_to client_application_representations_url(@client_application)
        end
      end
    end

    describe 'GET show' do
      before do
        @representation = create :representation, client_application: @client_application
      end

      it 'succeeds showing the representation' do
        scoped_get :show, params: { id: @representation }
        must_render :show
        assigns(:representation).must_equal @representation
      end
    end

    describe 'GET new' do
      it 'builds a new representation for the requested client application' do
        scoped_get :new
        must_render :new
        assigns(:representation).must_be :new_record?
        assigns(:representation).client_application.must_equal @client_application
      end
    end

    describe 'GET rename' do
      before do
        @representation = create :representation, client_application: @client_application
      end

      it 'succeeds and works directly on the representation (no draft involved)' do
        scoped_get :rename, params: { id: @representation }
        must_render :rename
        assigns(:representation).must_equal @representation
      end
    end

    describe 'GET edit' do
      describe 'when a draft is not available' do
        before do
          @representation = create :cover_representation, client_application: @client_application
          Representation.any_instance.expects(:trigger_draft_creation).once
          scoped_get :edit, params: { id: @representation }
        end

        it 'redirects to the index page with an alert message' do
          must_redirect_to client_application_representations_path(@client_application)
          must_have_alert 'representations.errors.draft_unavailable'
        end
      end

      describe 'when the representation already has a draft' do
        before do
          @representation = create :representation, client_application: @client_application
          @draft = create :representation_draft, representation: @representation, client_application: @client_application
        end

        it 'uses the existing draft and succeeds' do
          scoped_get :edit, params: { id: @representation }
          must_render :edit
          assigns(:draft).must_equal @draft
          assigns(:representation).must_equal @draft
          assigns(:draft).representation.must_equal @representation
          @representation.reload
          @representation.must_be :has_draft?
        end
      end
    end

    describe 'POST create' do
      describe 'when the information is valid' do
        before do
          scoped_post :create, params: { representation: attributes_for(:representation) }
        end

        it 'creates the new representation and redirects to the edit action to allow changing its structure' do
          must_redirect_to edit_client_application_representation_path(@client_application, assigns(:representation))
          must_have_notice 'representations.new.messages.success'
          assigns(:representation).must_be :persisted?
        end
      end

      describe 'when the information is not valid' do
        before do
          scoped_post :create, params: { representation: attributes_for(:representation, name: '') }
        end

        it 'renders back the new page and shows the errors' do
          must_render :new
          must_display_form_errors
        end
      end
    end

    describe 'PUT update' do
      let(:json_configuration) { JSON.dump configuration }
      after { UniqueAccessWorker.unstub :perform_async }

      describe 'when a draft is not available' do
        before do
          @representation = create :cover_representation, client_application: @client_application
          Representation.any_instance.expects(:trigger_draft_creation).once
          UniqueAccessWorker.expects(:perform_async).never
          scoped_put :update, params: { id: @representation }
        end

        it 'redirects to the index page with an alert message' do
          must_redirect_to client_application_representations_path(@client_application)
          must_have_alert 'representations.errors.draft_unavailable'
        end
      end

      describe 'when the information is valid' do
        let(:configuration) { {'nevermind' => true}}

        before do
          @representation = create :cover_representation, client_application: @client_application
          @representation.draft = @representation.generate_draft
          RepresentationDraft.any_instance.expects(:from_json).with(json_configuration).returns(true).once
          UniqueAccessWorker.expects(:perform_async).with("/uniqueaccess/representation/#{@representation.draft.to_param}", {user: authenticated.username, saved: true}, 'http://test.host:5000/wsock')
          scoped_put :update, params: {id: @representation, representation: {configuration: json_configuration}, format: :json}
        end

        after do
          Representation.any_instance.unstub(:from_json)
        end

        it 'updates the representation and returns a success notice' do
          must_render nil
          parsed_response = JSON.parse(response.body).symbolize_keys
          parsed_response.must_equal notice: I18n.t('representations.save_draft.messages.success'), lock_version: @representation.lock_version
        end
      end

      describe 'when the information is not valid' do
        let(:configuration) { Hash.new }

        before do
          @representation = create :cover_representation, client_application: @client_application
          @representation.draft = @representation.generate_draft
          UniqueAccessWorker.expects(:perform_async).never
          scoped_put :update, params: {id: @representation, representation: {configuration: json_configuration}, format: :json}
          @representation.reload
        end

        it 'returns a 422 HTTP status code and describes the validation errors, without updating the representation' do
          assert_response 422
          failures = [I18n.t('errors.messages.invalid')]
          parsed_response = JSON.parse(response.body).symbolize_keys
          parsed_response.must_equal alert: I18n.t('representations.apply_draft.messages.fail'),
                                     failures: failures
          @representation.rows.wont_be_empty
        end
      end

      describe 'when the record is stale' do
        let(:configuration) { {'nevermind' => true}}

        after do
          RepresentationDraft.any_instance.unstub(:from_json)
        end

        it 'returns a 422 HTTP status code when when draft is stale' do
          representation = create :cover_representation, client_application: @client_application
          draft = representation.draft = representation.generate_draft
          RepresentationDraft.any_instance.expects(:from_json).with(json_configuration).raises(ActiveRecord::StaleObjectError.new(draft, :save)).once
          UniqueAccessWorker.expects(:perform_async).never
          scoped_put :update, params: { id: representation, representation: {lock_version: draft.lock_version, configuration: json_configuration}, format: :json }
          representation.reload
          assert_response 422
          failures = [I18n.t('activerecord.errors.stale_object')]
          parsed_response = JSON.parse(response.body).symbolize_keys
          parsed_response.must_equal alert: I18n.t('representations.apply_draft.messages.fail'),
                                     failures: failures,
                                     lock_version: representation.draft.lock_version
          representation.rows.wont_be_empty
        end
      end
    end

    describe 'GET preview' do
      describe 'when a draft is not available' do
        before do
          @representation = create :cover_representation, client_application: @client_application
          Representation.any_instance.expects(:trigger_draft_creation).once
          scoped_get :preview, params: { id: @representation }
        end

        it 'redirects to the index page with an alert message' do
          must_redirect_to client_application_representations_path(@client_application)
          must_have_alert 'representations.errors.draft_unavailable'
        end
      end

      describe 'go back link' do
        describe 'when the request specifies a :gbt parameter' do
          it 'offers a go back link to the specified url'
        end

        describe "when the request doesn't specify a :gbt parameter" do
          it 'offers a go back link to the show page for the representation'
        end
      end

      describe 'previewing' do
        it "shows a preview for the representation's draft"
      end

      describe 'when the original representation is in use by a route' do
        before do
          @representation = create :cover_representation, client_application: @client_application
          @representation.draft = @representation.generate_draft
          @route = create :homepage_route, client_application: @client_application, representation: @representation

          scoped_get :preview, params: { id: @representation }
        end

        it 'suggests that route as the default via @default_route' do
          assigns(:default_route).must_equal @route
        end
      end

      describe 'when the original representation is not in use by any route' do
        before do
          @representation = create :cover_representation, client_application: @client_application
          @representation.draft = @representation.generate_draft
          @client_application.routes.clear

          scoped_get :preview, params: { id: @representation }
        end

        it "doesn't suggest any route (@default_route is nil)" do
          assigns(:default_route).must_be_nil
        end
      end
    end

    describe 'GET copy' do
      before do
        @representation = create :representation, client_application: @client_application
        scoped_get :copy, params: { id: @representation }
      end

      it 'builds a new representation based on the requested one, without persisting it' do
        must_render :copy
        assigns(:representation).must_be :new_record?
        assigns(:representation).name.wont_equal @representation.name
      end
    end

    describe 'POST create_copy' do
      before do
        @representation = create :representation, client_application: @client_application
      end

      it 'creates a copy of this representation with a new name' do
        post :create_copy, representation: { name: 'new copy' }, client_application_id: @client_application, id: @representation
        assigns(:representation).wont_be :new_record?
        assigns(:representation).wont_equal @representation
        assigns(:representation).name.must_equal 'new copy'
        must_have_notice 'representations.copy.messages.success'
      end
    end

    describe 'DELETE discard_draft' do
      after { UniqueAccessWorker.unstub :perform_async }

      describe 'when the representations has a draft' do
        before do
          @representation = create :representation, client_application: @client_application
          @representation.draft = create(:representation_draft, representation: @representation)
        end

        it 'destroys it' do
          UniqueAccessWorker.expects(:perform_async).with("/uniqueaccess/representation/#{@representation.draft.to_param}", {user: authenticated.username, destroyed: true}, 'http://test.host:5000/wsock')
          delete :discard_draft, client_application_id: @client_application, id: @representation
          must_have_notice 'representations.discard_draft.messages.success'
          must_redirect_to client_application_representations_path(@client_application)
        end
      end

      describe "when the representations doesn't have a draft" do
        before do
          @representation = create :representation, client_application: @client_application
          UniqueAccessWorker.expects(:perform_async).never
        end

        it 'shows fail message' do
          delete :discard_draft, client_application_id: @client_application, id: @representation
          must_have_alert 'representations.discard_draft.messages.failed'
          must_redirect_to client_application_representations_path(@client_application)
        end
      end
    end

    describe 'DELETE destroy' do
      describe 'when it can be destroyed' do
        before do
          @representation = create :representation, client_application: @client_application
          @representation.draft = create(:representation_draft, representation: @representation)
          UniqueAccessWorker.expects(:perform_async).with("/uniqueaccess/representation/#{@representation.draft.to_param}", {user: authenticated.username, destroyed: true}, 'http://test.host:5000/wsock')
          scoped_delete :destroy, params: { id: @representation }
        end

        it 'destroys the representation and redirects to the representations list with a notice' do
          must_redirect_to client_application_representations_path(@client_application)
          must_have_notice 'representations.destroy.messages.success'
          ->{ BaseRepresentation.find(@representation.id) }.must_raise ActiveRecord::RecordNotFound
        end
      end

      describe "when it can't be destroyed" do
        before do
          @representation = create :representation, client_application: @client_application
          @route = create(:homepage_route, representation: @representation, client_application: @client_application)
          @representation.routes << @route
          @representation.save
          UniqueAccessWorker.expects(:perform_async).never
          scoped_delete :destroy, params: { id: @representation }
        end

        it "doesn't destroy the representation and shows an alert indicating the reason" do
          must_redirect_to client_application_representations_path(@client_application)
          must_have_alert 'representations.destroy.messages.error'
          @representation.wont_be :destroyed?
        end
      end
    end

    describe 'GET manage_assets' do
      describe 'when a draft is not available' do
        before do
          @representation = create :cover_representation, client_application: @client_application
          Representation.any_instance.expects(:trigger_draft_creation).once
          scoped_get :manage_assets, params: { id: @representation }
        end

        it 'redirects to the index page with an alert message' do
          must_redirect_to client_application_representations_path(@client_application)
          must_have_alert 'representations.errors.draft_unavailable'
        end
      end

      describe 'when the representation has a draft' do
        before do
          @representation = create :representation, client_application: @client_application
          @representation.draft = @representation.generate_draft
          scoped_get :manage_assets, params: { id: @representation }
        end

        it "redirects to the representation_assets controller for the representation's draft" do
          must_redirect_to client_application_representation_assets_path(@client_application, @representation.draft)
        end
      end
    end

    describe 'POST apply' do
      before do
        @representation = create :representation, client_application: @client_application
        @draft = create :representation_draft, representation: @representation
      end

      it 'calls Purger route invalidation with all associated routes' do
        Purger.expects(:purge_representation).with(@draft).once
        scoped_post :apply, params: { id: @representation }
      end
    end
  end

  protected

  def scoped_request(method, action, params, client_application = nil)
    send method, action,
         params.merge(client_application_id: client_application || @client_application)
  end

  %i(get post patch put delete).each do |meth|
    class_eval <<-RB
      def scoped_#{meth}(action, params: {}, client_application: nil)
        scoped_request #{meth.inspect}, action, params, client_application
      end
    RB
  end
end
