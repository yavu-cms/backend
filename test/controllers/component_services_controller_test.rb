require 'test_helper'

class ComponentServicesControllerTest < ActionController::TestCase
  describe ComponentServicesController do
    let(:permissions) { %w[ component/manage ] }

    before do
      login permissions: permissions
      @controller = ComponentServicesController.new
      @component = create :component
    end

    describe 'Permissions sanity checks' do
      describe 'without permission' do
        let(:permissions) { [] }

        describe 'read component service' do
          describe 'when trying to get index' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :index, component_id: @component }
            end
          end

          describe 'when trying to get show' do
            it 'must be forbidden' do
              component_service = create :component_service, component: @component
              must_be_forbidden -> { get :show, component_id: @component, id: component_service }
            end
          end
        end

        describe 'create a component service' do
          describe 'when trying to get new' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :new, component_id: @component }
            end
          end

          describe 'when trying to post create' do
            it 'must be forbidden' do
              must_be_forbidden -> { it_adds_a ComponentService, options: { component_id: @component } }
            end
          end
        end

        describe 'updating a component service' do
          describe 'when trying to get edit' do
            it 'must be forbidden' do
              component_service = create :component_service, component: @component
              must_be_forbidden -> { get :edit, component_id: @component, id: component_service }
            end
          end

          describe 'when trying to put update' do
            it 'must be forbidden' do
              component_service = create :component_service, component: @component
              must_be_forbidden -> { patch :update, component_id: @component, id: component_service, component_service: { name: 'brand new name' } }
            end
          end
        end

        describe 'delete a component service' do
          describe 'when trying to delete destroy' do
            it 'must be forbidden' do
              component_service = create :component_service, component: @component
              must_be_forbidden -> { it_deletes_a ComponentService, factory: component_service, options: { component_id: @component } }
            end
          end
        end
      end
    end

    describe 'get index' do
      before do
        create_list :component_service, 3, component: @component
      end

      it 'must succeed' do
        get :index, component_id: @component
        must_render :index
        assigns(:services).wont_be_empty
      end
    end

    describe 'get new' do
      it 'must succeed' do
        get :new, component_id: @component
        must_render :new
        assigns(:service).must_be :new_record?
      end
    end

    describe 'get show' do
      before do
        @component_service = create :component_service, component: @component
      end

      it 'must succeed' do
        get :show, id: @component_service, component_id: @component
        must_render :show
        assigns(:service).must_equal @component_service
      end
    end

    describe 'get edit' do
      before do
        @component_service = create :component_service, component: @component
      end

      it 'must succeed' do
        get :edit, id: @component_service, component_id: @component
        must_render :edit
        assigns(:service).wont_be :new_record?
      end
    end

    describe 'post create' do
      before do
        @view = create :view, component: @component
      end

      it 'creates a service' do
        post :create, component_id: @component, component_service: attributes_for(:component_service, component: @component, view_id: @view)
        assigns(:service).wont_be :new_record?
        must_redirect_to component_service_path(@component, assigns(:service))
        must_have_notice 'component/services.new.messages.success'
      end

      describe 'when invalid values are provided' do
        it 'fails and tells that to the user' do
          post :create, component_id: @component, component_service: attributes_for(:component_service, component: @component, view_id: nil)
          must_render :new
          must_display_form_errors
          assigns(:service).must_be :new_record?
        end
      end
    end

    describe 'patch update' do
      before do
        @component_service = create :component_service, component: @component
      end

      it 'updates given value' do
        patch :update, component_id: @component, id: @component_service, component_service: { name: 'brand new name' }
        assigns(:service).wont_be :new_record?
        assigns(:service).name.must_equal 'brand new name'
        must_redirect_to component_service_path(@component, assigns(:service))
        must_have_notice 'component/services.update.messages.success'
      end

      describe 'when invalid values are provided' do
        it 'fails and tells that to the user' do
          patch :update, component_id: @component, id: @component_service, component_service: attributes_for(:component_service, component: @component, view_id: nil)
          must_render :edit
          must_display_form_errors
        end
      end
    end

    describe 'delete destroy' do
      before do
        @component_service = create :component_service, component: @component
      end

      it 'deletes the service' do
        delete :destroy, component_id: @component, id: @component_service
        ->{ @component_service.reload }.must_raise ActiveRecord::RecordNotFound
        must_redirect_to component_services_path(@component)
        must_have_notice 'component/services.destroy.messages.success'
      end

      describe 'when the service cannot be destroyed' do
        before do
          ComponentService.any_instance.stubs(:destroy).returns(false)
        end

        after do
          ComponentService.any_instance.unstub(:destroy)
        end

        it 'tells the user the outcome' do
          delete :destroy, component_id: @component, id: @component_service
          ->{ @component_service.reload }.must_be_silent
          must_redirect_to component_services_path(@component)
          must_have_alert 'component/services.destroy.messages.fail'
        end
      end
    end
  end
end
