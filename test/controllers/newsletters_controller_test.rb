require 'test_helper'

class NewslettersControllerTest < ActionController::TestCase
  describe NewslettersController do
    let(:permissions) { %w[ client_application/read client_application/design_manage ] }

    before do
      login permissions: permissions
      @controller = NewslettersController.new
      @client_application = create :client_application
      pretend_user_workspace_includes @client_application
    end

    describe 'Permissions sanity checks' do
      describe 'design manage and read a new client application' do
        let(:client_application_outside_workspace) { create :client_application }
        let(:newsletter_outside_workspace) { create :newsletter, client_application: client_application_outside_workspace }

        before do
          client_application_outside_workspace.newsletters << newsletter_outside_workspace
        end

        describe 'without permissions' do
          let(:permissions) { [] }
          let(:newsletter) { create :newsletter, client_application: @client_application }

          before do
            @client_application.newsletters << newsletter
          end

          describe 'when trying to get index' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :index, client_application_id: @client_application }
              must_be_forbidden -> { get :index, client_application_id: client_application_outside_workspace }
            end
          end

          describe 'when trying to get show' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :show, client_application_id: @client_application, id: newsletter }
              must_be_forbidden -> { get :show, client_application_id: client_application_outside_workspace, id: newsletter_outside_workspace }
            end
          end

          describe 'when trying to get edit' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :edit, client_application_id: @client_application, id: newsletter }
              must_be_forbidden -> { get :edit, client_application_id: client_application_outside_workspace, id: newsletter_outside_workspace }
            end
          end

          describe 'when trying to patch update' do
            it 'must be forbidden' do
              updated_newsletter = newsletter
              updated_other_newsletter = newsletter_outside_workspace
              updated_newsletter.name = 'a_newsletter'
              updated_other_newsletter.name = 'other_newsletter'
              must_be_forbidden -> { patch :update, client_application_id: @client_application, id: newsletter, newsletter: updated_newsletter.attributes }
              must_be_forbidden -> { patch :update, client_application_id: client_application_outside_workspace, id: newsletter_outside_workspace, newsletter: updated_other_newsletter.attributes }
            end
          end

          describe 'when trying to get new' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :new, client_application_id: @client_application }
              must_be_forbidden -> { get :new, client_application_id: client_application_outside_workspace }
            end
          end

          describe 'when trying to post create' do
            it 'must be forbidden' do
              must_be_forbidden -> { post :create, client_application_id: @client_application, newsletter: attributes_for(:newsletter) }
              must_be_forbidden -> { post :create, client_application_id: client_application_outside_workspace, newsletters: attributes_for(:newsletter) }
            end
          end

          describe 'when trying to delete destroy' do
            it 'must be forbidden' do
              must_be_forbidden -> { delete :destroy, client_application_id: @client_application, id: newsletter }
              must_be_forbidden -> { delete :destroy, client_application_id: client_application_outside_workspace, id: newsletter_outside_workspace }
            end
          end

          describe 'when trying to post enable' do
            it 'must be forbidden' do
              nl = create :newsletter, enabled: false
              other_nl = create :newsletter, enabled: false
              @client_application.newsletters << nl
              client_application_outside_workspace.newsletters << other_nl
              must_be_forbidden -> { post :enable, client_application_id: @client_application, id: nl }
              must_be_forbidden -> { post :enable, client_application_id: client_application_outside_workspace, id: other_nl }
            end
          end

          describe 'when trying to post disable' do
            it 'must be forbidden' do
              must_be_forbidden -> { post :enable, client_application_id: @client_application, id: newsletter }
              must_be_forbidden -> { post :enable, client_application_id: client_application_outside_workspace, id: newsletter_outside_workspace }
            end
          end

          describe 'when trying to get sendmail' do
            it 'must be forbidden' do
              nl = create :newsletter
              other_nl = create :newsletter
              @client_application.newsletters << nl
              client_application_outside_workspace.newsletters << other_nl
              must_be_forbidden -> { get :sendmail, client_application_id: @client_application, id: nl }
              must_be_forbidden -> { get :sendmail, client_application_id: client_application_outside_workspace, id: newsletter_outside_workspace }
            end
          end
        end

        describe 'when have design manage and read client application' do
          describe 'when trying to get index' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :index, client_application_id: client_application_outside_workspace }
            end
          end

          describe 'when trying to get show' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :show, client_application_id: client_application_outside_workspace, id: newsletter_outside_workspace }
            end
          end

          describe 'when trying to get edit' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :edit, client_application_id: client_application_outside_workspace, id: newsletter_outside_workspace }
            end
          end

          describe 'when trying to patch update' do
            it 'must be forbidden' do
              updated_other_newsletter = newsletter_outside_workspace
              updated_other_newsletter.name = 'other_newsletter'
              must_be_forbidden -> { patch :update, client_application_id: client_application_outside_workspace, id: newsletter_outside_workspace, newsletter: updated_other_newsletter.attributes }
            end
          end

          describe 'when trying to get new' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :new, client_application_id: client_application_outside_workspace }
            end
          end

          describe 'when trying to post create' do
            it 'must be forbidden' do
              must_be_forbidden -> { post :create, client_application_id: client_application_outside_workspace, newsletters: attributes_for(:newsletter) }
            end
          end

          describe 'when trying to delete destroy' do
            it 'must be forbidden' do
              must_be_forbidden -> { delete :destroy, client_application_id: client_application_outside_workspace, id: newsletter_outside_workspace }
            end
          end

          describe 'when trying to post enable' do
            it 'must be forbidden' do
              other_nl = create :newsletter, enabled: false
              client_application_outside_workspace.newsletters << other_nl
              must_be_forbidden -> { post :enable, client_application_id: client_application_outside_workspace, id: other_nl }
            end
          end

          describe 'when trying to post disable' do
            it 'must be forbidden' do
              must_be_forbidden -> { post :enable, client_application_id: client_application_outside_workspace, id: newsletter_outside_workspace }
            end
          end

          describe 'when trying to get sendmail' do
            it 'must be forbidden' do
              nl = create :newsletter
              client_application_outside_workspace.newsletters << nl
              must_be_forbidden -> { get :sendmail, client_application_id: client_application_outside_workspace, id: newsletter_outside_workspace }
            end
          end
        end
      end
    end

    describe 'get index' do
      describe 'on a regular request' do
        it 'must succeed' do
          get :index, client_application_id: @client_application
          must_render :index
          assigns(:newsletters).wont_be_nil
        end
      end
    end

    describe 'get new' do
      it 'must succeed' do
        get :new, client_application_id: @client_application
        must_render :new
        assigns(:newsletter).must_be :new_record?
      end
    end

    describe 'post create' do
      describe 'when valid data is posted' do
        it 'adds a newsletter and redirects to the show page' do
          post :create, client_application_id: @client_application, newsletter: attributes_for(:newsletter)
          assigns(:newsletter).wont_be :new_record?
          must_redirect_to client_application_newsletter_path(@client_application, assigns(:newsletter))
          must_have_notice 'newsletters.new.messages.success'
        end
      end

      describe 'when invalid data is posted' do
        it 'shows again the new page and displays the validation errors' do
          nl = build :newsletter
          nl.frequency = nil
          post :create, client_application_id: @client_application, newsletter: nl.attributes
          must_render :new
          assigns(:newsletter).must_be :new_record?
          must_display_form_errors
        end
      end
    end

    describe 'get show' do
      it 'must succeed' do
        nl = create :newsletter
        @client_application.newsletters << nl
        get :show, client_application_id: @client_application, id: nl
        must_render :show
        assigns(:newsletter).wont_be :new_record?
      end
    end

    describe 'get edit' do
      it 'must succeed' do
        nl = create :newsletter
        @client_application.newsletters << nl
        get :edit, client_application_id: @client_application, id: nl
        must_render :edit
        assigns(:newsletter).wont_be :new_record?
      end
    end

    describe 'patch update' do
      describe 'when an invalid set of attributes is submitted' do
        it 'shows again the edit page and displays the validation errors' do
          nl = create :newsletter
          @client_application.newsletters << nl
          nl.frequency = nil
          patch :update, client_application_id: @client_application, id: nl, newsletter: nl.attributes
          must_render :edit
          assigns(:newsletter).wont_be :new_record?
          must_display_form_errors
        end
      end

      describe 'when a valid set of attributes is submitted' do
        it 'updates the newsletter and redirects to show' do
          nl = create :newsletter
          @client_application.newsletters << nl
          patch :update, client_application_id: @client_application, id: nl, newsletter: nl.attributes
          assigns(:newsletter).wont_be :new_record?
          must_redirect_to client_application_newsletter_path(@client_application, assigns(:newsletter))
          must_have_notice 'newsletters.edit.messages.success'
        end
      end
    end

    describe 'delete destroy' do
      it 'deletes the newsletter' do
        nl = create :newsletter
        @client_application.newsletters << nl
        delete :destroy, client_application_id: @client_application, id: nl
        must_redirect_to client_application_newsletters_path(@client_application)
        must_have_notice 'newsletters.destroy.messages.success'
      end
    end

    describe 'post enable' do
      it 'enables the newsletter and redirects to show' do
        nl = create :newsletter, enabled: false
        @client_application.newsletters << nl
        post :enable, client_application_id: @client_application, id: nl
        must_redirect_to client_application_newsletters_path(@client_application)
        must_have_notice 'newsletters.enable.messages.success'
      end
    end

    describe 'post disable' do
      it 'disables the newsletter and redirects to show' do
        nl = create :newsletter
        @client_application.newsletters << nl
        post :disable, client_application_id: @client_application, id: nl
        must_redirect_to client_application_newsletters_path(@client_application)
        must_have_notice 'newsletters.disable.messages.success'
      end
    end

    describe 'get sendmail' do
      let(:nl) { create :newsletter }
      before do
        NewsletterSenderWorker.stubs(:perform_async).with(nl.name)
      end
      after do
        NewsletterSenderWorker.unstub(:perform_async)
      end

      it 'sends the newsletter and redirects to index' do
        @client_application.newsletters << nl
        get :sendmail, client_application_id: @client_application, id: nl
        must_redirect_to client_application_newsletters_path(@client_application)
        must_have_notice 'newsletters.sendmail.messages.success'
      end
    end
  end
end
