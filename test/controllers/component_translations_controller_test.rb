require 'test_helper'

class ComponentTranslationsControllerTest < ActionController::TestCase
  describe ComponentTranslationsController do
    let(:permissions) { %w[ component/manage ] }
    let(:component_translation) { create :component_translation, component: @component }

    before do
      login permissions: permissions
      @controller = ComponentTranslationsController.new
      @component = create :component
    end

        describe 'Permissions sanity checks' do
      describe 'without permission' do
        let(:permissions) { [] }

        describe 'read component views' do
          describe 'when trying to get index' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :index, component_id: @component }
            end
          end

          describe 'when trying to get show' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :show, component_id: @component, id: component_translation }
            end
          end
        end

        describe 'create a component translation' do
          describe 'when trying to get new' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :new, component_id: @component }
            end
          end

          describe 'when trying to post create' do
            it 'must be forbidden' do
              must_be_forbidden -> { it_adds_a ComponentTranslation, options: { component_id: @component } }
            end
          end
        end

        describe 'updating a component translation' do
          describe 'when trying to get edit' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :edit, component_id: @component, id: component_translation }
            end
          end

          describe 'when trying to put update' do
            it 'must be forbidden' do
              modified_values = component_translation.to_hash.merge(a_new_key: 'modified_value', other_key: 'value').to_yaml
              must_be_forbidden -> { put :update, component_id: @component, id: component_translation, translation: modified_values}
            end
          end
        end

        describe 'delete a component translation' do
          describe 'when trying to delete destroy' do
            it 'must be forbidden' do
              must_be_forbidden -> { it_deletes_a ComponentTranslation, factory: component_translation, options: { component_id: @component } }
            end
          end
        end
      end
    end

    describe 'get index' do
      before do
        create_list :component_translation, 3, component: @component
      end

      it 'must succeed' do
        get :index, component_id: @component
        must_render :index
        assigns(:component_translations).wont_be_nil
      end
    end

    describe 'get new' do
      it 'must succeed' do
        get :new, component_id: @component
        must_render :new
        assigns(:component_translation).must_be :new_record?
        assigns(:component_translation).component_id.must_equal @component.id
      end
    end

    describe 'post create' do
      describe 'when succeeds' do
        it 'adds a component_translation' do
          it_adds_a ComponentTranslation, options: { component_id: @component }
          must_redirect_to component_translation_url(@component, assigns(:component_translation))
          assigns(:component_translation).wont_be :new_record?
          must_have_notice 'component/translations.new.messages.success'
        end
      end

      describe 'when fails' do
        it 'shows again the new page and displays the validation errors' do
          # Must raise an error because it has the same locale
          post :create, component_id: @component,
            component_translation: attributes_for(:component_translation, locale: component_translation.locale)

          must_render :new
          must_display_form_errors
          assigns(:component_translation).must_be :new_record?
        end
       end
    end

    describe 'get show' do
      it 'must succeed' do
        get :show, component_id: @component, id: component_translation
        must_render :show
        assigns(:component_translation).wont_be :new_record?
      end
    end

    describe 'get edit' do
      it 'must succeed' do
        get :edit, component_id: @component, id: component_translation
        must_render :edit
        assigns(:component_translation).wont_be :new_record?
      end
    end

    describe 'patch update' do
      describe 'when succeeds' do
        it 'updates the translation and redirects to show' do
          modified_values = component_translation.to_hash.merge(a_new_key: 'modified_value', other_key: 'value').to_yaml

          patch :update, id: component_translation, component_id: @component,
            component_translation: attributes_for(:component_translation, values: modified_values)

          must_redirect_to component_translation_url(@component, assigns(:component_translation))
          assigns(:component_translation).wont_be :new_record?
          assigns(:component_translation).values.must_equal modified_values
          must_have_notice 'component/translations.update.messages.success'
        end
      end

      describe 'when fails' do
        it 'shows again edit page and displays the validation errors' do
          error_yaml = "key: value\n  sub_key: other_value"
          # A component_translation can't be saved when a malformed yaml is set
          patch :update, id: component_translation, component_id: @component,
            component_translation: attributes_for(:component_translation, values: error_yaml)

          must_render :edit
          assigns(:component_translation).wont_be :new_record?
          must_display_form_errors
        end
      end
    end

    describe 'delete destroy' do
      it 'deletes the component translation' do
        it_deletes_a ComponentTranslation, factory: component_translation, options: { component_id: @component }
        must_redirect_to component_translations_url(@component)
        must_have_notice 'component/translations.destroy.messages.success'
      end

      describe 'when the translation cannot be destroyed' do
        before do
          ComponentTranslation.any_instance.stubs(:destroy).returns(false)
        end

        after do
          ComponentTranslation.any_instance.unstub(:destroy)
        end

        it 'redirects with the appropriate alert message' do
          delete :destroy, component_id: @component, id: component_translation
          must_redirect_to component_translations_url(@component)
          must_have_alert 'component/translations.destroy.messages.alert'
          ->{ component_translation.reload }.must_be_silent
        end
      end
    end
  end
end
