require 'test_helper'
require 'mocha/setup'

class BaseRepresentationsViewsControllerTest < ActionController::TestCase
  describe BaseRepresentationsViewsController do
    let(:permissions) { %w[ client_application/read client_application/manage ] }
    
    before do
      login permissions: permissions
      @controller = BaseRepresentationsViewsController.new
      @draft = create :representation_draft
      @representation = create :representation, draft: @draft
      @client_application = @representation.client_application
      pretend_user_workspace_includes @client_application
    end

    describe 'Permissions sanity checks' do
      describe 'read a client application' do
        let(:other_draf) { create :representation_draft }
        let(:representation_outside_workspace) { create :representation, draft: other_draf }
        let(:client_application_outside_workspace) { representation_outside_workspace.client_application }
        let(:view) { create :view_with_placeholders, format: 'application/atom+xml' }

        describe 'without permissions' do
          let(:permissions) { [] }
          
          describe 'when trying to get index' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :index, client_application_id: @client_application, representation_id: @representation }
              must_be_forbidden -> { get :index, client_application_id: client_application_outside_workspace, representation_id: representation_outside_workspace }
            end
          end

          describe 'when trying to get show' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :show, client_application_id: @client_application, representation_id: @representation, id: view }
              must_be_forbidden -> { get :show, client_application_id: client_application_outside_workspace, representation_id: representation_outside_workspace, id: view }
            end
          end

          describe 'when trying to get edit' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :show, client_application_id: @client_application, representation_id: @representation, id: view }
              must_be_forbidden -> { get :show, client_application_id: client_application_outside_workspace, representation_id: representation_outside_workspace, id: view }
            end
          end

          describe 'when trying to put update' do
            it 'must be forbidden' do
              updated_view = view
              updated_view.name = 'view_name'
              must_be_forbidden -> { put :update, client_application_id: @client_application, representation_id: @representation, id: view, view: updated_view.attributes }
              must_be_forbidden -> { put :update, client_application_id: client_application_outside_workspace, representation_id: representation_outside_workspace, id: view, view: updated_view.attributes }
            end
          end

          describe 'when trying to delete destroy' do
            it 'must_be_forbidden' do
              must_be_forbidden -> { delete :destroy, client_application_id: @client_application, representation_id: @representation, id: view }
              must_be_forbidden -> { delete :destroy, client_application_id: client_application_outside_workspace, representation_id: representation_outside_workspace, id: view }
            end
          end

          describe 'when trying to get new' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :new, client_application_id: @client_application, representation_id: @representation }
              must_be_forbidden -> { get :new, client_application_id: client_application_outside_workspace, representation_id: representation_outside_workspace }
            end
          end

          describe 'when trying to post create' do 
            it 'must be forbidden' do
              must_be_forbidden -> { post :create, client_application_id: @client_application, representation_id: @representation, view: attributes_for(:view_with_placeholders, format: 'application/atom+xml') }
              must_be_forbidden -> { post :create, client_application_id: client_application_outside_workspace, representation_id: representation_outside_workspace, view: attributes_for(:view_with_placeholders, format: 'application/atom+xml') }
            end
          end
        end

        describe 'when have client application read permissions' do
          let(:permissions) { %w[ client_application/read client_application/manage ] }        
          describe 'when trying to get index' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :index, client_application_id: client_application_outside_workspace, representation_id: representation_outside_workspace }
            end
          end

          describe 'when trying to get show' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :show, client_application_id: client_application_outside_workspace, representation_id: representation_outside_workspace, id: view }
            end
          end

          describe 'when trying to get edit' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :show, client_application_id: client_application_outside_workspace, representation_id: representation_outside_workspace, id: view }
            end
          end

          describe 'when trying to put update' do
            it 'must be forbidden' do
              updated_view = view
              updated_view.name = 'view_name'
              must_be_forbidden -> { put :update, client_application_id: client_application_outside_workspace, representation_id: representation_outside_workspace, id: view, view: updated_view.attributes }
            end
          end

          describe 'when trying to delete destroy' do
            it 'must_be_forbidden' do
              must_be_forbidden -> { delete :destroy, client_application_id: client_application_outside_workspace, representation_id: representation_outside_workspace, id: view }
            end
          end

          describe 'when trying to get new' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :new, client_application_id: client_application_outside_workspace, representation_id: representation_outside_workspace }
            end
          end

          describe 'when trying to post create' do 
            it 'must be forbidden' do
              must_be_forbidden -> { post :create, client_application_id: client_application_outside_workspace, representation_id: representation_outside_workspace, view: attributes_for(:view_with_placeholders, format: 'application/atom+xml') }
            end
          end
        end
      end
    end

    describe 'get index' do
      describe 'on a regular request without elements' do
        it 'must succeed' do
          get :index, client_application_id: @client_application, representation_id: @representation
          must_render :index
          assigns(:views).wont_be_nil
        end
      end

      describe 'on a regular request whit elements' do
        it 'must succeed' do
          atom = create :view_with_placeholders, format: 'application/atom+xml'
          plain = create :view_with_placeholders, format: 'text/plain'
          rss = create :view_with_placeholders, format: 'application/rss+xml'
          json = create :view_with_placeholders, format: 'application/json'
          get :index, client_application_id: @client_application, representation_id: @representation
          must_render :index
          assigns(:views).count.must_equal 4
        end
      end

      describe 'whit wrong client_application' do
        it 'must redirect' do
          get :index, client_application_id: -1, representation_id: @representation
          must_redirect_to client_applications_path
          must_have_alert 'representations.errors.invalid_client_application_or_representation'
        end
      end

      describe 'whit wrong representation' do
        it 'must redirect' do
          get :index, client_application_id: @client_application, representation_id: -1
          must_redirect_to client_application_representations_path(@client_application)
          must_have_alert 'representations.errors.draft_unavailable'
        end
      end
    end

    describe 'get new' do
      before do
        get :new, client_application_id: @client_application, representation_id: @representation
      end

      it 'must succeed' do
        must_render :new
        assigns(:view).must_be :new_record?
      end
    end

    describe 'get show' do
      let(:view) { create :view_with_placeholders, format: 'application/atom+xml' }

      it 'must_succed' do  
        get :show, client_application_id: @client_application, representation_id: @representation, id: view
        must_render :show
        assigns(:view).wont_be :new_record?
        assigns(:view).must_equal view
      end
    end

    describe 'get edit' do
      let(:view) { create :view_with_placeholders, format: 'application/atom+xml' }

      it 'must succed' do
        get :edit, client_application_id: @client_application, representation_id: @representation, id: view
        must_render :edit
        assigns(:view).wont_be :new_record?
      end
    end

    describe 'destroy' do
      let(:view) { create :view_with_placeholders, format: 'application/atom+xml' }
      let(:associated_view) { create :view_with_placeholders, format: 'application/atom+xml' }

      it 'deletes alternative representation view and redirects to the index page' do
        it_deletes_a View, factory: view, options: { client_application_id: @client_application, representation_id: @representation, id: view.id }
        must_redirect_to client_application_representation_views_path(@client_application, @representation)
        must_have_notice 'representations_views.destroy.messages.success'
      end

      it 'show error if have associations' do
        @draft.views << associated_view
        it_doesnt_delete_a View, factory: associated_view, options: { client_application_id: @client_application, representation_id: @representation, id: associated_view.id }
        must_redirect_to client_application_representation_views_path(@client_application, @representation)
        must_have_alert 'representations_views.destroy.messages.error'
      end
    end

    describe 'post create' do
      let(:view) { create :view_with_placeholders, format: 'application/atom+xml' }

      it 'must redirect to show if succeeds' do
        it_adds_a View, factory: :view_with_placeholders, options: { client_application_id: @client_application, representation_id: @representation, view: { format: view.format } }
        must_redirect_to client_application_representation_view_path(@client_application, @representation, assigns[:view])
        must_have_notice 'representations_views.create.messages.success'
      end
    end

    describe 'post associate' do
      let(:view) { create :view_with_placeholders, format: 'application/atom+xml' }
      let(:associated_view) { create :view_with_placeholders, format: 'application/atom+xml' }

      it 'must redirect to index if succeeds' do
        post :associate, { client_application_id: @client_application, representation_id: @representation, id: view.id }
        must_redirect_to client_application_representation_views_path(@client_application, @representation)
        must_have_notice 'representations_views.associate.messages.success'
      end

      it 'show error if already have view with this format' do
        post :associate, { client_application_id: @client_application, representation_id: @representation, id: associated_view.id }
        post :associate, { client_application_id: @client_application, representation_id: @representation, id: view.id }
        must_redirect_to client_application_representation_views_path(@client_application, @representation)
        must_have_alert 'representations_views.associate.messages.format_in_use'
      end
    end

    describe 'post disassociate' do
      let(:view) { create :view_with_placeholders, format: 'application/atom+xml' }

      it 'must redirect to index if succeeds' do
        post :associate, { client_application_id: @client_application, representation_id: @representation, id: view.id }
        post :disassociate, { client_application_id: @client_application, representation_id: @representation, id: view.id }
        must_redirect_to client_application_representation_views_path(@client_application, @representation)
        must_have_notice 'representations_views.disassociate.messages.success'
      end

      it 'show error if already have view with this format' do
        post :disassociate, { client_application_id: @client_application, representation_id: @representation, id: view.id }
        must_redirect_to client_application_representation_views_path(@client_application, @representation)
        must_have_alert 'representations.errors.view_unavailable'
      end
    end
  end
end
