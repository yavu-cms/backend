require 'test_helper'

class GalleriesControllerTest < ActionController::TestCase
  describe GalleriesController do
    let(:permissions) { %w[ gallery/read gallery/create gallery/update/all gallery/destroy/all ] }

    before do
      login permissions: permissions
      @controller = GalleriesController.new
    end

    describe 'Permissions sanity checks' do
      describe 'create a new gallery' do
        let(:permissions) { %w[ gallery/read ] }

        describe 'when trying to get new' do
          it 'must be forbidden' do
            must_be_forbidden ->{ get :new }
          end
        end

        describe 'when trying to post create' do
          it 'must be forbidden' do
            must_be_forbidden ->{ post :create, gallery: attributes_for(:media_gallery) }
          end
        end
      end

      describe 'updating galleries' do
        describe 'with :unused permission' do
          let(:permissions) { %w[ gallery/update/unused ] }

          describe 'when editing an unused gallery' do
            let(:gallery) { create :media_gallery }

            it 'must succeed' do
              get :edit, id: gallery
              must_render :edit
            end
          end

          describe 'when editing a used gallery' do
            describe 'when it is in use' do
              let(:component_value) { create :component_value_gallery, with_component: true }
              let(:gallery) { component_value.referable }

              it 'must be forbidden' do
                must_be_forbidden ->{ get :edit, id: gallery }
              end
            end
          end
        end

        describe 'with :all permission' do
          let(:permissions) { %w[ gallery/update/all ] }

          describe 'when editing an unused gallery' do
            let(:gallery) { create :media_gallery }

            it 'must succeed' do
              get :edit, id: gallery
              must_render :edit
            end
          end

          describe 'when editing a used gallery' do
            let(:component_value) { create :component_value_gallery, with_component: true }
            let(:gallery) { component_value.referable }
            let(:component) { component_value.component }

            describe 'when the user can update the component' do
              before do
                pretend_user_can(:update, component)
              end

              it 'must succeed' do
                get :edit, id: gallery
                must_render :edit
              end
            end

            describe 'when the user cannot update the component' do
              before do
                pretend_user_cannot(:update, component)
              end

              it 'must be forbidden' do
                must_be_forbidden ->{ get :edit, id: gallery }
              end
            end
          end
        end
      end

      describe 'destroying galleries' do
        describe 'with :unused permission' do
          let(:permissions) { %w[ gallery/destroy/unused ] }

          describe 'when destroying an unused gallery' do
            let(:gallery) { create :media_gallery }

            it 'must succeed' do
              delete :destroy, id: gallery
              must_redirect_to galleries_path, true
            end
          end

          describe 'when destroying a used gallery' do
            let(:component_value) { create :component_value_gallery, with_component: true }
            let(:gallery) { component_value.referable }
            let(:component) { component_value.component }

            it 'must be forbidden' do
              must_be_forbidden ->{ delete :destroy, id: gallery }
            end
          end
        end

        describe 'with :all permission' do
          let(:permissions) { %w[ gallery/destroy/all ] }

          describe 'when destroying an unused gallery' do
            let(:gallery) { create :gallery }

            it 'must succeed' do
              delete :destroy, id: gallery
              must_redirect_to galleries_path, true
            end
          end

          describe 'when destroying a used article' do
            let(:component_value) { create :component_value_gallery, with_component: true }
            let(:gallery) { component_value.referable }
            let(:component) { component_value.component }

            describe 'when the user can update the component' do
              before do
                pretend_user_can(:update, component)
              end

              it 'must succeed' do
                delete :destroy, id: gallery
                must_redirect_to galleries_path, true
              end
            end

            describe 'when the user cannot update the component' do
              before do
                pretend_user_cannot(:update, component)
              end

              it 'must be forbidden' do
                must_be_forbidden ->{ delete :destroy, id: gallery }
              end
            end
          end
        end
      end
    end
    
    
    
    
    
    
    
    
    
    
    
    describe 'get index' do
      describe 'on a regular request' do
        it 'must succeed' do
          get :index
          must_render :index
          assigns(:galleries).wont_be_nil
        end
      end
    end

    describe 'get new' do
      describe 'regular new' do
        it 'must redirect to the index page and tell the user the type is not selected' do
          get :new
          must_redirect_to galleries_path
          must_have_alert 'galleries.new.messages.missing_type'
        end
      end

      describe 'type specific' do
        describe 'new media gallery' do
          it 'succeeds' do
            get :new_media
            must_render :new
            assigns(:gallery).must_be :new_record?
            assigns(:gallery).must_be_instance_of MediaGallery
          end
        end
      end
    end

    describe 'post create' do
      describe 'when valid attributes are submitted' do
        it 'must create the gallery and redirect to show' do
          post :create, gallery: { title: 'a title' }
          must_redirect_to gallery_path(assigns(:gallery))
        end
      end

      describe 'when invalid attributes are submitted' do
        it 'shows again the new form and displays errors' do
          post :create, gallery: { title: '', type: 'MediaGallery' }
          must_render :new
          assigns(:gallery).must_be :new_record?
          must_display_form_errors
        end
      end
    end

    describe 'get show' do
      it 'succeeds' do
        gallery = MediaGallery.create(title: 'title')
        get :show, id: gallery
        must_render :show
        assigns(:gallery).must_equal gallery
      end
    end

    describe 'get edit' do
      it 'succeeds' do
        gallery = MediaGallery.create(title: 'title')
        get :edit, id: gallery
        must_render :edit
        assigns(:gallery).must_equal gallery
      end
    end

    describe "get search" do
      describe 'when params[:id] is present' do
        it 'returns a JSON with matching gallery' do
          gallery = Gallery.create(title: 'gallery one')
          get :search, id: gallery
          results = JSON.parse(response.body)
          results.wont_be_empty
          results.first['text'].must_equal gallery.title
          must_succeed
        end
      end

      describe 'when params[:id] is not present' do
        it "returns an empty JSON" do
          get :search
          results = JSON.parse(response.body)
          results.first.must_be_nil
          must_succeed
        end
      end

      describe "when id doesn't exist" do
        it 'should an empty JSON' do
          get :search, id: nil
          results = JSON.parse(response.body)
          results.first.must_be_nil
          must_succeed
        end
      end

      describe "when a query is present" do
        before do
          @gallery = MediaGallery.create(title: 'cool gallery')
        end

        it "should return matching galleries" do
          get :search, q: 'ool gall'
          results = JSON.parse(response.body)
          results.wont_be_empty
          results.first['text'].must_equal @gallery.title
        end
      end
    end

    describe 'patch update' do
      before do
        @gallery = MediaGallery.create(title: 'old_title')
      end

      describe 'when valid information is sent' do
        before do
          patch :update, id: @gallery, gallery: {title: 'new title'}
        end

        it 'updates the medium and redirects to show' do
          @gallery.reload
          @gallery.title.must_equal 'new title'
          must_redirect_to gallery_path(assigns(:gallery))
        end
      end

      describe 'when invalid information is sent' do
        before do
          patch :update, id: @gallery, gallery: {title: ''}
        end

        it 'fails to update, shows the errors and renders the edit view' do
          must_render :edit
          must_display_form_errors
          assigns(:gallery).must_equal @gallery
        end
      end
    end

    describe 'delete destroy' do
      it 'succeeds' do
        gallery = create :media_gallery
        delete :destroy, id: gallery
        ->{ MediaGallery.find(gallery.id) }.must_raise ActiveRecord::RecordNotFound
      end
    end
  end
end
