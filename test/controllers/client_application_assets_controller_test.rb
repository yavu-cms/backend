require 'test_helper'

class ClientApplicationAssetsControllerTest < ActionController::TestCase
  describe ClientApplicationAssetsController do
    let(:permissions) { %w[ client_application/read client_application/design_manage ] }

    before do
      @client_application = create :client_application
      login permissions: permissions
      pretend_user_workspace_includes @client_application
      @controller = ClientApplicationAssetsController.new
      ClientApplicationAssetUploader.root = Rails.root.join(*%w{tmp tests client_application_assets_controller})
    end

    after do
      FileUtils.remove_dir(ClientApplicationAssetUploader.root, true)
    end

    describe 'Permissions sanity checks' do
      describe 'design manage a new client application' do
        let(:client_application_outside_workspace) { create :client_application }
        let(:other_asset) { create :client_application_asset_editable, client_application: client_application_outside_workspace }

        describe 'without permissions' do
          let(:permissions) { [] }
          let(:asset) { create :client_application_asset_editable, client_application: @client_application }

          describe 'when trying to get index' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :index, client_application_id: @client_application }
              must_be_forbidden -> { get :index, client_application_id: client_application_outside_workspace }
            end
          end

          describe 'when trying to get show' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :show, client_application_id: @client_application, id: asset }
              must_be_forbidden -> { get :show, client_application_id: client_application_outside_workspace, id: other_asset }
            end
          end

          describe 'when trying to get edit' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :edit, client_application_id: @client_application, id: asset }
              must_be_forbidden -> { get :edit, client_application_id: client_application_outside_workspace, id: other_asset }
            end
          end

          describe 'when trying to put update' do
            it 'must be forbidden' do
              update_asset = asset
              update_asset.prefix_path = '/sarasa'
              update_other_asset = other_asset
              update_other_asset.prefix_path = '/other_sarasa'
              must_be_forbidden -> { put :update, client_application_id: @client_application, id: asset, asset: update_asset.attributes }
              must_be_forbidden -> { put :update, client_application_id: client_application_outside_workspace, id: other_asset, asset: update_other_asset.attributes }
            end
          end

          describe 'when trying to delete destroy' do
            it 'must_be_forbidden' do
              must_be_forbidden -> { delete :destroy, client_application_id: @client_application, id: asset }
              must_be_forbidden -> { delete :destroy, client_application_id: client_application_outside_workspace, id: other_asset }
            end  
          end

          describe 'when trying to get new' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :new, client_application_id: @client_application }
              must_be_forbidden -> { get :new, client_application_id: client_application_outside_workspace }
            end
          end

          describe 'when trying to post create' do
            let(:go_back_url) { 'some_url' }
            
            before do
              @session_hash = { 'client_application_asset.list_path' => go_back_url }
            end
            
            it 'must be forbidden' do
              must_be_forbidden -> { it_adds_a ClientApplicationAsset, options: { client_application_id: @client_application}, session: @session_hash }
              must_be_forbidden -> { it_adds_a ClientApplicationAsset, options: { client_application_id: client_application_outside_workspace}, session: @session_hash }
            end
          end

          describe 'when trying to get new empty' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :new_empty, client_application_id: @client_application }
              must_be_forbidden -> { get :new_empty, client_application_id: client_application_outside_workspace }
            end
          end

          describe 'when trying to post create empty' do
            let(:go_back_url) { 'some_url' }
            
            before do
              @session_hash = { 'client_application_asset.list_path' => go_back_url }
            end
            
            it 'must be forbidden' do
              must_be_forbidden -> { post :create_empty, client_application_id: @client_application, session: @session_hash }
              must_be_forbidden -> { post :create_empty, client_application_id: client_application_outside_workspace, session: @session_hash }
            end
          end

          describe 'when trying to get rename' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :rename, client_application_id: @client_application, id: asset }
              must_be_forbidden -> { get :rename, client_application_id: client_application_outside_workspace, id: other_asset }
            end
          end

          describe 'when trying to post update name' do
            it 'must be forbidden' do
              updated_asset = asset
              updated_asset.file = "updated"
              updated_other_asset = other_asset
              updated_other_asset.file = "updated_other"
              must_be_forbidden -> { put :update_name, client_application_id: @client_application, id: asset, asset: updated_asset }
              must_be_forbidden -> { put :update_name, client_application_id: client_application_outside_workspace, id: other_asset, asset: updated_other_asset }
            end
          end

          describe 'when trying to get edit content' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :edit_content, client_application_id: @client_application, id: asset }
              must_be_forbidden -> { get :edit_content, client_application_id: client_application_outside_workspace, id: other_asset }
            end
          end

          describe 'when trying to patch update content' do
            it 'must be forbidden' do
              must_be_forbidden -> { put :update, client_application_id: @client_application, id: asset, client_application_asset: { content: 'sarasa' } }
              must_be_forbidden -> { put :update, client_application_id: client_application_outside_workspace, id: other_asset, client_application_asset: { content: 'other_sarasa' } }
            end
          end

          describe 'when trying to get dissasociate representations' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :dissasociate_representations, client_application_id: @client_application, id: asset }
              must_be_forbidden -> { get :dissasociate_representations, client_application_id: client_application_outside_workspace, id: other_asset }
            end
          end

          describe 'when trying to post toggle as' do
            it 'must be forbidden' do
              must_be_forbidden -> { post :toggle_as, client_application_id: @client_application, id: asset, toggable: :favicon }
              must_be_forbidden -> { post :toggle_as, client_application_id: client_application_outside_workspace, id: other_asset, toggable: :favicon }
            end
          end
        end

        describe 'when have client_application/read' do
          describe 'when trying to get show with client application outside user workspace' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :show, client_application_id: client_application_outside_workspace, id: other_asset }
            end
          end

          describe 'when trying to get edit with client application outside user workspace' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :edit, client_application_id: client_application_outside_workspace, id: other_asset }
            end
          end

          describe 'when trying to put update with client application outside user workspace' do
            it 'must be forbidden' do
              update_other_asset = other_asset
              update_other_asset.prefix_path = '/other_sarasa'
              must_be_forbidden -> { put :update, client_application_id: client_application_outside_workspace, id: other_asset, asset: update_other_asset.attributes }
            end
          end

          describe 'when trying to delete destroy with client application outside user workspace' do
            it 'must_be_forbidden' do
              must_be_forbidden -> { delete :destroy, client_application_id: client_application_outside_workspace, id: other_asset }
            end  
          end

          describe 'when trying to get new with client application outside user workspace' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :new, client_application_id: client_application_outside_workspace }
            end
          end

          describe 'when trying to post create with client application outside user workspace' do
            let(:go_back_url) { 'some_url' }
            
            before do
              @session_hash = { 'client_application_asset.list_path' => go_back_url }
            end
            
            it 'must be forbidden' do
              must_be_forbidden -> { it_adds_a ClientApplicationAsset, options: { client_application_id: client_application_outside_workspace}, session: @session_hash }
            end
          end

          describe 'when trying to get new empty with client application outside user workspace' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :new_empty, client_application_id: client_application_outside_workspace }
            end
          end

          describe 'when trying to post create empty with client application outside user workspace' do
            let(:go_back_url) { 'some_url' }
            
            before do
              @session_hash = { 'client_application_asset.list_path' => go_back_url }
            end
            
            it 'must be forbidden' do
              must_be_forbidden -> { post :create_empty, client_application_id: client_application_outside_workspace, session: @session_hash }
            end
          end

          describe 'when trying to get rename with client application outside user workspace' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :rename, client_application_id: client_application_outside_workspace, id: other_asset }
            end
          end

          describe 'when trying to post update name with client application outside user workspace' do
            it 'must be forbidden' do
              updated_other_asset = other_asset
              updated_other_asset.file = "updated_other"
              must_be_forbidden -> { put :update_name, client_application_id: client_application_outside_workspace, id: other_asset, asset: updated_other_asset }
            end
          end

          describe 'when trying to get edit content with client application outside user workspace' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :edit_content, client_application_id: client_application_outside_workspace, id: other_asset }
            end
          end

          describe 'when trying to patch update content with client application outside user workspace' do
            it 'must be forbidden' do
              must_be_forbidden -> { put :update, client_application_id: client_application_outside_workspace, id: other_asset, client_application_asset: { content: 'other_sarasa' } }
            end
          end

          describe 'when trying to get dissasociate representations with client application outside user workspace' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :dissasociate_representations, client_application_id: client_application_outside_workspace, id: other_asset }
            end
          end

          describe 'when trying to post toggle as with client application outside user workspace' do
            it 'must be forbidden' do
              must_be_forbidden -> { post :toggle_as, client_application_id: client_application_outside_workspace, id: other_asset, toggable: :favicon }
            end
          end
        end
      end
    end

    describe 'get index' do
      it 'must succeed' do
        # An editable asset
        client_application_asset_editable = create :client_application_asset_editable,
          client_application: @client_application
        # A not editable asset
        client_application_asset_not_editable = create :client_application_asset_not_editable,
          client_application: client_application_asset_editable.client_application
        # Make the request: we expect to get two rows
        get :index, client_application_id: @client_application

        must_render :index
        assigns(:application_assets).count.must_equal 2

        assert_select "#action-dropdown-#{client_application_asset_editable.id} a[href=?]",
          edit_content_client_application_asset_path(client_application_asset_editable.client_application,
                                                     client_application_asset_editable)

        assert_select "#action-dropdown-#{client_application_asset_not_editable.id} a[href=?]",
          edit_content_client_application_asset_path(client_application_asset_not_editable.client_application,
                                                     client_application_asset_not_editable), false
      end
    end

    describe 'get new' do
      before do
        get :new, client_application_id: @client_application
      end

      it 'must succeed' do
        must_render :new
        assert_select '#new_client_application_asset[action=?]',
          client_application_assets_path(@client_application)
        assigns(:asset).must_be :new_record?
      end

      # This view is shared with other module so we must check go back action
      it 'must set go back action to client_application_assets_path' do
        assert_select '.context-actions a[href=?]',
          client_application_assets_path(@client_application), t_action(:client_application_assets, :back)
      end

      it 'must set session[client_application_asset.*_path]' do
        session['client_application_asset.list_path'].must_equal client_application_assets_path(assigns(:asset).client_application)
        session['client_application_asset.edit_path'].must_be_nil
        session['client_application_asset.edit_content_path'].must_be_nil
        session['client_application_asset.rename_path'].must_be_nil
        session['client_application_asset.show_path'].must_be_nil
      end
    end

    describe 'get new_empty' do
      before do
        get :new_empty, client_application_id: @client_application
      end

      it 'must succeed' do
        must_render :new_empty
        assigns(:asset).must_be :new_record?
      end

      it 'must set go back action to client_application_assets_path' do
        assert_select '.context-actions a[href=?]',
          client_application_assets_path(@client_application), t_action(:client_application_assets, :back)
      end

      it 'must set session[client_application_asset.*_path]' do
        session['client_application_asset.list_path'].must_equal client_application_assets_path(assigns(:asset).client_application)
        session['client_application_asset.edit_path'].must_be_nil
        session['client_application_asset.edit_content_path'].must_be_nil
        session['client_application_asset.rename_path'].must_be_nil
        session['client_application_asset.show_path'].must_be_nil
      end
    end

    describe 'post create' do
      let(:go_back_url) { 'some_url' }
      before do
        @session_hash = {
              'client_application_asset.list_path' => go_back_url
        }
      end


      describe 'when succeeds' do
        # This action must redirect to list
        it "adds a client application asset " do
          it_adds_a ClientApplicationAsset,
            options: { client_application_id: @client_application},
            session: @session_hash
          assigns(:asset).wont_be :new_record?
          must_redirect_to go_back_url
          must_have_notice 'client_application_assets.new.messages.success'
        end

        it "must redirect to client_application_assets_path when session[client_application_asset.list_path] is nil " do
          it_adds_a ClientApplicationAsset,
            options: { client_application_id: @client_application}
          assigns(:asset).wont_be :new_record?
          must_redirect_to client_application_assets_path(@client_application)
        end
      end

      describe 'when fails' do
        it "shows again new page and displays the validation errors" do
          asset = create :client_application_asset, client_application: @client_application
          # This is a broken request: it creates a client_application_asset that  is already created
          post :create,
            {
              client_application_asset: attributes_for(:client_application_asset),
              client_application_id: asset.client_application
            }, @session_hash
          # Must raise an error because it just exists
          # We also are setting session[client_application_asset.list_path] because this is not an integration test
          must_render :new
          assigns(:asset).must_be :new_record?
          must_display_form_errors
          # This view is shared with other module so we must check go back action is setted as specified by session
          assert_select '.context-actions a[href=?]', go_back_url, t_action(:client_application_assets, :back)
        end


        it "when session client_application_asset.list_path is nil go_back must link_to to client_application_assets_path" do
          create :client_application_asset, client_application: @client_application
          # This is a broken request: it creates a client_application_asset that  is already created
          post :create,
            {
              client_application_asset: attributes_for(:client_application_asset),
              client_application_id: @client_application
            }
          must_render :new
          assert_select '.context-actions a[href=?]', client_application_assets_path(@client_application), t_action(:client_application_assets, :back)
        end
      end
    end

    describe 'post create_empty' do
      let(:go_back_url) { 'some_url' }
      before do
        @session_hash = {
              'client_application_asset.list_path' => go_back_url
        }
      end

      describe 'when succeeds' do
        # This action must redirect to list
        it "adds a client application asset " do
          post :create_empty, {
            client_application_asset: { file: 'some_file_name', type: 'js'},
            client_application_id: @client_application
            }, @session_hash
          assigns(:asset).wont_be :new_record?
          must_redirect_to go_back_url
          must_have_notice 'client_application_assets.new.messages.success'
        end
      end

      describe 'when fails' do
        it "shows again new_empty page and displays the validation errors" do
          create :client_application_asset_stylesheet, client_application: @client_application
          post :create_empty, {
            client_application_asset: { type: 'css', file: 'stylesheet' },
            client_application_id: @client_application
            }, @session_hash
          # Must raise an error because it just exists
          # We also are setting session[client_application_asset.list_path] because this is not an integration test
          must_display_form_errors
          must_render :new_empty
          # This view is shared with other module so we must check go back action is setted as specified by session
          assert_select '.context-actions a[href=?]', go_back_url, t_action(:client_application_assets, :back)
        end
      end
    end

    describe 'get show' do
      it 'must succeed when not editable? and wont add action to edit content' do
        asset = create :client_application_asset_not_editable, client_application: @client_application
        get :show, client_application_id: asset.client_application, id: asset
        must_render :show
        assigns(:asset).wont_be :new_record?
        assert_select ".context-actions li a[href=?]",
          edit_content_client_application_asset_path(@client_application, asset), false
      end

      it 'must succeed when editable? and add action to edit content' do
        asset = create :client_application_asset_editable, client_application: @client_application
        get :show, client_application_id: asset.client_application, id: asset
        must_render :show
        assigns(:asset).wont_be :new_record?
        assert_select ".context-actions li a[href=?]",
          edit_content_client_application_asset_path(@client_application, asset)
      end

      # This view is shared with other module so we must check go back action
      it 'must set go back action to client_application_assets_path' do
        asset = create :client_application_asset, client_application: @client_application
        get :show, client_application_id: asset.client_application, id: asset
        assert_select '.context-actions a[href=?]',
          client_application_assets_path(@client_application), t_action(:client_application_assets, :back)
      end

      it 'must set session[client_application_asset.*_path]' do
        asset = create :client_application_asset, client_application: @client_application
        get :show, client_application_id: asset.client_application, id: asset
        session['client_application_asset.list_path'].must_equal client_application_assets_path(assigns(:asset).client_application)
        session['client_application_asset.edit_path'].must_equal edit_client_application_asset_path(assigns(:asset).client_application, assigns(:asset))
        session['client_application_asset.edit_content_path'].must_equal edit_content_client_application_asset_path(assigns(:asset).client_application, assigns(:asset))
        session['client_application_asset.rename_path'].must_equal rename_client_application_asset_path(assigns(:asset).client_application, assigns(:asset))
        session['client_application_asset.show_path'].must_equal client_application_asset_path(assigns(:asset).client_application, assigns(:asset))
      end

    end

    describe 'get edit' do
      before do
        asset = create :client_application_asset, client_application: @client_application
        get :edit, client_application_id: asset.client_application, id: asset
      end

      it 'must succeed' do
        must_render :edit
        assigns(:asset).wont_be :new_record?
        assert_select "#edit_client_application_asset_#{assigns(:asset).id}[action=?]",
          client_application_asset_path(assigns(:asset).client_application, assigns(:asset))
      end

      # This view is shared with other module so we must check go back action
      it 'must set go back action to client_application_assets_path' do
        assert_select '.context-actions a[href=?]',
          client_application_assets_path(@client_application), t_action(:client_application_assets, :back)
      end

      it 'must set session[client_application_asset.*_path]' do
        session['client_application_asset.list_path'].must_equal client_application_assets_path(assigns(:asset).client_application)
        session['client_application_asset.edit_path'].must_equal edit_client_application_asset_path(assigns(:asset).client_application, assigns(:asset))
        session['client_application_asset.edit_content_path'].must_equal edit_content_client_application_asset_path(assigns(:asset).client_application, assigns(:asset))
        session['client_application_asset.rename_path'].must_equal rename_client_application_asset_path(assigns(:asset).client_application, assigns(:asset))
        session['client_application_asset.show_path'].must_equal client_application_asset_path(assigns(:asset).client_application, assigns(:asset))
      end
    end

    describe 'get rename' do
      before do
        @asset = create :client_application_asset, client_application: @client_application
      end

      it 'must succeed' do
        get :rename, client_application_id: @asset.client_application, id: @asset
        must_render :rename
        assigns(:asset).wont_be :new_record?
      end
    end

    describe 'post toggle_as' do
      describe 'favicon' do
        describe 'set as favicon' do
          describe 'when image asset is given' do
            before do
              @asset = create :client_application_asset, client_application: @client_application
            end

            it 'must succeed' do
              post :toggle_as, client_application_id: @client_application, id: @asset, toggable: :favicon
              assigns(:client_application).favicon.must_equal @asset
              must_redirect_to client_application_assets_path(@client_application)
              must_have_notice 'client_application_assets.set_as_favicon.messages.success'
            end
          end

          describe 'when other asset type is given' do
            before do
              @asset = create :client_application_asset_stylesheet, client_application: @client_application
            end

            it 'must fail' do
              post :toggle_as, client_application_id: @client_application, id: @asset, toggable: :favicon
              assigns(:client_application).favicon.must_be_nil
              must_redirect_to client_application_assets_path(@client_application)
              must_have_alert 'client_application_assets.set_as_favicon.messages.error'
            end
          end

          describe 'when asset is from another client application' do
            before do
              @another_client_app = create :client_application
              @asset_2 = create :client_application_asset, client_application: @another_client_app
            end

            it 'should raise an error' do
              -> { post :toggle_as, client_application_id: @client_application, id: @asset_2, toggable: :favicon }.must_raise ActiveRecord::RecordNotFound
            end
          end
        end

        describe 'unset as favicon' do
          before do
            @asset = create :client_application_asset, client_application: @client_application
            @client_application.favicon = @asset
            @client_application.save!
          end

          it 'must succeed' do
            post :toggle_as, client_application_id: @client_application, id: @asset, toggable: :favicon
            assigns(:client_application).favicon.must_be_nil
            must_redirect_to client_application_assets_path(@client_application)
            must_have_notice 'client_application_assets.unset_as_favicon.messages.success'
          end
        end
      end

      describe 'logo' do
        describe 'set as logo' do
          describe 'when image asset is given' do
            before do
              @asset = create :client_application_asset, client_application: @client_application
            end

            it 'must succeed' do
              post :toggle_as, client_application_id: @client_application, id: @asset, toggable: :logo
              assigns(:client_application).logo.must_equal @asset
              must_redirect_to client_application_assets_path(@client_application)
              must_have_notice 'client_application_assets.set_as_logo.messages.success'
            end
          end

          describe 'when other asset type is given' do
            before do
              @asset = create :client_application_asset_stylesheet, client_application: @client_application
            end

            it 'must fail' do
              post :toggle_as, client_application_id: @client_application, id: @asset, toggable: :logo
              assigns(:client_application).logo.must_be_nil
              must_redirect_to client_application_assets_path(@client_application)
              must_have_alert 'client_application_assets.set_as_logo.messages.error'
            end
          end

          describe 'when asset is from another client application' do
            before do
              @another_client_app = create :client_application
              @asset_2 = create :client_application_asset, client_application: @another_client_app
            end

            it 'should raise an error' do
              -> { post :toggle_as, client_application_id: @client_application, id: @asset_2, toggable: :logo }.must_raise ActiveRecord::RecordNotFound
            end
          end
        end

        describe 'unset as logo' do
          before do
            @asset = create :client_application_asset, client_application: @client_application
            @client_application.logo = @asset
            @client_application.save!
          end

          it 'must succeed' do
            post :toggle_as, client_application_id: @client_application, id: @asset, toggable: :logo
            assigns(:client_application).logo.must_be_nil
            must_redirect_to client_application_assets_path(@client_application)
            must_have_notice 'client_application_assets.unset_as_logo.messages.success'
          end
        end
      end
    end

    describe 'patch update name' do
      let(:go_back_url) { 'some_url' }
      let(:show_url) { 'other_url' }
      before do
        @session_hash = {
              'client_application_asset.list_path' => go_back_url,
              'client_application_asset.show_path' => show_url
        }
        @asset = create :client_application_asset, client_application: @client_application
      end

      it 'redirects to component assets' do
        patch :update_name,
          {
            id: @asset,
            client_application_id: @asset.client_application,
            client_application_asset: { file: 'new_name' }
          }, @session_hash

        must_redirect_to show_url
      end
    end

    describe "patch update" do
      let(:go_back_url) { 'some_url' }
      let(:show_url) { 'other_url' }
      before do
        @session_hash = {
              'client_application_asset.list_path' => go_back_url,
              'client_application_asset.show_path' => show_url
        }
      end

      describe 'when succeeds' do
        it 'updates the asset and redirects to show' do
          asset = create :client_application_asset, client_application: @client_application
          other_file = upload_test_file('image_2.jpg')
          patch :update,
            {
              id: asset,
              client_application_asset: attributes_for(:client_application_asset, file: other_file),
              client_application_id: asset.client_application
            }, @session_hash
          assigns(:asset).wont_be :new_record?
          must_redirect_to show_url
          assigns(:asset).file_identifier.must_equal other_file.original_filename
          must_have_notice 'client_application_assets.edit.messages.success'
        end

        it 'must redirect to client_application_asset_path when client_application_asset.show_path is nil' do
          asset = create :client_application_asset, client_application: @client_application
          other_file = upload_test_file('image_2.jpg')
          patch :update,
            id: asset,
            client_application_asset: attributes_for(:client_application_asset, file: other_file),
            client_application_id: asset.client_application
          assigns(:asset).wont_be :new_record?
          must_redirect_to client_application_asset_path(asset.client_application, asset)
        end
      end

      describe 'when fails' do
        it "shows again edit page and displays the validation errors" do
          asset = create :client_application_asset, client_application: @client_application
          file_exists = upload_test_file('image_2.jpg')
          create(:client_application_asset, client_application: asset.client_application, file: file_exists)
          # A component_asset can't be saved when duplicates other entry
          patch :update,
            {
              id: asset,
              client_application_asset: attributes_for(:client_application_asset, file: file_exists),
              client_application_id: asset.client_application
            }, @session_hash
          must_render :edit
          assigns(:asset).wont_be :new_record?
          must_display_form_errors
          # This view is shared with other module so we must check go back action is setted as specified by session
          assert_select '.context-actions a[href=?]', go_back_url, t_action(:client_application_assets, :back)
        end

        it "when session[client_application_asset.list_path] is nil go_back must link_to to client_application_assets_path" do
          asset = create :client_application_asset, client_application: @client_application
          file_exists = upload_test_file('image_2.jpg')
          create(:client_application_asset, client_application: asset.client_application, file: file_exists)
          # A component_asset can't be saved when duplicates other entry
          patch :update,
            id: asset ,
            client_application_asset: attributes_for(:client_application_asset, file: file_exists),
            client_application_id: asset.client_application
          assert_select '.context-actions a[href=?]', client_application_assets_path(asset.client_application), t_action(:client_application_assets, :back)
        end
      end
    end

    describe 'get edit content' do
      describe 'when asset is editable' do
        before do
          asset_editable = create :client_application_asset_editable, client_application: @client_application
          get :edit_content, client_application_id: asset_editable.client_application, id: asset_editable
        end

        it 'must succeed' do
          must_render :edit_content
          asset = assigns(:asset)
          asset.wont_be :new_record?
          assert_select "#edit_client_application_asset_#{asset.id}[action=?]",
            update_content_client_application_asset_path(asset.client_application, asset)
        end
        #
        # This view is shared with other module so we must check go back action
        it 'must set go back action to client_application_assets_path' do
          assert_select '.context-actions a[href=?]',
            client_application_assets_path(@client_application), t_action(:client_application_assets, :back)
        end

        it 'must set session[client_application_asset.*_path]' do
          session['client_application_asset.list_path'].must_equal client_application_assets_path(assigns(:asset).client_application)
          session['client_application_asset.edit_path'].must_equal edit_client_application_asset_path(assigns(:asset).client_application, assigns(:asset))
          session['client_application_asset.edit_content_path'].must_equal edit_content_client_application_asset_path(assigns(:asset).client_application, assigns(:asset))
          session['client_application_asset.rename_path'].must_equal rename_client_application_asset_path(assigns(:asset).client_application, assigns(:asset))
          session['client_application_asset.show_path'].must_equal client_application_asset_path(assigns(:asset).client_application, assigns(:asset))
        end
      end

      it 'must fail for not editable?' do
        asset_not_editable = create :client_application_asset_not_editable, client_application: @client_application

        get :edit_content, client_application_id: asset_not_editable.client_application, id: asset_not_editable
        must_redirect_to client_application_asset_path(asset_not_editable.client_application, asset_not_editable)
        assigns(:asset).wont_be :new_record?
        must_have_alert 'client_application_assets.edit_content.messages.error'
      end
    end

    describe "patch update content" do

      let(:go_back_url) { 'some_url' }
      let(:show_url) { 'other_url' }
      before do
        @session_hash = {
              'client_application_asset.list_path' => go_back_url,
              'client_application_asset.show_path' => show_url
        }
      end

      describe 'when succeeds' do
        it 'updates content of component asset and redirects to show' do
          asset_editable = create :client_application_asset_editable, client_application: @client_application
          content = 'new content'
          patch :update_content,
            {
              id: asset_editable,
              client_application_asset: attributes_for(:client_application_asset_editable, content: content),
              client_application_id: asset_editable.client_application
            }, @session_hash

          must_redirect_to show_url

          assigns(:asset).wont_be :new_record?

          IO.read(assigns(:asset).file.current_path).must_equal content

          must_have_notice 'client_application_assets.edit_content.messages.success'
        end

        it 'must redirect to client_application_asset_path when client_application_asset.show_path is nil' do
          asset = create :client_application_asset_editable, client_application: @client_application
          patch :update_content,
            id: asset,
            client_application_asset: attributes_for(:client_application_asset_editable, content: 'some'),
            client_application_id: asset.client_application
          assigns(:asset).wont_be :new_record?
          must_redirect_to client_application_asset_path(asset.client_application, asset)
        end
      end
    end

    describe 'delete destroy' do
      it 'succeds' do
        asset = create :client_application_asset, client_application: @client_application
        it_deletes_a ClientApplicationAsset, factory: asset, options: { client_application_id: asset.client_application }
        must_redirect_to client_application_assets_path(asset.client_application)
        must_have_notice 'client_application_assets.destroy.messages.success'
      end

      it 'fails' do
        representation = create :representation, client_application: @client_application
        asset = create :client_application_asset, client_application: @client_application
        representation.assets << asset
        it_doesnt_delete_a ClientApplicationAsset, factory: asset, options: { client_application_id: asset.client_application }
        must_redirect_to client_application_assets_path(asset.client_application)
        must_have_alert 'client_application_assets.destroy.messages.has_representations'
      end
    end


    describe 'get dissasociate_representations' do
      it 'succeeds when it has representations associated' do
        representation = create :representation, client_application: @client_application
        asset = create :client_application_asset, client_application: @client_application
        representation.assets << asset
        representation.assets.wont_be_empty
        get :dissasociate_representations, client_application_id: asset.client_application, id: asset
        must_redirect_to client_application_assets_path(asset.client_application)
        must_have_notice 'client_application_assets.dissasociate_representations.messages.success'
        representation.reload
        representation.assets.must_be_empty
      end

      it 'succeeds when it has no representation associated' do
        asset = create :client_application_asset, client_application: @client_application
        asset.representations.must_be_empty
        get :dissasociate_representations, client_application_id: asset.client_application, id: asset
        must_redirect_to client_application_assets_path(asset.client_application)
        must_have_notice 'client_application_assets.dissasociate_representations.messages.success'
      end
    end
  end
end
