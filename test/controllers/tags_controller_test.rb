require 'test_helper'

class TagsControllerTest < ActionController::TestCase
  describe TagsController do
    let(:permissions) { %w[ tag/read tag/create tag/update/all tag/destroy/all ] }

    before do
      login permissions: permissions
      @controller = TagsController.new
    end

    describe 'Permissions sanity checks' do
      describe 'create a new tag' do
        let(:permissions) { %w[ tag/read ] }

        describe 'when trying to get new' do
          it 'must be forbidden' do
            must_be_forbidden ->{ get :new }
          end
        end

        describe 'when trying to post create' do
          it 'must be forbidden' do
            must_be_forbidden ->{ post :create, tag: attributes_for(:tag) }
          end
        end
      end

      describe 'updating tags' do
        describe 'with :unused permission' do
          let(:permissions) { %w[ tag/update/unused ] }

          describe 'when editing an unused tag' do
            let(:tag) { create :tag }

            it 'must succeed' do
              get :edit, id: tag
              must_render :edit
            end
          end

          describe 'when editing a used tag' do
            describe 'when it is used by an article' do
              let(:tag) { create :tag, articles: create_list(:article, 1) }

              it 'must be forbidden' do
                must_be_forbidden ->{ get :edit, id: tag }
              end

              it 'must be forbidden to blacklist' do
                must_be_forbidden ->{ post :blacklist, id: tag }
              end
            end

            describe 'when it is used by a medium' do
              let(:tag) { create :tag, media: create_list(:image_medium, 1) }

              it 'must be forbidden' do
                must_be_forbidden ->{ get :edit, id: tag }
              end
            end
          end
        end

        describe 'with :all permission' do
          let(:permissions) { %w[ tag/update/all ] }

          describe 'when editing an unused tag' do
            let(:tag) { create :tag }

            it 'must succeed' do
              get :edit, id: tag
              must_render :edit
            end
          end

          describe 'when editing a used article' do
            describe 'when it is used by an article' do
              let(:tag) { create :tag, articles: [article] }
              let(:article) { create :article }

              describe 'when the user can edit the article' do
                before do
                  pretend_user_can(:update, article)
                end

                it 'must succeed' do
                  get :edit, id: tag
                  must_render :edit
                end
              end

              describe 'when the user cannot edit the article' do
                before do
                  pretend_user_cannot(:update, article)
                end

                it 'must be forbidden' do
                  must_be_forbidden ->{ get :edit, id: tag }
                end
              end
            end

            describe 'when it is used by a medium' do
              let(:tag) { create :tag, media: [medium] }
              let(:medium) { create :image_medium }

              describe 'when the user can edit the medium' do
                before do
                  pretend_user_can(:update, medium)
                end

                it 'must succeed' do
                  get :edit, id: tag
                  must_render :edit
                end
              end

              describe 'when the user cannot edit the medium' do
                before do
                  pretend_user_cannot(:update, medium)
                end

                it 'must be forbidden' do
                  must_be_forbidden ->{ get :edit, id: tag }
                end
              end
            end
          end
        end

      end

      describe 'destroying tags' do
        describe 'with :unused permission' do
          let(:permissions) { %w[ tag/destroy/unused ] }

          describe 'when destroying an unused tag' do
            let(:tag) { create :tag }

            it 'must succeed' do
              delete :destroy, id: tag
              must_redirect_to tags_path, true
            end
          end

          describe 'when destroying a used tag' do
            describe 'when it is used by an article' do
              let(:tag) { create :tag, articles: create_list(:article, 1) }

              it 'must be forbidden' do
                must_be_forbidden ->{ delete :destroy, id: tag }
              end
            end

            describe 'when it is used by a medium' do
              let(:tag) { create :tag, media: create_list(:image_medium, 1) }

              it 'must be forbidden' do
                must_be_forbidden ->{ delete :destroy, id: tag }
              end
            end
          end
        end

        describe 'with :all permission' do
          let(:permissions) { %w[ tag/destroy/all ] }

          describe 'when destroying an unused tag' do
            let(:tag) { create :tag }

            it 'must succeed' do
              delete :destroy, id: tag
              must_redirect_to tags_path, true
            end
          end

          describe 'when destroying a used article' do
            describe 'when it is used by an article' do
              let(:tag) { create :tag, articles: [article] }
              let(:article) { create :article }

              describe 'when the user can destroy the article' do
                before do
                  pretend_user_can(:update, article)
                end

                it 'must succeed' do
                  delete :destroy, id: tag
                  must_redirect_to tags_path, true
                end
              end

              describe 'when the user cannot edit the article' do
                before do
                  pretend_user_cannot(:update, article)
                end

                it 'must be forbidden' do
                  must_be_forbidden ->{ delete :destroy, id: tag }
                end
              end
            end

            describe 'when it is used by a medium' do
              let(:tag) { create :tag, media: [medium] }
              let(:medium) { create :image_medium }

              describe 'when the user can destroy the medium' do
                before do
                  pretend_user_can(:update, medium)
                end

                it 'must succeed' do
                  delete :destroy, id: tag
                  must_redirect_to tags_path, true
                end
              end

              describe 'when the user cannot destroy the medium' do
                before do
                  pretend_user_cannot(:update, medium)
                end

                it 'must be forbidden' do
                  must_be_forbidden ->{ delete :destroy, id: tag }
                end
              end
            end
          end
        end
      end

      describe 'blacklisting tags' do
        let(:permissions) { [] }

        describe '[un]blacklisting a tag' do
          let(:tag) { create :tag }

          it 'must be forbidden' do
            must_be_forbidden ->{ post :blacklist, id: tag }
          end

          it 'must be forbidden' do
            must_be_forbidden ->{ post :unblacklist, id: tag }
          end
        end
      end
    end

    describe 'get index' do
      it 'succeeds' do
        get :index
        must_render :index
        assigns(:tags).wont_be_nil
        assigns(:order).wont_be_nil
        assigns(:filter).wont_be_nil
      end

      describe 'when specifying a sort criterion' do
        before do
          @last  = create :tag, name: 'xyz'
          @first = create :tag, name: 'abc'
        end

        it 'sorts results using it' do
          get :index, { sort: { field: 'name', order: :asc } }
          must_render :index
          assigns(:tags).must_equal [@first, @last]
        end
      end

      describe 'when specifying a page' do
        before do
          @tag_1 = create :tag, name: 'XXX', created_at: 3.minutes.ago
          @tag_2 = create :tag, name: 'YYY', created_at: 2.minutes.ago
          @tag_3 = create :tag, name: 'ZZZ', created_at: 1.minutes.ago
        end

        it 'shows records for that page' do
          get :index, { page: 3, per_page: 1 }
          must_render :index
          assigns(:tags).count.must_equal 1
          assigns(:tags).must_equal [@tag_3]
        end
      end

      describe 'when specifying the number of elements per page to show' do
        before do
          10.times { |i| create :tag, name: "Tag #{i + 1}" }
        end

        it 'shows at most that number of records' do
          get :index, { per_page: 4 }
          must_render :index
          assigns(:tags).count.must_equal 4
        end
      end

      describe 'when filter criteria is submitted' do
        before do
          @tag_1 = create :tag, name: 'abc'
          @tag_2 = create :tag, name: 'xyz'
        end

        it 'renders index action and filters the results' do
          get :index, { filter: { name: 'a' } }
          must_render :index
          assigns(:tags).count.must_equal 1
          assigns(:tags).must_equal [@tag_1]
        end
      end

      describe 'when reset filter is submitted' do
        before do
          get :index, { commit: I18n.t('activerecord.actions.shared.clear_filter') }
        end

        it 'redirects to index, resetting the filters' do
          must_redirect_to tags_path
          assigns(:filter).values.must_equal TagsFilter.defaults
        end
      end
    end

    describe 'get new' do
      before do
        get :new
      end

      it 'render new action' do
        must_render :new
        assigns(:tag).must_be :new_record?
      end
    end

    describe 'post create' do
      describe 'when valid values are submitted' do
        it 'adds a tag and redirects to the show page and show notice message' do
          it_adds_a Tag
          must_redirect_to tag_path(assigns(:tag))
          assigns(:tag).wont_be :new_record?
          must_have_notice 'tags.new.messages.success'
        end
      end

      describe 'when invalid values are submitted' do
        before do
          post :create, tag: { name: nil, slug: nil }
        end

        it 'renders back new action and displays errors' do
          must_render :new
          assigns(:tag).must_be :new_record?
          must_display_form_errors
        end
      end
    end

    describe 'get show' do
      let(:tag) { create :tag }

      before do
        get :show, id: tag
      end

      it 'render show action for the request tag' do
        must_render :show
        assigns(:tag).must_equal tag
      end
    end

    describe 'get edit' do
      let(:tag) { create :tag }

      before do
        get :edit, id: tag
      end

      it 'renders edit action for the requested tag' do
        must_render :edit
        assigns(:tag).must_equal tag
      end
    end

    describe 'patch/put update' do
      let(:tag) { create :tag }

      describe 'when valid values are submitted' do
        before do
          patch :update, id: tag, tag: { name: 'changed tag' }
        end

        it 'modifies the tag, redirects to show and display notice message' do
          assigns(:tag).name.must_equal 'changed tag'
          must_redirect_to tag_path(assigns(:tag))
          must_have_notice 'tags.edit.messages.success'
        end
      end

      describe 'when invalid values are submitted' do
        before do
          put :update, id: tag, tag: { name: nil, slug: nil }
        end

        it 'renders back edit action and displays errors' do
          must_render :edit
          assigns(:tag).must_equal tag
          must_display_form_errors
        end
      end
    end

    describe 'delete destroy' do
      it 'removes the tag, redirects to the index page and shows a notice' do
        it_deletes_a Tag
        must_redirect_to tags_path, true
        must_have_notice 'tags.destroy.messages.success'
      end
    end

    describe 'get search' do
      describe 'when params[:id] is present' do
        it 'assigns @tag' do
          get :search, id: create(:tag)
          assigns :tag
          must_succeed
        end
      end

      describe 'when params[:id] is not present' do
        it "doesn't assign @tag" do
          get :search
          assigns(:tag).must_be_nil
          must_succeed
        end
      end

      describe 'when param ids is not empty' do
        let(:names) { [@tag_1.name, @tag_2.name] }

        before do
          @tag_1 = create :tag, name: 'rainbow'
          @tag_2 = create :tag, name: 'potluck'
          @tag_3 = create :tag, name: 'luck of the Irish'
        end

        it 'calls Tag find' do
          get :search, ids: names

          matches = JSON.parse(response.body)
          matches.count.must_equal 2
          matches.map { |x| x['text'] }.sort.must_equal names.sort
        end
      end

      describe 'when params ids is empty' do
        let(:query) { 'Rumpelstiltskin' }

        after do
          Tag.unstub :search
        end

        it 'calls search with except' do
          tag = create(:tag)

          Tag.expects(:search).with(query: query, except: tag.id.to_s).returns(Tag.all).once

          get :search, q: query, except: tag
        end

        it 'calls search without except' do
          Tag.expects(:search).with(query: query, except: nil).returns(Tag.all).once

          get :search, q: query
        end
      end
    end

    describe 'get combine' do
      let(:tag) { create :tag }

      before do
        get :combine, id: tag
      end

      it 'render combine action' do
        must_render :combine
        assigns(:tag).must_equal tag
      end
    end

    describe 'post combine_update' do
      let(:tag) { create :tag, name: 'Tag' }

      before do
        @tag_one = create(:tag, name: 'Tag 1')
        @tag_two = create(:tag, name: 'Tag 2')
        post :combine_update, { id: tag.id, tag: { serialized_combined: 'Tag 1,Tag 2' } }
      end

      it 'has new combined tags, redirects to show and displays notice' do
        assigns(:tag).combined.sort.must_equal [@tag_one, @tag_two].sort
        must_redirect_to tag_path(assigns(:tag))
        must_have_notice 'tags.combine.messages.success'
      end
    end

    describe 'post separate' do
      let(:tag) { create(:tag, name: 'Tag 1') }

      before do
        tag.combiner = create(:tag, name: 'Tag 2')
        post :separate, id: tag
      end

      it 'separates the combined tag' do
        must_succeed
        assigns(:tag).must_equal tag
        assigns(:tag).combined.must_be_empty
      end
    end

    describe 'post [un]blacklist' do
      let(:tag) { create :tag }

      it 'succeeds to blacklist' do
        post :blacklist, id: tag
        assigns(:tag).hidden?.must_equal true
      end

      it 'succeeds to unblacklist' do
        post :unblacklist, id: tag
        assigns(:tag).hidden?.must_equal false
      end
    end
  end
end
