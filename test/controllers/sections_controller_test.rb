require 'test_helper'

class SectionsControllerTest < ActionController::TestCase
  describe SectionsController do
    let(:permissions) { %w[ section/read section/create section/update/all section/destroy/all ] }

    before do
      login permissions: permissions
      @controller = SectionsController.new
    end

    describe 'Permissions sanity checks' do
      let(:permissions) { %w[ section/read ] }
      let(:section) { create :section }

      describe 'create a new section' do
        describe 'when trying to get new' do
          it 'must be forbidden' do
            must_be_forbidden ->{ get :new }
          end
        end

        describe 'when trying to post create' do
          it 'must be forbidden' do
            must_be_forbidden ->{ post :create, section: attributes_for(:section) }
          end
        end
      end

      describe 'update a section' do
        describe 'when trying to get edit' do
          it 'must be forbidden' do
            must_be_forbidden ->{ get :edit, id: section }
          end
        end

        describe 'when trying to put update' do
          it 'must be forbidden' do
            must_be_forbidden ->{ put :update, id: section, section: attributes_for(:section) }
          end
        end
      end

      describe 'destroy a section' do
        describe 'when trying to delete destroy' do
          it 'must be forbidden' do
            must_be_forbidden ->{ delete :destroy, id: section }
          end
        end
      end
    end

    describe 'get index' do
      it 'succeeds' do
        get :index
        must_render :index
        assigns(:sections).wont_be_nil
        assigns(:filter).wont_be_nil
      end

      describe 'when specifying a sort criterion' do
        before do
          @section_1 = create :section, name: '1337', slug: '1337'
          @section_3 = create :section, name: 'Some section', slug: 'some-section'
          @section_2 = create :section, name: 'abc', slug: 'abc'
        end

        it 'sorts results using it' do
          get :index, { sort: { field: 'name', order: 'asc' } }
          assigns(:sections).must_equal [@section_1, @section_2, @section_3]
          must_render :index
        end
      end

      describe 'when specifying a page' do
        before do
          @section_1 = create :section, name: '1337', slug: '1337', created_at: 3.minutes.ago
          @section_2 = create :section, name: 'abc', slug: 'abc', created_at: 2.minutes.ago
          @section_3 = create :section, name: 'Some section', slug: 'some-section', created_at: 1.minutes.ago
        end

        it 'shows records for that page' do
          get :index, { page: 2, per_page: 2 }
          must_render :index
          assigns(:sections).count.must_equal 1
          assigns(:sections).must_equal [@section_3]
        end
      end

      describe 'when specifying the number of elements per page to show' do
        before do
          10.times { create :section }
        end

        it 'shows at most that number of records' do
          get :index, { per_page: 5 }
          must_render :index
          assigns(:sections).count.must_equal 5
        end
      end

      describe 'filter' do
        before do
          create :section, name: '135', slug: '135'
          create :section, name: 'abc', slug: 'abc'
        end

        describe 'when filter criteria is submitted' do
          before do
            get :index, { filter: { name: 'a' } }
          end

          it 'should filter results if a filter is submitted' do
            must_succeed
            assigns[:sections].wont_be_nil
            assigns[:sections].count.must_be :<, Section.count
          end
        end

        describe 'when reset is submitted' do
          before do
            get :index, { commit: I18n.t('activerecord.actions.shared.clear_filter') }
          end

          it 'clears the filter and redirects to index' do
            assigns(:filter).values.must_equal SectionsFilter.defaults
            must_redirect_to sections_url
          end
        end
      end
    end

    describe 'get new' do
      it 'succeeds' do
        get :new
        must_render :new
      end
    end

    describe 'create' do
      before { @supplement = create :supplement }
      it 'adds a section' do
        it_adds_a Section, options: { section: { supplement_id: @supplement.id}}
        must_redirect_to section_url(assigns(:section))
        must_have_notice 'sections.new.messages.success'
      end

      describe 'when submitted values are invalid' do
        it 'returns errors' do
          post :create, section: { name: nil }
          must_display_form_errors
        end
      end
    end

    describe 'get show' do
      it 'succeeds' do
        get :show, id: create(:section)
        must_render :show
      end
    end

    describe 'get edit' do
      it 'succeeds' do
        get :edit, id: create(:section)
        must_render :edit
      end
    end

    describe 'update' do
      before do
        @section = create :section
        patch :update, id: @section, section: attributes_for(:section, name: 'changed section')
      end

      it 'updates the section and redirects to show page' do
        assigns(:section).name.must_equal 'changed section'
        must_redirect_to section_url(assigns(:section))
        must_have_notice 'sections.edit.messages.success'
      end

      describe 'when submitted values are invalid' do
        it 'returns an error' do
          patch :update, id: @section, section: { name: nil }
          must_display_form_errors
        end
      end
    end

    describe 'destroy' do
      it 'removes a section' do
        it_deletes_a Section
        must_redirect_to sections_url, true
        must_have_notice 'activerecord.actions.section.destroyed'
      end
    end

    describe 'tree view' do
      it 'succeeds' do
        get :tree
        must_render :tree
        assigns(:sections).wont_be_nil
      end
    end

    describe 'search' do
      describe 'when a search is being made' do
        let(:query) { 'Awwwsome' }
        let(:section) { create :section }
        let(:matches) { [section] }

        before do
          Section.expects(:search).with(query: query).returns(matches).once
          matches.expects(:limit).with(20).returns(matches).once
          get :search, q: query
        end

        after do
          Section.unstub :search
        end

        it 'returns the matching elements in a JSON response' do
          must_succeed
          results = JSON.parse(response.body)
          results.wont_be_empty
          results.first['text'].must_equal section.to_s
        end
      end

      describe 'when specific ids are being requested' do
        let(:section_1) { create :section, name: 'First section' }
        let(:section_2) { create :section, name: 'Second section' }
        let(:ids) { [section_1.id, section_2.id] }

        before do
          get :search, ids: ids
        end

        it 'returns the requested editions' do
          must_succeed
          results = JSON.parse(response.body)
          results.count.must_equal 2
          results.first['text'].must_equal section_1.to_s
          results.last['text'].must_equal section_2.to_s
        end
      end
    end
  end
end
