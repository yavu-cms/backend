require 'test_helper'

class ApiControllerTest < ActionController::TestCase
  describe ApiController do
    describe 'GET /api/register/:client' do
      it 'must be implemented'
    end

    describe 'GET /api/context' do
      it 'must be implemented'
    end

    describe 'POST/GET/PUT/DELETE/PATCH /api/service' do
      it 'must be implemented'
    end

    describe 'POST/GET/PUT/DELETE/PATCH /api/service/preview' do
      it 'must be implemented'
    end

    describe 'POST /api/accounting' do
      it 'must be implemented'
    end
  end
end
