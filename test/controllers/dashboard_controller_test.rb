require 'test_helper'

class DashboardControllerTest < ActionController::TestCase
  describe DashboardController do
    before do
      @controller = DashboardController.new
    end

    describe 'get index' do
      it 'must succeed' do
        login permissions: []
        get :index
        must_succeed
      end
    end

    describe '#article_group_data' do
      let(:hash_keys) { %i[title range articles accounting_method accounting_params update_params].to_set }

      it 'returns hash with valid keys' do
        @controller.send(:article_group_data, 'last_year').keys.to_set.must_equal(hash_keys)
      end
    end
  end
end
