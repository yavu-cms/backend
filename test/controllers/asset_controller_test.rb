require 'test_helper'

class AssetControllerTest < ActionController::TestCase
  describe AssetController do
    before do
      login permissions: [ ]
      @controller = AssetController.new
    end

    describe 'serve_component_content' do
      describe 'when asset is valid' do
        before do
          @asset = create :component_asset
          @content = 'something'
          IO.expects(:read).with(@asset.file.current_path).returns(@content)
        end

        it 'succeeds' do
          get :serve_component_content, id: @asset
          response.body.must_equal @content
          must_succeed
        end
      end

      describe 'when asset is not valid' do
        it 'fails' do
          ->{ get :serve_component_content, id: 'not_exists' }.must_raise ActiveRecord::RecordNotFound
        end
      end
    end

    describe 'serve_client_application_content' do
      describe 'when asset is valid' do
        before do
          @asset = create :client_application_asset
          @content = 'something_lalala'
          IO.expects(:read).with(@asset.file.current_path).returns(@content)
        end

        it 'succeeds' do
          get :serve_client_application_asset_content, id: @asset
          response.body.must_equal @content
          must_succeed
        end
      end

      describe 'when asset is not valid' do
        it 'fails' do
          ->{ get :serve_client_application_asset_content, id: 'not_exists' }.must_raise ActiveRecord::RecordNotFound
        end
      end
    end
  end
end
