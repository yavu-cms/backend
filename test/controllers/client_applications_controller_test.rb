require 'test_helper'

class ClientApplicationsControllerTest < ActionController::TestCase
  describe ClientApplicationsController do
    let(:permissions) { %w[ client_application/read client_application/manage client_application/design_manage client_application/destroy client_application/invalidate_routes ] }

    before do
      login permissions: permissions
      @controller = ClientApplicationsController.new
    end

    describe 'Permissions sanity checks' do
      describe 'manage a new client application' do
        describe 'without permissions' do
          let(:permissions) { [] }

          describe 'when trying to get new client application' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :new }
            end
          end

          describe 'when trying to post create' do
            it 'must be forbidden' do
              must_be_forbidden -> { it_doesnt_add_a ClientApplication, factory: :client_application }
            end
          end

          describe 'when trying to get edit a client application' do
            let(:client_application) { create :client_application }
            let(:client_application_outside_workspace) { create :client_application }

            it 'must be forbidden' do
              pretend_user_workspace_includes client_application
              must_be_forbidden -> { get :edit, id: client_application }
              must_be_forbidden -> { get :edit, id: client_application_outside_workspace }
            end
          end

          describe 'when trying to put update a client application' do
            let(:client_application) { create :client_application }
            let(:client_application_outside_workspace) { create :client_application }

            it 'must be forbidden' do
              pretend_user_workspace_includes client_application
              client_application.name = 'modified name'
              client_application_outside_workspace.name = 'other modified name'
              must_be_forbidden -> { put :update, id: client_application, client_application: client_application.attributes }
              must_be_forbidden -> { put :update, id: client_application_outside_workspace, client_application: client_application_outside_workspace.attributes }
            end
          end

          describe 'when trying to post notify clients' do
            let(:client_application) { create :client_application }
            let(:client_application_outside_workspace) { create :client_application }
            it 'must be forbidden' do
              pretend_user_workspace_includes client_application
              must_be_forbidden -> { post :notify_clients, id: client_application }
              must_be_forbidden -> { post :notify_clients, id: client_application_outside_workspace }
            end
          end

          describe 'when trying to post disable a client application' do
            let(:client_application) { create :client_application }
            let(:client_application_outside_workspace) { create :client_application }
            it 'must be forbidden' do
              pretend_user_workspace_includes client_application
              must_be_forbidden -> { post :disable, id: client_application }
              must_be_forbidden -> { post :disable, id: client_application_outside_workspace }
            end
          end

          describe 'when trying to post enable a client application' do
            let(:client_application) { create :disabled_client_application }
            let(:client_application_outside_workspace) { create :disabled_client_application }
            it 'must be forbidden' do
              pretend_user_workspace_includes client_application
              must_be_forbidden -> { post :enable, id: client_application }
              must_be_forbidden -> { post :enable, id: client_application_outside_workspace }
            end
          end

          describe 'when trying to get invalidate routes' do
            let(:client_application) { create :disabled_client_application }
            let(:client_application_outside_workspace) { create :disabled_client_application }
            it 'must be forbidden' do
              pretend_user_workspace_includes client_application
              must_be_forbidden -> { get :invalidate_routes, id: client_application }
              must_be_forbidden -> { get :invalidate_routes, id: client_application_outside_workspace }
            end
          end

          describe 'when trying to post invalidate routes' do
            let(:client_application) { create :disabled_client_application }
            let(:client_application_outside_workspace) { create :disabled_client_application }
            it 'must be forbidden' do
              pretend_user_workspace_includes client_application
              must_be_forbidden -> { post :invalidate_routes, id: client_application }
              must_be_forbidden -> { post :invalidate_routes, id: client_application_outside_workspace }
            end
          end

          describe 'whe trying to get copy' do
            let(:client_application) { create :client_application }
            let(:client_application_outside_workspace) { create :disabled_client_application }

            it 'must be forbidden' do
              pretend_user_workspace_includes client_application
              must_be_forbidden -> { get :copy, id: client_application }
              must_be_forbidden -> { get :copy, id: client_application_outside_workspace }
            end
          end

          describe 'whe trying to post copy' do
            let(:client_application) { create :client_application }
            let(:client_application_outside_workspace) { create :disabled_client_application }

            it 'must be forbidden' do
              pretend_user_workspace_includes client_application
              must_be_forbidden -> { post :create_copy, id: client_application }
              must_be_forbidden -> { post :create_copy, id: client_application_outside_workspace }
            end
          end
        end

        describe 'when have :manage permission' do
          let(:permissions) { %w[ client_application/manage ] }
          let(:client_application) { create :client_application }
          let(:client_application_outside_workspace) { create :client_application }

          before do
            pretend_user_workspace_includes client_application
          end

          describe 'when trying to get new client application' do
            it 'succeeds' do
              get :new
              must_render :new
            end
          end

          describe 'when trying to post create' do
            it 'succeeds' do
              post :create, client_application: attributes_for(:client_application).merge(news_source_id: create(:news_source))
              must_redirect_to client_application_path(assigns(:client_application))
              assigns(:client_application).wont_be :new_record?
              must_have_notice 'client_applications.new.messages.success'
            end
          end

          describe 'when trying to get edit a client application outside workspace' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :edit, id: client_application_outside_workspace }
            end
          end

          describe 'when trying to put update a client application outside workspace' do
            it 'must be forbidden' do
              client_application_outside_workspace.name = 'other modified name'
              must_be_forbidden -> { put :update, id: client_application_outside_workspace, client_application: client_application_outside_workspace.attributes }
            end
          end

           describe 'when trying to post notify clients for client application outside workspace' do
            it 'must be forbidden' do
              must_be_forbidden -> { post :notify_clients, id: client_application_outside_workspace }
            end
          end

          describe 'when trying to post disable for client application outside workspace' do
            it 'must be forbidden' do
              must_be_forbidden -> { post :disable, id: client_application_outside_workspace }
            end
          end

          describe 'when trying to post enable for client application outside workspace' do
            it 'must be forbidden' do
              must_be_forbidden -> { post :enable, id: client_application_outside_workspace }
            end
          end

          describe 'when trying to get invalidate routes' do
            let(:client_application) { create :client_application }
            it 'succeeds' do
              get :invalidate_routes, id: client_application
              must_render :invalidate_routes
            end
            it 'fails for a client application outside workspace'
          end
        end
      end

      describe 'read a client application' do
        describe 'without permissions' do
          let(:permissions) { [] }

          describe 'when trying to get index' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :index }
            end
          end

          describe 'when trying to get show' do
            let(:client_application) { create :client_application }
            let(:client_application_outside_workspace) { create :disabled_client_application }

            it 'must be forbidden' do
              must_be_forbidden -> { get :show, id: client_application }
              must_be_forbidden -> { get :show, id: client_application_outside_workspace }
            end
          end
        end

        describe 'with :read permissions' do
          let(:permissions) { %w[ client_application/read ] }
          let(:client_application) { create :client_application }
          let(:client_application_outside_workspace) { create :client_application }

          before do
            pretend_user_workspace_includes client_application
          end

          describe 'when trying to get show client applicacion into user workspace' do
            it 'succeeds' do
              get :show, id: client_application
              must_render :show
              assigns(:client_application).must_equal client_application
            end
          end

          describe 'when trying to get show client application outside workspace' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :show, id: client_application_outside_workspace }
            end
          end
        end
      end

      describe 'design manage a client application' do
        let(:client_application) { create :client_application }
        let(:client_application_outside_workspace) { create :client_application }

        before do
          pretend_user_workspace_includes client_application
        end

        describe 'without permissions' do
          let(:permissions) { [] }

          describe 'when trying to post compile_assets' do
            it 'must be forbidden' do
              must_be_forbidden -> { post :compile_assets, id: client_application }
              must_be_forbidden -> { post :compile_assets, id: client_application_outside_workspace }
            end
          end

          describe 'when trying to get preview' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :preview, id: client_application }
              must_be_forbidden -> { get :preview, id: client_application_outside_workspace }
            end
          end

          describe 'when trying to get update_preview' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :update_preview, id: client_application }
              must_be_forbidden -> { get :update_preview, id: client_application_outside_workspace }
            end
          end
        end

        describe 'with :design_manage permissions' do
          let(:permissions) { %w[ client_application/design_manage ] }

          describe 'when trying to post compile_assets for a client application outside workspace' do
            it 'must be forbidden' do
              must_be_forbidden -> { post :compile_assets, id: client_application_outside_workspace }
            end
          end

          describe 'when trying to get preview for a client application outside workspace' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :preview, id: client_application_outside_workspace }
            end
          end

          describe 'when trying to get update preview for a client application outside workspace' do
            it 'must_be_forbidden'
          end
        end
      end

      describe 'destroy client application' do
        describe 'without permissions' do
          let(:permissions) { [] }
          describe 'when trying to delete destroy' do
            it 'must be forbidden' do
              must_be_forbidden -> { it_deletes_a ClientApplication  }
            end
          end
        end
      end

      describe 'extra attributes for client application' do
        let(:client_application) { create :client_application }
        describe 'without permissions' do
          let(:permissions) { [] }
          describe 'when trying to get extra_attributes' do
            it 'must be forbidden' do
              must_be_forbidden ->{ get :extra_attributes, id: client_application }
            end
          end
        end

        describe 'with permissions' do
          let(:permissions) { %w[ client_application/extra_attributes_management ] }
          it 'succeeds' do
            pretend_user_workspace_includes client_application
            get :extra_attributes, id: client_application
            must_render :extra_attributes
          end
        end
      end
    end

    describe 'get index' do
      describe 'on a regular request' do
        before do
          client_application_with_cover = create :client_application
          cover_representation = create :representation, client_application: client_application_with_cover
          cover_representation.stubs(:cover?).returns(true)
          @client_applications = [
            create(:client_application),
            client_application_with_cover,
            create(:disabled_client_application)
          ]
        end

        it 'must succeed' do
          get :index
          must_render :index
          assigns(:client_applications).sort.must_equal @client_applications.sort
        end
      end

      describe 'when specifying a page' do
        before do
          @client_application_1 = create :client_application, name: 'Number one', created_at: 3.minutes.ago
          @client_application_2 = create :client_application, name: 'Number two', created_at: 2.minutes.ago
          @client_application_3 = create :client_application, name: 'Number three', created_at: 1.minute.ago
        end

        it 'shows records for that page' do
          get :index, { page: 3, per_page: 1 }
          must_render :index
          assigns(:client_applications).count.must_equal 1
          assigns(:client_applications).must_equal [@client_application_3]
        end
      end

      describe 'when specifying the number of elements per page to show' do
        before do
          4.times { create :client_application }
        end

        it 'shows at most that number of records' do
          get :index, { per_page: 2 }
          must_render :index
          assigns(:client_applications).count.must_equal 2
        end
      end

      describe 'when filtering results' do
        before do
          @cli_app_1 = create :client_application
          @cli_app_2 = create :disabled_client_application
        end

        it 'show the matching records' do
          get :index, { filter: { enabled: 'enabled' } }
          assigns(:client_applications).to_a.must_equal [@cli_app_1]
        end
      end

      describe 'when reset is submitted' do
        before do
          get :index, { commit: I18n.t('activerecord.actions.shared.clear_filter') }
        end

        it 'clear the filters and redirects to index' do
          assigns(:filter).values.must_equal ClientApplicationsFilter.defaults
          must_redirect_to client_applications_url
        end
      end
    end

    describe 'get new' do
      it 'must succeed' do
        get :new
        must_render :new
        assigns(:client_application).must_be :new_record?
      end
    end

    describe 'post create' do
      describe 'when valid data is posted' do
        it 'adds a client application and redirects to the show page' do
          post :create, client_application: attributes_for(:client_application).merge(news_source_id: create(:news_source))
          must_redirect_to client_application_path(assigns(:client_application))
          assigns(:client_application).wont_be :new_record?
          must_have_notice 'client_applications.new.messages.success'
        end
      end

      describe 'when invalid data is posted' do
        before do
          post :create, client_application: attributes_for(:client_application, name: '')
        end

        it 'shows again the new page and displays the validation errors' do
          must_render :new
          assigns(:client_application).must_be :new_record?
          must_display_form_errors
        end
      end
    end

    describe 'get show' do
      let(:client_application) { create :client_application }

      it 'must succeed' do
        pretend_user_workspace_includes client_application
        get :show, id: client_application
        must_render :show
        assigns(:client_application).must_equal client_application
      end
    end

    describe 'get edit' do
      let(:client_application) { create :client_application }

      before do
        pretend_user_workspace_includes client_application
        get :edit, id: client_application
      end

      it 'must succeed' do
        must_render :edit
        assigns(:client_application).wont_be :new_record?
      end
    end

    describe 'patch update' do
      let(:client_application) { create :client_application }

      before do
        pretend_user_workspace_includes client_application
      end

      describe 'when an invalid set of attributes is submitted' do
        before do
          patch :update, id: client_application, client_application: attributes_for(:client_application, name: '')
        end

        it 'shows again the edit page and displays the validation errors' do
          must_render :edit
          assigns(:client_application).must_equal client_application
          must_display_form_errors
        end
      end

      describe 'when a valid set of attributes is submitted' do
        it 'updates the client application and redirects to show' do
          patch :update, id: client_application, client_application: attributes_for(:client_application, name: 'Another name for FrontApp')
          assigns(:client_application).name.must_equal 'Another name for FrontApp'
          must_redirect_to client_application_path(assigns(:client_application))
        end
      end
    end

    describe 'delete destroy' do
      let(:client_application) { create :client_application }

      it 'deletes the client application' do
        pretend_user_workspace_includes client_application
        delete :destroy, id: client_application
        must_redirect_to client_applications_path, true
        must_have_notice 'client_applications.destroy.messages.success'
      end
    end

    describe 'get preview' do
      before do
        @client_application = create :client_application
        pretend_user_workspace_includes @client_application
        get :preview, id: @client_application
      end

      it 'succeeds' do
        must_render :preview
      end
    end

    describe 'get update_preview' do
      it 'needs to be refactored and tested'
    end

    describe 'get compile assets' do
      let(:client_application) { create :client_application }

      after do
        CompileAssetsWorker.unstub(:perform_async)
      end

      it 'Must call CompileAssetsWorker perform_async' do
        pretend_user_workspace_includes client_application
        CompileAssetsWorker.expects(:perform_async).with(client_application.id, authenticated.username).once
        post :compile_assets, id: client_application
        must_redirect_to client_application_path(assigns(:client_application))
        must_have_notice 'client_applications.assets_compile.message'
      end
    end

    describe 'get notify clients' do
      let(:client_application) { create :client_application }
      after do
        FrontendServer.unstub(:notify)
      end

      it 'must call FrontendServer notify' do
        pretend_user_workspace_includes client_application
        FrontendServer.expects(:notify).with(client_application).once
        get :notify_clients, id: client_application
        must_redirect_to client_applications_path
        must_have_notice 'client_applications.notify.message'
      end
    end

    describe 'get enable' do
      let(:client_application) { create :disabled_client_application }

      it 'enables the client application and redirects to show' do
        pretend_user_workspace_includes client_application
        get :enable, id: client_application
        client_application.reload.must_be :enabled
        must_redirect_to client_application_path(assigns(:client_application))
        must_have_notice 'client_applications.enable.messages.success'
      end
    end

    describe 'get disable' do
      let(:client_application) { create :client_application }

      it 'disables the client application and redirects to show' do
        pretend_user_workspace_includes client_application
        get :disable, id: client_application
        client_application.reload.wont_be :enabled
        must_redirect_to client_application_path(assigns(:client_application))
        must_have_notice 'client_applications.disable.messages.success'
      end
    end

    describe 'get search' do
      describe 'when param[:ids] is not empty' do
        before do
          @cli_app_1 = create :client_application
          @cli_app_2 = create :client_application
          @cli_app_3 = create :client_application
          @cli_apps  = [@cli_app_1, @cli_app_3]
          @ids       = @cli_apps.map(&:id)
        end

        it 'searches the client applications whose id\'s match with ids param' do
          expected = [
            { 'id' => @cli_app_1.id, 'text' => @cli_app_1.name },
            { 'id' => @cli_app_3.id, 'text' => @cli_app_3.name }
          ]
          get :search, ids: @ids
          JSON.parse(response.body).must_equal expected
        end
      end

      describe 'when params[:ids] is empty' do
        before do
          @cli_app_1 = create :client_application
          @cli_app_2 = create :client_application, name: 'my front'
          @cli_app_3 = create :client_application
          search_limit = 100
          @controller.expects(:search_limit).returns(search_limit).once
          results_stub = stub
          results_stub.expects(:limit).with(search_limit).returns([@cli_app_2]).once
          ClientApplication.expects(:search).with(query: 'my front').returns(results_stub).once
        end

        after do
          ClientApplication.unstub(:search)
        end

        it 'calls ClientApplication#search with params[:q]' do
          get :search, q: 'my front'
        end
      end
    end

    describe 'post invalidate_routes' do
      let(:client_application) { create :client_application_with_routes }
      let(:client_application_outside_workspace) { create :client_application_with_routes  }

      describe 'when all routes are invalidated' do
        before { @controller.stubs(:all_routes_invalidated?).returns(true) }
        after  { @controller.unstub(:all_routes_invalidated?) }

        it 'succeeds and renders client applications index' do
          skip 'Fix this test'
          pretend_user_workspace_includes client_application
          post :purge_routes, id: client_application, routes_to_purge: client_application.routes.map(&:id)
          must_redirect_to client_applications_path
        end
      end
      describe 'when some routes are not invalidated' do
        it 'shows again invalidate_routes page with errors'
      end
    end
  end
end
