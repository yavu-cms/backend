require 'test_helper'
require 'mocha/setup'

class ArticlesControllerTest < ActionController::TestCase
  describe ArticlesController do
    let(:permissions) { %w[ article/read article/create/all article/update/all article/destroy/all ] }
    let(:sections_workspace) { :all }

    before do
      login permissions: permissions, sections: sections_workspace
      @controller = ArticlesController.new
    end

    describe 'Permissions sanity checks' do
      let(:non_visible_article) { create(:non_visible_article) }
      let(:visible_article) { create(:visible_article) }

      before do
        pretend_user_workspace_includes [non_visible_article.section, visible_article.section]
      end

      describe 'create a new article' do
        describe 'without permissions' do
          let(:permissions) { %w[ article/read ] }

          describe 'when trying to get new' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :new }
            end
          end

          describe 'when trying to post create' do
            it 'must be forbidden' do
              must_be_forbidden -> { it_doesnt_add_an Article , factory: :non_visible_article }
              must_be_forbidden -> { it_doesnt_add_an Article , factory: :visible_article }
            end
          end

          describe 'when trying to get copy' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :copy, id: non_visible_article }
              must_be_forbidden -> { get :copy, id: visible_article }
            end
          end

          describe 'when trying to post create_copy' do
            it 'must be forbidden' do
              must_be_forbidden -> { post :create_copy, article: attributes_for(:non_visible_article) }
              must_be_forbidden -> { post :create_copy, article: attributes_for(:visible_article) }
            end
          end
        end

        describe 'when :invisible permissions' do
          let(:permissions) { %w[ article/create/invisible ] }

          describe 'when trying to post create visible article' do
            it 'must be forbidden' do
              must_be_forbidden -> { post :create, article: build(:visible_article).attributes }
            end
          end

          describe 'when trying to create invisible article' do
            it 'succeed' do
              art = build(:non_visible_article)
              pretend_user_workspace_includes [art.section]
              post :create, article: art.attributes
              must_redirect_to article_path(assigns(:article))
              assigns(:article).wont_be :new_record?
              must_have_notice 'articles.new.messages.success'
            end
          end

          describe 'when trying to get copy visible article' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :copy, id: visible_article }
            end
          end

          describe 'when trying to get copy invisible article' do
            it 'succeed' do
              get :copy, id: non_visible_article
              must_render :copy
              assigns(:article).wont_be_nil
            end
          end

          describe 'when trying to post create_copy visible article' do
            it 'must be forbidden' do
              must_be_forbidden -> { post :create_copy, article: create(:visible_article).attributes }
            end
          end

          describe 'when trying to post create_copy invisible article' do
            it 'succeed' do
              art = build(:non_visible_article)
              pretend_user_workspace_includes [art.section]
              post :create_copy, article: art.attributes
              must_redirect_to article_path(assigns(:article))
              assigns(:article).wont_be :new_record?
              must_have_notice 'articles.new.messages.success'
            end
          end
        end
      end

      describe 'read article' do
        describe 'without permissions' do
          let(:permissions) { %w[ article/create/all ] }

          describe 'when trying to get index' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :index }
            end
          end

          describe 'when trying to get show article' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :show, id: non_visible_article }
            end
          end

          describe 'when trying to get search' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :search }
            end
          end

          describe 'when trying to get search_by_section_and_edition' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :search_by_section_and_edition }
            end
          end

          describe 'when trying to get media by article' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :media_by_article, id: non_visible_article }
            end
          end
        end
      end

      describe 'update article' do
        describe 'without permissions' do
          let(:permissions) { [] }

          describe 'when trying to get edit article' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :edit, id: visible_article }
            end
          end

          describe 'when trying to put update article' do
            it 'must be forbidden' do
              non_visible_article.title = 'updated article title'
              must_be_forbidden -> { put :update, id: non_visible_article }
            end
          end
        end

        describe 'when has :invisible permission' do
          let(:permissions) { %w[ article/update/invisible ] }

          describe 'when trying to get edit visible article' do
            it 'must be forbidden' do
              must_be_forbidden -> { get :edit, id: visible_article }
            end
          end

          describe 'when trying to get edit invisible article' do
            it 'succeed' do
              get :edit, id: non_visible_article
              must_render :edit
              assigns(:article).wont_be :new_record?
            end
          end

          describe 'when trying to put update visible article' do
            it 'must be forbidden' do
              visible_article.title = 'updated article title'
              must_be_forbidden -> { put :update, id: visible_article }
            end
          end

          describe 'when trying to put invisible article' do
            it 'succeed' do
              non_visible_article.title = 'updated article title'
              put :update, id: non_visible_article, article: non_visible_article.attributes
              must_redirect_to article_path(assigns(:article))
              assigns(:article).title.must_equal 'updated article title'
            end
          end
        end
      end

      describe 'destroy article' do
        describe 'without permissions' do
          let(:permissions) { [] }

          describe 'when trying to destroy article' do
            it 'must be forbidden' do
              must_be_forbidden -> { it_doesnt_delete_an Article, factory: non_visible_article }
            end
          end
        end

        describe 'when have :invisible permissions' do
          let(:permissions) { %w[ article/destroy/invisible ] }

          describe 'when trying to destroy visible article' do
            it 'must be forbidden' do
              must_be_forbidden -> { it_doesnt_delete_an Article, factory: visible_article }
            end
          end

          describe 'when trying to destroy invisible article' do
            it 'succeed' do
              it_deletes_an Article, factory: non_visible_article
              must_redirect_to articles_path, true
              must_have_notice 'articles.destroy.messages.success'
            end
          end
        end
      end
    end

    describe 'get index' do
      describe 'on a regular request' do
        it 'must succeed' do
          get :index
          must_render :index
          assigns(:articles).wont_be_nil
        end
      end

      describe 'when specifying a sort criterion' do
        before do
          @article_2 = create :article, title: 'Boahahaha', created_at: 2.minutes.ago
          @article_3 = create :article, title: 'ZZLast one', created_at: 1.minute.ago
          @article_1 = create :article, title: 'Ahhlalala', created_at: 3.minutes.ago
        end

        it 'sorts results using it' do
          skip "FIX ME"
          get :index, { sort: { field: 'title', order: 'asc' } }
          assigns(:articles).must_equal [@article_1, @article_2, @article_3]
          must_render :index
        end
      end

      describe 'when specifying a page' do
        before do
          @article_1 = create :article, updated_at: 6.minutes.ago
          @article_2 = create :article, updated_at: 4.minutes.ago
          @article_3 = create :article, updated_at: 2.minute.ago
        end

        it 'shows records for that page' do
          skip "FIX ME"
          get :index, { page: 2, per_page: 2 }
          must_render :index
          assigns(:articles).count.must_equal 1
          assigns(:articles).must_equal [@article_3]
        end
      end

      describe 'when specifying the number of elements per page to show' do
        before do
          10.times { create :article }
        end

        it 'shows at most that number of records' do
          skip "FIX ME"
          get :index, { per_page: 5 }
          must_render :index
          assigns(:articles).count.must_equal 5
        end
      end

      describe 'when filtering results' do
        before do
          @article_1 = create :article, title: 'A title'
          @article_2 = create :article, title: 'Other title'
          @article_3 = create :article, title: 'Bla bla title'
        end

        it 'show the matching records' do
          skip "FIX ME"
          get :index, { filter: { containing: 'Bla' } }
          assigns(:articles).to_a.must_equal [@article_3]
        end
      end

      describe 'when reset is submitted' do
        before do
          get :index, { commit: I18n.t('activerecord.actions.shared.clear_filter') }
        end

        it 'clear the filters and redirects to index' do
          assigns(:filter).values.must_equal ArticlesFilter.defaults
          must_redirect_to articles_url
        end
      end
    end

    describe 'get new' do
      it 'must succeed' do
        get :new
        must_render :new
        assigns(:article).must_be :new_record?
      end
    end

    describe 'post create' do
      describe 'when valid data is posted' do
        before { @section = create :section }

        it 'adds an article and redirects to the show page' do
          pretend_user_workspace_includes [@section]
          it_adds_an Article, options: {article: {section_id: @section.id}}
          must_redirect_to article_path(assigns(:article))
          assigns(:article).wont_be :new_record?
          must_have_notice 'articles.new.messages.success'
        end
      end

      describe 'when invalid data is posted' do
        before do
          post :create, article: attributes_for(:article, title: '')
        end

        it 'shows again the new page and displays the validation errors' do
          must_render :new
          assigns(:article).must_be :new_record?
          must_display_form_errors
        end
      end
    end

    describe 'get show' do
      let(:article) { create :article }

      before do
        pretend_user_workspace_includes [article.section]
      end

      it 'must succeed' do
        get :show, id: article
        must_render :show
        assigns(:article).must_equal article
      end
    end

    describe 'get edit' do
      let(:article) { create :article }

      it 'must succeed' do
        pretend_user_workspace_includes [article.section]
        get :edit, id: article
        must_render :edit
        assigns(:article).wont_be :new_record?
      end
    end

    describe 'patch update' do
      let(:article) { create :article }
      let(:channel) { "/uniqueaccess/article/#{article.to_param}" }
      let(:announcements_url) { 'http://test.host:5000/wsock' }

      before do
        pretend_user_workspace_includes [article.section]
      end

      after do
        UniqueAccessWorker.unstub(:perform_async)
      end

      describe 'when an invalid set of attributes is submitted' do
        before do
          UniqueAccessWorker.expects(:perform_async).never
          patch :update, id: article, article: attributes_for(:article, title: '')
        end

        it 'shows again the edit page and displays the validation errors' do
          must_render :edit
          assigns(:article).must_equal article
          must_display_form_errors
        end
      end

      describe 'when a valid set of attributes is submitted' do
        before do
          UniqueAccessWorker.expects(:perform_async).with(channel, {user: authenticated.username, saved: true}, announcements_url)
          patch :update, id: article, article: attributes_for(:article, title: 'modified article')
        end

        it 'updates the article and redirects to show' do
          assigns(:article).title.must_equal 'modified article'
          must_redirect_to article_path(assigns(:article))
        end
      end
    end

    describe 'delete destroy' do
      let(:article) { create :article }
      let(:channel) { "/uniqueaccess/article/#{article.to_param}" }
      let(:announcements_url) { 'http://test.host:5000/wsock' }

      before do
        pretend_user_workspace_includes [article.section]
      end

      after { UniqueAccessWorker.unstub :perform_async }

      it 'deletes the article and redirects to the index page' do
        UniqueAccessWorker.expects(:perform_async).with(channel, {user: authenticated.username, destroyed: true}, announcements_url)
        it_deletes_an Article, factory: article
        must_redirect_to articles_path, true
        must_have_notice 'articles.destroy.messages.success'
      end

      it 'wont delete the article if is used in a cover and redirects to the index page' do
        config = create(:cover_articles_component_configuration)
        view = config.component.views.create name: 'Named view'
        ArticlesCoverArticlesComponentConfiguration.create! article: article,
          cover_articles_component_configuration: config,
          view: view

        UniqueAccessWorker.expects(:perform_async).never
        it_doesnt_delete_an Article, factory: article
        must_redirect_to articles_path, true
        must_have_alert 'articles.destroy.messages.has_covers_associated'
      end
    end

    describe 'get copy' do
      let(:article) { create :article }
      let(:copy) { build :article }

      before do
        Article.any_instance.expects(:copy).once.returns(copy)
        pretend_user_workspace_includes [article.section]
      end

      after do
        Article.any_instance.unstub(:copy)
      end

      it 'must succeed' do
        get :copy, id: article
        must_succeed
        assigns(:article).must_equal copy
      end
    end

    describe 'post create copy' do
      it 'must redirect to show if succeeds' do
        art = build(:visible_article)
        pretend_user_workspace_includes [art.section]
        post :create_copy, article: art.attributes
        must_redirect_to articles_path(assigns(:article))
        must_have_notice 'articles.new.messages.success'
      end

      it 'must stay in copy if fails' do
        art = build(:non_visible_article, title: nil)
        pretend_user_workspace_includes [art.section]
        post :create_copy, article: art.attributes
        must_render :copy
        must_display_form_errors
      end

      it 'must create a new article copy' do
        art = build(:visible_article)
        pretend_user_workspace_includes [art.section]
        post :create_copy, article: art.attributes
        Article.count.must_equal 1
      end
    end

    describe 'search' do
      describe 'when params[:id] is present' do
        it 'assigns @article' do
          article = create :article
          pretend_user_workspace_includes [article.section]
          get :search, id: article
          assigns :article
          must_succeed
        end
      end

      describe 'when params[:id] is not present' do
        it "doesn't assign @article" do
          get :search
          assigns(:article).must_be_nil
          must_succeed
        end
      end

      describe 'when param ids is not empty' do
        let(:articles) { create_list :article, 4 }
        let(:ids) { articles[0..2].map &:id }

        after do
          Article.unstub :where
        end

        it 'calls Article find' do
          get :search, ids: ids
        end
      end

      describe 'when params ids is empty' do
        after do
          Article.unstub(:search)
        end

        it 'calls search with except if id is not nil' do
          skip 'UPDATE ME'
        end
        it 'calls search without except if id is nil' do
          skip 'UPDATE ME'
        end
      end
    end

    describe '#search by section and edition' do

      before do
        @edition         = create :edition
        @today_edition   = create :supplement_edition
        @section         = create :section, supplement: @edition.supplement
        @another_section = create :section, supplement: @edition.supplement
        @another_section_today = create :section, supplement: @today_edition.supplement
        @section_today   = create :section, supplement: @today_edition.supplement

        @article_1 = create :article, section: @section, edition: @edition
        @article_2 = create :article, section: @section, edition: @edition
        @article_3 = create :article, section: @another_section, edition: @edition, supplement: @edition.supplement
        @article_4 = create :article, section: @section_today, edition: @today_edition, supplement: @today_edition.supplement
        @article_5 = create :article, section: @another_section_today, edition: @today_edition, supplement: @today_edition.supplement
      end

      describe 'when params[:section_id_for_search] is present' do
        it 'must return visible articles involved with the section passed by params' do
          get :search_by_section_and_edition, section_id: @section.id
          assigns(:articles).to_a.must_equal [@article_1, @article_2 ]
        end
      end

      describe 'when params[:edition_date_for_search] is present' do
        it 'must return articles from this edition' do
          get :search_by_section_and_edition, edition_date: '20/03/2014'
          assigns(:articles).to_a.must_equal [@article_4, @article_5]
        end
      end

      describe 'when limit is specified' do
        it 'limits the number of articles to that number' do
          get :search_by_section_and_edition, section_id: @section.id, limit: 1
          assigns(:articles).count.must_equal 1
        end
      end

      describe 'when user workspace is limited by section' do
        let(:my_edition) { create(:edition) }
        let(:my_section) { create(:section, supplement: my_edition.supplement) }
        let(:sections_workspace) { [ my_section ] }

        before do
          @article_x = create :article, section: my_section, edition: my_edition
        end

        it 'limits the number of articles' do
          get :search_by_section_and_edition
          Article.count.must_equal 6
          assigns(:articles).count.must_equal 1
        end
      end
    end
  end
end
