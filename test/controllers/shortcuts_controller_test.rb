require 'test_helper'

class ShortcutsControllerTest < ActionController::TestCase
  describe ShortcutsController do
    let(:permissions) { [] }

    before do
      login permissions: permissions
      @controller = ShortcutsController.new
    end

    describe 'get manage' do
      before do
        get :manage
      end

      it 'render manage' do
        must_render :manage
      end

      it 'assigns' do
        assigns(:shortcuts).wont_be_nil
      end
    end

    describe 'get new' do
      before do
        get :new, url: 'http://www.example.com', title: 'home'
      end

      it 'renders the new action and sets the passed in parameters' do
        must_render :new
        assigns(:shortcut).must_be :new_record?
        assigns(:shortcut).url.must_equal 'http://www.example.com'
        assigns(:shortcut).title.must_equal 'home'
      end
    end

    describe 'post create' do
      describe 'when valid values are submitted' do
        it 'adds a shortcut and show notice message' do
          it_adds_a Shortcut, factory: :admin_shortcut
          assigns(:shortcut).wont_be :new_record?
          must_have_notice 'shortcuts.new.messages.success'
        end

        it 'render action create when :postbuster params is present' do
          post :create, postbuster: '', shortcut: attributes_for(:admin_shortcut)
          must_render :create
        end
      end

      describe 'when invalid values are submitted' do
        before do
          post :create, shortcut: { title: nil, url: nil }
        end

        it 'renders back the new action and shows the errors' do
          must_render :new
          assigns(:shortcut).must_be :new_record?
          must_display_form_errors
        end
      end
    end

    describe 'get edit' do
      let(:shortcut) { create(:shortcut, user: authenticated) }

      before do
        get :edit, id: shortcut
      end

      it 'renders the edit action with the requested element' do
        must_render :edit
        assigns(:shortcut).must_equal shortcut
      end
    end

    describe 'patch/put update' do
      let(:shortcut) { create :shortcut, user: authenticated }

      describe 'when valid values are submitted' do
        before do
          patch :update, id: shortcut, shortcut: { title: 'changed title' }
        end

        it 'modifies the tag' do
          assigns(:shortcut).title.must_equal 'changed title'
          must_have_notice 'shortcuts.edit.messages.success'
        end
      end

      describe 'when invalid values are submitted' do
        before do
          put :update, id: shortcut, shortcut: { url: nil, title: nil }
        end

        it 'renders back the edit action and displays the errors' do
          must_render :edit
          assigns(:shortcut).must_equal shortcut
          must_display_form_errors
        end
      end
    end

    describe 'delete destroy' do
      let(:shortcut_to_delete) { create :shortcut, user: authenticated, show_in_menu: false }

      it 'deletes the shortcut, redirects to index and displays a notice' do
        it_deletes_a Shortcut, factory: shortcut_to_delete
        must_redirect_to shortcuts_path
        must_have_notice 'shortcuts.destroy.messages.success'
      end
    end

    describe 'add_to_menu' do
      let(:shortcut) { create :shortcut, user: authenticated, show_in_menu: false }

      before do
        patch :add_to_menu, id: shortcut
      end

      it 'assigns' do
        assigns(:shortcut).must_equal shortcut
        assigns(:shortcut).must_be :show_in_menu
      end
    end

    describe 'remove_from_menu' do
      let(:shortcut) { create :shortcut, user: authenticated, show_in_menu: true }

      before do
        patch :remove_from_menu, id: shortcut
      end

      it 'assigns' do
        assigns(:shortcut).must_equal shortcut
        assigns(:shortcut).wont_be :show_in_menu
      end
    end
  end
end
