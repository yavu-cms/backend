require 'test_helper'

class CoversControllerTest < ActionController::TestCase
  describe CoversController do
    let(:permissions) { %w[ cover/read cover/update/all ] }
    let(:representations) { :all }

    before do
      @client_application = create :client_application
      login permissions: permissions, client_applications: [@client_application], representations: representations
      @controller = CoversController.new
    end

    describe 'Permissions sanity checks' do
      let(:client_application_outside_workspace) { create :client_application }
      let(:representation_outside_workspace) { create :cover_representation, client_application: client_application_outside_workspace }

      describe 'without permissions' do
        let(:permissions) { [] }

        describe 'when trying to get index' do
          it 'must be forbidden' do
            must_be_forbidden -> { get :index }
          end
        end

        describe 'when trying to get edit' do
          it 'must be forbidden' do
            @representation = create :cover_representation, client_application: @client_application
            must_be_forbidden -> { get :edit, client_application_id: @client_application, id: @representation }
            must_be_forbidden -> { get :edit, client_application_id: client_application_outside_workspace, id: representation_outside_workspace }
          end
        end

        describe 'when trying to patch update' do
          it 'must be forbidden' do
            @representation = create :cover_representation, client_application: @client_application
            UniqueAccessWorker.expects(:perform_async).never
            must_be_forbidden -> { patch :update, client_application_id: @client_application, id: @representation }
            must_be_forbidden -> { patch :update, client_application_id: client_application_outside_workspace, id: representation_outside_workspace }
          end
        end
      end

      describe 'when have cover permissions' do
        describe 'outside client application' do
          describe 'when trying to get edit' do
            it 'must be forbidden' do
              must_be_forbidden -> {
                get :edit,
                  client_application_id: client_application_outside_workspace,
                  id: representation_outside_workspace
              }
            end
          end

          describe 'when trying to patch update' do
            it 'must be forbidden' do
              @representation = create :cover_representation, client_application: @client_application
              UniqueAccessWorker.expects(:perform_async).never
              must_be_forbidden -> {
                patch :update,
                  client_application_id: client_application_outside_workspace,
                  id: representation_outside_workspace
              }
            end
          end
        end

        describe 'in same client application but not in "own" representation' do
          # This limit the user workspace only to this representation
          let(:representations) { [ create(:cover_representation, client_application: @client_application) ] }
          let(:representation_outside_workspace) { create :cover_representation, client_application: @client_application }

          before do
            @controller.class.skip_before_action :ensure_draft_exists
          end

          after do
            @controller.class.before_action :ensure_draft_exists
          end

          describe 'when trying to get edit' do
            it 'must be forbidden' do
              must_be_forbidden -> {
                get :edit,
                  client_application_id: @client_application,
                  id: representation_outside_workspace
              }
            end
          end
          describe 'when trying to patch update' do
            it 'must be forbidden' do
              must_be_forbidden -> {
                patch :update,
                  client_application_id: @client_application,
                  id: representation_outside_workspace
              }
            end
          end
        end
      end
    end

    describe 'GET index' do
      describe 'on a regular request' do
        before do
          create_list :cover_representation, 2, client_application: @client_application
        end

        it 'must succeed' do
          get :index
          must_render :index
          assigns(:covers).count.must_equal 2
        end
      end

      describe 'when specifying a sort criterion' do
        before do
          @rep2 = create :cover_representation, name: 'I go second', client_application: @client_application
          @rep4 = create :cover_representation, name: 'Zap! I go last', client_application: @client_application
          @rep1 = create :cover_representation, name: 'I go first!', client_application: @client_application
          @rep3 = create :cover_representation, name: 'Mmm.. I go third', client_application: @client_application
        end

        it 'sorts results using it' do
          get :index, sort: { field: 'name', order: 'asc' }
          assigns(:covers).must_equal [@rep1, @rep2, @rep3, @rep4]
          must_render :index
        end
      end

      describe 'when specifying a page' do
        let(:per_page) { 2 }
        let(:page) { 2 }

        before do
          @rep1 = create :cover_representation, name: 'Rep. 1', client_application: @client_application, created_at: 3.minutes.ago
          @rep2 = create :cover_representation, name: 'Rep. 2', client_application: @client_application, created_at: 2.minutes.ago
          @rep3 = create :cover_representation, name: 'Rep. 3', client_application: @client_application, created_at: 1.minutes.ago
        end

        it 'shows records for that page' do
          get :index, page: page, per_page: per_page
          must_render :index
          assigns(:covers).count.must_equal 1
          assigns(:covers).first.must_equal @rep3
        end
      end

      describe 'when specifying the number of elements per page to show' do
        let(:per_page) { 3 }

        before do
          10.times { create :cover_representation, client_application: @client_application }
        end

        it 'shows at most that number of records' do
          get :index, per_page: per_page
          must_render :index
          assigns(:covers).count.must_equal per_page
        end
      end

      describe 'when filtering results' do
        before do
          @cover_1 = create :cover_representation, name: 'Superb Representation', client_application: @client_application
          @cover_2 = create :cover_representation, name: 'Cool representation', client_application: @client_application
          @cover_3 = create :cover_representation, name: 'The other thing', client_application: @client_application
        end

        it 'show the matching records' do
          get :index, filter: { name: 'representation' }
          must_render :index
          assigns(:covers).sort.must_equal [@cover_1, @cover_2].sort
        end

        it 'ignores bad params' do
          get :index, filter: { name: 'representation', sneaky: 'some value' }
          must_render :index
          assigns(:covers).sort.must_equal [@cover_1, @cover_2].sort
        end
      end

      describe 'when reset is submitted' do
        before do
          get :index, commit: I18n.t('activerecord.actions.shared.clear_filter')
        end

        it 'clears the filters and redirects to index' do
          assigns(:filter).values.must_equal CoversFilter.defaults
          must_redirect_to covers_path
        end
      end
    end

    describe 'edit' do
      describe 'when a draft is not available' do
        before do
          @representation = create :cover_representation, client_application: @client_application
          Representation.any_instance.expects(:trigger_draft_creation).once
          get :edit, client_application_id: @client_application, id: @representation
        end

        it 'redirects to the index page with an alert message' do
          must_redirect_to covers_path
          must_have_alert 'representations.errors.draft_unavailable'
        end
      end

      describe 'when at least one cover component or cover articles component is available' do
        before do
          @representation = create :cover_representation, client_application: @client_application
          @representation.draft = create :representation_draft, client_application: @client_application, representation: @representation
          get :edit, client_application_id: @client_application, id: @representation
        end

        it 'succeeds' do
          must_render :edit
        end
      end

      describe 'when neither cover components nor cover articles components are available' do
        before do
          @representation = create :representation, client_application: @client_application
          Representation.any_instance.expects(:cover?).returns(true).once
          get :edit, client_application_id: @client_application, id: @representation
        end

        after do
          Representation.any_instance.unstub(:cover?)
        end

        it 'redirects to the show action telling the user that the editor is not usable' do
          must_redirect_to covers_path
          must_have_alert 'representations.cover.messages.no_cover_components_available'
        end
      end

      describe 'update' do
        after { UniqueAccessWorker.unstub :perform_async }

        describe 'when the representation is not a cover' do
          before do
            @representation = create :representation, client_application: @client_application
            UniqueAccessWorker.expects(:perform_async).never
            patch :update, client_application_id: @client_application, id: @representation
          end

          it 'redirects to the show action with an alert message' do
            must_redirect_to covers_path
            must_have_alert 'representations.cover.messages.not_cover'
          end
        end

        describe 'when the representation is a cover' do
          let(:json_configuration) { JSON.dump configuration }

          describe 'when a draft is not available' do
            let(:configuration) { {'nevermind' => true} }

            before do
              @representation = create :cover_representation, client_application: @client_application
              Representation.any_instance.expects(:trigger_draft_creation).once
              UniqueAccessWorker.expects(:perform_async).never
              patch :update, client_application_id: @client_application, id: @representation, cover: { configuration: json_configuration, operation: 'apply' }
            end

            it 'redirects to the index page with an alert message' do
              must_redirect_to covers_path
              must_have_alert 'representations.errors.draft_unavailable'
            end
          end

          describe 'when the submitted configuration is valid' do
            describe 'when new modules are added and descriptions are changed' do
              let(:configuration) { {'nevermind' => true} }

              before do
                @representation = create :cover_representation, client_application: @client_application
                @representation.draft = @representation.generate_draft
                RepresentationDraft.any_instance.expects(:cover_from_json).with(json_configuration).returns(true).once
                UniqueAccessWorker.expects(:perform_async).with("/uniqueaccess/representation/#{@representation.draft.to_param}", {user: authenticated.username, saved: true}, 'http://test.host:5000/wsock')
                patch :update, client_application_id: @client_application, id: @representation, cover: { configuration: json_configuration, operation: operation }
              end

              after do
                RepresentationDraft.any_instance.unstub(:cover_from_json)
              end

              describe 'when the operation is "save"' do
                let(:operation) { 'save' }

                it 'updates the cover for the representation and returns to the editor with a success message' do
                  must_redirect_to covers_path
                  must_have_notice 'representations.cover.messages.success'
                end
              end

              describe 'when the operation is "apply"' do
                let(:operation) { 'apply' }

                it 'updates the cover for the representation and goes to the preview page with a success message' do
                  must_redirect_to preview_client_application_representation_path(@client_application, @representation), true
                  must_have_notice 'representations.cover.messages.success'
                end
              end
            end
          end

          describe 'when the submitted configuration is invalid' do
            let(:configuration) { {} }

            before do
              @representation = create :cover_representation, client_application: @client_application
              @representation.draft = @representation.generate_draft
              RepresentationDraft.any_instance.expects(:cover_from_json).with(json_configuration).returns(false).once
              UniqueAccessWorker.expects(:perform_async).never
              patch :update, client_application_id: @client_application, id: @representation, cover: { configuration: json_configuration }
            end

            after do
              RepresentationDraft.any_instance.unstub(:cover_from_json)
            end

            it 'redirects to the cover editor with an alert' do
              must_redirect_to covers_path
              must_have_alert 'representations.cover.messages.wrong_structure'
            end
          end

          describe 'when representation is stale' do
            let(:configuration) { {'nevermind' => true} }
            it 'must fail when draft is stale' do
              representation = create :cover_representation, client_application: @client_application
              draft = representation.draft = representation.generate_draft
              UniqueAccessWorker.expects(:perform_async).never
              BaseRepresentation.find(draft.id).tap do |d|
                d.enable_locking!
                d.touch
              end
              patch :update, client_application_id: @client_application, id: representation, representation: { lock_version: draft.lock_version }, cover: { configuration: json_configuration }
              must_redirect_to covers_path
              must_have_alert 'activerecord.errors.stale_object'
            end
          end
        end
      end
    end
  end
end
