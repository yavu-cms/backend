module('Plugin initialization', {
  setup: function() {
    stub($, 'datepicker');
    stub($.datepicker, 'regional');
    stub($.datepicker, 'setDefaults');
    stub($.fn, 'datepicker');

    this.target = $('#datepicker');
  },
  teardown: function() {
    unstub($.datepicker, 'regional');
    unstub($.datepicker, 'setDefaults');
    unstub($, 'datepicker');
    unstub($.fn, 'datepicker');
  }
});

test('Proper initialization - No options', function() {
  expect(1);

  this.target.backendPlugin('datepicker');

  ok($.fn.datepicker.called(1), 'The datepicker initializer is called.');
});

test('Proper initialization - With trigger', function() {
  expect(5);

  var options = { trigger: '#trigger' },
    trigger = $('#trigger'),
    $fakeWidget = $('<div/>').insertAfter(trigger);

  stub($.fn, 'datepicker', function(opts) {
    if (typeof opts === 'string') {
      if (opts === 'widget') {
        return $fakeWidget;
      } else {
        ok(opts.match(/(show|hide)/), 'The datepicker is toggled.');
      }
    } else {
      ok(opts.hasOwnProperty('showOn') && opts.showOn === 'button', 'The plugin tricks the datepicker into adding the trigger button.');
    }
  });

  this.target.backendPlugin('datepicker', options);

  ok($.fn.datepicker.called(1), 'The datepicker initializer is called.');
  ok($('button.ui-datepicker-trigger').length === 0, 'The default button for the datepicker is removed.');

  trigger.trigger('click');
  trigger.trigger('click');
});


module('Plugin destruction', {
  setup: function() {
    var options = { trigger: '#trigger' };

    this.target = $('#datepicker');

    stub($, 'datepicker');
    stub($.datepicker, 'regional');
    stub($.datepicker, 'setDefaults');
    stub($.fn, 'off');
    stub($.fn, 'datepicker', function(args) {
      if (typeof args === 'string') {
        ok(args === 'destroy', 'The datepicker destroy method is called.');
      }
    });

    this.target.backendPlugin('datepicker', options);
  },
  teardown: function() {
    unstub($.datepicker, 'regional');
    unstub($.datepicker, 'setDefaults');
    unstub($, 'datepicker');
    unstub($.fn, 'datepicker');
  }
});

test('Proper destruction', function() {
  expect(1);

  this.target.backendPlugin('datepicker', 'destroy');
});