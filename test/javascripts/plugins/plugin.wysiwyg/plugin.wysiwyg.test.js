module('Plugin initialization', {
  setup: function() {
    this.target = $('#wysiwyg');
    window.CKEDITOR = {
      config: {
        toolbar_basic: null,
        toolbar: null
      }
    };
  },
  teardown: function() {
    unstub(CKEDITOR, 'replace');
  }
});

test('Proper initialization', function() {
  expect(4);

  var options = {opt1: true};

  stub(CKEDITOR, 'replace', function(name, opts) {
    equal(name, 'wysiwyg', 'The name is not passed as expected');
    deepEqual(opts, options, 'The plugin adds no options for the inner editor.');
  });

  this.target.backendPlugin('wysiwyg', options);

  notEqual(CKEDITOR.config.toolbar_basic, null, 'Toolbar configuration has not changed');
  equal(CKEDITOR.config.toolbar, 'basic', 'Toolbar has not been set to basic');
});


module('Plugin destruction', {
  setup: function() {
    this.target = $('#wysiwyg');

    function Ckeditor_instance() {
      };
    Ckeditor_instance.prototype.destroy = function() {
      ok(true, 'Inner editor destroy() method is called.');
    };

    function Ckeditor() {
      this.instances = { wysiwyg: new Ckeditor_instance() };
    };

    window.CKEDITOR = new Ckeditor();
    window.CKEDITOR.config = {
      toolbar_basic: null,
      toolbar: null
    };
    stub(CKEDITOR, 'replace');

    this.target.backendPlugin('wysiwyg');
  },
  teardown: function() {
    unstub(CKEDITOR, 'replace');
  }
});

test('Proper destruction', function() {
  expect(1);

  this.target.backendPlugin('wysiwyg', 'destroy');
});
