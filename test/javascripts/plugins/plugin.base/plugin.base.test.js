module('Backend plugins - Base - Plugins definition', {
  generateDefinition: function(def, name) {
    return function() {
      return $.backendPlugin(name || 'somePlugin', def);
    };
  },
  teardown: function() {
    $.backendPlugin.clear();
  }
});

test('Plugin definition helper is available', function() {
  ok(typeof $.backendPlugin == 'function', '$.backendPlugin is a function');
});

test('All new plugins must provide a valid definition', function() {
  throws(this.generateDefinition(),              'Plugin definition does not accept an undefined definition');
  throws(this.generateDefinition(null),          'Plugin definition does not accept a null definition');
  throws(this.generateDefinition('stringy'),     'Plugin definition does not accept a stringy definition');
  throws(this.generateDefinition(10.1),          'Plugin definition does not accept a numeric definition');
  throws(this.generateDefinition(function(){}),  'Plugin definition does not accept an empty function definition');
  throws(this.generateDefinition({}),            'Plugin definition does not accept an empty object definition');

  throws(this.generateDefinition({init:'string'}), 'Plugin definition checks that the initializer is a function - invalid case');
  ok(this.generateDefinition({init:function(){}}), 'Plugin definition checks that the initializer is a function - valid case');

  ok(this.generateDefinition(function(){return {init:function(){}};}), 'Plugin definition evaluates a function when received as a definition');
});

test('Plugins cannot be defined more than once', function() {
  function define() {
    var validPluginDefinition = { init: function() {} };

    try {
      return $.backendPlugin('plugin', validPluginDefinition);
    } catch (error) {
      return false;
    }
  }

  ok(define(),  'The first attempt is successful');
  ok(!define(), 'The second attempt is not successful');
});

test('Plugins are defined and made available immediately after their definition', function() {
  expect(1);

  $.backendPlugin('newPlugin', { init: function() {} });

  $('body').backendPlugin('newPlugin');
  ok(true, 'Defined plugin is available')
});


module('Backend plugins - Base - Plugins invocation', {
  setup: function() {
    var expectedResponse = 'Destroyed',
      myPluginDefinition = {
      init: function(options) {
        ok(true, 'Plugin initializer was called');
        $(this).data('options', options);
      },
      destroy: function() {
        ok(true, 'Plugin method "destroy" was called');
        return expectedResponse;
      }
    };

    this.pluginName = 'myPlugin';
    this.element = $('.myPluginElement');
    this.expectedResponse = expectedResponse;

    $.backendPlugin.clear();
    $.backendPlugin(this.pluginName, myPluginDefinition);
  },
  teardown: function() {
    $.backendPlugin.clear();
  }
});

test('Plugin element function is available', function() {
  ok(typeof $.fn.backendPlugin == 'function', '$.fn.backendPlugin is a function');
});

test('Plugin initializer is correctly invoked on plugin instantiation', function() {
  expect(1);

  this.element.backendPlugin(this.pluginName);
});

test('Plugin initializer is given any arguments provided on plugin instantiation', function() {
  expect(2);

  var opts = { some: 'option' };
  this.element.backendPlugin(this.pluginName, opts);

  deepEqual(this.element.data('options'), opts, 'The plugin initializer receives the arguments provided');
});

test('Only defined plugins are available', function() {
  function shouldThrowError() {
    this.element.backendPlugin('not' + this.pluginName);
  }

  throws(shouldThrowError, 'The plugin element function throws an error when invoked with an undefined plugin');
});

test('A plugin method is correctly invoked', function() {
  expect(3);

  this.element.backendPlugin(this.pluginName);
  var response = this.element.backendPlugin(this.pluginName, 'destroy');

  equal(response, this.expectedResponse, 'Plugin method invocation returns whatever the function returns.');
});

module('Backend plugins - Base - All plugins querying', {
  setup: function() {
    this.pluginName = 'myPlugin';
    this.pluginDefinition = {
      init: function() {}
    };

    $.backendPlugin.clear();
    $.backendPlugin(this.pluginName, this.pluginDefinition);
  },
  teardown: function() {
    $.backendPlugin.clear();
  }
});

test('All plugins are available through $.backendPlugin.all()', function() {
  var expected = {};
  expected[this.pluginName] = this.pluginDefinition;
  deepEqual($.backendPlugin.all(), expected, 'All available plugins are accessible');
});

module('Backend plugins - Base - Plugins clearing through $.backendPlugin.clear()', {
  setup: function() {
    this.pluginName = 'myPlugin';
    this.pluginDefinition = {
      init: function() {}
    };

    this.otherPluginName = 'otherPlugin';
    this.otherPluginDefinition = {
      init: function() {}
    };

    $.backendPlugin(this.pluginName, this.pluginDefinition);
    $.backendPlugin(this.otherPluginName, this.otherPluginDefinition);
  }
});

test('Plugins can be cleared', function() {
  var expected = {};
  expected[this.pluginName] = this.pluginDefinition;
  expected[this.otherPluginName] = this.otherPluginDefinition;
  deepEqual($.backendPlugin.all(), expected, 'All available plugins are accessible before clearing them');

  expected = {};
  $.backendPlugin.clear();
  deepEqual($.backendPlugin.all(), expected, 'No plugin is available after clearing them');
});

test('Plugins can be cleared by subset', function() {
  var expected = {};
  expected[this.pluginName] = this.pluginDefinition;
  expected[this.otherPluginName] = this.otherPluginDefinition;
  deepEqual($.backendPlugin.all(), expected, 'All available plugins are accessible before clearing them');

  expected = {};
  expected[this.pluginName] = this.pluginDefinition;
  $.backendPlugin.clear([this.otherPluginName]);
  deepEqual($.backendPlugin.all(), expected, 'Only the specified subset of plugins is cleared');
});