module('Plugin initialization', {
  setup: function() {
    this.target = $('#target');

    this.target.backendPlugin('collapsible');
  }
});

test('Proper initialization', function() {
  var toggle = $('#container .collapsible-toggle');

  ok(toggle.length > 0, 'The toggle is added to the page');
  ok(toggle.nextAll('#target').length == 1, 'The toggle is added as a sibling of the target element');
});


module('Plugin destruction', {
  setup: function() {
    this.target = $('#target');
    this.collapsedClass = 'collapsed';
    this.risenClass = 'collapsed';
    this.pluginClass = 'collapsible';

    this.target.backendPlugin('collapsible');
  }
});

test('Proper destruction', function() {
  this.target.backendPlugin('collapsible', 'destroy');

  var toggle = $('#container .collapsible-toggle');
  ok(toggle.length == 0, 'The toggle is removed after the plugin is destroyed.');

  ok(!this.target.hasClass(this.pluginClass), 'Plugin class is removed from the target element.');
  ok(!this.target.hasClass(this.collapsedClass), 'Collapsed state class is removed from the target element.');
  ok(!this.target.hasClass(this.risenClass), 'Risen state class is removed from the target element.');
});


module('Plugin behavior', {
  setup: function() {
    this.target = $('#target');
    this.opts = {
      classes: {
        collapsed: 'col',
        risen:     'rise'
      },
      labels: {
        collapse: 'Col',
        rise:     'Rise'
      },
      collapseOnInit: true
    };

    this.target.backendPlugin('collapsible', this.opts);

    this.toggle = $('#container .collapsible-toggle');
  },
  teardown: function() {
    this.target.backendPlugin('collapsible', 'destroy');
  }
});

test('Initial state', function() {
  ok(this.target.hasClass(this.opts.classes.collapsed), 'The collapsed initial state of the plugin collapses the target element.');
  ok(this.toggle.html() == this.opts.labels.rise, 'The initial label of the toggle is the correct one, given the initial state of the plugin.');
});

test('Toggle behavior', function() {
  this.toggle.trigger('click');

  ok(this.target.hasClass(this.opts.classes.risen), 'Clicking the toggle rises the target element when it is collapsed.');
  ok(!this.target.hasClass(this.opts.classes.collapsed), 'Clicking the toggle rises the target element when it is collapsed.');
  ok(this.toggle.html() == this.opts.labels.collapse, 'The label for the toggle changes accordingly.');

  this.toggle.trigger('click');

  ok(!this.target.hasClass(this.opts.classes.risen), 'Clicking the toggle collapses the target element when it is risen.');
  ok(this.target.hasClass(this.opts.classes.collapsed), 'Clicking the toggle collapses the target element when it is risen.');
  ok(this.toggle.html() == this.opts.labels.rise, 'The label for the toggle changes accordingly.');
});

test('Manual behavior', function() {
  this.target.backendPlugin('collapsible', 'rise');

  ok(this.target.hasClass(this.opts.classes.risen), 'Manually rising the target element rises it when it is collapsed.');
  ok(!this.target.hasClass(this.opts.classes.collapsed), 'Manually rising the target element rises it when it is collapsed.');
  ok(this.toggle.html() == this.opts.labels.collapse, 'The label for the toggle changes accordingly.');

  this.target.backendPlugin('collapsible', 'collapse');

  ok(!this.target.hasClass(this.opts.classes.risen), 'Manually collapsing the target element collapses it when it is risen.');
  ok(this.target.hasClass(this.opts.classes.collapsed), 'Manually collapsing the target element collapses it when it is risen.');
  ok(this.toggle.html() == this.opts.labels.rise, 'The label for the toggle changes accordingly.');

  this.target.backendPlugin('collapsible', 'toggle');

  ok(this.target.hasClass(this.opts.classes.risen), 'Manually toggling the element rises it back again.');
  ok(!this.target.hasClass(this.opts.classes.collapsed), 'Manually toggling the element rises it back again.');
  ok(this.toggle.html() == this.opts.labels.collapse, 'The label for the toggle changes accordingly.');
});