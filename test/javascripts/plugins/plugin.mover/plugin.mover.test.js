module('Plugin initialization', {
  setup: function() {
    this.target = $('#mover');
  },
  teardown: function() {
    this.target.backendPlugin('mover', 'destroy');
  }
});

test('Proper initialization', function() {
  expect(1);

  this.target.backendPlugin('mover', { destination: '#destination' });

  ok(this.target.parent().is('#destination'), 'The element is not moved to destination');
});


module('Plugin destruction', {
  setup: function() {
    this.target = $('#mover');

    this.target.backendPlugin('mover', { destination: '#destination' });
  },
  teardown: function() {
  }
});

test('Proper destruction', function() {
  expect(1);

  this.target.backendPlugin('mover', 'destroy');

  ok(this.target.parent().is(':not(#destination)'), 'The element is not restored to its original container');
});