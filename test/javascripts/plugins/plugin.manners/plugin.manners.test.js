module('Plugin initialization', {
  setup: function() {
    stub($.fn, 'on', function(eventType, handler) {
      ok(eventType.match(/^click\./), 'The click event is bound.');
    });

    this.target = $('#manners');
  },
  teardown: function() {
    unstub($.fn, 'on');

    this.target.backendPlugin('manners', 'destroy');
  }
});

test('Proper initialization - No options', function() {
  expect(2);

  this.target.backendPlugin('manners');

  ok($.fn.on.called(1), 'An event listener is bound.');
});

module('Plugin behavior', {
  setup: function() {
    $.backendPlugin('initializer', { init: function() {} });

    stub($.fn, 'foundation');

    this.target = $('#manners');
  },
  teardown: function() {
    unstub($.fn, 'foundation');
    unstub($, 'get');

    $.backendPlugin.clear(['initializer']);

    this.target.backendPlugin('manners', 'destroy');
  }
});

test('Click event handling - defaults', function() {
  var expectedUrl = this.target.attr('href');

  this.target.backendPlugin('manners');

  stub($, 'get', function(url, handler) {
    ok(url === expectedUrl, 'The URL is correctly taken.');

    handler();
  });

  this.target.trigger('click');

  ok($.fn.foundation.called(1), 'The reveal dialog is shown.');
});


test('Click event handling - custom URL', function() {
  var expectedUrl = '#some-fancy-url',
    options = { url: expectedUrl };

  this.target.backendPlugin('manners', options);

  stub($, 'get', function(url, handler) {
    ok(url === expectedUrl, 'The URL is correctly taken.');

    handler();
  });

  this.target.trigger('click');

  ok($.fn.foundation.called(1), 'The reveal dialog is shown.');
});


module('Plugin destruction', {
  setup: function() {
    this.target = $('#manners');

    stub($.fn, 'off');

    this.target.backendPlugin('manners');
  },
  teardown: function() {
    unstub($.fn, 'off');
  }
});

test('Proper destruction', function() {
  expect(1);

  this.target.backendPlugin('manners', 'destroy');

  ok($.fn.off.called(1), 'The event listener was unregistered.');
});