module('Plugin initialization', {
  setup: function() {
    var trgt = this.target = $('#livefilter');
    var opts = this.opts = { triggerOn: ['keyup'] };

    stub($.fn, 'on', function(eventType, callback) {
      deepEqual($(this), trgt, 'The plugin registers event listeners on the target element when delegation is not used');
      equal(eventType, 'keyup.livefilter', 'The plugin registers itself as a listener for the specified events');
    });
  },
  teardown: function() {
    unstub($.fn, 'on');
    this.target.backendPlugin('livefilter', 'destroy');
  }
});

test('Proper initialization', function() {
  expect(2);

  this.target.backendPlugin('livefilter');
});


module('Plugin destruction', {
  setup: function() {
    var target = this.target = $('#livefilter');

    this.target.backendPlugin('livefilter');

    stub($.fn, 'off', function(eventType) {
      deepEqual($(this), target, 'Event listeners on the target element are unregistered');
      equal(eventType, '.livefilter', 'The whole plugin namespace is unregistered');
    });
  },
  teardown: function() {
    unstub($.fn, 'off');
  }
});

test('Proper destruction', function() {
  this.target.backendPlugin('livefilter', 'destroy');
});


module('Plugin behavior', {
  setup: function() {
    this.target = $('#livefilter');
  },
  teardown: function() {
    this.target.backendPlugin('livefilter', 'destroy');
  }
});

test('An empty value when triggering the filtering clears (resets) the elements list', function() {
  var options = {
    matchingClass: 'test-match',
    nonMatchingClass: 'test-no-match',
    searchIn: '#options'
  };

  this.target.backendPlugin('livefilter', options);

  var fsHandler = mock();

  this.target.on('filterStart', fsHandler);

  // Fake a value and a keyup event on the target element
  this.target.val('');
  this.target.trigger('keyup');

  ok(fsHandler.called(0), 'Filtering was triggered');
});

test('Manually triggering the filter method performs the search and highlighting', function() {
  var options = {
    matchingClass: 'test-match',
    nonMatchingClass: 'test-no-match',
    searchIn: '#options'
  };

  this.target.backendPlugin('livefilter', options);

  // Manually trigger the filtering
  this.target.backendPlugin('livefilter', 'filter', 'giraffe');

  ok($('#options .test-match').length > 0, 'Matching elements were found');
  ok($('#options .test-no-match').length > 0, 'Non-matching elements were found');
});

test('Only matching elements are marked as matching when filtering', function() {
  var options = {
    matchingClass: 'test-match',
    nonMatchingClass: 'test-no-match',
    searchIn: '#options'
  };

  this.target.backendPlugin('livefilter', options);

  // Manually trigger the filtering
  this.target.backendPlugin('livefilter', 'filter', 'giraffe');

  ok($('#options .test-match').length == 1, 'Just one matching element was found and marked as such');
  ok($('#options .test-no-match').length == $('#options > *').length - 1, 'The appropriate number of non-matching elements was found and marked as such');
});

test('Filtering mechanism is flexible and usable', function() {
  expect(8);

  var options = {
      matchingClass: 'test-match',
      nonMatchingClass: 'test-no-match',
      searchIn: '#options',
      responseDelay: 1
    },
    $options = $('#options'),
    optionsCount = $options.find('> *').length;

  this.target.backendPlugin('livefilter', options);

  // (1) Full matching
  this.target.backendPlugin('livefilter', 'filter', 'lion');

  ok($options.find('.test-match').length == 1, 'Full match filters correctly the elements');
  ok($options.find('.test-no-match').length == optionsCount - 1, 'Full match filters correctly the elements');

  // (2) Partial match, at the beginning
  this.target.backendPlugin('livefilter', 'filter', 'ho');

  ok($options.find('.test-match').length == 2, 'Partial match, at the beginning filters correctly the elements');
  ok($options.find('.test-no-match').length == optionsCount - 2, 'Partial match, at the beginning filters correctly the elements');

  // (2) Partial match, in the middle
  this.target.backendPlugin('livefilter', 'filter', 'ot');

  ok($options.find('.test-match').length == 2, 'Partial match, in the middle filters correctly the elements');
  ok($options.find('.test-no-match').length == optionsCount - 2, 'Partial match, in the middle filters correctly the elements');

  // (4) Partial match, at the end
  this.target.backendPlugin('livefilter', 'filter', 'og');

  ok($options.find('.test-match').length == 3, 'Partial match, at the end filters correctly the elements');
  ok($options.find('.test-no-match').length == optionsCount - 3, 'Partial match, at the end filters correctly the elements');
});

test('Manually triggering the clear method resets the elements list', function() {
  var options = {
    matchingClass: 'test-match',
    nonMatchingClass: 'test-no-match',
    searchIn: '#options'
  };

  this.target.backendPlugin('livefilter', options);

  // Fake some result matching
  $('#options > *').addClass('test-no-match');

  // Manually trigger the clear method
  this.target.backendPlugin('livefilter', 'clear');

  ok($('#options .test-match').length === 0, 'Filtering marks were cleared');
  ok($('#options .test-no-match').length === 0, 'Filtering marks were cleared');
});

test('Filter start and filter end events are triggered when filtering', function() {
  expect(2);

  // Register event listeners
  this.target.on('filterStart.test', function() {
    ok(true, 'Filter start event was emitted');
  });
  this.target.on('filterEnd.test', function() {
    ok(true, 'Filter end event was emitted');
  });

  this.target.backendPlugin('livefilter');

  // Trigger filtering
  this.target.backendPlugin('livefilter', 'filter', 'a query');

  // Unregister event listeners
  this.target.off('.test');
});

test('Filter input is hidden when the result set is empty and hideWhenEmpty == true', function() {
  this.target.backendPlugin('livefilter', {hideWhenEmpty: true});

  ok(this.target.is(':visible'));

  $('#options').empty();
  this.target.backendPlugin('livefilter', 'reload');

  ok(!this.target.is(':visible'));
});

module('Plugin behavior - Event delegation', {
  setup: function() {
    this.target = $('#form');

    this.opts = {
      delegate: '#livefilter'
    };

    stub($.fn, 'on', function(eventType, delegate, callback) {
      equal(delegate, '#livefilter', 'The events are registered using the provided delegate');
    });
  },
  teardown: function() {
    unstub($.fn, 'on');

    this.target.backendPlugin('livefilter', 'destroy');
  }
});

test('Event emitted in the delegate element correctly triggers the filtering', function() {
  expect(1);

  this.target.backendPlugin('livefilter', this.opts);
});