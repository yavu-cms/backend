module('Plugin initialization', {
  setup: function() {
    this.target = $('#mediaelement-preview');
  },
  teardown: function() {
    unstub($.fn, 'mediaelementplayer');
  }
});

test('Proper initialization - No options', function() {
  expect(1);
  stub($.fn, 'mediaelementplayer');

  this.target.backendPlugin('mediaelement');
  console.log($.fn);
  ok($.fn.mediaelementplayer.called(1), 'The mediaelement initializer is called.');
});

test('Proper initialization - With options', function() {
  expect(2);
  var expectedOpts = { enablePluginDebug: true };

  stub($.fn, 'mediaelementplayer', function(opts) {
    deepEqual(opts, expectedOpts, 'the plugin was called and enablePluginDebug settings overwritten by the parameter received');
  });

  this.target.backendPlugin('mediaelement', expectedOpts);

  ok($.fn.mediaelementplayer.called(1), 'The mediaelement initializer is called.');
});
