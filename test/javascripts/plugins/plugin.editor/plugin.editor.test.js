module('Plugin initialization', {
  setup: function() {
    this.target = $('#editor');
    this.opts = { classes: { selected: 'sel', disabled: 'dis' } };

    // Fake an initially-selected item
    this.selected = 'item-3';
    this.target.data('editor-selected-item', this.selected);

    stub($.fn, 'on', function(eventType) {
      var matches = eventType.match(/^(click|ajax(Start|Stop))\..*$/);
      ok(matches, 'Registered listener for event type ' + matches[1]);
    });
    stub($, 'ajax');
  },
  teardown: function() {
    unstub($.fn, 'on');
    unstub($, 'ajax');
  }
});

test('Proper plugin initialization', function() {
  expect(5);

  var selectedItem = this.target.find('#' + this.selected);

  this.target.backendPlugin('editor', this.opts);

  ok(selectedItem.hasClass(this.opts.classes.selected), 'The initially selected value is marked as selected');
  ok($.ajax.called(1), 'The preview action was called to preview the initially selected element');
});


module('Plugin destruction', {
  setup: function() {
    this.target = $('#editor');

    this.target.backendPlugin('editor');

    stub($.fn, 'off', function(eventType) {
      equal(eventType, '.editor', '$.fn.off() was called to unregister the .editor namespace');
    });
  },
  teardown: function() {
    unstub($.fn, 'off');
  }
});

test('Plugin reverts target element to its original state', function() {
  expect(2);

  this.target.backendPlugin('editor', 'destroy');
});


module('Plugin behavior', {
  setup: function() {
    this.target = $('#editor');
    var selected = this.selected = 'item-2';
    var opts = this.opts = {
      previewUrl: '#preview',
      urlParameterName: 'param1',
      requestMethod: 'put',
      classes: { disabled: 'dis', selected: 'sel' }
    };

    stub($, 'ajax', function(url, params) {
      equal(url, opts.previewUrl, 'The ajax request is made to the preview URL');
      ok(params.data.hasOwnProperty(opts.urlParameterName), 'The selected value is sent on the urlParameterName option request parameter');
      equal(params.data[opts.urlParameterName], selected, 'The selected value is correctly sent in the ajax request');
      equal(params.type, opts.requestMethod, 'The ajax request uses the specified method');
    });

    // Register an event listener for the `change` event
    this.target.on('change.editor', function(e, value) {
      equal(value, selected, 'The change event is emitted with the new value.');
    });

    this.target.backendPlugin('editor', this.opts);
  },
  teardown: function() {
    this.target.backendPlugin('editor', 'destroy');

    unstub($, 'ajax');
  }
});

test('Preview behavior', function() {
  expect(4);

  this.target.backendPlugin('editor', 'preview', this.selected);
});

test('Selected getter method behavior', function() {
  expect(2);

  var selectedValue = this.target.backendPlugin('editor', 'selected');
  equal(typeof selectedValue, 'undefined', 'There is no initially selected item');

  // Mock element selection
  this.target.data('editor-selected-item', this.selected);

  selectedValue = this.target.backendPlugin('editor', 'selected');
  equal(selectedValue, this.selected, 'The selected value is returned correctly after an item has been selected');
});

test('Selected setter method behavior', function() {
  expect(7);

  var selectedItem = this.target.find('#' + this.selected),
    selectedValue;

  selectedValue = this.target.backendPlugin('editor', 'selected');
  equal(typeof selectedValue, 'undefined', 'There is no initially selected item');

  // Use the setter method
  this.target.backendPlugin('editor', 'selected', this.selected);

  selectedValue = this.target.backendPlugin('editor', 'selected');
  equal(selectedValue, this.selected, 'The selected value is returned correctly after an item has been selected');
  ok(selectedItem.hasClass(this.opts.classes.selected), 'The selected item is marked with the `selected` class');


  // Select the same element again - it should not be deselected
  this.target.backendPlugin('editor', 'selected', this.selected);

  selectedValue = this.target.backendPlugin('editor', 'selected');
  equal(selectedValue, this.selected, 'The selected value is not deselected when selected two times in a row');
  ok(selectedItem.hasClass(this.opts.classes.selected), 'The selected item is still marked with the `selected` class when selected two times in a row');
});

test('Select method behavior', function() {
  expect(7);

  var selectedItem = this.target.find('#' + this.selected),
    selectedValue;

  // Trigger the click event on the selected item
  selectedItem.trigger('click');

  selectedValue = this.target.backendPlugin('editor', 'selected');
  equal(selectedValue, this.selected, 'The selected value is returned correctly after an item has been selected');
  ok(selectedItem.hasClass(this.opts.classes.selected), 'The selected item is marked with the `selected` class');
});

test('Enable/Disable detail behavior', function() {
  expect(2);

  var detail = this.target.find('#detail');

  // Trigger the disable detail method through an ajaxStart event
  this.target.trigger('ajaxStart');
  ok(detail.hasClass(this.opts.classes.disabled), 'The detail pane is disabled on an ajaxStart event');

  // Trigger the enable detail method through an ajaxStop event
  this.target.trigger('ajaxStop');
  ok(!detail.hasClass(this.opts.classes.disabled), 'The detail pane is enabled on an ajaxStop event');
});