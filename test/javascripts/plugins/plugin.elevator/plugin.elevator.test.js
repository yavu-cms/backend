module('Plugin initialization', {
  setup: function() {
    var anchor = this.anchor = $('#anchor');
    this.opts = { target: '#container' };
    var container = $(this.opts.target);

    stub($.fn, 'on', function(eventType) {
      if (eventType.match(/^click\./)) {
        deepEqual($(this), anchor, 'click event handler is properly attached to the anchor element');
      } else if (eventType.match(/^scroll\./)) {
        deepEqual($(this), container, 'scroll event handler is properly attached to the target element');
      } 
    });

    stub($.fn, 'trigger', function(eventType) {
      equal(eventType, 'scroll', 'Scroll event was triggered to setup the initial state');
    });
  },
  teardown: function() {
    unstub($.fn, 'on');
    unstub($.fn, 'trigger');

    this.anchor.backendPlugin('elevator', 'destroy');
  }
});

test('Proper plugin initialization', function() {
  expect(3);

  this.anchor.backendPlugin('elevator', this.opts);
});


module('Plugin destruction', {
  setup: function() {
    this.anchor = $('#anchor');
    this.opts = { target: '#container' };

    this.anchor.backendPlugin('elevator', this.opts);

    stub($.fn, 'off', function(eventType) {
      equal(eventType, '.elevator', '$.fn.off() was called to unregister the .elevator namespace');
    });
  },
  teardown: function() {
    unstub($.fn, 'off');
  }
});

test('Plugin reverts the element to its original state', function() {
  expect(2);

  this.anchor.backendPlugin('elevator', 'destroy');
});


module('Plugin behavior', {
  setup: function() {
    this.anchor = $('#anchor');
    var opts = this.opts = {
      target: '#container',
      fadeDuration: 10,
      threshold: 50,
      triggerOnInit: false
    };
    this.container = $(this.opts.target);

    stub($.fn, 'fadeIn', function(duration) {
      equal(duration, opts.fadeDuration, 'The correct duration is provided to the fadeIn method');
    });
    stub($.fn, 'fadeOut', function(duration) {
      equal(duration, opts.fadeDuration, 'The correct duration is provided to the fadeOut method');
    });
  },
  teardown: function() {
    this.anchor.backendPlugin('elevator', 'destroy');

    unstub($.fn, 'fadeIn');
    unstub($.fn, 'fadeOut');
  }
});

test('Anchor visibility toggling', function() {
  expect(2);

  var opts = this.opts;

  this.anchor.backendPlugin('elevator', this.opts);

  // Force a fadeOut of the anchor
  stub($.fn, 'scrollTop', function() {
    return opts.threshold / 2;
  });
  this.container.trigger('scroll');
  unstub($.fn, 'scrollTop');

  // Force a fadeIn of the anchor
  stub($.fn, 'scrollTop', function() {
    return opts.threshold * 2;
  });
  this.container.trigger('scroll');
  unstub($.fn, 'scrollTop');
});