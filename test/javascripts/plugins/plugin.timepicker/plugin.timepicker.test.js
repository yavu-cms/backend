module('Plugin initialization', {
  setup: function() {
    stub($, 'timepicker');
    stub($.timepicker, 'regional');
    stub($.timepicker, 'setDefaults');
    stub($.fn, 'timepicker');

    this.target = $('#timepicker');
  },
  teardown: function() {
    unstub($.timepicker, 'regional');
    unstub($.timepicker, 'setDefaults');
    unstub($, 'timepicker');
    unstub($.fn, 'timepicker');
  }
});

test('Proper initialization - No options', function() {
  expect(1);

  this.target.backendPlugin('timepicker');

  ok($.fn.timepicker.called(1), 'The timepicker initializer is called.');
});

test('Proper initialization - With trigger', function() {
  expect(4);

  var options = { trigger: '#trigger' },
    trigger = $('#trigger'),
    $fakeWidget = $('<div/>').insertAfter(trigger);

  stub($.fn, 'timepicker', function(opts) {
    if (typeof opts === 'string') {
      if (opts === 'widget') {
        return $fakeWidget;
      } else {
        ok(opts.match(/(show|hide)/), 'The timepicker is toggled.');
      }
    }
  });

  this.target.backendPlugin('timepicker', options);

  ok($.fn.timepicker.called(1), 'The timepicker initializer is called.');
  ok($('button.ui-timepicker-trigger').length === 0, 'The default button for the timepicker is removed.');

  trigger.trigger('click');
  trigger.trigger('click');
});


module('Plugin destruction', {
  setup: function() {
    var options = { trigger: '#trigger' };

    this.target = $('#timepicker');

    stub($, 'timepicker');
    stub($.timepicker, 'regional');
    stub($.timepicker, 'setDefaults');
    stub($.fn, 'off');
    stub($.fn, 'timepicker', function(args) {
      if (typeof args === 'string') {
        ok(args === 'destroy', 'The timepicker destroy method is called.');
      }
    });

    this.target.backendPlugin('timepicker', options);
  },
  teardown: function() {
    unstub($.timepicker, 'regional');
    unstub($.timepicker, 'setDefaults');
    unstub($, 'timepicker');
    unstub($.fn, 'timepicker');
  }
});

test('Proper destruction', function() {
  expect(1);

  this.target.backendPlugin('timepicker', 'destroy');
});
