module('Plugin initialization', {
  setup: function() {
    var simplePlugin = {
        init: function() {
          ok(true, 'simplePlugin#init was called');
        },
        destroy: function() {
        }
      },
      complexPlugin  = {
        init: function(opts) {
          ok(true, 'complexPlugin#init was called');
          ok(!!opts, 'complexPlugin#init received a non-null argument');
        },
        destroy: function() {}
      };

    $.backendPlugin.clear(['simplePlugin', 'complexPlugin']);
    $.backendPlugin('simplePlugin', simplePlugin);
    $.backendPlugin('complexPlugin', complexPlugin);

    this.container = $('.container');
  }
});

test('Init method is correctly invoked on all child elements', function() {
  expect(3);

  this.container.backendPlugin('initializer');
});


module('Plugin destruction', {
  setup: function() {
    var simplePlugin = { init: function() {}, destroy: function() { ok(true, 'simplePlugin#destroy was called'); } },
      complexPlugin  = { init: function() {}, destroy: function() { ok(true, 'complexPlugin#destroy was called'); } };

    $.backendPlugin.clear(['simplePlugin', 'complexPlugin']);
    $.backendPlugin('simplePlugin', simplePlugin);
    $.backendPlugin('complexPlugin', complexPlugin);

    this.container = $('.container');

    this.container.backendPlugin('initializer');
  }
});

test('Destroy method is correctly invoked on all initialized plugins', function() {
  expect(2);

  this.container.backendPlugin('initializer', 'destroy');
});