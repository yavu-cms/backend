module('Plugin initialization', {
  setup: function() {
    this.target = $('#indicator');
    this.opts   = {
      hideOnInit: true,
      bindTo: {
        show:   ['showTrigger'],
        hide:   ['hideTrigger'],
        toggle: ['toggleTrigger1', 'toggleTrigger2']
      }
    };

    stub($.fn, 'on');
    stub($.fn, 'hide');
    stub($.fn, 'show');
    stub($.fn, 'toggle');
  },
  teardown: function() {
    unstub($.fn, 'on');
    unstub($.fn, 'hide');
    unstub($.fn, 'show');
    unstub($.fn, 'toggle');
  }
});

test('Proper plugin initialization', function() {
  this.target.backendPlugin('indicators', this.opts);

  ok($.fn.on.called(4), 'All event listeners are registered on init');
  ok($.fn.hide.called(1), 'The target element is hidden at start by default');
  ok($.fn.show.called(0), 'The target element is not shown at start by default');
});


module('Plugin destruction', {
  setup: function() {
    this.target = $('#indicator');

    stub($.fn, 'on');
    stub($.fn, 'off');
    stub($.fn, 'hide');

    this.target.backendPlugin('indicators');
  },
  teardown: function() {
    unstub($.fn, 'on');
    unstub($.fn, 'off');
    unstub($.fn, 'hide');
  }
});

test('Plugin reverts target element to its original state', function() {
  this.target.backendPlugin('indicators', 'destroy');

  ok($.fn.off.called(), '$.fn.off() is called to unregister event listeners');
});


module('Plugin behavior', {
  setup: function() {
    this.opts = {
      bindTo: {
        show:   ['ajaxStart'],
        hide:   ['ajaxStop'],
        toggle: ['click']
      },
      hideOnInit: false,
      context: '#container'
    };

    stub($.fn, 'show');
    stub($.fn, 'hide');
    stub($.fn, 'toggle');

    this.target = $('#indicator');
    this.context = $(this.opts.context);

    this.target.backendPlugin('indicators', this.opts);
  },
  teardown: function() {
    this.target.backendPlugin('indicators', 'destroy');

    unstub($.fn, 'show');
    unstub($.fn, 'hide');
    unstub($.fn, 'toggle');
  }
});

test('Visibility togglers are correctly invoked when the events are triggered.', function() {
  this.context.trigger('ajaxStart');
  ok($.fn.show.called(1), 'A show-bound event triggers the show method.');

  this.context.trigger('ajaxStop');
  ok($.fn.hide.called(1), 'A hide-bound event triggers the hide method.');

  this.context.trigger('click');
  ok($.fn.toggle.called(1), 'A toggle-bound event triggers the toggle method.');
});