module('Plugin initialization', {
  setup: function() {
    this.target = $('#dropzone');

    stub($.fn, 'dropzone');
    stub(window, 'Dropzone');
    stub(window.Dropzone, 'forElement', function() {
      var myStub = {};

      stub(myStub, 'disable');

      return myStub;
    });
  },
  teardown: function() {
    this.target.backendPlugin('dropzone', 'destroy');

    unstub($.fn, 'dropzone');
    unstub(window, 'Dropzone');
  }
});

test('Proper initialization', function() {
  expect(2);

  this.target.backendPlugin('dropzone');

  ok($.fn.dropzone.called(1), 'The Dropdown initializer was called');
  ok(this.target.find('.message').length === 1, 'The message was added');
});


module('Plugin destruction', {
  setup: function() {
    this.target = $('#dropzone');

    stub($.fn, 'dropzone');
    stub(window, 'Dropzone');
    stub(window.Dropzone, 'forElement', function() {
      var myStub = {};

      stub(myStub, 'disable', function() {
        ok(true, 'Dropzone was disabled');
      });

      return myStub;
    });

    this.target.backendPlugin('dropzone');
  },
  teardown: function() {
    unstub($.fn, 'dropzone');
    unstub(window, 'Dropzone');
  }
});

test('Proper destruction', function() {
  expect(1);

  this.target.backendPlugin('dropzone', 'destroy');
});