/**
 * Stub logic for QUnit tests.
 *
 * @author Nahuel Cuesta Luengo <ncuesta@cespi.unlp.edu.ar>
 */

(function(global) {
  'use strict';

  // Wrapper for functions that adds some logic for better and easier assertions.
  // Note that the fn argument is optional and will default to a no-op function.
  // Usage example:
  //   stb = new Stub(function() { /* ... */ });
  //   // Asserting calls
  //   stb.called()  // => TRUE if the stub has been called at least once.
  //   stb.called(2) // => TRUE if the stub has been called exactly twice.
  function Stub(fn, original) {
    var _cld = 0;

    fn = fn || function() {};

    function _stub() {
      _cld++;

      return fn.apply(this, arguments);
    }

    // Calls assertions.
    _stub.called = function(n) {
      if (typeof n == 'number') {
        return _cld === n;
      } else {
        return _cld > 0;
      }
    };
    _stub.calledAtLeast = function(n) {
      return _cld >= n;
    };

    // Original member
    _stub.original = original;

    return _stub;
  }

  Stub.constructor = Stub;

  // Stub an attribute in an object.
  // Usage example:
  //   stub(jQuery, 'ajax', function() { /* ... */ });
  //     // => Will stub jQuery.ajax with the given function
  global.stub = function(object, attribute, stub) {
    object[attribute] = new Stub(stub, object[attribute]);

    return object[attribute];
  };

  // Un-stub a previously stubbed attribute in an object.
  // Usage example:
  //   unstub(jQuery, 'ajax');
  //     // => Will restore jQuery.ajax to its original behavior
  global.unstub = function(object, attribute) {
    object[attribute] = object[attribute].original;

    return object[attribute];
  };

  // Create a mock for a function.
  // Usage example:
  //   var fn = mock();
  //     // => Will return a mock function of type Stub (see above)
  global.mock = function() {
    return new Stub(function() {});
  };

})(window);