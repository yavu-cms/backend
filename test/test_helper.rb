ENV['RAILS_ENV'] = 'test'

require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require 'factory_girl'
require 'minitest/spec'
require 'action_view/test_case'
require 'sidekiq/testing'
# Custom test helper methods
require_relative 'support/mini_test_helpers'
require_relative 'support/factory_girl_helpers'
require_relative 'support/database_cleaner'

FactoryGirl.find_definitions

class ActiveSupport::TestCase
  ActiveRecord::Migration.check_pending!

  # MiniTest integration
  class << self
    remove_method :describe
  end
  extend MiniTest::Spec::DSL
  include MiniTestHelpers

  # Factory Girl configuration
  FactoryGirl::SyntaxRunner.send :include, FactoryGirlHelpers
  # Factory Girl integration
  include FactoryGirl::Syntax::Methods

  # Tell +MiniTest::Spec+ to use +ActiveSupport::TestCase+ when describing
  # an +ActiveRecord+ model
  register_spec_type self do |desc|
    desc < ActiveRecord::Base if desc.is_a? Class
  end

  Sidekiq::Testing.fake!

  # Results reporting configuration
  if ENV['CI']
    reporters = [MiniTest::Reporters::SpecReporter.new]
    reporters << MiniTest::Reporters::JUnitReporter.new if ENV['JUNIT']
  else
    reporters = MiniTest::Reporters::ProgressReporter.new
  end
  MiniTest::Reporters.use! reporters
end

class ActionController::TestCase
  include Devise::TestHelpers
end
