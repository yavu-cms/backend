require_relative 'common_helpers'

# Helper methods specific to MiniTest
module MiniTestHelpers
  include CommonHelpers

  def login(user = nil, permissions: [], sections: [], client_applications: [], representations: [])
    user ||= create(:user)
    set_user_permissions(user, permissions) unless permissions.blank?
    sections, client_applications, representations = set_global_workspace!(user, sections, client_applications, representations)
    user.add_to_workspace sections + client_applications + representations
    sign_in(@_authenticated_user = user)
  end

  def set_user_permissions(user, permissions)
    user.roles = create_list(:role, 1, name: 'automatic role', permissions: permissions)
  end

  def must_be_forbidden(request_block)
    request_block.must_raise CanCan::AccessDenied
  end

  def authenticated
    @_authenticated_user
  end

  def create_route_with_component(route: route_factory, component: component)
    component_configuration = build(:component_configuration, component: component)
    representation = build(:representation_with_component, component_configuration: component_configuration)
    create(route, representation: representation)
  end

  def db_alteration_procedure(op, diff, model, factory, options, session)
    sym = model.to_s.underscore.to_sym
    factory ||= sym
    case op
      when :destroy
        if factory.is_a? model
          object_to_delete = factory
        else
          object_to_delete = create factory
        end
        count = model.count
        options[:id] = object_to_delete
        delete :destroy, options, session
      when :create
        count = model.count
        options = { sym => attributes_for(factory) }.deep_merge(options)
        post :create, options, session
      else
        raise "Unknown db alter operation: #{op.inspect}"
    end
    expected_count = [count + diff, 0].max
    model.count.must_equal expected_count
  end

  # Assertion for controller tests that checks if a POST request
  # to the +create+ action for a given +model+ actually increases
  # in one the count of objects for that model.
  #
  # === Examples
  #
  #   it 'adds a Supplement' do
  #     it_adds_a Supplement
  #   end
  #
  #   it 'adds an Edition' do
  #     it_adds_an Edition, factory: :edition_for_tomorrow
  #   end
  def it_adds_a(model, factory: nil, options: {}, session: {})
    db_alteration_procedure :create, 1, model, factory, options, session
  end
  alias :it_adds_an :it_adds_a

  # Negative assertion
  def it_doesnt_add_a(model, factory: nil, options: {}, session: {})
    db_alteration_procedure :create, 0, model, factory, options, session
  end
  alias :it_doesnt_add_an :it_doesnt_add_a

  # Assertion for controller tests that checks if a DELETE request
  # to the +destroy+ action for a given +model+ actually decreases
  # in one the count of objects for that model.
  #
  # === Examples
  #
  #   it 'deletes a Tag' do
  #     it_deletes_a Tag
  #   end
  #
  #   it 'deletes an Article' do
  #     it_deletes_an Article, factory: :article_with_title
  #   end
  def it_deletes_a(model, factory: nil, options: {}, session: {})
    db_alteration_procedure :destroy, -1, model, factory, options, session
  end
  alias :it_deletes_an :it_deletes_a

  # Negative assertion
  def it_doesnt_delete_a(model, factory: nil, options: {}, session: {})
    db_alteration_procedure :destroy, 0, model, factory, options, session
  end
  alias :it_doesnt_delete_an :it_doesnt_delete_a

  def must_redirect_to(url, allow_query_params = false)
    [200, 302].must_include response.status
    response.location.must_match %r(#{url}#{'\\?.*' if allow_query_params}\z)
  end

  # Assertion for controller tests that checks if the last request
  # succeeded and rendered the expected template.
  # Parameters for this method are provided as received to the
  # +assert_template+ assertion.
  #
  # === Examples
  #
  #   it 'renders the index view' do
  #     must_render :index
  #   end
  #
  # @see TemplateAssertions#assert_template
  def must_render(options = {}, message = nil)
    must_succeed
    assert_template options, message
  end

  # Assertion to check if the form errors are being displayed on the page.
  #
  # === Examples
  #
  #   it 'fails to validate' do
  #     must_display_form_errors
  #   end
  def must_display_form_errors
    assert_select '#error_explanation .error-msg'
  end

  # Assertion to check if a flash message is set for a type with a
  # specific message. Note that +message+ should be the i18n key that
  # will be used for the message and that this helper will automatically
  # translate it using I18n#translate to check if it is correctly set.
  #
  # === Examples
  #
  #   it 'sets a nice notice' do
  #     must_have_flash_message :notice, 'articles.clone.notice'
  #   end
  def must_have_flash_message(type, message, placeholders)
    flash[type].must_equal I18n.translate(message, placeholders)
  end

  # Handy method to check if a notice flash message is set.
  #
  # === Examples
  #
  #   it 'sets a nice notice' do
  #     must_have_notice 'articles.clone.notice'
  #   end
  #
  #   # Or with some placeholders for translation:
  #
  #   it 'sets a nice notice' do
  #     must_have_notice 'articles.clone.notice', article_name: article.name
  #   end
  def must_have_notice(message, placeholders = {})
    must_have_flash_message :notice, message, placeholders
  end

  # Handy method to check if an info flash message is set.
  #
  # === Examples
  #
  #   it 'sets a great info' do
  #     must_have_info 'representations.cover.info'
  #   end
  #
  #   # Or with some placeholders for translation:
  #
  #   it 'sets a great info' do
  #     must_have_info 'representations.cover.info', representation_name: representation.name
  #   end
  def must_have_info(message, placeholders = {})
    must_have_flash_message :info, message, placeholders
  end

  # Handy method to check if an alert flash message is set.
  #
  # === Examples
  #
  #   it 'sets a horrible alert' do
  #     must_have_alert 'articles.clone.alert'
  #   end
  #
  #   # Or with some placeholders for translation:
  #
  #   it 'sets a horrible alert' do
  #     must_have_alert 'articles.clone.alert', article_name: article.name
  #   end
  def must_have_alert(message, placeholders = {})
    must_have_flash_message :alert, message, placeholders
  end

  def must_succeed
    assert_response :success
    # TO BE ADDED SOON ~.~
    # assert_select '.translation_missing', false, 'Found missing translations'
  end

  # Method to mock a permission over a subject. Tells the application that the
  # user +can+ perform +action+ on +subject+.
  #
  # === Examples
  #
  #   before do
  #     pretend_user_can(:read, article)
  #   end
  #   it 'succeeds in reading the article' do
  #     user.can?(:read, article).must_equal true
  #   end
  def pretend_user_can(action, subject, ability = nil)
    mock_permission(:can, action, subject, ability)
  end

  # Method to mock a negative permission over a subject. Tells the
  # application that the user +cannot+ perform +action+ on +subject+.
  #
  # === Examples
  #
  #   before do
  #     pretend_user_cannot(:read, article)
  #   end
  #   it 'fails to read the article' do
  #     user.can?(:read, article).must_equal false
  #   end
  def pretend_user_cannot(action, subject, ability = nil)
    mock_permission(:cannot, action, subject, ability)
  end

  # Method to mock the inclusion of a +resource_or_resources+ in the currently logged
  # in user workspace. This enables mocking the actual workspace of the user when testing.
  # NOTE: This method requires access to the currently logged in user instance
  # via the +authenticated+ or +current_user+ method in its scope.
  #
  # === Examples
  #
  #   before do
  #     pretend_user_workspace_includes(my_client_application)
  #   end
  #   it "has the client application in the user's workspace" do
  #     authenticated.workspace.must_include my_client_application
  #   end
  def pretend_user_workspace_includes(resource_or_resources)
    (authenticated || current_user).add_to_workspace resource_or_resources
  end

  # Assertion to check if the currently logged in user is able to
  # perform +action+ on +subject+.
  # NOTE: This method requires access to the current Ability instance
  # via an +ability+ method or variable in its scope.
  #
  # === Examples
  #
  #   let(:ability) { current_ability }
  #   it 'can read magazines' do
  #     must_be_able_to :read, Magazine
  #   end
  def must_be_able_to(action, subject)
    ability.can?(action, subject).must_equal true, "Expected user to be able to #{action} #{subject.inspect}, but instead wasn't."
  end

  # Assertion to check if the currently logged in user is unable to
  # perform +action+ on +subject+.
  # NOTE: This method requires access to the current Ability instance
  # via an +ability+ method or variable in its scope.
  #
  # === Examples
  #
  #   let(:ability) { current_ability }
  #   it 'cannot destroy sections' do
  #     wont_be_able_to :destroy, Section
  #   end
  def wont_be_able_to(action, subject)
    ability.can?(action, subject).must_equal false, "Expected user not to be able to #{action} #{subject.inspect}, but instead was."
  end

  protected

  # Support method for +pretend_user_*+ helpers
  def mock_permission(predicate, action, subject, ability = nil)
    if ability.nil?
      @controller.instance_variable_set(:@current_user, authenticated) unless @controller.instance_variable_get(:@current_user).present?
      ability = @controller.try(:current_ability)
      raise "Unable to retrieve current ability while trying to mock permission #{predicate} #{action.inspect} for subject #{subject.inspect}" if ability.nil?
    end
    ability.public_send predicate, action, subject
  end

  # Auxiliar method for +login_*+ helpers
  def set_global_workspace!(user, sections, client_applications, representations)
    if sections == :all
      user.all_sections = true
      sections = []
    end
    if client_applications == :all
      user.all_client_applications = true
      client_applications = []
    end
    if representations == :all
      user.all_representations = true
      representations = []
    end
    user.save!
    [ sections, client_applications, representations ]
  end
end
