require 'test_helper'

# Base class for Permissions tests. This provides a way for testing *Permissions
# classes in a simple and clean way.
#
# === Example
#
#   class MyPermissionsTest < PermissionsTestCase
#     describe 'read' do
#       it 'allows reading :stuff' do
#         must_be_able_to :read, :stuff
#       end
#     end
#   end
#
# For more thorough (and real life) examples, you may want to refer to:
# @see TagPermissionsTest
# @see SupplementPermissionsTest
class PermissionsTestCase < ActiveSupport::TestCase
  class LocalPermission < Permission; end

  let(:permissions) { [] }
  let(:current_user) { User.new }
  let(:ability) do
    stubbed_user = current_user.tap { |u| u.stubs(:permissions).returns(permissions) }
    Ability.new(stubbed_user)
  end

  before do
    Ability.stubs(:permissions_proxy).returns(LocalPermission)
  end
end