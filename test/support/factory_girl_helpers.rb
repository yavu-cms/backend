require_relative 'common_helpers'

# Helper methods specific to FactoryGirl
module FactoryGirlHelpers
  include CommonHelpers
end