require 'database_cleaner'

DatabaseCleaner.strategy = :transaction

module AutocleanData
  def before_setup
    super
    DatabaseCleaner.start
  end

  def after_teardown
    super
    DatabaseCleaner.clean
  end
end

class MiniTest::Unit::TestCase
  include AutocleanData
end
