# Common helper methods
module CommonHelpers
  def upload_test_file(name)
    path = Rails.root.join(*%W(test fixtures #{name}))
    Rack::Test::UploadedFile.new(path)
  end

  def t_action(scope, action)
    I18n.t action, scope: [:activerecord, :actions, scope], default: I18n.t(action , scope: [:activerecord, :actions, :shared])
  end
end