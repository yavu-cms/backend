require 'test_helper'
require 'mocha/setup'

module Accounting
  class MostVisitedArticlesIntegrationTest < ActiveSupport::TestCase
    describe Accounting::MostVisitedArticles do
      before do
        MostVisitedArticles.delete_all
      end

      describe '.last_period' do
        let(:supplement) { create :supplement }
        let(:sec_A) { create(:section, name: 'A', supplement: supplement) }
        let(:sec_B) { create(:section, name: 'B', supplement: supplement) }
        let(:sec_C) { create(:section, name: 'C', supplement: supplement) }
        let(:artsA) { create_list :article, 3, section: sec_A, supplement: supplement }
        let(:artsB) { create_list :article, 3, section: sec_B, supplement: supplement }
        let(:artsC) { create_list :article, 3, section: sec_C, supplement: supplement }

        let(:articles) { artsA + artsB + artsC }
        let(:most_visited) { artsA.first }

        before do
          Accounting::MostVisitedArticles.destroy_all
          Article.any_instance.stubs(:date).returns(DateTime.current)
          artsA.first.visit!(8)
          artsA.each { |a| a.visit!(8) }
          artsB.each { |a| a.visit!(4) }
          artsC.each { |a| a.visit!(2) }
        end

        after do
          Article.any_instance.unstub(:date)
        end

        describe 'when section is given' do
          it 'must return the most visited articles where all records belong to the given section' do
            last_period = Accounting::MostVisitedArticles.last_period(
              DateTime.current.beginning_of_day..DateTime.current.end_of_day,
              section: Section.find(most_visited.section.id))
            last_period.wont_be :empty?
            last_period.all? { |a| a.section == most_visited.section }.must_equal true
          end

          it 'must contain as first record the most visited article' do
            last_period = Accounting::MostVisitedArticles.last_period(
              DateTime.current.beginning_of_day..DateTime.current.end_of_day,
              section: Section.find(most_visited.section.id))
            last_period.wont_be :empty?
            last_period.first.must_equal most_visited
          end
        end

        describe 'when section is not given' do
          it 'must return the 3 most visited articles' do
            last_period = Accounting::MostVisitedArticles.last_period(
              DateTime.current.beginning_of_day..DateTime.current.end_of_day, 3)
            last_period.size.must_equal 3
            last_period.first.must_equal most_visited
          end

          it 'does not return the most visited articles when it is from yesterday' do
            skip
          end
        end
      end
    end
  end
end
