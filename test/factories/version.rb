FactoryGirl.define do
  factory :version, class: Version do
    sequence(:item_id)
    item_type 'Article'
    event     'create'
  end
end

