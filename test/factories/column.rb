FactoryGirl.define do
  factory :column, class: Column do
    sequence(:order,1)
    row

    factory :column_with_comp_configs do
      ignore do
        cc_counts 1
        cover_cc_counts 0
      end
      after(:build) do |column, evaluator|
        cc = FactoryGirl.build_list(
          :component_configuration,
          evaluator.cc_counts,
          column: column,
        )
        cover_cc = FactoryGirl.build_list(
          :cover_component_configuration,
          evaluator.cover_cc_counts,
          column: column,
        )
        column.component_configurations = cc + cover_cc
      end
    end

    factory :column_with_cover_comp_configs do
      ignore do
        cc_counts 1
      end
      after(:build) do |column, evaluator|
        column.component_configurations = FactoryGirl.build_list(
          :component_configuration,
          evaluator.cc_counts,
          view: FactoryGirl.build(:view_without_placeholders)
        )
      end
    end

    factory :app1_representation1_row1_col1 do
      order 1
      association :row, factory: :app1_representation1_row1, strategy: :build
    end

    factory :app1_representation1_row1_col2 do
      order 2
      association :row, factory: :app1_representation1_row1, strategy: :build
    end

    factory :app1_representation1_row2_col1 do
      order 1
      association :row, factory: :app1_representation1_row2, strategy: :build
    end

    ### For Draft

    factory :app1_representation1_with_draft_row1_col1 do
      order 1
      association :row, factory: :app1_representation1_with_draft_row1, strategy: :build
    end

    factory :app1_representation1_with_draft_row1_col2 do
      order 2
      association :row, factory: :app1_representation1_with_draft_row1, strategy: :build
    end

    factory :app1_representation1_with_draft_row2_col1 do
      order 1
      association :row, factory: :app1_representation1_with_draft_row2, strategy: :build
    end
  end
end
