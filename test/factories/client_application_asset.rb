FactoryGirl.define do
  factory :client_application_asset, aliases: [:client_application_asset_not_editable] do
    client_application
    file { upload_test_file('image_1.jpg')} 
  end

  factory :client_application_asset_editable, class: ClientApplicationAsset, aliases: [:client_application_asset_javascript] do
    client_application
    file { upload_test_file('javascript.js')} 
  end

  factory :client_application_asset_stylesheet, class: ClientApplicationAsset do
    client_application
    file { upload_test_file('stylesheet.css')} 
  end

end
