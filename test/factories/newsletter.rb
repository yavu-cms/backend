FactoryGirl.define do
  factory :newsletter do
    sequence(:name) { |n| "A cool newsletter #{n}" }
    frequency '*/1 * * * *'
    expiration 30
    generator '<div>Hello</div>'
    context '@a = 1'
    enabled true
    association :client_application, strategy: :build
    after(:build) do |nl|
      nl.stubs(:syntax).returns(true)
      nl.class.skip_callback(:save, :after, :program_worker)
    end
  end
end
