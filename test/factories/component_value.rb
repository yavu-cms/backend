FactoryGirl.define do
  factory :component_value do
    association :component_field, factory: :component_field_model

    ignore do
      visible true
    end

    factory :component_value_article do
      association :component_field, factory: :component_field_model_article
      association :referable, factory: :article
      after(:build) do |value, evaluator|
        value.referable.is_visible = evaluator.visible
      end
    end

    factory :component_value_section do
      ignore do
        visible true
      end
      association :component_field, factory: :component_field_model_section
      association :referable, factory: :section
      after(:build) do |value, evaluator|
        value.referable.is_visible = evaluator.visible
      end
    end

    factory :component_value_medium do
      ignore do
        visible true
        with_component false
      end

      component_field nil
      association :referable, factory: :embedded_medium

      after(:build) do |value, evaluator|
        unless value.component_field
          value.component_field = build :component_field_model, model_class: 'Medium', with_component: evaluator.with_component, component_value: value
        end
      end
    end

    factory :component_value_gallery do
      ignore do
        visible true
        with_component false
      end

      component_field nil
      association :referable, factory: :media_gallery

      after(:build) do |value, evaluator|
        unless value.component_field
          value.component_field = build :component_field_model, model_class: 'Gallery', with_component: evaluator.with_component, component_value: value
        end
      end
    end
  end
end
