FactoryGirl.define do
  factory :client_application_assets_base_representation do
    client_application_asset
    base_representation { FactoryGirl.build :representation }
  end
end
