FactoryGirl.define do
  factory :row, class: Row do
    representation
    sequence(:order, 1)
    factory :app1_representation1_row1 do
      order 1
      association :representation, factory: :app1_representation1
    end

    factory :app1_representation1_row2 do
      order 2
      association :representation, factory: :app1_representation1, strategy: :build
    end

    factory :app1_representation1_with_draft_row1 do
      order 1
      association :representation, factory: :draft_for_app1_representation1, strategy: :build
    end

    factory :app1_representation1_with_draft_row2 do
      order 2
      association :representation, factory: :draft_for_app1_representation1, strategy: :build
    end

    factory :app1_representation2_row1 do
      order 1
      association :representation, factory: :app1_representation2, strategy: :build
    end
  end
end
