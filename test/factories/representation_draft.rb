FactoryGirl.define do
  factory :representation_draft, class: RepresentationDraft do
    sequence(:name) { |n| "Representation draft #{n}" }
    association :client_application, factory: :client_application, strategy: :build
    association :representation, factory: :representation
  end
end