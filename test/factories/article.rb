FactoryGirl.define do
  factory :article do
    heading   'heading one'
    sequence(:title) { |i| "title #{i}" }
    lead      'lead one'
    body      'body one'
    signature 'signature one'
    time      '15:58:50'

    ignore do
      supplement { FactoryGirl.build :supplement }
    end

    after(:build) do |article, evaluator|
      article.section = FactoryGirl.build :section, supplement: evaluator.supplement unless article.section
      article.edition = FactoryGirl.build :edition, supplement: evaluator.supplement unless article.edition
    end

    factory :article_without_edition do
      after(:build) do |article, _|
        article.edition = nil
      end
    end

    factory :copyable_article do
      tags { [FactoryGirl.create(:tag, name: 'Tag 1'), FactoryGirl.create(:tag, name: 'Tag 2')] }
      article_media { [FactoryGirl.create(:article_medium), FactoryGirl.create(:article_medium, medium: FactoryGirl.create(:image_medium))] }
      articles { [FactoryGirl.create(:article), FactoryGirl.create(:article, title: 'article 2')] }
    end

    factory :visible_article do
      after(:build) do |article, evaluator|
        article.section = FactoryGirl.create :visible_section, supplement: evaluator.supplement
        article.edition = FactoryGirl.create :edition, supplement: evaluator.supplement
      end
    end

    factory :non_visible_article do
      after(:build) do |article, evaluator|
        article.section = FactoryGirl.create :slug_section, supplement: evaluator.supplement
        article.edition = FactoryGirl.create :non_visible_edition, supplement: evaluator.supplement
      end
    end
  end
end
