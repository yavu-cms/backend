FactoryGirl.define do
  factory :component_translation do
    association :component, strategy: :build
    sequence(:locale, 'aa')
    values 'key: value'
  end
end
