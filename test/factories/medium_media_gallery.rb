FactoryGirl.define do
  factory :medium_media_gallery do
    sequence(:order)
    association :medium, factory: :image_medium, strategy: :build
    association :media_gallery, strategy: :build
  end
end