FactoryGirl.define do
  factory :gallery do
    sequence(:title) { |n| "Gallery #{n}" }

    factory :media_gallery, class: 'MediaGallery'
  end
end