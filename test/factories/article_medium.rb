FactoryGirl.define do
  factory :article_medium do
    association :medium, factory: :image_medium, strategy: :build
    association :article, strategy: :build
    caption 'an article medium'
    main    true
    order   1
  end
end

