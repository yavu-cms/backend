# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :config, class: 'ApplicationConfiguration' do
    key :test
    value 'test'
  end
end