FactoryGirl.define do
  factory :section do
    sequence(:name) { |i| "Name #{i}" }
    supplement

    initialize_with { new(name: name, supplement: supplement) }

    factory :slug_section do
      name 'slug section'
    end

    factory :visible_section, class: Section do
      name 'Visible Section'
      is_visible true
    end

    factory :invisible_section, class: Section do
      name 'Invisible Section'
      is_visible false
    end

    factory :child_section, class: Section do
      association :parent, factory: :section
    end

    factory :child_child_section, class: Section do
      association :parent, factory: :child_section
    end

    factory :another_child_section, class: Section do
      association :parent, factory: :section
    end

    factory :migration, class: Section do
      name 'migration'
    end
  end
end
