FactoryGirl.define do
  factory :component do
    sequence(:name,1) { |x| "Component name #{x}" }
    enabled true
    context ''
    readme 'readme'
    extras ''
    description 'description'
    ignore do
      views_count 1
      translations_count 0
      with_assets false
    end

    after(:build) do |component, evaluator|
      component.views = FactoryGirl.build_list(:view, evaluator.views_count, component: component)
      component.translations = FactoryGirl.build_list(:component_translation, evaluator.translations_count, component: component)
      component.assets = [
          FactoryGirl.build(:component_asset_not_editable, component: component),
          FactoryGirl.build(:component_asset_editable, component: component)
      ] if evaluator.with_assets
    end
    factory :cover_component, class: CoverComponent do
      ignore do
        views_count 0
      end
    end
    factory :cover_articles_component, class: CoverArticlesComponent
  end
end
