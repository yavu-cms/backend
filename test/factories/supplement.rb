FactoryGirl.define do
  factory :supplement do
    name 'supplement'
    association :news_source, strategy: :build

    factory :supplement_with_section do
      after(:create) do |supplement, _|
        supplement.sections << FactoryGirl.create(:section, supplement: supplement)
        supplement.editions << FactoryGirl.create(:edition, supplement: supplement)
        supplement.save!
      end
    end

    factory :default_supplement do
      is_default true

      factory :active_edition_supplement do
        after(:create) do |supplement, evaluator|
          supplement.active_edition = FactoryGirl.create(:active_edition, supplement: supplement)
          supplement.save!
        end
      end

      before(:build) do |supplement, evaluator|
        supplement.news_source.is_default = true
      end
    end

    factory :non_default_supplement do
      is_default false

      factory :non_default_supplement_with_active_edition do
        after(:create) do |supplement, evaluator|
          supplement.active_edition = FactoryGirl.create(:edition, supplement: supplement)
          supplement.save!
        end
      end
    end
  end
end
