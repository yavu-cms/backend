FactoryGirl.define do
  factory :client_application do
    sequence(:name) { |n| "Application #{n}" }
    sequence(:url) { |n| "http://www.example-#{n}.com" }
    skip_initialize_routes true

    after(:build) do |client_application, evaluator|
      client_application.news_source = FactoryGirl.build :news_source
    end

    factory :client_application_with_routes do
      skip_initialize_routes false
    end

    factory :disabled_client_application do
      enabled false
    end

    factory :client_application_with_assets do
      after(:build) do |client_application, evaluator|
        client_application.assets = [
          FactoryGirl.build(:client_application_asset_not_editable, client_application: client_application),
          FactoryGirl.build(:client_application_asset_editable, client_application: client_application)
        ]
      end
    end
  end
end
