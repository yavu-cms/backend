FactoryGirl.define do
  factory :representation, class: Representation do
    sequence(:name) { |n| "Representation #{n}" }
    client_application

    factory :app1_representation1 do
      name 'Representation 1 for client_application'
    end

    factory :draft_for_app1_representation1, class: RepresentationDraft do
      name 'Draft for app1_representation1'
    end

    factory :app1_representation1_with_draft do
      name 'Representation 1 for client_application with a draft'
      association :draft, factory: :draft_for_app1_representation1, strategy: :build
    end

    factory :app1_representation2 do
      name 'Representation 2 for client_application'
    end

    factory :app2_representation1 do
      name 'Representation 1 for another_client_application'
      association :client_application, factory: :another_client_application, strategy: :build
    end

    factory :representation_with_assets, class: Representation do
      association :client_application, factory: :client_application_with_assets
      after(:build) do |representation, evaluator|
        representation.client_application.assets.each do |x|
          representation.client_application_assets_base_representations.build(client_application_asset: x)
        end
      end
    end

    factory :cover_representation do
      after(:build) do |representation, evaluator|
        representation.rows << (row = FactoryGirl.build(:row, representation: representation))
        row.columns << (column = FactoryGirl.build(:column, row: row))
        column.component_configurations << FactoryGirl.build(:cover_component_configuration, column: column, cover_articles_count: 1)
      end
      after(:create) do |representation, evaluator|
        representation.reload
      end
    end

    factory :representation_with_component do
      ignore do
        component_configuration nil
      end
      after(:build) do |representation, evaluator|
        representation.rows << (row = FactoryGirl.build(:row, representation: representation))
        row.columns << (column = FactoryGirl.build(:column, row: row))
        column.component_configurations << 
        if evaluator.component_configuration
          evaluator.component_configuration.column = column
          evaluator.component_configuration
        else
          FactoryGirl.build(:component_configuration, column: column)
        end
      end
    end
  end
end
