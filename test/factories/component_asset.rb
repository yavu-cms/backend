FactoryGirl.define do
  factory :component_asset, aliases: [:component_asset_not_editable, :component_asset_image] do
    component
    file { upload_test_file('image_1.jpg')} 
  end

  factory :component_asset_editable, class: ComponentAsset, aliases: [:component_asset_javascript] do
    component
    file { upload_test_file('javascript.js')} 
  end

  factory :component_asset_stylesheet, class: ComponentAsset do
    component
    file { upload_test_file('stylesheet.css')} 
  end


end
