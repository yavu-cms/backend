FactoryGirl.define do

  factory :shortcut do |variable|
    title 'a shortcut'
    sequence(:url) { |n| "a.shortcut.org/#{n}" }
    association :user
    show_in_menu false

    factory :admin_shortcut, class: Shortcut do
      association :user, factory: :admin
    end

    factory :journalist_shortcut, class: Shortcut do
      association :user, factory: :journalist
    end
  end
end
