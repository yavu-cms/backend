FactoryGirl.define do
  factory :component_field do
    sequence(:name, 1) { |n| "field_#{n}" }
    ignore do
      with_component false
    end

    factory :component_field_scalar, class: ComponentFieldScalar do
      default 'some scalar value'
    end

    factory :component_field_integer, class: ComponentFieldInteger do
      default 10
    end

    factory :component_field_hash, class: ComponentFieldHash do
      default Hash[key: 'value']
    end

    factory :component_field_model, class: ComponentFieldModel do
      factory :component_field_model_article do
        model_class 'Article'
        ignore do
          visible true
        end

        after(:build) do |field, evaluator|
          field.component_value = FactoryGirl.build(:component_value_article, component_field: field, visible: evaluator.visible)
        end
      end

      factory :component_field_model_section do
        model_class 'Section'
        ignore do
          visible true
        end

        after(:build) do |field, evaluator|
          field.component_value = FactoryGirl.build(:component_value_section, component_field: field, visible: evaluator.visible)
        end
      end

      factory :component_field_model_medium do
        model_class 'Medium'
        ignore do
          visible true
        end

        after(:build) do |field, evaluator|
          field.component_value = FactoryGirl.build(:component_value_medium, component_field: field, visible: evaluator.visible)
        end
      end
    end

    factory :component_field_multi_model, class: ComponentFieldMultiModel do
      ignore do
        values_count 0
        visible true
      end

      factory :component_field_multi_model_article do
        model_class 'Article'

        after(:build) do |field, evaluator|
          field.component_values = FactoryGirl.build_list(:component_value_article, evaluator.values_count, component_field: field, visible: evaluator.visible)
        end
      end

      factory :component_field_multi_model_section do
        model_class 'Section'

        after(:build) do |field, evaluator|
          field.component_values = FactoryGirl.build_list(:component_value_section, evaluator.values_count, component_field: field, visible: evaluator.visible)
        end
      end

      factory :component_field_multi_model_medium do
        model_class 'Medium'

        after(:build) do |field, evaluator|
          field.component_values = FactoryGirl.build_list(:component_value_medium, evaluator.values_count, component_field: field)
        end
      end
    end

    factory :component_field_ordered_multi_model, class: ComponentFieldOrderedMultiModel do
      ignore do
        values_count 0
      end

      factory :component_field_ordered_multi_model_article do
        model_class 'Article'

        after(:build) do |field, evaluator|
          field.component_values = FactoryGirl.build_list(:component_value_article, evaluator.values_count, component_field: field)
        end
      end

      factory :component_field_ordered_multi_model_section do
        model_class 'Section'

        after(:build) do |field, evaluator|
          field.component_values = FactoryGirl.build_list(:component_value_section, evaluator.values_count, component_field: field)
        end
      end

      factory :component_field_ordered_multi_model_medium do
        model_class 'Medium'

        after(:build) do |field, evaluator|
          field.component_values = FactoryGirl.build_list(:component_value_medium, evaluator.values_count, component_field: field)
        end
      end
    end

    after(:build) do |component_field, evaluator|
      if evaluator.with_component
        component_field.component = create :component
      end
    end
  end
end
