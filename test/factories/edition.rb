FactoryGirl.define do
  factory :edition do
    name 'Edition'
    date '2013-03-01 12:00:00'
    is_visible true
    supplement

    factory :unamed_edition do
      name nil
      date { Date.today }
    end

    factory :non_default_supplement_edition do
      date '2014-03-20 12:00:00'
      association :supplement, factory: :non_default_supplement
    end

    factory :supplement_edition do
      date '2014-03-20 12:00:00'
      association :supplement, factory: :default_supplement
    end

    factory :active_default_supplement_edition do
      date '2014-03-20 12:00:00'
      is_active true
      association :supplement, factory: :default_supplement
    end

    factory :non_visible_edition do
      is_visible false
    end

    factory :active_edition do
      is_active true
    end

    factory :inactive_edition do
      date '1999-03-01 12:00:00'
      is_active false
    end

    factory :edition_with_articles do
      after(:build) do |edition, evaluator|
        edition.articles = FactoryGirl.build_list(:article, 5, section: build(:section, supplement: edition.supplement), edition: edition, supplement: edition.supplement)
      end
    end
  end
end
