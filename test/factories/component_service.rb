FactoryGirl.define do
  factory :component_service do
    sequence(:name) { |n| "Service #{n}" }
    association :component, strategy: :build
    http_method 'GET'
    body 'true'
    association :view, strategy: :build
  end
end
