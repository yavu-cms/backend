FactoryGirl.define do
  factory :extra_attribute_without_model_classes, class: ExtraAttribute do
    sequence(:name,1) { |x| "name_#{x}" }
    attribute_type :boolean

    factory :extra_attribute do
      after(:build) do |extra_attribute, evaluator|
        extra_attribute.send :write_attribute, :model_classes, ExtraAttribute.initial_model_classes
      end

      factory :extra_attribute_boolean, class: ExtraAttribute

      factory :extra_attribute_string, class: ExtraAttribute do
        attribute_type :string
      end
    end
  end
end

