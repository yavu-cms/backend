FactoryGirl.define do
  factory :component_configuration do
    description 'Component configuration'
    column
    association :component, factory: :component, strategy: :build
    sequence(:order) { |n| n }
    after(:build) do |component_configuration,evaluator|
      unless evaluator.view
        component_configuration.view = component_configuration.component.views.reject(&:partial?).sample if component_configuration.component
      end
    end
    factory :cover_component_configuration, class: CoverComponentConfiguration do
      ignore do
        cover_articles_count 0
      end
      association :component, factory: :cover_component, strategy: :build
      after(:build) do |cover_component_configuration, evaluator|
        cover_component_configuration.cover_articles_component_configurations = FactoryGirl.build_list(
          :cover_articles_component_configuration,
          evaluator.cover_articles_count,
          cover_component_configuration: cover_component_configuration
        ) if evaluator.cover_articles_count > 0
      end
    end
    factory :cover_articles_component_configuration, class: CoverArticlesComponentConfiguration do
      column nil
      association :component, factory: :cover_articles_component, strategy: :build
      association :cover_component_configuration, factory: :cover_component_configuration, strategy: :build
    end
  end
end
