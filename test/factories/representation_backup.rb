FactoryGirl.define do
  factory :representation_backup, class: RepresentationBackup do
    sequence(:name) { |n| "Representation backup #{n}" }
    association :client_application, factory: :client_application, strategy: :build
    association :representation, factory: :representation
  end
end