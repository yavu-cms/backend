FactoryGirl.define do
  factory :articles_cover_articles_component_configuration do
    association :view, factory: :cover_articles_component_view
    association :article, factory: :article
    association :cover_articles_component_configuration, factory: :cover_articles_component_configuration

    factory :articles_cover_articles_component_configuration_no_view do
      view nil
      after(:build) do |object, evaluator|
        object.view = FactoryGirl.build(:view, component: object.component) unless object.view
        object.component.views << object.view
      end
    end

  end
end