FactoryGirl.define do
  factory :user do
    sequence(:username) { |n| "user-#{n}" }
    sequence(:email) { |n| "user-#{n}@example.org" }
    new_password 'user'

    ignore do
      permissions []
    end

    after(:build) do |user, evaluator|
      if evaluator.permissions.present?
        role = create :role, name: "ad-hoc-for-#{user.username}", permissions: evaluator.permissions
        user.roles << role
      end
    end

    factory :admin do
      username 'admin'
      all_sections true
    end

    factory :editor do
      username 'editor'
    end

    factory :journalist do
      username 'journalist'
    end

    factory :designer do
      username 'designer'
    end
  end
end
