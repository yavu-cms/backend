FactoryGirl.define do
  factory :view do
    sequence(:name) { |x|  "View #{x}" }
    value '<%= Hello_World %>'

    factory :component_view, class: View do
      component
    end

    factory :cover_articles_component_view do
      association :component, factory: :cover_articles_component, strategy: :build
    end

    factory :view_without_placeholders do
      value '<h1>Hi there!</h1>'
    end

    factory :view_with_placeholders do
      value "<div>#{View::CONTENT_PLACEHOLDER}</div>"
    end
  end
end
