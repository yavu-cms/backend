FactoryGirl.define do
  factory :audio_medium do
    type 'AudioMedium'
    file { upload_test_file('canario.mp3') }

    factory :audio_medium_in_use_by_article, traits: [:in_use_by_article]
    factory :audio_medium_in_use_by_gallery, traits: [:in_use_by_gallery]
    factory :audio_medium_in_use_by_component_value, traits: [:in_use_by_component_value]
  end

  factory :embedded_medium do
    type 'EmbeddedMedium'
    content 'embedded content'

    factory :embedded_medium_in_use_by_article, traits: [:in_use_by_article]
    factory :embedded_medium_in_use_by_gallery, traits: [:in_use_by_gallery]
    factory :embedded_medium_in_use_by_component_value, traits: [:in_use_by_component_value]
    factory :embedded_medium_in_use_by_article_and_gallery, traits: [:in_use_by_article, :in_use_by_gallery]
  end

  factory :file_medium do
    type 'FileMedium'
    file { upload_test_file('file_medium.txt') }

    factory :file_medium_in_use_by_article, traits: [:in_use_by_article]
    factory :file_medium_in_use_by_gallery, traits: [:in_use_by_gallery]
    factory :file_medium_in_use_by_component_value, traits: [:in_use_by_component_value]
  end

  factory :image_medium do
    type 'ImageMedium'
    xsmall_version '50x50'
    small_version  '100x100'
    medium_version '200x200'
    big_version    '300x300'
    xbig_version   '400x400'
    file { upload_test_file('image_1.jpg') }

    factory :image_medium_in_use_by_article, traits: [:in_use_by_article]
    factory :image_medium_in_use_by_gallery, traits: [:in_use_by_gallery]
    factory :image_medium_in_use_by_component_value, traits: [:in_use_by_component_value]
  end

  trait :in_use_by_article do
    after(:build) do |medium, evaluator|
      medium.article_media = FactoryGirl.build_list :article_medium, 1, medium: medium
    end
  end

  trait :in_use_by_gallery do
    after(:build) do |medium, evaluator|
      medium.medium_media_galleries = FactoryGirl.build_list :medium_media_gallery, 1, medium: medium
    end
  end

  trait :in_use_by_component_value do
    ignore do
      with_component true
    end

    after(:build) do |medium, evaluator|
      medium.component_values = FactoryGirl.build_list :component_value_medium, 1, referable: medium, with_component: evaluator.with_component
    end
  end
end
