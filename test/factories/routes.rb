# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :route do
    name "A route"

    after(:build) do |route, evaluator|
      route.representation ||= FactoryGirl.build :representation
      route.client_application ||= FactoryGirl.build :client_application
    end

    factory :homepage_route, class: HomepageRoute do
      name "A homepage route"
      pattern "/homepage"
    end
    factory :redirect_route, class: RedirectRoute do
      name "A redirect route"
      pattern "/redirect_to/blablabla"
      redirect_url "hola.com"
    end
    factory :article_route, class: ArticleRoute do
      name "An article route"
      pattern "/article"
    end
    factory :section_route, class: SectionRoute do
      name "A section route"
      pattern "/section"
    end
  end
end
