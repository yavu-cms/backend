FactoryGirl.define do
  factory :news_source do
    sequence(:name) { |i| "News source number #{i}" }
  end
end
