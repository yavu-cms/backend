$(document).ready(function() {
  var $carousel = $('.carousel');
  var $links = $carousel.find('a').hide();
  var i = 0;
  setInterval(function() {
    $links.eq(i).fadeIn(250).delay(3000).fadeOut(250, function() {
      $(this).appendTo($carousel);
      i = (i + 1) % $links.length;
    });
  }, 4000);
});