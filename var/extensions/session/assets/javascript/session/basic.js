/*
 * SimpleModal Basic Modal Dialog
 * http://simplemodal.com
 *
 * Copyright (c) 2013 Eric Martin - http://ericmmartin.com
 *
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 */

jQuery(function($) {
  // Load dialog on click
  $(document.body).on('click', '.basic-modal .basic', function() {
    var modalContent = $('#basic-modal-content');
    modalContent.modal();
    set_optimal_size(modalContent);
    return false;
  });

  setTimeout(function() {
    $('.basic-modal.show-immediately .basic').trigger('click');
  }, 1500);

  function set_optimal_size(pivot) {
    if (pivot.hasClass('edition-mode')) {
      pivot.closest('.simplemodal-container').css({width: '250px', left: '50%'});
    }
  }
});
