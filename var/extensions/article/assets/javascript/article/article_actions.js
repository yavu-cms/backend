;jQuery(function($) {
  var $container = $('.article--main'),
  	$actions = $container.find('.actions'),
    $resizable = $container.find('.text-resizable'),
    UPPER_LIMIT = 250,
    LOWER_LIMIT = 25;
  
  function resizeText($elements, size) {
    var current = $container.data('current-text-size') || 100,
      newSize = Math.max(Math.min(current + size, UPPER_LIMIT), LOWER_LIMIT);

  	$container.data('current-text-size', newSize);
  	$elements.css('font-size', newSize + '%');
  }
  
  $actions
    .on('click', '.action-print', function(e) {
      e.preventDefault();
      window.print();
      return false;
    })
    .on('click', '.action-shrink', function(e) {
    	e.preventDefault();
      resizeText($resizable, -10);
      return false;
    })
    .on('click', '.action-expand', function(e) {
    	e.preventDefault();
      resizeText($resizable, 10);      
      return false;
    });
});