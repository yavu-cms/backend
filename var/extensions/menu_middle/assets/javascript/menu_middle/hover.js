; jQuery(function() {
  var $menu = $('.menu_middle');
  
  function findGroup($pivot) {
    var sectionClass = $pivot.attr('class').match(/section-\d+/);
    return $menu.find('.' + sectionClass);
  }
  
  function hoverIn() {
		findGroup($(this)).addClass('selected');
  }
  
  function hoverOut() {
		findGroup($(this)).removeClass('selected');
  }
  
  $menu
  	.on('mouseenter', '.menu_middle_section, .menu_middle_dropdown', hoverIn)
    .on('mouseleave', '.menu_middle_section, .menu_middle_dropdown', hoverOut);
});