jQuery(function($) {
  var $customizer = $('.customizer');
  
  $(document.body).addClass($customizer.data('selected'));

  $customizer
    .on('click', '.switch', function(e) {
      var $switch = $(this);
      e.preventDefault();
      $('body')
        .removeClass($customizer.data('selected'))
        .addClass($switch.data('theme'));
      $customizer.data('selected', $switch.data('theme'));
      return false;
    }).on('click', '.handle', function(e) {
      e.preventDefault();
      $customizer.find('.drawer').toggleClass('visible');
      return false;
    });

  // Make this the first element in the page
  $customizer.prependTo(document.body);
});
