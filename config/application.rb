require File.expand_path('../boot', __FILE__)
require_relative '../app/middlewares/health_check'

require 'rails/all'
require 'elasticsearch/rails/instrumentation'

# Assets should be precompiled for production (so we don't need the gems loaded then)
Bundler.require(*Rails.groups(assets: %w(development test)))

module BackendApp
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Do not enforce available locales if nothing has been said about that
    I18n.enforce_available_locales ||= false

    # Custom directories with classes and modules you want to be autoloadable.
    config.autoload_paths += Dir["#{config.root}/app/models/**"]

    # Configure sensitive parameters which will be filtered from the log file.
    config.filter_parameters += [:password]

    # Use SQL instead of Active Record's schema dumper when creating the database.
    # This is necessary if your schema can't be completely dumped by the schema dumper,
    # like if you have constraints or database-specific column types.
    # config.active_record.schema_format = :sql

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    Rails.application.config.time_zone = YavuSettings.i18n.time_zone

    # Enable the asset pipeline.
    config.assets.enabled = true

    # Version of your assets, change this if you want to expire all your assets.
    config.assets.version = '1.0.1'
    # Added so that third party gems can provide images as assets
    config.assets.paths << Rails.root.join('vendor', 'assets')
    config.assets.paths << Rails.root.join('vendor', 'assets', 'images')
    config.assets.paths << Rails.root.join('vendor', 'assets', 'fonts')
    config.assets.precompile += %w( modernizr.js )
    config.assets.precompile += %w( all-plugins.js all-plugins.css libs.js libs.css )
    config.assets.precompile += %w( foundation-icons.eot foundation-icons.svg foundation-icons.ttf foundation-icons.woff )
    # Nasty hack to include Faye's JS files in the asset pipeline
    begin
      faye = Bundler.load.specs.find { |s| s.name == 'faye' }
      config.assets.paths += faye.load_paths
    rescue
    end

    # factory girl generator
    config.generators do |g|
      g.fixture_replacement :factory_girl
      g.orm :active_record
    end

    config.middleware.use Rack::Deflater

    config.middleware.delete Rack::ETag

    config.middleware.insert_before Rails::Rack::Logger, HealthCheck

    config.cache_store = :redis_store, YavuSettings.redis.cache_store, { expires_in: 30.minutes }
  end
end
