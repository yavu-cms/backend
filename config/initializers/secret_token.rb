# Be sure to restart your server when you modify this file.

# Your secret key for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Rails.application.config.secret_key_base = ENV['SECRET_KEY_BASE'] || 'bd6e9957a25db73755be28ea505aa42124dd226aa08f79a313bd4acb986734c0a74c86a5ae9f0d78df1ed9852100cba4ba98078e61f13eaf0aeee961ba29ecf2'
