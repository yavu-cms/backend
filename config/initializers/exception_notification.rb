require 'exception_notification/rails'
require 'exception_notification/sidekiq'

ExceptionNotification.configure do |config|
  # Ignore additional exception types.
  # ActiveRecord::RecordNotFound, AbstractController::ActionNotFound and ActionController::RoutingError are already added.
  config.ignored_exceptions += %w{ActionController::InvalidAuthenticityToken}

  # Adds a condition to decide when an exception must be ignored or not.
  # The ignore_if method can be invoked multiple times to add extra conditions.
  config.ignore_if do |exception, options|
    !Rails.env.production?
  end

  # Notifiers =================================================================

  # Email notifier sends notifications by email.
  config.add_notifier :email, {
    email_format: :html,
    email_prefix: ENV['EXCEPTION_NOTIFICATION_PREFIX'] || '[Exception - Yavu] ',
    sender_address: ENV['EXCEPTION_NOTIFICATION_SENDER'],
    exception_recipients: ENV['EXCEPTION_NOTIFICATION_RECIPIENTS'].to_s.split(',')
  }

  # Error grouping
  config.error_grouping = true
end
