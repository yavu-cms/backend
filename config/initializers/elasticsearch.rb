# Be sure to restart your server when you modify this file.

# YavuSettings.elasticsearch.client is passed in as is to the Elasticsearch::Client class to initialize the client
# instance to be shared across models.
#
# You may refer to https://github.com/elasticsearch/elasticsearch-ruby/tree/master/elasticsearch-transport#configuration
# for every available configuration option
if YavuSettings.elasticsearch.enabled && YavuSettings.elasticsearch.client.present?
  Elasticsearch::Model.client = Elasticsearch::Client.new YavuSettings.elasticsearch.client
  # Create the indices of each searchable model
  Article.__elasticsearch__.create_index!
end
