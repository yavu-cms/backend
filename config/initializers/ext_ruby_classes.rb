class Hash
   def walk(pre: ->(_,_){}, post: ->(_,_){})
     each do |k, v|
       pre.(k,v)
       v.walk(pre: pre, post: post) if v.is_a?(Hash)
       post.(k,v)
     end
   end
end
