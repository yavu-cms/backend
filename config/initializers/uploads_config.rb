Rails.application.config.to_prepare do
  # Defaults for every uploader
  CarrierWave.configure do |config|
    config.cache_dir = "#{Rails.root}/tmp/uploads"
  end

  # Multimedia settings
  BaseUploader.configure do |config|
    config.base_path = YavuSettings.media.upload_base_path
    config.root = File.join YavuSettings.media.upload_root_dir, config.base_path
  end

  # Component assets settings
  ComponentAssetUploader.configure do |config|
    config.root = YavuSettings.assets.upload_dir.component
  end

  # Client assets settings
  ClientApplicationAssetUploader.configure do |config|
    config.root = YavuSettings.assets.upload_dir.client_application
  end
end
