# Sidekiq Configuration
raise 'YavuSettings.redis.namespace not set' unless YavuSettings.redis.namespace
raise 'YavuSettings.sidekiq.namespace not set' unless YavuSettings.sidekiq.namespace
redis_configuration = { url: YavuSettings.redis.url, namespace: "#{YavuSettings.redis.namespace}:#{YavuSettings.sidekiq.namespace}" }
sidekiq_log_level   = Logger.const_get YavuSettings.sidekiq.log_level.to_s.upcase rescue Logger::INFO
Sidekiq::Logging.logger.level = sidekiq_log_level
Sidekiq::Logging.logger = nil if Rails.env.test?

config_block = proc do |config|
  config.redis = redis_configuration
end

# Server-side
Sidekiq.configure_server &config_block

# Client-side
Sidekiq.configure_client &config_block

# Enable sidekiq unique jobs args
SidekiqUniqueJobs.config.unique_args_enabled = true
