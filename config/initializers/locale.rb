# The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
# Rails.application.config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
Rails.application.config.i18n.default_locale = YavuSettings.i18n.default_locale
