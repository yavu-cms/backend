# The following line enables caching. Comment it to disable caching api_resources
require 'yavu/api/resource/resourceable_cache' if YavuSettings.cache.enabled
require 'yavu/api/ext/active_record/base'
require 'yavu/frontend/server/preview'
require 'pp'
