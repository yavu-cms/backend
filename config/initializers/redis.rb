raise 'YavuSettings.redis.url must be set' unless YavuSettings.redis.url
raise 'YavuSettings.redis.namespace must be set' unless YavuSettings.redis.namespace
$redis = Redis::Namespace.new YavuSettings.redis.namespace, redis: Redis.new(url: YavuSettings.redis.url)
