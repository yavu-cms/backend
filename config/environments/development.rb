BackendApp::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports and disable caching.
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false
#  config.action_controller.perform_caching = true

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = false

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Only use best-standards-support built into browsers.
  config.action_dispatch.best_standards_support = :builtin

  # Raise an error on page load if there are pending migrations
  config.active_record.migration_error = :page_load

  # Raise an error if the requested locale is not available
  config.i18n.enforce_available_locales = true

  # Debug mode disables concatenation and preprocessing of assets.
  config.assets.debug = true

  # Mailers settings
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = YavuSettings.mailer.smtp_settings.to_hash

  # General mailer configurations
  config.action_mailer.raise_delivery_errors = true
  config.action_mailer.perform_deliveries    = true
  config.action_mailer.default_url_options = { host: 'localhost', port: 3000 }
end

