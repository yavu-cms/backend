Rails.application.routes.draw do
  devise_for :users

  mount YavuSurveys::Engine  => '/surveys',   as: :yavu_surveys if defined?(YavuSurveys::Engine)

  root to: 'dashboard#index'

  get 'dashboard', to: 'dashboard#index'
  get "covers", to: 'covers#index'
  get 'jobs', to: 'dashboard#jobs'
  post 'dashboard/most_box', to: 'dashboard#most_box'

  # API main controller
  get 'api/register', to: 'api#register'
  get 'api/context/:id', to: 'api#context'
  post 'api/accounting', to: 'api#accounting'
  get 'api/:id', to: 'api#show'
  match 'api/service/:id/:component_configuration_id/:service_name', to: 'api#service', via: :all
  match 'api/service/preview/:id/:representation/:route/:component_configuration_id/:service_name', to: 'api#service_preview', via: :all
  post 'api/alive', to: 'api#alive'

  # Serve uploaded component asset content
  get 'component_asset_serve/:id', to: "asset#serve_component_content", as: 'component_asset_serve'

  # Serve uploaded client_application asset content
  get 'client_application_asset_serve/:id', to: "asset#serve_client_application_asset_content", as: 'client_application_asset_serve'

  # Asset Pipeline for frontend preview
  get "#{YavuSettings.assets.prefix}/:identifier/*path",  to: ClientApplicationSprocketsController, via: :all, format: false
  get "#{YavuSettings.assets.prefix_preview}/:identifier/*path",  to: ClientApplicationSprocketsPreviewController, via: :all, format: false

  # Frontend clients management
  get 'frontend_servers', to: "frontend_servers#index"
  post 'frontend_servers/:id/:client_application_id', to: "frontend_servers#configure", as: 'configure_frontend_server'
  put 'frontend_servers/:id', to: "frontend_servers#restart", as: 'restart_frontend_server'

  resources :media do
    collection do
      get 'search'
      get 'autocomplete'
      # Type-specific new object routes
      get 'new_audio'
      get 'new_embedded'
      get 'new_file'
      get 'new_image'
    end

    member do
      get 'find'
    end
  end

  resources :extra_attributes, except: [:show]

  resources :settings, only: [:index] do
    collection do
      get 'edit_all'
      put 'update_all'
    end
  end

  resources :shortcuts, except: [:show, :index] do
    collection do
      get 'manage'
    end

    member do
      put 'remove_from_menu'
      put 'add_to_menu'
    end
  end

  resources :articles do
    member do
      get 'copy'
      get 'media', to: 'articles#media_by_article'
    end

    collection do
      get 'most_commented'
      get 'most_read'
      post 'create_copy'
      get 'search',  as: :search
      get 'search_by_section_and_edition', as: :search_by_section_and_edition
    end
  end

  resources :supplements do
    member do
      post 'become_default'
      get 'sections'
      get 'editions'
      get 'active_edition'
    end
  end

  resources :news_sources do
  end

  resources :sections do
    collection do
      get 'tree'
      get 'search',  as: :search
    end

    member do
      get 'articles'
    end
  end

  resources :tags do
    member do
      get 'combine'
      post 'combine_update'
      post 'blacklist'
      post 'unblacklist'
    end

    collection do
      post 'separate'
      get  'search', as: :search
    end
  end

  resources :editions do
    member do
      put 'become_active'
      get 'articles'
    end

    collection do
      get 'search',  as: :search
    end
  end

  resources :client_applications do
    member do
      get  'preview'
      get  'invalidate_routes'
      get  'extra_attributes'
      get  'update_preview'
      post 'enable'
      post 'disable'
      get  'copy'
      post 'create_copy'
      post 'compile_assets'
      post 'notify_clients'
      post 'invalidate_routes', to: 'client_applications#purge_routes'
      patch 'extra_attributes', to: 'client_applications#manage_extra_attributes'
    end

    collection do
      get 'search',  as: :search
    end

    resources :client_application_assets, as: :assets, path: 'assets' do
      member do
        get 'dissasociate_representations'
        get 'edit_content'
        get 'rename'
        post 'toggle_as'
        patch 'update_name'
        patch 'update_content'
      end
      collection do
        get  'new_empty'
        post 'create_empty'
      end
    end

    resources :routes, as: :routes

    resources :representations, as: :representations do
      member do
        get    'rename'
        get    'cover', to: 'covers#edit'
        patch  'update_cover', to: 'covers#update'
        patch  'update_name'
        get    'copy'
        post   'create_copy'
        delete 'discard_draft'
        get    'preview'
        post   'apply'
        get    'manage_assets'
      end

      resources :base_representations_views, as: :views, path: 'views' do
        member do
          post 'associate'
          post 'disassociate'
        end
      end

      resources :representation_assets, as: :assets, path: 'assets', only: [:index, :show, :edit, :new] do
        collection do
          patch 'batch_action'
          get   'new_empty'
        end
        member do
          get   'rename'
          get   'edit_content'
          post  'toggle_into_representation'
          post  'toggle_as_favicon'
        end
      end

      resources :backups, only: [:index, :show, :destroy] do
        member do
          post 'restore'
        end
      end
    end

    resources :newsletters, as: :newsletters do
      member do
        post 'enable'
        post 'disable'
        get  'sendmail'
      end
    end
  end

  resources :galleries do
    collection do
      get 'new_media'
      get 'search'
    end
  end

  resources :users
  resources :roles
  get 'profile', to: 'users#profile'
  patch 'profile', to: 'users#update_profile'

  resources :components do
    member do
      get  'copy'
      post 'create_copy'
      get  'configuration_form'
      get  'enable'
      get  'disable'
    end

    collection do
      get 'search',  as: :search
    end

    resources :services, controller: 'component_services'

    resources :views, controller: 'component_views' do
      member do
        post :make_default
      end
    end

    resources :component_translations, path: 'translations', as: :translations

    resources :component_assets, as: :assets, path: 'assets' do
      member do
        get 'edit_content'
        get 'rename'
        patch 'update_name'
        patch 'update_content'
      end

      collection do
        get   'new_empty'
        post  'create_empty'
      end
    end
  end

  get 'auditing', to: 'versions#index', as: 'auditing'
  resources :versions, only: [:index, :show, :destroy]
  get 'versions/:id/diff_current', to: 'versions#diff_current', as: 'diff_current'
  post 'versions/:id/restore', to: 'versions#restore', as: 'restore_version'

  # Sidekiq panel
  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'

  # Debug/Development routes
  if Rails.env.development?
    get 'debug', to: 'debug#index'
    get 'debug/icons', to: 'debug#icons'
  end
end
