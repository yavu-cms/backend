# Load the rails application.
require File.expand_path('../application', __FILE__)

# Initialize the rails application.
Rails.application.initialize!

# Patch for migrations (See: https://github.com/rails/rails/pull/13247)
require File.expand_path('../../lib/patches/abstract_mysql_adapter', __FILE__)
